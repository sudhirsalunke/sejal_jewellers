-- MySQL dump 10.13  Distrib 5.6.35, for Linux (i686)
--
-- Host: localhost    Database: lms
-- ------------------------------------------------------
-- Server version	5.6.35

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `Brands`
--

DROP TABLE IF EXISTS `Brands`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `Brands` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(45) DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `id_UNIQUE` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `Brands`
--

LOCK TABLES `Brands` WRITE;
/*!40000 ALTER TABLE `Brands` DISABLE KEYS */;
INSERT INTO `Brands` VALUES (1,'maruti1','2017-05-02 12:45:53','2017-05-02 01:55:52'),(2,'ford','2017-05-02 01:56:30','2017-05-02 01:56:30'),(3,'tata','2017-05-02 06:48:56','2017-05-02 06:48:56');
/*!40000 ALTER TABLE `Brands` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `admin`
--

DROP TABLE IF EXISTS `admin`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `admin` (
  `id` int(6) NOT NULL AUTO_INCREMENT,
  `email` varchar(255) NOT NULL,
  `password` varchar(255) NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `admin`
--

LOCK TABLES `admin` WRITE;
/*!40000 ALTER TABLE `admin` DISABLE KEYS */;
INSERT INTO `admin` VALUES (1,'admin@lms.com','5f4dcc3b5aa765d61d8327deb882cf99','2017-04-24 00:00:00','2017-04-24 00:00:00');
/*!40000 ALTER TABLE `admin` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `car_master`
--

DROP TABLE IF EXISTS `car_master`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `car_master` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `updated_at` datetime DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `admin_id` int(6) DEFAULT NULL,
  `seller_type` varchar(255) DEFAULT NULL,
  `location` varchar(255) DEFAULT NULL,
  `rto_location` varchar(255) DEFAULT NULL,
  `dealer_id` int(6) DEFAULT NULL,
  `commision` bigint(20) DEFAULT NULL,
  `brand` int(6) DEFAULT NULL,
  `model_name` int(6) DEFAULT NULL,
  `price` float DEFAULT NULL,
  `manufacturing_year` varchar(255) DEFAULT NULL,
  `fuel_type` varchar(45) DEFAULT NULL,
  `reg_no` varchar(255) DEFAULT NULL,
  `kms_driven` float DEFAULT NULL,
  `colour` varchar(45) DEFAULT NULL,
  `power_steering` enum('1','0') DEFAULT NULL,
  `power_wndows` enum('1','0') DEFAULT NULL,
  `ac` enum('1','0') DEFAULT NULL,
  `fuel_capacity` float DEFAULT NULL,
  `refurbishment` enum('1','0') DEFAULT NULL,
  `reb_time` date DEFAULT NULL,
  `reb_cost` varchar(45) DEFAULT NULL,
  `status` enum('0','1','2') DEFAULT '0',
  `direct_sel` enum('1','0') DEFAULT NULL,
  `total_price` float DEFAULT NULL,
  `negotiation` varchar(255) DEFAULT NULL,
  `payment` varchar(255) DEFAULT NULL,
  `machanical_reb` date DEFAULT NULL,
  `painting_reb` date DEFAULT NULL,
  `finishing_reb` date DEFAULT NULL,
  `fuel_reb` date DEFAULT NULL,
  `mechanical_cost` bigint(20) DEFAULT NULL,
  `painting_cost` bigint(20) DEFAULT NULL,
  `finishing_cost` bigint(20) DEFAULT NULL,
  `fuel_cost` bigint(20) DEFAULT NULL,
  `payment_amount` bigint(20) DEFAULT NULL,
  `payment_date` date DEFAULT NULL,
  `month` date DEFAULT NULL,
  `owner` varchar(255) DEFAULT NULL,
  `insurance` date DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=10 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `car_master`
--

LOCK TABLES `car_master` WRITE;
/*!40000 ALTER TABLE `car_master` DISABLE KEYS */;
INSERT INTO `car_master` VALUES (1,'2017-05-05 04:24:40','2017-05-05 04:24:40',NULL,'customer','mumbai','delhi',NULL,25000,1,0,250000,'2015','Disel','25',2500,'black',NULL,NULL,NULL,800,'1',NULL,NULL,'0',NULL,856000,'sadad asd asd as','part','2017-12-22','2017-12-26','2017-05-26','2017-05-29',2500,2600,0,0,233232,NULL,NULL,NULL,NULL),(2,'2017-05-06 11:19:59','2017-05-06 11:19:59',1,'customer','mumbai','delhi',NULL,234234,2,0,3424230000,'2017','Petrol','120',1200,'red',NULL,NULL,NULL,850,'1',NULL,NULL,'0',NULL,NULL,'dsad sad as','part',NULL,NULL,NULL,NULL,234,3342,2342,34,432432234,'2017-05-19',NULL,NULL,NULL),(3,'2017-05-06 11:54:21','2017-05-06 11:54:21',1,NULL,'mumbai','delhi',NULL,NULL,2,4,NULL,'2016','Disel','250',120,'grey',NULL,NULL,NULL,1200,NULL,NULL,NULL,'0',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(4,'2017-05-06 11:56:47','2017-05-06 11:56:47',1,NULL,'mumbai','delhi',NULL,NULL,2,4,NULL,'2016','Disel','250',120,'grey',NULL,NULL,NULL,1200,NULL,NULL,NULL,'0',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(5,'2017-05-06 11:56:55','2017-05-06 11:56:55',1,NULL,'mumbai','delhi',NULL,NULL,2,4,NULL,'2016','Disel','250',120,'grey',NULL,NULL,NULL,1200,NULL,NULL,NULL,'0',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(6,'2017-05-06 11:58:17','2017-05-06 11:58:17',1,NULL,'mumbai','aaaa',NULL,NULL,2,4,NULL,'2014','Petrol','250',25000,'blue',NULL,NULL,NULL,520,NULL,NULL,NULL,'0',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(7,'2017-05-06 11:58:28','2017-05-06 11:58:28',1,NULL,'mumbai','aaaa',NULL,NULL,2,4,NULL,'2014','Petrol','250',25000,'blue',NULL,NULL,NULL,520,NULL,NULL,NULL,'0',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(8,'2017-05-06 12:00:30','2017-05-06 12:00:30',1,NULL,'mumbai','delhi',NULL,NULL,2,4,NULL,'2015','Disel','252',250,'black',NULL,NULL,NULL,2500,NULL,NULL,NULL,'0',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(9,'2017-05-06 12:04:10','2017-05-06 12:04:10',1,'customer','sad','sadad',NULL,32423,1,1,32424200000,'2015','Petrol','250',250,'sdsa',NULL,NULL,NULL,65464,'0',NULL,NULL,'0',NULL,NULL,'sa A ssadas asd','part',NULL,NULL,NULL,NULL,23423,324,23423,234234,0,'1970-01-01',NULL,NULL,NULL);
/*!40000 ALTER TABLE `car_master` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `car_models`
--

DROP TABLE IF EXISTS `car_models`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `car_models` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(45) DEFAULT NULL,
  `brand_id` int(6) DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `id_UNIQUE` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `car_models`
--

LOCK TABLES `car_models` WRITE;
/*!40000 ALTER TABLE `car_models` DISABLE KEYS */;
INSERT INTO `car_models` VALUES (1,'test1',1,'2017-05-02 02:00:12','2017-05-02 03:39:21'),(2,'test 3',1,'2017-05-02 02:01:53','2017-05-02 03:38:40'),(3,'test 3',1,'2017-05-02 03:35:49','2017-05-02 03:38:40'),(4,'test2',2,'2017-05-02 03:39:42','2017-05-02 03:39:42');
/*!40000 ALTER TABLE `car_models` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `car_refurbishment_mapping`
--

DROP TABLE IF EXISTS `car_refurbishment_mapping`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `car_refurbishment_mapping` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `car_id` int(6) DEFAULT NULL,
  `machanical_reb_start_date` date DEFAULT NULL,
  `machanical_reb_end_date` date DEFAULT NULL,
  `machanical_reb_actual_date` date DEFAULT NULL,
  `painting_reb_start_date` date DEFAULT NULL,
  `painting_reb_end_date` date DEFAULT NULL,
  `painting_reb_actual_date` date DEFAULT NULL,
  `finishing_reb_start_date` date DEFAULT NULL,
  `finishing_reb_end_date` date DEFAULT NULL,
  `finishing_reb_actual_date` date DEFAULT NULL,
  `Fuel` date DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `id_UNIQUE` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `car_refurbishment_mapping`
--

LOCK TABLES `car_refurbishment_mapping` WRITE;
/*!40000 ALTER TABLE `car_refurbishment_mapping` DISABLE KEYS */;
INSERT INTO `car_refurbishment_mapping` VALUES (2,0,'2017-05-24','2017-05-30','0000-00-00','2017-05-23','2017-05-31','0000-00-00','2017-05-30','2017-05-31','0000-00-00','2017-05-31','2017-05-05 04:41:42','2017-05-05 04:41:42'),(3,1,'2017-05-12','2017-05-15','0000-00-00','0000-00-00','0000-00-00','0000-00-00','0000-00-00','0000-00-00','0000-00-00','0000-00-00','2017-05-05 07:43:50','2017-05-05 07:43:50'),(4,1,'2017-05-12','2017-05-15','0000-00-00','2017-05-16','2017-05-17','0000-00-00','0000-00-00','0000-00-00','0000-00-00','0000-00-00','2017-05-05 07:48:58','2017-05-05 07:48:58'),(5,1,'2017-05-12','2017-05-15','1970-01-01','2017-05-26','0000-00-00','0000-00-00','0000-00-00','0000-00-00','0000-00-00','0000-00-00','2017-05-05 07:49:13','2017-05-05 07:49:13'),(6,1,'2017-05-12','2017-05-15','2017-05-10','0000-00-00','0000-00-00','0000-00-00','0000-00-00','0000-00-00','0000-00-00','0000-00-00','2017-05-05 07:53:05','2017-05-05 07:53:05');
/*!40000 ALTER TABLE `car_refurbishment_mapping` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `customer_car_mapping`
--

DROP TABLE IF EXISTS `customer_car_mapping`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `customer_car_mapping` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `customer_id` int(6) DEFAULT NULL,
  `car_id` int(6) DEFAULT NULL,
  `type` enum('1','0') DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `id_UNIQUE` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `customer_car_mapping`
--

LOCK TABLES `customer_car_mapping` WRITE;
/*!40000 ALTER TABLE `customer_car_mapping` DISABLE KEYS */;
INSERT INTO `customer_car_mapping` VALUES (2,4,4,'0','2017-05-05 04:55:11',NULL),(3,43,0,'0','2017-05-06 10:57:04',NULL),(5,44,2,'0','2017-05-06 11:20:01',NULL),(7,45,0,'0','2017-05-06 11:54:58',NULL),(8,46,9,'0','2017-05-06 12:04:58',NULL);
/*!40000 ALTER TABLE `customer_car_mapping` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `customer_master`
--

DROP TABLE IF EXISTS `customer_master`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `customer_master` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `admin_id` int(15) DEFAULT NULL,
  `first_name` varchar(45) DEFAULT NULL,
  `last_name` varchar(45) DEFAULT NULL,
  `email` varchar(45) DEFAULT NULL,
  `Address` varchar(45) DEFAULT NULL,
  `adhar_card` varchar(45) DEFAULT NULL,
  `pan_card` varchar(45) DEFAULT NULL,
  `identity_card` varchar(45) DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  `car_type` enum('1','0') DEFAULT NULL,
  `car_id` int(6) DEFAULT NULL,
  `mob_no` bigint(20) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=50 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `customer_master`
--

LOCK TABLES `customer_master` WRITE;
/*!40000 ALTER TABLE `customer_master` DISABLE KEYS */;
INSERT INTO `customer_master` VALUES (1,NULL,'abc','bcd','abc@gmail.com','sad,sadsa,dsa',NULL,NULL,NULL,'2017-05-05 11:22:52','2017-05-05 04:40:17',NULL,NULL,9956231245),(2,NULL,'','','','',NULL,NULL,NULL,'2017-05-05 04:43:22','2017-05-05 04:43:22',NULL,NULL,0),(3,NULL,'dsad','asd','sad@wsds.com','sad',NULL,NULL,NULL,'2017-05-05 04:43:35','2017-05-05 04:43:35',NULL,NULL,0),(4,NULL,'drtu','ruuy','ytutyu','sds sd as',NULL,NULL,NULL,'2017-05-05 04:45:36','2017-05-05 04:55:11',NULL,NULL,1416544646),(5,NULL,'fds','sdfsd','sdf@gmail.com','asdf',NULL,NULL,NULL,'2017-05-05 05:57:33','2017-05-05 05:57:33',NULL,NULL,NULL),(6,NULL,'fds','sdfsd','sdf@gmail.com','asdf',NULL,NULL,NULL,'2017-05-05 05:57:48','2017-05-05 05:57:48',NULL,NULL,NULL),(7,NULL,'dsaf','sdf','sdf@dfd.cdsaf','dfsdf',NULL,NULL,NULL,'2017-05-05 06:08:47','2017-05-05 06:08:47',NULL,NULL,NULL),(8,NULL,'sdf','sdf','sdf@dsfd.dfd','sdf',NULL,NULL,NULL,'2017-05-05 06:09:45','2017-05-05 06:09:45',NULL,NULL,NULL),(9,NULL,'dsfsd','fsdf','sd@daasd.com','dsfs',NULL,NULL,NULL,'2017-05-05 06:12:08','2017-05-05 06:12:08',NULL,NULL,NULL),(10,NULL,'efrewr','werwe','rew@sdf.com','sadas',NULL,NULL,NULL,'2017-05-05 06:13:25','2017-05-05 06:13:25',NULL,NULL,NULL),(11,NULL,'fsdf','sdf','dsf@gmail.com','dfsdfds',NULL,NULL,NULL,'2017-05-05 06:14:29','2017-05-05 06:14:29',NULL,NULL,NULL),(12,NULL,'sdasd','sadsa','d@gfg.com','fdasfdsfsd',NULL,NULL,NULL,'2017-05-05 06:22:38','2017-05-05 06:22:38',NULL,NULL,NULL),(13,NULL,'sdasd','sadsa','d@gfg.com','fdasfdsfsd',NULL,NULL,NULL,'2017-05-05 06:22:51','2017-05-05 06:22:51',NULL,NULL,NULL),(14,NULL,'sdasd','sadsa','d@gfg.com','fdasfdsfsd',NULL,NULL,NULL,'2017-05-05 06:22:57','2017-05-05 06:22:57',NULL,NULL,NULL),(15,NULL,'sdasd','sadsa','d@gfg.com','fdasfdsfsd',NULL,NULL,NULL,'2017-05-05 06:23:12','2017-05-05 06:23:12',NULL,NULL,NULL),(16,NULL,'sdasd','sadsa','d@gfg.com','fdasfdsfsd',NULL,NULL,NULL,'2017-05-05 06:23:13','2017-05-05 06:23:13',NULL,NULL,NULL),(17,NULL,'gfdg','fdg','fdg@fdsfsd.comsa','sdasd',NULL,NULL,NULL,'2017-05-05 06:24:59','2017-05-05 06:24:59',NULL,NULL,NULL),(18,NULL,'gfdg','fdg','fdg@fdsfsd.comsa','sdasd',NULL,NULL,NULL,'2017-05-05 06:25:01','2017-05-05 06:25:01',NULL,NULL,NULL),(19,NULL,'gfdg','fdg','fdg@fdsfsd.comsa','sdasd',NULL,NULL,NULL,'2017-05-05 06:25:02','2017-05-05 06:25:02',NULL,NULL,NULL),(20,NULL,'gfdg','fdg','fdg@fdsfsd.comsa','sdasd',NULL,NULL,NULL,'2017-05-05 06:25:03','2017-05-05 06:25:03',NULL,NULL,NULL),(21,NULL,'gfdg','fdg','fdg@fdsfsd.comsa','sdasd',NULL,NULL,NULL,'2017-05-05 06:25:04','2017-05-05 06:25:04',NULL,NULL,NULL),(22,NULL,'gfdg','fdg','fdg@fdsfsd.comsa','sdasd',NULL,NULL,NULL,'2017-05-05 06:25:04','2017-05-05 06:25:04',NULL,NULL,NULL),(23,NULL,'gfdg','fdg','fdg@fdsfsd.comsa','sdasd',NULL,NULL,NULL,'2017-05-05 06:25:05','2017-05-05 06:25:05',NULL,NULL,NULL),(24,NULL,'gfdg','fdg','fdg@fdsfsd.comsa','sdasd',NULL,NULL,NULL,'2017-05-05 06:25:05','2017-05-05 06:25:05',NULL,NULL,NULL),(25,NULL,'gfdg','fdg','fdg@fdsfsd.comsa','sdasd',NULL,NULL,NULL,'2017-05-05 06:25:06','2017-05-05 06:25:06',NULL,NULL,NULL),(26,NULL,'gfdg','fdg','fdg@fdsfsd.comsa','sdasd',NULL,NULL,NULL,'2017-05-05 06:25:06','2017-05-05 06:25:06',NULL,NULL,NULL),(27,NULL,'gfdg','fdg','fdg@fdsfsd.comsa','sdasd',NULL,NULL,NULL,'2017-05-05 06:25:06','2017-05-05 06:25:06',NULL,NULL,NULL),(28,NULL,'gfdg','fdg','fdg@fdsfsd.comsa','sdasd',NULL,NULL,NULL,'2017-05-05 06:25:06','2017-05-05 06:25:06',NULL,NULL,NULL),(29,NULL,'dfgdfg','dfgdfg','f@fdf.fdfs','dfdsf',NULL,NULL,NULL,'2017-05-05 06:39:49','2017-05-05 06:39:49',NULL,NULL,NULL),(30,NULL,'fdgdfg','fdg','fdgdf@sds.com','gfgdf',NULL,NULL,NULL,'2017-05-05 06:57:52','2017-05-05 06:57:52',NULL,NULL,NULL),(31,NULL,'fdgdfg','fdg','fdgdf@sds.com','gfgdf',NULL,NULL,NULL,'2017-05-05 06:59:43','2017-05-05 06:59:43',NULL,NULL,NULL),(32,NULL,'fdgdfg','fdg','fdgdf@sds.com','gfgdf',NULL,NULL,NULL,'2017-05-05 06:59:48','2017-05-05 06:59:48',NULL,NULL,NULL),(33,NULL,'fdgdfg','fdg','fdgdf@sds.com','gfgdf',NULL,NULL,NULL,'2017-05-05 07:00:02','2017-05-05 07:00:02',NULL,NULL,NULL),(34,NULL,'fdgdfg','fdg','fdgdf@sds.com','gfgdf',NULL,NULL,NULL,'2017-05-05 07:00:08','2017-05-05 07:00:08',NULL,NULL,NULL),(35,NULL,'fdgdfg','fdg','fdgdf@sds.com','gfgdf',NULL,NULL,NULL,'2017-05-05 07:00:19','2017-05-05 07:00:19',NULL,NULL,NULL),(36,NULL,'fdgdfg','fdg','fdgdf@sds.com','gfgdf',NULL,NULL,NULL,'2017-05-05 07:01:11','2017-05-05 07:01:11',NULL,NULL,NULL),(37,NULL,'fdgdfg','fdg','fdgdf@sds.com','gfgdf',NULL,NULL,NULL,'2017-05-05 07:02:12','2017-05-05 07:02:12',NULL,NULL,NULL),(38,NULL,'fdgdfg','fdg','fdgdf@sds.com','gfgdf',NULL,NULL,NULL,'2017-05-05 07:02:35','2017-05-05 07:02:35',NULL,NULL,NULL),(39,1,'fdsf','sdfsf','sdfdsfW@dsfds.com','fdsfsd',NULL,NULL,NULL,'2017-05-06 10:37:35','2017-05-06 10:37:35',NULL,NULL,NULL),(40,1,'fdsf','sdfsf','sdfdsfW@dsfds.com','fdsfsd',NULL,NULL,NULL,'2017-05-06 10:37:40','2017-05-06 10:37:40',NULL,NULL,NULL),(41,1,'fdsf','sdfsf','sdfdsfW@dsfds.com','fdsfsd',NULL,NULL,NULL,'2017-05-06 10:37:41','2017-05-06 10:37:41',NULL,NULL,NULL),(42,1,'fdsf','sdfsf','sdfdsfW@dsfds.com','fdsfsd',NULL,NULL,NULL,'2017-05-06 10:37:41','2017-05-06 10:37:41',NULL,NULL,NULL),(43,1,'vithal','dalvi1','dalvi1@gmailc.com','test customer,test -451215',NULL,NULL,NULL,'2017-05-06 10:57:04','2017-05-06 10:57:04',NULL,NULL,986523145),(44,1,'sdad','sad','sad@dsads.com','sadsa sd sa',NULL,NULL,NULL,'2017-05-06 11:19:14','2017-05-06 11:20:01',NULL,NULL,98989898),(45,1,'sas','sa','as@sdadas.sad','sadsa,sadsad,,asd',NULL,NULL,NULL,'2017-05-06 11:54:47','2017-05-06 11:54:57',NULL,NULL,213123212),(46,1,'aSDSADs','ASADS','dsda@dsdfs.com','sadasd sadas ',NULL,NULL,NULL,'2017-05-06 12:04:58','2017-05-06 12:04:58',NULL,NULL,123123123),(47,NULL,'dsfdsf','sdfsd','sda@sda.com','sdasdsad',NULL,NULL,NULL,'2017-05-06 06:15:23','2017-05-06 06:15:23',NULL,NULL,NULL),(48,NULL,'dsfdsf','sdfsd','sda@sda.com','sdasdsad',NULL,NULL,NULL,'2017-05-06 06:15:24','2017-05-06 06:15:24',NULL,NULL,NULL),(49,NULL,'sadasd','sad','sad@fdfds.com','wewqewq',NULL,NULL,NULL,'2017-05-06 06:39:41','2017-05-06 06:39:41',NULL,NULL,NULL);
/*!40000 ALTER TABLE `customer_master` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `dealer_master`
--

DROP TABLE IF EXISTS `dealer_master`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `dealer_master` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `first_name` varchar(45) DEFAULT NULL,
  `last_name` varchar(45) DEFAULT NULL,
  `email` varchar(255) DEFAULT NULL,
  `Address` varchar(45) DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  `admin_id` int(6) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `email_UNIQUE` (`email`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `dealer_master`
--

LOCK TABLES `dealer_master` WRITE;
/*!40000 ALTER TABLE `dealer_master` DISABLE KEYS */;
INSERT INTO `dealer_master` VALUES (2,'test','dealer','ds@fdf.com','sdsad','2017-04-27 03:50:21','2017-04-27 04:08:57',1),(3,'dealer','test2','test2@gmail.com','sadas sd asd asd','2017-04-27 04:33:20','2017-04-27 04:33:20',1);
/*!40000 ALTER TABLE `dealer_master` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2017-05-06 18:54:22
