ALTER TABLE  `karigar_master` CHANGE  `code`  `code` VARCHAR( 45 ) CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL ;

ALTER TABLE  `karigar_master` ADD  `wastage` FLOAT( 10, 2 ) NOT NULL ,
ADD  `kund_rate` FLOAT( 10, 2 ) NULL ;


ALTER TABLE  `department_ready_product` ADD  `custom_kun_wt` FLOAT( 10, 2 ) NOT NULL ,
ADD  `custom_black_beads_wt` FLOAT( 10, 2 ) NOT NULL ;

ALTER TABLE  `admin` ADD  `location_id` INT( 11 ) NOT NULL AFTER  `department_id` ;

mysql -h dbinstanonymous.cy9evftloyct.ap-southeast-1.rds.amazonaws.com -u dbmstanonymous -p pwdmstanonymous

    'hostname' => 'dbinstanonymous.cy9evftloyct.ap-southeast-1.rds.amazonaws.com',
    'username' => 'dbmstanonymous',
    'password' => 'pwdmstanonymous',
    'database' => 'sejal_antique_manufacturing',

DELETE FROM `sejal_antique_manufacturing`.`controllers` WHERE `controllers`.`id` = 6 LIMIT 1;;
DELETE FROM `sejal_antique_manufacturing`.`controllers` WHERE `controllers`.`id` = 22 LIMIT 1;;
DELETE FROM `sejal_antique_manufacturing`.`controllers` WHERE `controllers`.`id` = 9 LIMIT 1;;
DELETE FROM `sejal_antique_manufacturing`.`controllers` WHERE `controllers`.`id` = 88 LIMIT 1;;
DELETE FROM `sejal_antique_manufacturing`.`controllers` WHERE `controllers`.`id` = 41 LIMIT 1;;
DELETE FROM `sejal_antique_manufacturing`.`controllers` WHERE `controllers`.`id` = 102 LIMIT 1;;
DELETE FROM `sejal_antique_manufacturing`.`controllers` WHERE `controllers`.`id` = 8 LIMIT 1;;
DELETE FROM `sejal_antique_manufacturing`.`controllers` WHERE `controllers`.`id` = 29 LIMIT 1;;
DELETE FROM `sejal_antique_manufacturing`.`controllers` WHERE `controllers`.`id` = 3 LIMIT 1;;
DELETE FROM `sejal_antique_manufacturing`.`controllers` WHERE `controllers`.`id` = 107 LIMIT 1;;
DELETE FROM `sejal_antique_manufacturing`.`controllers` WHERE `controllers`.`id` = 7 LIMIT 1;;
DELETE FROM `sejal_antique_manufacturing`.`controllers` WHERE `controllers`.`id` = 118 LIMIT 1;;
DELETE FROM `sejal_antique_manufacturing`.`controllers` WHERE `controllers`.`id` = 103 LIMIT 1;;
DELETE FROM `sejal_antique_manufacturing`.`controllers` WHERE `controllers`.`id` = 85 LIMIT 1;;
DELETE FROM `sejal_antique_manufacturing`.`controllers` WHERE `controllers`.`id` = 69 LIMIT 1;;
DELETE FROM `sejal_antique_manufacturing`.`controllers` WHERE `controllers`.`id` = 87 LIMIT 1;;
DELETE FROM `sejal_antique_manufacturing`.`controllers` WHERE `controllers`.`id` = 21 LIMIT 1;;
DELETE FROM `sejal_antique_manufacturing`.`controllers` WHERE `controllers`.`id` = 68 LIMIT 1;;
DELETE FROM `sejal_antique_manufacturing`.`controllers` WHERE `controllers`.`id` = 98 LIMIT 1;;
DELETE FROM `sejal_antique_manufacturing`.`controllers` WHERE `controllers`.`id` = 86 LIMIT 1;;


ALTER TABLE  `quality_control` ADD  `black_beads_wt` FLOAT( 10, 2 ) NOT NULL DEFAULT  '0.00';
ALTER TABLE  `Receive_products` ADD  `black_beads_wt` FLOAT( 10, 2 ) NOT NULL DEFAULT  '0.00';


CREATE TABLE IF NOT EXISTS `product_category_rate` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) DEFAULT NULL,
  `percentage` float(10,2) DEFAULT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=21 ;

INSERT INTO `controllers` (`id`, `name`, `methods`, `updated_at`, `created_at`, `display_name`, `master_id`, `sequence`, `status`) VALUES (NULL, 'Product_category_rate', 'Product_category_rate', '', '', 'Product category', '2', '97', '1');

ALTER TABLE  `parent_category` ADD  `app_order` INT NOT NULL DEFAULT  '0';
ALTER TABLE `parent_category`  ADD `kundan_category` VARCHAR(255) NOT NULL,  ADD `stone_category` VARCHAR(255) NOT NULL,  ADD `black_beads_category` VARCHAR(255) NOT NULL;
ALTER TABLE  `department_ready_product` ADD  `custom_total_wt` VARCHAR( 255 ) NOT NULL ;


UPDATE `controllers` SET  `name` =  'product_category_rate',`methods` =  'product_category_rate'  WHERE  `controllers`.`id` =135;