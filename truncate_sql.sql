


manufacturing:-
truncate manufacturing_order;
truncate manufacturing_order_mapping;
truncate Karigar_manufacturing_mapping;
truncate make_set;
truncate manufacturing_order_variant_mapping;
truncate department_ready_product;
truncate tag_products;
truncate prepare_kundan_karigar_order;
SET SQL_SAFE_UPDATES = 0;
delete from Prepare_product_list where module='2';
delete from Receive_products where module='2';
delete from quality_control where module='2';
delete from quality_control_hallmarking where module='2';
truncate repair_product;
delete from quality_control where module='3';
delete from Receive_products where module='3';
delete from make_set_quantity_relation where status='1';
truncate sale_stock;
truncate product_code_max_count;