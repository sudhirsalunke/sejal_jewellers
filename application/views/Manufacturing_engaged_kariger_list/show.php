<div class="content-page">
<div class="content">
   <div class="container">
      <div class="row">
         <div class="col-sm-12 card-box-wrap">
            <div class="panel panel-default">
              <div class="panel-heading">
               <h4 class="inlineBlock"><span><?= @$display_title?></span></h4>                
              </div>
              <div class="panel-body">
                <div class="table-rep-plugin">
                  <div class="table-responsive b-0 scrollhidden">
                    <?php 
                    if ($dep_id != '2'  && $dep_id != '3' && $dep_id != '6' && $dep_id != '10') {
                      $table_name='manufacturing_details_of_engaged_karigar_list';
                    } else {
                      $table_name='mfg_details_eng_karigar_bom_list';
                    }
                    
                  ?>
               <table class="table table-bordered custdatatable" id="<?php echo $table_name;?>">
                  <thead>
                  <?php
                          $data['heading']=$table_name;
                          $this->load->view('master/table_header',$data);
                        ?>
             <!--         <tr>
                         <th class="col4">Karigar Name</th>
                         <th class="col4">Parent Category</th>
                         <th class="col4">Order Id</th>
                         <th>Karigar Engaged Date</th>
                         <th>Proposed delivery Date</th>
                         <th class="col4">Quantity</th>
                         <th class="col4">Total Weight</th>
                         <th></th>
                     </tr> -->
                  </thead>
             <!--      <tbody>
                    <?php foreach ($result as $key => $value) {
                    
                    $timestamp_date = strtotime($value['created_at']);
                     ?>
                    <tr>
                        <td><?=$value['k_name'];?></td>
                        <td><?=$value['name'];?></td>
                         <td><?=$value['id'];?></td>
                        <td><?=date('d-m-Y',$timestamp_date);?></td>
                        <td><?=date('d-m-Y',strtotime("+12 day",$timestamp_date))?></td>
                        <td><?=$value['quantity'];?></td>
                        <td><?=$value['approx_weight'];?></td>
                        <td><a target="_blank" href="<?= ADMIN_PATH ?>Karigar_order_list/re_print_order/<?= $value['kmm_id']?>"><button class="btn btn-primary small loader-hide  btn-sm" name="commit" type="button" >Print</button></a></td>
                    </tr>
                  <?php } ?>
                  </tbody> -->
               </table>
               </div>
               </div>
            </div>
         </div>
      </div>
   </div>
</div>