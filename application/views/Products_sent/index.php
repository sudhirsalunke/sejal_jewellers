<div class="content-page">
  <div class="content">
    <div class="container">
      <div class="row">
        <div class="col-sm-12 card-box-wrap">
          <div class="card-box padmobile">
            <div class="panel panel-default">
              <div class="panel-heading">
                <h4 class="inlineBlock">Sent To Karigar</h4>
              </div>
              <div class="clearfix"></div>
              <div class="panel-body">
                <div class="table-rep-plugin">
                  <div class="table-responsive b-0 scrollhidden">              
                    <table class="table table-bordered custdatatable" id="Products_sent">
                      <thead>
                        <?php
                                $data['heading']='Products_sent';
                            $this->load->view('master/table_header',$data)?>
                          <!--  <tr>
                               <th class="col4">Order Id</th>
                               <th class="col4">Order Date</th>
                               <th class="col4">Delivery Date</th>
                               <th class="col4">Corporate</th>
                               <th class="col4">Assign Products</th>
                              <th class="col3"></th>
                           </tr> -->
                      </thead>
                    </table>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>