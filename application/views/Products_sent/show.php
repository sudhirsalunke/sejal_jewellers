<div class="content-page">
  <div class="content">
     <div class="container">
        <div class="row">
          <div class="col-sm-12 card-box-wrap">
            <form action="<?=ADMIN_PATH.'Products_sent/export'?>" method="post" name="product_reprint" id="product_reprint">
              <div class="card-box padmobile">
                <div class="panel panel-default">
                  <div class="panel-heading">                  
                    <h4 class="inlineBlock"><span>Sent Design</span></h4>
                    <div class="pull-right btn-wrap2">
                      <input type="hidden" name="search" id="search_textbox">
                      <input type="hidden" name="order_id" value="<?=$order_id;?>"> 
                      <button type="submit" name="compact_reprint" class="add_senior_manager_button btn  btn-info waves-effect w-md waves-light btn-md">Normal Reprint</button>                     
                      <button type="submit" name="normal_reprint" class="add_senior_manager_button btn  btn-info waves-effect w-md waves-light btn-md">compact Reprint</button>                      
                      <button type="submit" name="export" class="add_senior_manager_button btn btn-warning waves-effect w-md waves-light btn-md">EXPORT</button>
                    </div>
                  </div>               
                  <div class="clearfix"></div>
                  <div class="panel-body">
                    <div class="table-rep-plugin">             
                      <span>ORDER ID :- <?=$order_id;?></span>                   
                      <div class="clearfix"></div>
                    <div class="table b-0 scrollhidden table-responsive">
                      <table class="table custdatatable" id="Products_sent_by_order">
                        <thead>
                          <?php
                                  $data['heading']='Products_sent_by_order';
                              $this->load->view('master/table_header',$data)?>
                               <!--     <tr>
                                 <th class="col4">Product Id</th>
                                 <th class="col4">Karigar Name</th>
                                 <th class="col4">Proposed Delivery Date</th>
                                 <th class="col4">Quantity</th>
                                 <th class="col4">Image</th>
                                <th class="col3"></th>
                             </tr> -->
                        </thead>
                      </table>
                    </div>                               
                  </div>
                </div>
              </div>              
            </div>
          </form>
        </div>
      </div>
    </div>
  </div>
</div>

