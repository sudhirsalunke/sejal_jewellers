<div class="content-page">
<div class="content">
   <div class="container">
      <div class="row">

         <div class="col-sm-12 col-xs-12 card-box-wrap ">
            <div class="card-box padmobile clearfix">
           
            <div class="panel-heading frmHeading">
               <h4 class="inlineBlock"><?=$page_title;?>
               </h4>
               
               </div>
				<div class="col-md-10 col-xs-12 m-t-15">
               		<div class="row ">
	               		<div class="col-sm-5">
	               			<div class="form-group">
			       				<label class="col-sm-5 control-label " for="inputEmail3">Karigar Name: </label>
			       				<div class="col-sm-5">
			       					<label class="control-label" for="inputEmail3"><?= $receive_order[0]['km_name'];?></label>
			       				</div>
		       				</div>
		       			</div>
	               		<div class="col-sm-5">
	               			<div class="form-group">
			       				<label class="col-sm-5 control-label " for="inputEmail3">Receipt Code: </label>
			       				<div class="col-sm-5">
			       					<label class="control-label" for="inputEmail3"><?= $receive_order[0]['receipt_code'];?></label>
			       				</div>
		       				</div>
		       			</div>
	               		<div class="col-sm-5">
	               			<div class="form-group">
			       				<label class="col-sm-5 control-label " for="inputEmail3">Received Date: </label>
			       				<div class="col-sm-5">
			       					<label class="control-label" for="inputEmail3"><?=date('d-m-Y',strtotime($receive_order[0]['date']));?></label>
			       				</div>
		       				</div>
		       			</div>
		            </div>
               	</div>
               	<div class="col-xs-12 m-t-15">
               <form name="category" id="qc_check" role="form" class="form-horizontal" method="post">
               <input type="hidden" name="department_id" id="department_id" value="<?=$department_id;?>">
                <input type="hidden" name="karigar_id" id="karigar_id" value="<?=$receive_order[0]['karigar_id'];?>">
                  <input type="hidden" name="receipt_code" id="receipt_code" value="<?=$receive_order[0]['receipt_code'];?>">
               	<div class="clo-sm-12" style="overflow-x: scroll;">
			          <table class="table table-bordered" id="Receive_pieces">
			            <thead>
			             <tr>
			               <th class="col4">Select
				                <div class="checkbox checkbox-purpal">
				                    <input type="checkbox" id="check_selectall_product_qc" data-parsley-multiple="groups" data-parsley-mincheck="2" data-parsley-id="69" class="product_qc" value="148">
				                    <label for="check_selectall_qc"></label>
				                </div>
			               </th>
			               <th class="col4">Order Name</th>
			               <th class="col4">Product</th>
			                <?php if($department_id == '2' || $department_id=='3' || $department_id =='6' || $department_id=='10'): ?>
			               <th class="col4">Weight</th>
			               <?php else: ?>
			               	  <th class="col4">Quantity</th>
			               <?php endif;?>
			               <th class="col4">Net WT </th>
			               <th class="col4">Weight Range</th>
			               <?php if($department_id == '0'  || $department_id == '1'
           					||  $department_id == '5'  ||  $department_id == '7'  ||  $department_id == '8'  ||  $department_id == '11'){?>
			               <th class="col4">Is HM ?  
			               <div class="checkbox checkbox-purpal">
		                   <input type="checkbox" id="check_selectall_product_hm" data-parsley-multiple="groups" data-parsley-mincheck="2" data-parsley-id="69" class="product_hm" value="149" >
		                   <label for="check_selectall_product_hm"></label>
		                   </div>
			               </th>
			               <th class="col4">HM Net Wt</th>
			               <th class="col4">HM Quantity</th>
			               <?php }?>
			             </tr>
			           </thead>
			           <tbody>
			            <?php
			            //print_r($receive_order);
			            if(!empty($receive_order)){
			              foreach ($receive_order as $key => $value) {
			                ?>
			                <tr>
			                  <td>
			                  <div class="checkbox checkbox-purpal qc_checked">
			                    <input type="checkbox" data-parsley-multiple="groups" data-parsley-mincheck="2" data-parsley-id="69" name="qc[receive_product_id][]" value="<?=$value['id']?>" class="from-control">
			                    <label for="order_id"></label>
			                  </div>
			                  </td>
			                  <td><?=$value['order_name']?></td>
			                  <td><input type="hidden" class="form-control" name="qc[product_code][<?=$value['id']?>]" value="<?=$value['name'];?>"><?=$value['name']?>
			                  </td>
			                  <td>
			                  <?php if($department_id != '2' && $department_id!='3' && $department_id !='6' && $department_id!='10'): ?>
			                <input type="hidden" value="<?=(($value['from_weight'] + $value['to_weight'])/2)?>" id="appximate_weight_<?= $key?>">
			                  <input type="text" class="form-control" name="qc[quantity][<?=$value['id']?>]" value="<?=($value['rp_qnt'] - $value['qc_quantity'])?>"><p id="quantity_<?= $value['id']?>_error" class="text-danger"></p>
           					   <td>
			                  <input type="text" class="form-control" name="qc[weight][<?=$value['id']?>]" value="<?=$value['net_wt']?>"><p class="text-danger" id="weight_<?= $value['id']?>_error"></p>
			                  </td>
			                  
			                  <?php else: ?>
			                  	<input type="hidden" value="<?= $value['karigar_id'];?>"  name="qc[karigar_id][<?=$value['id']?>]">
			               	    <input type="text" class="form-control" name="qc[bom_weight][<?=$value['id']?>]" value="<?=($value['rp_wt'] - $value['qc_weight'])?>"><p id="bom_weight_<?= $value['id']?>_error" class="text-danger"></p>
			               
			                    <td><input type="text"  class="form-control" name="qc[bom_nt_wt][<?=$value['id']?>]" value="<?= $value['net_wt'];?>"><p class="text-danger" id="bom_nt_wt_<?= $value['id']?>_error"></p></td>
			               	   <?php endif;?>	
			               	</td>
			                  <td>
			                  <span class="pull-right"><?=$value['from_weight'].'-'.$value['to_weight']?></span>
			                  </td>
			                  <!-- <td><input type="text" class="form-control" name="qc[weight][<?=$value['id']?>]" value="<?=(($value['from_weight'] + $value['to_weight'])/2)*$value['quantity']?>"><p class="text-danger" id="weight_<?= $value['id']?>_error"></p></td> -->

			                  </td>
			                 <?php  if($department_id == '0'  || $department_id == '1'
           							||  $department_id == '5'  ||  $department_id == '7'  ||  
           							$department_id == '8'  ||  $department_id == '11') {
           					  ?>
			                  <td>
			                  		<div class="checkbox checkbox-purpal product_hm">
					        			<input data-parsley-multiple="group1" data-parsley-id="76" type="checkbox" id="hallmarking_checked" name="qc[hallmarking][<?=$value['id']?>]" value="1" class="from-control">
					        			<label for="hallmarking"></label>
					        		</div>	
					        </td>
					        	<td><input type="text"  class="form-control" name="qc[net_wt][<?=$value['id']?>]"><p class="text-danger" id="net_wt_<?= $value['id']?>_error"></p></td>

					        	<td><input type="text"  class="form-control" name="qc[hallmarking_quantity][<?=$value['id']?>]"><p class="text-danger" id="hallmarking_quantity_<?= $value['id']?>_error"></p></td>
					        	   <?php }?>
			                </tr>
			                <?php
			              }
			            }else{ ?>
			                <tr>
			                    <td colspan="6">No Orders Found</td>
			                </tr>
			           <?php } ?>
			          </tbody>
			        </table>
			      </div>
               		<!-- <div class="col-md-10 col-xs-12 m-t-15">
	               		<div class="row ">
		               		<div class="col-sm-5">
		               			<div class="form-group">
				       				<label class="col-sm-5 control-label " for="inputEmail3">Karigar Name </label>
				       				<div class="col-sm-5">
				       					<label class="control-label" for="inputEmail3"><?= $receive_order['name'];?></label>
				       				</div>
			       				</div>
			       			</div>
			       			<div class="col-sm-5">
			       				<div class="form-group">
				       				<label class="col-sm-5 control-label" for="inputEmail3">Receipt Code </label>
				       				<div class="col-sm-5">
				       					<label class="control-label" for="inputEmail3"><?= $receive_order['receipt_code'];?></label>
				       				</div>
			       				</div>
			       			</div>
			       			<div class="col-sm-5">
			       				<label class="col-sm-5 control-label" for="inputEmail3">Receipt Order Weight <span class="asterisk">*</span></label>
			       				<div class="col-sm-5">
			       					<div class="form-group">
				       					<input type="text" class="form-control" name="weight" value="<?= round($receive_order['weight'],2)?>">
						        			<label for="bom_checking"></label>
			       				<span class="text-danger" id="weight_error"></span>
			       					</div>
			       				</div>
			       			</div>
			       			<div class="col-sm-5">
		               			<div class="form-group">
				       				<label class="col-sm-5 control-label " for="inputEmail3">Quantity<span class="asterisk">*</span></label>
				       				<div class="col-sm-5">
				       					<div class="form-group">
				       						<input type="text" class="form-control" name="quantity" value="<?= $receive_order['quantity'] - $rp_quantity;?>">
						        			<label for="bom_checking"></label>
			       					<span class="text-danger" id="quantity_error"></span>
			       						</div>
				       				</div>
			       				</div>
			       			</div>
			            </div>
               		</div> -->
               		<!-- <div class="col-md-12 col-xs-12 mr10">
	                	<h4 class="header-title business_manager_header_title m-t-0 m-b-30 bmsmheader bmsmheaderabm"><span>Products </span>
               			</h4>
					</div> -->
					 <div class="col-md-10 col-xs-12">
					  <div class="table-rep-plugin">
					    <div class="table-responsive b-0 scrollhidden">
					      <table class="table table-bordered" id="sub_catagory">
					        <thead>
					           <tr>
					               <th class="col4">Sr NO</th>
					               <th class="col4">Parameter</th>
					               <th class="col4"><div class="checkbox checkbox-purpal">
			                    <input type="checkbox" id="check_all_qc" data-parsley-multiple="groups" data-parsley-mincheck="2" data-parsley-id="69" class="product check_all" value="147" checked="">
			                    <label for="check_all_qc"></label>
			                  </div></th>
					              <th class="col4">Remark</th>
					           </tr>
					        </thead>
					        <tbody>
					        	<tr>
					        		<td><span class="pull-right">1</span></td>
					        		<td>Ghat Weight</td>
					        		<td>
					        		<div class="checkbox checkbox-purpal">
					        			<input data-parsley-multiple="group1" data-parsley-id="76" type="checkbox" name="ghat_wt" value="1" class="from-control checked_checkboxses" checked>
					        			<label for="bom_checking"></label>
					        		</div>
					        		</td>
					        		<td><textarea name="ghat_wt_remark" class="txt-box"></textarea></td>
					        	</tr>
					        	<tr>
					        		<td><span class="pull-right">2</span></td>
					        		<td>DESIGN CHECKING</td>
					        		<td>
					        		<div class="checkbox checkbox-purpal">
					        			<input data-parsley-multiple="group1" data-parsley-id="76" type="checkbox" name="design_checking" value="1" class="from-control checked_checkboxses" checked>
					        			<label for="design_checking"></label>
					        		</div>
									</td>
					        		<td><textarea name="design_checking_remark" class="txt-box" ></textarea></td>
					        	</tr>
					        	<tr>
					        		<td><span class="pull-right">3</span></td>
					        		<td>PAIR MATCHING</td>
					        		<td>
					        		<div class="checkbox checkbox-purpal">
					        			<input data-parsley-multiple="group1" data-parsley-id="76" type="checkbox" name="pair_matching"  value="1" class="from-control checked_checkboxses" checked>
					        			<label for="pair_matching"></label>
					        		</div>
									</td>
					        		<td><textarea name="pair_matching_remark" class="txt-box" ></textarea></td>
					        	</tr>
					        	<tr>
					        		<td><span class="pull-right">4</span></td>
					        		<td>CARATAGE/PURITY</td>
					        		<td>
					        		<div class="checkbox checkbox-purpal">
					        			<input data-parsley-multiple="group1" data-parsley-id="76" type="checkbox" name="karatage/purity" value="1" class="from-control checked_checkboxses" checked>
					        			<label for="karatage/purity"></label>
					        		</div>
									</td>
					        		<td><textarea name="karatage/purity_remark" class="txt-box" ></textarea></td>
					        	</tr>
					        	<tr>
					        		<td><span class="pull-right">5</span></td>
					        		<td>SIZE</td>
					        		<td>
					        			<div class="checkbox checkbox-purpal">
					        			<input data-parsley-multiple="group1" data-parsley-id="76" type="checkbox" name="size" value="1" class="from-control checked_checkboxses" checked>
					        			<label for="size"></label>
					        		</div>
									</td>
					        		<td><textarea name="size_remark" class="txt-box"></textarea></td>
					        	</tr>
					        	<tr>
					        		<td><span class="pull-right">6</span></td>
					        		<td>STAMPING</td>
					        		<td>
					        			<div class="checkbox checkbox-purpal">
					        			<input data-parsley-multiple="group1" data-parsley-id="76" type="checkbox" name="stamping" value="1" class="from-control checked_checkboxses" checked>
					        			<label for="stamping"></label>
					        		</div>
									</td>
					        		<td><textarea name="stamping_remark" class="txt-box"></textarea></td>
					        	</tr>
					        	<tr>
					        		<td><span class="pull-right">7</span></td>
					        		<td>SHARP EDGE</td>
					        		<td>
					        			<div class="checkbox checkbox-purpal">
					        			<input data-parsley-multiple="group1" data-parsley-id="76" type="checkbox" name="sharp_edge" value="1" class="from-control checked_checkboxses" checked>
					        			<label for="sharp_edge"></label>
					        		</div>
									</td>
					        		<td><textarea name="sharp_edge_remark" class="txt-box" ></textarea></td>
					        	</tr>
					        	<tr>
					        		<td><span class="pull-right">8</span></td>
					        		<td>SOLDER/LINKING</td>
					        		<td>
					        			<div class="checkbox checkbox-purpal">
					        			<input data-parsley-multiple="group1" data-parsley-id="76" type="checkbox" name="solder/linking" value="1" class="from-control checked_checkboxses" checked>
					        			<label for="solder/linking"></label>
					        		</div>
									</td>
					        		<td><textarea name="solder/linking_remark" class="txt-box" ></textarea></td>
					        	</tr>
					        	<tr>
					        		<td><span class="pull-right">9</span></td>
					        		<td>SHAPE OUT</td>
					        		<td>
					        			<div class="checkbox checkbox-purpal">
					        			<input data-parsley-multiple="group1" data-parsley-id="76" type="checkbox" name="shape_out" value="1" class="from-control checked_checkboxses" checked>
					        			<label for="shape_out"></label>
					        		</div>
									</td>
					        		<td><textarea name="shape_out_remark" class="txt-box"></textarea></td>
					        	</tr>
					        	<tr>
					        		<td><span class="pull-right">10</span></td>
					        		<td>FINISHING</td>
					        		<td>
					        			<div class="checkbox checkbox-purpal">
					        			<input data-parsley-multiple="group1" data-parsley-id="76" type="checkbox" name="finishing" value="1" class="from-control checked_checkboxses" checked>
					        			<label for="finishing"></label>
					        		</div>

									</td>
					        		<td><textarea name="finishing_remark" class="txt-box"></textarea></td>
					        	</tr>
					        	<tr>
					        		<td><span class="pull-right">11</span></td>
					        		<td>GR WT/NET WT</td>
					        		<td>
					        			<div class="checkbox checkbox-purpal">
					        			<input data-parsley-multiple="group1" data-parsley-id="76" type="checkbox" name="gr_wt/nt_wt" value="1" class="from-control checked_checkboxses" checked>
					        			<label for="gr_wt/nt_wt"></label>
					        		</div>
									</td>
					        		<td><textarea name="gr_wt/nt_wt_remark" class="txt-box"></textarea></td>
					        	</tr>
					        	<tr>
					        		<td><span class="pull-right">12</span></td>
					        		<td>TAG DETAIL</td>
					        		<td>
					        			<div class="checkbox checkbox-purpal">
					        			<input data-parsley-multiple="group1" data-parsley-id="76" type="checkbox" name="tag_detail" value="1" class="from-control checked_checkboxses" checked>
					        			<label for="tag_detail"></label>
					        		</div>
									</td>
					        		<td><textarea name="tag_detail_remark" class="txt-box"></textarea></td>
					        	</tr>
					        	<!--<tr>
					        		<td>13</td>
					        		<td>FINDING/LOCK</td>
					        		<td>
					        			<div class="checkbox checkbox-purpal">
					        			<input data-parsley-multiple="group1" data-parsley-id="76" type="checkbox" name="finding/lock" value="1" class="from-control" <?=($result['finding/lock'] == true) ? 'checked' : ''?> <?=($receive_from_hallmarking == false) ? 'checked' : '' ?>>
					        			<label for="finding/lock"></label>
					        		</div>
									</td>
					        		<td><textarea name="finding/lock_remark" class="txt-box"></textarea></td>
					        	</tr>-->
					        	<tr>
					        		<td><span class="pull-right">13</span></td>
					        		<td>WEARING/TEST</td>
					        		<td>
					        			<div class="checkbox checkbox-purpal">
					        			<input data-parsley-multiple="group1" data-parsley-id="76" type="checkbox" name="wearing_test" value="1" class="from-control checked_checkboxses" checked>
					        			<label for="wearing_test"></label>
					        		</div>
									</td>
					        		<td><textarea name="wearing_test_remark" class="txt-box"></textarea></td>
					        	</tr><tr>
					        		<td><span class="pull-right">14</span></td>
					        		<td>ALIGNMENT</td>
					        		<td>
					        			<div class="checkbox checkbox-purpal">
					        			<input data-parsley-multiple="group1" data-parsley-id="76" type="checkbox" name="alignment" value="1" class="from-control checked_checkboxses" checked>
					        			<label for="alignment"></label>
					        		</div>
									</td>
					        		<td><textarea name="alignment_remark" class="txt-box"></textarea></td>
					        	</tr>
					        	<tr>
					        		<td><span class="pull-right">15</span></td>
					        		<td>PACKING</td>
					        		<td>
					        			<div class="checkbox checkbox-purpal">
					        			<input data-parsley-multiple="group1" data-parsley-id="76" type="checkbox" name="packing" value="1" class="from-control txt-box checked_checkboxses" checked>
					        			<label for="packing"></label>
					        		</div>

									</td>
					        		<td><textarea name="packing_remark" class="txt-box"></textarea></td>
					        	</tr>
					        	<!--<tr>
					        		<td>17</td>
					        		<td>Weight Check</td>
					        		<td>
					        			<div class="checkbox checkbox-purpal">
					        			<input data-parsley-multiple="group1" data-parsley-id="76" type="checkbox" name="weight_check" value="1" class="from-control txt-box" <?=($result['weight_check'] == true) ? 'checked' : ''?> <?=($receive_from_hallmarking == false) ? 'checked' : '' ?>>
					        			<label for="weight_check"></label>
					        		</div>

									</td>
					        		<td><textarea name="weight_check_remark" class="txt-box"></textarea></td>
					        	</tr>-->
					        	<tr>
					        		<td><span class="pull-right">16</span></td>
					        		<td>Meena</td>
					        		<td>
					        			<div class="checkbox checkbox-purpal">
					        			<input data-parsley-multiple="group1" data-parsley-id="76" type="checkbox" name="Meena" value="1" class="from-control txt-box checked_checkboxses" checked>
					        			<label for="packing"></label>
					        		</div>

									</td>
					        		<td><textarea name="Meena_remark" class="txt-box"></textarea></td>
					        	</tr>
					        	<!-- <tr>
					        		<td>18</td>
					        		<td>HALLMARKING</td>
					        		<td>
					        			<div class="checkbox checkbox-purpal">
					        			<input data-parsley-multiple="group1" data-parsley-id="76" type="checkbox" id="hallmarking_checked" name="hallmarking" value="1" class="from-control txt-box" checked>
					        			<label for="hallmarking"></label>
					        		</div>

									</td>
					        		<td><textarea name="hallmarking_remark" class="from-control"><?=$result['hallmarking_remark'] ?></textarea></td>

					        	</tr> -->
								<?php if($department_id == '0'  || $department_id == '1'  ||  $department_id == '5'  ||  $department_id == '7'  ||  $department_id == '8'  ||  $department_id == '11'){?>
					        	<tr>
					        		<td></td>
					        		<td></td>
					        		<td> Hallmarking Center <span class="asterisk">*</span></td>
					        		<td>
					        			<select name="hc_id" class="form-control">
					        				<option value="">Select Hallmarking Center</option>
                                    <?php foreach (@$hallmarking_centers as $h_key => $h_value) { ?>
                                        <option value="<?=$h_value['id']?>"><?=$h_value['name']?></option>
                                    <?php } ?>
					        			</select>
					        			<p class="text-danger" id="hc_id_error"></p>
					        		</td>
					        	</tr>
					        	<tr>
					        		<td></td>
					        		<td></td>
					        		<td> Hallmarking Logo <span class="asterisk">*</span></td>
					        		<td>
					        			<input type="text"  class="form-control" name="hc_logo"><p class="text-danger" id="hc_logo_error"></p>
					        		</td>
					        	</tr>
					        	<?php }?>
					        	<!-- <tr>
					        		<td></td>
					        		<td></td>
					        		<td> Hallmarking Net Wt<span class="asterisk">*</span> </td>
					        		<td><input type="text"  class="form-control" name="net_wt"><p class="text-danger" id="net_wt_error"></p></td>
					        	</tr>
					        	<tr>
					        		<td></td>
					        		<td></td>
					        		<td> Hallmarking Quantity<span class="asterisk">*</span> </td>
					        		<td><input type="text"  class="form-control" name="hallmarking_quantity"><p class="text-danger" id="hallmarking_quantity_error"></p></td>
					        	</tr> -->
					        </tbody>
					     </table>
					   </div>
					  </div>

					<div class="col-md-12 col-xs-12 mr10">
	                       <textarea placeholder="Enter Special Remark" id="name" class="col-xs-12 col-md-12 col-lg-12 col-sm-12 txt-box form-control" name="remark" rows="6"></textarea> 
							<span class="text-danger" id="remark_error"></span>
					</div>
					</div>				
					<div class="form-group">
						<div class="col-sm-offset-4 col-sm-8 texalin m-t-15 float_btn">
							<button class="btn btn-success waves-effect waves-light  btn-md qc_accept_btn" name="commit" type="button" onclick="accept_all_m_qc(); " >ACCEPT</button>
							<button class="btn btn-danger waves-effect waves-light  btn-md qc_reject_btn" name="commit" type="button" onclick="reject_all_m_qc(); " style="display: none">REJECT</button>
						</div>
          	</div>
          	</td>
      </div>
      </form>
      </div>
   </div>
</div>