<!DOCTYPE html>
<html>
<head>
  <title>Karigar Receipt</title>
<style>
  body {
    background: rgb(204,204,204);
    font-family: arial; 
    font-size: 14px;
    text-align: left;
  }
  th {
    vertical-align: text-bottom;
    
  }
  page {
    background: white;
    display: block;
    margin: 0 auto;
    margin-bottom: 0.5cm;
    box-shadow: 0 0 0.5cm rgba(0,0,0,0.5);
  }
  page[size="A4"] {  
    width: 21cm;
    height: 29.7cm;
    /*  height:  auto;*/
    padding: 3em; 
    box-sizing: border-box;
  }
  page[size="A4"][layout="portrait"] {
    width: 29.7cm;
    height: 21cm;  
  }
  @media print {
    body, page {
      margin: 0;
      box-shadow: 0;    
    }

  }
table {

    border-collapse: collapse;
    width: 60%;
    border: 1px solid black;


  }
th, th ,td ,tr{

    padding: 5px;
    text-align: left;
    line-height: 100%;
    border: 1px solid black;
    border-color:#cccccc; /*grey*/
    font-size:10px
  }
  td{
    vertical-align: top;
    font-size: 10px;
  }
  th{
    vertical-align: top;
    font-size: 12px;
  }

  @page {
    size: auto;   /* auto is the initial value */
    margin: 0;  /* this affects the margin in the printer settings */
  }
  .print_area *{
    font-size:12px;
    left: 0;
    top: 0;
  }


  @media print {



    .print_area, .print_area * {
      visibility: visible;
      font-size: 12px;
    }
    .input_data
    {
      display: none;
    }
    .button_html
    {
      display: none;
    }
    .print_area {
      font-size: 12px;
      left: 0;
      top: 0;
    }
    .pgbrk 
    {
      page-break-after: always;
    }
  }
</style>
</head>
  <body onload="window.print()">
    <page size="A4">
      <div class="content-page">
      <div class="content">
        <div class="container">
            <div class="row">
              <div class="col-sm-12 card-box-wrap">
                  <div class="card-box padmobile  clearfix">
                      <?php //print_r($receipt_code);
                      //if(!empty($product_data)){ 
                       //$i=0; //foreach($product_data as $val){ ?>
                      <div class="print_area stick">  
                        <div class="col-sm-6">
                          <table style="width: 100%">
                            <tbody>
                              <tr>
                                <th colspan="10">Karigar Receipt</th>
                              </tr>
                              <tr>
                                <td>Rec. No.</td>
                                <td style="text-align: right;"><?php echo $receipt_code[0]['receipt_code'];?></td>
                                <td> </td>
                                <td> </td>
                                <td>Date</td>
                                <th style="text-align: right;"><?php echo date('d/m/Y');?></th>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td></td>
                              </tr>
                              <tr>
                                <td>Karigar Name</td>
                                <th><?php echo $receipt_code[0]['karigar_name'];?></th>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td></td>
                              </tr>
                              <tr>
                                <th>Sr No.</th>
                                <th>Product</th>
                                <?php if($department_id !='2' && $department_id !='3' && $department_id !='6' && $department_id !='10'):?>
                                <th>Quantity</th>
                                <?php else:?>
                                <th>Weight</th>  
                                <?php endif;?>
                                <th>Gross Wt</th>
                                <th>Net Weight</th>
                                <th>weight range</th>
                                <th>Wastage</th>
                                <th>Pure</th>
                                <th>Stone wt</th>
                                <th>Amount</th>
                              </tr>
                             
                             <?php
                             $total_weight = 0;
                             $total_pure = 0;
                             $total_wastage = 0;
                             $total_amount = 0;
                             $total_gross_wt=0;
                             $total_stone_wt=0;
                             $i=1;
                            foreach ($receipt_code as $key => $value) {
                              //print_r($receipt_code);
                              //$total_weight += $value['gross_wt'];
                              $total_gross_wt += $value['gross_wt'];
                              $total_weight += $value['net_wt'];
                              
                              $total_pure += $value['pure'];
                              $total_amount += $value['amount'];
                              $total_wastage += $value['wastage'];
                              $total_stone_wt += $value['stone_wt'];
                              ?>
                                <tr>
                                  <td style="text-align: right;"><?= $i;?></td>
                                  <td><?php echo $value['name'];?></td>
                                  <?php if($department_id !='2' && $department_id !='3' && $department_id !='6' && $department_id !='10'):?>
                                  <td style="text-align: right;"><?php echo $value['quantity'];?></td>
                                  <?php else:?>
                                  <td style="text-align: right;"><?php echo $value['weight'];?></td> 
                                  <?php endif;?> 
                   <!--           <td style="text-align: right;"><?php echo $value['gross_wt'];?></td> -->
                                  <td style="text-align: right;"><?php echo $value['gross_wt'];?></td>
                                  <td style="text-align: right;"><?php echo $value['net_wt'];?></td>
                                  <td style="text-align: right;"><?php echo $value['from_weight'].'-'.$value['to_weight'];?></td>
                                  <td style="text-align: right;"><?php echo $value['wastage'];?></td>
                                  <td style="text-align: right;"><?php echo round($value['pure'],3) ;?></td> 
                                  <td style="text-align: right;"><?php echo round($value['stone_wt'],3) ;?></td>
                                  <td style="text-align: right;"><?php echo $value['amount'];?></td>
                                </tr> 
                              <?php 
                              $i++;
                              }
                              ?>

                              <tr>
                                <td></td>
                                <td></td>
                                <th>Total</th>
                                <td style="text-align: right;"><?= $total_gross_wt?></td> 
                                <td style="text-align: right;"><?= $total_weight?></td>
                                <td></td>
                                <td style="text-align: right;"><?= $total_wastage?></td>
                                <td style="text-align: right;"><?= $total_pure?></td>
                                <td style="text-align: right;"><?= $total_stone_wt?></td>
                                <td style="text-align: right;"><?= $total_amount?></td>
                              </tr>
                              <tr>
                                <td>&nbsp;</td>
                                <td>&nbsp;</td>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td></td>
                              </tr>
                              <!-- <tr>
                                <td></td>
                                <th>TOTAL Wt</th>
                                <td style="text-align: right;">155.6</td>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td></td>
                              </tr>
                              <tr>
                                <td></td>
                                <th>Total Pure</th>
                                <td style="text-align: right;">149.33</td>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td></td>
                              </tr>  -->            
                            </tbody>  
                          </table>
                        </div>
                      </div>
                    <?php //} } ?>
                  </div>
              </div> 
            </div>
        </div>
      </div>
    </div>
    </page>    
  </body>
</html>