<div class="content-page">
<div class="content">
   <div class="container">
      <div class="row">
         <div class="col-sm-12 card-box-wrap">
            <div class="panel panel-default">
               <div class="panel-heading">
               <h4 class="inlineBlock"><?= $display_name;?>
               </h4>
               <div class="pull-right single-receive btn-wrap3">
                <?php
                if($_GET['status'] == 'complete'){
                  $table_col_name='manufacturing_quality_control';
                ?>
                  <a href="<?= ADMIN_PATH?>tag_products" class="add_senior_manager_button btn btn-primary waves-effect w-md waves-light m-b-5 btn-md pull-right single-receive">Tag Products</a>
                <?php
                }else{
                      if($dep_id == '2' || $dep_id=='3' || $dep_id =='6' || $dep_id=='10'){
                          $table_col_name='mfg_qc_ctl_reje_bom_orders';
                      }else{
                          $table_col_name='manufacturing_quality_control_rejected_orders';
                      }
                   
                }
                ?>
                </div>
               </div>
               <!-- <button  type="button" onclick="export_qc_products()" class="add_senior_manager_button btn btn-primary  waves-effect w-md waves-light m-b-5 hidden-xs  btn-md">Export</button> -->
               
                <!-- <button  type="button" class="add_senior_manager_button btn btn-info  waves-effect w-md waves-light m-b-5 hidden-xs  btn-md" onclick="check_all_to_hallmarking()">Send All to Hallmarking</button> -->
               <div class="clearfix"></div>
               <div class="panel-body">
               <div class="table-rep-plugin">
                  <div class="table-responsive b-0 scrollhidden">
              <form target="_blank" enctype='multipart/form-data' class="send_all_to_hallmarking" role="form" name="product_form" id="export" action="<?=ADMIN_PATH.'Manufacturing_quality_control/send_all_to_hallmarking';?>" method="post">
                <input type="hidden" name="hallmarking_center_id" id="hc_id">
                <input type="hidden" name="gross_weight" id="gr_weight">
                <input type="hidden" name="hallmarking_center_logo" id="hc_logo">
               <table class="table table-bordered" id="<?php echo $table_col_name;?>">
               
                  <thead>

                   <?php
                          $data['heading']=$table_col_name;
                          $this->load->view('master/table_header',$data);
                        ?>
               <!--      <tr>
                      <th class="col4">#</th>
                      <th class="col4">Receipt Code</th>
                      <th class="col4">Order Id</th>
                      <th class="col4">Product</th>
                      <th class="col4">Karigar Name</th>
                      <th class="col4">Quantity</th>
                      <?php
                        if($_GET['status'] == 'complete'){
                      ?>
                      <th class="col4">Approx. Wt</th>
                      <?php
                      }else{
                      ?>
                      <th class="col4">Weight</th>
                      <?php
                      }
                      ?> -->
                      <!-- <th class="col4">Qc Status</th> -->
                      <!-- <th class="col3"></th>
                    </tr> -->
                  </thead>
               </table>
              </form>
               </div>
               </div>
               </div>
            </div>
         </div>
      </div>
   </div>
</div>