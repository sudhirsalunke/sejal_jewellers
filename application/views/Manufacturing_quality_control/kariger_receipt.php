<div class="content-page">
<div class="content">
   <div class="container">
      <div class="row">
         <div class="col-sm-12 card-box-wrap">
            <div class="card-box padmobile">
            <div class="panel-heading frmHeading">
               <h4 class="inlineBlock"><span><?= @$display_name?></span>
               </h4>
               </div>
               <div class="panel-body">
               <div class="col-lg-12">
               <input type="hidden" value="<?= $is_kundan?>" id="is_kundan">
                  
                  <?php
                    if($is_kundan == true){
                      ?>
                        <form role="form" class="form-horizontal" method="POST" id="karigar_receive_order" action="<?= ADMIN_PATH?>karigar_receive_order/print_pdf">
                      <?php
                    }else{
                      ?>
                        <form role="form" target="_blank" class="form-horizontal" method="post" action="print_receipt" id="print_receipt_frm">
                      <?php
                    }
                  ?>
                    
                            <div class="col-lg-12">
                                <div class="form-group">
                                    <label class="col-sm-2 col-xs-12 control-label" for="inputEmail3"> Select Karigar<span class="asterisk">*</span></label>
                                    <div class="col-sm-4 col-xs-12">
                                        <select class="form-control" name="kariger_id" onchange="get_receive_product_from_kairgar(this.value)" id="kariger_id">
                                            <option value=''>Select Karigar</option>
                                            <?php 
                                            if(!empty($all_karigars)){
                                            foreach ($all_karigars as $key => $value) {
                                             	foreach ($receive_from_kariger as $rec_value) {                                             	
                                             	    if($rec_value['karigar_id'] == $value['id']){
                                             	    	 ?>
	                                                    <option value="<?= $value['id']?>"><?= $value['name']?></option>
	                                                    <?php
                                             	    }
                                             	}                                                                                    
                                              }                                           
                                            }  
                                            ?>
                                        </select>
                                        <span class="text-danger" id="kariger_id_error"></span>
                                    </div>
                                 
                                </div>
                            </div>
                            <div class="col-lg-12">
                                <div class="form-group">
                                     <div class="col-sm-12 table-rep-plugin col-lg-12">

                                        <div class="table-responsive b-0 scrollhidden">
                                           <table class="table table-bordered">
                                              <thead>
                                                 <tr>
                                                    <th class="col1">Product</th>
                                                    <th class="col1">Weight Range</th>
                                                    <?php if($department_id !='2' && $department_id !='3' && $department_id !='6' && $department_id !='10'):?>
                                                    <th class="col1">Quantity<span class="asterisk">*</span></th>
                                                    <th class="col1">Gross Wt</th>
                                                    <?php else:?>
                                                    <th class="col1">Weight<span class="asterisk">*</span></th>
                                                   <?php endif;?>
                                                    <th class="col1">Net Wt<span class="asterisk">*</span></th>
                                                    <th class="col1">Wastage %</th>
                                                    <th class="col1">Melting</th>
                                                    <th class="col1">Pure</th>
                                                    <?php if($department_id !='2' && $department_id !='3' && $department_id !='6' && $department_id !='10'){?>
                                                    <th class="col1">Stone Wt</th>
                                                    <?php }?>
                                                    <th class="col1">Amount</th>
                                                
                                          
                                      <!--               <th class="col1"></th> -->
                                                 </tr>
                                              </thead>
                                              <tbody id="product_details">
                                                 <tr>
                                                    <th class="col1" colspan="10">No data found..</th>
                                                                 
                                                    <!-- <th class="col1"></th>    -->                
                                                 </tr>
                                              </tbody>
                                           </table>
                                           
                                           
                                           <span class="text-danger" id="qnty_error"></span>
                                           </div>
                                        </div>                                             
                                </div>
                             </div>
                          
                            <div class="col-lg-12">
                                <div class="form-group">
                                    <!-- <label class="col-sm-2 control-label" for="inputEmail3"> Net Weight<span class="asterisk">*</span></label>
                                    <div class="col-sm-2">
                                        <input type="text" min="0" placeholder="" id="net_wt" class="form-control" name="net_wt">
                                        <span class="text-danger" id="net_wt_error"></span>
                                    </div> -->
                                  <?php
                                    if($is_kundan == true){ 
                                  ?>
                                    <label class="col-sm-2 control-label" for="inputEmail3"> Net Weight<span class="asterisk">*</span></label>
                                    <div class="col-sm-2">
                                        <input type="text" min="0" placeholder="" id="net_wt" class="form-control" name="net_wt">
                                        <span class="text-danger" id="net_wt_error"></span>
                                    </div>
                                    <label class="col-sm-2 control-label" for="inputEmail3"> Few Weight<span class="asterisk">*</span></label>
                                    <div class="col-sm-2">
                                        <input type="text" min="0" placeholder="" id="few_wt" class="form-control" name="few_wt">
                                        <span class="text-danger" id="few_wt_error"></span>
                                    </div>
                                    <label class="col-sm-2 control-label" for="inputEmail3"> Mina Weight<span class="asterisk">*</span></label>
                                    <div class="col-sm-2">
                                        <input type="text" min="0" placeholder="" id="mina_wt" class="form-control" name="mina_wt">
                                        <span class="text-danger" id="mina_wt_error"></span>
                                    </div>
                                    <label class="col-sm-2 control-label" for="inputEmail3"> Stone Weight<span class="asterisk">*</span></label>
                                    <div class="col-sm-2">
                                        <input type="text" min="0" placeholder="" id="stone_wt" class="form-control" name="stone_wt">
                                        <span class="text-danger" id="stone_wt_error"></span>
                                    </div>
                                    <label class="col-sm-2 control-label" for="inputEmail3"> Kundan Weight<span class="asterisk">*</span></label>
                                    <div class="col-sm-2">
                                        <input type="text" min="0" placeholder="" id="kundan_wt" class="form-control" name="kundan_wt">
                                        <span class="text-danger" id="kundan_wt_error"></span>
                                    </div>
                                    <label class="col-sm-2 control-label" for="inputEmail3"> Stone Amount<span class="asterisk">*</span></label>
                                    <div class="col-sm-2">
                                        <input type="text" min="0" placeholder="" id="stone_wt" class="form-control" name="stone_amt">
                                        <span class="text-danger" id="stone_amt_error"></span>
                                    </div>
                                    <label class="col-sm-2 control-label" for="inputEmail3"> Kundan Amount<span class="asterisk">*</span></label>
                                    <div class="col-sm-2">
                                        <input type="text" min="0" placeholder="" id="kundan_wt" class="form-control" name="kundan_amt">
                                        <span class="text-danger" id="kundan_amt_error"></span>
                                    </div>
                                    <label class="col-sm-2 control-label" for="inputEmail3"> Other Amount<span class="asterisk">*</span></label>
                                    <div class="col-sm-2">
                                        <input type="text" min="0" placeholder="" id="other_wt" class="form-control" name="other_wt">
                                        <span class="text-danger" id="other_wt_error"></span>
                                    </div>
                                  <?php
                                    }
                                  ?>
                              </div>
                             </div>
                             
                            <!--  <div class="col-lg-12">
                                <div class="form-group">
                                    
                                    <label class="col-sm-2 control-label" for="inputEmail3">Quantity<span class="asterisk">*</span></label>
                                    <div class="col-sm-1">
                                        <input type="number" min="0" placeholder="" id="Quantity" class="form-control" name="quantity">
                                        <span class="text-danger" id="quantity_error"></span>
                                    </div>
                                </div>
                             </div><br/><br/> -->
                            
                              <span id="totalRecodcnt"></span>
                             <?php
                              if($is_kundan == true){
                              ?>
                                <div class="col-lg-12">
                                   <div class="col-lg-7 sticky-btn" id="button_save">
                                      <div class="form-group">
                                          <div class="col-sm-offset-5 col-sm-8 texalin">
                                              <button onclick="Print_receipt()" type="button" class="btn btn-primary waves-effect waves-light  btn-md">Print Receipt</button>&nbsp;&nbsp;
                                              <a href="<?= ADMIN_PATH?>/Karigar_receive_order"><button class="btn btn-primary waves-effect waves-light  btn-md" name="commit" type="button">Back</button></a>
                                          </div>
                                      </div>
                                   </div>
                                </div>
                              <?php
                              }else{
                              ?>
                                <div class="col-lg-12">
                                   <div class="col-lg-12 sticky-btn" id="button_save">
    	                                <div class="form-group ">
    	                                    <div class="col-sm-8 texalin btnSection"> 
    	                                        <a href="<?= ADMIN_PATH?>Received_orders"><button class="btn btn-default waves-effect waves-light btn-md pull-left sticky-back" name="commit" type="button">Back</button></a>
                                              <button class="btn btn-success waves-effect waves-light btn-md pull-right" id="save_print" name="commit" type="button"  onclick="validate_print_karigar_from()"> Save and Print</button>&nbsp;&nbsp;
    	                                    </div>
    	                                </div>
                                   </div>
                                </div>
                            <?php 
                            }
                            ?>                                        
                  </form>
               </div>
               <div class="clearfix"></div>
               </div>
            </div>
         </div>
      </div>
   </div>
</div>