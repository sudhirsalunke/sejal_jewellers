s<div class="content-page">
<div class="content">
   <div class="container">
      <div class="row">
         <div class="col-sm-12 card-box-wrap">
            <div class="card-box padmobile">
               <h4 class="header-title business_manager_header_title m-t-0 m-b-30 bmsmheader bmsmheaderabm"><span><?= $display_name;?></span>
              </h4>
               <div class="clearfix"></div>
               <div class="table-rep-plugin">
                  <div class="table-responsive b-0 scrollhidden" >
              <form enctype='multipart/form-data' role="form" name="product_form" id="export">
                <div class="col-sm-12 col-xs-12 m-t-15">
                   <div class="row col-sm-8 form-group">
                    <div class="col-sm-4 no_pad">
                    <label class="col-sm-5 control-label " for="inputEmail3">Karigar Name </label>
                    <div class="col-sm-5"></div>
                  </div>
                  <div class="col-sm-4 no_pad">
                    <label class="col-sm-5 control-label" for="inputEmail3">Receipt Code</label>
                    <div class="col-sm-5"></div>
                  </div>
                   </div>
                    <div class="row col-sm-8 form-group">
                      <div class="col-sm-4 no_pad">
                        <label class="col-sm-5 control-label" for="inputEmail3">Received Date </label>
                        <div class="col-sm-5"></div>
                      </div>
                    
                      <div class="col-sm-4 no_pad">
                        <label class="col-sm-5 control-label" for="inputEmail3">Quantity</label>
                        <div class="col-sm-5"><?=$result['quantity']?> </div>
                      </div>
                   </div>
                 </div>
              <table class="table custdatatable table-bordered" id="sub_catagory" >
                  <thead>
                     <tr>
                         <th class="col4">#</th>
                         <th class="col4">Parameter</th>
                        <th class="col3">Remark</th>
                     </tr>
                  </thead>
                  <tbody>
                    <?php 
                    $check_list_array =array('ghat_wt','design_checking','pair_matching','karatage/purity','size','stamping','sharp_edge','solder/linking','shape_out','finishing','gr_wt/nt_wt','tag_detail','wearing_test','alignment','packing','Meena');
                    $start=1;
                    foreach ($check_list_array as $key => $value) {
                        if (empty($result[$value])) { 
                          $text_name = strtoupper(str_replace('_', ' ', $value))
                          ?>
                           <tr>
                              <td><?=$start?></td>
                              <td><?=$text_name?></td>
                              <td><?=@$result[$value.'_remark']?></td>
                            </tr>
                       <?php $start++; }
                    } ?>
                    </tbody>
               </table>
              </form>
               </div>
               </div>
            </div>
         </div>
      </div>
   </div>
</div>