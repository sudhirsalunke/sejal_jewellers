<style media="print">
@page {
  size: auto;
  margin: 0;
}
img {
  -webkit-print-color-adjust: exact;
}
</style>
<center><div class="content-page">
<div class="content">
   <div class="container">
      <div class="row">
         <div class="col-sm-12 card-box-wrap">
            <div class="card-box padmobile">
               <h4 class="header-title business_manager_header_title m-t-0 m-b-30 bmsmheader bmsmheaderabm"><span>Karigar Resend Receipt</span>
               </h4>
                <div class="clearfix"></div>
               <div class="product_listing">
                <div class="print_area">
                
                <div class="col-sm-12 m-t-20 single_product_div">
                    <p class="col-sm-5 control-label" for="inputEmail3">Order Number :<?php echo $result['order_id'];?></p>
                    <p class="col-sm-5 control-label" for="inputEmail3">Date :<?php echo date('d-m-Y')?></p>             
                    <p class="col-sm-5 control-label" for="inputEmail3">Karigar Name :<?php echo $result['karigar_name'];?></p>
                    <!-- <p class="col-sm-5 control-label" for="inputEmail3">Product Code :<?php //echo $result['product_code'];?></p> -->
                </div>
                <div class="col-sm-12" style="width:50%">
                   <table class="table table-bordered custdatatable">
                        <thead>
                           <tr>
                              <th class="col4" style="border: 1px solid #000;">Parent Category</th>
                              <th class="col4" style="border: 1px solid #000;">Weight Range</th>
                              <th class="col4" style="border: 1px solid #000;">Quantity</th>
                              <th class="col4" style="border: 1px solid #000;">Weight</th>
                           </tr>
                        </thead>
                        <tbody>
                        <?php
                            ?>
                            <tr>
                              <th class="col4" style="border: 1px solid #000;"><?php echo $result['name'];?></th>
                              <th class="col4" style="border: 1px solid #000;"><?php echo $result['from_weight'].'-'.$result['to_weight'];?></th>
                              <th class="col4" style="border: 1px solid #000;"><?php echo $result['quantity'];?></th>
                              <th class="col4" style="border: 1px solid #000;"><?php echo $result['weight'];?></th>
                           </tr>
                            <?php
                        ?>
                           
                        </tbody>
                     </table>

                </div>
              </div>
              <div class="clearfix"></div>
            </div>
               </div>
             </div>
            </div>
         </div>
      </div>
   </div>
</div></center>

<style>
.space
{
  height: 30px;
}
.space1
{
  height: 20px;
}
.karigar_name
{
  font-weight:bold;
}
.product_listing >div
{
  background-color: #fff;
}
.barcode_text {
    color: #000;
    font-size: 10px;
    font-weight: bold;
    text-align: left;
    margin-left: 10px;
}

table td{
  padding: 3px 5px;
}

@media print {
  /*body * {
    visibility: hidden;
  }*/
  .print_area, .print_area * {
    visibility: visible;
  }
  .print_area {
    
    left: 0;
    top: 0;
  }
  

}

</style>