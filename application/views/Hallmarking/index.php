<div class="content-page">
<div class="content">
   <div class="container">
      <div class="row">
         <div class="col-sm-12 card-box-wrap">

            <div class="panel panel-default">
              <div class="panel-heading">
               <h4 class="inlineBlock"><span><?= $display_name;?></span></h4>
               <div class="pull-right btn-wrap m-t-5">                 
                <?php if($product_status=='receive'){ ?>
               <button  type="button" onclick="export_hm_products()" class="add_senior_manager_button btn btn-warning  waves-effect w-md waves-light btn-md">Export</button>

               <?php } 
                if($product_status=='send'){ ?>
               <button  type="button" onclick="send_to_qcexported()" class="add_senior_manager_button btn btn-primary  waves-effect w-md waves-light  btn-md">Accept From Hallmarking</button>
               <!-- <button  type="button" onclick="all_chekced_qc_sent()" class="add_senior_manager_button btn btn-primary  waves-effect w-md waves-light m-b-5 hidden-xs  btn-md">Shortlist All Products</button> -->
               <?php } 

               if($product_status=='exported') { ?>
<!--                   <a href="javascript:void(0);" onclick="hm_barcode_products();"><button  type="button" class="add_senior_manager_button btn btn-primary  waves-effect w-md waves-light btn-md">Bar code Sticker</button></a>
 -->               <a href="javascript:void(0);" onclick="export_hm_export_products();"><button  type="button" class="add_senior_manager_button btn btn-warning  waves-effect w-md waves-light btn-md">Export</button></a>
                <a href="<?=ADMIN_PATH.'Import_bill_excel'?>"><button  type="button" class="add_senior_manager_button btn btn-import  waves-effect w-md waves-light btn-md">Import GT Excel</button></a>
               <?php } ?>


               </div>
              </div>
              <form class="hm_product_form" enctype='multipart/form-data' role="form" name="product_form" id="export" method="post" >
              <div class="panel-body">
                <div class="table-rep-plugin">
                  <div class="table-responsive b-0 scrollhidden">              
                    <p class="text-danger export_hallmark_error"></p>
                     <input type="hidden" id="add_selected_hallmark">
                     <div class="table-responsive">
                       <table class="table table-bordered custdatatable stripe row-border order-column dataTable no-footer" id="Hallmarking">
                       <thead>
                        <?php
                          $data['heading']='Hallmarking';
                          $this->load->view('master/table_header',$data);
                        ?>
                        </thead>
                     </table>
                     </div>
                  </div>
                </div>
              </div>
        </form>
                <div class="clearfix"></div>                
            </div>
         </div>
      </div>
   </div>
</div>