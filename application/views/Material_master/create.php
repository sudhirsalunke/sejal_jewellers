<div class="content-page">
<div class="content">
   <div class="container">
      <div class="row">
         <div class="col-sm-6 card-box-wrap">
            <div class="panel panel-default">
              <div class="panel-heading frmHeading">
                <h4 class="inlineBlock"><span>Add Material</span></h4>
              </div>
              <div class="panel-body">
                <div class="col-lg-12">
                  <form name="material_master_form" id="material_master_form" role="form" class="form-horizontal" method="post">
                     <div class="form-group">
                        <label class="col-sm-4 control-label" for="inputEmail3">Material Description <span class="asterisk">*</span></label>
                        <div class="col-sm-8">
                           <textarea  placeholder="Enter Material Description" class="form-control" name="material[description]"> </textarea>
                           <span class="text-danger" id="description_error"></span>
                        </div>
                     </div>
                     <div class="form-group">
                        <label class="col-sm-4 control-label" for="inputEmail3">Short Code </label>
                        <div class="col-sm-8">
                           <input type="text" placeholder="Enter Short Code" class="form-control" name="material[short_code]">
                           <span class="text-danger" id="short_code_error"></span>
                        </div>
                     </div>
                    <div class="btnSection">
                       
                         <button class="btn btn-default waves-effect waves-light m-l-5 btn-md" onclick="window.location='<?= ADMIN_PATH?>Material'" type="reset">
                           Back
                           </button>
                          <button class="btn btn-success waves-effect waves-light btn-md pull-right" name="commit" type="button" onclick="save_material_master('store'); ">
                         SAVE
                         </button>
                     
                  </div>
                  </form>
               </div>
              </div>
            </div>
         </div>
      </div>
   </div>
</div>