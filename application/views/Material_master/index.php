<div class="content-page">
<div class="content">
   <div class="container">
      <div class="row">
         <div class="col-sm-12 card-box-wrap">
            <div class="panel panel-default">
              <div class="panel-heading">                
                <h4 class="inlineBlock">All Materials</h4>
                <a href="<?= ADMIN_PATH?>Material/create" class="pull-right add_senior_manager_button btn btn-primary waves-effect w-md waves-light m-t-5 btn-md single-add">ADD MATERIAL</a>
              </div>
              <div class="panel-body">
              <div class="clearfix"></div>
               <div class="table-rep-plugin">
                  <div class="table-responsive b-0 scrollhidden">
                    <table class="table table-bordered custdatatable" id="material_master_table">
                      <thead>
                         <?php
                          $data['heading']='material_master_table';
                          $this->load->view('master/table_header',$data);
                        ?>
                         <!-- <tr>
                            <th class="col4">Description</th>
                            <th class="col4">Short Code</th>
                            <th class="col3"></th>
                         </tr> -->
                      </thead>
                    </table>
                  </div>
               </div>
              </div>               
            </div>
         </div>
      </div>
   </div>
</div>