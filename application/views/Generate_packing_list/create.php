<div class="content-page">
<div class="content">
   <div class="container">
      <div class="row">
         <div class="col-sm-12 card-box-wrap">
            <div class="panel panel-default">
              <div class="panel-heading">
               <h4 class="inlineBlock"><span><?= $page_title;?></span></h4>
               <div class="pull-right m-t-5 btn-wrap">
                 <a onclick="generate_packing_list()"><button  type="button" class="add_senior_manager_button btn btn-primary waves-effect w-md waves-light btn-md">Generate Packing List</button></a>
               </div>
              </div>
              <div class="panel-body">
                <?php //print_r($result); ?>
               <div class="clearfix"></div>
                <div class="table-rep-plugin">
                  <div class="table-responsive b-0 scrollhidden">
                    <form enctype='multipart/form-data' role="form" name="product_form" id="export">
                      <!--   <select name="category_id" placeholder="Select Category" class="chosen-select" onchange="get_product_by_category(this.id)"  style="width:200px;">
                       
                        <option>Select Category</option>
                        <?php 
                        foreach ($category as $key => $value) {
                          ?>
                          <option value="<?=$value['id']?>"><?=$value['name']?></option>
                          <?php } ?>
                      </select> -->

                      <div class="row form-group">
                       <div class="col-sm-6 no_pad pull-right">
                          <div class="col-sm-6">
                                    <label>Gold Rate <span class="asterisk">*</span></label>
                                    <input type="text" placeholder="Enter Gold Rate" id="name" class="form-control" name="gold_rate" value="">
                                    <span class="text-danger" id="gold_rate_error"></span>
                                    </div>
                                    <div class="col-sm-6">
                                    <label>Other Charges <span class="asterisk">*</span></label>
                                     <input type="text" placeholder="Enter Other Charges" i class="form-control" name="other_charges" value="">
                                    <span class="text-danger" id="other_charges_error"></span>
                                </div>
                        </div>
                      </div>
                        <span class="text-danger products_error" id="products_error"></span>
                        <table class="table custdatatable table-bordered " id="Generate_packing_list_create">
                          <thead>
                           <?php
                              $data['heading']='Generate_packing_list_create';
                              $this->load->view('master/table_header',$data);
                            ?>
                          </thead>
                        </table>
                      <!--  <table class="table table-bordered custdatatable" id="Generate_packing_list/create">
                        <thead>
                  
                          <tr>
                            <th class="col4">#</th>
                            <th class="col4">Product Code</th>
                            <th class="col4">Order Id</th>
                            <th class="col4">Corporate</th>
                            <th class="col4">Category Name</th>
                            <th class="col4">Date</th>
                            <th class="col4">Quantity</th>
                            <th class="col4">Size</th>
                            <th class="col4">Total Gr.Wt</th>
                            <th class="col4">Total Net.Wt</th>
                            <th class="col3"></th>
                          </tr>
                        </thead>
                        <tbody>
                        <?php if (count($result)==0) { ?>
                          <tr>
                            <td colspan="11">No result found</td>
                          </tr>
                        <?php } else{ 
                         foreach ($result as $key => $value){ ?>
                        <tr>
                          <td><div class="PREPARE_check checkbox checkbox-purpal"><input type="checkbox" class="from-control" value="<?=$value['id']?>" name="product_code[]" data-parsley-id="76" data-parsley-multiple="group1"><label for="PREPARE_check"></label></div></td>
                          <td><?=$value['product_code'];?></td>
                          <td><?=$value['order_id'];?></td>
                          <td><?=$value['corporate_name'];?></td>
                          <td><?=$value['category_name'];?></td>
                          <td><?=date('d-m-Y '.'||'.' H:i:s',strtotime($value["created_at"]))?></td>
                          <td><?=$value['quantity'];?></td>
                          <td><?=$value['size'];?></td>
                          <td><?=$value['total_gr_wt'];?></td>
                          <td><?=$value['total_net_wt'];?></td>
                          <td><a href="<?=ADMIN_PATH.'Print_Stickers/index/'.$value['id']?>"><button id="b_'.$value['id'].'" class=" btn btn-primary small loader-hide  btn-sm  m-l-10" name="commit" type="button">PRINT STICKERS</button></a></td>
                        </tr>
                        <?php } } ?>
                        
                        </tbody>
                      </table> -->
                    </form>
                  </div>
                </div>
              </div>               
            </div>
         </div>
      </div>
   </div>
</div>