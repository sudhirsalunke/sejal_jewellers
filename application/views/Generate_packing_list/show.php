<div class="content-page">
<div class="content">
  <div class="container">
    <div class="row">
      <div class="col-sm-12 card-box-wrap">
        <div class="panel panel-default">
          <div class="panle-heading">
           <h4 class="inlineBlock"><span><?=$display_title;?></span></h4>                
          </div>
          <div class="panel-body">
            <div class="clearfix"></div>
            <div class="table-rep-plugin">
              <div class="table-responsive b-0 scrollhidden">
                <form enctype='multipart/form-data' role="form" name="product_form" id="export">
                  <p> Packing Order No. <?=$order_no;?></p>
                 <table class="table table-bordered custdatatable" id="packing_order_view">
                    <thead>
                          <?php
                        $data['heading']='packing_order_view';
                        $this->load->view('master/table_header',$data);
                      ?>
                    
                    </thead>
                 </table>
                </form>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>