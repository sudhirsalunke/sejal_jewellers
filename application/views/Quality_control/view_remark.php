<div class="content-page">
<div class="content">
   <div class="container">
      <div class="row">
         <div class="col-sm-12 card-box-wrap">
            <div class="panel panel-default">
            <div class="panel-heading frmHeading">
               <h4 class="inlineBlock"><span><?= $display_name;?></span>
              </h4>
              </div>
              <?php //print_r($result);?>
               <div class="clearfix"></div>
               <div class="table-rep-plugin">
                  <div class="table-responsive b-0 scrollhidden" >
              <form enctype='multipart/form-data' role="form" name="product_form" id="export">
               <div class="panel panel-body">
                <div class="col-sm-12 col-xs-12">
                  <div class="row">
                    <div class="form-group col-sm-12 col-xs-12">                    
                      <label class="col-sm-2 col-xs-6 control-label " for="inputEmail3">Name </label>
                      <div class="col-sm-2 col-xs-6"><?= $this->session->userdata('first_name').' '.$this->session->userdata('last_name')?></div>
                    </div>
                    <div class="form-group col-sm-12 col-xs-12">                  
                      <label class="col-sm-2 col-xs-6 control-label" for="inputEmail3">Employee ID </label>
                      <div class="col-sm-2 col-xs-6"><?= $this->session->userdata('user_id')?></div>
                    </div>
                    <div class="form-group col-sm-12 col-xs-12">
                      <label class="col-sm-2 col-xs-6 control-label" for="inputEmail3">Product Code </label>
                      <div class="col-sm-2 col-xs-6"><?=$result['product_code']?></div>
                      </div>
                  </div>
                    
                  <div class="row">
                     <div class="form-group col-sm-12 col-xs-12">
                        <label class="col-sm-2 col-xs-6 control-label" for="inputEmail3">UID </label>
                        <div class="col-sm-2 col-xs-6"><?=$result['corporate_product_id']?></div>
                      </div>
                      <div class="form-group col-sm-12 col-xs-12">
                        <label class="col-sm-2 col-xs-6 control-label" for="inputEmail3">Size </label>
                        <div class="col-sm-2 col-xs-6"><?=$result['cp_size']?></div>
                      </div>
                      <div class="form-group col-sm-12 col-xs-12">
                          <label class="col-sm-2 col-xs-6 control-label" for="inputEmail3">Quantity </label>
                          <div class="col-sm-2 col-xs-6"><?= $result['quantity']?> </div>
                      </div>
                  </div>
                    
                  <div class="row">
                     <div class="form-group col-sm-12 col-xs-12">
                        <label class="col-sm-2 col-xs-6 control-label" for="inputEmail3">Total GR. WT. </label>
                        <div class="col-sm-2 col-xs-6"><?= $result['total_gr_wt']?></div>
                      </div>
                      <div class="form-group col-sm-12 col-xs-12">
                        <label class="col-sm-2 col-xs-6 control-label" for="inputEmail3">Less WT. </label>
                        <div class="col-sm-2 col-xs-6"><?= $result['less_wt']?> </div>
                      </div>
                      <div class="form-group col-sm-12 col-xs-12">
                        <label class="col-sm-2 col-xs-6 control-label" for="inputEmail3">Total NET. WT. </label>
                        <div class="col-sm-2 col-xs-6"><?= $result['total_net_wt']?></div>
                      </div>
                   </div>
                  </div>
                </div>
                <div class="col-md-12">
                 <?php
                 if(empty($result['bom_checking/rrl_code']) || empty($result['design_checking']) ||empty($result['karatage/purity']) || empty($result['size']) || empty($result['stamping']) || empty($result['sharp_edge']) || empty($result['solder/linking']) || empty($result['shape_out']) || empty($result['finishing']) || empty($result['gr_wt/nt_wt']) || empty($result['tag_detail']) || empty($result['finding/lock']) || empty($result['wearing_test']) || empty($result['alignment']) || empty($result['packing']) || empty($result['weight_check'])){
                 ?>
              <table class="table custdatatable table-bordered" id="sub_catagory" >
                  <thead>
                     <tr>
                         <th class="col4">#</th>
                         <th class="col4">Parameter</th>
                        <th class="col3">Remark</th>
                     </tr>
                  </thead>
                  <tbody>
                    <?php if($result['bom_checking/rrl_code']==''){ ?>
                    <tr>
                      <td>1</td>
                      <td>BOM CHECKING/RRL CODE</td>
                      <td><?=$result['bom_checking/rrl_code_remark'] ?></td>
                    </tr>
                    <?php }if($result['design_checking']==''){ ?>
                    <tr>
                      <td>2</td>
                      <td>DESIGN CHECKING</td>
                      <td><?=$result['design_checking_remark'] ?></td>
                    </tr>
                     <?php }if($result['pair_matching']==''){ ?>
                    <tr>
                      <td>3</td>
                      <td>PAIR MATCHING</td>
                      <td><?=$result['pair_matching_remark'] ?></td>
                    </tr>
                    <?php }if($result['karatage/purity']==''){ ?>
                    <tr>
                      <td>4</td>
                      <td>CARATAGE/PURITY</td>
                      <td><?=$result['karatage/purity_remark'] ?></td>
                    </tr>
                    <?php }if($result['size']==''){ ?>
                    <tr>
                      <td>5</td>
                      <td>SIZE</td>
                      <td><?=$result['size_remark'] ?></td>
                    </tr>
                    <?php }if($result['stamping']==''){ ?>
                    <tr>
                      <td>6</td>
                      <td>STAMPING</td>
                      <td><?=$result['stamping_remark'] ?></td>
                    </tr>
                    <?php }if($result['sharp_edge']==''){ ?>
                    <tr>
                      <td>7</td>
                      <td>SHARP EDGE</td>
                      <td><?=$result['sharp_edge_remark'] ?></td>
                    </tr>
                    <?php }if($result['solder/linking']==''){ ?>
                    <tr>
                      <td>8</td>
                      <td>SOLDER/LINKING</td>
                      <td><?=$result['solder/linking_remark'] ?></td>
                    </tr>
                    <?php }if($result['shape_out']==''){ ?>
                    <tr>
                      <td>9</td>
                      <td>SHAPE OUT</td>
                      <td><?=$result['shape_out_remark'] ?></td>
                    </tr>
                    <?php }if($result['finishing']==''){ ?>
                    <tr>
                      <td>10</td>
                      <td>FINISHING</td>
                      <td><?=$result['finishing_remark'] ?></td>
                    </tr>
                    <?php }if($result['gr_wt/nt_wt']==''){ ?>
                    <tr>
                      <td>11</td>
                      <td>GR WT/NET WT</td>
                      <td><?=$result['gr_wt/nt_wt_remark'] ?></td>
                    </tr>
                    <?php }if($result['tag_detail']==''){ ?>
                    <tr>
                      <td>12</td>
                      <td>TAG DETAIL</td>
                      <td><?=$result['tag_detail_remark'] ?></td>
                    </tr>
                    <?php }if($result['finding/lock']==''){ ?>
                    <tr>
                      <td>13</td>
                      <td>FINDING/LOCK</td>
                      <td><?=$result['finding/lock_remark'] ?></td>
                    </tr>
                    <?php }if($result['wearing_test']==''){ ?>
                    <tr>
                      <td>14</td>
                      <td>WEARING/TEST</td>
                      <td><?=$result['wearing_test_remark'] ?></td>
                    </tr>
                    <?php }if($result['alignment']==''){ ?>
                    <tr>
                      <td>15</td>
                      <td>ALIGNMENT</td>
                      <td><?=$result['alignment_remark'] ?></td>
                    </tr>
                    <?php }if($result['packing']==''){ ?>
                    <tr>
                      <td>16</td>
                      <td>PACKING</td>
                      <td><?=$result['packing_remark'] ?></td>
                    </tr>
                    <?php }if(empty($result['weight_check'])){ ?>
                    <tr>
                      <td>17</td>
                      <td>Weight Check</td>
                      <td><?=$result['weight_check_remark'] ?></td>
                    </tr>
                    <?php }?>
                    </tbody>
               </table>
               <?php } 
               if($result['remark']!=''){ ?>
                    <table class="table custdatatable table-bordered">
                      <thead>
                        <tr>
                          <th class="col4">Special Remark</th>
                        </tr>
                      </thead>
                      <tbody>
                      <tr>
                      <td><?=$result['remark'] ?></td>
                    </tr>
                    <?php } ?>
               
                 </tbody>
               </table>
                </div>
                <div class="form-group btnSection clearfix">
                    <button class="btn btn-default waves-effect waves-light  btn-md qc_reject" onclick="javascript:close_toast_popup(document.referrer)" name="commit" type="button">
                     Back
                    </button>
                  </div>
               </div>
              </form>
               
                  
                
               </div>
               </div>
            </div>
         </div>
      </div>
   </div>
</div>