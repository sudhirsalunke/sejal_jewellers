<div class="content-page">
<div class="content">
   <div class="container">
      <div class="row">
         <div class="col-sm-12 card-box-wrap">
            <div class="panel panel-default">            
            <div class="panel-heading">
               <h4 class="inlineBlock"><?= $display_name;?>
               </h4>
               <div class="pull-right btn-wrap">                
              <?php if(@$_GET['status']!='rejected'){ ?>
               <button  type="button" onclick="export_qc_products()" class="add_senior_manager_button btn btn-warning waves-effect w-md waves-light m-b-5 btn-md pull-right">EXPORT</button>
              <?php }?>
              <?php if($product_status=='complete'){ ?>
              <!--  <a href="<?= ADMIN_PATH?>Generate_packing_list/create"><button  type="button" class="add_senior_manager_button btn btn-primary  waves-effect w-md waves-light m-b-5 hidden-xs  btn-md">Generate Packing List</button></a> -->
              <a onclick="multiple_send_to_hallmarking()" ><button  type="button" class="add_senior_manager_button btn btn-info waves-effect w-md waves-light m-b-5 btn-md send_to_hm pull-right">SEND TO HALLMARKING</button></a>
              <?php } ?>
            </div>
              </div>
               <form enctype='multipart/form-data' role="form" name="product_form" id="export">
              <div class="panel-body">             
              <?php if(@$_GET['status']!='rejected'){ ?>
              <div class="form-group col-sm-4">
              <select class="form-control  waves-effect w-md waves-light m-b-5" name="hc_id">
              <option value="0">Select Hallmarking Center</option>
                <?php
                  foreach ($hallmarking_center as $hc_key => $hc_value) {
                    ?>
                      <option value="<?= $hc_value['id']?>"><?= $hc_value['name']?></option>
                    <?php
                  }
                ?>
              </select>

              </div>
               <?php } ?>
               <div class="clearfix"></div>
               <div class="table-rep-plugin">
                  <div class="b-0 scrollhidden">
   <!--        <?php if($_GET['status']=='rejected') { ?>
                  <div class="form-group col-sm-4">
                <select id="corporate_id" class="form-control search-input-select" filtercol="1">
                  <option value="">Select Corporate</option>
                  <?php
                   $all_corporates=array();
                   $all_corporates =get_corporates();
                  foreach ($all_corporates as $key => $value) { ?>
                      <option value="<?=$value['id']?>"><?=$value['name']?></option>
                 <?php } ?>
                </select>
                </div>


                <?php } ?> -->
              <div class="table-responsive">
               <table class="table table-bordered" id="Quality_control">
                  <thead>
                  <?php
                          $data['heading']='Quality_control';
                      $this->load->view('master/table_header',$data)?>
                  </thead>
               </table>
              </div>
               </div>
               </div>
              </form>
            </div>
         </div>
      </div>
   </div>
</div>