<style type="text/css">
  .fixed {position:fixed; top:75px;z-index: 1000;background: #dcdcdc;padding: 11px 4px;width: 80%}
</style>
<div class="content-page">
  <div class="content">
   <div class="container">
      <div class="row">
     <div class="col-sm-12 card-box-wrap">
      <div class="panel panel-default">
        <div class="panel-heading frmHeading">
         <h4 class="">Not Assigned To Karigar - <small>Prepare Order</small></h4>
        </div>

       <div class="search panel-body">
         <div class="form-group" >

       <!-- <h4 class="header-title business_manager_header_title m-t-0 m-b-30 bmsmheader bmsmheaAll Prepared Ordersderabm"><small>Prepare Order</small>
       </h4> -->     
        
       <div class="col-lg-12" style="margin-bottom:30px;">
       <div class="row">
         <div class="form-group" id="task_flyout">
          <b> <span id="product_count" class="pull-right" style="display: none"></span></b>  
          <label class="col-sm-2 control-label font-600" for="inputEmail3"> Select karigar <span class="asterisk">*</span></label>
          <div class="col-sm-4 ">
            <?php //print_r($selected_products);  ?>
            <select name="karigar" class="form-control karigar chosen-select" id="sel_karigar" >
              <option value=''>Select Karigar</option>
              <?php

              if(!empty($karigar)){
                foreach ($karigar as $key => $value) {
                  ?>
                  <option value="<?=$value['id']?>" <?=(!empty($karigar_id) && $karigar_id == $value['id']) ? 'selected' : ''?>><?= $value['name'];?> (<?= $value['code'];?>)</option>
                 <?php
                }
              }
              ?>
            </select>
            <span class="text-danger" id="karigar_error"></span>

          </div>
        </div>
      </div>
      </div>   


      <div class="col-lg-12">  
      
        <input type="hidden" id="add_selected_ids">
        <form name="serach_text" id="Prepare_order" role="form" class="form-horizontal" method="post">
        <div class="form-group">
      <label class="col-sm-2 control-label font-600" style="text-align: left;" for="inputEmail3"> Search By Corporate</label>
        <div class="col-sm-8">
          <?php
          if(!empty($corporates)){
            foreach ($corporates as $co_key => $co_value) {
              ?>
              <div class="checkbox checkbox-purpal col-md-3">
                <input onchange="show_order_filters(this)" type="checkbox" name="eo.corporate" value="<?=$co_value['id']?>" class="filter from-control " data-parsley-multiple="groups" data-parsley-mincheck="2" data-parsley-id="69">
                <label for="inputEmail3"> <?=$co_value['name']?> </label>
              </div>
              <?php
            }
          }
          ?>
        </div>
      </div>
         <div class="col-sm-12">
         <div class="table-responsive">
          <table class="table table-bordered custdatatable" id="Receive_pieces">
            <thead>
             <tr>
               <th class="col4">Select</th>
               <th class="col4">Order No</th>
               <th class="col4">Order Date </th>
               <th class="col4">Corporate</th>
               <th class="col4">Total Design</th>
               <th class="col4">Pending Design</th>
          
              <button class="btn btn-hide btn-md pull-right" id="hide" type="button">hide</button>
              <button class="btn btn-warning btn-md pull-right" id="show" type="button" style="display:none">show</button>

             </tr>
           </thead>
           <tbody id="orders">
            <?php
            /*if(!empty($orders)){
              foreach ($orders as $cp_key => $cp_value) {
                ?>
                <tr class="row1 corporate_<?=$cp_value['corporate_id']?>">
                  <td><div class="checkbox checkbox-purpal">
                    <input type="checkbox" data-parsley-multiple="groups" data-parsley-mincheck="2" data-parsley-id="69" name="cp.order_id" class="filter" value="<?=$cp_value['order_id']?>">
                    <label for="order_id"></label>
                  </div></td>
                  <td><?=$cp_value['order_id']?></td>
                  <td><?=date('d-m-Y',strtotime($cp_value['date']));?></td>
                  <td><?=$cp_value['corporate']?></td>
                  <td><?=$cp_value['total_products']?></td>
                  <td><?=$cp_value['pending_products']?></td>
                </tr>
                <?php
              }
            }else{ ?>
                      <tr class="row1">
                          <td colspan="6">No Orders Found</td>
                      </tr>
           <?php } */?>
          </tbody>
        </table>
      </div>
      </div>
      <div class="show_design_filters">
    <h4 class="header-title business_manager_header_title m-t-0 m-b-30 bmsmheader bmsmheaderabm"><span>Search By Design</span>
      </h4>
      <?php// print_r($corporates);?>
      <div class="form-group categorycode">
      <label class="col-sm-2 control-label font-600" style="text-align: left;" for="inputEmail3">Category</label>
        <div class="col-sm-8 " id="category">
          <?php
          if(!empty($category)){
            foreach ($category as $c_key => $c_value) {
              ?>
              <!-- <div class="checkbox checkbox-purpal col-md-3">
                <input type="checkbox" name="p.category_id" value="<?=$c_value['id']?>" class="filter from-control" data-parsley-multiple="groups" data-parsley-mincheck="2" data-parsley-id="69">
                <label for="inputEmail3"> <?=$c_value['name']?> </label>
              </div> -->
              <div class="checkbox checkbox-purpal col-md-3">
                <input type="checkbox" name="cp.product_master" value="<?=$c_value['product_master']?>" class="filter from-control" data-parsley-multiple="groups" data-parsley-mincheck="2" data-parsley-id="69">
                <label for="inputEmail3"> <?=$c_value['product_master'].' ('.$c_value['product_count'].')'?> </label>
              </div>
              
              <?php
            }
          }
          ?>
        </div>
      </div>
      <!--<div class="form-group weight_range">
      <label class="col-sm-2 control-label font-600" style="text-align: left;" for="inputEmail3">Weight Range</label>
        <div class="col-sm-8" id="weight_range">-->
          <!-- <?php
          if(!empty($weights)){
            foreach ($weights as $w_key => $w_value) {
              ?> -->
             <!--  <div class="checkbox checkbox-purpal col-md-3">
                <input name="p.weight_band_id"  value="<?=$w_value['id']?>" class="filter from-control" type="checkbox" data-parsley-multiple="group1" data-parsley-id="76">
                <label for="inputEmail3"><?=$w_value['from_weight']?> - <?=$w_value['to_weight']?> </label>
              </div> -->
              <!-- <div class="checkbox checkbox-purpal col-md-3">
                <input name="cp.weight_band"  value="<?=$w_value['weight_band']?>" class="filter from-control" type="checkbox" data-parsley-multiple="group1" data-parsley-id="76">
                <label for="inputEmail3"><?=$w_value['weight_band']?> </label>
              </div>
              
              <?php
            }
          }
          ?>
        </div>
      </div>-->
      </div>
      <div class="form-group productcode">
      <label class="col-sm-2 control-label font-600" style="text-align: left;" for="inputEmail3">Design Code</label>
       <div class="col-sm-4">
        <input type="text" class="filter form-control pd-lft" name="p.product_code_LIKE" placeholder="Enter Design Code"></input>
      </div>
    </div>
    <input type="hidden" id="selected_id" name="cp.id" value="<?= @$selected_products['no_of_products']?>">
  </form>
</div>              
<form name="serach_text" id="products" role="form" class="form-horizontal" method="post">
  <input type="hidden" id="karigar_id" name="karigar_id">
  <input type="hidden" id="prev_karigar_id" name="prev_karigar_id" value="<?= @$id?>">
  <input type="hidden" id="selected_id" name="id" value="<?= @$selected_products['no_of_products']?>">
  <input type="hidden" name="mode" value="<?=(!empty($selected_products['karigar_id'])) ? 'edit' : 'add' ?>">
 <span class="text-danger col-sm-10 text-right m-t-20" id="products_error"></span>
  <div class="col-lg-12 product_listing_div" style="">
    <div class="col-lg-12">
        <button class="btn btn-primary btn-md pull-right" id="check_all_prepared" type="button">Check All</button>
        <input type="hidden" id="all_check_qc">
    </div>
                <!-- <?php
                  foreach ($products as $p_key => $p_value) {
                    if(!empty($p_value['order_id'])){
                    ?>
                  <div class="col-lg-6" style="padding-top: 10px;">
                    <div class="col-lg-5">
                      <img src="<?= UPLOAD_IMAGES.'product/'.$p_value['product_code'].'/'.'small'.'/'.$p_value['image'];?>" >
                    </div>
                    <div class="col-lg-6">
                      <p>Order Id : <?= $p_value['order_id']?></p>
                      <p>Product Code : <?= $p_value['sort_Item_number']?></p>
                      <p>Pair/Pieces : <?//= $p_value['order_id']?></p>
                      <p>Size/Length : <?//= $p_value['order_id']?></p>
                      <p>Weight Range : <?= $p_value['from_weight']?> - <?= $p_value['to_weight']?></p>
                      <p><input type="checkbox" name="products"  value="<?=$w_value['id']?>" class="from-control"></p>
                    </div>
                  </div>
                    <?php
                  }
                  }
                  ?> --><div class="load_html">
                  
                </div>
                
              </div>
             
              <div class="col-sm-offset-5 col-sm-4 texalin m-t-30">
               <button id="prepare_order_submit" class="btn btn-success waves-effect waves-light btn-md" style="padding: 9px 26px !important" name="commit" type="button" onclick="prepare_order('1'); ">
                 Save
               </button> 

               <button id="prepare_order_edit" class="btn btn-info waves-effect waves-light btn-md" style="padding: 9px 26px !important" name="commit" type="button" onclick="prepare_order('2'); ">
                 Save And Edit
               </button>
             </div>
           </form>
           <div class="clearfix"></div>
         </div>
       </div>
     </div>

   </div>
 </div>
</div>
<script type="text/javascript">
  var karigar_id = '<?=$id?>';
</script>