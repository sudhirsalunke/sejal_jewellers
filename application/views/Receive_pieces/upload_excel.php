<div class="content-page">
<div class="content">
   <div class="container">
      <div class="row">
         <div class="col-sm-6 card-box-wrap">
            <div class="panel panel-default">
              <div class="panel-heading frmHeading">
                <h4 class="inline inlineBlock"><?= strtoupper($page_title);?></h4>                 
              </div>
              <div class="panel-body">
                <div class="col-lg-12">
                <form name="Upload_receive_pieces" id="Upload_receive_pieces" role="form" class="form-horizontal" method="post" enctype="multipart/form-data">
                  <div class="col-sm-12 form-group">
                    <label class="col-sm-4 control-label" for="inputEmail3"> Order Id<span class="asterisk">*</span></label>
                    <div class="col-sm-8">
                     <input type="text" name="order_id" class="form-control" id="order_id" placeholder="Enter Order Id">
                      <span class="text-danger" id="order_id_error"></span>
                    </div>
                  </div>
                  <div class="col-sm-12 form-group">
                    <label class="col-sm-4 control-label" for="inputEmail3"> Select Corporate <span class="asterisk">*</span></label>
                    <div class="col-sm-8">
                      <input type="hidden" name="corporate" value="<?=$vendor?>">
                      <?=@$corporates[$vendor]?>
                      <span class="text-danger" id="corporate_error"></span>
                    </div>
                  </div>
                  <div class="col-sm-12 form-group">
                    <label class="col-sm-4 control-label" for="inputEmail3"> Status<span class="asterisk">*</span></label>
                    <div class="col-sm-8">
                      <select  class="form-control category_id" name=status>
                        <option value='Approve'>Approve</option>
                        <option value='Reject'>Reject</option>
                      </select>
                      <span class="text-danger" id="status_error"></span>
                    </div>
                  </div>
                  <div class="col-sm-12 form-group">                    
                      <label class="col-sm-4 control-label" for="inputEmail3"> Import Excel <span class="asterisk">*</span></label>
                      <div class="col-sm-8 m-t-2">
                         <input type="file" placeholder="Enter Name" id="category" class="" name="Upload_product_excel[file]">
                         <span class="text-danger" id="file_error"></span>
                      </div>
                   
                  </div>
                 
                    <div class="texalin btnSection">
                    <button class="btn btn-default waves-effect waves-light btn-md" name="commit" type="button" onclick="history.back();">Back</button>  
                         <button class="btn btn-primary waves-effect waves-light btn-md pull-right" name="commit" type="button" onclick="Receive_pieces_excel();">
                         UPLOAD
                         </button>
                     
                    </div>
                  
                </form>
                <div class="col-lg-offset-4 errors col-lg-6">
                </div>
              </div>
              </div>
            </div>
         </div>
      </div>
   </div>
</div>