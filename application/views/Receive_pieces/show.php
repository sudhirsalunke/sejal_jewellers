<div class="content-page">
<div class="content">
   <div class="container">
      <div class="row">
         <div class="col-sm-12 card-box-wrap">
            <div class="card-box padmobile">
              <div class="panel panel-default">
               <div class="panel-heading">
               <h4 class="inlineBlock">Receive Designs
               </h4>
               <div class="pull-right m-t-5 btn-wrap">
               <a onclick="multiple_pieces_to_rejected()" ><button  type="button" class="add_senior_manager_button btn btn-danger waves-effect w-md waves-light m-b-5 btn-md">All Reject</button></a>

                <a onclick="multiple_pieces_to_approved()" ><button  type="button" class="add_senior_manager_button btn btn-success  waves-effect w-md waves-light m-b-5 btn-md ">All Approve</button></a> 

                <a href="<?= ADMIN_PATH?>Receive_pieces/export_products/<?=$order_id;?>"><button  type="button" class="add_senior_manager_button btn btn-warning waves-effect w-md waves-light m-b-5 btn-md">Export Design</button></a>
               </div>
               </div>
               </div>
        
               <!--  <a href="<?= ADMIN_PATH?>Receive_pieces/Upload_excel"><button  type="button" class="add_senior_manager_button btn btn-primary  waves-effect w-md waves-light m-b-5 hidden-xs  btn-md">Upload Excel</button></a> -->
               
               <div class="clearfix"></div>
               <div class="panel-body">
               <div class="table-rep-plugin">
                  <div class="table-responsive b-0 scrollhidden">
              <form enctype='multipart/form-data' role="form" name="product_form" id="product_form">
               <table class="table custdatatable" id="show_pieces">
                  <thead>
                     <tr>
                        <th class="col4"><div class="checkbox checkbox-purpal"><input value="0" type="checkbox" id="check_all"><label for="check_all"></label> </div></th>
                         <th class="col4">Order Id</th>
                         <th class="col4">Code</th>
                         <th class="col4">Size</th>
                         <th class="col4">Karigar</th>
                         <th class="col4">Metal</th>
                         <th class="col4">Category</th>
                         <th class="col4">Sub Category</th>
                         <th class="col4">Carat</th>
                         <th class="col4">WT Range</th>
                        <th class="col4">Image</th>
                        <th class="col3"></th>
                     </tr>
                  </thead>
               </table>
              </form>
               </div>
               </div>
               </div>
            </div>
         </div>
      </div>
   </div>
</div>