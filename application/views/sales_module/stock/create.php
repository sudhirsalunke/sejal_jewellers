<div class="content-page">
    <div class="content">
        <div class="container">
            <div class="row">
                <div class="col-sm-12 card-box-wrap">
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            <h4 class="inlineBlock"><span><?= $display_title;?></span></h4>                
                        </div>
                        <div class="stock-check">    
                            <form name="category" id="qc_check" role="form" class="form-horizontal" method="post">
                                <input type="hidden" class="form-control" name="action_type" id="action_type" value="stock">
                                <div class="row">  
                                    <div class="form-group col-sm-2">

                                        <div class="form-group">
                                            <select class="form-control sel_department" name="department_id" id='department_id' onchange="get_category_dep(this)">
                                                <option value=''>Department</option>
                                                <?php foreach ($department as $d_key => $d_value) { ?>
                                                    <option value="<?= $d_value['id'] ?>"><?= $d_value['name'] ?></option>
                                                <?php } ?>
                                            </select>
                                            <span class="text-danger" id="department_id_error"></span>
                                        </div>
                                    </div> 
                                    <div class="form-group col-sm-2">   
                                        <div class="form-group">           
                                            <select class="form-control stock_category_id" name="category_id" id="category_id" onChange="get_product_category(this)">
                                                <option value="">Please Select category</option>
                                            </select>
                                        </div>
                                    </div>

                                    <div class="form-group col-sm-2">                                       
                                        <div class="form-group">

                                            <select class="form-control sel_karigar" name="kariger_id" id="kariger_id">
                                                <option value="">Please Select karigar</option>                                       
                                                <?php foreach ($karigar as $k_key => $k_value) { ?>
                                                    <option value="<?= $k_value['id'] ?>">
                                                        <?= $k_value['name'] ?>
                                                    </option>
                                                <?php } ?>
                                            </select>
                                            <span class="text-danger" id="category_id_error"></span>
                                        </div>
                                    </div>
                                </div>

                                <div class="table-rep-plugin">
                                    <div class="table-responsive b-0 scrollhidden">
                                        <table class="table custdatatable table-bordered" id="stock_products">
                                            <thead>
                                                <tr>
                                                    <th class="col3">Product</th>
                                                    <th class="col3">Code</th>
                                                    <th class="col3">Qty</th>
                                                    <th class="col4">Net wt</th>
                                                    <th class="col3">Few wt</th>
                                                    <th class="col3">K Pure</th>
                                                    <th class="col3">M Wt</th>
                                                    <th class="col3">Wax</th>
                                                    <th class="col3">C/SWt</th>
                                                    <th class="col3">Moti Wt</th>
                                                    <th class="col3">CH wt</th>
                                                    <?php if ($department_id=='8'){?>
                                                    <th class="col3">Beads wt</th>
                                                    <?php } ?>
                                                    <th class="col4">Gr Wt</th>
                                                    <th class="col3">CH PCS</th>
                                                    <th class="col4">CH@</th>
                                                    <th class="col3">CH amt</th>
                                                    <th class="col4">Kun Pc</th>
                                                    <th class="col3">Kun@</th>
                                                    <th class="col3">Kun Amt</th>
                                                    <th class="col3">stn wt</th>
                                                    <th class="col3">stn@</th><!--  C/S@-->
                                                    <th class="col3">C/S Amt</th>
                                                    <th class="col3">Oth Amt</th>
                                                    <th class="col3">Ttl Amt</th>
                                                    <th class="col3"></th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                        
                                                <tr class="tr_clone abc">   
                                                    <td class="col3">
                                                        <select style="width: 140px" name="parent_category_id[]" class="form-control product_code item_cat create_code">
                                                            <option value="">Select Product</option>
                                                            <?php
                                                            /* foreach ($parent_category as $p_key => $p_val) { ?>
                                                                <option data-code="<?= $p_val['code']?>" value="<?= $p_val['name'] ?>" data-max_cnt="<?= $p_val['max_count'] ?>"><?= $p_val['name'] ?></option>
                                                            <?php }*/ ?>
                                                        </select>
                                                    </td>

                                                    <td class="col3">   
                                                        <input type="text" name="product_code[]" class="form-control item_code product_code_tp"  style="width: 150px" readonly="">                                         
                                                    </td>                                        
                                                    <td class="col3"> 
                                                        <input type="text" name="quantity[]" class="form-control quantity" value="1" readonly=""> 
                                                    </td>

                                                    <td> 
                                                        <input type="text" name="net_wt[]" class="form-control net_wt" onkeyup="calculate_grs_wt_stock(this)">
                                                    </td>
                                                    <td>
                                                        <input type="hidden" class="form-control rp_id" name="rp_id[]" value="1">
                                                        <input onkeyup="calculate_grs_wt_stock(this)" type="text" min="0"  id="few_wt" class="form-control few_wt" name="few_wt[]">
                                                    </td>
                                                    <td>
                                                        <input onkeyup="calculate_grs_wt_stock(this)" type="text" min="0"   class="form-control kundan_pure" name="kundan_pure[]">
                                                    </td>
                                                    <td>
                                                        <input type="text" min="0"   class="form-control mina_wt" onkeyup="calculate_grs_wt_stock(this)" name="mina_wt[]">
                                                    </td>
                                                    <td>
                                                        <input type="text" min="0"   class="form-control wax_wt" onkeyup="calculate_grs_wt_stock(this)" name="wax_wt[]">
                                                    </td>
                                                    <td>
                                                        <input type="text" min="0"   class="form-control color_stone" onkeyup="calculate_grs_wt_stock(this)" name="color_stone[]">
                                                    </td>
                                                    <td>
                                                        <input type="text" min="0"   class="form-control moti" onkeyup="calculate_grs_wt_stock(this)" name="moti[]">
                                                    </td>
                                                    <td>
                                                        <input type="text" min="0"  class="form-control checker_wt" onkeyup="calculate_grs_wt_stock(this)" name="checker_wt[]">
                                                    </td>
                                                    <?php if ($department_id=='8'){ ?>
                                                    <td>
                                                        <input type="text" min="0"  class="form-control black_beads_wt" onkeyup="calculate_grs_wt_stock(this)" name="black_beads_wt[]">
                                                    </td>
                                                    <?php } ?>
                                                    <td>
                                                        <input type="text" min="0"  class="form-control ttl_gross_wt" name="gross_wt[]" readonly="">
                                                    </td>
                                                    <td>
                                                        <input type="text" min="0" onkeyup="calculate_checker_amt_stock(this)" class="form-control checker_pcs" name="checker_pcs[]">
                                                    </td>
                                                    <td>
                                                        <input type="text" min="0"  class="form-control checker_rate" onkeyup="calculate_checker_amt_stock(this)" name="checker_rate[]">
                                                    </td>
                                                    <td>
                                                        <input type="text" min="0" class="form-control checker_amt" name="checker_amt[]" readonly="">
                                                    </td>
                                                <!--    <td>
                                                         <input type="text" min="0" class="form-control kundan_wt" onkeyup="calculate_kun_amt_stock(this)" name="kundan_wt[]" value="">
                                                    </td> -->
                                                    <input type="hidden" name="code[]" class="product_cat_code">
                                                    <td>
                                                        <input type="text" min="0"  onkeyup="calculate_kun_amt_stock(this)"  class="form-control kundan_pc" name="kundan_pc[]" value="">
                                                    </td>
                                                    <td>
                                                        <input type="text" min="0" class="form-control kundan_rate" onkeyup="calculate_kun_amt_stock(this)" name="kundan_rate[]" value="">
                                                    </td>
                                                    <td>
                                                        <input type="text" min="0"  class="form-control kundan_amt" name="kundan_amt[]" onkeyup="calculate_total_amt_stock(this)" value="">
                                                    </td>
                                                    <td>
                                                        <input type="text" min="0"   class="form-control stone_wt" name="stone_wt[]" readonly="">
                                                    </td>

                                                    <td>
                                                        <input type="text" min="0"  class="form-control color_stone_rate" name="color_stone_rate[]" onkeyup="calculate_grs_wt_stock(this)" >
                                                    </td>
                                                    <td>
                                                        <input type="text" min="0"   class="form-control stone_amt" name="stone_amt[]" onkeyup="calculate_total_amt_stock(this)">
                                                    </td>
                                                    <td>
                                                        <input type="text" min="0"  class="form-control other_wt" name="other_wt[]" onkeyup="calculate_total_amt_stock(this)">
                                                    </td>
                                                    <td>
                                                        <input type="text" min="0" class="form-control ttl_wt" name="total_wt[]" readonly="">
                                                    </td>
                                                    <td>
                                                        <input type="button" name="add" value="Add" class="btn btn-sm btn-success  btn-sm tr_clone_add">                         
                                                        <button class="btn btn-danger waves-effect waves-light  btn-sm" onclick="remove_tr(this)" name="commit" type="button">REMOVE</button>
                                                    </td>
                                                </tr>

                                     <!-- <tr class="add_more_stock">
                                    </tr> -->
                                            </tbody>
                                        </table>
                                    </div>
                                    <div class="div_error col-sm-offset-5 col-sm-8"></div>   
                                    <div class="form-group btnSection">
                                        <button type="button" onclick="window.location = '<?= ADMIN_PATH . 'ready_product' ?>'" class="btn btn-md btn-default waves-effect waves-light" >BACK</button>  
                                        <button type="button" class="btn btn-md btn-success waves-effect waves-light pull-right print_kundan_order" onclick="receive_kundan_karigar_order(this,'stock');">Submit</button>
                                    </div>
                            </form>
                        </div>
                    </div>
                    <div class="clearfix">
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
</div>
