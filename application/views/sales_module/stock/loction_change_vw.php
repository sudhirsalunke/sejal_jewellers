<div class="content-page">
<div class="content">
   <div class="container">
      <div class="row">
         <div class="col-md-10 col-sm-10 col-sm-12 card-box-wrap">
            <div class="panel panel-default">

              <div class="panel-heading frmHeading">                
               <h4 class="inlineBlock"><?= $display_title;?></h4>

              </div>
               <div class="panel-body">
                  <form name="update_product_location" id="update_product_location" role="form" class="form-horizontal" method="post">
                    <input type="hidden" name="drp_id" id="drp_id" value="<?=$ready_product_data['id'];?>">
                     <div class="form-group">
                        <label class="col-sm-3 control-label" for="inputEmail3"> Product Code <span class="asterisk"></span></label>

                        <div class="col-sm-4 m-t-0">
                 
                      <input type="text" name="product_code" class="form-control" id="product_code" value="<?= $product_code;?>" readonly="">
             
                        
                        </div>


                     </div> 
                       <div class="form-group">
                        <label class="col-sm-3 control-label" for="inputEmail3"> Location <span class="asterisk">*</span></label>
                        <div class="col-sm-4">
                           <select name="location_id" class="form-control" id="location_id">
                           <option value="">Select location</option>
                           <?php
                              foreach ($location as $key => $value) {
                                 ?>
                                     <option value="<?= $value['id']?>" <?= ($value['id']==$ready_product_data['location_id']) ? 'selected' : ''?>><?= $value['name']?></option>
                                 <?php 
                              }
                            ?>
                             
                           </select>
                           <span class="text-danger" id="corporate_error"></span>
                        </div>
                     </div>               
          
                    <div class="texalin btnSection">       
                   <button class="btn btn-default waves-effect waves-light btn-md" name="commit" type="button" onclick="history.back();">Back</button>           
                      <button class="btn upload-btn waves-effect waves-light btn-md pull-right" name="commit" type="button" onclick="Save_product_location();" style="width:97px;" >
                         update
                      </button>
                    </div>                  
                  </form>
                  <div class="col-lg-offset-4 errors col-lg-6">
                  </div>
               </div>
               <div class="clearfix"></div>
            </div>
         </div>
      </div>
   </div>
</div>
