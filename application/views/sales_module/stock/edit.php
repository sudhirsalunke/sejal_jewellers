<div class="content-page">
    <div class="content">
        <div class="container">
            <div class="row">
                <div class="col-sm-12 card-box-wrap">
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            <h4 class="inlineBlock"><span><?= $display_title;?></span></h4>                
                        </div>
                        <?php //print_r($product_details[0])?>
                        <div class="stock-check">    
                            <form name="category" id="qc_check" role="form" class="form-horizontal" method="post">
                                <input type="hidden" class="form-control" name="action_type" id="action_type" value="edit">
                                <div class="row">  
                                    <div class="form-group col-sm-2">

                                        <div class="form-group">
                                            <select class="form-control sel_department" name="department_id" id="department_id" disabled="">
                                           
                                                <?php foreach ($department as $d_key => $d_value) { ?>
                                                    <option value="<?=$d_value['id']?>" <?= ($d_value['id']==$product_details[0]['department_id']) ? 'selected' : ''?>><?=$d_value['name']?></option>


                                                <?php } ?>
                                            </select>
                                            <span class="text-danger" id="department_id_error"></span>
                                        </div>
                                    </div> 
                               
                                    <div class="form-group col-sm-2">   
                                        <div class="form-group">           
                                            <select class="form-control" name="category_id" id="category_id" disabled="">
                                                     <?php foreach ($category as $c_key => $c_value) { ?>
                                                    <option value="<?=$d_value['id']?>" <?= ($c_value['code']==$product_details[0]['item_category_code']) ? 'selected' : ''?>><?=$c_value['name']?></option>


                                                <?php } ?>
                                            </select>
                                        </div>
                                    </div>

                                    <div class="form-group col-sm-2">                                       
                                        <div class="form-group">

                                            <select class="form-control" name="kariger_id" id="kariger_id">
                                                <option value="">Please Select karigar</option>                                       
                                                <?php foreach ($karigar as $k_key => $k_value) { ?>
                                                    <option value="<?= $k_value['id'] ?>"  <?= ($k_value['id']==$product_details[0]['karigar_id']) ? 'selected' : ''?>>
                                                        <?= $k_value['name'] ?>
                                                    </option>
                                                <?php } ?>
                                            </select>
                                            <span class="text-danger" id="category_id_error"></span>
                                        </div>
                                    </div>

                                    <div class="form-group col-sm-2">                                       
                                        <div class="form-group">

                                        <select class="form-control stone_category" name="stone_category" 
                                        id="stone_category" onclick="calculate_stone_category_stock(this)" value="">
                                                <option value="">Stone category</option>                                       
                                                <?php foreach ($ac_product_category as $stc_key => $stc_value) { ?>
                                                    <option value="<?= $stc_value['percentage'] ?>"  <?= ($stc_value['percentage']==$product_details[0]['stone_category']) ? 'selected' : ''?>>
                                                        <?= $stc_value['name'] ?>
                                                    </option>
                                                <?php } ?>
                                            </select>
                                            <span class="text-danger" id="stone_category_id_error"></span>
                                        </div>
                                    </div>
                                    <div class="form-group col-sm-2">                                       
                                        <div class="form-group">

                                            <select class="form-control kundan_category" name="kundan_category" id="kundan_category" onclick="calculate_kundan_category_stock(this)" >
                                                <option value="">Kundan category</option>                                       
                                                  <?php foreach ($ac_product_category as $stc_key => $stc_value) { ?>
                                                    <option value="<?= $stc_value['percentage'] ?>"  <?= ($stc_value['percentage']==$product_details[0]['kundan_category']) ? 'selected' : ''?>>
                                                        <?= $stc_value['name'] ?>
                                                    </option>
                                                <?php } ?>
                                            </select>
                                            <span class="text-danger" id="category_id_error"></span>
                                        </div>
                                    </div>
                                    <div class="form-group col-sm-2">                                       
                                        <div class="form-group">

                                            <select class="form-control ch_category" name="ch_category" id="ch_category" onclick="calculate_ch_category_stock(this)" >
                                                <option value="">CH category</option>                                       
                                                  <?php foreach ($ac_product_category as $stc_key => $stc_value) { ?>
                                                    <option value="<?= $stc_value['percentage'] ?>"  <?= ($stc_value['percentage']==$product_details[0]['ch_category']) ? 'selected' : ''?>>
                                                        <?= $stc_value['name'] ?>
                                                    </option>
                                                <?php } ?>
                                            </select>
                                            <span class="text-danger" id="category_id_error"></span>
                                        </div>
                                    </div>
                                </div>

                                <div class="table-rep-plugin">
                                    <div class="table-responsive b-0 scrollhidden">
                                        <table class="table custdatatable table-bordered" id="stock_products">
                                            <thead>
                                                <tr>
                                                    <th class="col3">Product</th>
                                                    <th class="col3">Code</th>
                                                    <th class="col3">Qty</th>
                                                    <th class="col4">Net wt</th>
                                                    <?php if ($department_id !='11'){?>
                                           <!--          <th class="col3">CH wt</th> -->
                                                    <th class="col3">Few wt</th>
                                                    <th class="col3">K Pure</th>
                                                    <th class="col3">M Wt</th>
                                                    <th class="col3">Wax</th>
                                                    <th class="col3">C/SWt</th>
                                                    <th class="col3">Moti Wt</th>
                                                    <th class="col3">CH wt</th>
                                                    <?php }else if ($department_id=='8'){?>
                                                    <th class="col3">Beads wt</th>
                                                    <?php } ?>
                                                    <th class="col4">Gr Wt</th>
                                                    <?php if ($department_id!='11'){?>
                                                    <th class="col3">CH PCS</th>
                                                    <th class="col4">CH@</th>
                                                    <th class="col3">CH amt</th>
                                                    <th class="col4">Kun Pc</th>
                                                    <th class="col3">Kun@</th>
                                                    <th class="col3">Kun Amt</th>
                                                    <th class="col3">stn wt</th>
                                                    <th class="col3">stn@</th><!--  C/S@-->
                                                    <th class="col3">C/S Amt</th>
                                                    <th class="col3">Beads wt</th>
                                                    <th class="col3">Oth Amt</th>
                                                    <th class="col3">Ttl Amt</th>
                                                    <?php } ?>
                                                </tr>
                                            </thead>
                                            <tbody>
                                        <?php// print_r($product_details);?>
                                                <tr class="tr_clone abc">   
                                                    <td class="col3">
                                                        <select style="width: 140px" name="parent_category_id" class="form-control" disabled="">
                                                            <option value="">Select Product</option>
                                                            <?php
                                                             foreach ($parent_category as $p_key => $p_val) { ?>
                                                                <option value="<?= $p_val['name'] ?>" <?= ($p_val['name']==$product_details[0]['product']) ? 'selected' : ''?>><?= $p_val['name'] ?></option>
                                                            <?php } ?>
                                                        </select>
                                                    </td>
                                                 
                                                    <td class="col3"> 
                                                    <input type="hidden" name="product_code" class="product_cat_code" value="<?php echo $product_details[0]['product_code'];?>">  
                                                        <input type="text" name="code" class="form-control" value="<?php echo $product_details[0]['product_code'];?>" class="form-control" readonly style="width: 150px" disabled="">

                                                    </td>                                        
                                                    <td class="col3"> 
                                                        <input type="text" name="quantity" class="form-control" value="<?php echo $product_details[0]['quantity'];?>" readonly> 
                                                    </td>

                                                    <td> 
                                                    <?php if(!empty($product_details[0]['custom_net_wt'])){ $custom_net_wt= $product_details[0]['custom_net_wt']; }else{ $custom_net_wt= $product_details[0]['net_wt']; } ?>

                                                        <input type="hidden" name="net_wt" class="form-control" value="<?php echo $product_details[0]['net_wt'];?>">    
                                                        <input type="text" name="custom_net_wt" class="form-control net_wt custom_net_wt" onkeyup="calculate_grs_wt_stock(this)" value="<?= round($custom_net_wt,2)?>">

                                                        
                                                    </td>
                                                       <?php if ($department_id !='11'){ ?>
                                                    <td>                                                      
                                                        <input onkeyup="calculate_grs_wt_stock(this)" type="text" min="0"  id="few_wt" class="form-control few_wt" name="few_wt" value="<?php echo round($product_details[0]['few_wt'],2);?>">
                                                    </td>
                                                    <td>
                                                        <input type="hidden" min="0"   class="form-control" name="kundan_pure" value="<?php echo $product_details[0]['kundan_pure'];?>"> 
                                                        <?php if(!empty( $product_details[0]['custom_kundan_pure'])){ $custom_kundan_pure=$product_details[0]['custom_kundan_pure'];}else{ $custom_kundan_pure= $product_details[0]['kundan_pure'];} ;?>

                                                        <input onkeyup="calculate_grs_wt_stock(this)" type="text" min="0" class="form-control kundan_pure custom_kundan_pure"  name="custom_kundan_pure"      value="<?= round($custom_kundan_pure,2)?>">

                                                    </td>
                                                    <td>
                                                        <input type="text" min="0"   class="form-control mina_wt" onkeyup="calculate_grs_wt_stock(this)" name="mina_wt" 
                                                        value="<?php echo round($product_details[0]['mina_wt'],2);?>">
                                                    </td>
                                                    <td>
                                                        <input type="text" min="0"   class="form-control wax_wt" onkeyup="calculate_grs_wt_stock(this)" name="wax_wt" value="<?php echo round($product_details[0]['wax_wt'],2);?>" >
                                                    </td>
                                                    <td>
                                                        <input type="text" min="0"   class="form-control color_stone" onkeyup="calculate_grs_wt_stock(this)" name="color_stone" value="<?php echo round($product_details[0]['color_stone'],2);?>" >
                                                    </td>
                                                    <td>
                                                        <input type="text" min="0"   class="form-control moti" onkeyup="calculate_grs_wt_stock(this)" name="moti" value="<?php echo round($product_details[0]['moti'],2);?>" >
                                                    </td>
                                                    <td>
                                                        <input type="hidden" min="0"  class="form-control " 
                                                        name="checker_wt" value="<?php echo round($product_details[0]['checker_wt'],2);?>" >   

                                                        <?php if(!empty( $product_details[0]['custom_checker_wt'])){ $custom_checker_wt= $product_details[0]['custom_checker_wt'];}else{ $custom_checker_wt= $product_details[0]['checker_wt'];} ;?>  

                                                        <input type="text" min="0"  class="form-control custom_checker_wt checker_wt" onkeyup="calculate_grs_wt_stock(this)" 
                                                        name="custom_checker_wt" value="<?= round($custom_checker_wt,2)?>">


                                                    </td>
                                                    <?php } ?>
                                                    
                                                    <td>
                                                        <input type="hidden" min="0"  class="form-control" name="gross_wt" value="<?php echo $product_details[0]['gr_wt'];?>">  
                                                        <?php if(!empty( $product_details[0]['custom_gr_wt'])){ $custom_gr_wt= $product_details[0]['custom_gr_wt'];}else{ $custom_gr_wt= $product_details[0]['gr_wt'];} ;?> 
                                                        <input type="text" min="0"  class="form-control ttl_gross_wt" name="custom_gr_wt" value="<?= round($custom_gr_wt,2)?>" readonly="">

                                                    </td>
                                                    <?php if ($department_id != '11'){ ?>
                                                <!--     <td>
                                                        <input type="text" min="0" onkeyup="calculate_checker_amt_stock(this)" class="form-control checker" name="checker" value="<?php echo $product_details[0]['checker'];?>" >
                                                    </td> -->
                                                    <td>
                                                        <input type="text" min="0"  class="form-control checker_pcs" onkeyup="calculate_checker_amt_stock(this)" name="checker_pcs" value="<?php echo round($product_details[0]['checker_pcs'],2);?>" >
                                                    </td>
                                                 <!--   <td>
                                                         <input type="text" min="0" class="form-control kundan_wt" onkeyup="calculate_kun_amt_stock(this)" name="kundan_wt" value="">
                                                    </td> -->
                                                     <td>
                                                        <input type="text" min="0" onkeyup="calculate_checker_amt_stock(this)" class="form-control checker_rate" name="checker_rate" value="<?php echo round($product_details[0]['checker_rate'],2);?>" >
                                                    </td>
                                                    <td>
                                                        <input type="text" min="0" class="form-control checker_amt" name="checker_amt" value="<?php echo round($product_details[0]['checker_amt'],2);?>" readonly="">
                                                    </td>
                                          
                                                    <td>
                                                        <input type="text" min="0"  onkeyup="calculate_kun_amt_stock(this)"  class="form-control kundan_pc" name="kundan_pc" value="<?php echo round($product_details[0]['kundan_pc'],2);?>" >
                                                    </td>
                                                    <td>
                                                        <input type="text" min="0" class="form-control kundan_rate" onkeyup="calculate_kun_amt_stock(this)" name="kundan_rate" value="<?php echo round($product_details[0]['kundan_rate'],2);?>">
                                                    </td>
                                                    <td>
                                                        <input type="text" min="0"  class="form-control kundan_amt" name="kundan_amt" onkeyup="calculate_total_amt_stock(this)" value="<?php echo round($product_details[0]['kundan_amt'],2);?>" >
                                                    </td>
                                                    <td>
                                                        <input type="hidden" min="0"   class="form-control" name="stone_wt" value="<?php echo round($product_details[0]['stone_wt'],2);?>">
                                                        <?php if(!empty( $product_details[0]['custom_stone_wt'])){ $custom_stone_wt=$product_details[0]['custom_stone_wt'];}else{ $custom_stone_wt= $product_details[0]['stone_wt'];} ;?>
                                                        <input type="text" min="0"   class="form-control custom_stone_wt stone_wt" name="custom_stone_wt" value="<?= round($custom_stone_wt);?>"  readonly="">
                                                    </td>

                                                    <td>
                                                        <input type="text" min="0"  class="form-control color_stone_rate" name="color_stone_rate" onkeyup="calculate_grs_wt_stock(this)" value="<?php echo round($product_details[0]['color_stone_rate'],2);?>" >
                                                    </td>
                                                    <td>
                                                        <input type="text" min="0"   class="form-control stone_amt" name="stone_amt" onkeyup="calculate_total_amt_stock(this)" value="<?php echo round($product_details[0]['stone_amt'],2);?>" >
                                                    </td>
                                                          <td>
                                                        <input type="text" min="0"  class="form-control black_beads_wt" onkeyup="calculate_grs_wt_stock(this)" name="black_beads_wt" value="<?php echo round($product_details[0]['black_beads_wt'],2);?>" >
                                                    </td>
                                                    <td>
                                                        <input type="text" min="0"  class="form-control other_wt" name="other_wt" onkeyup="calculate_total_amt_stock(this)" value="<?php echo round($product_details[0]['other_wt'],2);?>" >
                                                    </td>
                                                    <td>
                                                        <input type="text" min="0" class="form-control ttl_wt" name="total_wt" value="<?php echo round($product_details[0]['total_wt'],2);?>"  readonly="">
                                                    </td>
                                                    <?php }?>
                                                <!--     <td>
                                                        <input type="button" name="add" value="Add" class="btn btn-sm btn-success  btn-sm tr_clone_add">                         
                                                        <button class="btn btn-danger waves-effect waves-light  btn-sm" onclick="remove_tr(this)" name="commit" type="button">REMOVE</button>
                                                    </td> -->
                                                </tr>

                     
                                            </tbody>
                                        </table>
                                    </div>
                                    <div class="div_error col-sm-offset-5 col-sm-8"></div>   
                                    <div class="form-group btnSection">
                                        <a href="javascript:history.go(-1)"><button type="button" class="btn btn-md btn-default waves-effect waves-light" >BACK</button> </a> 
                                        <button type="button" class="btn btn-md btn-success waves-effect waves-light pull-right print_kundan_order" onclick="receive_kundan_karigar_order(this,'edit');">Submit</button>
                                    </div>
                            </form>
                        </div>
                    </div>
                    <div class="clearfix">
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
</div>
<script type="text/javascript">
function calculate_stone_category_stock( evt) {
    evt.value = evt.value.replace(/[^0-9.]/g, '');
    var net_wt = parseFloat($('.custom_net_wt').val()) || 0;
    var stone_wt = parseFloat($('.custom_stone_wt').val()) || 0;
    var stone_category = parseFloat($('.stone_category').val()) || 0;
    var stone =parseFloat(stone_category*stone_wt/100);
    var ttl_net_wt = parseFloat(net_wt + stone);
    var ttl_stone_wt = parseFloat(stone_wt - stone);
    parseFloat($('.custom_net_wt').val(ttl_net_wt.toFixed(2)));
    parseFloat($('.custom_stone_wt').val(ttl_stone_wt.toFixed(2)));

}
function calculate_kundan_category_stock( evt) {
    evt.value = evt.value.replace(/[^0-9.]/g, '');
    var net_wt = parseFloat($('.custom_net_wt').val()) || 0;
    var kundan_pure = parseFloat($('.custom_kundan_pure').val()) || 0;
    var kundan_category = parseFloat($('.kundan_category').val()) || 0;
    var kundan =parseFloat(kundan_category*kundan_pure/100);
    var ttl_net_wt = parseFloat(net_wt + kundan);
    var ttl_kundan_pure = parseFloat(kundan_pure - kundan);
    parseFloat($('.custom_net_wt').val(ttl_net_wt.toFixed(2)));
    parseFloat($('.custom_kundan_pure').val(ttl_kundan_pure.toFixed(2)));

}
function calculate_ch_category_stock( evt) {
    evt.value = evt.value.replace(/[^0-9.]/g, '');
    var net_wt = parseFloat($('.custom_net_wt').val()) || 0;
    var checker_wt = parseFloat($('.custom_checker_wt').val()) || 0;
    var ch_category = parseFloat($('.ch_category').val()) || 0;
    var ch =parseFloat(ch_category*checker_wt/100);
    var ttl_net_wt = parseFloat(net_wt + ch);
    var ttl_checker_wt = parseFloat(checker_wt - ch);
    parseFloat($('.custom_net_wt').val(ttl_net_wt.toFixed(2)));
    parseFloat($('.custom_checker_wt').val(ttl_checker_wt.toFixed(2)));

}
</script>