<div class="content-page">
    <div class="content">
        <div class="container">
            <div class="row">
                <div class="col-sm-12 card-box-wrap">
                    <div class="panel panel-default">
                    <div class="panel-heading">
                       <h4 class="inlineBlock"><span><?= $display_title; ?></span></h4>                
                    </div>
                    <div class="panel-body">                       
                        <form name="sales_stock" id="sales_stock_form" role="form" class="form-horizontal" method="post">
             
                            <div class="col-sm-12">
                                <div class="row">  
                                   <div class="form-group col-sm-6">
                                        <label class="col-sm-4 control-label" for="inputEmail3"> Select Department <span class="asterisk">*</span>
                                        </label>
                                        <div class="col-sm-8">
                                            <select class="form-control sel_department" name="sales_stock[department_id]">
                                                <option value=''>Select Department</option>
                                                <?php foreach ($department as $d_key=> $d_value) { ?>
                                                <option value="<?=$d_value['id']?>">
                                                    <?=$d_value['name'] ?>
                                                </option>
                                                <?php } ?>
                                            </select>
                                            <span class="text-danger" id="department_id_error"></span>
                                        </div>
                                    </div>  
             <!--                   <div class="form-group col-sm-6">
                                        <label class="col-sm-4 control-label" for="inputEmail3"> Select Weight Range <span class="asterisk">*</span>
                                        </label>
                                        <div class="col-sm-8">
                                            <select class="form-control sel_department" name="sales_stock[weight_range_id]">
                                                <option value=''>Select Weight Range</option>
                                            <?php
                                          if(!empty($weight_range)){
                                            foreach ($weight_range as $key => $value) {
                                              ?>
                                              <option value="<?=$value['from_weight']."-".$value['to_weight']?>"><?=$value['from_weight']."-".$value['to_weight']?></option>
                                              <?php
                                            }
                                          }
                                      ?>
                                     </select>

                                            <span class="text-danger" id="weight_range_id_error"></span>
                                        </div>
                                    </div>  -->
                                    <div class="form-group col-sm-6">
                                        <label class="col-sm-4 control-label" for="inputEmail3"> Select  category <span class="asterisk">*</span>
                                        </label>
                                        <div class="col-sm-8">
                
                                            <select name="sales_stock[category_id]" class="form-control">
                                                <option value="">Please Select category</option>
                                                <?php 
                                                    foreach ($category as $key => $value) {
                                                        ?>
                                                        <option value="<?= $value['name'] ?>"><?= $value['name'] ?></option>
                                                        <?php
                                                    }
                                                ?>
                                            </select>
                                            <span class="text-danger" id="category_id_error"></span>
                                        </div>
                                    </div> 
                                     </div>
                                     <div class="row">  
                                                       
                                    <div class="form-group col-sm-6">
                                        <label class="col-sm-4 control-label" for="inputEmail3"> Select Product <span class="asterisk">*</span>
                                        </label>
                                        <div class="col-sm-8">
                
                                            <select name="sales_stock[parent_category_id]" class="form-control">
                                                <option value="">Please Select Product</option>
                                                <?php 
                                                    foreach ($parent_category as $key => $value) {
                                                        ?>
                                                        <option value="<?= $value['name'] ?>"><?= $value['name'] ?></option>
                                                        <?php
                                                    }
                                                ?>
                                            </select>
                                            <span class="text-danger" id="parent_category_id_error"></span>
                                        </div>
                                    </div>
                                    <div class="form-group col-sm-6">
                                        <label class="col-sm-4 control-label" for="inputEmail3"> Iteam Code<span class="asterisk">*</span>
                                        </label>
                                        <div class="col-sm-8">
                                            <input type="text" name="sales_stock[product_code]" class="form-control" placeholder="Iteam Code">
                                            <span class="text-danger" id="product_code_error"></span>
                                        </div>
                                    </div>
                                      </div>
                                     <div class="row">  
                               <!--      <div class="form-group col-sm-6">
                                        <label class="col-sm-4 control-label" for="inputEmail3"> Weight<span class="asterisk">*</span>
                                        </label>
                                        <div class="col-sm-8">
                                            <input type="text" name="sales_stock[weight]" class="form-control" placeholder="Enter Weight">
                                            <span class="text-danger" id="weight_error"></span>
                                        </div>
                                    </div> -->
                                         <div class="form-group col-sm-6">
                                        <label class="col-sm-4 control-label" for="inputEmail3"> Quantity<span class="asterisk">*</span>
                                        </label>
                                        <div class="col-sm-8">
                                            <input type="text" name="sales_stock[quantity]" class="form-control" placeholder="Enter Quantity">
                                            <span class="text-danger" id="quantity_error"></span>
                                        </div>
                                    </div>
                                               <div class="form-group col-sm-6">
                                        <label class="col-sm-4 control-label" for="inputEmail3"> Net Weight<span class="asterisk">*</span>
                                        </label>
                                        <div class="col-sm-8">
                                            <input type="text" name="sales_stock[net_wt]" class="form-control" placeholder="Enter Weight">
                                            <span class="text-danger" id="net_wt_error"></span>
                                        </div>
                                    </div>
                        
                                      </div>
                                     <div class="row">  
                         
                                     <div class="form-group col-sm-6">
                                        <label class="col-sm-4 control-label" for="inputEmail3"> Enter Few wt<span class="asterisk">*</span>
                                        </label>
                                        <div class="col-sm-8">
                                            <input type="text" min="0" placeholder="Enter Few wt" id="few_wt" class="form-control" name="sales_stock[few_wt]">
                                            <span class="text-danger" id="few_wt_error"></span>
                                        </div>
                                    </div>   

                                    <div class="form-group col-sm-6">
                                        <label class="col-sm-4 control-label" for="inputEmail3">Enter Gr Wt<span class="asterisk">*</span>
                                        </label>
                                        <div class="col-sm-8">
                                            <input type="text" placeholder="Enter Gr Wt" class="form-control" name="sales_stock[gr_wt]">
                                            <span class="text-danger" id="gr_wt_error"></span>
                                        </div>
                                     </div>
                        
                                </div>


                                    
                                
                               <!--      
                                <div class="row">
                               <div class="form-group col-sm-6">
                                        <label class="col-sm-4 control-label" for="inputEmail3"> Enter Meena Wt
                                        <span class="asterisk">*</span></label>
                                        <div class="col-sm-8">
                                            <input type="text" min="0" placeholder="Enter Meena Wt"  class="form-control" name="sales_stock[meena_wt]">
                                            <span class="text-danger" id="meena_wt_error"></span>
                                        </div>
                                </div>
                                    </div> -->

                                <div class="row">
                                    <div class="form-group col-sm-6">
                                        <label class="col-sm-4 control-label" for="inputEmail3"> Enter Wax Wt
                                        <span class="asterisk">*</span>
                                        </label>
                                        <div class="col-sm-8">
                                            <input type="text" min="0" placeholder="Enter Wax Wt" class="form-control" name="sales_stock[wax_wt]">
                                            <span class="text-danger" id="wax_wt_error"></span>
                                        </div>
                                    </div>
                                 <div class="form-group col-sm-6">
                                    <label class="col-sm-4 control-label" for="inputEmail3">kundan pcs<span class="asterisk">*</span>
                                    </label>
                                    <div class="col-sm-8">
                                        <input type="text" placeholder="Enter kundan pcs" class="form-control" name="sales_stock[kundan_pcs]">
                                        <span class="text-danger" id="kundan_pcs_error"></span>
                                    </div>
                                </div>
                                </div>

                                <div class="row">
                                 <!--    <div class="form-group col-sm-6">
                                        <label class="col-sm-4 control-label" for="inputEmail3"> Enter Moti Wt <span class="asterisk">*</span>
                                        </label>
                                        <div class="col-sm-8">
                                            <input type="text" min="0" placeholder="Enter Moti Wt" class="form-control" name="sales_stock[moti_wt]">
                                            <span class="text-danger" id="moti_wt_error"></span>
                                        </div>
                                    </div> -->
                                 <div class="form-group col-sm-6">
                                        <label class="col-sm-4 control-label" for="inputEmail3"> Enter kundan <span class="asterisk">*</span>
                                        </label>
                                        <div class="col-sm-8">
                                            <input type="text" min="0" placeholder="Enter Kun pure" id="kun_pure" class="form-control" name="sales_stock[kundan]">
                                            <span class="text-danger" id="kundan_error"></span>
                                        </div>
                                    </div>
                                    <div class="form-group col-sm-6">
                                    <label class="col-sm-4 control-label" for="inputEmail3">Enter color stone <span class="asterisk">*</span>
                                    </label>
                                    <div class="col-sm-8">
                                        <input type="text" placeholder="Enter color stone" class="form-control" name="sales_stock[color_stone]">
                                        <span class="text-danger" id="color_stone_error"></span>
                                    </div>
                                </div>
                                </div>
                                </div>
                            <div class="col-sm-12">                      
                   
                                <div class="row">
                                <div class="form-group btnSection">
                                    <button type="button" onclick="window.location='<?=ADMIN_PATH.'ready_product'?>'" class="btn btn-md btn-default waves-effect waves-light" >BACK</button>  
                                    <button type="button" class="btn btn-md btn-success waves-effect waves-light pull-right" onclick="save_sales_stock('stock');">Submit</button>
                                </div>
                                </div>
                            </div> 
                                <label class="col-sm-7 control-label" for="inputEmail3"> <span class="text-danger" id="common_error"></span>
                                </label>
                                
                                
                        
                        </form>
                        </div>
                            </div>
                    </div>
                    <div class="clearfix"></div>
                </div>
            </div>
        </div>
    </div>
</div>
