<div class="content-page">
  <div class="content">
    <div class="container">
      <div class="row">
        <div class="col-sm-12 card-box-wrap">
          <div class="card-box padmobile dash-board">
            <h5 class=" m-b-20"><!--header-title business_manager_header_title-->
            <span>DASHBOARD</span>
            </h5> 
           <!--  <h4 class="header-title business_manager_header_title m-b-30 bmsmheader bmsmheaderabm">
            <span>Orders</span>
            </h4>

            <div class="row">              
              <div class="col-md-12 col-xs-12 section-3 product-box marg_top-bottom no_pad">
                <div class="col-lg-3 col-md-3 col-sm-6">
                  <a href="<?= ADMIN_PATH.'Prepared_order';?>">
                     <div class="card-box dash-box box-shadow darkBlue_box dash_box_content text-center" data-aos="flip-up" data-aos-easing="linear" data-aos-delay="400" data-aos-duration="800">
                   
                    <div class="widget-chart-1 white_color_font">
                    
                         <h4 class="">Prepared Orders</h4>
                        <h2 class="number-size"><?=$order_prepare?></h2>
                        <span class="label label-gray">Orders</span>
            
                    </div>
                  </div>
                </a>
                </div> 
                <div class="col-lg-3 col-md-3 col-sm-6">
                  <a href="<?=ADMIN_PATH.'Prepared_order?status=sent'?>">
                     <div class="card-box dash-box box-shadow darkGreen_box dash_box_content text-center" data-aos="flip-up" data-aos-easing="linear" data-aos-delay="500" data-aos-duration="800">
                   
                    <div class="widget-chart-1 white_color_font">
                        <h4 class="">Orders Sent</h4>
                        <h2 class="number-size"><?=$order_sent?></h2>
                        <span class="label label-gray">Orders</span>
                     
                    </div>
                  </div>
                </a>
                </div> 
               
                
              </div>
            </div> -->

            <h4 class="header-title business_manager_header_title m-b-30 bmsmheader bmsmheaderabm">
            <span>Bombay</span>
            </h4>

            <div class="row">              
              <div class="col-md-12 col-xs-12 section-3 product-box marg_top-bottom no_pad">
                           <div class="col-lg-3 col-md-3 col-sm-6">
                  <a href="<?= ADMIN_PATH.'sales_stock?department=1'?>">
                    <div class="card-box dash-box box-shadow dark_purple_box dash_box_content text-center" data-aos="flip-down" data-aos-easing="linear" data-aos-delay="750" data-aos-duration="800">
                   
                    <div class="widget-chart-1 white_color_font">                     
                        <h4 class="">Stock</h4>
                        <h2 class="number-size"><?=$bom_stock?></h2>
                        <span class="label label-gray">Products</span>
                      
                    </div>
                  </div>
                </a>
                </div>
                 <div class="col-lg-3 col-md-3 col-sm-6">
                  <a href="<?= ADMIN_PATH.'Sales_recieved_product?department=1'?>">
                     <div class="card-box dash-box box-shadow pink_box dash_box_content text-center" data-aos="flip-down" data-aos-easing="linear" data-aos-delay="650" data-aos-duration="800">
                   
                    <div class="widget-chart-1 white_color_font">
                     
                         <h4 class="">Received Products from Delhi</h4>
                        <h2 class="number-size"><?=$bom_recieved_products?></h2>
                        <span class="label label-gray">Products</span>
                      
                    </div>
                  </div>
                </a>
                </div>
                <div class="col-lg-3 col-md-3 col-sm-6">
                  <a href="<?= ADMIN_PATH.'Sales_recieved_product?status=sended_to_manufacture?department=2'?>">
                   <div class="card-box dash-box box-shadow orange_box dash_box_content text-center" data-aos="flip-down" data-aos-easing="linear" data-aos-delay="750" data-aos-duration="800">
                   
                    <div class="widget-chart-1 white_color_font">
                      <div class="widget-detail-1">
                        <h4 class="">Rejected products from Delhi</h4>
                        <h2 class="number-size"><?=$bom_manufacture_send;?></h2>
                        <span class="label label-gray">Products</span>
                      </div>
                    </div>
                  </div>
                </a>
                </div>
   
          

              </div>
            </div>

 <h4 class="header-title business_manager_header_title m-b-30 bmsmheader bmsmheaderabm">
            <span>Delhi</span>
            </h4>

            <div class="row">              
              <div class="col-md-12 col-xs-12 section-3 product-box marg_top-bottom no_pad">
                           <div class="col-lg-3 col-md-3 col-sm-6">
                  <a href="<?= ADMIN_PATH.'sales_stock?department=2'?>">
                    <div class="card-box dash-box box-shadow dark_purple_box dash_box_content text-center" data-aos="flip-down" data-aos-easing="linear" data-aos-delay="750" data-aos-duration="800">
                   
                    <div class="widget-chart-1 white_color_font">                     
                        <h4 class="">Stock</h4>
                        <h2 class="number-size"><?=$delhi_stock?></h2>
                        <span class="label label-gray">Products</span>
                      
                    </div>
                  </div>
                </a>
                </div>
                 <div class="col-lg-3 col-md-3 col-sm-6">
                  <a href="<?= ADMIN_PATH.'Sales_recieved_product?department=2' ?>">
                     <div class="card-box dash-box box-shadow pink_box dash_box_content text-center" data-aos="flip-down" data-aos-easing="linear" data-aos-delay="650" data-aos-duration="800">
                   
                    <div class="widget-chart-1 white_color_font">
                     
                         <h4 class="">Received Products from Bombay</h4>
                        <h2 class="number-size"><?=$delhi_recieved_products?></h2>
                        <span class="label label-gray">Products</span>
                      
                    </div>
                  </div>
                </a>
                </div>
                <div class="col-lg-3 col-md-3 col-sm-6">
                  <a href="<?= ADMIN_PATH.'Sales_recieved_product?status=sended_to_manufacture?department=1'?>">
                   <div class="card-box dash-box box-shadow orange_box dash_box_content text-center" data-aos="flip-down" data-aos-easing="linear" data-aos-delay="750" data-aos-duration="800">
                   
                    <div class="widget-chart-1 white_color_font">
                      <div class="widget-detail-1">
                        <h4 class="">Rejected Products from Bombay</h4>
                        <h2 class="number-size"><?=$delhi_manufacture_send;?></h2>
                        <span class="label label-gray">Products</span>
                      </div>
                    </div>
                  </div>
                </a>
                </div>
   
          

              </div>
            </div>

          <!--  <h4 class="header-title business_manager_header_title m-b-30 bmsmheader bmsmheaderabm">
            <span>Invoice</span>
            </h4> -->

           <!--  <div class="row">              
              <div class="col-md-12 col-xs-12 section-3 product-box marg_top-bottom no_pad">
                 <div class="col-lg-3 col-md-3 col-sm-6">
                  <a href="<?= ADMIN_PATH.'sales_invoice' ?>">
                     <div class="card-box dash-box box-shadow purple_box dash_box_content text-center" data-aos="zoom-in" data-aos-easing="linear" data-aos-delay="850" data-aos-duration="800">
                   
                    <div class="widget-chart-1 white_color_font">
                      
                        <h4 class="">Invoice</h4>
                        <h2 class="number-size light-blue-font"><?=$invoices;?></h2>
                        <span class="label label-gray">Invoices</span>
                      
                    </div>
                  </div>
                </a>
                </div>
                <div class="col-lg-3 col-md-3 col-sm-6">
                  <a href="<?=ADMIN_PATH.'sales_invoice?status=sent'?>">
                    <div class="card-box dash-box box-shadow blue_box dash_box_content text-center" data-aos="zoom-in" data-aos-delay="1000" data-aos-duration="800">                   
                    <div class="widget-chart-1 white_color_font">
                     
                       <h4 class="">Sent to Dispatch</h4>
                        <h2 class="number-size"><?=$dispatch_invoices;?></h2>
                        <span class="label label-gray">Invoices</span>
                     
                    </div>
                  </div>
                </a>
                </div>
               

              </div>
            </div> -->
            
            </div>
              <!--===================================================-->
      </div><!-- end row -->
    </div><!-- end container -->
  </div><!-- end content -->
</div><!-- end content-page -->