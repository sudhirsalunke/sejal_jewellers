<div class="content-page">
<div class="content">
   <div class="container">
      <div class="row">
         <div class="col-sm-12 card-box-wrap">
            <div class="card-box padmobile">
               <div class="panel-heading frmHeading">
               <h4 class="inlineBlock"><span>Prepare Order</span>
               </h4>
               </div>
               <div class="col-lg-12">
                  <form name="prepare_order" id="prepare_order" role="form" class="form-horizontal" method="post">
                     <div class="form-group">
                        <label class="col-sm-4 control-label" for="inputEmail3"> Order No :</label>
                        <div class="col-sm-4">
                          <label><?=$order_id;?></label>
                        </div>
                     </div>
                     <div class="form-group">
                        <label class="col-sm-4 control-label" for="inputEmail3"> Order Name <span class="asterisk">*</span> </label>
                        <div class="col-sm-4">

                            <input value="" name="product[order_name]" class="form-control " placeholder="Enter Order Name" type="text">
                             <span class="text-danger" id="order_name_error"></span>

                        </div>
                     </div>
                     <div class="form-group">
                        <label class="col-sm-4 control-label" for="inputEmail3"> Department <span class="asterisk">*</span> </label>
                        <div class="col-sm-4 date">

                             <!-- <input value="" name="product[order_name]" class="form-control " placeholder="Enter Order Name" type="text"> -->
                             <select class="form-control" name="product[department_id]">
                              <option value="">Select Department</option>
                              <?php foreach ($department as $d_key => $d_value) { ?>
                                <option value="<?=$d_value['id']?>"><?=$d_value['name']?></option>
                             <?php } ?>
                             </select>
                             <span class="text-danger" id="department_id_error"></span>

                        </div>
                     </div>
                     <div class="form-group">
                      <div class="table-responsive">
                        <table id="tbl" class="table">
                          <thead>
                            <tr>
                              <th>Parent Category <span class="asterisk">*</span></th>
                              <th>Weight Range <span class="asterisk">*</span></th>
                              <th>Quantity<span class="asterisk">*</span></th>
                              <th></th>
                            </tr>
                          </thead> 
                              <tbody>
                                <tr id="tr0">
                                   <td> <input type="hidden" name="product[cnt][]" value="0" >
                                      <select class="form-control" name="product[parent_category][]">
                                        <option value="" id="category_0" >Select Parent Category</option>
                                        <div id="cat_html">
                                        <?php foreach ($parent_category as $c_key => $c_value) { ?>
                                            <option value="<?=$c_value['id']?>"><?=$c_value['name']?></option>
                                       <?php } ?>
                                     </div>
                                      </select>
                                      <span class="text-danger" id="parent_category_0_error"></span>
                                   </td>

                                   <td>
                                      <select class="form-control" id="weight_'0" name="product[weight][]">
                                        <option value="">Select Weight Range</option>
                                         <?php foreach ($weight_range as $w_key => $w_value) { ?>
                                            <option value="<?=$w_value['id']?>"><?=$w_value['from_weight']." - ".$w_value['to_weight']?></option>
                                       <?php } ?>
                                      </select>
                                      <span class="text-danger" id="weight_0_error"></span>
                                   </td>
                                  
                                   <td>
                                    <input type="text" placeholder="Enter Quantity" id="product_qty_0" class="form-control qty"  name="product[quantity][]">
                                    <span class="text-danger" id="quantity_0_error"></span></td>
                                   
                                   <td><a id="add_more" class="btn more-btn waves-effect waves-light m-l-5" href="javascript:void(0)" onclick="add_more_prepareproducts()" title="Add More">Add More</a></td>
                                </tr>
                           </tbody>
                        </table>
                      </div>
                           
                     </div>
                    <div class="form-group">
                     <div class="col-sm-8 texalin btnSection">
                         
                         <button class="btn btn-default waves-effect waves-light m-l-5  btn-md" onclick="window.location='<?= ADMIN_PATH?>Prepared_order'" type="reset">
                           BACK
                           </button>
                           <button id="Receive_Product_btn" class="btn btn-success waves-effect waves-light  btn-md pull-right" name="commit" type="button" onclick="save_sell_prepare_order('store'); ">
                         SAVE
                         </button>
                     </div>
                  </div>
                  <div class="col-sm-12">
                    <div class="col-lg-offset-4 errors col-lg-6">
                    </div>
                  </div>  
                  </form>
               </div>
               <div class="clearfix"></div>
            </div>
         </div>
      </div>
   </div>
</div>

<script type="text/javascript">
  var cnt=1;
  var category_array =<?=json_encode($parent_category);?>;
  var weight_range_array =<?=json_encode($weight_range);?>;
</script>
