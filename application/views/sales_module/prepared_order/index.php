<div class="content-page">
<div class="content">
   <div class="container">
      <div class="row">
         <div class="col-sm-12 card-box-wrap">
            <div class="panel panel-default">
              <div class="panel-heading">
               <h4 class="inlineBlock"><?=$display_title;?>
               </h4>
                 <?php if (@$_GET['status'] != "sent") { ?>
                  <a href="<?= ADMIN_PATH.'Prepared_order/create'?>" class="btn-wrap2 pull-right single-receive">
                     <button  type="button" class="add_senior_manager_button btn btn-primary waves-effect w-md waves-light m-b-5 btn-md pull-right single-receive">Prepare Order</button>
                  </a>
              <?php } ?>
              </div>
            
                
               
               <div class="clearfix"></div>
               <div class="panel-body">
               <div class="table-rep-plugin">
                  <div class="table-responsive b-0 scrollhidden">
               <!-- <div class="form-group col-sm-4">
                <select id="corporate_id" class="form-control search-input-select" filtercol="1">
                  <option value="">Select Corporate</option>
                  <?php
                   $all_corporates=array();
                   $all_corporates =get_corporates();
                  foreach ($all_corporates as $key => $value) { ?>
                      <option value="<?=$value['id']?>"><?=$value['name']?></option>
                 <?php } ?>
                </select>
                </div> -->
               <table class="table custdatatable table-bordered " id="sales_prepared_order_table">
                  <thead>
                  <?php
                          $data['heading']='sales_prepared_order_table';
                          $this->load->view('master/table_header',$data);
                        ?>
                    <!--  <tr>
                        <th class="col4">#</th>
                        <th class="col4">Order Id</th>
                        <th class="col4">Order Name</th>
                        <th class="col4">Department Name</th>
                        <th class="col4">Categories</th>
                        <th class="col4">Total Quantity</th>
                        <th></th>
                     </tr> -->
                  </thead>
               </table>
               </div>
               </div>
               </div>
            </div>
         </div>
      </div>
   </div>
</div>
