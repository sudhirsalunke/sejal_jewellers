<div class="content-page">
<div class="content">
   <div class="container">
      <div class="row">
         <div class="col-sm-12 card-box-wrap">
            <div class="panel panel-default clearfix">
              <div class="panel-heading frmHeading">
                <h4 class="inlineBlock"><span>Edit Order</span></h4>
               </div>
               <div class="col-lg-12">
                  <form name="prepare_order" id="prepare_order" role="form" class="form-horizontal" method="post">
                    <input type="hidden" name="order_id" value="<?=$order['id']?>">
                    <div class="row">
                       <div class="form-group col-sm-12 col-xs-12">
                        <label class="col-sm-4 col-xs-6 control-label" for="inputEmail3"> Order No :</label>
                        <div class="col-sm-4 col-xs-6">
                          <label><?=$order['id'];?></label>
                        </div>
                     </div>
                     <div class="form-group col-sm-12 col-xs-12">
                        <label class="col-sm-4 col-xs-6 control-label" for="inputEmail3"> Order Name <span class="asterisk">*</span> </label>
                        <div class="col-sm-4 col-xs-6">                         
                            <input  name="product[order_name]" class="form-control " placeholder="Enter Order Name" type="text" value="<?=$order['order_name'];?>">
                             <span class="text-danger" id="order_name_error"></span>
                        </div>
                     </div>
                     <div class="form-group col-sm-12 col-xs-12">
                        <label class="col-sm-4 col-xs-6 control-label" for="inputEmail3"> Department  <span class="asterisk">*</span></label>
                        <div class="col-sm-4 col-xs-6 date">
                             <select class="form-control col-xs-4" name="product[department_id]">
                              <option value="">Select Department</option>
                              <?php foreach ($department as $d_key => $d_value) { ?>
                                <option value="<?=$d_value['id']?>" <?=($order['department_id']==$d_value['id'])?'selected' : ''?>><?=$d_value['name']?></option>
                             <?php } ?>
                             </select>
                             <span class="text-danger" id="department_id_error"></span>

                        </div>
                     </div>
                     </div>
                     <div class="row">
                     <div class="form-group col-sm-12">
                      <div class="table-responsive">
                        <table id="tbl" class="table">
                          <thead>
                            <tr>
                              <th>Parent Category <span class="asterisk">*</span></th>
                              <th>Weight Range <span class="asterisk">*</span></th>
                              <th>Quantity<span class="asterisk">*</span></th>
                              <th></th>
                            </tr>
                          </thead> 
                              <tbody>
                                <?php foreach ($order_details as $o_key => $o_value) { ?>
                                
                                <tr id="tr<?=$o_key?>">
                                  <input type="hidden" name="product[id][]" value="<?=$o_value['id']?>">
                                  <input type="hidden" name="product[cnt][]" value="<?=$o_key?>">
                                   <td>
                                      <select class="form-control" name="product[parent_category][]">
                                        <option value="" id="category_0" >Select Parent Category</option>
                                        <div id="cat_html">
                                        <?php foreach ($parent_category as $c_key => $c_value) { ?>
                                            <option value="<?=$c_value['id']?>" <?=($o_value['parent_category_id']==$c_value['id']) ?'selected' : ''?>><?=$c_value['name']?></option>
                                       <?php } ?>
                                     </div>
                                      </select>
                                      <span class="text-danger" id="parent_category_<?=$o_key?>_error"></span>
                                   </td>

                                   <td>
                                      <select class="form-control" id="weight_'0" name="product[weight][]">
                                        <option value="">Select Weight Range</option>
                                         <?php foreach ($weight_range as $w_key => $w_value) { ?>
                                            <option value="<?=$w_value['id']?>" <?=($o_value['weight_range_id']==$w_value['id']) ?'selected' : ''?>><?=$w_value['from_weight']." - ".$w_value['to_weight']?></option>
                                       <?php } ?>
                                      </select>
                                      <span class="text-danger" id="weight_<?=$o_key?>_error"></span>
                                   </td>
                                  
                                   <td>
                                    <input type="text" placeholder="Enter Quantity" id="product_qty_0" class="form-control qty"  name="product[quantity][]" value="<?=$o_value['quantity']?>">
                                    <span class="text-danger" id="quantity_<?=$o_key?>_error"></span>
                                  </td>
                                   
                                   <td>
                                    <?php if ($o_key==0) { ?>
                                      <a id="add_more" class="btn more-btn waves-effect waves-light m-l-5" href="javascript:void(0)" onclick="add_more_prepareproducts()" title="Add More">Add More</a>
                                   <?php }else{ ?>
                                    <a href="javascript:void(0)" onclick="remove_prepare_order(<?=$o_key?>)"><i class="fa fa-times text-danger m-t-10"></i></a>
                                  <?php  } ?>
                                    </td>
                                </tr>
                                <?php } ?>
                              </tbody>
                        </table>
                      </div>                           
                     </div>
                     </div>
                    <div class="form-group">
                     <div class="col-sm-12 texalin btnSection">
                     <button class="btn btn-default waves-effect waves-light m-l-5 btn-md pull-left" onclick="window.location='<?= ADMIN_PATH?>Prepared_order'" type="reset">
                           BACK
                           </button>
                         <button id="Receive_Product_btn" class="btn btn-success waves-effect waves-light btn-md pull-right" name="commit" type="button" onclick="save_sell_prepare_order('update'); ">
                         SAVE
                         </button>                         
                     </div>
                  </div>
                  <div class="col-sm-12">
                    <div class="col-lg-offset-4 errors col-lg-6">
                    </div>
                  </div>  
                  </form>
               </div>
               <div class="clearfix"></div>
            </div>
         </div>
      </div>
   </div>
</div>

<script type="text/javascript">
  var cnt=1;
  var category_array =<?=json_encode($parent_category);?>;
  var weight_range_array =<?=json_encode($weight_range);?>;
</script>
