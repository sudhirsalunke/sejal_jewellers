<div class="content-page">
<div class="content">
   <div class="container">
      <div class="row">
         <div class="col-sm-12 card-box-wrap">
            <div class="panel panel-default clearfix">
            <div class="panel-heading frmHeading">
               <h4 class="inlineBlock"><span>View Order : <?=$order_id?></span></h4>
               </div>
               <div class="col-lg-12">
                  <form name="prepare_order" id="prepare_order" role="form" class="form-horizontal" method="post">
                    <div class="form-group">
                        <label class="col-sm-4 col-xs-6 control-label" for="inputEmail3"> Order No :</label>
                        <div class="col-sm-4 col-xs-6 date">
                          <?=$order_id?>
                        </div>
                     </div>
                     <div class="form-group">
                        <label class="col-sm-4 col-xs-6 control-label" for="inputEmail3"> Order Name :</label>
                        <div class="col-sm-4 col-xs-6 date">
                          <?=@$order_details[0]['order_name']?>
                        </div>
                     </div>
                     <div class="form-group">
                        <label class="col-sm-4 col-xs-6 control-label" for="inputEmail3"> Department :</label>
                        <div class="col-sm-4 col-xs-6 date">
                          <?=@$order_details[0]['department_name']?>
                        </div>
                     </div>
                     <div class="form-group">
                      <div class="table-responsive">
                        <table id="tbl" class="table">
                          <thead>
                            <tr>
                              <th>Parent Category </th>
                              <th>Weight Range</th>
                              <th>Quantity</th>
                              <!-- <th></th> -->
                            </tr>
                          </thead> 
                              <tbody>
                                <?php foreach ($order_details as $o_key => $o_value) { ?>
                                
                                <tr id="tr<?=$o_key?>">
                                  <td><label><?=$o_value['parent_category_name']?></label></td>
                                  <td><label style="float:right"><?=$o_value['weight_range']?></label></td> 
                                  <td><label  style="float:right"><?=$o_value['quantity']?></label></td>
                                </tr>
                                <?php } ?>
                           </tbody>
                        </table>                           
                      </div>
                     </div>
                    <div class="form-group">
                     <div class="col-sm-12 texalin btnSection">
                        <a class="btn btn-default waves-effect waves-light m-l-5  btn-md" href='javascript:history.go(-1)'>BACK</a>
                     </div>
                  </div>
                  <div class="col-sm-12">
                    <div class="col-lg-offset-4 errors col-lg-6">
                    </div>
                  </div>  
                  </form>
               </div>
               <div class="clearfix"></div>
            </div>
         </div>
      </div>
   </div>
</div>

