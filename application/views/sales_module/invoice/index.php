<div class="content-page">
<div class="content">
   <div class="container">
      <div class="row">
         <div class="col-sm-12 card-box-wrap">
            <div class="panel panel-default">
            <div class="panel-heading">
               <h4 class="inlineBlock"><?=$display_title;?>
               </h4>
             
               <?php if (@$_GET['status']!="sent") { ?>
               <div class="pull-right btn-wrap3 single-add">
                    <a href="<?=ADMIN_PATH.'sales_invoice/create'?>" class="add_senior_manager_button btn btn-purp waves-effect w-md waves-light btn-md pull-right single-add">Genrate Invoice</a>
                    </div>
                <?php } ?>
                </div>
               <div class="clearfix"></div>
            <div class="panel-body">
               <div class="table-rep-plugin">
              <div class="table-responsive b-0 scrollhidden">
              <table class="table custdatatable table-bordered " id="sales_invoice_table">
                  <thead>
                     <tr>
                        <th class="col1">#</th>
                        <th class="col2">Invoice No</th>
                        <th class="col3">No Of Products</th>
                        <th>Total Pcs</th>
                        <th>Customer Name</th>
                        <th>Color Stone</th>
                        <th>Kundan Category</th>
                        <th>Gross Weight</th>
                        <th>Total Amount</th>
                        <th>Date</th>
                        <th></th>
                     </tr>
                  </thead>
               </table>
               </div>
               </div>
               </div>
            </div>
         </div>
      </div>
   </div>
</div>
