<div class="content-page">
<div class="content">
   <div class="container">
      <div class="row">
         <div class="col-sm-12 card-box-wrap">
            <div class="panel panel-default">
            <div class="panel-heading">
               <h4 class="inlineBlock"><span><?=$display_title?></span></h4>
               </div>
               <div class="panel-body">
               <div class="col-lg-12">
                  <form name="sales_invoice" id="sales_invoice" role="form" class="form-horizontal" method="post">
                    <div class="form-group">
                      <div class="table-responsive">
                        <table class="table">
                          <tr>
                            <td><div class="col-sm-12 form-inline">
                             <div class="row">
                              <label class="col-sm-5 control-label">Transaction Date<span class="asterisk">*</span></label>
                              <input type="text" name="invoice_details[transaction_date]" class="col-sm-6 form-control datepickerInput1">
                              </div>
                             </div>
                              <span class="text-danger col-sm-offset-5 col-sm-12" id="transaction_date_error"></span>
                            </td>                        
                            <td>Invoice NO</td>
                            <td><?=$chitti_no_temp?></td>
                          </tr>
                          <tr>
                            <td><div class="col-sm-12 form-inline">
                            <div class="row">
                            <label class="col-sm-5 control-label">Customer Name<span class="asterisk">*</span></label>
                           <select class="form-control col-sm-6" name="invoice_details[customer_id]">
                              <option value="">Select Customer</option>
                               <?php foreach ($all_customers as $c_value) { ?>
                                <option value="<?=$c_value['id']?>"><?=$c_value['name']?></option>
                              <?php } ?>
                            </select>
                              <span class="text-danger col-sm-offset-5 col-sm-12" id="customer_id_error"></span>
                              </div>
                           </div>
                            </td>
                              <div class="col-sm-12 form-inline">
                                  <td><label class="label-control">Weight/Value</label></td>
                                  <td><input type="text" class="form-control col-sm-12"  name="invoice_details[weight_value]"></td>
                              </div>
                          </tr>
                          <tr>
                            <td><div class="col-sm-12 form-inline">
                            <div class="row">
                            <label class="col-sm-5 control-label">Colour Stone Category<span class="asterisk">*</span></label>
                           <select class="form-control col-sm-6" name="invoice_details[color_stone_id]" id="cs_cat_val">
                              <option value="">Select CS Category</option>
                              <?php foreach ($variations as $cs_key => $cs_value) { ?>
                                <option value="<?=$cs_value['id']?>"><?=$cs_value['name']?></option>
                              <?php } ?>
                            </select>
                              <span class="text-danger col-sm-offset-5 col-sm-12" id="color_stone_id_error"></span>
                           </div></div></td>
                           
                            <div class="col-sm-12 form-inline">
                            <div class="row">
                            <td>
                            <label class="col-sm-5">Kundan Category<span class="asterisk">*</span></label> 
                            </td><td>
                           <select class="form-control col-sm-5" name="invoice_details[kundan_category_id]" id="k_cat_val">
                              <option value="">Select Kundan Category</option>
                               <?php foreach ($variations as $ks_value) { ?>
                                <option value="<?=$ks_value['id']?>"><?=$ks_value['name']?></option>
                              <?php } ?>
                            </select>
                            <span class="text-danger col-sm-offset-5 col-sm-12" id="kundan_category_id_error"></span>
                           </div></div></td>
                          </tr>
                          <tr class="form-inline">
                            <td><label class="col-sm-5 control-label">Gold Rate </label><input type="text" name="invoice_details[gold_rate]" class="form-control"></td>
                            <td></td>
                            <td></td>
                          </tr>
                          <tr>
                            <td colspan="3"><label class="label-control">Details</label><textarea class="form-control" name="invoice_details[details]"></textarea></td>
                          </tr>
                        </table>
                      </div>
                     </div>
                     <div class="table-responsive">
                     <table id="invoice_table" class="table">
                        <thead>
                          <th><!-- Type<span class="asterisk">*</span> --></th>
                          <th>Item No<span class="asterisk">*</span></th>
                          <th>Gr Wt</th>
                          <th>CS Wt</th>
                          <th>Kun Wt</th>
                          <th>Net Wt</th>
                          <th>C/S @</th>
                          <th>C/S</th>
                          <th>Kun Pcs</th>
                          <th>Kun @</th>
                          <th>Kun</th>
                          <th>Rodo</th>
                          <th>Rodi</th>
                          <th>Other</th>
                          <th>Total</th>
                          <th></th> 
                        </thead>
                        <tbody>
                          <tr>
                            <td>
                              <!-- <select name="invoice[type][]">
                                <option value="">Select Type</option>
                                 <?php foreach ($all_types as $type_value) { ?>
                                <option value="<?=$type_value['id']?>"><?=$type_value['name']?></option>
                              <?php } ?>
                              </select> -->
                            </td>
                            <td><input type="text" class="form-control" onkeyup="autocomplete_product(this)" onfocusout="invoice_data_from_item(this)" name="invoice[item_no][]">
                              <input type="hidden" name="invoice[stock_id][]">
                            </td>
                            <td><label>0.000</label>
                            <input type="hidden" name="invoice[gross_wt][]">
                            </td>
                            <td><input type="text" class="form-control calculation_invoice" onkeyup="total_invoice_table(this)" name="invoice[cs_wt][]"></td>
                            <td><input type="text" class="form-control calculation_invoice" onkeyup="total_invoice_table(this)" name="invoice[kundan_wt][]"></td>
                            <td><label>0.000</label></td>
                            <td><input type="text" class="form-control calculation_invoice" onkeyup="calculation_invoice(this)" name="invoice[cs_a][]"></td>
                            <td><label>0.000</label></td>
                            <td><label>0.000</label>
                            <input type="hidden" name="invoice[kundan_pcs][]">
                            </td>
                            <td><input type="text" class="form-control calculation_invoice" onkeyup="calculation_invoice(this)" name="invoice[kundan_a][]"></td>
                            <td><label>0.000</label></td>
                            <td><input type="text" class="form-control calculation_invoice" onkeyup="total_invoice_table(this)" name="invoice[rodo][]"></td>
                            <td><input type="text" class="form-control calculation_invoice" onkeyup="total_invoice_table(this)" name="invoice[radi][]"></td>
                            <td><input type="text" class="form-control calculation_invoice" onkeyup="calculation_invoice(this)" name="invoice[other][]"></td>
                            <td><label>0.000</label></td>
                            <td><button id="add-new" type="button" class="btn more-btn">Add More</button>
                             </td>
                          </tr>
                          <tr id="total_tr">
                            <td><br>
                              Total
                            </td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td>
                             </td>
                          </tr>
                        </tbody>
                     </table>
                   </div>

                   <!--  <div class="form-group">
                     <div class="col-sm-offset-5 col-sm-8 texalin">
                         <button id="Receive_Product_btn" class="btn btn-primary waves-effect waves-light  btn-md" name="commit" type="button" onclick="">
                         SAVE
                         </button>
                         <button class="btn btn-default waves-effect waves-light m-l-5  btn-md" onclick="window.location='<?= ADMIN_PATH?>prepared_order'" type="reset">
                           CANCEL
                           </button>
                     </div>
                  </div> -->
                  <div class="col-sm-12">
                    <div class="col-lg-offset-2 errors col-lg-10">
                      <div class="text-danger" id="all_invoice_error"></div>
                      <p class="text-danger" id="type_error"></p>
                      <p class="text-danger" id="item_no_error"></p>
                      <p class="text-danger" id="cs_wt_error"></p>
                      <p class="text-danger" id="kundan_wt_error"></p>
                      <p class="text-danger" id="cs_amt_error"></p>
                      <p class="text-danger" id="kundan_amt_error"></p>
                      <p class="text-danger" id="rodo_error"></p>
                      <p class="text-danger" id="radi_error"></p>
                      <p class="text-danger" id="other_error"></p>
                    </div>
                  </div>
                  <div class="col-xs-12">
                  <button type="button" onclick="save_invoice('store')" class="btn btn-success btn-md col-xs-offset-5" >SUBMIT</button>
                  </div>
                  </form>
               </div>
               
            </div>
         </div>
      </div>
   </div>
</div>
