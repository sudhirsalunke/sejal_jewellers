<div class="content-page">
<div class="content">
   <div class="container">
      <div class="row">
         <div class="col-sm-12 card-box-wrap">
            <div class="panel panel-default clearfix">
            <div class="panel-heading frmHeading">            
               <h4 class="inlineBlock"><span><?=$display_title;?></span></h4>
              </div>
               <div class="clearfix"></div>

               <div class="panel-body">
                  <div class="col-sm-6">
                    <div class="col-sm-12 form-control ">
                      <label class="label-control col-sm-8">Date :</label>
                      <label><?=date_dmy($invoice['transaction_date'],'-');?></label>
                    </div>

                    <div class="col-sm-12 form-control ">
                      <label class="label-control col-sm-8 col-xs-8">Customer Name :</label>
                      <label><?=$invoice['customer_name']?></label>
                    </div>

                    <div class="col-sm-12 form-control ">
                      <span class="label-control col-sm-8 col-xs-8">Color Stone Category :</span>
                      <label><?=$invoice['color_stone_name']?></label>
                    </div>

                     <div class="col-sm-12 form-control ">
                      <span class="label-control col-sm-8 col-xs-8">Gold Rate :</span>
                      <label><?=$invoice['gold_rate']?></label>
                    </div>

                  </div>

                  <div class="col-sm-6">
                    <div class="col-sm-12 form-control ">
                      <label class="label-control col-sm-8">Chitti No :</label>
                      <label><?=$invoice['id']?></label>
                    </div>

                    <div class="col-sm-12 form-control ">
                      <label class="label-control col-sm-8">Kundan Category:</label>
                      <label><?=$invoice['kundan_category_name']?></label>
                    </div>

                     <div class="col-sm-12 form-control ">
                      <label class="label-control col-sm-8">Weight/Value</label>
                     <label><?=$invoice['weight_value']?></label>
                    </div>

                    <div class="col-sm-12 form-control ">
                      <label class="label-control col-sm-8">Details :</label>
                     <label><?=$invoice['details']?></label>
                    </div>

                  </div>
                  <div class="table-responsive">
                    <table class="table">
                    <thead>
                      <th>#</th>
                      <th>Item No</th>
                      <th>Gr Wt</th>
                      <th>CS Wt</th>
                      <th>Kun Wt</th>
                      <th>Net Wt</th>
                      <th>C/S @</th>
                      <th>C/S</th>
                      <th>Kun Pcs</th>
                      <th>Kun @</th>
                      <th>Kun</th>
                      <th>Rodo</th>
                      <th>Rodi</th>
                      <th>Other</th>
                      <th>Total</th>
                      <th></th> 
                    </thead>
                    <tbody>
                      <?php
                        $total_gr_wt=$total_cs_wt=$total_kun_wt=$total_net_wt=$total_cs_amt=$total_cs=$total_kun_pcs=$total_kun_amt=$total_kun=$total_rodo=$total_rodi=$total_other=$total=0.00;
                       foreach ($invoice_details as $key => $value) {

                          $total_gr_wt +=$value['gross_wt'];
                          $total_cs_wt +=$value['cs_wt'];
                          $total_kun_wt +=$value['kundan_wt'];
                          $total_net_wt +=$value['net_wt'];
                          $total_cs_a +=$value['cs_a'];
                          $total_cs_amt +=$value['cs_amt'];
                          $total_kun_pcs +=$value['kundan_pcs'];
                          $total_kun_a +=$value['kundan_a'];
                          $total_kun_amt +=$value['kundan_amt'];
                          $total_rodo +=$value['rodo'];
                          $total_rodi +=$value['rodi'];
                          $total_other +=$value['other'];
                          $total +=$value['total_amt'];
                        ?>
                        <tr>
                        <td><span style="float:right"><?=$key+1?></span></td>
                        <td><span style="float:right"><?=$value['product_code']?></span></td>
                        <td><span style="float:right"><?=@$value['gross_wt']?></span></td>
                        <td><span style="float:right"><?=@$value['cs_wt']?></span></td>
                        <td><span style="float:right"><?=@$value['kundan_wt']?></span></td>
                        <td><span style="float:right"><?=@$value['net_wt']?></span></td>
                        <td><span style="float:right"><?=@$value['cs_a']?></span></td>
                        <td><span style="float:right"><?=@$value['cs_amt']?></span></td>
                        <td><span style="float:right"><?=@$value['kundan_pcs']?></span></td>
                        <td><span style="float:right"><?=@$value['kundan_a']?></span></td>
                        <td><span style="float:right"><?=@$value['kundan_amt']?></span></td>
                        <td><span style="float:right"><?=$value['rodo']?></span></td>
                        <td><span style="float:right"><?=$value['rodi']?></span></td>
                        <td><span style="float:right"><?=$value['other']?></span></td>
                        <td><span style="float:right"><?=$value['total_amt']?></span></td>
                        <td></td> 
                      </tr>
                      <?php } ?>
                      <tr>
                        <td></td>
                        <td>Total</td>
                        <td><span style="float:right"><?=$total_gr_wt?></span></td>
                        <td><span style="float:right"><?=$total_cs_wt?></span></td>
                        <td><span style="float:right"><?=$total_kun_wt?></span></td>
                        <td><span style="float:right"><?=$total_net_wt?></span></td>
                        <td><span style="float:right"><?=$total_cs_a?></span></td>
                        <td><span style="float:right"><?=$total_cs_amt?></span></td>
                        <td><span style="float:right"><?=$total_kun_pcs?></span></td>
                        <td><span style="float:right"><?=$total_kun_a?></span></td>
                        <td><span style="float:right"><?=$total_kun_amt?></span></td>
                        <td><span style="float:right"><?=$total_rodo?></span></td>
                        <td><span style="float:right"><?=$total_rodi?></span></td>
                        <td><span style="float:right"><?=$total_other?></span></td>
                        <td><span style="float:right"><?=$total?></span></td>
                        
                      </tr>
                    </tbody>
                  </table>
                  </div>
                  <div class="form-group">
                  <div class="col-md-12">
                <a href="<?=ADMIN_PATH.'sales_invoice';?>" class="text-center pull-left">BACK</a>
                <a href="<?=ADMIN_PATH.'sales_invoice/print_invoice/'.$invoice['id']?>" class="text-center btn btn-info pull-right">PRINT</a> 
                </div>               
              </div>
               </div>
              
            </div>
         </div>
      </div>
   </div>
</div>
