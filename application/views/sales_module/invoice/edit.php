<div class="content-page">
<div class="content">
   <div class="container">
      <div class="row">
         <div class="col-sm-12 card-box-wrap">
            <div class="panel panel-default">
              <div class="panel-heading frmHeading">
                <h4 class="inlineBlock"><span><?=$display_title?></span></h4>
              </div>
              <div class="panel-body">
                  <form name="sales_invoice" id="sales_invoice" role="form" class="form-horizontal" method="post"><input type="hidden" value="<?=$invoice['id']?>" name="invoice_id">
                    <div class="form-group">
                      <div class="table-responsive">
                        <table class="table">
                          <tr>
                           <td><div class="form-inline">
                            <label class="col-sm-5 label-control">Transaction Date<span class="asterisk">*</span></label>
                            <input type="text" name="invoice_details[transaction_date]" value="<?=date_dmy($invoice['transaction_date'])?>" class="col-sm-offset-1 col-sm-6 form-control datepickerInput">
                               </div>
                             <span class="text-danger" id="transaction_date_error"></span>
                           </td>
                        
                           <td>Invoice NO</td>
                           <td><?=$invoice['id']?></td>
                          </tr>
                          <tr>
                            <td><div class="form-inline">
                              <label class="col-sm-5 label-control">Customer Name<span class="asterisk">*</span></label>
                               <select class="form-control col-sm-offset-1 col-sm-" name="invoice_details[customer_id]">
                                  <option value="">Select Customer</option>
                                   <?php foreach ($all_customers as $c_value) { ?>
                                    <option value="<?=$c_value['id']?>" <?=($c_value['id']==$invoice['customer_id'])?'selected' : ''?>><?=$c_value['name']?></option>
                                  <?php } ?>
                                </select>
                                  <span class="text-danger" id="customer_id_error"></span>
                               </div>
                            </td>
                              <div class="form-inline">
                                  <td><label class="label-control">Weight/Value</label></td>
                                  <td><input type="text" class="form-control col-sm-12"  name="invoice_details[weight_value]" value="<?=$invoice['weight_value']?>"></td>
                              </div>
                          </tr>
                          <tr>
                            <td><div class="form-inline">
                              <label class="col-sm-5 label-control">Colour Stone Category<span class="asterisk">*</span></label>
                             <select class="form-control col-sm-offset-1 col-sm-6" name="invoice_details[color_stone_id]">
                                <option value="">Select CS Category</option>
                                <?php foreach ($variations as $cs_key => $cs_value) { ?>
                                  <option value="<?=$cs_value['id']?>" <?=($invoice['color_stone_id']==$cs_value['id']) ? 'selected' : ''?>><?=$cs_value['name']?></option>
                                <?php } ?>
                              </select>
                                <span class="text-danger" id="color_stone_id_error"></span>
                             </div>
                            </td>
                            
                            <td><div class="form-inline">
                              <label>Kundan Category<span class="asterisk">*</span></label>
                            </td><td>
                            <select class="form-control  col-sm-6" name="invoice_details[kundan_category_id]">
                              <option value="">Select Kundan Category</option>
                               <?php foreach ($variations as $ks_value) { ?>
                                <option value="<?=$ks_value['id']?>" <?=($invoice['kundan_category_id']==$ks_value['id']) ? 'selected' : ''?>><?=$ks_value['name']?></option>
                              <?php } ?>
                            </select>
                            <span class="text-danger" id="kundan_category_id_error"></span>
                           </div></td>
                          </tr>
                           <tr class="form-inline">
                            <td><label class="col-sm-5 control-label">Gold Rate </label><input type="text" name="invoice_details[gold_rate]" class="form-control col-sm-offset-1 col-sm-6" value="<?=$invoice['gold_rate']?>"></td>
                            <td></td>
                            <td></td>
                          </tr>
                          <tr>
                            <td colspan="3"><label class="label-control col-sm-5">Details</label><textarea class="form-control" name="invoice_details[details]"><?=$invoice['details']?></textarea></td>
                          </tr>
                        </table>
                      </div>    
                    </div>
                     <div class="table-responsive">
                        <table id="invoice_table" class="table">
                          <thead>
                            <th></th>
                            <th>Item No</th>
                            <th>Gr Wt</th>
                            <th>CS Wt</th>
                            <th>Kun Wt</th>
                            <th>Net Wt</th>
                            <th>C/S @</th>
                            <th>C/S</th>
                            <th>Kun Pcs</th>
                            <th>Kun @</th>
                            <th>Kun</th>
                            <th>Rodo</th>
                            <th>Rodi</th>
                            <th>Other</th>
                            <th>Total</th>
                            <th></th> 
                          </thead>
                          <tbody>
                          <?php 
                          $total_gr_wt=$total_cs_wt=$total_kun_wt=$total_net_wt=$total_cs_amt=$total_cs=$total_kun_pcs=$total_kun_amt=$total_kun=$total_rodo=$total_rodi=$total_other=$total=0.00;
                          foreach ($invoice_details as $key => $value) { 

                            $total_gr_wt +=$value['gross_wt'];
                            $total_cs_wt +=$value['cs_wt'];
                            $total_kun_wt +=$value['kundan_wt'];
                            $total_net_wt +=$value['net_wt'];
                            $total_cs_a +=$value['cs_a'];
                            $total_cs_amt +=$value['cs_amt'];
                            $total_kun_pcs +=$value['kundan_pcs'];
                            $total_kun_a +=$value['kundan_a'];
                            $total_kun_amt +=$value['kundan_amt'];
                            $total_rodo +=$value['rodo'];
                            $total_rodi +=$value['rodi'];
                            $total_other +=$value['other'];
                            $total +=$value['total_amt'];
                            ?>
                            <tr>
                              <td><input type="hidden" value="<?=$value['id']?>" name="invoice[id][]">
                               <!--  <select name="invoice[type][]">
                                  <option value="">Select Type</option>
                                   <?php foreach ($all_types as $type_value) { ?>
                                  <option value="<?=$type_value['id']?>" <?=($value['type']==$type_value['id'])?'selected':'';?>><?=$type_value['name']?></option>
                                <?php } ?>
                                </select> -->
                              </td>
                              <td><input type="text" class="form-control" onkeyup="autocomplete_product(this)" onfocusout="invoice_data_from_item(this)" name="invoice[item_no][]" value="<?=$value['product_code']?>">
                                <input type="hidden" name="invoice[stock_id][]" value="<?=$value['stock_id']?>">
                              </td>
                              <td><label><?=$value['gross_wt']?></label></td>
                              <td><input type="text" class="form-control calculation_invoice" onkeyup="total_invoice_table()" name="invoice[cs_wt][]" value="<?=$value['cs_wt']?>"></td>
                              <td><input type="text" class="form-control calculation_invoice" onkeyup="total_invoice_table()" name="invoice[kundan_wt][]" value="<?=$value['kundan_wt']?>"></td>
                              <td><label><?=$value['net_wt']?></label></td>
                              <td><input type="text" class="form-control calculation_invoice" onkeyup="calculation_invoice(this)" name="invoice[cs_a][]" value="<?=$value['cs_a']?>"></td>
                              <td><label><?=$value['cs_amt']?></label></td>
                              <td><label><?=$value['kundan_pcs']?></label></td>
                              <td><input type="text" class="form-control calculation_invoice" onkeyup="calculation_invoice(this)" name="invoice[kundan_a][]" value="<?=$value['kundan_a']?>"></td>
                              <td><label><?=$value['kundan_amt']?></label></td>
                              <td><input type="text" class="form-control calculation_invoice" onkeyup="total_invoice_table()" name="invoice[rodo][]" value="<?=$value['rodo']?>"></td>
                              <td><input type="text" class="form-control calculation_invoice" onkeyup="total_invoice_table()" name="invoice[radi][]" value="<?=$value['rodi']?>"></td>
                              <td><input type="text" class="form-control calculation_invoice" onkeyup="calculation_invoice(this)" name="invoice[other][]" value="<?=$value['other']?>"></td>
                              <td><label><?=$value['total_amt'];?></label></td>
                              <td><?php if ($key==0) { ?>
                                <button id="add-new" type="button" class="btn more-btn btn-sm">Add More</button>
                              <?php }else{ ?>
                                <button type="button" onclick="remove_row_invoice(this)" class="btn btn-sm  btn-danger">Remove</button>
                              <?php } ?>
                                
                               </td>
                            </tr>
                            <?php  } ?>
                            <tr id="total_tr">
                              <td><br>
                                Total
                              </td>
                              <td></td>
                              <td><?=$total_gr_wt?></td>
                              <td><?=$total_cs_wt?></td>
                              <td><?=$total_kun_wt?></td>
                              <td><?=$total_net_wt?></td>
                              <td><?=$total_cs_a?></td>
                              <td><?=$total_cs_amt?></td>
                              <td><?=$total_kun_pcs?></td>
                              <td><?=$total_kun_a?></td>
                              <td><?=$total_kun_amt?></td>
                              <td><?=$total_rodo?></td>
                              <td><?=$total_rodi?></td>
                              <td><?=$total_other?></td>
                              <td><?=$total?></td>
                           
                            </tr>
                          </tbody>
                        </table>
                      </div>

                   <!--  <div class="form-group">
                     <div class="col-sm-offset-5 col-sm-8 texalin">
                         <button id="Receive_Product_btn" class="btn btn-primary waves-effect waves-light  btn-md" name="commit" type="button" onclick="">
                         SAVE
                         </button>
                         <button class="btn btn-default waves-effect waves-light m-l-5  btn-md" onclick="window.location='<?= ADMIN_PATH?>prepared_order'" type="reset">
                           CANCEL
                           </button>
                     </div>
                  </div> -->
                  <div class="col-sm-12">
                    <div class="col-lg-offset-2 errors col-lg-10">
                      <div class="text-danger" id="all_invoice_error"></div>
                      <p class="text-danger" id="type_error"></p>
                      <p class="text-danger" id="item_no_error"></p>
                      <p class="text-danger" id="cs_wt_error"></p>
                      <p class="text-danger" id="kundan_wt_error"></p>
                      <p class="text-danger" id="cs_amt_error"></p>
                      <p class="text-danger" id="kundan_amt_error"></p>
                      <p class="text-danger" id="rodo_error"></p>
                      <p class="text-danger" id="radi_error"></p>
                      <p class="text-danger" id="other_error"></p>
                    </div>
                  </div>  
                  <div class="col-xs-12">
                    <button type="button" onclick="save_invoice('update')" class="btn btn-success btn-md col-xs-offset-5">SUBMIT</button>
                  </div>
                </form>
               </div>
               <div class="clearfix"></div>
            </div>
         </div>
      </div>
   </div>
</div>
