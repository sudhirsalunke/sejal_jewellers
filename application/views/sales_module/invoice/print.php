<!DOCTYPE html>
<html>

<head>
    <meta name="google" content="notranslate" />
    <style>
     @page {
    size: auto;   /* auto is the initial value */
    margin: 0;  /* this affects the margin in the printer settings */
}
  </style>
</head>

<body onload="window.print()">

    <div style="width: 90%; margin: 0 auto;">
        <h2 style="text-align: center;">Invoice No.<?=$invoice['id']?></h2>
          <table class="table">
            <thead>
                <tr>
                  <th style=" text-align: left; border: 1px solid black; padding: 6px; width: 100px;" colspan="2">Date</th>
                  <th style=" text-align: left; border: 1px solid black; padding: 6px; width: 100px;" colspan="6"><?=date_dmy($invoice['transaction_date'],'-');?></th>
                 
                  <th style=" text-align: left; border: 1px solid black; padding: 6px; width: 100px;" colspan="2">Chitti No</th>
                  <th style=" text-align: left; border: 1px solid black; padding: 6px; width: 100px;" colspan="6"><?=$invoice['id']?></th> 
                </tr>
                 <tr>
                  <th style=" text-align: left; border: 1px solid black; padding: 6px; width: 100px;" colspan="2">Customer Name</th>
                  <th style=" text-align: left; border: 1px solid black; padding: 6px; width: 100px;" colspan="6"><?=$invoice['customer_name']?></th>
                 
                  <th style=" text-align: left; border: 1px solid black; padding: 6px; width: 100px;" colspan="2">Kundan Category</th>
                  <th style=" text-align: left; border: 1px solid black; padding: 6px; width: 100px;" colspan="6"><?=$invoice['kundan_category_name']?></th> 
                </tr>
                 <tr>
                  <th style=" text-align: left; border: 1px solid black; padding: 6px; width: 100px;" colspan="2">Gold Rate</th>
                  <th style=" text-align: left; border: 1px solid black; padding: 6px; width: 100px;" colspan="6"><?=$invoice['gold_rate']?></th>
                 
                  <th style=" text-align: left; border: 1px solid black; padding: 6px; width: 100px;" colspan="2">Weight/Value</th>
                  <th style=" text-align: left; border: 1px solid black; padding: 6px; width: 100px;" colspan="6"><?=$invoice['weight_value']?></th> 
                </tr>
                 <tr>
                  <th style=" text-align: left; border: 1px solid black; padding: 6px; width: 100px;" colspan="2">Color Stone Category</th>
                  <th style=" text-align: left; border: 1px solid black; padding: 6px; width: 100px;" colspan="6"><?=$invoice['color_stone_name']?></th>
                 
                  <th style=" text-align: left; border: 1px solid black; padding: 6px; width: 100px;" colspan="2">Details</th>
                  <th style=" text-align: left; border: 1px solid black; padding: 6px; width: 100px;" colspan="6"><?=$invoice['details']?></th> 
                </tr>
                <tr>
                  <th style=" text-align: left; border: 1px solid black; padding: 6px; width: 100px;">#</th>
                 
                  <th style=" text-align: left; border: 1px solid black; padding: 6px; width: 100px;">Item No</th>
                  <th style=" text-align: left; border: 1px solid black; padding: 6px; width: 100px;">Gr Wt</th>
                  <th style=" text-align: left; border: 1px solid black; padding: 6px; width: 100px;">CS Wt</th>
                  <th style=" text-align: left; border: 1px solid black; padding: 6px; width: 100px;">Kun Wt</th>
                  <th style=" text-align: left; border: 1px solid black; padding: 6px; width: 100px;">Net Wt</th>
                  <th style=" text-align: left; border: 1px solid black; padding: 6px; width: 100px;">C/S @</th>
                  <th style=" text-align: left; border: 1px solid black; padding: 6px; width: 100px;">C/S</th>
                  <th style=" text-align: left; border: 1px solid black; padding: 6px; width: 100px;">Kun Pcs</th>
                  <th style=" text-align: left; border: 1px solid black; padding: 6px; width: 100px;">Kun @</th>
                  <th style=" text-align: left; border: 1px solid black; padding: 6px; width: 100px;">Kun</th>
                  <th style=" text-align: left; border: 1px solid black; padding: 6px; width: 100px;">Rodo</th>
                  <th style=" text-align: left; border: 1px solid black; padding: 6px; width: 100px;">Rodi</th>
                  <th style=" text-align: left; border: 1px solid black; padding: 6px; width: 100px;">Other</th>
                  <th style=" text-align: left; border: 1px solid black; padding: 6px; width: 100px;">Total</th>
                  </tr>
            </thead>
            <tbody>
                      <?php
                        $total_gr_wt=$total_cs_wt=$total_kun_wt=$total_net_wt=$total_cs_amt=$total_cs=$total_kun_pcs=$total_kun_amt=$total_kun=$total_rodo=$total_rodi=$total_other=$total=0.00;
                       foreach ($invoice_details as $key => $value) { 
                          $row_total=$value['cs']+$value['kun']+$value['other'];

                          $total_gr_wt +=$value['gross_wt'];
                          $total_cs_wt +=$value['cs_wt'];
                          $total_kun_wt +=$value['kundan_wt'];
                          $total_net_wt +=$value['net_wt'];
                          $total_cs_a +=$value['cs_a'];
                          $total_cs_amt +=$value['cs_amt'];
                          $total_kun_pcs +=$value['kundan_pcs'];
                          $total_kun_a +=$value['kundan_a'];
                          $total_kun_amt +=$value['kundan_amt'];
                          $total_rodo +=$value['rodo'];
                          $total_rodi +=$value['rodi'];
                          $total_other +=$value['other'];
                          $total +=$value['total_amt'];
                        ?>
                        <tr>
                        <td style=" text-align: left; border: 1px solid black; padding: 6px; width: 100px;"><?=$key+1?></td>
                        <td style=" text-align: left; border: 1px solid black; padding: 6px; width: 100px;"><?=$value['product_code']?></td>
                        <td style=" text-align: left; border: 1px solid black; padding: 6px; width: 100px;"><?=@$value['gross_wt']?></td>
                        <td style=" text-align: left; border: 1px solid black; padding: 6px; width: 100px;"><?=@$value['cs_wt']?></td>
                        <td style=" text-align: left; border: 1px solid black; padding: 6px; width: 100px;"><?=@$value['kundan_wt']?></td>
                        <td style=" text-align: left; border: 1px solid black; padding: 6px; width: 100px;"><?=@$value['net_wt']?></td>
                        <td style=" text-align: left; border: 1px solid black; padding: 6px; width: 100px;"><?=@$value['cs_a']?></td>
                        <td style=" text-align: left; border: 1px solid black; padding: 6px; width: 100px;"><?=@$value['cs_amt']?></td>
                        <td style=" text-align: left; border: 1px solid black; padding: 6px; width: 100px;"><?=@$value['kundan_pcs']?></td>
                        <td style=" text-align: left; border: 1px solid black; padding: 6px; width: 100px;"><?=@$value['kundan_a']?></td>
                        <td style=" text-align: left; border: 1px solid black; padding: 6px; width: 100px;"><?=@$value['kundan_a']?></td>
                        <td style=" text-align: left; border: 1px solid black; padding: 6px; width: 100px;"><?=$value['rodo']?></td>
                        <td style=" text-align: left; border: 1px solid black; padding: 6px; width: 100px;"><?=$value['rodi']?></td>
                        <td style=" text-align: left; border: 1px solid black; padding: 6px; width: 100px;"><?=$value['other']?></td>
                        <td style=" text-align: left; border: 1px solid black; padding: 6px; width: 100px;"><?=$value['total_amt']?></td>
                       
                      </tr>
                      <?php } ?>
                      <tr>
                        <td style=" text-align: left; border: 1px solid black; padding: 6px; width: 100px;"></td>
                        <td style=" text-align: left; border: 1px solid black; padding: 6px; width: 100px;">Total</td>
                       
                        <td style=" text-align: left; border: 1px solid black; padding: 6px; width: 100px;"><?=$total_gr_wt?></td>
                        <td style=" text-align: left; border: 1px solid black; padding: 6px; width: 100px;"><?=$total_cs_wt?></td>
                        <td style=" text-align: left; border: 1px solid black; padding: 6px; width: 100px;"><?=$total_kun_wt?></td>
                        <td style=" text-align: left; border: 1px solid black; padding: 6px; width: 100px;"><?=$total_net_wt?></td>
                        <td style=" text-align: left; border: 1px solid black; padding: 6px; width: 100px;"><?=$total_cs_a?></td>
                        <td style=" text-align: left; border: 1px solid black; padding: 6px; width: 100px;"><?=$total_cs_amt?></td>
                        <td style=" text-align: left; border: 1px solid black; padding: 6px; width: 100px;"><?=$total_kun_pcs?></td>
                        <td style=" text-align: left; border: 1px solid black; padding: 6px; width: 100px;"><?=$total_kun_a?></td>
                        <td style=" text-align: left; border: 1px solid black; padding: 6px; width: 100px;"><?=$total_kun_amt?></td>
                        <td style=" text-align: left; border: 1px solid black; padding: 6px; width: 100px;"><?=$total_rodo?></td>
                        <td style=" text-align: left; border: 1px solid black; padding: 6px; width: 100px;"><?=$total_rodi?></td>
                        <td style=" text-align: left; border: 1px solid black; padding: 6px; width: 100px;"><?=$total_other?></td>
                        <td style=" text-align: left; border: 1px solid black; padding: 6px; width: 100px;"><?=$total?></td>
                       
                        
                      </tr>
                    </tbody>
                  </table>

    </div>