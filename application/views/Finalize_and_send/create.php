<div class="content-page">
<div class="content">
   <div class="container">
      <div class="row">
         <div class="col-sm-6 card-box-wrap">
            <div class="panel panel-default">
               <div class="panel-heading frmHeading">
                  <h4 class="inlineBlock"><span>Finalize and Send</span></h4>                  
               </div>
               <div class="panel-body">
                  <div class="col-lg-12">
                  <form name="finalize" id="finalize" role="form" class="form-horizontal" method="post">
                     <input type="hidden" name="category" value="<?=@$_GET['category']?>">
                     <input type="hidden" name="weight_range" value="<?=@$_GET['weight_range']?>">
                     <input type="hidden" name="product_code" value="<?=@$_GET['product_code']?>">
                     <input type="hidden" name="karigar" value="<?=@$_GET['karigar']?>">
                     <input type="hidden" name="corporate" value="<?=@$_GET['corporate']?>">
                   <div class="form-group">
                        <label class="col-sm-4 control-label" for="inputEmail3"> Document No <span class="asterisk"></span></label>
                        <div class="col-sm-8">
                        <input type="hidden" name="order_id" value="<?= $order_id?>">
                           <input type="text" placeholder="" id="name" class="form-control" name="order_id" value="<?= $order_id?>" disabled>
                           <span class="text-danger" id="name_error"></span>
                        </div>
                     </div>
                     <div class="form-group">
                        <label class="col-sm-4 control-label" for="inputEmail3"> Date <span class="asterisk"></span></label>
                        <div class="col-sm-8">
                           <input type="text" placeholder="" id="name" class="form-control" name="date" disabled value="<?=date('d-m-Y');?>">
                           <span class="text-danger" id="name_error"></span>
                        </div>
                     </div>
                     <div class="form-group">
                        <label class="col-sm-4 control-label" for="inputEmail3"> Corporate <span class="asterisk"></span></label>
                        <div class="col-sm-8">
                           <input type="hidden" name="corporate" value="<?=$product_corporate['corporate']?>">
                           <select disabled="" name="corporate" class="form-control">
                           <?php
                              foreach ($corporates as $key => $value) {
                                 ?>
                                     <option <?=(@$_GET['corporate']==$value['id']) ? 'selected' : '' ?> value="<?= $value['id']?>"><?= $value['name']?></option>
                                 <?php 
                              }
                            ?>
                             
                           </select>
                           <span class="text-danger" id="code_error"></span>
                        </div>
                     </div>
                     <div class="form-group">
                        <label class="col-sm-4 control-label" for="inputEmail3"> Comments <span class="asterisk"></span></label>
                        <div class="col-sm-8">
                           <textarea name="comment" class="form-control"></textarea>
                           <span class="text-danger" id="code_error"></span>
                        </div>
                     </div>
                     <div class="form-group">
                        <label class="col-sm-4 control-label" for="inputEmail3">No of Design <span class="asterisk"></span></label>
                        <div class="col-sm-8">
                           <input type="text" placeholder="" id="code" class="form-control" name="Sub_category[code]" required="" value="<?=count($shortlisted_products)?>" disabled>
                           <span class="text-danger" id="code_error"></span>
                        </div>

                     </div>                    
                  <div class="btnSection">                     
                        <a href="<?=ADMIN_PATH?>Finalize_and_send/export_shortlisted_images/<?=$order_id?>" class="btn btn-primary waves-effect waves-light btn-md"> Export Images</a>                        
                        <a href="javascript:void(0)" class="btn btn-success waves-effect waves-light btn-md pull-right" onclick="Finalize_and_send()">
                            Export Design
                          </a>

                  </div>
                  </form>
               </div>
               </div>
               <div class="clearfix"></div>
            </div>
         </div>
      </div>
   </div>
</div>