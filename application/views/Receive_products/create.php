<div class="content-page">
   <div class="content">
      <div class="container">
        <div class="row">
         <div class="col-sm-12 card-box-wrap">
           
              <div class="panel panel-default">
               <div class="panel-heading frmHeading">
               <h4 class="inlineBlock"><span>Receive Products</span>
               </h4>
               </div>
              
              
                    <input type="hidden" id="code_name" >
                    <input type="hidden" id="karigar_name" >
                  <form id="ReceiveProductsForm" role="form" class="form-horizontal" method="post">
                            <div class="col-lg-12">
                                <div class="form-group">
                                    <label class="col-sm-2 control-label text-right" for="inputEmail3"> Select Karigar<span class="asterisk">*</span></label>
                                    <div class="col-sm-2">
                                        <select class="form-control" name="karigar_id" onchange="get_karigar_engaged_products(this.value)" id="kariger_id">
                                            <option value=''>Select Karigar</option>
                                            <?php 
                                            if(!empty($karigar_list)){
                                                foreach ($karigar_list as $key => $value) {
                                                    ?>
                                                    <option value="<?= $value['id']?>"><?= $value['name']?></option>
                                                    <?php
                                                  }                                           
                                            }  
                                            ?>
                                        </select>
                                        <span class="text-danger" id="karigar_id_error"></span>
                                    </div>
                                    <label class="col-sm-2 control-label text-right" for="inputEmail3">Select Orders<span class="asterisk">*</span></label>
                                    <div class="col-sm-2" id="create_orders">
                                         <select class="form-control product_name" name="sub_category_id" id="parent_category_id">
                                            <option value=''>Please Select Order</option>
                                        </select>
                                        <span class="text-danger" id="sub_category_id_error"></span>
                                    </div>
                                    <label class="col-sm-2 control-label text-right" for="inputEmail3">Barcode<span class="asterisk">*</span></label>
                                    <div class="col-sm-2" id="create_orders">
                                         <input type="text" class="form-control product_name" name="barcode_id" id="barcode_id" onchange="get_engaged_products('','append_data')">
                                        <span class="text-danger" id="barcode_id_error"></span>
                                    </div>
                                    
                                </div>
                            </div>
                            
                        <div class="products" style="display: none">
                            <h4 class="header-title business_manager_header_title m-t-0 m-b-30 bmsmheader bmsmheaderabm"><span>Products</span></h4>
                            <div class="form-group">
                            <div class="col-sm-3">
                            <input type="text" class="form-control prodcut_code" name="prodcut_code" id="prodcut_code" placeholder="Enter prodcut code" oninput="get_engaged_products();">
                            </div>                                    
                                    <div class="col-sm-3" id="create_orders">
                                         <select class="form-control product_name" name="category_name" id="category_name" onchange="get_engaged_products();">
                                            <option value=''>Select Category</option>
                                              <?php 
                                            if(!empty($category_list)){
                                                foreach ($category_list as $key => $value) {
                                                    ?>
                                                    <option value="<?= $value['name']?>"><?= $value['name']?></option>
                                                    <?php
                                                  }                                           
                                            }  
                                            ?>
                                        </select>
                                    </div>
                                      
                                      <div class="col-sm-3" id="create_orders">
                                         <select class="form-control product_name" name="article_id" id="article_id" onchange="get_engaged_products();">
                                            <option value=''>Select Article</option>
                                              <?php 
                                            if(!empty($article_list)){
                                                foreach ($article_list as $key => $value) {
                                                    ?>
                                                    <option value="<?= $value['id']?>"><?= $value['name']?></option>
                                                    <?php
                                                  }                                           
                                            }  
                                            ?>
                                        </select>
                                    </div>
                                
                                <!--      <div class="col-sm-2" id="create_orders">
                                           <button class="btn btn-primary small  btn-md" name="Search" type="button" onclick="get_engaged_products();">Search</button>
                                           </div> -->
                                </div>
 </div>

                            <div class="table-responsive table-rep-plugin">
                               
                                <div class="b-0" style="overflow-x:scroll;">
                                    <table class="table table-bordered keycontrol" id="tag_products_tbl">
                                        <thead>
                                            <tr>
                                               <!--  <th class="col4">
                                                    <div class="checkbox checkbox-purpal">
                                                        <input value="0" checked="" type="checkbox" id="check_all_1" class="id_chek"><label for="check_all_1"></label>
                                                    </div>
                                                </th> -->
                                                <th class="col4">BARCODE</th>
                                                <th class="col4">ORDER ID</th>
                                                <th class="col4">KARIGAR ID</th>
                                                <th class="col4">PRODUCT CODE</th>
                                                <th class="col4">LINE #</th>
                                                <th class="col4">CATEGORY</th>
                                                <th class="col4">SUB CATEGORY</th>
                                                <th class="col4">QTY</th>
                                                <th class="col3">SIZE</th>
                                                <th class="col3">GROSS WT</th>
                                                <th class="col3">LESS WT</th>
                                                <th class="col3">NET WT</th>
                                                <!-- <th class="col3">STONE WT</th> -->
                                                <th class="col3"><div class="checkbox checkbox-purpal"><input value="0" checked="" type="checkbox" id="check_all_2"><label for="check_all_2">QC Pass?</label>
                                                    </div></th>
                                                <th class="col3"></th>
                                                <!-- <th></th> -->
                                            </tr>
                                        </thead>
                                           <tbody id="appenddata"> 

                                          </tbody>
                                        <tbody id="trdata"> 
                                            <tr>
                                                  <td colspan="5"  id="first_tr">No Data found</td>
                                            </tr>
                                        </tbody>
                                     
                                        <input type = "hidden" name="barcode_append_id" id="barcode_append_id" value="">
                                    </table>
                                </div>
                            </div>
                         
                             </form>
                            <div class="col-lg-12">
                                <div class="form-group">
                                    <div class="col-sm-offset-5 col-sm-8 texalin">
                                        
                                        <input type="hidden" name="selected_ids" id="tag_products_added">
                                        <span id="error_pdf" class="text-danger"></span><br>
                                        <button class="btn btn-success waves-effect waves-light  btn-md float_btn" name="commit" type="button" onclick="save_RcvproductFrmKarigar(this);">Save</button>
                                        
                                         
                                    
                            </div>
                         </div>
                          </div>                  
                   
               </div>
               
            </div>
         </div>
      </div>
   </div>









  