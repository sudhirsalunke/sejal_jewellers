  <div class="content-page">
    <div class="content">
      <div class="container">
        <div class="row">
          <div class="col-sm-12 card-box-wrap">
            <form enctype='multipart/form-data' role="form" name="product_form" id="export">
              <div class="panel panel-default">              
                <div class="panel-heading">
                 <h4 class="inlineBlock"><span><?=$display_title;?></span></h4>              
                   <div class="pull-right btn-wrap">
                  <?php if($display_title =='all received  products from karigar'){ ?>
                   <a href="<?= ADMIN_PATH.'Receive_products/create' ?>" class="pull-right btn-wrap">
                     <button  type="button" class="add_senior_manager_button btn btn-receive waves-effect w-md waves-light m-b-5 btn-md pull-right">Receive Product</button>
                  </a>
                   <?php } ?>
                   
                  <?php if(!empty($_GET['status'])&& $_GET['status'] == 'pending'){?>
                  
                   <a onclick="Pending_product_for_qc_amend_remark('Reject')" ><button  type="button" class="add_senior_manager_button btn btn-danger waves-effect w-md waves-light m-b-5  btn-md pull-right" id="rejectd_btn">All Reject</button></a>

                   <a onclick="Pending_product_for_qc_amend_remark('Amend')" ><button  type="button" class="add_senior_manager_button btn btn-info waves-effect w-md waves-light m-b-5  btn-md pull-right" id="amended_btn">All Amend</button></a>       
                 <?php } ?>
                  </div>
                </div>
                <div class="panel-body">
                  <div class="table-rep-plugin">              
                    <div class="table-responsive b-0 scrollhidden">
                     <table class="table custdatatable table-bordered " id="Receive_products">
                        <thead>
                              <?php
                                $data['heading']='Receive_products';
                                $this->load->view('master/table_header',$data);
                              ?>
                              <!--      <tr>                       
                              <th class="col4"><?=(!empty($_GET['status'])&& $_GET['status'] == 'pending') ? '<div class="checkbox checkbox-purpal"><input value="0" type="checkbox" id="check_all"><label for="check_all"></label> </div>' : '#' ?></th>
                              <th class="col4">Product Code</th>
                              <th class="col4">Order Id</th>
                              <th class="col4">Line #</th>
                              <th class="col4">Corporate</th>
                              <th class="col3">Category</th>
                              <th class="col3">Sub Category</th>
                              <th>Date</th>
                              <th>Quantity</th>
                              <th>Size</th>
                              <th></th>                        
                            </tr> -->
                        </thead>
                      </table>
                    </div>
                  </div>
                </div>              
              </div> 
            </form>   
          </div>
        </div>
      </div>
    </div>
  </div>

