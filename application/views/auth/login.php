<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta name="description" content="A fully featured admin theme which can be used to build CRM, CMS, etc.">
        <meta name="author" content="Coderthemes">

        <!-- App Favicon -->
        <!-- <link rel="shortcut icon" href="<?=ADMIN_IMAGES_PATH ?>favicon.ico"> -->

        <!-- App title -->
        <title>Faber Lounge- <?=@$page_title ?></title>

        <!-- App CSS -->
        <link href="<?=ADMIN_CSS_PATH ?>bootstrap.min.css" rel="stylesheet" type="text/css" />
        <link href="<?=ADMIN_CSS_PATH ?>core.css" rel="stylesheet" type="text/css" />
        <link href="<?=ADMIN_CSS_PATH ?>components.css" rel="stylesheet" type="text/css" />
        <link href="<?=ADMIN_CSS_PATH ?>icons.css" rel="stylesheet" type="text/css" />
        <link href="<?=ADMIN_CSS_PATH ?>pages.css?<?= time();?>" rel="stylesheet" type="text/css" />
        <link href="<?=ADMIN_CSS_PATH ?>menu.css" rel="stylesheet" type="text/css" />
        <link href="<?=ADMIN_CSS_PATH ?>responsive.css" rel="stylesheet" type="text/css" />

        <!-- HTML5 Shiv and Respond.js IE8 support of HTML5 elements and media queries -->
        <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
        <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
        <script src="https://oss.maxcdn.com/libs/respond.js/1.3.0/respond.min.js"></script>
        <![endif]-->

        <script src="<?=ADMIN_JS_PATH ?>modernizr.min.js"></script>
        
    </head>
    <body class="loginPage">

        <div class="account-pages"></div>
        <div class="clearfix"></div>
        <div class="wrapper-page">
            <div class="text-center">
                <!-- <a href="index.html" class="logo"><span><img src="<?=ADMIN_IMAGES_PATH?>logo.png" /></span></a> -->
                <h5 class="text-muted m-t-0 font-600"></h5>
            </div>
        	<div class="card-box">
                <div class="text-center">
                    <h4 class="text-uppercase font-bold m-b-0 text-left">Sign In</h4>
                </div>
                <div class="panel-body">
                    <form class="form-horizontal m-t-20" name="form_login" id="form_login" action="<?=ADMIN_PATH;?>auth" method="post">
                        <h4 id="error_txt"></h4>
                        <div class="form-group ">
                            <div class="col-xs-12">
                                <input class="form-control" type="text" name="username" id="username" value="<?php echo set_value('username'); ?>" placeholder="Email ID">
                                <span class="text-danger"><?php echo form_error('username'); ?></span>
                            </div>
                        </div>

                        <div class="form-group">
                            <div class="col-xs-12">
                                <input class="form-control" autocomplete="false" type="password" placeholder="Password" name="password" id="password">
                                <span class="text-danger"><?php echo form_error('password'); ?></span>
                            </div>
                        </div>
                       <!--  <div class="form-group row">
                           <div class="col-md-12">
                               <div class="checkbox checkbox-info pull-left p-t-0">
                                   <input id="checkbox-signup" type="checkbox" class="filled-in chk-col-light-blue">
                                   <label for="checkbox-signup"> Remember me </label>
                               </div> </div>
                       </div> -->

                        <div class="form-group text-center m-t-30">
                            <div class="col-xs-12">
                                <button class="btn btn-primary  waves-effect waves-light btn-md" name='submit' type='submit' onclick="check_popup()" value="SIGN IN">LOG  IN</button>
                            </div>
                        </div>
                        <!-- <div class="form-group m-b-0">
                            <div class="col-sm-12 text-center">
                                <a href="javascript:void(0)" id="to-recover" class="pull-right"><i class="fa fa-lock m-r-5"></i> <label>Forgot Password?</label></a>
                            </div>
                        </div> -->
                    </form>

                </div>
            </div>
            <!-- end card-box-->
            
        </div>
        <!-- end wrapper page -->
        

        
    	<script>
            var resizefunc = [];
        </script>

        <!-- jQuery  -->
        <script src="<?=ADMIN_JS_PATH ?>jquery.min.js"></script>
        <script src="<?=ADMIN_JS_PATH ?>bootstrap.min.js"></script>
        <script src="<?=ADMIN_JS_PATH ?>detect.js"></script>
        <script src="<?=ADMIN_JS_PATH ?>fastclick.js"></script>
        <script src="<?=ADMIN_JS_PATH ?>jquery.slimscroll.js"></script>
        <script src="<?=ADMIN_JS_PATH ?>jquery.blockUI.js"></script>
        <script src="<?=ADMIN_JS_PATH ?>waves.js"></script>
        <script src="<?=ADMIN_JS_PATH ?>wow.min.js"></script>
        <script src="<?=ADMIN_JS_PATH ?>jquery.nicescroll.js"></script>
        <script src="<?=ADMIN_JS_PATH ?>jquery.scrollTo.min.js"></script>

        <!-- App js -->
        <script src="<?=ADMIN_JS_PATH ?>jquery.core.js"></script>
        <script src="<?=ADMIN_JS_PATH ?>jquery.app.js"></script>
     <script>
    //     function check_popup () {
    //          var windowName = 'userConsole'; 
    //       var popUp = window.open('', windowName, 'width=0px, height=700, left=24, top=24, scrollbars, resizable');
    //         if (popUp == null || typeof(popUp)=='undefined') {  
    //             var txt = 'POP-UP is Blocked in your browser.Please allow POP-UP in your browser to Login In Shilpi'; 
    //         }else{  
    //         var txt="";
    //           popUp.close();
    //           $('#form_login').submit();
    //       }
    //       $('#error_txt').html(txt);
    //     }
    //     </script>	
	</body>
</html>
