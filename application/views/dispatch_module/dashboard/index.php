<div class="content-page">
  <div class="content">
    <div class="container">
      <div class="row">
        <div class="col-sm-12 card-box-wrap">
          <div class="card-box padmobile dash-board">
            <h5 class=" m-b-20"><!--header-title business_manager_header_title-->
            <span>DASHBOARD</span>
            </h5>     
            <h4 class="header-title business_manager_header_title m-b-30 bmsmheader bmsmheaderabm">
            <span>Invoices</span>
            </h4>

            <div class="row">              
              <div class="col-md-12 col-xs-12 section-3 product-box marg_top-bottom no_pad">
                <div class="col-lg-3 col-md-3 col-sm-6">
                  <a href="<?= ADMIN_PATH.'Dispatch_invoices';?>">
                   <div class="card-box dash-box box-shadow pink_box dash_box_content text-center" data-aos="zoom-in-left" data-aos-easing="linear" data-aos-delay="500" data-aos-duration="800">
                    
                    <div class="widget-chart-1 white_color_font">                     
                        <h4 class="">Received From Sale</h4>
                        <h2 class="number-size"><?=$recived_invoice?></h2>
                        <span class="label label-gray">Invoices</span>                      
                    </div>
                  </div>
                </a>
                </div><!-- end col -->
                <div class="col-lg-3 col-md-3 col-sm-6">
                  <a href="<?=ADMIN_PATH.'Dispatch_invoices?status=approved'?>">
                    <div class="card-box dash-box box-shadow darkGreen_box dash_box_content text-center" data-aos="zoom-in-left" data-aos-easing="linear" data-aos-delay="650" data-aos-duration="800">
                    
                    <div class="widget-chart-1 white_color_font">                      
                        <h4 class="">Approved Invoices</h4>
                        <h2 class="number-size"><?=$approved_invoice?></h2>
                        <span class="label label-gray">Invoices</span>                      
                    </div>
                  </div>
                </a>
                </div><!-- end col -->

                 <div class="col-lg-3 col-md-3 col-sm-6">
                  <a href="<?=ADMIN_PATH.'Dispatch_invoices?status=rejected'?>">
                   <div class="card-box dash-box box-shadow red_box dash_box_content text-center" data-aos="zoom-in-left" data-aos-easing="linear" data-aos-delay="800" data-aos-duration="800">
                    
                    <div class="widget-chart-1 white_color_font">                      
                        <h4 class="">Rejected Invoices</h4>
                        <h2 class="number-size"><?=$rejected_invoice?></h2>
                        <span class="label label-gray">Invoices</span>                     
                    </div>
                  </div>
                </a>
                </div><!-- end col -->
              </div>
            </div>
              
              <h4 class="header-title business_manager_header_title m-b-30 bmsmheader bmsmheaderabm">
            <span>Hallmarking</span>
            </h4>

            <div class="row">              
              <div class="col-md-12 col-xs-12 section-3 product-box marg_top-bottom no_pad">
                 <div class="col-lg-3 col-md-3 col-sm-6">
                  <a href="<?=ADMIN_PATH.'Dispatch/hallmarking'?>">
                     <div class="card-box dash-box box-shadow blue_box dash_box_content text-center" data-aos="zoom-out" data-aos-easing="linear" data-aos-delay="800" data-aos-duration="800">
                    
                    <div class="widget-chart-1 white_color_font">
                      
                        <h4 class="">Sent To Hallmarking</h4>
                        <h2 class="number-size"><?=$sent_to_hallamrking;?></h2>
                        <span class="label label-gray">Products</span>
                      
                    </div>
                  </div>
                </a>
                </div><!-- end col -->
                <div class="col-lg-3 col-md-3 col-sm-6">
                  <a href="<?=ADMIN_PATH.'Dispatch/hallmarking_recieved'?>">
                     <div class="card-box dash-box box-shadow grey_box dash_box_content text-center" data-aos="zoom-out" data-aos-easing="linear" data-aos-delay="800" data-aos-duration="800">
                   
                    <div class="widget-chart-1 white_color_font">
                     
                         <h4 class="">Recieved From Hallmarking</h4>
                        <h2 class="number-size"><?=$rec_from_hallamrking;?></h2>
                        <span class="label label-gray">Products</span>
                      
                    </div>
                  </div>
                </a>
                </div><!-- end col -->
              </div>
            </div>

            <h4 class="header-title business_manager_header_title m-b-30 bmsmheader bmsmheaderabm">
            <span>Dispatch</span>
            </h4>

            <div class="row">              
              <div class="col-md-12 col-xs-12 section-3 product-box marg_top-bottom no_pad">
                 <div class="col-lg-3 col-md-3 col-sm-6">
                  <a href="<?=ADMIN_PATH.'Dispatch/dispatch_products'?>">
                     <div class="card-box dash-box box-shadow darkBlue_box dash_box_content text-center" data-aos="zoom-in" data-aos-easing="linear" data-aos-delay="800" data-aos-duration="800">
                   
                    <div class="widget-chart-1 white_color_font">
                        <h4 class="">dispatched Products</h4>
                        <h2 class="number-size"><?=$dispatch_products?></h2>
                        <span class="label label-gray">Products</span>
                      </div>
                    </div>
                  </div>
                </a>
                </div><!-- end col -->
              </div>
            </div>
        </div>
              <!--===================================================-->
      </div><!-- end row -->
    </div><!-- end container -->
  </div><!-- end content -->
</div><!-- end content-page -->