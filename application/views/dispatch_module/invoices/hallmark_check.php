<div class="content-page">
<div class="content">
   <div class="container">
      <div class="row">
         <div class="col-sm-12 card-box-wrap">
            <div class="panel panel-default">
              <div class="panel-heading">
               <h4 class="inlineBlock"><span><?=$display_title;?></span></h4>                
              </div>
              <div class="panel-body">
                <div class="table-rep-plugin">
                  <!--    <span class="text-danger" id="center_hallmarking_error" style="display:none">* Hallmarking Center is required</span>
                  <span class="text-danger" id="gross_weight_error" style="display:none">* Gross Weight is required</span>
                  <span class="text-danger" id="person_name_error" style="display:none">* Person Name is required</span>
                  <span class="text-danger" id="quntity_error" style="display:none">* Quantity is required</span> -->
                 <div class="table-responsive b-0 scrollhidden">
                    <form id="hallmarking_sent_frm">
                        <table class="table custdatatable table-bordered " id="dispatch_invoice_table">
                          <thead>
                               <tr>                   
                                  <th class="col1">#</th>
                                  <th class="col3">Product Code</th>
                                  <th class="col2">Hallmarking Center</th>
                                  <th class="col3">Gross Weight</th>
                                  <th>Person Name</th>
                                  <th>Hallmarking Logo</th>
                                  <th>Client Name</th>
                                  <th>Pieces/Quantity</th>
                               </tr>
                            <?php
                             
                             foreach ($selected_id as $key => $value) {
                                $count = 0;
                                foreach ($invoice_details as $key_detail => $detail_value) {
                                    if( $value ==  $detail_value['id']){
                                        ?>
                                         <tr>
                                            <td><?php echo $key+1;?></td>  
                                            <td><?php echo $detail_value['product_code'];?></td>
                                            <td>
                                              <select name="hallmarrking_center[]">
                                                <option value="">Select Center</option>
                                                <?php foreach ($hallmarking_centers as $key => $value) {
                                                  ?>
                                                  <option value="<?php echo $value['id'];?>"><?php echo $value['name'];?></option>
                                                  <?php
                                                }?>
                                              </select>
                                              <span class="text-danger"><span>
                                            </td>
                                            <td><input type="text" name="hallmarking_gr_wt[]" value="<?php echo $detail_value['gr_wt'];?>" palceholder="Gross weight"/> <span class="text-danger"><span></td>
                                            <td><input type="text" name="person_name[]" palceholder="Person Name"/> <span class="text-danger"><span></td>
                                            <td><input type="text" name="hallmarking_logo[]" palceholder="Hallmarking Logo"/></td>
                                            <td><?php echo $invoice['customer_name'];?></td>
                                            <td>
                                              <input type="text" name="quantity[]" palceholder="Pices/Quantity"/><br/>
                                              <span class="text-danger" id="quntity_<?php echo $count;?>"><span>
                                            </td>
                                            <input type="hidden" name="invoice_detail_id[]" value="<?php echo $detail_value['id'];?>"/>
                                         <tr> 
                                        <?php
                                    }
                                    $count++;

                             }}?>                             
                          </thead>
                        </table>
                    </form>
                    <div class="col-sm-offset-4 col-sm-8 ">
                      <button type="button" class="text-center btn btn-primary" onclick="sent_invoice_to_hallmarking()">Submit</button> 
                    </div>
                  </div>
                </div>
              </div>
              <div class="clearfix"></div>                
            </div>
         </div>
      </div>
   </div>
</div>
