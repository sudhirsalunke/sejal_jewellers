<div class="content-page">
<div class="content">
   <div class="container">
      <div class="row">
         <div class="col-sm-12 card-box-wrap print_div">
            <div class=" padmobile">
               <h4 class="header-title business_manager_header_title m-t-0 m-b-30 bmsmheader bmsmheaderabm"><span><?=$display_title;?></span>
               </h4>

               <div class="clearfix"></div>
               <input type="hidden" id="invoice_id" value="<?=$invoice['id']?>">
               <div class="col-md-12 card-box">
                  <div class="col-sm-6">
                    <div class="col-sm-12 form-control ">
                      <label class="label-control col-sm-8">Date :</label>
                      <label><?=date_dmy($invoice['transaction_date'],'-');?></label>
                    </div>

                    <div class="col-sm-12 form-control ">
                      <label class="label-control col-sm-8">Customer Name :</label>
                      <label><?=$invoice['customer_name']?></label>
                    </div>

                    <div class="col-sm-12 form-control ">
                      <span class="label-control col-sm-8">Color Stone Category :</span>
                      <label><?=$invoice['color_stone_name']?></label>
                    </div>

                  </div>

                  <div class="col-sm-6">
                    <div class="col-sm-12 form-control ">
                      <label class="label-control col-sm-8">Chitti No :</label>
                      <label><?=$invoice['id']?></label>
                    </div>

                    <div class="col-sm-12 form-control ">
                      <label class="label-control col-sm-8">Kundan Category:</label>
                      <label><?=$invoice['kundan_category_name']?></label>
                    </div>

                    <div class="col-sm-12 form-control ">
                      <label class="label-control col-sm-8">Details :</label>
                     <label><?=$invoice['details']?></label>
                    </div>

                  </div><br><br>
                  <form enctype='multipart/form-data' role="form" name="product_form" id="hallmark_invoices" action="<?=ADMIN_PATH.'Dispatch_invoices/hallmark_invoices'?>" method="post">
                      <div class="table-responsive">
                        <table class="table table-bordered custdatatable" id="show_dispatch_invoice">
                        <thead>
                          <th>Type</th>
                          <th>Item No</th>
                          <th>Gr Wt</th>
                          <th>CS Wt</th>
                          <th>Kun Wt</th>
                          <th>Net Wt</th>
                          <th>C/S @</th>
                          <th>C/S</th>
                          <th>Kun Pcs</th>
                          <th>Kun @</th>
                          <th>Kun</th>
                          <th>Rodo</th>
                          <th>Rodi</th>
                          <th>Other</th>
                          <th>Total</th>
                          <th></th> 
                        </thead>
                        <tbody> 
                        </tbody>
                      </table>
                      </div>
                   </form>    
               </div><br/>
              <div class="col-sm-offset-4 col-sm-8 ">
                <!-- <a href="<?=ADMIN_PATH.'sales_invoice/print_invoice/'.$invoice['id']?>" class="text-center btn btn-primary">PRINT</a>
                <a href="<?=ADMIN_PATH.'sales_invoice';?>" class="text-center btn btn-warning">BACK</a> -->
                         
              </div>
            </div>
         </div>
      </div>
   </div>
</div>

<!--modal-->
 <div id="hallmarking_dispatch_model" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                    <h4 class="modal-title">SEND TO HALLMARKING</h4>
                </div>
                <div class="modal-body">
                    <form class="form-horizontal" method="post" id="dispatch_hallmarking">
                      <input type="hidden" id="sid_id" name="hallmarking[sid_id]">
                        <div class="form-group">
                            <label class="col-sm-4" for="inputEmail3"> Hallmarking Center <span class="asterisk">*</span></label>
                            <div class="col-sm-8">
                                <select name="hallmarking[hc_id]" class="form-control">
                                    <option value="">Select Hallmarking Center</option>
                                    <?php if(!empty($hallmarking_centers)){
                                        foreach (@$hallmarking_centers as $h_key => $h_value) { ?>
                                        <option value="<?=$h_value['id']?>"><?=$h_value['name']?></option>
                                    <?php } } ?>
                                </select>
                                 <span class="text-danger" id="hc_id_error"></span>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-sm-4" for="inputEmail3">Hallmarking logo <span class="asterisk">*</span></label>
                            <div class="col-sm-8">
                                <input type="text" name="hallmarking[hc_logo]" class="form-control">
                                <span class="text-danger" id="hc_logo_error"></span>
                            </div>
                        </div>
                         <div class="form-group">
                            <label class="col-sm-4" for="inputEmail3">Client Name</label>
                            <div class="col-sm-8">
                                <label><?=$invoice['customer_name']?></label>
                            </div>
                        </div>
                         <div class="form-group">
                            <label class="col-sm-4" for="inputEmail3">Gross weight<span class="asterisk">*</span></label>
                            <div class="col-sm-8">
                                <input type="text"  name="hallmarking[total_gr_wt]" class="form-control">
                                <span class="text-danger" id="total_gr_wt_error"></span>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-sm-4" for="inputEmail3">Person Name<span class="asterisk">*</span></label>
                            <div class="col-sm-8">
                                <input type="text"  name="hallmarking[person_name]" class="form-control">
                                <span class="text-danger" id="person_name_error"></span>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-sm-4" for="inputEmail3" >Quantity<span class="asterisk">*</span></label>
                            <div class="col-sm-8">
                                <input type="text"  class="form-control" name="hallmarking[quantity]">
                                <span class="text-danger" id="quantity_error"></span>
                            </div>
                        </div>
                        <div class="modal-footer">  
                            
                            <button type="button" class="btn btn-primary waves-effect waves-light" onclick="dispatch_hallmarking_store()">SUBMIT</button>
                            
                            <button id="barcode_submit" type="button" class="btn btn-danger waves-effect waves-light" data-dismiss="modal">No</button>
                        </div>
                   </form>
               </div> 
            </div><!-- /.modal-content -->
        </div><!-- /.modal-dialog -->
    </div>


 <div id="dispatch_transfered" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                    <h4 class="modal-title">SEND TO DISPATCH</h4>
                </div>
                <div class="modal-body">
                    <form class="form-horizontal" method="post" id="dispatch_recieve">
                      <input type="hidden" id="sid_id_dispatch" name="dispatch[sid_id]">
                      <input type="hidden" id="quantity_dispatch" name="dispatch[quantity]">
                       <input type="hidden" id="chitti_no" value="<?=$invoice['id']?>">
                       
                         <div class="form-group">
                            <label class="col-sm-4" for="inputEmail3">Mode<span class="asterisk">*</span></label>
                            <div class="col-sm-8 form-inline">
                              
                              <?php foreach ($dispatch_mode as $dm_key => $dm_value) { ?>
                                <div class="radio radio-success">
                                  <input type="radio" name="dispatch[mode]" value="<?=$dm_value['encrypted_id']?>" id="radio<?=$dm_key?>" class="dispatch_mode">
                                    <label for="radio<?=$dm_key?>"><?=$dm_value['mode_name']?></label>
                              </div>
                             <?php } ?>
                                <span class="text-danger col-sm-12" id="mode_error"></span>
                            </div>
                        </div>
                         <?php foreach ($dispatch_mode_data as $dmd_key1 => $dmd_value1) { 
                          ?>

                        <div id="div_<?=$dmd_key1?>" class="mode_div">
                           <?php
                              if (!empty($dispatch_mode_data[$dmd_key1][0]['mode_type'])) {
                                $mode_type=$dispatch_mode_data[$dmd_key1][0]['mode_type'];
                              }else{
                                $mode_type=0;
                              }
                               if ($mode_type ==2) { ?>
                                <select name="dispatch[<?=$dmd_key1?>][name]" class="form-control">
                                   <option value="">Please Select One option</option>
                             <?php } ?>
                           

                        <?php foreach ($dispatch_mode_data[$dmd_key1] as $dmd_key => $dmd_value) {  
                            $name_field =RenameUploadFile($dmd_value['name']);
                          ?>
                           <?php if ($mode_type==1) { ?>
                                  <div class="form-group">
                                    <label class="col-sm-4" for="inputEmail3" ><?=$dmd_value['name']?><span class="asterisk">*</span></label>
                                    <div class="col-sm-8">
                                      <input type="text"  class="form-control"  name="dispatch[<?=$dmd_key1?>][<?=$name_field?>]" >
                                      <span class="text-danger" id="<?=$dmd_key1.$name_field?>_error"></span>
                                    </div>
                                  </div>

                                  <?php  }elseif ($dmd_value['mode_type']==2) { ?>
                                 
                                  <option value="<?=$dmd_value['name']?>"><?=$dmd_value['name']?></option>
                                       
                                  <?php  }elseif ($dmd_value['mode_type']==3) { ?>
                                     
                                  <?php  } ?>
                                
                         <?php  } if ($mode_type ==2) { ?>
                                </select>
                                <span class="text-danger" id="<?=$dmd_key1?>name_error"></span>
                             <?php } ?>
                        </div>
                        <?php } ?>
                       
                        <div class="modal-footer">  
                            
                            <button type="button" class="btn btn-primary waves-effect waves-light" onclick="dispatch_transfer()">SUBMIT</button>
                            
                            <button id="barcode_submit" type="button" class="btn btn-danger waves-effect waves-light" data-dismiss="modal">No</button>
                        </div>
                   </form>
               </div> 
            </div>
        </div>
    </div>
