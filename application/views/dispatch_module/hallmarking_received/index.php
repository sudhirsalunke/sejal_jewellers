<div class="content-page">
<div class="content">
   <div class="container">
      <div class="row">
         <div class="col-sm-12 card-box-wrap">
            <div class="panel panel-default">
              <div class="panel-heading">
               <h4 class="inlineBlock"><span><?=$display_title;?></span></h4>
              </div>
              <div class="panel-body">
                <div class="table-rep-plugin">
                  <div class="table-responsive b-0 scrollhidden">         
                    <table class="table custdatatable table-bordered " id="dispatch_hallmarking_rec_table">
                      <thead>
                        <?php
                          $data['heading']='dispatch_hallmarking_rec_table';
                          $this->load->view('master/table_header',$data);
                        ?>
<!-- 
                        <tr>                            
                          <th>#</th>
                          <th>Item No</th>
                          <th>Quantity</th>
                          <th>Hallmarking Center</th>
                          <th>Invoice No</th>
                          <th>Recieved Weight</th>
                          <th>Amount Per Qty</th>
                          <th>Total Amount</th>
                          <th></th>
                        </tr> -->
                      </thead>
                    </table>                 
                  </div>
                </div>
              </div>
               <div class="clearfix"></div>              

            </div>
         </div>
      </div>
   </div>
</div>

<!--modal-->
 <div id="dispatch_transfered" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                    <h4 class="modal-title">SEND TO DISPATCH</h4>
                </div>
                <div class="modal-body">
                    <form class="form-horizontal" method="post" id="dispatch_recieve">
                      <input type="hidden" id="sid_id_dispatch" name="dispatch[sid_id]">
                      <input type="hidden" id="hr_id_dispatch" name="dispatch[hr_id]">
                      <input type="hidden" id="quantity_dispatch" name="dispatch[quantity]">
                      
                       
                       
                         <div class="form-group">
                            <label class="col-sm-4" for="inputEmail3">Mode<span class="asterisk">*</span></label>
                            <div class="col-sm-8 form-inline">
                              
                              <?php foreach ($dispatch_mode as $dm_key => $dm_value) { ?>
                                <div class="radio radio-success">
                                  <input type="radio" name="dispatch[mode]" value="<?=$dm_value['encrypted_id']?>" id="radio<?=$dm_key?>" class="dispatch_mode">
                                    <label for="radio<?=$dm_key?>"><?=$dm_value['mode_name']?></label>
                              </div>
                             <?php } ?>
                                <span class="text-danger col-sm-12" id="mode_error"></span>
                            </div>
                        </div>
                         <?php foreach ($dispatch_mode_data as $dmd_key1 => $dmd_value1) { 
                          ?>

                        <div id="div_<?=$dmd_key1?>" class="mode_div">
                           <?php
                              if (!empty($dispatch_mode_data[$dmd_key1][0]['mode_type'])) {
                                $mode_type=$dispatch_mode_data[$dmd_key1][0]['mode_type'];
                              }else{
                                $mode_type=0;
                              }
                               if ($mode_type ==2) { ?>
                                <select name="dispatch[<?=$dmd_key1?>][name]" class="form-control">
                                  <option value="">Please Select One option</option>
                             <?php } ?>
                           

                        <?php foreach ($dispatch_mode_data[$dmd_key1] as $dmd_key => $dmd_value) {  
                            $name_field =RenameUploadFile($dmd_value['name']);
                          ?>
                           <?php if ($mode_type==1) { ?>
                                  <div class="form-group">
                                    <label class="col-sm-4" for="inputEmail3" ><?=$dmd_value['name']?><span class="asterisk">*</span></label>
                                    <div class="col-sm-8">
                                      <input type="text"  class="form-control"  name="dispatch[<?=$dmd_key1?>][<?=$name_field?>]" >
                                      <span class="text-danger" id="<?=$dmd_key1.$name_field?>_error"></span>
                                    </div>
                                  </div>

                                  <?php  }elseif ($dmd_value['mode_type']==2) { ?>
                                 
                                  <option value="<?=$dmd_value['name']?>"><?=$dmd_value['name']?></option>
                                       
                                  <?php  }elseif ($dmd_value['mode_type']==3) { ?>
                                     
                                  <?php  } ?>
                                
                         <?php  } if ($mode_type ==2) { ?>
                                </select>
                                <span class="text-danger" id="<?=$dmd_key1?>name_error"></span>
                             <?php } ?>
                        </div>
                        <?php } ?>
                         
                         <!-- <div id="div_cargo" class="mode_div">
                          <div class="form-group">
                              <label class="col-sm-4" for="inputEmail3" >BVC Logistic<span class="asterisk">*</span></label>
                              <div class="col-sm-8">
                                  <input type="text"  class="form-control"  name="dispatch[cargo_bvc]" >
                                  <span class="text-danger" id="cargo_bvc_error"></span>
                              </div>
                          </div>
                           <div class="form-group">
                            <label class="col-sm-4" for="inputEmail3" >Sequel Logistic<span class="asterisk">*</span></label>
                            <div class="col-sm-8">
                                <input type="text"  class="form-control"  name="dispatch[cargo_sequel]" >
                                <span class="text-danger" id="cargo_sequel_error"></span>
                            </div>
                        </div>
                         <div class="form-group">
                            <label class="col-sm-4" for="inputEmail3" >Al-ferrari<span class="asterisk">*</span></label>
                            <div class="col-sm-8">
                                <input type="text"  class="form-control"  name="dispatch[cargo_al]" >
                                <span class="text-danger" id="cargo_al_error"></span>
                            </div>
                        </div>
                      </div>
                      <div id="div_anagadiya" class="mode_div">
                          <div class="form-group">
                            <label class="col-sm-4" for="inputEmail3" >Amrat Kranti<span class="asterisk">*</span></label>
                            <div class="col-sm-8">
                                <input type="text"  class="form-control"  name="dispatch[a_amrat]" >
                                <span class="text-danger" id="a_amrat_error"></span>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-sm-4" for="inputEmail3" >Madhav Magan<span class="asterisk">*</span></label>
                            <div class="col-sm-8">
                                <input type="text"  class="form-control"  name="dispatch[a_madhav]" >
                                <span class="text-danger" id="cargo_al_error"></span>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-sm-4" for="inputEmail3" >Bomin<span class="asterisk">*</span></label>
                            <div class="col-sm-8">
                                <input type="text"  class="form-control"  name="dispatch[a_bomin]" >
                                <span class="text-danger" id="cargo_al_error"></span>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-sm-4" for="inputEmail3" >RR<span class="asterisk">*</span></label>
                            <div class="col-sm-8">
                                <input type="text"  class="form-control"  name="dispatch[a_rr]" >
                                <span class="text-danger" id="cargo_al_error"></span>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-sm-4" for="inputEmail3" >Ramesh Parekh<span class="asterisk">*</span></label>
                            <div class="col-sm-8">
                                <input type="text"  class="form-control"  name="dispatch[a_ramesh]" >
                                <span class="text-danger" id="cargo_al_error"></span>
                            </div>
                        </div>
                      </div>
                      <div id="div_via_thru" class="mode_div">
                          <div class="form-group">
                              <label class="col-sm-4" for="inputEmail3" >Sent thru<span class="asterisk">*</span></label>
                              <div class="col-sm-8">
                                  <input type="text"  class="form-control"  name="dispatch[st_sent_through]" >
                                  <span class="text-danger" id="cargo_bvc_error"></span>
                              </div>
                          </div>
                           <div class="form-group">
                            <label class="col-sm-4" for="inputEmail3" >Delievered At<span class="asterisk">*</span></label>
                            <div class="col-sm-8">
                                <input type="text"  class="form-control"  name="dispatch[st_delieverd_at]" >
                                <span class="text-danger" id="cargo_sequel_error"></span>
                            </div>
                        </div>
                        
                      </div> -->
                       
                        <div class="modal-footer">  
                            
                            <button type="button" class="btn btn-primary waves-effect waves-light" onclick="dispatch_transfer()">SUBMIT</button>
                            
                            <button id="barcode_submit" type="button" class="btn btn-danger waves-effect waves-light" data-dismiss="modal">No</button>
                        </div>
                   </form>
               </div> 
            </div><!-- /.modal-content -->
        </div><!-- /.modal-dialog -->
    </div>
