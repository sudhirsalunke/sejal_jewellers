<div class="content-page">
<div class="content">
   <div class="container">
      <div class="row">
         <div class="col-sm-12 card-box-wrap">
            <div class="panel panel-default">
              <div class="panel-heading">
               <h4 class="inlineBlock"><span><?=$display_title;?></span></h4>
              </div>
              <div class="panel-body">
                <div class="table-rep-plugin">
                  <div class="table-responsive b-0 scrollhidden">         
                    <table class="table custdatatable table-bordered " id="dispatch_hallmarking_table">
                      <thead>
                      <?php
                          $data['heading']='dispatch_hallmarking_table';
                          $this->load->view('master/table_header',$data);
                        ?>
                     <!--    <tr>                            
                          <th>#</th>
                          <th>Item No</th>
                          <th>Quantity</th>
                          <th>Hallmarking Center</th>
                          <th>Invoice No</th>
                          <th>Total Weight</th>
                          <th>Person Name</th>
                          <th></th>
                        </tr> -->
                      </thead>
                    </table>                 
                  </div>
                </div>
              </div>
               <div class="clearfix"></div>              

            </div>
         </div>
      </div>
   </div>
</div>

<!--modal-->
 <div id="hallmarking_dispatch_recieved_model" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                    <h4 class="modal-title">SEND TO DISPATCH</h4>
                </div>
                <div class="modal-body">
                    <form class="form-horizontal" method="post" id="dispatch_hallmarking_recieve">
                      <input type="hidden" id="sid_id" name="hallmarking[sid_id]">
                      <input type="hidden" id="qch_id" name="hallmarking[qch_id]">
                       
                       
                         <div class="form-group">
                            <label class="col-sm-4" for="inputEmail3">weight<span class="asterisk">*</span></label>
                            <div class="col-sm-8">
                                <input type="text"  name="hallmarking[total_gr_wt]" class="form-control">
                                <span class="text-danger" id="total_gr_wt_error"></span>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-sm-4" for="inputEmail3" >Quantity<span class="asterisk">*</span></label>
                            <div class="col-sm-8">
                                <input type="text"  class="form-control" id="qty_entered" name="hallmarking[quantity]" onkeyup="calculate_total_rec_amt()">
                                <span class="text-danger" id="quantity_error"></span>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-sm-4" for="inputEmail3" >Amount<span class="asterisk">*</span></label>
                            <div class="col-sm-8">
                                <input type="text"  class="form-control" id="amt_entered" name="hallmarking[amount]" onkeyup="calculate_total_rec_amt()">
                                <span class="text-danger" id="amount_error"></span>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-sm-4" for="inputEmail3" >Total Amount<span class="asterisk">*</span></label>
                            <div class="col-sm-8">
                                <input type="text" id="total_amount"  class="form-control" name="hallmarking[total_amount]" readonly>
                                <span class="text-danger" id="tot_amt_error"></span>
                            </div>
                        </div>
                        <div class="modal-footer">  
                            
                            <button type="button" class="btn btn-primary waves-effect waves-light" onclick="dispatch_hallmarking_recieve()">SUBMIT</button>
                            
                            <button id="barcode_submit" type="button" class="btn btn-danger waves-effect waves-light" data-dismiss="modal">No</button>
                        </div>
                   </form>
               </div> 
            </div><!-- /.modal-content -->
        </div><!-- /.modal-dialog -->
    </div>
