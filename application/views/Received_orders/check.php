<div class="content-page">
<div class="content">
   <div class="container">
      <div class="row">
        <div class="col-sm-12 col-xs-12 card-box-wrap card-box padmobile">
            
               <h4 class="header-title business_manager_header_title m-t-0 m-b-30 bmsmheader bmsmheaderabm"><span><?= strtoupper($page_title);?></span>
               </h4>
				<div class="col-md-10 col-xs-12 m-t-15">
               		<div class="row ">
	               		<div class="col-sm-5">
	               			<div class="form-group">
			       				<label class="col-sm-5 control-label " for="inputEmail3">Karigar Name: </label>
			       				<div class="col-sm-5">
			       					<label class="control-label" for="inputEmail3"><?= $receive_order[0]['km_name'];?></label>
			       				</div>
		       				</div>
		       			</div>
	               		<div class="col-sm-5">
	               			<div class="form-group">
			       				<label class="col-sm-5 control-label " for="inputEmail3">Receipt Code: </label>
			       				<div class="col-sm-5">
			       					<label class="control-label" for="inputEmail3"><?= $receive_order[0]['receipt_code'];?></label>
			       				</div>
		       				</div>
		       			</div>
	               		<div class="col-sm-5">
	               			<div class="form-group">
			       				<label class="col-sm-5 control-label " for="inputEmail3">Receive Date: </label>
			       				<div class="col-sm-5">
			       					<label class="control-label" for="inputEmail3"><?=date('d-m-Y',strtotime($receive_order[0]['date']));?></label>
			       				</div>
		       				</div>
		       			</div>
		            </div>
               	</div>
               	<div class="clearfix"></div>
               	<div class="col-xs-12 m-t-15">
	                <form name="category" id="qc_check" role="form" class="form-horizontal" method="post">
		               	<div class="clo-sm-12">
					        <table class="table table-bordered" id="Receive_pieces">
					            <thead>
					             <tr>
					               <th class="col4">Select</th>
					               <th class="col4">Order Name</th>
					               <th class="col4">Parent Category</th>
					               <th class="col4">Quantity</th>
					               <th class="col4">Weight Range</th>
					               <th class="col4">Approx. WT </th>
					               <th class="col4">Is HM ?</th>
					               <th class="col4">HM Net Wt</th>
					               <th class="col4">HM Quantity</th>

					             </tr>
					           </thead>
					           <tbody>
					            <?php
					            if(!empty($receive_order)){
					              foreach ($receive_order as $key => $value) {
					                ?>
					                <input type="hidden" value="<?=(($value['from_weight'] + $value['to_weight'])/2)?>" id="appximate_weight_<?= $key?>">
					                <tr>
					                  <td><div class="checkbox checkbox-purpal">
					                    <input type="checkbox" data-parsley-multiple="groups" data-parsley-mincheck="2" data-parsley-id="69" name="qc[receive_product_id][]" class="product" value="<?=$value['id']?>">
					                    <label for="order_id"></label>
					                  </div></td>
					                  <td><?=$value['order_name']?></td>
					                  <td><?=$value['name']?></td>
					                  <td><input type="text" class="form-control" name="qc[quantity][<?=$value['id']?>]" value="<?=($value['rp_qnt'] - $value['qc_quantity'])?>"><p id="quantity_<?= $value['id']?>_error" class="text-danger"></p></td>
					                  <td><?=$value['from_weight'].'-'.$value['to_weight']?></td>
					                  <td><input type="text" class="form-control" name="qc[weight][<?=$value['id']?>]" value="<?=(($value['from_weight'] + $value['to_weight'])/2)*$value['quantity']?>"><p class="text-danger" id="weight_<?= $value['id']?>_error"></p></td>
					                  <td><div class="checkbox checkbox-purpal">
							        			<input data-parsley-multiple="group1" data-parsley-id="76" type="checkbox" id="hallmarking_checked" name="qc[hallmarking][<?=$value['id']?>]" value="1" class="from-control txt-box">
							        			<label for="hallmarking"></label></td>
							        	<td><input type="text"  class="form-control" name="qc[net_wt][<?=$value['id']?>]"><p class="text-danger" id="net_wt_<?= $value['id']?>_error"></p></td>
							        	<td><input type="text"  class="form-control" name="qc[hallmarking_quantity][<?=$value['id']?>]"><p class="text-danger" id="hallmarking_quantity_<?= $value['id']?>_error"></p></td>
					                </tr>
					                <?php
					              }
					            }else{ ?>
					                <tr>
					                    <td colspan="6">No Orders Found</td>
					                </tr>
					           <?php } ?>
					          </tbody>
					        </table><div class="clearfix"></div>
					    </div><div class="clearfix"></div>
		               		<!-- <div class="col-md-10 col-xs-12 m-t-15">
			               		<div class="row ">
				               		<div class="col-sm-5">
				               			<div class="form-group">
						       				<label class="col-sm-5 control-label " for="inputEmail3">Karigar Name </label>
						       				<div class="col-sm-5">
						       					<label class="control-label" for="inputEmail3"><?= $receive_order['name'];?></label>
						       				</div>
					       				</div>
					       			</div>
					       			<div class="col-sm-5">
					       				<div class="form-group">
						       				<label class="col-sm-5 control-label" for="inputEmail3">Receipt Code </label>
						       				<div class="col-sm-5">
						       					<label class="control-label" for="inputEmail3"><?= $receive_order['receipt_code'];?></label>
						       				</div>
					       				</div>
					       			</div>
					       			<div class="col-sm-5">
					       				<label class="col-sm-5 control-label" for="inputEmail3">Receipt Order Weight <span class="asterisk">*</span></label>
					       				<div class="col-sm-5">
					       					<div class="form-group">
						       					<input type="text" class="form-control" name="weight" value="<?= round($receive_order['weight'],2)?>">
								        			<label for="bom_checking"></label>
					       				<span class="text-danger" id="weight_error"></span>
					       					</div>
					       				</div>
					       			</div>
					       			<div class="col-sm-5">
				               			<div class="form-group">
						       				<label class="col-sm-5 control-label " for="inputEmail3">Quantity<span class="asterisk">*</span></label>
						       				<div class="col-sm-5">
						       					<div class="form-group">
						       						<input type="text" class="form-control" name="quantity" value="<?= $receive_order['quantity'] - $rp_quantity;?>">
								        			<label for="bom_checking"></label>
					       					<span class="text-danger" id="quantity_error"></span>
					       						</div>
						       				</div>
					       				</div>
					       			</div>
					            </div>
		               		</div> -->
		               		<!-- <div class="col-md-12 col-xs-12 mr10">
			                	<h4 class="header-title business_manager_header_title m-t-0 m-b-30 bmsmheader bmsmheaderabm"><span>Products </span>
		               			</h4>
							</div> -->
							<div class="col-md-10 col-xs-8">
							  <div class="table-rep-plugin">
							    <div class="table-responsive b-0 scrollhidden">
							      <table class="table table-bordered" id="sub_catagory">
							        <thead>
							           <tr>
							               <th class="col4">Sr NO</th>
							               <th class="col4">Parameter</th>
							               <th class="col4"><span class="checkbox checkbox-purpal">
					                    <input type="checkbox" id="check_all_qc" data-parsley-multiple="groups" data-parsley-mincheck="2" data-parsley-id="69" class="product check_all" value="147" checked="">
					                    <label for="check_all_qc"></label>
					                  </span></th>
							              <th class="col3">Remark</th>
							           </tr>
							        </thead>
							        <tbody>
							        	<tr>
							        		<td>1</td>
							        		<td>Ghat Weight</td>
							        		<td>
							        		<div class="checkbox checkbox-purpal">
							        			<input data-parsley-multiple="group1" data-parsley-id="76" type="checkbox" name="ghat_wt" value="1" class="from-control checked_checkboxses" checked>
							        			<label for="bom_checking"></label>
							        		</div>
							        		</td>
							        		<td><textarea name="ghat_wt_remark" class="txt-box"></textarea></td>
							        	</tr>
							        	<tr>
							        		<td>2</td>
							        		<td>DESIGN CHECKING</td>
							        		<td>
							        		<div class="checkbox checkbox-purpal">
							        			<input data-parsley-multiple="group1" data-parsley-id="76" type="checkbox" name="design_checking" value="1" class="from-control checked_checkboxses" checked>
							        			<label for="design_checking"></label>
							        		</div>
											</td>
							        		<td><textarea name="design_checking_remark" class="txt-box" ></textarea></td>
							        	</tr>
							        	<tr>
							        		<td>3</td>
							        		<td>PAIR MATCHING</td>
							        		<td>
							        		<div class="checkbox checkbox-purpal">
							        			<input data-parsley-multiple="group1" data-parsley-id="76" type="checkbox" name="pair_matching"  value="1" class="from-control checked_checkboxses" checked>
							        			<label for="pair_matching"></label>
							        		</div>
											</td>
							        		<td><textarea name="pair_matching_remark" class="txt-box" ></textarea></td>
							        	</tr>
							        	<tr>
							        		<td>4</td>
							        		<td>CARATAGE/PURITY</td>
							        		<td>
							        		<div class="checkbox checkbox-purpal">
							        			<input data-parsley-multiple="group1" data-parsley-id="76" type="checkbox" name="karatage/purity" value="1" class="from-control checked_checkboxses" checked>
							        			<label for="karatage/purity"></label>
							        		</div>
											</td>
							        		<td><textarea name="karatage/purity_remark" class="txt-box" ></textarea></td>
							        	</tr>
							        	<tr>
							        		<td>5</td>
							        		<td>SIZE</td>
							        		<td>
							        			<div class="checkbox checkbox-purpal">
							        			<input data-parsley-multiple="group1" data-parsley-id="76" type="checkbox" name="size" value="1" class="from-control checked_checkboxses" checked>
							        			<label for="size"></label>
							        		</div>
											</td>
							        		<td><textarea name="size_remark" class="txt-box"></textarea></td>
							        	</tr>
							        	<tr>
							        		<td>6</td>
							        		<td>STAMPING</td>
							        		<td>
							        			<div class="checkbox checkbox-purpal">
							        			<input data-parsley-multiple="group1" data-parsley-id="76" type="checkbox" name="stamping" value="1" class="from-control checked_checkboxses" checked>
							        			<label for="stamping"></label>
							        		</div>
											</td>
							        		<td><textarea name="stamping_remark" class="txt-box"></textarea></td>
							        	</tr>
							        	<tr>
							        		<td>7</td>
							        		<td>SHARP EDGE</td>
							        		<td>
							        			<div class="checkbox checkbox-purpal">
							        			<input data-parsley-multiple="group1" data-parsley-id="76" type="checkbox" name="sharp_edge" value="1" class="from-control checked_checkboxses" checked>
							        			<label for="sharp_edge"></label>
							        		</div>
											</td>
							        		<td><textarea name="sharp_edge_remark" class="txt-box" ></textarea></td>
							        	</tr>
							        	<tr>
							        		<td>8</td>
							        		<td>SOLDER/LINKING</td>
							        		<td>
							        			<div class="checkbox checkbox-purpal">
							        			<input data-parsley-multiple="group1" data-parsley-id="76" type="checkbox" name="solder/linking" value="1" class="from-control checked_checkboxses" checked>
							        			<label for="solder/linking"></label>
							        		</div>
											</td>
							        		<td><textarea name="solder/linking_remark" class="txt-box" ></textarea></td>
							        	</tr>
							        	<tr>
							        		<td>9</td>
							        		<td>SHAPE OUT</td>
							        		<td>
							        			<div class="checkbox checkbox-purpal">
							        			<input data-parsley-multiple="group1" data-parsley-id="76" type="checkbox" name="shape_out" value="1" class="from-control checked_checkboxses" checked>
							        			<label for="shape_out"></label>
							        		</div>
											</td>
							        		<td><textarea name="shape_out_remark" class="txt-box"></textarea></td>
							        	</tr>
							        	<tr>
							        		<td>10</td>
							        		<td>FINISHING</td>
							        		<td>
							        			<div class="checkbox checkbox-purpal">
							        			<input data-parsley-multiple="group1" data-parsley-id="76" type="checkbox" name="finishing" value="1" class="from-control checked_checkboxses" checked>
							        			<label for="finishing"></label>
							        		</div>

											</td>
							        		<td><textarea name="finishing_remark" class="txt-box"></textarea></td>
							        	</tr>
							        	<tr>
							        		<td>11</td>
							        		<td>GR WT/NET WT</td>
							        		<td>
							        			<div class="checkbox checkbox-purpal">
							        			<input data-parsley-multiple="group1" data-parsley-id="76" type="checkbox" name="gr_wt/nt_wt" value="1" class="from-control checked_checkboxses" checked>
							        			<label for="gr_wt/nt_wt"></label>
							        		</div>
											</td>
							        		<td><textarea name="gr_wt/nt_wt_remark" class="txt-box"></textarea></td>
							        	</tr>
							        	<tr>
							        		<td>12</td>
							        		<td>TAG DETAIL</td>
							        		<td>
							        			<div class="checkbox checkbox-purpal">
							        			<input data-parsley-multiple="group1" data-parsley-id="76" type="checkbox" name="tag_detail" value="1" class="from-control checked_checkboxses" checked>
							        			<label for="tag_detail"></label>
							        		</div>
											</td>
							        		<td><textarea name="tag_detail_remark" class="txt-box"></textarea></td>
							        	</tr>
							        	<!--<tr>
							        		<td>13</td>
							        		<td>FINDING/LOCK</td>
							        		<td>
							        			<div class="checkbox checkbox-purpal">
							        			<input data-parsley-multiple="group1" data-parsley-id="76" type="checkbox" name="finding/lock" value="1" class="from-control" <?=($result['finding/lock'] == true) ? 'checked' : ''?> <?=($receive_from_hallmarking == false) ? 'checked' : '' ?>>
							        			<label for="finding/lock"></label>
							        		</div>
											</td>
							        		<td><textarea name="finding/lock_remark" class="txt-box"></textarea></td>
							        	</tr>-->
							        	<tr>
							        		<td>14</td>
							        		<td>WEARING/TEST</td>
							        		<td>
							        			<div class="checkbox checkbox-purpal">
							        			<input data-parsley-multiple="group1" data-parsley-id="76" type="checkbox" name="wearing_test" value="1" class="from-control checked_checkboxses" checked>
							        			<label for="wearing_test"></label>
							        		</div>
											</td>
							        		<td><textarea name="wearing_test_remark" class="txt-box"></textarea></td>
							        	</tr><tr>
							        		<td>15</td>
							        		<td>ALIGNMENT</td>
							        		<td>
							        			<div class="checkbox checkbox-purpal">
							        			<input data-parsley-multiple="group1" data-parsley-id="76" type="checkbox" name="alignment" value="1" class="from-control checked_checkboxses" checked>
							        			<label for="alignment"></label>
							        		</div>
											</td>
							        		<td><textarea name="alignment_remark" class="txt-box"></textarea></td>
							        	</tr>
							        	<tr>
							        		<td>16</td>
							        		<td>PACKING</td>
							        		<td>
							        			<div class="checkbox checkbox-purpal">
							        			<input data-parsley-multiple="group1" data-parsley-id="76" type="checkbox" name="packing" value="1" class="from-control txt-box checked_checkboxses" checked>
							        			<label for="packing"></label>
							        		</div>

											</td>
							        		<td><textarea name="packing_remark" class="txt-box"></textarea></td>
							        	</tr>
							        	<!--<tr>
							        		<td>17</td>
							        		<td>Weight Check</td>
							        		<td>
							        			<div class="checkbox checkbox-purpal">
							        			<input data-parsley-multiple="group1" data-parsley-id="76" type="checkbox" name="weight_check" value="1" class="from-control txt-box" <?=($result['weight_check'] == true) ? 'checked' : ''?> <?=($receive_from_hallmarking == false) ? 'checked' : '' ?>>
							        			<label for="weight_check"></label>
							        		</div>

											</td>
							        		<td><textarea name="weight_check_remark" class="txt-box"></textarea></td>
							        	</tr>-->
							        	<tr>
							        		<td>17</td>
							        		<td>Meena</td>
							        		<td>
							        			<div class="checkbox checkbox-purpal">
							        			<input data-parsley-multiple="group1" data-parsley-id="76" type="checkbox" name="Meena" value="1" class="from-control txt-box checked_checkboxses" checked>
							        			<label for="packing"></label>
							        		</div>

											</td>
							        		<td><textarea name="Meena_remark" class="txt-box"></textarea></td>
							        	</tr>
							        	<!-- <tr>
							        		<td>18</td>
							        		<td>HALLMARKING</td>
							        		<td>
							        			<div class="checkbox checkbox-purpal">
							        			<input data-parsley-multiple="group1" data-parsley-id="76" type="checkbox" id="hallmarking_checked" name="hallmarking" value="1" class="from-control txt-box" checked>
							        			<label for="hallmarking"></label>
							        		</div>

											</td>
							        		<td><textarea name="hallmarking_remark" class="from-control"><?=$result['hallmarking_remark'] ?></textarea></td>

							        	</tr> -->
							        	<tr>
							        		<td></td>
							        		<td></td>
							        		<td> Hallmarking Center <span class="asterisk">*</span></td>
							        		<td>
							        			<select name="hc_id" class="form-control">
							        				<option value="">Select Hallmarking Center</option>
		                                    <?php foreach (@$hallmarking_centers as $h_key => $h_value) { ?>
		                                        <option value="<?=$h_value['id']?>"><?=$h_value['name']?></option>
		                                    <?php } ?>
							        			</select>
							        			<p class="text-danger" id="hc_id_error"></p>
							        		</td>
							        	</tr>
							        	<tr>
							        		<td></td>
							        		<td></td>
							        		<td> Hallmarking Logo <span class="asterisk">*</span></td>
							        		<td>
							        			<input type="text"  class="form-control" name="hc_logo"><p class="text-danger" id="hc_logo_error"></p>
							        		</td>
							        	</tr>
							        	<!-- <tr>
							        		<td></td>
							        		<td></td>
							        		<td> Hallmarking Net Wt<span class="asterisk">*</span> </td>
							        		<td><input type="text"  class="form-control" name="net_wt"><p class="text-danger" id="net_wt_error"></p></td>
							        	</tr>
							        	<tr>
							        		<td></td>
							        		<td></td>
							        		<td> Hallmarking Quantity<span class="asterisk">*</span> </td>
							        		<td><input type="text"  class="form-control" name="hallmarking_quantity"><p class="text-danger" id="hallmarking_quantity_error"></p></td>
							        	</tr> -->
							        </tbody>
							     </table><div class="clearfix"></div>
							   	</div>
							  </div>
							  <div class="col-md-12 col-xs-12 mr10">
				                       <textarea placeholder="Enter Special Remark" id="name" class="col-xs-12 col-md-12 col-lg-12 col-sm-12 txt-box form-control" name="remark" rows="4"></textarea> 
										<span class="text-danger" id="remark_error"></span>
							  </div>
							</div>				
							<div class="form-group">
								<div class="col-sm-offset-5 col-sm-8 texalin m-t-15">
									<button class="btn btn-primary waves-effect waves-light  btn-md qc_accept_btn" name="commit" type="button" onclick="accept_all_m_qc(); ">ACCEPT</button>
									<button class="btn btn-primary waves-effect waves-light  btn-md qc_reject_btn" name="commit" type="button" onclick="reject_all_m_qc(); ">REJECT</button>
								</div>
		          			</div>
	      			</form>
      			</div>
      			<div class="clearfix"></div>
		</div>