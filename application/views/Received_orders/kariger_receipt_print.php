<style media="print">
 @page {
  size: auto;
  margin: 0;
       }
</style>
<center><div class="content-page">
<div class="content">
   <div class="container">
      <div class="row">
         <div class="col-sm-12 card-box-wrap">
            <div class="card-box padmobile">
               <h4 class="header-title business_manager_header_title m-t-0 m-b-30 bmsmheader bmsmheaderabm"><span>KARIGAR RECEIPT</span>
               </h4>
                <div class="clearfix"></div>
               <div class="product_listing">
                <div class="print_area">
                
                <div class="col-sm-12 m-t-20 single_product_div">
                    <p class="col-sm-5 control-label" for="inputEmail3">Karigar Name :<?php echo $receipt_code[0]['karigar_name'];?></p>
              <!--       <p class="col-sm-5 control-label" for="inputEmail3">Net Weight :<?php echo $receipt_code[0]['net_wt'];?></p> -->
                    <p class="col-sm-5 control-label" for="inputEmail3">Receipt Code :<?php echo $receipt_code[0]['receipt_code'];?></p>
                    <?php
                      if($result['is_kundan'] == true){
                        ?>
                        <p class="col-sm-5 control-label" for="inputEmail3">Net Weight :<?php echo $result['net_wt'];?></p>
                        <p class="col-sm-5 control-label" for="inputEmail3">Few Weight :<?php echo $result['few_wt'];?></p>
                        <p class="col-sm-5 control-label" for="inputEmail3">Mina Weight :<?php echo $result['mina_wt'];?></p>
                        <p class="col-sm-5 control-label" for="inputEmail3">Stone Weight :<?php echo $result['stone_wt'];?></p>
                        <p class="col-sm-5 control-label" for="inputEmail3">Kundan Weight :<?php echo $result['kundan_wt'];?></p>
                        <p class="col-sm-5 control-label" for="inputEmail3">Stone Amount :<?php echo $result['stone_amt'];?></p>
                        <p class="col-sm-5 control-label" for="inputEmail3">Kundan Amount :<?php echo $result['kundan_amt'];?></p>
                        <p class="col-sm-5 control-label" for="inputEmail3">Other Weight :<?php echo $result['other_wt'];?></p>
                        <?php
                      }
                    ?>
                </div>
                <div class="col-sm-12" style="width:50%">
                   <table class="table table-bordered custdatatable">
                        <thead>
                           <tr>
                              <th class="col4" style="border: 1px solid #000;">Order id</th>
                              <th class="col4" style="border: 1px solid #000;">Product</th>
                              <th class="col4" style="border: 1px solid #000;">Weight Range</th>
                              <th class="col4" style="border: 1px solid #000;">Quantity</th>
                              <th class="col4" style="border: 1px solid #000;">Net Weight</th>
                           </tr>
                        </thead>
                        <tbody>
                          <?php 
                            foreach ($receipt_code as $key => $value) {
                              ?>
                               <tr>
                               <?php
                               if($result['is_kundan'] == true){
                                ?>
                                <th class="col4" style="border: 1px solid #000;"><?php echo $value['kmm_id'];?></th>
                                <?php
                               }else{
                               ?>
                                  <th class="col4" style="border: 1px solid #000;"><?php echo $value['order_id'];?></th>
                                <?php
                                }
                                ?>  
                                  <th class="col4" style="border: 1px solid #000;"><?php echo $value['name'];?></th>
                                  <th class="col4" style="border: 1px solid #000;"><?php echo $value['weights'];?></th>
                                  <th class="col4" style="border: 1px solid #000;"><?php echo $value['quantity'];?></th>
                                  <th class="col4" style="border: 1px solid #000;"><?php echo $value['net_wt'];?></th>
                               </tr>
                              <?php
                            }
                          ?>
                        </tbody>
                     </table>

                </div>
              </div> 
              <div class="clearfix"></div>
            </div>
               </div>
             </div>
            </div>
         </div>
      </div>
   </div>
</div>
</center>
<style>
.space
{
  height: 30px;
}
.space1
{
  height: 20px;
}
.karigar_name
{
  font-weight:bold;
}
.product_listing >div
{
  background-color: #fff;
}
.barcode_text {
    color: #000;
    font-size: 10px;
    font-weight: bold;
    text-align: left;
    margin-left: 10px;
}

table td{
  padding: 3px 5px;
}

@media print {
  /*body * {
    visibility: hidden;
  }*/
  .print_area, .print_area * {
    visibility: visible;
  }
  .print_area {
    
    left: 0;
    top: 0;
  }
  

}

</style>