<!DOCTYPE html>
<html>

<head>
    <meta name="google" content="notranslate" />
</head>

<body>

    <div style="width: 90%; margin: 0 auto;">
        <h2 style="text-align: center;">Hallmarking Issue Voucher</h2>
        <table style="border: 1px solid black;   border-collapse: collapse;
    width: 100%;">
            
            <!--  <tr>
                <th colspan="5" style="text-align:center;border: 1px solid black;  padding: 6px;">Inter Dept Transfer Voucher</th>
              
            </tr> -->
            <!-- <tr>
                <td style="border: 1px solid black;  padding: 6px;">Dept From</td>
                <td style="border: 1px solid black;  padding: 6px;background-color:yellow">MFG</td>
                <td style="border: 1px solid black;  padding: 6px;"></td>
                <td style="border: 1px solid black;  padding: 6px;"></td>
                <td style="border: 1px solid black;  padding: 6px;background-color:yellow"></td>
                <td style="border: 1px solid black;  padding: 6px;">Dept To</td>
                <td style="border: 1px solid black;  padding: 6px;background-color:yellow"><?=$data[0]['department_name']?></td>

            </tr> -->

            <tr>
                
                <td style="border: 1px solid black;  padding: 6px;">Date</td>
                <td style="border: 1px solid black;  padding: 6px;"><?= date('Y-m-d')?></td>
                <td style="border: 1px solid black;  padding: 6px;"></td>
                <td style="border: 1px solid black;  padding: 6px;"></td>
                <td style="border: 1px solid black;  padding: 6px;"></td>
                <!-- <td style="border: 1px solid black;  padding: 6px;"></td>
                <td style="border: 1px solid black;  padding: 6px;"></td> -->

            </tr>
            <tr>
                <td colspan="5" style="border: 1px solid black;  padding: 6px;"></td>

            </tr>

           
            <tr>
                <th style=" text-align: left; border: 1px solid black; padding: 6px; width: 100px;">Sr .No</th>
                <th style=" text-align: left; border: 1px solid black;  padding: 6px;  width: 100px;">Item Code</th>
                <th style=" text-align: left; border: 1px solid black; padding: 6px;  width: 100px;">Nt WT.</th>
                <th style=" text-align: left; border: 1px solid black;  padding: 6px;  width: 100px;">Center</th>
                <th style=" text-align: left; border: 1px solid black;  padding: 6px;  width: 100px;">Logo</th>
                <!-- <th style=" text-align: left; border: 1px solid black;  padding: 6px;  width: 100px;">IMG</th>
                <th style=" text-align: left; border: 1px solid black;  padding: 6px;  width: 100px;">Narration</th> -->
            </tr>
            <?php 
                $total_gr_wt=0;
            foreach ($data as $key => $value) { 
                $total_gr_wt += $value['net_wt'];
                ?>
                <tr>
                <td style=" text-align: left; border: 1px solid black; padding: 6px; width: 100px;"><?=$key+1?></td>
                <td style=" text-align: left; border: 1px solid black;  padding: 6px;  width: 100px;"><?=$value['sub_category']?></td>
                <td style=" text-align: left; border: 1px solid black; padding: 6px;  width: 100px;"><?=$value['net_wt']?></td>
                <td style=" text-align: left; border: 1px solid black;  padding: 6px;  width: 100px;"><?=$value['hc_name']?></td>
                <td style=" text-align: left; border: 1px solid black;  padding: 6px;  width: 100px;"><?=$value['hc_logo']?></td>
                <!-- <td style=" text-align: left; border: 1px solid black;  padding: 6px;  width: 100px;">-</td>
                <td style=" text-align: left; border: 1px solid black;  padding: 6px;  width: 100px;"></td> -->
            </tr>
           <?php } ?>
            <tr>
                <th colspan="2" style=" text-align: left; border: 1px solid black; padding: 6px; width: 100px;">TOTAL</th>
               
                <th style=" text-align: left; border: 1px solid black; padding: 6px;  width: 100px;"><?=$total_gr_wt?></th>
                <th colspan="2" style=" text-align: left; border: 1px solid black;  padding: 6px;  width: 100px;"></th>
                <!-- <th colspan="2" style=" text-align: left; border: 1px solid black;  padding: 6px;  width: 100px;"></th> -->
            </tr>
        </table>
    </div>
</body>
</html>
