<div class="content-page">
<div class="content">
   <div class="container">
      <div class="row">
         <div class="col-sm-12 card-box-wrap">
            <div class="panel panel-default">
              <div class="panel-heading">
                 <h4 class="inlineBlock"><?= $display_name;?>
                 </h4>
                    <div class="pull-right wrap-1024">
                <a href="<?= ADMIN_PATH .'Mfg_dep_direct_order/create' ?>">
                    <button type="button" class="add_senior_manager_button btn btn-purp waves-effect w-md waves-light m-b-5 btn-md">Receive add-hoc orders </button></a>
                <a href="<?= ADMIN_PATH?>Manufacturing_quality_control/karigar_receipt" type="button" class="add_senior_manager_button btn btn-primary waves-effect w-md waves-light m-b-5 btn-md">Receive products from karigar</a>                
                </div>
  
               </div>
                <!-- <button  type="button" onclick="export_qc_products()" class="add_senior_manager_button btn btn-primary  waves-effect w-md waves-light m-b-5 hidden-xs  btn-md">Export</button> -->
               <div class="panel-body">
               <div class="table-rep-plugin">
                  <div class="table-responsive b-0 scrollhidden">

           <?php 
                    if ($dep_id != '2' && $dep_id !='3' && $dep_id !='6' && $dep_id !='10') {
                      $table_name='received_orders';
                    } else {
                      $table_name='received_orders_bombay';
                    }
                    
                  ?>
              <form enctype='multipart/form-data' class="check_all_qc" role="form" name="product_form" id="export" action="Manufacturing_quality_control/check_all" method="post">
               <table class="table table-bordered custdatatable" id="<?php echo $table_name;?>">
                  <thead>
                     <?php
                          $data['heading']=$table_name;
                          $this->load->view('master/table_header',$data);
                        ?>
                  <!--   <tr>
                      <th class="col4"/>#</th>
                      <th class="col4">Order Id</th>
                      <th class="col4">Order Date</th>
                      <th class="col4">Karigar Name</th>
                      <th class="col4">Product</th>
                      <th class="col4">Weight Range</th>
                      <th class="col4">Order Quantity</th>
                      <th class="col4">Pending Quantity</th>
                    </tr> -->
                  </thead>
               </table>
              </form>
               </div>
               </div>
               </div>
            </div>
         </div>
      </div>
   </div>
</div>