<div class="content-page">
<div class="content">
   <div class="container">
      <div class="row">
         <div class="col-sm-12 card-box-wrap">
            <div class="card-box padmobile">
               <h4 class="header-title business_manager_header_title m-t-0 m-b-30 bmsmheader bmsmheaderabm"><span><?= $display_name;?></span>
               </h4>
               <div class="clearfix"></div>
               <div class="table-rep-plugin">
                  <div class="table-responsive b-0 scrollhidden">
               <table class="table table-bordered">
                  <thead>
                    <tr>
                      <th class="col4">#</th>
                      <th class="col4">Order Id</th>
                      <th class="col4">Receipt Code</th>
                      <th class="col4">Parent Category</th>
                      <th class="col4">Quantity</th>
                      <th class="col4">Weight Range</th>
                      <th class="col4">Rejected Weight</th>
                    </tr>
                  </thead>
                  <tbody>
                    <?php 
                      foreach ($result as $key => $value) {
                        ?>
                        <tr>
                          <td><?= $key+1?></td>
                          <td><?= $value['order_id']; ?></td>
                          <td><?= $value['receipt_code']; ?></td>
                          <td><?= $value['name']; ?></td>
                          <td><?= $value['am_quantity']; ?></td>
                          <td><?= $value['from_weight'].'-'.$value['to_weight']; ?></td>
                          <td><?= $value['rejected_weight']; ?></td>
                        </tr>
                        <?php
                      }
                    ?>
                  </tbody>
               </table>
               </div>
               </div>
            </div>
         </div>
      </div>
   </div>
</div>