<div class="content-page">
<div class="content">
   	<div class="container">
      <div class="row">
        <div class="col-sm-12 card-box-wrap">
            <div class="panel panel-default">
            	<div class="panel-heading">
               		<h4 class="inlineBlock"><span><?= strtoupper($page_title);?></span></h4>            		
            	</div>
               	<form name="category" id="qc_check" role="form" class="form-horizontal" method="post">
               		<div class="col-md-10 col-xs-12 m-t-15">
		               	<div class="row form-group">
		               		<div class="col-sm-4">
			       				<label class="col-sm-6 control-label " for="inputEmail3">Order ID </label>
			       				<div class="col-sm-6">
			       				
			       					<label class="control-label" for="inputEmail3"><?=$order_data['manufacturing_order_id']?></label>
			       				</div>
			       			</div>
			       			<div class="col-sm-4">
			       				<label class="col-sm-6 control-label" for="inputEmail3">Sub Category </label>
			       				<div class="col-sm-6">
			       					<label class="control-label" for="inputEmail3"><?=$order_data['name']?></label>
			       				</div>
			       			</div>
			       			<div class="col-sm-4">
			       				<label class="col-sm-6 control-label" for="inputEmail3">Quantity <span class="asterisk">*</span></label>
			       				<div class="col-sm-6">
		                       		<input type="text" placeholder="Quantity" id="name" class="form-control" name="quantity" value="1">
		                       		<span class="text-danger" id="quantity_error"></span>
		                    	</div>
			       			</div>
		               	</div>
		                <div class="row form-group">
		                    <div class="col-sm-4">
			       				<label class="col-sm-6 control-label" for="inputEmail3">Net WT. <span class="asterisk">*</span></label>
			       				<div class="col-sm-6">
		                       		<input type="text" placeholder="Net WT." id="net_wt" class="form-control" name="net_wt" value="<?=@$result['net_wt']?>">
		                       		<span class="text-danger" id="net_wt_error"></span>
		                    	</div>
			       			</div>
			       			<div class="col-sm-4">
			       				<label class="col-sm-6 control-label" for="inputEmail3">Few WT. <span class="asterisk">*</span></label>
			       				<div class="col-sm-6">
		                       		<input type="text" placeholder="Few WT." id="few_wt" class="form-control" name="few_wt" value="<?=@$result['few_wt']?>">
		                       		<span class="text-danger" id="few_wt_error"></span>
		                    	</div>
			       			</div>
			       			<div class="col-sm-4">
			       				<label class="col-sm-6 control-label" for="inputEmail3">MKS WT. <span class="asterisk">*</span></label>
			       				<div class="col-sm-6">
		                       		<input type="text" placeholder="MKS WT." id="mks_wt" class="form-control" name="mks_wt" value="<?=@$result['mks_wt']?>">
		                       		<span class="text-danger" id="mks_wt_error"></span>
		                    	</div>
			       			</div>
		                </div>
		                <div class="row form-group">
		                    <div class="col-sm-4">
			       				<label class="col-sm-6 control-label" for="inputEmail3">Stone WT. <span class="asterisk">*</span></label>
			       				<div class="col-sm-6">
		                       		<input type="text" placeholder="Stone WT." id="stone_wt" class="form-control" name="stone_wt" value="<?=@$result['stone_wt']?>">
		                       		<span class="text-danger" id="stone_wt_error"></span>
		                    	</div>
			       			</div>
			       			<div class="col-sm-4">
			       				<label class="col-sm-6 control-label" for="inputEmail3">Kundan WT. <span class="asterisk">*</span></label>
			       				<div class="col-sm-6">
		                       		<input type="text" placeholder="Kundan WT." id="kundan_wt" class="form-control" name="kundan_wt" value="<?=@$result['kundan_wt']?>">
		                       		<span class="text-danger" id="kundan_wt_error"></span>
		                    	</div>
			       			</div>
			       			<div class="col-sm-4">
			       				<label class="col-sm-6 control-label" for="inputEmail3">Kundan PC <span class="asterisk">*</span></label>
			       				<div class="col-sm-6">
		                       		<input type="text" placeholder="Kundan PC" id="kundan_pc" class="form-control" name="kundan_pc" value="<?=@$result['kundan_pc']?>">
		                       		<span class="text-danger" id="kundan_pc_error"></span>
		                    	</div>
			       			</div>
		                </div>
		                <div class="row form-group">
		                    <div class="col-sm-4">
			       				<label class="col-sm-6 control-label" for="inputEmail3">Stone Amount <span class="asterisk">*</span></label>
			       				<div class="col-sm-6">
		                       		<input type="text" placeholder="Stone Amount" id="stone_amt" class="form-control" name="stone_amt" value="<?=@$result['stone_amt']?>">
		                       		<span class="text-danger" id="stone_amt_error"></span>
		                    	</div>
			       			</div>
			       			<div class="col-sm-4">
			       				<label class="col-sm-6 control-label" for="inputEmail3">Kundan Amount <span class="asterisk">*</span></label>
			       				<div class="col-sm-6">
		                       		<input type="text" placeholder="Kundan Amount" id="kundan_amt" class="form-control" name="kundan_amt" value="<?=@$result['kundan_amt']?>">
		                       		<span class="text-danger" id="kundan_amt_error"></span>
		                    	</div>
			       			</div>
			       			<div class="col-sm-4">
			       				<label class="col-sm-6 control-label" for="inputEmail3">Other Amount <span class="asterisk">*</span></label>
			       				<div class="col-sm-6">
		                       		<input type="text" placeholder="Other Amount" id="other_amt" class="form-control" name="other_amt" value="<?=@$result['other_amt']?>">
		                       		<span class="text-danger" id="other_amt_error"></span>
		                    	</div>
			       			</div>
			       		</div>
	               	</div>
	               	<div class="col-md-10 col-xs-8">
					  <div class="table-rep-plugin">
					    <div class="table-responsive b-0 scrollhidden">
					      <table class="table custdatatable table-bordered" id="sub_catagory">
					        <thead>
					           <tr>
					               <th class="col4">Sr NO</th>
					               <th class="col4">Parameter</th>
					               <th class="col4">QC</th>
					              <th class="col3">Remark</th>
					           </tr>
					        </thead>
					        <tbody>
					        	<tr>
					        		<td>1</td>
					        		<td>BOM CHECKING/RRL CODE</td>
					        		<td>
					        		<div class="checkbox checkbox-purpal">
					        			<input data-parsley-multiple="group1" data-parsley-id="76" type="checkbox" name="bom_checking/rrl_code" value="1" class="from-control" <?=(@$result['bom_checking/rrl_code'] == true) ? 'checked' : ''?>  <?=(@$receive_from_hallmarking == false) ? 'checked' : '' ?>>
					        			<label for="bom_checking"></label>
					        		</div>
					        		</td>
					        		<td><textarea name="bom_checking/rrl_code_remark" class="txt-box"></textarea></td>
					        	</tr>
					        	<tr>
					        		<td>2</td>
					        		<td>DESIGN CHECKING</td>
					        		<td>
					        		<div class="checkbox checkbox-purpal">
					        			<input data-parsley-multiple="group1" data-parsley-id="76" type="checkbox" name="design_checking" value="1" class="from-control" <?=(@$result['design_checking'] == true) ? 'checked' : ''?> <?=(@$receive_from_hallmarking == false) ? 'checked' : '' ?>>
					        			<label for="design_checking"></label>
					        		</div>
									</td>
					        		<td><textarea name="design_checking_remark" class="txt-box" ></textarea></td>
					        	</tr>
					        	<tr>
					        		<td>3</td>
					        		<td>PAIR MATCHING</td>
					        		<td>
					        		<div class="checkbox checkbox-purpal">
					        			<input data-parsley-multiple="group1" data-parsley-id="76" type="checkbox" name="pair_matching"  value="1" class="from-control" <?=(@$result['pair_matching'] == true) ? 'checked' : ''?> <?=(@$receive_from_hallmarking == false) ? 'checked' : '' ?>>
					        			<label for="pair_matching"></label>
					        		</div>
									</td>
					        		<td><textarea name="pair_matching_remark" class="txt-box" ></textarea></td>
					        	</tr>
					        	<tr>
					        		<td>4</td>
					        		<td>CARATAGE/PURITY</td>
					        		<td>
					        		<div class="checkbox checkbox-purpal">
					        			<input data-parsley-multiple="group1" data-parsley-id="76" type="checkbox" name="karatage/purity" value="1" class="from-control" <?=(@$result['karatage/purity'] == true) ? 'checked' : ''?> <?=(@$receive_from_hallmarking == false) ? 'checked' : '' ?>>
					        			<label for="karatage/purity"></label>
					        		</div>
									</td>
					        		<td><textarea name="karatage/purity_remark" class="txt-box" ></textarea></td>
					        	</tr>
					        	<tr>
					        		<td>5</td>
					        		<td>SIZE</td>
					        		<td>
					        			<div class="checkbox checkbox-purpal">
					        			<input data-parsley-multiple="group1" data-parsley-id="76" type="checkbox" name="size" value="1" class="from-control" <?=(@$result['size'] == true) ? 'checked' : ''?>  <?=(@$receive_from_hallmarking == false) ? 'checked' : '' ?>>
					        			<label for="size"></label>
					        		</div>
									</td>
					        		<td><textarea name="size_remark" class="txt-box"></textarea></td>
					        	</tr>
					        	<tr>
					        		<td>6</td>
					        		<td>STAMPING</td>
					        		<td>
					        			<div class="checkbox checkbox-purpal">
					        			<input data-parsley-multiple="group1" data-parsley-id="76" type="checkbox" name="stamping" value="1" class="from-control" <?=(@$result['stamping'] == true) ? 'checked' : ''?> <?=(@$receive_from_hallmarking == false) ? 'checked' : '' ?>>
					        			<label for="stamping"></label>
					        		</div>
									</td>
					        		<td><textarea name="stamping_remark" class="txt-box"></textarea></td>
					        	</tr>
					        	<tr>
					        		<td>7</td>
					        		<td>SHARP EDGE</td>
					        		<td>
					        			<div class="checkbox checkbox-purpal">
					        			<input data-parsley-multiple="group1" data-parsley-id="76" type="checkbox" name="sharp_edge" value="1" class="from-control" <?=(@$result['sharp_edge'] == true) ? 'checked' : ''?> <?=(@$receive_from_hallmarking == false) ? 'checked' : '' ?>>
					        			<label for="sharp_edge"></label>
					        		</div>
									</td>
					        		<td><textarea name="sharp_edge_remark" class="txt-box" ></textarea></td>
					        	</tr>
					        	<tr>
					        		<td>8</td>
					        		<td>SOLDER/LINKING</td>
					        		<td>
					        			<div class="checkbox checkbox-purpal">
					        			<input data-parsley-multiple="group1" data-parsley-id="76" type="checkbox" name="solder/linking" value="1" class="from-control" <?=(@$result['solder/linking'] == true) ? 'checked' : ''?> <?=(@$receive_from_hallmarking == false) ? 'checked' : '' ?>>
					        			<label for="solder/linking"></label>
					        		</div>
									</td>
					        		<td><textarea name="solder/linking_remark" class="txt-box" ></textarea></td>
					        	</tr>
					        	<tr>
					        		<td>9</td>
					        		<td>SHAPE OUT</td>
					        		<td>
					        			<div class="checkbox checkbox-purpal">
					        			<input data-parsley-multiple="group1" data-parsley-id="76" type="checkbox" name="shape_out" value="1" class="from-control" <?=(@$result['shape_out'] == true) ? 'checked' : ''?> <?=(@$receive_from_hallmarking == false) ? 'checked' : '' ?>>
					        			<label for="shape_out"></label>
					        		</div>
									</td>
					        		<td><textarea name="shape_out_remark" class="txt-box"></textarea></td>
					        	</tr>
					        	<tr>
					        		<td>10</td>
					        		<td>FINISHING</td>
					        		<td>
					        			<div class="checkbox checkbox-purpal">
					        			<input data-parsley-multiple="group1" data-parsley-id="76" type="checkbox" name="finishing" value="1" class="from-control" <?=(@$result['finishing'] == true) ? 'checked' : ''?> <?=(@$receive_from_hallmarking == false) ? 'checked' : '' ?>>
					        			<label for="finishing"></label>
					        		</div>

									</td>
					        		<td><textarea name="finishing_remark" class="txt-box"></textarea></td>
					        	</tr>
					        	<tr>
					        		<td>11</td>
					        		<td>GR WT/NET WT</td>
					        		<td>
					        			<div class="checkbox checkbox-purpal">
					        			<input data-parsley-multiple="group1" data-parsley-id="76" type="checkbox" name="gr_wt/nt_wt" value="1" class="from-control" <?=(@$result['gr_wt/nt_wt'] == true) ? 'checked' : ''?> <?=(@$receive_from_hallmarking == false) ? 'checked' : '' ?>>
					        			<label for="gr_wt/nt_wt"></label>
					        		</div>
									</td>
					        		<td><textarea name="gr_wt/nt_wt_remark" class="txt-box"></textarea></td>
					        	</tr>
					        	<tr>
					        		<td>12</td>
					        		<td>TAG DETAIL</td>
					        		<td>
					        			<div class="checkbox checkbox-purpal">
					        			<input data-parsley-multiple="group1" data-parsley-id="76" type="checkbox" name="tag_detail" value="1" class="from-control" <?=(@$result['tag_detail'] == true) ? 'checked' : ''?> <?=(@$receive_from_hallmarking == false) ? 'checked' : '' ?>>
					        			<label for="tag_detail"></label>
					        		</div>
									</td>
					        		<td><textarea name="tag_detail_remark" class="txt-box"></textarea></td>
					        	</tr>
					        	<tr>
					        		<td>13</td>
					        		<td>FINDING/LOCK</td>
					        		<td>
					        			<div class="checkbox checkbox-purpal">
					        			<input data-parsley-multiple="group1" data-parsley-id="76" type="checkbox" name="finding/lock" value="1" class="from-control" <?=(@$result['finding/lock'] == true) ? 'checked' : ''?> <?=(@$receive_from_hallmarking == false) ? 'checked' : '' ?>>
					        			<label for="finding/lock"></label>
					        		</div>
									</td>
					        		<td><textarea name="finding/lock_remark" class="txt-box"></textarea></td>
					        	</tr><tr>
					        		<td>14</td>
					        		<td>WEARING/TEST</td>
					        		<td>
					        			<div class="checkbox checkbox-purpal">
					        			<input data-parsley-multiple="group1" data-parsley-id="76" type="checkbox" name="wearing_test" value="1" class="from-control" <?=(@$result['wearing_test'] == true) ? 'checked' : ''?> <?=(@$receive_from_hallmarking == false) ? 'checked' : '' ?>>
					        			<label for="wearing_test"></label>
					        		</div>
									</td>
					        		<td><textarea name="wearing_test_remark" class="txt-box"></textarea></td>
					        	</tr><tr>
					        		<td>15</td>
					        		<td>ALIGNMENT</td>
					        		<td>
					        			<div class="checkbox checkbox-purpal">
					        			<input data-parsley-multiple="group1" data-parsley-id="76" type="checkbox" name="alignment" value="1" class="from-control" <?=(@$result['alignment'] == true) ? 'checked' : ''?> <?=(@$receive_from_hallmarking == false) ? 'checked' : '' ?>>
					        			<label for="alignment"></label>
					        		</div>
									</td>
					        		<td><textarea name="alignment_remark" class="txt-box"></textarea></td>
					        	</tr>
					        	<tr>
					        		<td>16</td>
					        		<td>PACKING</td>
					        		<td>
					        			<div class="checkbox checkbox-purpal">
					        			<input data-parsley-multiple="group1" data-parsley-id="76" type="checkbox" name="packing" value="1" class="from-control txt-box" <?=(@$result['packing'] == true) ? 'checked' : ''?> <?=(@$receive_from_hallmarking == false) ? 'checked' : '' ?>>
					        			<label for="packing"></label>
					        		</div>

									</td>
					        		<td><textarea name="packing_remark" class="txt-box"></textarea></td>
					        	</tr>
					        	<tr>
					        		<td>17</td>
					        		<td>HALLMARKING</td>
					        		<td>
					        			<div class="checkbox checkbox-purpal">
					        			<input data-parsley-multiple="group1" data-parsley-id="76" type="checkbox" id="hallmarking_checked" name="hallmarking" value="1" class="from-control txt-box" <?=(@$result['hallmarking'] == true) ? 'checked' : ''?> <?=(!isset($result['bom_checking/rrl_code'])) ? 'checked' : '' ?>>
					        			<label for="hallmarking"></label>
					        		</div>

									</td>
					        		<td><textarea name="hallmarking_remark" class="from-control"><?=$result['hallmarking_remark'] ?></textarea></td>
					        	</tr>
					        </tbody>
					     </table>
					   </div>
					  </div>
						<input type="hidden" name="qc_id" value="<?=$result['id'] ?>">
						<div class="col-md-12 col-xs-12 mr10">
	                       <textarea placeholder="Enter Special Remark" id="name" class="col-xs-12 col-md-12 col-lg-12 col-sm-12 txt-box form-control" name="remark" rows="4"></textarea> 
							<span class="text-danger" id="remark_error"></span>
						</div>
					</div>					
					<div class="form-group">
						<div class="col-sm-offset-5 col-sm-8 texalin m-t-15">
							<button class="btn btn-primary waves-effect waves-light  btn-md" name="commit" type="button" onclick="accept_m_hallmarking_qc(); ">ACCEPT</button>
							<button class="btn btn-primary waves-effect waves-light  btn-md" name="commit" type="button" onclick="reject_m_hallmarking_qc(); ">REJECT</button>
						</div>
          			</div>
          		</form>
         	</div>
      	</div>     
   	</div>
</div>