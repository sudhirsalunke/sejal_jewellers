<!-- <div class="content-page">
<div class="content">
   <div class="container">
      <div class="row">
         <div class="col-sm-12 card-box-wrap">
            <div class="card-box padmobile">
               <h4 class="header-title business_manager_header_title m-t-0 m-b-30 bmsmheader bmsmheaderabm"><span><?= $display_name;?></span>
              </h4>
               <div class="clearfix"></div>
               <div class="table-rep-plugin">
                  <div class="table-responsive b-0 scrollhidden" >
              <form enctype='multipart/form-data' role="form" name="product_form" id="export">
                <div class="col-sm-12 col-xs-12 m-t-15">
                   <div class="row col-sm-8 form-group">
                    <div class="col-sm-4 no_pad">
                    <label class="col-sm-5 control-label " for="inputEmail3">Name </label>
                    <div class="col-sm-5"><?= $this->session->userdata('first_name').' '.$this->session->userdata('last_name')?></div>
                  </div>
                  <div class="col-sm-4 no_pad">
                    <label class="col-sm-5 control-label" for="inputEmail3">Employee ID </label>
                    <div class="col-sm-5"><?= $this->session->userdata('user_id')?></div>
                  </div>
                   </div>
                    <div class="row col-sm-8 form-group">
                      <div class="col-sm-4 no_pad">
                        <label class="col-sm-5 control-label" for="inputEmail3">Quantity </label>
                        <div class="col-sm-5"><?= $result['quantity']?> </div>
                      </div>
                    
                      <div class="col-sm-4 no_pad">
                        <label class="col-sm-5 control-label" for="inputEmail3">Total GR. WT. </label>
                        <div class="col-sm-5"><?= $result['total_gr_wt']?></div>
                      </div>
                      <div class="col-sm-4 no_pad">
                        <label class="col-sm-5 control-label" for="inputEmail3">Total NET. WT. </label>
                        <div class="col-sm-5"><?= $result['total_net_wt']?></div>
                      </div>
                   </div>
                 </div>
                   <?php
                 if($result['bom_checking/rrl_code_remark']!='' || $result['design_checking_remark']!='' ||$result['karatage/purity_remark']!=''|| $result['size_remark']!='' || $result['stamping_remark']!='' || $result['sharp_edge_remark']!='' || $result['solder/linking_remark']!='' || $result['shape_out_remark']!='' || $result['finishing_remark']!='' || $result['gr_wt/nt_wt_remark']!='' || $result['tag_detail_remark']!='' || $result['finding/lock_remark']!='' || $result['wearing_test_remark']!='' || $result['alignment_remark']!='' || $result['packing_remark']!='' || $result['hallmarking_remark']!=''){
                 ?>
              <table class="table custdatatable table-bordered" id="sub_catagory" >
                  <thead>
                     <tr>
                         <th class="col4">#</th>
                         <th class="col4">Parameter</th>
                        <th class="col3">Remark</th>
                     </tr>
                  </thead>
                  <tbody>
                    <?php if($result['bom_checking/rrl_code_remark']!=''){ ?>
                    <tr>
                      <td>1</td>
                      <td>BOM CHECKING/RRL CODE</td>
                      <td><?=$result['bom_checking/rrl_code_remark'] ?></td>
                    </tr>
                    <?php }if($result['design_checking_remark']!=''){ ?>
                    <tr>
                      <td>2</td>
                      <td>DESIGN CHECKING</td>
                      <td><?=$result['design_checking_remark'] ?></td>
                    </tr>
                     <?php }if($result['pair_matching_remark']!=''){ ?>
                    <tr>
                      <td>3</td>
                      <td>PAIR MATCHING</td>
                      <td><?=$result['pair_matching_remark'] ?></td>
                    </tr>
                    <?php }if($result['karatage/purity_remark']!=''){ ?>
                    <tr>
                      <td>4</td>
                      <td>CARATAGE/PURITY</td>
                      <td><?=$result['karatage/purity_remark'] ?></td>
                    </tr>
                    <?php }if($result['size_remark']!=''){ ?>
                    <tr>
                      <td>5</td>
                      <td>SIZE</td>
                      <td><?=$result['size_remark'] ?></td>
                    </tr>
                    <?php }if($result['stamping_remark']!=''){ ?>
                    <tr>
                      <td>6</td>
                      <td>STAMPING</td>
                      <td><?=$result['stamping_remark'] ?></td>
                    </tr>
                    <?php }if($result['sharp_edge_remark']!=''){ ?>
                    <tr>
                      <td>7</td>
                      <td>SHARP EDGE</td>
                      <td><?=$result['sharp_edge_remark'] ?></td>
                    </tr>
                    <?php }if($result['solder/linking_remark']!=''){ ?>
                    <tr>
                      <td>8</td>
                      <td>SOLDER/LINKING</td>
                      <td><?=$result['solder/linking_remark'] ?></td>
                    </tr>
                    <?php }if($result['shape_out_remark']!=''){ ?>
                    <tr>
                      <td>9</td>
                      <td>SHAPE OUT</td>
                      <td><?=$result['shape_out_remark'] ?></td>
                    </tr>
                    <?php }if($result['finishing_remark']!=''){ ?>
                    <tr>
                      <td>10</td>
                      <td>FINISHING</td>
                      <td><?=$result['finishing_remark'] ?></td>
                    </tr>
                    <?php }if($result['gr_wt/nt_wt_remark']!=''){ ?>
                    <tr>
                      <td>11</td>
                      <td>GR WT/NET WT</td>
                      <td><?=$result['gr_wt/nt_wt_remark'] ?></td>
                    </tr>
                    <?php }if($result['tag_detail_remark']!=''){ ?>
                    <tr>
                      <td>12</td>
                      <td>TAG DETAIL</td>
                      <td><?=$result['tag_detail_remark'] ?></td>
                    </tr>
                    <?php }if($result['finding/lock_remark']!=''){ ?>
                    <tr>
                      <td>13</td>
                      <td>FINDING/LOCK</td>
                      <td><?=$result['finding/lock_remark'] ?></td>
                    </tr>
                    <?php }if($result['wearing_test_remark']!=''){ ?>
                    <tr>
                      <td>14</td>
                      <td>WEARING/TEST</td>
                      <td><?=$result['wearing_test_remark'] ?></td>
                    </tr>
                    <?php }if($result['alignment_remark']!=''){ ?>
                    <tr>
                      <td>15</td>
                      <td>ALIGNMENT</td>
                      <td><?=$result['alignment_remark'] ?></td>
                    </tr>
                    <?php }if($result['packing_remark']!=''){ ?>
                    <tr>
                      <td>16</td>
                      <td>PACKING</td>
                      <td><?=$result['packing_remark'] ?></td>
                    </tr>
                    <?php }if($result['hallmarking_remark']!=''){ ?>
                    <tr>
                      <td>17</td>
                      <td>HALLMARKING</td>
                      <td><?=$result['hallmarking_remark'] ?></td>
                    </tr>
                    <?php }?>
                    </tbody>
               </table>
                <?php }

                if($result['remark']!=''){ ?>
                    <table class="table custdatatable table-bordered">
                      <thead>
                        <tr>
                          <th class="col4">Special Remark</th>
                        </tr>
                      </thead>
                      <tbody>
                      <tr>
                      <td><?=$result['remark'] ?></td>
                    </tr>
                    <?php } ?>
               
                  </tbody>
               </table>
              </form>
               </div>
               </div>
            </div>
         </div>
      </div>
   </div>
</div> -->