<?php        
   $process_details=getDashboardCartSettings($display_title);
          
?>
<div class="content-page">
  <div class="content">
    <div class="container">
      <div class="row">
        <div class="col-sm-12 card-box-wrap">
          <div class="card-box padmobile dash-board">
            <h5 class=" m-b-20"><!--header-title business_manager_header_title-->
            <span><?=$display_title;?></span>
            </h5>    
            
            <?php 
            foreach ($process_details as $key => $value) {                   
        
             ?>


             <h4 class="m-b-20 header-title"><span><?= $key;?></span></h4>
             <div class="row">
             <div class="col-sm-12 col-md-12 col-xs-12 product-box" style="padding-left: 0;padding-right: 0;">             
            <?php foreach ($value as $key1 => $val) { 

              ?>

                <div class="col-lg-3 col-md-3 col-sm-6 " data-aos="zoom-in" data-aos-delay="800" 
                     data-aos-duration="1000">
                  <a href="<?= ADMIN_PATH.$val['process_link']?>">
                     <div class="card-box dash-box box-shadow <?=$val['process_class'];?> dash_box_content text-center">
                      
                      <div class="widget-chart-1 white_color_font">

                        <h4 class=""><?=$val['process'];?></h4>
                        <h2 class="number-size"> <?=$$val['process_count'];?> </h2>
                        <span class="label label-gray"><?=$val['label_gray'];?></span>
                  
                      </div>
                    </div>
                  </a>
                </div> 
                
       
              <?php 
                   } ?>
                    </div>
                    </div>
                   <?php
                 }
                   ?>

         
          </div>
          
              <!--===================================================-->
    </div><!-- end container -->
  </div><!-- end content -->
</div><!-- end content-page -->