
<?php        
        // $page_process_details=getDashboardCartSettings($display_title);
        // print_r($page_process_details);die;
?>
<div class="content-page">
  <div class="content">
    <div class="container">
      <div class="row">
        <div class="col-sm-12 card-box-wrap">
          <div class="card-box padmobile dash-board">

            <h5 class=" m-b-20"><!--header-title business_manager_header_title-->
            <span><?=$display_title;?></span>
            </h5> 
            <h4 class="m-b-20 header-title">
            <span>Orders</span>
            </h4>

            <div class="row">              
              <div class="col-md-12 col-xs-12 section-3 product-box marg_top-bottom no_pad">
                <div class="col-lg-3 col-md-3 col-sm-6" data-aos="zoom-out" data-aos-delay="400" 
     data-aos-duration="800">
                  <a href="<?= ADMIN_PATH.'Manufacturing_department_order' ?>">
                  <div class="card-box dash-box box-shadow darkBlue_box dash_box_content text-center">
                    
                    <div class="widget-chart-1 white_color_font ">
                      <!-- <div class="widget-detail-1"> -->
                        <h4 class="">Department Orders</h4>
                        <h2 class="number-size"> <?=$orders?>  </h2>
                        <span class="label label-gray">ORDERS</span>
                     <!--  </div> -->
                    </div>
                  </div></a>
                </div><!-- end col -->
                 <div class="col-lg-3 col-md-3 col-sm-6" data-aos="zoom-out" data-aos-delay="500" 
     data-aos-duration="800">
                  <a href="<?= ADMIN_PATH.'Karigar_order_list' ?>">
                   <div class="card-box dash-box box-shadow orange_box dash_box_content text-center">
                   
                    <div class="widget-chart-1 white_color_font">
                     <!--  <div class="widget-detail-1"> -->
                         <h4 class="">Not Sent To Karigar</h4>
                        <h2 class="number-size"> <?=$not_sent_to_karigar;?> </h2>
                        <span class="label label-gray">ORDERS</span>
                     <!--  </div> -->
                    </div>
                  </div></a>
                </div><!-- end col-->
                <div class="col-lg-3 col-md-3 col-sm-6" data-aos="zoom-out" data-aos-delay="500" 
     data-aos-duration="800">
                  <a href="<?= ADMIN_PATH.'Order_sent' ?>">
                   <div class="card-box dash-box box-shadow blue_box dash_box_content text-center">

                    <div class="widget-chart-1 white_color_font">
                      <!-- <div class="widget-detail-1"> -->
                        <h4 class="">Sent To Karigar</h4>
                        <h2 class="number-size"> <?=$sent_to_karigar;?> </h2>
                        <span class="label label-gray">ORDERS</span>
                      <!-- </div> -->
                    </div>
                  </div></a>
                </div>
                 <div class="col-lg-3 col-md-3 col-sm-6" data-aos="zoom-out" data-aos-delay="500" 
     data-aos-duration="800">
                  <a href="<?= ADMIN_PATH.'Manufacturing_engaged_karigar' ?>">
                  <div class="card-box dash-box box-shadow green_box dash_box_content text-center">

                    <div class="widget-chart-1 white_color_font">
                     <!--  <div class="widget-detail-1"> -->
                        <h4 class="">Karigar Engaged</h4>
                        <h2 class="number-size "> <?=$engaged_karigar;?> </h2>
                        <span class="label label-gray">ORDERS</span>
                      <!-- </div> -->
                    </div>
                  </div></a>
                </div>
              </div>
            </div>
            

              <!--==================================-->
              <h4 class="header-title business_manager_header_title m-b-30">
            <span>Products</span>
            </h4>

            <div class="row">              
              <div class="col-md-12 col-xs-12 section-3 product-box marg_top-bottom no_pad">
               
               <!-- <div class="col-lg-3 col-md-3 col-sm-6">
                  <a href="<?= ADMIN_PATH.'Mnfg_accounting' ?>"><div class="card-box dash-box box-shadow">
                    <h4 class="header-title m-t-0 m-b-30">Accounting Department</h4>
                    <div class="widget-chart-1">
                      <div class="widget-detail-1">
                        <h2 class="number-size light-blue-font"><?=$received_orders;?> </h2>
                        <span class="label label-gray">Orders</span>
                      </div>
                    </div>
                  </div></a>
                </div> -->

               <div class="col-lg-3 col-md-3 col-sm-6" data-aos="zoom-in" data-aos-delay="700" 
     data-aos-duration="800">
                  <a href="<?= ADMIN_PATH.'Received_orders' ?>">
                  <div class="card-box dash-box box-shadow purple_box dash_box_content text-center">
                    
                    <div class="widget-chart-1 white_color_font">
                     <!--  <div class="widget-detail-1"> -->
                        <h4 class="">Pending To Be Received </h4>
                        <h2 class="number-size light-blue-font"> <?=$received_orders;?> </h2>
                        <span class="label label-gray">ORDERS</span>
                     <!--  </div> -->
                    </div>
                  </div></a>
                </div>
                <div class="col-lg-3 col-md-3 col-sm-6" data-aos="zoom-in" data-aos-delay="800" 
                     data-aos-duration="800">
                  <a href="<?= ADMIN_PATH.'Received_receipt' ?>">
                   <div class="card-box dash-box box-shadow pink_box dash_box_content text-center">
                    
                    <div class="widget-chart-1 white_color_font">
                      <!-- <div class="widget-detail-1"> -->
                        <h4 class="">Pending QC</h4>
                        <h2 class="number-size"> <?=$received_receipt_mfg;?> </h2>
                        <span class="label label-gray">PRODUCTS</span>
                      <!-- </div> -->
                    </div>
                  </div></a>
                </div>
                <!-- <div class="col-lg-3 col-md-3 col-sm-6">
                  <a href="<?= ADMIN_PATH.'Manufacturing_quality_control?status=complete' ?>"><div class="card-box dash-box box-shadow">
                    <h4 class="header-title m-t-0 m-b-30">QC Accepted</h4>
                    <div class="widget-chart-1">
                      <div class="widget-detail-1">
                        <h2 class="number-size light-green-font"> <?=$approved_qc;?> </h2>
                        <span class="label label-gray">PRODUCTS</span>
                      </div>
                    </div>
                  </div></a>
                </div>-->
                <div class="col-lg-3 col-md-3 col-sm-6" data-aos="zoom-in" data-aos-delay="900" 
                   data-aos-duration="800">
                  <a href="<?= ADMIN_PATH.'Manufacturing_quality_control?status=rejected'?>">
                  <div class="card-box dash-box box-shadow red_box dash_box_content text-center">
                    
                    <div class="widget-chart-1 white_color_font">
                     <!--  <div class="widget-detail-1"> -->
                        <h4 class="">QC Rejected</h4>
                        <h2 class="number-size light-red-font"> <?=$rejected_qc;?> </h2>
                        <span class="label label-gray">PRODUCTS</span>
                     <!--  </div> -->
                    </div>
                  </div></a>
                </div><!-- end col -->             
                  <div class="col-lg-3 col-md-3 col-sm-6" data-aos="zoom-in" data-aos-delay="900" 
                   data-aos-duration="800">
                  <a href="<?= ADMIN_PATH.'repair_product'?>">
                  <div class="card-box dash-box box-shadow yellow_box dash_box_content text-center">
                    
                    <div class="widget-chart-1 white_color_font">
                     <!--  <div class="widget-detail-1"> -->
                        <h4 class="">Repair</h4>
                        <h2 class="number-size light-red-font"> <?=$repair_product;?> </h2>
                        <span class="label label-gray">PRODUCTS</span>
                     <!--  </div> -->
                    </div>
                  </div></a>
                </div>
              <!--   <div class="col-lg-3 col-md-3 col-sm-6">
                  <a href="<?= ADMIN_PATH.'Manufacturing_quality_control/resend_to_karagar' ?>"><div class="card-box dash-box box-shadow">
                    <h4 class="header-title m-t-0 m-b-30">Resent To Karigar</h4>
                    <div class="widget-chart-1">
                      <div class="widget-detail-1">
                        <h2 class="number-size light-red-font"> <?=$resend_to_karagir;?> </h2>
                        <span class="label label-gray">PRODUCTS</span>
                      </div>
                    </div>
                  </div></a>
                </div> --><!-- end col -->   
                  <!-- <div class="col-lg-3 col-md-3 col-sm-6">
                  <a href="<?= ADMIN_PATH.'Mnfg_accounting_rejected_orders'?>"><div class="card-box dash-box box-shadow">
                    <h4 class="header-title m-t-0 m-b-30">Manufacturing Rejected</h4>
                    <div class="widget-chart-1">
                      <div class="widget-detail-1">
                        <h2 class="number-size light-blue-font"><?=$rejected_products;?></h2>
                        <span class="label label-gray">Orders</span>
                      </div>
                    </div>
                  </div></a>
                </div>  -->            
              
            
            </div>
          </div>
            <!-- End row -->
            <?php if($display_title !='Bombay'){?>
            <h4 class="header-title business_manager_header_title m-b-30">
            <span>Hallmarking</span>
            </h4>

            <div class="row">              
              <div class="col-md-12 col-xs-12 section-3 product-box marg_top-bottom no_pad">

                 <div class="col-lg-3 col-md-3" data-aos="flip-left" data-aos-delay="1000" 
     data-aos-duration="800">
                  <a href="<?= ADMIN_PATH?>Manufacturing_hallmarking?status=send">
                   <div class="card-box dash-box box-shadow yellowGold_box dash_box_content text-center">
                    
                    <div class="widget-chart-1 white_color_font">
                      <!-- <div class="widget-detail-1"> -->
                        <h4 class="">Pending To Be Receive From HM</h4>
                        <h2 class="number-size"> <?=$sent_to_hallmarking;?> </h2>
                        <span class="label label-gray">PRODUCTS</span>
                      <!-- </div> -->
                    </div>
                  </div></a>
                </div><!-- end col-->
               
                <!--<div class="col-lg-3 col-md-3">
                  <a href="<?= ADMIN_PATH.'Manufacturing_hallmarking?status=receive' ?>"><div class="card-box dash-box box-shadow">
                    <h4 class="header-title m-t-0 m-b-30">Hallmarking QC Accepted</h4>
                    <div class="widget-chart-1">
                      <div class="widget-detail-1">
                        <h2 class="number-size light-green-font"><?=$approved_hallmarking;?></h2>
                        <span class="label label-gray">PRODUCTS</span>
                      </div>
                    </div>
                  </div></a>
                </div> --><!-- end col -->
                <!-- <div class="col-lg-3 col-md-3">
                  <a href="<?= ADMIN_PATH.'Manufacturing_hallmarking?status=rejected' ?>"><div class="card-box dash-box box-shadow">
                    <h4 class="header-title m-t-0 m-b-30">Hallmarking QC Rejected</h4>
                    <div class="widget-chart-1">
                      <div class="widget-detail-1">
                        <h2 class="number-size light-red-font"><?=$rejected_hallmarking;?></h2>
                        <span class="label label-gray">PRODUCTS</span>
                      </div>
                    </div>
                  </div></a>
                </div>--><!-- end col -->
                <!-- <div class="col-lg-3 col-md-3">
                  <a href="<?= ADMIN_PATH.'Make_set' ?>"><div class="card-box dash-box box-shadow">
                    <h4 class="header-title m-t-0 m-b-30">Make Set</h4>
                    <div class="widget-chart-1">
                      <div class="widget-detail-1">
                        <h2 class="number-size light-blue-font"><?=$make_set;?></h2>
                        <span class="label label-gray">SETS</span>
                      </div>
                    </div>
                  </div></a>
                </div> --><!-- end col -->
                <!-- <div class="col-lg-3 col-md-3">
                  <a href="<?= ADMIN_PATH.'Make_set' ?>"><div class="card-box dash-box box-shadow">
                    <h4 class="header-title m-t-0 m-b-30">Ready Products</h4>
                    <div class="widget-chart-1">
                      <div class="widget-detail-1">
                        <h2 class="number-size light-blue-font"></h2>
                        <span class="label label-gray">comming soon..</span>
                      </div>
                    </div>
                  </div></a>
                </div> --><!-- end col -->
                <!-- <div class="col-lg-3 col-md-3">
                  <a href="<?= ADMIN_PATH.'Make_set' ?>"><div class="card-box dash-box box-shadow">
                    <h4 class="header-title m-t-0 m-b-30">Not Ready Products</h4>
                    <div class="widget-chart-1">
                      <div class="widget-detail-1">
                        <h2 class="number-size light-blue-font"></h2>
                        <span class="label label-gray">comming soon..</span>
                      </div>
                    </div>
                  </div></a>
                </div> --><!-- end col -->

              <!-- </div></a>-->
            </div>

      </div><!-- end row -->
       <h4 class="header-title business_manager_header_title m-b-30">
            <span>Tagging</span>
            </h4>
           <div class="row">
              <!-- <div class="col-lg-3 col-md-3 col-sm-6">
                  <a href="<?= ADMIN_PATH.'Make_set' ?>">
                  <div class="card-box dash-box box-shadow">
                    <h4 class="header-title m-t-0 m-b-30">Make Set</h4>
                    <div class="widget-chart-1">
                      <div class="widget-detail-1">
                        <h2 class="number-size light-blue-font"><?=$make_set;?></h2>
                        <span class="label label-gray">SETS</span>
                      </div>
                    </div>
                  </div></a>
              </div> -->
               <div class="col-md-12 col-xs-12 section-3 product-box marg_top-bottom no_pad">
              <div class="col-lg-3 col-md-3 col-sm-6">
                  <a href="<?= ADMIN_PATH.'Manufacturing_quality_control?status=complete' ?>">
                  <div class="card-box dash-box box-shadow yellow_box dash_box_content text-center" data-aos="fade-down" data-aos-easing="linear" data-aos-delay="400" data-aos-duration="800">
                    
                    <div class="widget-chart-1 white_color_font">
                     <!--  <div class="widget-detail-1"> -->
                        <h4 class="">Pending For Tagging</h4>
                        <h2 class="number-size"> <?=$approved_qc;?></h2>
                        <span class="label label-gray">PRODUCTS</span>
                      <!-- </div> -->
                    </div>
                  </div></a>
              </div>
               <div class="col-lg-3 col-md-3 col-sm-6">
                  <a href="<?= ADMIN_PATH.'tag_products/create' ?>">
                  <div class="card-box dash-box box-shadow dark_purple_box dash_box_content text-center" data-aos="fade-down" data-aos-easing="linear" data-aos-delay="400" data-aos-duration="800">
                    
                    <div class="widget-chart-1 white_color_font">
                     <!--  <div class="widget-detail-1"> -->
                        <h4 class="">Tagged Products</h4>
                        <h2 class="number-size"><?=$tag_products?></h2>
                        <span class="label label-gray">PRODUCTS</span>
                      <!-- </div> -->
                    </div>
                  </div></a>
              </div>
               
                    <div class="col-lg-3 col-md-3 col-sm-6" data-aos="zoom-in" data-aos-delay="900" 
                   data-aos-duration="800">
                  <a href="<?= ADMIN_PATH.'Few_box' ?>">
                  <div class="card-box dash-box box-shadow darkBlue_box dash_box_content text-center">
                    
                    <div class="widget-chart-1 white_color_font">
                     <!--  <div class="widget-detail-1"> -->
                        <h4 class="">Few Box</h4>
                        <h2 class="number-size light-red-font"><?= $few_weight;?></h2>
                        <span class="label label-gray">PRODUCTS</span>
                     <!--  </div> -->
                    </div>
                  </div></a>
                </div>
              </div>
           </div> 
          <h4 class="header-title business_manager_header_title m-b-30">
            <span>Kundan Karigar Products</span>
            </h4>

            <div class="row">              
              <div class="col-md-12 col-xs-12 section-3 product-box marg_top-bottom no_pad">
                 <!-- <div class="col-lg-3 col-md-3 col-sm-6">
                  <a href="<?= ADMIN_PATH.'kundan_karigar?status=pending' ?>">
                    <div class="card-box dash-box box-shadow">
                    <h4 class="header-title m-t-0 m-b-30">Not Sent To Karigar</h4>
                    <div class="widget-chart-1">
                      <div class="widget-detail-1">
                        <h2 class="number-size light-blue-font"> <?= $kundan_pending?>  </h2>
                        <span class="label label-gray">Orders</span>
                      </div>
                    </div>
                  </div>
                </a>
                </div> -->
                 <div class="col-lg-3 col-md-3 col-sm-6 aos-item aos-init aos-animate" data-aos="fade-right" data-aos-delay="800" data-aos-duration="800">
                  <a href="<?= ADMIN_PATH.'kundan_karigar?status=complete' ?>">
                    <div class="card-box dash-box box-shadow blue_box dash_box_content text-center">
                   
                    <div class="widget-chart-1 white_color_font">
                     <!--  <div class="widget-detail-1"> -->
                         <h4 class="">Sent To Karigar</h4>
                        <h2 class="number-size"> <?= $kundan_complete ?> </h2>
                        <span class="label label-gray"> ORDERS</span>
                      <!-- </div> -->
                    </div>
                  </div>
                </a>
                </div><!-- end col -->

              <div class="col-lg-3 col-md-3 col-sm-6 aos-item aos-init aos-animate" data-aos="fade-right" data-aos-delay="850" data-aos-duration="800">
                  <a href="<?= ADMIN_PATH.'' ?>kundan_QC?status=pending">
                    <div class="card-box dash-box box-shadow yellowGold_box dash_box_content text-center">
                    
                    <div class="widget-chart-1 white_color_font">
                      <!-- <div class="widget-detail-1"> -->
                        <h4 class="">Pending QC</h4>
                        <h2 class="number-size light-blue-font"><?= $received_receipt;?></h2>
                        <span class="label label-gray">ORDERS</span>
                      <!-- </div> -->
                    </div>
                  </div>
                </a>
                </div>

                <div class="col-lg-3 col-md-3 col-sm-6 aos-item aos-init aos-animate" data-aos="fade-right" data-aos-delay="900" data-aos-duration="800">
                  <a href="<?= ADMIN_PATH.'karigar_receive_order' ?>">
                    <div class="card-box dash-box box-shadow darkGreen_box dash_box_content text-center">
                    
                    <div class="widget-chart-1 white_color_font">
                      <!-- <div class="widget-detail-1"> -->
                        <h4 class="">QC Accepted</h4>
                        <h2 class="number-size light-blue-font"><?= $receive_order;?> </h2>
                        <span class="label label-gray">ORDERS</span>
                      <!-- </div> -->
                    </div>
                  </div>
                </a>
                </div><!-- end col -->
                <!-- end col -->
                <!-- <div class="col-lg-3 col-md-3 col-sm-6">
                  <a href="<?= ADMIN_PATH?>kundan_QC?status=complete">
                    <div class="card-box dash-box box-shadow">
                    <h4 class="header-title m-t-0 m-b-30">QC Checked</h4>
                    <div class="widget-chart-1">
                      <div class="widget-detail-1">
                        <h2 class="number-size light-blue-font"><?= $qc_complete;?></h2>
                        <span class="label label-gray">Orders</span>
                      </div>
                    </div>
                  </div>
                </a>
                </div> --><!-- end col -->
                <div class="col-lg-3 col-md-3 col-sm-6 aos-item aos-init aos-animate" data-aos="fade-right" data-aos-delay="1000" data-aos-duration="800">
                  <a href="<?= ADMIN_PATH?>kundan_QC?status=rejected">
                    <div class="card-box dash-box box-shadow orange_box dash_box_content text-center">
                  
                    <div class="widget-chart-1 white_color_font">                     
                        <h4 class="">QC Rejected</h4>
                        <h2 class="number-size light-red-font"> <?= $kun_qc_rejected?> </h2>
                        <span class="label label-gray">PRODUCTS</span>
                      </div>
                  </div>
                </a>
                </div><!-- end col -->
  
              </div>
              <?php }?>
               <div class="col-lg-12 col-md-12 col-sm-12">
            <h4 class="header-title business_manager_header_title m-b-30">
            <span>Ready Products</span>
            
            </h4></div>
            
               <div class="col-lg-3 col-md-3 col-sm-6 aos-item aos-init aos-animate" data-aos="fade-left" data-aos-delay="1000" data-aos-duration="800">
                  <a href="<?= ADMIN_PATH?>ready_product">
                    <div class="card-box dash-box box-shadow dark_purple_box dash_box_content text-center">
                   
                    <div class="widget-chart-1 white_color_font">
                     
                         <h4 class="">Ready Products</h4>
                        <h2 class="number-size"><?= $ready_products?></h2>
                        <span class="label label-gray">PRODUCTS</span>
                     
                    </div>
                  </div>
                </a>
                </div><!-- end col -->

                 <div class="col-lg-3 col-md-3 col-sm-6 aos-item aos-init aos-animate" data-aos="fade-left" data-aos-delay="1000" data-aos-duration="800">
                  <a href="<?= ADMIN_PATH?>ready_product?status=pending">
                    <div class="card-box dash-box box-shadow yellow_box dash_box_content text-center">
                    
                    <div class="widget-chart-1 white_color_font">
                      <!-- <div class="widget-detail-1"> -->
                        <h4 class="">Pending To Be Approved By Sales</h4>
                        <h2 class="number-size light-blue-font"><?=$ready_products_pending_approved?></h2>
                        <span class="label label-gray">PRODUCTS</span>
                      <!-- </div> -->
                    </div>
                  </div>
                </a>
                </div><!-- end col -->
                 <div class="col-lg-3 col-md-3 col-sm-6 aos-item aos-init aos-animate" data-aos="fade-left" data-aos-delay="1000" data-aos-duration="800">
                  <a href="<?= ADMIN_PATH?>ready_product/approved_by_sale">
                    <div class="card-box dash-box box-shadow green_box dash_box_content text-center">
                   
                    <div class="widget-chart-1 white_color_font">                     
                         <h4 class="">Approved By Sales</h4>
                        <h2 class="number-size light-blue-font"><?=$sales_approved?></h2>
                        <span class="label label-gray">PRODUCTS</span>
                      </div>
                  </div>
                </a>
                </div><!-- end col -->
                  <div class="col-lg-3 col-md-3 col-sm-6 aos-item aos-init aos-animate" data-aos="fade-left" data-aos-delay="1000" data-aos-duration="800">
                  <a href="<?= ADMIN_PATH?>ready_product/rejected_by_sale">
                    <div class="card-box dash-box box-shadow red_box dash_box_content text-center">
                   
                    <div class="widget-chart-1 white_color_font">
                     
                         <h4 class="">Rejected By Sales</h4>
                        <h2 class="number-size light-red-font"><?=$sales_rejected?></h2>
                        <span class="label label-gray">PRODUCTS</span>
                     
                    </div>
                  </div>
                </a>
                </div><!-- end col -->
           
            </div>
             <div class="row but-sec">
             <div class="col-md-10 col-xs-12 text-center">
             
                  
                </div>
              </div>
              <!--===================================================-->
    </div><!-- end container -->
  </div><!-- end content -->
</div><!-- end content-page -->