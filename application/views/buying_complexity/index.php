<div class="content-page">
<div class="content">
   <div class="container">
      <div class="row">
         <div class="col-sm-12 card-box-wrap">
            <div class="panel panel-default">
               <div class="panel-heading right-480">                  
                  <h4 class="inlineBlock">All Buying Complexity</h4>
                  <div class="pull-right btn-wrap2">
                  <a href="Buying_complexity/create" class="add_senior_manager_button btn btn-purp waves-effect w-md waves-light m-t-5 btn-md pull-right">ADD Buying Complexity </button></a>
                  </div>
               </div>
               <div class="panel-body">                  
                  <div class="clearfix"></div>
                  <div class="table-rep-plugin">
                     <div class="b-0 scrollhidden  table-responsive">
                        <table class="table table-bordered ustdatatable" id="buying_complexity">
                           <thead>
                           <?php
                             $data['heading']='buying_complexity';
                             $this->load->view('master/table_header',$data);
                           ?>
                            <!--   <tr>
                                  <th class="col4">Buying Complexity Name</th>
                                 <th class="col3"></th>
                              </tr> -->
                           </thead>
                        </table>
                     </div>
                  </div>
               </div>
            </div>
         </div>
      </div>
   </div>
</div>