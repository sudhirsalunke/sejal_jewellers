
<?php
$corporate_ids="";
if(@$corporate_id){
  $corporate_ids = $corporate_id ;
}else{
  $corporate_ids = "all";
}
?>
<div class="content-page">
  <div class="content">
    <div class="container">
      <div class="row">
        <div class="col-sm-12 card-box-wrap">
          <div class="card-box padmobile dash-board">
            <h5 class=" m-b-20"><!--header-title business_manager_header_title-->
            <span>DASHBOARD</span>
            </h5> 
            <form method="post" action="<?= ADMIN_PATH.'Corporate_Dashboard' ?>">                

                    <div class="form-group row">
                     <div class="col-lg-2 col-md-4 col-sm-4">
                                  <select class="form-control" name="corporate" id="corporate">
                          <option value="">Please select corporate</option>
                            <?php
                              foreach ($corporates as $key => $value) {
                            ?>
                              <option value="<?= $value['id']?>" <?php if($value['id'] == $corporate_id){ echo "selected";} ?>><?= $value['name']?></option>
                            <?php } ?>  
                            ?>
                          </select>
                        </div>
          <!--               <label class="control-label text-right col-md-1 col-form-label vertical-middle">From Date</label> -->
                        <!-- <div class="col-md-2">
                            <input type="text" name="from_date" value="" class="form-control order_name   datepickerInput datepicker-autoclose" placeholder="From Date">
                        </div>
                        <!-- <label class="control-label text-right col-md-1 col-form-label vertical-middle">To Date</label> 
                        <div class="col-md-2">
                            <input type="text" name="to_date" value="" class="form-control order_name    datepickerInput datepicker-autoclose" placeholder="To Date">

                            <small class="form-control-feedback error_msg" id="order_name_error"></small>
                        </div> -->
                        <div class="col-sm-4 report_btn">
                            <button type="submit" class="btn btn-info btn-sm">Search</button>
                            <a type="button" class="btn btn-danger btn-sm" href="<?= ADMIN_PATH.'Corporate_Dashboard' ?>">Reset</a>
                        </div>
                    </div>
                  
                </form>
            <h4 class="m-b-20 header-title">
            <span>Due Date Reminder</span>
            </h4>

            <div class="row">              
              <div class="col-md-12 col-xs-12 section-3 product-box marg_top-bottom no_pad">
<!-- 
              <div class="col-lg-3 col-md-3 col-sm-6 aos-item" data-aos="fade-up"  data-aos-delay="750" data-aos-duration="800">
                  <a href="<?= ADMIN_PATH.'Engaged_karigar_list' ?>">
                  <div class="card-box dash-box box-shadow green_box dash_box_content text-center">
                   
                      <div class="widget-chart-1 white_color_font">
            
                         <h4 class="">Karigar Engaged</h4>
                        <h2 class="number-size"><?=$engaged_karigar;?> </h2>

                        <span class="label label-gray">KARIGAR</span>

                    </div>
                  </div></a>
                </div> --><!-- end col -->
                <div class="col-lg-3 col-md-3 col-sm-6 aos-item" data-aos="fade-up"  data-aos-delay="850" data-aos-duration="800">
                  <a href="<?= ADMIN_PATH.'due_date_reminder?status=due_date' ?>">
                  <div class="card-box dash-box box-shadow darkBlue_box dash_box_content text-center">
                   
                      <div class="widget-chart-1 white_color_font">
                      <!-- <div class="widget-detail-1"> -->
                         <h4 class="">Karigar Due Tracking</h4>
                        <h2 class="number-size"><?=$due_date_engaged_karigar;?></h2>
                        <span class="label label-gray">Orders</span>
                      <!-- </div> -->
                    </div>
                  </div></a>
                </div><!-- end col -->  


                <div class="col-lg-3 col-md-3 col-sm-6 aos-item" data-aos="fade-up"  data-aos-delay="850" data-aos-duration="800">
                  <a href="<?= ADMIN_PATH.'due_date_reminder?status=late'?>">
                  <div class="card-box dash-box box-shadow red_box  dash_box_content text-center">
                   
                      <div class="widget-chart-1 white_color_font">
                      <!-- <div class="widget-detail-1"> -->
                         <h4 class="">Karigar Late Orders</h4>
                        <h2 class="number-size"><?=$late_date;?></h2>
                        <span class="label label-gray">Orders</span>
                      <!-- </div> -->
                    </div>
                  </div></a>
                </div><!-- end col -->     




                 <div class="col-lg-3 col-md-3 col-sm-6 aos-item" data-aos="fade-up"  data-aos-delay="850" data-aos-duration="800">
                  <a href="<?= ADMIN_PATH.'Order_Tracking/'.$corporate_ids.'' ?>">
                  <div class="card-box dash-box box-shadow dark_purple_box dash_box_content text-center">
                   
                      <div class="widget-chart-1 white_color_font">
                      <!-- <div class="widget-detail-1"> -->
                         <h4 class="">Order tracking</h4>
                        <h2 class="number-size"><?=$order_tracking;?></h2>
                        <span class="label label-gray">ORDER</span>
                      <!-- </div> -->
                    </div>
                  </div></a>
                </div><!-- end col -->

                 <div class="col-lg-3 col-md-3 col-sm-6 aos-item" data-aos="fade-up"  data-aos-delay="850" data-aos-duration="800">
                  <a href="<?= ADMIN_PATH.'Design_Tracking/'.$corporate_ids.'' ?>">
                  <div class="card-box dash-box box-shadow yellow_box  dash_box_content text-center">
                   
                      <div class="widget-chart-1 white_color_font">
                      <!-- <div class="widget-detail-1"> -->
                         <h4 class="">Design tracking</h4>
                        <h2 class="number-size"><?=$design_tracking;?></h2>
                        <span class="label label-gray">DESIGN</span>
                      <!-- </div> -->
                    </div>
                  </div></a>
                </div><!-- end col -->        
         
              </div>
            </div>
      </div><!-- end row -->

<h4 class="m-b-20 header-title">
            <span>Order Details</span>
            </h4>
       <div class="row">
       
            <div class="col-lg-6">
                <div class="card graph-card">
                    <div class="card-body body-scroll">
                        <h3 class="card-title m-l-5"><span class="lstick"></span>Order Wise </h3>
                           <div id="order_chart" class="ct-chart ct-golden-section">
                          <div class="chartist-tooltip" style="top: 246px; left: 63px;">
                              <span class="chartist-tooltip-value">8</span>
                          </div>
                        </div>
                    </div>


                </div>             
                            
            </div>

                 <div class="col-lg-6">
                <div class="card graph-card">
                    <div class="card-body body-scroll">
                        <h3 class="card-title m-l-5"><span class="lstick"></span>Design Wise </h3>
                           <div id="design_chart" class="ct-chart ct-golden-section">
                          <div class="chartist-tooltip" style="top: 246px; left: 63px;">
                              <span class="chartist-tooltip-value">8</span>
                          </div>
                        </div>
                    </div>


                </div>             
                            
            </div>
  
            
    </div><!-- end container -->
   
        <div class="card-box padmobile dash-board">
                 
            
            <h4 class="m-b-20 header-title">
            <span>Order Due Tracking</span>
            </h4>

            <div class="row">              
              <div class="col-md-12 col-xs-12 section-3 product-box marg_top-bottom no_pad">

              <div class="col-lg-3 col-md-3 col-sm-6 aos-item" data-aos="fade-up"  data-aos-delay="750" data-aos-duration="800">
                  <a href="<?= ADMIN_PATH.'Corporate_Dashboard/reminder/'.$corporate_ids.'?filter=5' ?>">
                  <div class="card-box dash-box box-shadow red_box dash_box_content text-center">
                   
                      <div class="widget-chart-1 white_color_font">
                      <!-- <div class="widget-detail-1"> -->
                         <h4 class="">5 days</h4>
                        <h2 class="number-size"><?=$five_days;?> </h2>

                        <span class="label label-gray">Orders</span>
                      <!-- </div> -->
                    </div>
                  </div></a>
                </div><!-- end col -->
                <div class="col-lg-3 col-md-3 col-sm-6 aos-item" data-aos="fade-up"  data-aos-delay="850" data-aos-duration="800">
                  <a href="<?= ADMIN_PATH.'Corporate_Dashboard/reminder/'.$corporate_ids.'?filter=4' ?>">
                  <div class="card-box dash-box box-shadow darkBlue_box dash_box_content text-center">
                   
                      <div class="widget-chart-1 white_color_font">
                      <!-- <div class="widget-detail-1"> -->
                         <h4 class="">4 Days</h4>
                        <h2 class="number-size"><?=$four_days;?></h2>
                        <span class="label label-gray">Orders</span>
                      <!-- </div> -->
                    </div>
                  </div></a>
                </div><!-- end col -->  
                 <div class="col-lg-3 col-md-3 col-sm-6 aos-item" data-aos="fade-up"  data-aos-delay="850" data-aos-duration="800">
                  <a href="<?= ADMIN_PATH.'Corporate_Dashboard/reminder/'.$corporate_ids.'?filter=3' ?>">
                  <div class="card-box dash-box box-shadow lightPink_box dash_box_content text-center">
                   
                      <div class="widget-chart-1 white_color_font">
                      <!-- <div class="widget-detail-1"> -->
                         <h4 class="">3 Days</h4>
                        <h2 class="number-size"><?=$three_days;?></h2>
                        <span class="label label-gray">Orders</span>
                      <!-- </div> -->
                    </div>
                  </div></a>
                </div><!-- end col -->  
                 <div class="col-lg-3 col-md-3 col-sm-6 aos-item" data-aos="fade-up"  data-aos-delay="850" data-aos-duration="800">
                  <a href="<?= ADMIN_PATH.'Corporate_Dashboard/reminder/'.$corporate_ids.'?filter=2' ?>">
                  <div class="card-box dash-box box-shadow pink_box  dash_box_content text-center">
                   
                      <div class="widget-chart-1 white_color_font">
                      <!-- <div class="widget-detail-1"> -->
                         <h4 class="">2 Days</h4>
                        <h2 class="number-size"><?=$two_days;?></h2>
                        <span class="label label-gray">PRODUCT</span>
                      <!-- </div> -->
                    </div>
                  </div></a>
                </div><!-- end col -->             
              </div>
            </div>

            <div class="row">              
              <div class="col-md-12 col-xs-12 section-3 product-box marg_top-bottom no_pad">

              <div class="col-lg-3 col-md-3 col-sm-6 aos-item" data-aos="fade-up"  data-aos-delay="750" data-aos-duration="800">
                  <a href="<?= ADMIN_PATH.'Corporate_Dashboard/reminder/'.$corporate_ids.'?filter=1' ?>">
                  <div class="card-box dash-box box-shadow orange_box dash_box_content text-center">
                   
                      <div class="widget-chart-1 white_color_font">
                      <!-- <div class="widget-detail-1"> -->
                         <h4 class="">1 Day</h4>
                        <h2 class="number-size"><?=$one_days;?> </h2>

                        <span class="label label-gray">ORDER</span>
                      <!-- </div> -->
                    </div>
                  </div></a>
                </div><!-- end col -->
                <div class="col-lg-3 col-md-3 col-sm-6 aos-item" data-aos="fade-up"  data-aos-delay="850" data-aos-duration="800">
                  <a href="<?= ADMIN_PATH.'Corporate_Dashboard/reminder/'.$corporate_ids.'?filter=0' ?>">
                  <div class="card-box dash-box box-shadow grey_box dash_box_content text-center">
                   
                      <div class="widget-chart-1 white_color_font">
                      <!-- <div class="widget-detail-1"> -->
                         <h4 class="">Today</h4>
                        <h2 class="number-size"><?=$today;?></h2>
                        <span class="label label-gray">ORDER</span>
                      <!-- </div> -->
                    </div>
                  </div></a>
                </div><!-- end col -->  
     <div class="col-lg-3 col-md-3 col-sm-6 aos-item" data-aos="fade-up"  data-aos-delay="850" data-aos-duration="800">
                  <a href="<?= ADMIN_PATH.'Corporate_Dashboard/reminder/'.$corporate_ids.'?filter=exceed'?>">
                  <div class="card-box dash-box box-shadow grey_box dash_box_content text-center">
                   
                      <div class="widget-chart-1 white_color_font">
                      <!-- <div class="widget-detail-1"> -->
                         <h4 class="">Over Due</h4>
                        <h2 class="number-size"><?=$over_due;?></h2>
                        <span class="label label-gray">ORDER</span>
                      <!-- </div> -->
                    </div>
                  </div></a>
                </div><!-- end col -->  
     
         
            </div>
      </div><!-- end row -->




  </div><!-- end content -->
</div><!-- end content-page -->
 <script>
    var order_wise_label ='<?=$order_wise_chart['labels']?>';
    var order_wise_data ='<?=$order_wise_chart['data']?>';
    var order_wise_max='<?=$order_wise_chart['max']?>';


    var design_wise_label ='<?=$design_wise_chart['labels']?>';
    var design_wise_data ='<?=$design_wise_chart['data']?>';
    var design_wise_max='<?=$design_wise_chart['max']?>';
  
 </script>




<!-- 
        <div class="card-box padmobile dash-board">
                 
            
            <h4 class="m-b-20 header-title">
            <span>Order Due Tracking</span>
            </h4>

            <div class="row">              
              <div class="col-md-12 col-xs-12 section-3 product-box marg_top-bottom no_pad">

              <div class="col-lg-3 col-md-3 col-sm-6 aos-item" data-aos="fade-up"  data-aos-delay="750" data-aos-duration="800">
                  <a href="<?= ADMIN_PATH.'Prepare_order' ?>">
                  <div class="card-box dash-box box-shadow red_box dash_box_content text-center">
                   
                      <div class="widget-chart-1 white_color_font">
                      <!-- <div class="widget-detail-1"> --
                         <h4 class="">Not assigned to Karigar</h4>
                        <h2 class="number-size"><?=$not_assign_karigar;?> </h2>

                        <span class="label label-gray">DESIGNS</span>
                      <!-- </div> --
                    </div>
                  </div></a>
                </div><!-- end col --
                <div class="col-lg-3 col-md-3 col-sm-6 aos-item" data-aos="fade-up"  data-aos-delay="850" data-aos-duration="800">
                  <a href="<?= ADMIN_PATH.'Products_sent' ?>">
                  <div class="card-box dash-box box-shadow darkBlue_box dash_box_content text-center">
                   
                      <div class="widget-chart-1 white_color_font">
                      <!-- <div class="widget-detail-1"> --
                         <h4 class="">Sent to Karigar</h4>
                        <h2 class="number-size"><?=$not_to_sent_karigar;?></h2>
                        <span class="label label-gray">DESIGNS</span>
                      <!-- </div> --
                    </div>
                  </div></a>
                </div><!-- end col --  
                 <div class="col-lg-3 col-md-3 col-sm-6 aos-item" data-aos="fade-up"  data-aos-delay="850" data-aos-duration="800">
                  <a href="<?= ADMIN_PATH.'Receive_products' ?>">
                  <div class="card-box dash-box box-shadow lightPink_box dash_box_content text-center">
                   
                      <div class="widget-chart-1 white_color_font">
                      <!-- <div class="widget-detail-1"> --
                         <h4 class="">Received from Karigar</h4>
                        <h2 class="number-size"><?=$received_from_karigar;?></h2>
                        <span class="label label-gray">PRODUCT</span>
                      <!-- </div> --
                    </div>
                  </div></a>
                </div><!-- end col --
                 <div class="col-lg-3 col-md-3 col-sm-6 aos-item" data-aos="fade-up"  data-aos-delay="850" data-aos-duration="800">
                  <a href="<?= ADMIN_PATH.'Hallmarking?status=send' ?>">
                  <div class="card-box dash-box box-shadow pink_box  dash_box_content text-center">
                   
                      <div class="widget-chart-1 white_color_font">
                      <!-- <div class="widget-detail-1"> --
                         <h4 class="">Sent to Hallmark</h4>
                        <h2 class="number-size"><?=$sent_to_hallmarking;?></h2>
                        <span class="label label-gray">PRODUCT</span>
                      <!-- </div> --
                    </div>
                  </div></a>
                </div><!-- end col --           
              </div>
            </div>

            <div class="row">              
              <div class="col-md-12 col-xs-12 section-3 product-box marg_top-bottom no_pad">

              <div class="col-lg-3 col-md-3 col-sm-6 aos-item" data-aos="fade-up"  data-aos-delay="750" data-aos-duration="800">
                  <a href="<?= ADMIN_PATH.'Hallmarking?status=exported' ?>">
                  <div class="card-box dash-box box-shadow orange_box dash_box_content text-center">
                   
                      <div class="widget-chart-1 white_color_font">
                      <!-- <div class="widget-detail-1"> --
                         <h4 class="">Hallmark QC Accepted</h4>
                        <h2 class="number-size"><?=$hallmarking_qc_accept;?> </h2>

                        <span class="label label-gray">ORDER</span>
                      <!-- </div> --
                    </div>
                  </div></a>
                </div><!-- end col --
                <div class="col-lg-3 col-md-3 col-sm-6 aos-item" data-aos="fade-up"  data-aos-delay="850" data-aos-duration="800">
                  <a href="<?= ADMIN_PATH.'Stock/1/?status=sent' ?>">
                  <div class="card-box dash-box box-shadow grey_box dash_box_content text-center">
                   
                      <div class="widget-chart-1 white_color_font">
                      <!-- <div class="widget-detail-1"> --
                         <h4 class="">Product Sent</h4>
                        <h2 class="number-size"><?=$product_set;?></h2>
                        <span class="label label-gray">ORDER</span>
                      <!-- </div> --
                    </div>
                  </div></a>
                </div><!-- end col --
     
         
            </div>
      </div><!-- end row --




  </div><!-- end content --> -