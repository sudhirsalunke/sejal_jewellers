<div class="content-page">
<div class="content">
   <div class="container">
      <div class="row">
         <div class="col-sm-6 card-box-wrap">
            <div class="panel panel-default">
              <div class="panel-heading frmHeading">
                <h4 class="inlineBlock"><span>Add Article</span></h4>
              </div>
              <div class="panel-body">
                <div class="col-lg-12">
                  <form name="category" id="Article" role="form" class="form-horizontal" method="post">
                     <div class="form-group">
                        <label class="col-sm-4 control-label" for="inputEmail3"> Enter Article Name <span class="asterisk">*</span></label>
                        <div class="col-sm-8">
                           <input type="text" placeholder="Enter Article Name" id="category" class="form-control" name="Article[name]">
                           <span class="text-danger" id="name_error"></span>
                        </div>
                     </div>
                     <div class="form-group">
                        <label class="col-sm-4 control-label" for="inputEmail3"> Enter Article Descripition <span class="asterisk">*</span></label>
                        <div class="col-sm-8">
                           <textarea class="form-control" name="Article[description]"></textarea>
                           <span class="text-danger" id="description_error"></span>
                        </div>
                     </div>
                    <div class="btnSection">                 
                        
                         <button class="btn btn-default waves-effect waves-light m-l-5 btn-md" onclick="window.location='<?= ADMIN_PATH?>Article'" type="reset">
                           back
                           </button>
                            <button class="btn btn-success waves-effect waves-light btn-md pull-right" name="commit" type="button" onclick="save_article(); ">
                         SAVE
                         </button>
                     
                  </div>
                  </form>
               </div>
              </div>
               <div class="clearfix"></div>
            </div>
         </div>
      </div>
   </div>
</div>