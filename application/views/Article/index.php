<div class="content-page">
<div class="content">
   <div class="container">
      <div class="row">
         <div class="col-sm-12 card-box-wrap">
            <div class="panel panel-default">
              <div class="panel-heading">                
               <h4 class="inlineBlock">All Article</h4>
               <div class="pull-right single-send">
                <a href="<?= ADMIN_PATH?>Article/create" class="add_senior_manager_button btn btn-primary waves-effect w-md waves-light btn-md single-send">ADD ARTICLE</a>
                </div>
              </div>
              <div class="panel-body">                
               <div class="clearfix"></div>
               <div class="table-rep-plugin">
                  <div class="b-0 scrollhidden table-resp">
                    <table class="table table-bordered custdatatable" id="Article">
                      <thead>
                        <?php
                          $data['heading']='Article';
                          $this->load->view('master/table_header',$data);
                        ?>

                         <!-- <tr>
                             <th class="col4">Article Name</th>
                             <th class="col4">Description</th>
                            <th class="col3"></th>
                         </tr> -->
                      </thead>
                    </table>
                  </div>
               </div>
              </div>
            </div>
         </div>
      </div>
   </div>
</div>