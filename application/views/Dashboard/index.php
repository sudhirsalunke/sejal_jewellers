<div class="content-page">
  <div class="content">
    <div class="container">
      <div class="row">
        <div class="col-sm-12 card-box-wrap" id="transcroller-body">
          <div class="card-box padmobile dash-board mr-20">
            <h5 class="m-b-20 bmsmheader bmsmheaderabm"><!--header-title business_manager_header_title-->
            <span>DASHBOARD</span>
            </h5>               
              <div class="row marg_bottom product-box">
                <div class="col-md-12 col-xs-12">
                    <div class="row">
                      <div class="col-md-12">
                         <h4 class="m-b-20  bmsmheader bmsmheaderabm pull-left header-title"><!--header-title business_manager_header_title-->
                             <span>New Designs</span>
                         </h4>
                      </div>
                    </div> 
                    <div class="row">
                    <div class="col-lg-3 col-md-4 col-sm-6 aos-item" data-aos="fade-down"  data-aos-delay="500" data-aos-duration="800">
                      <a href="<?= ADMIN_PATH.'Design/Design_with_images' ?>">
                        <div class="card-box dash-box box-shadow text-center dash_box_content purple_box">
                       
                        <div class="widget-chart-1 white_color_font">
                           <h4 class="">Design with images</h4>
                         <!--  <div class="widget-detail-1"> -->
                            <h2 class="number-size"> <?= $design_with_images?> </h2>
                            <span class="label">Designs</span>
                         <!--  </div> -->

                        </div>
                      </div></a>
                    </div><!-- end col -->


                    <div class="col-lg-3 col-md-4 col-sm-6 aos-item" data-aos="fade-down"  data-aos-delay="600" data-aos-duration="800">
                      <a href="<?= ADMIN_PATH.'Design/Design_without_images' ?>">
                       <div class="card-box dash-box box-shadow darkBlue_box dash_box_content text-center">
                      
                        <div class="widget-chart-1  white_color_font">
                          <!-- <div class="widget-detail-1"> -->
                            <h4 class="">Design without images</h4>
                            <h2 class="number-size"><?= $design_without_images?> </h2>

                            <span class="label label-gray">Designs</span>
                         <!--  </div> -->
                        </div>
                      </div></a>
                    </div><!-- end col -->

                    <div class="col-lg-3 col-md-4 col-sm-6 aos-item" data-aos="fade-down"  data-aos-delay="650" data-aos-duration="800">
                      <a href="<?= ADMIN_PATH.'Send_for_approval' ?>">
                        <div class="card-box dash-box box-shadow grey_box dash_box_content text-center">
                       
                        <div class="widget-chart-1 white_color_font">
                         <!--  <div class="widget-detail-1"> -->
                            <h4 class="">Shortlisted to be sent</h4>
                            <h2 class="number-size"> <?=$r_shortlisted_design_to_be_sent?>  </h2>
                            <span class="label label-gray">Designs</span>
                          <!-- </div> -->
                        </div>
                      </div></a>
                    </div><!-- end col -->
                  </div>
                </div>
              </div>
              <!-- End row -->
               <!-- <div class="row but-sec">
                <div class="col-md-10 col-xs-12  no_pad">
                  <div class="col-md-4">
                    <a href="<?= ADMIN_PATH.'Product/create' ?>" class="btn btn-purple waves-effect w-md m-b-5">ADD PRODUCTS</a>
                  </div>
                  <div class="col-md-4">
                    <a href="<?= ADMIN_PATH.'Upload_product_excel' ?>" class="btn btn-purple waves-effect w-md m-b-5">IMPORT PRODUCTS</a>
                  </div>
                  <div class="col-md-4">
                    <a href="<?= ADMIN_PATH.'Add_product_images' ?>" class="btn btn-purple waves-effect w-md m-b-5">UPLOAD IMAGES</a>
                  </div>
                </div>
              </div>  -->
              <!-- End row -->
            <div class="row  product-box"> 
                <div class="col-md-12 col-xs-12 section-2">
                  <img src="assets/images/reliance-logo.png" alt="reliance-logo" class="img-responsive">
                </div>

                <div class="col-lg-3 col-md-4 col-sm-6 aos-item" data-aos="flip-up"  data-aos-delay="700" data-aos-duration="800">
                  <a href="<?= ADMIN_PATH.'Approved_Design/1' ?>">
                  <div class="card-box dash-box box-shadow darkGreen_box dash_box_content text-center">                    
                    <div class="widget-chart-1 white_color_font">
                      <!-- <div class="widget-detail-1"> -->
                        <h4 class="">Approved Designs</h4>
                        <h2 class="number-size"><?=$r_get_approved_design?></h2>
                        <span class="label ">Designs</span>
                     <!--  </div> -->

                    </div>
                  </div></a>
                </div><!-- end col --><!-- 
                <div class="col-lg-3 col-md-3">
                  <a href="<?= ADMIN_PATH.'Send_for_approval' ?>"><div class="card-box dash-box box-shadow">
                    <h4 class="header-title m-t-0 m-b-30">Receive Pieces</h4>
                    <div class="widget-chart-1">
                      <div class="widget-detail-1">
                        <h2 class="number-size light-blue-font"> <?=$r_shortlisted_design_to_be_sent?>  </h2>
                        <span class="label label-gray">Design</span>
                      </div>
                    </div>
                  </div></a>
                </div> --><!-- end col --> 
                <div class="col-lg-3 col-md-4 col-sm-6 aos-item" data-aos="flip-up"  data-aos-delay="750" data-aos-duration="800">
                  <a href="<?= ADMIN_PATH.'Receive_pieces/1' ?>">
                   <div class="card-box dash-box box-shadow blue_box dash_box_content text-center">
                    
                    <div class="widget-chart-1 white_color_font">
                      <!-- <div class="widget-detail-1"> -->
                        <h4 class="">Sent for Approval</h4>
                        <h2 class="number-size"> <?=$r_sent_design_for_approval?>  </h2>
                        <span class="label">Designs</span>
                     <!--  </div> -->
                    </div>
                  </div></a>
                </div><!-- end col -->

                <div class="col-lg-3 col-md-4 col-sm-6 aos-item" data-aos="flip-up"  data-aos-delay="800" data-aos-duration="800">
                  <a href="<?= ADMIN_PATH.'Rejected_Design/1' ?>">
                   <div class="card-box dash-box box-shadow red_box dash_box_content text-center">
                    
                    <div class="widget-chart-1 white_color_font">
                     <!--  <div class="widget-detail-1"> -->
                        <h4 class="">Rejected Designs</h4>
                        <h2 class="number-size"> <?=$r_rejected_designs?> </h2>

                        <span class="label label-gray">Designs</span>
                      <!-- </div> -->
                    </div>
                  </div>
                </div><!-- end col -->
            </div>
            <!-- End row -->
            <div class="row"> 
                <div class="col-md-12 col-xs-12 section-2">
                  <img src="assets/images/carat-logo.png" alt="carat-logo" class="img-responsive">
                </div>

                <div class="col-lg-3 col-md-4 col-sm-6 aos-item" data-aos="flip-up"  data-aos-delay="850" data-aos-duration="800">
                  <a href="<?= ADMIN_PATH.'Approved_Design/2' ?>">
                  <div class="card-box dash-box box-shadow green_box dash_box_content text-center">
                    
                    <div class="widget-chart-1 white_color_font">
                      <!-- <div class="widget-detail-1"> -->
                        <h4 class="">Approved Designs</h4>
                        <h2 class="number-size"> <?=$c_get_approved_design?> </h2>
                        <span class="label">Designs</span>
                      <!-- </div> -->

                    </div>
                  </div>
                </div><!-- end col --><!-- 
                 <div class="col-lg-3 col-md-3">
                  <div class="card-box dash-box box-shadow">
                    <a href="<?= ADMIN_PATH.'Send_for_approval' ?>"><h4 class="header-title m-t-0 m-b-30">Receive pieces</h4>
                    <div class="widget-chart-1">
                      <div class="widget-detail-1">
                        <h2 class="number-size light-blue-font"> <?=$c_shortlisted_design_to_be_sent?> </h2>
                        <span class="label label-gray">Design</span>
                      </div>
                    </div>
                  </div></a>
                </div>  --><!-- end col -->
                <div class="col-lg-3 col-md-4 col-sm-6 aos-item" data-aos="flip-up"  data-aos-delay="900" data-aos-duration="800">
                  <a href="<?= ADMIN_PATH.'Receive_pieces/2' ?>">
                  <div class="card-box dash-box box-shadow darkBlue_box dash_box_content text-center">
                    
                    <div class="widget-chart-1 white_color_font">
                      <!-- <div class="widget-detail-1"> -->
                        <h4 class="">Sent for Approval</h4>
                        <h2 class="number-size"> <?=$c_sent_design_for_approval?> </h2>
                        <span class="label">Designs</span>
                      <!-- </div> -->
                    </div>
                  </div></a>
                </div><!-- end col -->
                <div class="col-lg-3 col-md-4 col-sm-6 aos-item" data-aos="flip-up"  data-aos-delay="950" data-aos-duration="800">
                  <a href="<?= ADMIN_PATH.'Rejected_Design/2' ?>">
                  <div class="card-box dash-box box-shadow orange_box dash_box_content text-center">
                   
                    <div class="widget-chart-1 white_color_font">
                      <!-- <div class="widget-detail-1"> -->
                         <h4 class="">Rejected Designs</h4>
                        <h2 class="number-size"> <?=$c_rejected_designs?> </h2>

                        <span class="label label-gray">Designs</span>
                      <!-- </div> -->
                    </div>
                  </div></a>
                </div><!-- end col -->
            </div>
          </div><!-- end card-box padmobile -->
        </div><!-- end col-sm-12 card-box-wrap -->
      </div><!-- end row -->
    </div><!-- end container -->
  </div><!-- end content -->
</div><!-- end content-page -->