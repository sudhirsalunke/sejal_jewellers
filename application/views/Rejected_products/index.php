<div class="content-page">
<div class="content">
   <div class="container">
      <div class="row">
         <div class="col-sm-12 card-box-wrap">
            <div class="panel panel-default">
              <div class="panel-heading">
                <h4 class="inlineBlock">All Rejected Designs</h4>
              </div>
              <div class="panel-body">
                <!--  <a href="<?= ADMIN_PATH?>Rejected_products/export_images/<?= @$vendor?>"><button  type="button" class="add_senior_manager_button btn btn-primary  waves-effect w-md waves-light m-b-5 hidden-xs  btn-md">Export Images</button></a>
               <a href="<?= ADMIN_PATH?>Rejected_products/export_products/<?= @$vendor?>"><button  type="button" class="add_senior_manager_button btn btn-primary  waves-effect w-md waves-light m-b-5 hidden-xs  btn-md">Export Products</button></a> -->
                <div class="clearfix"></div>
                <div class="table-rep-plugin">
                  <div class="table-responsive b-0 scrollhidden">
                    <form enctype='multipart/form-data' role="form" name="product_form" >
                      <table class="table table-bordered custdatatable" id="rejected_products">
                        <thead>
                           <tr>
                              <th class="col4">#</th>
                              <th class="col4">Order Id</th>
                              <th class="col4">Corporate</th>
                              <th class="col4">Comment</th>
                              <th class="col4">No Of Designs</th>
                              <th class="col4">Date</th>
                              <th class="col3"></th>
                           </tr>
                        </thead>
                      </table>
                    </form>
                  </div>
                </div>
              </div>
            </div>
         </div>
      </div>
   </div>
</div>