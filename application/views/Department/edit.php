<div class="content-page">
<div class="content">
   <div class="container">
      <div class="row">
         <div class="col-sm-6 card-box-wrap">
            <div class="panel panel-default">
              <div class="panel-heading frmHeading">
                <h4 class="inlineBlock"><span>Add Department</span></h4>                
              </div>
              <div class="panel-body">
                <div class="col-lg-12">
                  <form name="category" id="department" role="form" class="form-horizontal" method="post">
                  <input type="hidden" name="Department[encrypted_id]" value="<?= $Department['encrypted_id'] ?>">
                     <div class="form-group">
                        <label class="col-sm-4 control-label" for="inputEmail3"> Enter Department Name <span class="asterisk">*</span></label>
                        <div class="col-sm-8">
                           <input type="text" placeholder="Enter Department Name" id="category" class="form-control" name="Department[name]" value="<?= $Department['name']?>" readonly>
                           <span class="text-danger" id="name_error"></span>
                        </div>
                     </div>                 
               <!--        <div class="form-group">
                        <label class="col-sm-4 control-label" for="inputEmail3">NO Tagging<span class="asterisk"></span></label>
                        <div class="col-sm-2">
                           <div class="checkbox checkbox-purpal">
                             <input id="checkbox_tag_y"  name="Department[tagging_status]" type="checkbox" data-parsley-multiple="groups" data-parsley-mincheck="2" data-parsley-id="69" value="1"  <?=$Department['tagging_status']=='1' ? 'checked' : '';?> >
                              <label for="checkbox1"> Yes </label>
                            </div>
                            </div>
               
                     </div> -->
                    <div class="btnSection">

                         <button class="btn btn-default waves-effect waves-light m-l-5 btn-md" onclick="window.location='<?= ADMIN_PATH?>Department'" type="reset">
                             back
                          </button>
                          <button class="btn btn-success pull-right waves-effect waves-light btn-md" name="commit" type="button" onclick="edit_department(); ">
                             SAVE
                          </button>
                    
                  </div>
                  </form>
                </div>
              </div>                
               <div class="clearfix"></div>
            </div>
         </div>
      </div>
   </div>
</div>