<div class="content-page">
<div class="content">
   <div class="container">
      <div class="row">
         <div class="col-sm-12 card-box-wrap">
            <div class="panel panel-default">
               <div class="panel-heading">                  
                <h4 class="inlineBlock">All Departments</h4>
                <div class="pull-right single-add btn-wrap3">
                <a href="<?= ADMIN_PATH?>Department/create" class="add_senior_manager_button btn btn-purp waves-effect w-md waves-light m-b-5 btn-md single-add">ADD Department</a>
                </div>
               </div>
               <div class="panel-body">                  
                  <div class="clearfix"></div>
                  <div class="table-rep-plugin">
                  <div class="b-0 scrollhidden table-resp">
                     <table class="table table-bordered custdatatable" id="department">
                        <thead>
                        <?php
                          $data['heading']='department';
                          $this->load->view('master/table_header',$data);
                        ?>
                          <!--  <tr>
                               <th class="col4">Name</th>
                              <th class="col3"></th>
                           </tr> -->
                        </thead>
                     </table>
                  </div>
                  </div>
               </div>
            </div>
         </div>
      </div>
   </div>
</div>