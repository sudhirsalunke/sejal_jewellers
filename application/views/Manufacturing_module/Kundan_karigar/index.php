<div class="content-page">
<div class="content">
   <div class="container">
      <div class="row">
         <div class="col-sm-12 card-box-wrap">
            <div class="panel panel-default">
            <div class="panel-heading">
               <h4 class="inlineBlock"><span><?= @$display_name;?></span>
               </h4>
               <div class="pull-right single-add btn-wrap3">
               <button type="button" onclick="receive_kun_prod()" class="add_senior_manager_button btn btn-primary  waves-effect w-md waves-light m-b-5 btn-md pull-right single-add">Receive</button>
               </div>
              </div>

               <div class="panel-body">
                  <div class="col-lg-12 col-sm-12 col-xs-12 table-responsive b-0 scrollhidden">
                <form enctype='multipart/form-data' role="form" name="product_form" id="receive_kun_prod" action="" method="post"> 
                 <?php if($_GET['status'] == 'pending'){ 
                  $table_col_name='kundan_karigar_pending';
                  }else{  
                     $table_col_name='kundan_karigar';
                  } ?>
               <table class="table table-bordered custdatatable" id="<?php echo $table_col_name;?>">
                  <thead>
                   <?php
                          $data['heading']=$table_col_name;
                          $this->load->view('master/table_header',$data);
                        ?>
                   <!--   <tr>
                        <th class="col3" style="width: 26px;">
                           <div class="checkbox checkbox-purpal">
                              <input value="0" type="checkbox" id="check_all" onclick="add_selected_hallmark(this)"><label for="check_all"></label>
                           </div>
                        </th>
                        <th class="col4">Karigar Name</th>
                        
                        <?php
                           if($_GET['status'] == 'pending'){
                              ?>
                              <th class="col4">Quantity</th>
                              <th class="col4">Issue Voucher No</th>
                              <?php
                           }else{
                              ?>
                              <th class="col4">Pcs</th>
                              <th class="col4">Product Code</th>
                              <!-- <th class="col4">Weight Range</th>
                              <th class="col4">Approx Wt</th> -->
                              <!-- <th class="col4">Quantity</th> -->
                              <!-- <th class="col4">Issue Voucher No</th> -->
                          <!--     <?php
                           }
                        ?> -->
                        
                        <!-- <th class="col4">Parent category</th> -->
                        <!--<th class="col3"></th>
                     </tr> -->
                  </thead>
               </table>
               </form>
               </div>
               </div>
            </div>
         </div>
      </div>
   </div>
</div>