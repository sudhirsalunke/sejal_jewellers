<!DOCTYPE html>
<html>

<head>
    <meta name="google" content="notranslate" />
</head>

<body>

    <div style="width: 90%; margin: 0 auto;">
        <h2 style="text-align: center;">Receipt Voucher</h2>
        <table style="border: 1px solid black;   border-collapse: collapse;
    width: 100%;">
            
            <tr>
                <td style="border: 1px solid black;  padding: 6px;">Dept From</td>
                <td style="border: 1px solid black;  padding: 6px;">MFG</td>
                <td style="border: 1px solid black;  padding: 6px;"></td>
                <td style="border: 1px solid black;  padding: 6px;">Date</td>
                <td style="border: 1px solid black;  padding: 6px;"><?= date('Y-m-d')?></td>
                <td style="border: 1px solid black;  padding: 6px;"></td>

            </tr>

            <tr>
                <td style="border: 1px solid black;  padding: 6px;">Dept To</td>
                <td style="border: 1px solid black;  padding: 6px;"><?= $pdf_details[0]['d_name']?></td>
                <td style="border: 1px solid black;  padding: 6px;"></td>
                <td style="border: 1px solid black;  padding: 6px;">Detail</td>
                <td style="border: 1px solid black;  padding: 6px;"></td>
                <td style="border: 1px solid black;  padding: 6px;"></td>

            </tr>
            <tr>
                <td style="border: 1px solid black;  padding: 6px;"></td>
                <td style="border: 1px solid black;  padding: 6px;"></td>
                <td style="border: 1px solid black;  padding: 6px;"></td>
                <td style="border: 1px solid black;  padding: 6px;"></td>
                <td style="border: 1px solid black;  padding: 6px;"></td>
                <td style="border: 1px solid black;  padding: 6px;"></td>

            </tr>

           
            <tr>
                <th style=" text-align: left; border: 1px solid black; padding: 6px; width: 100px;">Sr .No</th>
                <th style=" text-align: left; border: 1px solid black;  padding: 6px;  width: 100px;">Type</th>
                <th style=" text-align: left; border: 1px solid black;  padding: 6px;  width: 100px;">Item Code</th>
                <th style=" text-align: left; border: 1px solid black; padding: 6px;  width: 100px;">Gross Wt.</th>
                <th style=" text-align: left; border: 1px solid black;  padding: 6px;  width: 100px;">IMG</th>
                <th style=" text-align: left; border: 1px solid black;  padding: 6px;  width: 100px;">Narration</th>
            </tr>
            <?php
            $groos_wt = 0;
            $item = 0;
            foreach ($pdf_details as $key => $value) {
                $groos_wt += $value['gross_wt'];
                $item = $key+1;
                ?>
                <tr>
                    <td style="border: 1px solid black;  padding: 6px;"><?= $item?></td>
                    <td style="border: 1px solid black;  padding: 6px;"><?= $value['name'];?></td>
                    <td style="border: 1px solid black;  padding: 6px;"><?= $value['product_code'];?></td>
                    <td style="border: 1px solid black;  padding: 6px;"><?= $value['gross_wt']?></td>
                    <?php if(!empty($value['image'])){?>
                        <td style="border: 1px solid black;  padding: 6px;"><img src="<?= ADMIN_PATH.$value['image']?>"></td>
                    <?php }else{?>
                        <td style="border: 1px solid black;  padding: 6px;"></td>
                    <?php } ?>
                    <td style="border: 1px solid black;  padding: 6px;"></td>

                </tr>
            
            <?php } ?>
             <tr>
                    <td style="border: 1px solid black;  padding: 6px;"><?= $item?></td>
                    <td style="border: 1px solid black;  padding: 6px;"></td>
                    <td style="border: 1px solid black;  padding: 6px;"></td>
                    <td style="border: 1px solid black;  padding: 6px;"><?= $groos_wt?></td>
                    <td style="border: 1px solid black;  padding: 6px;"></td>
                    <td style="border: 1px solid black;  padding: 6px;"></td>

                </tr>

        </table>

    </div>