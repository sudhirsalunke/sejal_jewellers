<!DOCTYPE html>
<html>

<head>
    <meta name="google" content="notranslate"/>
    <style>
    table tr{font-size: 14px;}
     @page {
        top:0;
    size: auto;   /* auto is the initial value */
    margin: 0;  /* this affects the margin in the printer settings */
    }

}

  </style>
</head>

<body onload="window.print()">

    <div style="width: 90%; margin: 0 auto;">
        <h2 style="text-align: center;">Issue Receipt Voucher</h2>
        <table style="border: 1px solid black;   border-collapse: collapse;
    width: 100%;">
      
            <?php if (empty($no_heading)) { ?>

            <tr>
                <td style="border: 1px solid black;  padding: 6px;">Issue Voucher No</td>
                <td style="border: 1px solid black;  padding: 6px;"><?= (!empty($result[0]['issue_voucher_no'])) ? $result[0]['issue_voucher_no'] : $chitti_no;?></td>
                <td style="border: 1px solid black;  padding: 6px;"></td>
                <td style="border: 1px solid black;  padding: 6px;">Date</td>
                <td style="border: 1px solid black;  padding: 6px;"><?= date('d-m-Y')?></td>
                  <?php if($result[0]['department_id'] !='2' && $result[0]['department_id'] !='3' && $result[0]['department_id'] !='6' && $result[0]['department_id'] !='10'){ ?>
                 <td style="border: 1px solid black;  padding: 6px;"></td>
                 <?php }?>

            </tr>

            <tr>
                <td style="border: 1px solid black;  padding: 6px;">Karigar Name</td>
                <td style="border: 1px solid black;  padding: 6px;"><?=$karigar_name?></td>
                <td style="border: 1px solid black;  padding: 6px;"></td>
                <td style="border: 1px solid black;  padding: 6px;">Detail</td>
                <td style="border: 1px solid black;  padding: 6px;"></td>
                 <?php if($result[0]['department_id'] !='2' && $result[0]['department_id'] !='3' && $result[0]['department_id'] !='6' && $result[0]['department_id'] !='10'){ ?>
                 <td style="border: 1px solid black;  padding: 6px;"></td>
                 <?php }?>

            </tr>
            <tr>

                
                <?php if($result[0]['department_id'] !='2' && $result[0]['department_id'] !='3' && $result[0]['department_id'] !='6' && $result[0]['department_id'] !='10'){ ?>
                 <td style="border: 1px solid black;  padding: 6px;" colspan="6"></td>
                 <?php }else{?>
                 <td style="border: 1px solid black;  padding: 6px;" colspan="5"></td>
                 <?php }?>


                <!-- <td style="border: 1px solid black;  padding: 6px;"></td>
                <td style="border: 1px solid black;  padding: 6px;"></td>
                <td style="border: 1px solid black;  padding: 6px;"></td>
                <td style="border: 1px solid black;  padding: 6px;"></td>
                <td style="border: 1px solid black;  padding: 6px;"></td> -->

            </tr>
           <?php } ?>
           <?php if (!empty($no_heading)) { ?>
                 <tr>
                <th style=" border: 1px solid black; padding: 6px; width: 100px;text-align: center;" colspan="6">Inter Dept Repair Voucher(MFG DEPT.)</th>                
   
            </tr>
            <tr>
                <th style=" text-align: left; border: 1px solid black; padding: 6px; width: 100px;">Dept From</th>
                <th style=" text-align: left; border: 1px solid black;  padding: 6px;  width: 100px;"></th>
                <th style=" text-align: left; border: 1px solid black;  padding: 6px;  width: 100px;">MFG</th>
                <?php if($result[0]['department_id'] !='2' && $result[0]['department_id'] !='3' && $result[0]['department_id'] !='6' && $result[0]['department_id'] !='10'){ ?>
                <th style=" text-align: left; border: 1px solid black; padding: 6px;  width: 100px;"></th>
                <?php } ?>
                <th style=" text-align: left; border: 1px solid black;  padding: 6px;  width: 100px;">Dept To</th>
                <th style=" text-align: left; border: 1px solid black;  padding: 6px;  width: 100px;"><?=$result[0]['department_name']?></th>
            </tr>
             <tr>
                <th style=" text-align: left; border: 1px solid black; padding: 6px; width: 100px;">Date</th>
                <th style=" text-align: left; border: 1px solid black;  padding: 6px;  width: 100px;"><?=$result[0]['repair_date']?></th>
                <?php if($result[0]['department_id'] !='2' && $result[0]['department_id'] !='3' && $result[0]['department_id'] !='6' && $result[0]['department_id'] !='10'){ ?>
                <th style=" text-align: left; border: 1px solid black;  padding: 6px;  width: 100px;"></th>
                <?php } ?>
                <th style=" text-align: left; border: 1px solid black; padding: 6px;  width: 100px;"></th>
                <th style=" text-align: left; border: 1px solid black;  padding: 6px;  width: 100px;">Chitti No.</th>
                <th style=" text-align: left; border: 1px solid black;  padding: 6px;  width: 100px;"><?=$result[0]['chitti_no']?></th>
            </tr>
            <tr>
                <th style=" text-align: left; border: 1px solid black; padding: 6px; width: 100px;"></th>
                <th style=" text-align: left; border: 1px solid black;  padding: 6px;  width: 100px;"></th>
                <th style=" text-align: left; border: 1px solid black;  padding: 6px;  width: 100px;"></th>
                <th style=" text-align: left; border: 1px solid black; padding: 6px;  width: 100px;"></th>
                <th style=" text-align: left; border: 1px solid black;  padding: 6px;  width: 100px;"></th>
                <?php if($result[0]['department_id'] !='2' && $result[0]['department_id'] !='3' && $result[0]['department_id'] !='6' && $result[0]['department_id'] !='10'){ ?>
                <th style=" text-align: left; border: 1px solid black;  padding: 6px;  width: 100px;"></th>
                <?php } ?>
            </tr>
            <tr>
                <th style=" text-align: left; border: 1px solid black; padding: 6px; width: 100px;">Detail</th>
                <th style=" text-align: left; border: 1px solid black;  padding: 6px;  width: 100px;"></th>
                <th style=" text-align: left; border: 1px solid black;  padding: 6px;  width: 100px;"></th>
                <th style=" text-align: left; border: 1px solid black; padding: 6px;  width: 100px;"></th>
                <th style=" text-align: left; border: 1px solid black;  padding: 6px;  width: 100px;"></th>
                <?php if($result[0]['department_id'] !='2' && $result[0]['department_id'] !='3' && $result[0]['department_id'] !='6' && $result[0]['department_id'] !='10'){ ?>
                <th style=" text-align: left; border: 1px solid black;  padding: 6px;  width: 100px;"></th>
                <?php }?>
            </tr>
            <tr>
                <th style=" text-align: left; border: 1px solid black; padding: 6px; width: 100px;"></th>
                <th style=" text-align: left; border: 1px solid black;  padding: 6px;  width: 100px;"></th>
                <th style=" text-align: left; border: 1px solid black;  padding: 6px;  width: 100px;"></th>
                <th style=" text-align: left; border: 1px solid black; padding: 6px;  width: 100px;"></th>
                <th style=" text-align: left; border: 1px solid black;  padding: 6px;  width: 100px;"></th>
                <?php if($result[0]['department_id'] !='2' && $result[0]['department_id'] !='3' && $result[0]['department_id'] !='6' && $result[0]['department_id'] !='10'){ ?>    
                <th style=" text-align: left; border: 1px solid black;  padding: 6px;  width: 100px;"></th>
                <?php }?>
             
            </tr>
            <?php }?>
           
            <tr>
                <th style=" text-align: left; border: 1px solid black; padding: 6px; width: 100px;">Sr .No</th>
                 <?php if(!empty($no_heading)){ ?>
                    <th style=" text-align: left; border: 1px solid black;  padding: 6px;  width: 100px;">Type</th>
                 <?php }?>
                   <?php if($result[0]['department_id'] !='2' && $result[0]['department_id'] !='3' && $result[0]['department_id'] !='6' && $result[0]['department_id'] !='10'){ ?>
                <th style=" text-align: left; border: 1px solid black;  padding: 6px;  width: 100px;">Item Code</th>
                   <?php }?>
                <?php if(empty($no_heading)){ $dis_name='Quantity';}else{ $dis_name='Gross Wt.';}?>
                <th style=" text-align: left; border: 1px solid black;  padding: 6px;  width: 100px;"><?= $dis_name?></th>
                <?php if(empty($no_heading)){ ?>
                <th style=" text-align: left; border: 1px solid black; padding: 6px;  width: 100px;">Net Wt.</th>
                 <?php } ?>
                <th style=" text-align: left; border: 1px solid black;  padding: 6px;  width: 100px;">IMG</th>
                <?php if(empty($no_heading)){ ?>
                <th style=" text-align: left; border: 1px solid black; padding: 6px;  width: 100px;">Pcs</th>
                <?php }else{?>
                <th style=" text-align: left; border: 1px solid black; padding: 6px;  width: 100px;">Narration</th>
                <?php }?>
                <!-- <th style=" text-align: left; border: 1px solid black;  padding: 6px;  width: 100px;">Narration</th> -->
            </tr>
            <?php
            $net_wt = 0;
            $item = 0;
            $qnt = 0;
            $wt=0;
            $pcs=0;
            foreach ($result as $key => $value) {
                $net_wt += $value['net_wt'];
                $qnt += $value['quantity'];
                $wt += $value['drp_gr_wt'];
                $pcs += $value['pcs'];
                $item++;
                ?>
                <tr>
                    <td style="border: 1px solid black;  padding: 6px; text-align: right; "><?= $item?></td>
                    <?php if(!empty($no_heading)){ ?>
                    <td style="border: 1px solid black;  padding: 6px; text-align: left;"><?= $value['type'];?></td>
                    <?php }?>
                    <?php if($value['department_id'] != '2' && $value['department_id'] !='3' && $value['department_id'] !='6' && $value['department_id'] !='10'){?>
                    <td style="border: 1px solid black;  padding: 6px;"><?= $value['product_code'];?></td>
                    <?php }?>

                   <?php if(!empty($no_heading)){ $dis_val=$value['drp_gr_wt'];}else{ $dis_val=$value['quantity'];}?>
                    <td style="border: 1px solid black;  padding: 6px; text-align: right;"><?= $dis_val;?></td>
                    <?php if(empty($no_heading)){ ?>
                    <td style="border: 1px solid black;  padding: 6px; text-align: right;"><?= $value['net_wt']?></td>
                     <?php } ?>
                    <?php if(!empty($value['image'])){ ?>
                        <td style="border: 1px solid black;  padding: 6px;"><img style="width:180px;height:auto;max-height: 180px;" src="<?= ADMIN_PATH.$value['image']?>"></td>
                    <?php }else{?>
                        <td style="border: 1px solid black;  padding: 6px;">Image Pending</td>
                    <?php } ?>
                    <?php if(empty($no_heading)){ ?>
                    <td style="border: 1px solid black;  padding: 6px; text-align: right;"><?= $value['pcs']?></td>
                    <?php }else{?>
                     <td style="border: 1px solid black;  padding: 6px; text-align: right;"><?= $value['remark']?></td>
                     <?php } ?>

                </tr>
            
            <?php } ?>
             <tr>
                    <td style="border: 1px solid black;  padding: 6px;">TOTAL</td>
                    <?php if(!empty($no_heading)){ ?>
                    <td style="border: 1px solid black;  padding: 6px; text-align: right;"></td>
                    <?php } ?>
                    <?php if($result[0]['department_id'] !='2' && $result[0]['department_id'] !='3' && $result[0]['department_id'] !='6' && $result[0]['department_id'] !='10'){ ?>
                    <td style="border: 1px solid black;  padding: 6px; text-align: right;"></td>
                    <?php } ?>

                 <?php if(!empty($no_heading)){ $dis_total=$wt;}else{ $dis_total=$qnt;}?> 
                    <td style="border: 1px solid black;  padding: 6px; text-align: right;"><?= $dis_total ?></td>
                   <?php if(empty($no_heading)){ ?>
                    <td style="border: 1px solid black;  padding: 6px; text-align: right;"><?= $net_wt?></td>
                    <?php } ?>
                    <td style="border: 1px solid black;  padding: 6px;"></td>
                    <?php if(empty($no_heading)){ ?>
                    <td style="border: 1px solid black;  padding: 6px; text-align: right;"><?= $pcs?></td>
                    <?php }else{?>     
                    <td style="border: 1px solid black;  padding: 6px; text-align: right;"></td>     
                    <?php } ?>       

                </tr>

        </table>

    </div>