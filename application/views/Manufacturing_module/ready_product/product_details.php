<div class="content-page">
<div class="content">
   <div class="container">
      <div class="row">
         <div class="col-sm-12 card-box-wrap">
            <div class="panel panel-default">
              <div class="panel-heading">
               <h4 class="inlineBlock"><?= $display_name;?></h4>
               <h5>Product Code : <?=$product_code;?>  
                   karigar Name: <?=$product_details[0]['karigar_name'];?>
                </h5>
               <?php if($department_id == '8'){ $table_name="mfg_ready_product_details_mangalsutra";}else if($department_id == '11'){ $table_name="mfg_ready_product_details_bangl";}else{ 
                $url=$this->uri->segment(1);
                if($url == "Sales_recieved_product"){
                $table_name="mfg_sales_ready_product_details";
                }else{
                $table_name="mfg_ready_product_details";
                }

                ;}?>
              </div>
              <div class="panel-body">
                <div class="clearfix"></div>
                <div class="table-rep-plugin">
                  <div class="table-responsive b-0 scrollhidden">
                    <table class="table table-bordered custdatatable" id="<?=$table_name;?>">
                      <thead>
                       <?php
                          $data['heading']=$table_name;
                          $this->load->view('master/table_header',$data);
                        ?>
                         <!-- <tr>
                             <th class="col4">Sub Category Name</th>
                             <th class="col4">Category Name</th>
                             <th class="col4">Code</th>
                            <th class="col3"></th>
                         </tr> -->
                      </thead>
                    </table>
                  </div>
                </div>
              </div>              
            </div>
         </div>
      </div>
   </div>
</div>