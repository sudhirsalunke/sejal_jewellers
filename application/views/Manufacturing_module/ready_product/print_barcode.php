<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
   "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">
   <head>
      <meta http-equiv="content-type" content="text/html;charset=UTF-8" />
      <title>Barcode</title>
      <link href="<?=ADMIN_CSS_PATH.'barcode/reset.css'?>" media="screen, print" rel="stylesheet" type="text/css" />
      <link href="<?=ADMIN_CSS_PATH.'barcode/grid.css'?>" media="screen, print" rel="stylesheet" type="text/css" />
      <link href="<?=ADMIN_CSS_PATH.'barcode/base.css'?>" media="screen, print" rel="stylesheet" type="text/css" />
   </head>
   <body>
      <div class="container_16">
         <div class="grid_16">
            <div id="content">
               <style>
                  .barcode_label {
                  font-family: Verdana;
                  font-size:7px;
                  font-weight:bold;
                  line-height:1;
                  height:40px;
                  margin:0px 0px 0px 70px;
                  padding-top:0px; page-break-before:always;}
                  .all_borders {
                  border: solid 1px #000;
                  }
                  .overdiv   {
                  font-family:Verdana;
                  font-size:10px;font-weight:bold;height:1em;
                  left:-172px;
                  position:relative;
                  text-align:center;
                  width:80px;
                  z-index:10;
                  }
                  .shilpi {
                  height:1em;
                  position:relative;
                  font-size: 7px;
                  top: 8px;
                  z-index:9;
                  left:-5px;
                  
                  }
                  .barcode_img {
                  position:relative;
                  left:-90px;
                  top:2px; }
                  .omega {margin-left:-15px}
               </style>
               <?php 
               //print_r($receipt_code);die;
               if(!empty($receipt_code)){
               foreach ($receipt_code as $key => $value) { 
                 // print_r($result);
                  if(!empty($barcode)){
                    $gw = (!empty(ceil($value['gr_wt']))) ? 'G: '.$value['gr_wt'].' / ' : '' ;
                    $nw = (!empty(ceil($value['net_wt']))) ? 'N: '.$value['net_wt']: '' ; 
                    $KW = (!empty(ceil($value['custom_kun_wt']))) ? 'KW: '.$value['custom_kun_wt'].' / ' : '' ;
                    $KP = (!empty(ceil($value['kundan_pc']))) ? 'KP: '.$value['kundan_pc']: '' ; 
                    $Q = (!empty(ceil($value['quantity']))) ? 'Q: '.$value['quantity'].' / ' : '' ;
                    $P = (!empty($value['sub_category'])) ? 'P: '.$value['sub_category'] : '' ;
                    $SW = (!empty(ceil($value['stone_wt']))) ? 'SW: '.$value['stone_wt'].'' : '' ;
                  
                  }else{
                    $gw = (!empty(ceil($value['custom_gr_wt']))) ? 'G: '.$value['custom_gr_wt'].' / ' : '' ;
                    $nw = (!empty(ceil($value['custom_net_wt']))) ? 'N: '.$value['custom_net_wt']: '' ;
                    $Q = (!empty(ceil($value['quantity']))) ? 'Q: '.$value['quantity'].' / ' : '' ;          
                    $P = (!empty($value['sub_category'])) ? 'P: '.$value['sub_category'] : '' ;
                    $KW = (!empty(ceil($value['custom_kun_wt']))) ? 'KW: '.$value['custom_kun_wt'].' / ' : '' ;  
                    $KP = (!empty(ceil($value['kundan_pc']))) ? 'KP: '.$value['kundan_pc'].'' : '' ;          
                    $SW = (!empty(ceil($value['custom_stone_wt']))) ? 'SW: '.$value['custom_stone_wt'].'' : '' ;
       
                  }
                  ?>
               <div class="barcode_label">
                  <div class="grid_3 alpha">
                     &nbsp;&nbsp;&nbsp;<br>
                     &nbsp;&nbsp;&nbsp;<?=$gw.$nw?> <br> 
                     <?php if (!empty($KW.$KP)) { ?>
                     &nbsp;&nbsp;&nbsp;<?=$KW.$KP?><br>
                     <?php } ?>
                     <?php if (!empty($Q.$P)) { ?>
                     &nbsp;&nbsp;&nbsp;<?=$Q.$P?><br>
                     <?php } ?> 
                     <?php if (!empty($SW)) { ?>
                     &nbsp;&nbsp;&nbsp;<?=$SW?><br>
                     <?php } ?>
                      
                  </div>
                  <div class="grid_10 omega">
                     <span class="shilpi"><?=$value['product_code']?></span> 
                     <span class="barcode_img"><?= (!empty($value['barcode_path'])) ? "<img src=".ADMIN_PATH.'uploads/barcode/'.$value['barcode_path']." style='width:128px;height:25px;margin-top:1px'> " : ''?></span>
                     <span class="overdiv" style="margin-top:2px;"></span>               
                  </div>
                  <div class="clear"></div>
                  <br>
               </div>
               <?php } }else {echo "No Barcode Data!";}?>
               
      

            </div>
            <div class="clear"></div>
         </div>
         <div class="clear"></div>
      </div>
   </body>
</html>