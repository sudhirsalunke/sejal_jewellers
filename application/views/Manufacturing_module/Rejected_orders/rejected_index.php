<div class="content-page">
<div class="content">
   <div class="container">
      <div class="row">
         <div class="col-sm-12 card-box-wrap">
            <div class="card-box padmobile">
               <h4 class="header-title business_manager_header_title m-t-0 m-b-30 bmsmheader bmsmheaderabm"><span><?= $display_name;?></span>
               </h4>
                <!-- <button  type="button" onclick="export_qc_products()" class="add_senior_manager_button btn btn-primary  waves-effect w-md waves-light m-b-5 hidden-xs  btn-md">Export</button> -->
                <a href="<?= ADMIN_PATH?>Mnfg_accounting_rejected_orders/Manufacturing_rejected_receipt">
                 <button  type="button" class="add_senior_manager_button btn btn-primary  waves-effect w-md waves-light m-b-5 hidden-xs  btn-md">Rejected products from manufacturing</button>
                </a> 
               <div class="clearfix"></div>
               <div class="table-rep-plugin">
                  <div class="table-responsive b-0 scrollhidden">

              <form enctype='multipart/form-data' class="check_all_qc" role="form" name="product_form" id="export" action="Mnfg_accounting/check_all" method="post">
               <table class="table table-bordered custdatatable" id="manufacturing_rejected_product">
                  <thead>
                    <tr>
                      <th class="col4"/>#</th>
                      <th class="col4">Order Id</th>
                      <th class="col4">Order Date</th>
                      <th class="col4">Rejected Date</th>
                      <th class="col4">Karigar Name</th>
                      <th class="col4">Parent Category</th>
                      <th class="col4">Weight Range</th>
                     <!--  <th class="col4">Order Quantity</th> -->
                      <th class="col4">Rejected Quantity</th>
                    </tr>
                  </thead>
               </table>
              </form>
               </div>
               </div>
            </div>
         </div>
      </div>
   </div>
</div>