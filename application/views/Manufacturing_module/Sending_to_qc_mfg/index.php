<div class="content-page">
<div class="content">
   <div class="container">
      <div class="row">
         <div class="col-sm-12 card-box-wrap">
            <div class="panel panel-default">
               
               <div class="panel-heading ">
               <h4 class="inlineBlock"><?= $display_name;?></h4>
               <div class="pull-right single-add btn-wrap3">
                </div>
               </div>
                    
                <div class="panel-body">
               <div class="table-rep-plugin">
                  <div class="table-responsive b-0 scrollhidden">
   
                      <form enctype='multipart/form-data' class="check_all_qc" role="form" name="product_form" id="export" action="Manufacturing_quality_control/check_all" method="post">
                        <?php 
                      if ($dep_id != '2'&& $dep_id !='3' && $dep_id !='6' && $dep_id !='10') {
                        $table_name='sending_to_qc';
                      } else {
                        $table_name='sending_to_qc_bom_dept';
                      }
                    
                  ?>
                  <input type="hidden" name="dep_id" id="dep_id" value="<?php echo $dep_id; ?>">
                       <table class="table table-bordered custdatatable" id="<?php echo $table_name; ?>">
                          <thead>
                            <?php
                                  $data['heading']=$table_name;
                                  $this->load->view('master/table_header',$data);
                            ?>
                           </thead>
                       </table>
                      </form>
                   </div>
                  </div>
               </div>
            </div>
         </div>
      </div>
   </div>
</div>