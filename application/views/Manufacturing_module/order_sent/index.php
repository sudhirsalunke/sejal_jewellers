<div class="content-page">
<div class="content">
   <div class="container">
      <div class="row">
         <div class="col-sm-12 card-box-wrap">
            <div class="card-box padmobile">
              <div class="panel panel-default">
              <div class="panel-heading">
               <h4 class="inlineBlock"><?= $display_title;?>
               </h4>
               </div>
               </div>
                <!-- <a href="Department"><button  type="button" class="add_senior_manager_button btn btn-purp  waves-effect w-md waves-light m-b-5  btn-md">Create New Order </button></a> -->
               <div class="clearfix"></div>
               <div class="panel-body">
               <div class="table-rep-plugin">
                  <div class="b-0 scrollhidden"> 
                  <?php 
                    if ($dep_id != '2'&& $dep_id !='3' && $dep_id !='6' && $dep_id !='10') {
                      $table_name='order_sent';
                    } else {
                      $table_name='order_sent_oth_dep';
                    }
                    
                  ?>
                  <div class="table-responsive">                     
                     <table class="table table-bordered custdatatable" id="<?php echo $table_name;?>">
                        <thead>
                        <?php
                          $data['heading']= $table_name;
                          $this->load->view('master/table_header',$data);
                        ?>
                           <!-- <tr>
                              <th class="col4">Order ID</th> -->
                              <!-- <th class="col4">Product Code</th> -->
                       <!--        <th class="col4">Order Date</th>
                              <th class="col4">Delivery Date</th>
                              <th class="col4">Karigar Name</th>
                              <th class="col4">Sub Category</th>
                              <th class="col4">Weight Range</th>
                              <th class="col4">Quantity</th>
                              <th class="col4">Approx Weight</th>
                              <th class="col4">Received Quantity</th>
                              <th class="col3"></th>
                           </tr> -->
                        </thead>
                     </table>
                  </div>                   
                  </div>                   
                  </div>
               </div>
            </div>
         </div>
      </div>
   </div>
</div>