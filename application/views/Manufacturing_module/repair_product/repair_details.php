<div class="row">
  <div class="col-sm-12">
    <table class="table m-b-0">
      <thead>
          <tr>
              <th>#</th>
               <?php if($dep_id != '2' && $dep_id !='3'  && $dep_id != '6'  && $dep_id != '10'){ ?>
              <th>Product Code</th>
              <?php }?>
              <th>Repair Date</th>
              <?php if($dep_id == '2' || $dep_id=='3' || $dep_id =='6' || $dep_id=='10'){ ?>
              <th>Weight</th>
              <?php }else{?>
              <th>Gr Wt</th>
              <?php }?>
              <th>Net Wt</th>
              <?php if($dep_id != '2' && $dep_id !='3' && $dep_id != '6' && $dep_id != '10'){ ?>
              <th>QTY</th>
                <?php }?>
           
          </tr>
      </thead>
      <tbody>
    <?php 
            //print_r($repair_details);
            if(sizeof($repair_details)>0){
              foreach($repair_details as $index => $value){
          ?>
            <tr>
                <td scope="row" class="success text-right" ><?=$index+1?></td>
                <?php if($dep_id != '2' && $dep_id !='3'  && $dep_id != '6'  && $dep_id != '10'){ ?>
                <td class="success"><?=$value['product_code']?></td>  
                 <?php }?>              
                <td class="success text-right" ><?=$value['date']?></td>
                <td class="success text-right" ><?=$value['gr_wt']?></td>
                <td class="success text-right" ><?=$value['net_wt']?></td>
                <?php if($dep_id != '2' && $dep_id !='3'  && $dep_id != '6'  && $dep_id != '10'){ ?>
                <td class="success text-right"><?=$value['quantity']?></td>
                <?php }?>
              </tr>
          <?php } }else{ ?>
          <tr>
          <td colspan="6">NO Data Found!</td>
          </tr> 
    <?php }?>
      </tbody>
    </table>
  </div>
</div>
   