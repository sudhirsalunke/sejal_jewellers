<div class="content-page">
<div class="content">
   <div class="container">
      <div class="row">
         <div class="col-sm-12 card-box-wrap">
            <div class="panel panel-default">
              <div class="panel-heading">
                <h4 class="inlineBlock"><span><?= $display_name;?></span></h4>
                  
              </div>
              <div class="panel-body"> 

               <div class="clearfix"></div>
               <div class="table-rep-plugin">
                  <div class="table-responsive b-0 scrollhidden">
                <?php if($department_id == '11'){ 
                  $table_name="manufacturing_repair_product_bgtrios_list";
                  }else{
                    $table_name="manufacturing_repair_product_list";
                    }?>
                <table class="table custdatatable table-bordered" id="<?=$table_name;?>">
                 <thead>
                  <?php
                          $data['heading']=$table_name;
                          $this->load->view('master/table_header',$data);
                        ?>
               
                  </thead>
                 
               </table>
            
               </div>
               </div>
              </div>
            </div>
         </div>
      </div>
   </div>
</div>