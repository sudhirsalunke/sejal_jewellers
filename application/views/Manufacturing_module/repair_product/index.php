<div class="content-page">
<div class="content">
   <div class="container">
      <div class="row">
         <div class="col-sm-12 card-box-wrap">
            <div class="panel panel-default">
              <div class="panel-heading">
                <h4 class="inlineBlock"><span><?= $display_name;?></span></h4>
                     <div class="pull-right single-add btn-wrap3">
               <button type="button" onclick="repair_to_ready_product_all()" class="add_senior_manager_button btn btn-primary  waves-effect w-md waves-light m-b-5 btn-md pull-right single-add ready_product_btn">Product Ready</button>
               </div>
              </div>
              <div class="panel-body"> 

               <div class="clearfix"></div>
               <div class="table-rep-plugin">
                  <div class="table-responsive b-0 scrollhidden">
                  <?php if($department_id == '2' || $department_id=='3' || $department_id =='6' || $department_id=='10'){ 
                        $table_name='mfg_repair_product_bom'; 
                    }elseif($department_id == '8'){ 
                        $table_name='manufacturing_repair_mangalsutra';
                      }elseif($department_id == '11'){ 
                        $table_name='manufacturing_repair_bgtrios';
                      }else{ 
                        $table_name='manufacturing_repair_product';}?>
               
              <form enctype='multipart/form-data' role="form" name="repair_product" id="repair_product">
                <table class="table custdatatable table-bordered" id="<?= $table_name;?>">
                 <thead>
                  <?php
                          $data['heading']=$table_name;
                          $this->load->view('master/table_header',$data);
                        ?>
               
                  </thead>
                 
               </table>
              </form>
               </div>
               </div>
              </div>
            </div>
         </div>
      </div>
   </div>
</div>