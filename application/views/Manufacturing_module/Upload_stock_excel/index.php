<div class="content-page">
<div class="content">
   <div class="container">
      <div class="row">
         <div class="col-md-10 col-sm-12 card-box-wrap">
            <div class="panel panel-default">

              <div class="panel-heading frmHeading">                
               <h4 class="inlineBlock"><?= $page_title;?></h4>

              </div>
               <div class="panel-body">
                  <form name="upload_stock_excel" id="upload_stock_excel" role="form" class="form-horizontal" method="post" enctype="multipart/form-data">
                    
                     <div class="form-group">
                        <label class="col-md-3 col-sm-3 control-label" for="inputEmail3"> Import Excel <span class="asterisk">*</span></label>

                        <div class="col-md-3 col-sm-5 m-t-0  Choose-new">
                           <label class="custom-file-upload">
                          <input type="file" placeholder="Enter Name" id="category" class="" name="upload_stock_excel[file]">
                          <i class="fa fa-upload"></i> Choose File
                        </label>
                        <div class='r m-b-5'>
                        </div> 
                        
                           <span class="text-danger" id="file_empty_error"></span>
                        </div>


                        <label class="col-md-3 col-sm-4 control-label" for="inputEmail3">
                          <a href="<?= UPLOAD_IMAGES?>template/Upload_stock_excel.xlsx" class="btn btn-primary  waves-effect w-md waves-light m-b-5"><i class="fa fa-download" aria-hidden="true"></i> Download Template</a>
      

                        </label>
                     </div> 
                        
                
                    <div class="texalin btnSection row">       
                   <a href="<?= ADMIN_PATH?>ready_product"><button class="btn btn-default waves-effect waves-light btn-md pull-left" name="commit" type="button">Back</button></a>        
                      <button class="btn upload-btn waves-effect waves-light btn-md pull-right save_stock_excel" name="commit" type="button" onclick="Save_stock_excel();" style="width:97px;" >
                         UPLOAD
                      </button>
                    </div>                  
                  </form>
                  <div class="div_error col-sm-offset-5 col-sm-8"></div>   
               </div>
               <div class="clearfix"></div>
            </div>
         </div>
      </div>
   </div>
</div>
