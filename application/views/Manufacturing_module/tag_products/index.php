<div class="content-page">
<div class="content">
   <div class="container">
      <div class="row">
         <div class="col-sm-12 card-box-wrap">
            <div class="card-box padmobile clearfix">
            <div class="panel-heading frmHeading">
               <h4 class="inlineBlock"><span><?=$display_name;?></span>
               </h4>
               </div>
               <div class="col-lg-12">
                  <form id="tag_products" role="form" class="form-horizontal" method="post">
                    <input type="hidden" id="code_name" >
                    <input type="hidden" id="karigar_name">
                            <div class="col-lg-12">
                                <div class="form-group">
                                    <label class="col-sm-2 control-label" for="inputEmail3"> Select Karigar<span class="asterisk">*</span></label>
                                    <div class="col-sm-2">
                                        <select class="form-control" name="karigar_id" onchange="get_hallmarked_pro_by_kariger(this.value)" id="kariger_id">
                                            <option value=''>Select Karigar</option>
                                            <?php 
                                            if(!empty($karigar_ids)){
                                                foreach ($karigar_ids as $key => $value) {
                                                    ?>
                                                    <option value="<?= $value['karigar_id']?>"><?= $value['name']?></option>
                                                    <?php
                                                  }                                           
                                            }  
                                            ?>
                                        </select>
                                        <span class="text-danger" id="karigar_id_error"></span>
                                    </div>
                                    <label class="col-sm-2 control-label" for="inputEmail3">Select Product Category<span class="asterisk">*</span></label>
                                    <div class="col-sm-2">
                                         <select class="form-control product_name" name="sub_category_id" id="parent_category_id" onchange="get_quantity_by_qc(this.value)">
                                            <option value=''>Select Product</option>
                                        </select>
                                        <span class="text-danger" id="sub_category_id_error"></span>
                                    </div>
                                    
                                </div>
                            </div>
                            <div class="col-lg-12">
                                <div class="form-group">
                                   <!--  <label class="col-sm-2 control-label" for="inputEmail3"> Select Product Code <span class="asterisk">*</span></label>
                                    <div class="col-sm-2">
                                        <select class="form-control product_code">
                                            <option value=''>Select Product Code</option>
                                        </select>
                                        <span class="text-danger" id="sub_category_id_error"></span>
                                    </div> -->

                                    <!-- <label class="col-sm-2 control-label" for="inputEmail3"> Net Weight<span class="asterisk">*</span></label>
                                    <div class="col-sm-2">
                                        <input type="text" min="0" placeholder="" class="form-control quantity" name="net_wt">
                                        <span class="text-danger" id="net_wt_error"></span>
                                    </div> -->

                                    <label class="col-sm-2 control-label" for="inputEmail3">Quantity<span class="asterisk">*</span></label>
                                    <div class="col-sm-1">
                                        <input type="text"  placeholder="" id="quantity" class="form-control quantity" onkeyup="tag_product_table_create(this)">
                                        <!-- <input type="text"  placeholder="" id="quantity" class="form-control quantity"> -->
                                        <span class="text-danger" id="quantity_error"></span>
                                    </div>
                                </div>
                                <!-- <div class="form-group">
                                    <label class="col-sm-2 control-label" for="inputEmail3">Is Sub Category<span class="asterisk">*</span></label>
                                    <div class="col-sm-2 form-inline">
                                        <div class="radio radio-success">
                                          <input type="radio" name="is_sub_cat" value="1" id="radio2" class="dispatch_mode">
                                            <label for="radio2">Yes</label>
                                        </div>
                                        <div class="radio radio-success">
                                          <input type="radio" name="is_sub_cat" value="0" id="radio2" class="dispatch_mode">
                                            <label for="radio2">No</label>
                                        </div>
                                        <span class="text-danger" id="is_sub_cat_error"></span>
                                    </div>
                                     <input type="hidden"  placeholder="" id="quantity_hidden" class="form-control quantity" name="Quantity" readonly="">
                                </div> -->
                                <!-- <div class="form-group">
                                    <div class="col-sm-offset-4 col-sm-1 texalin">
                                        <button class="btn btn-primary waves-effect waves-light  btn-md" name="commit" type="button" onclick="save_tag_products();">
                                            ADD
                                        </button>
                                    </div>
                                </div> -->
                                <div class="form-group">
                                </div>
                                <label class="col-sm-7 control-label" for="inputEmail3"> <span class="text-danger" id="common_error"></span></label>
                            </div>
                            <div class="added_cat" style="display: none">
                            <!-- <h4 class="header-title business_manager_header_title m-t-0 m-b-30 bmsmheader bmsmheaderabm"><span>Added Sub Categories</span></h4> -->
                            <div class="table-rep-plugin">
                               <div class="clearfix"></div>
                                <div class="b-0 scrollhidden table-responsive">
                                    <table class="table table-bordered" id="tag_products_tbl">
                                        <thead>
                                            <tr>
                                                <th class="col4">#</th>
                                                <!-- <th class="col4">Karigar Name</th> -->
                                                <th class="col4">Product</th>
                                                <th class="col4">Weight Range</th>
                                                <th class="col4">Item category</th>
                                                <!-- <th class="col4">Code</th> -->
                                               <!--  <th class="col4">Sub Code</th> -->
                                                <!-- <th class="col4">Code No.</th> -->
                                                <th class="col4">Gr Wt</th>
                                                <th class="col4">Net Wt</th>
                                                <th class="col3">Product code</th>
                                               <!--  <th class="col3"></th> -->
                                                <th></th>
                                            </tr>
                                        </thead>
                                        <tbody> 
                                            <tr>
                                                  <td colspan="5"  id="first_tr">No Data found</td>
                                            </tr>
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                             </form>
                            <div class="col-lg-12">
                                <div class="form-group">
                                    <div class="col-sm-offset-5 col-sm-8 texalin">
                                        
                                        <input type="hidden" name="selected_ids" id="tag_products_added">
                                        <span id="error_pdf" class="text-danger"></span><br>
                                        <button class="btn btn-success waves-effect waves-light  btn-md" name="commit" type="button" onclick="print_tag_products();">SUBMIT</button>
                                        
                                         
                                    </div>
                                </div>
                            </div>
                                            
                    </div>
               </div>
               <div class="clearfix"></div>
            </div>
         </div>
      </div>
   </div>
</div>