<html>
<style>
.space
{
  height: 30px;
}
.space1
{
  height: 20px;
}
.karigar_name
{
  font-weight:bold;
}
.product_listing >div
{
  background-color: #fff;
}
.barcode_text {
    color: #000;
    font-size: 10px;
    font-weight: bold;
    text-align: left;
    margin-left: 10px;
}

table h4{
  padding: 3px 5px;
}
.single_product_div p{font-size: 11px;line-height: 5px;}

@media print {
  @page {
    size: auto;   /* auto is the initial value */
    margin: 0;  /* this affects the margin in the printer settings */
}
  body * {

    visibility: hidden;
  }
  .print_area, .print_area * {
    visibility: visible;
  }
  .print_area {
  
    left: 0;
    top: 0;
  }
  

}


</style>
<body onload="window.print()">
<center><div class="content-page" >
<div class="content">
   <div class="container">
      <div class="row">
         <div class="col-sm-12 card-box-wrap">
            <div class="card-box padmobile">
         <!--       <h4 class="header-title business_manager_header_title m-t-0 m-b-30 bmsmheader bmsmheaderabm"><span><?= strtoupper('TAG PRODUCTS');?></span>
               </h4> -->
                <div class="clearfix"></div>
               <div class="product_listing">
                <div class="print_area">
                
                <div class="col-sm-6 m-t-20 single_product_div">
                   
                      <?php foreach ($tag_products as $key => $value) { ?>
                     
                        
                       <!--  <h6>Karigar Name :<?=$value['karigar_name']?></h6> -->
                        <p>Product :<?=$value['sub_category']?>&nbsp;&nbsp;Code :<?=$value['product_code']?></p>
                        <p>Gr WT :<?=round($value['gr_wt'],2)?>&nbsp;Net WT :<?=round($value['net_wt'],2)?>&nbsp;QTY :<?=$value['quantity']?></p>
                        <p><?= (!empty($value['barcode_path'])) ? "<img src=".ADMIN_PATH.'uploads/barcode/'.$value['barcode_path'].">" : ''?></p>
                        <hr>

                      <?php } ?>
                    
                </div>
              </div> 
              <div class="clearfix"></div>
            </div>
               </div>
             </div>
            </div>
         </div>
      </div>
   </div>
</div>
</center>

</body>
</html>