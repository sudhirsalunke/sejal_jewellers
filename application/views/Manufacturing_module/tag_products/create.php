<div class="content-page">
<div class="content">
   <div class="container">
      <div class="row">
         <div class="col-sm-12 card-box-wrap">
            <div class="panel panel-default">
             <div class="panel-heading">
               <h4 class="inlineBlock"><span><?= @$display_title?></span></h4>
               <div>
               <div class="panel-body">
               <div class="col-lg-12">
                  <form name="category" id="Metal" role="form" class="form-horizontal" method="post">
                     <div class="form-group">
                        <label class="col-sm-5 control-label" for="inputEmail3">Order Number</label>
                        <div class="col-sm-4"><?php echo $result[0]['order_id'];?>
                        </div>
                     </div>
                     <div class="form-group">
                        <label class="col-sm-5 control-label" for="inputEmail3">Sub Category</label>
                        <div class="col-sm-4"><?php echo $result[0]['sub_cat_name'];?>
                        </div>
                     </div>
                     <div class="form-group">
                        <label class="col-sm-5 control-label" for="inputEmail3">Quantity</label>
                        <div class="col-sm-4"><?php echo $result[0]['qc_quantity'];?>
                        </div>
                     </div>
                    <div class="form-group">
                     <div class="col-sm-offset-4 col-sm-8 texalin">
                         <a href="<?= ADMIN_PATH?>Manufacturing_tag_products/print_tag_products/<?=$result[0]['id']?>" target="_blank"><button class="btn btn-primary waves-effect waves-light  btn-md" onclick="Redirect_url('Manufacturing_hallmarking?status=receive')" name="commit" type="button">
                         Print
                         </button></a>
                         <button class="btn btn-default waves-effect waves-light m-l-5  btn-md" onclick="window.location='#'" type="reset">
                           CANCEL
                           </button>
                     </div>
                  </div>
                  </form>
               </div>
               </div>
               <div class="clearfix"></div>
            </div>
         </div>
      </div>
   </div>
</div>