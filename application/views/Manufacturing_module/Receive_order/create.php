.<div class="content-page">
<div class="content">
   <div class="container">
      <div class="row">
         <div class="col-sm-12 card-box-wrap">
            <div class="card-box padmobile">
               <h4 class="header-title business_manager_header_title m-t-0 m-b-30 bmsmheader bmsmheaderabm"><span><?= @$display_title?></span>
               </h4>
               <div class="col-lg-12">
                  <form role="form" class="form-horizontal" method="post" action="print_receipt" id="print_receipt_frm" onsubmit="return validate_print_kariger_from()">
                    
                            <div class="col-lg-12">
                                <div class="form-group">
                                    <label class="col-sm-2 control-label" for="inputEmail3"> Select Kariger<span class="asterisk">*</span></label>
                                    <div class="col-sm-2">
                                        <select class="form-control" name="kariger_id" onchange="get_receive_product_from_kairger(this.value)" id="kariger_id">
                                            <option value=''>Select Karigar</option>
                                            <?php 
                                            if(!empty($all_karigars)){
                                            foreach ($all_karigars as $key => $value) {
                                             	foreach ($receive_from_kariger as $rec_value) {                                             	
                                             	    if($rec_value['karigar_id'] == $value['id']){
                                             	    	 ?>
	                                                    <option value="<?= $value['id']?>"><?= $value['name']?></option>
	                                                    <?php
                                             	    }
                                             	}                                                                                    
                                              }                                           
                                            }  
                                            ?>
                                        </select>
                                        <span class="text-danger" id="kariger_id_error"></span>
                                    </div>
                                 
                                </div>
                            </div>
                            <div class="col-lg-12">
                                <div class="form-group">
                                     <div class="col-sm-4 table-rep-plugin col-lg-offset-2 col-lg-4">

                                        <div class="table-responsive b-0 scrollhidden">
                                           <table class="table table-bordered custdatatable">
                                              <thead>
                                                 <tr>
                                                    <th class="col1">Sub Category</th>                                                  
                                                    <th class="col1">Quantity</th>
                                                 </tr>
                                              </thead>
                                              <tbody id="product_details">
                                                 <tr>
                                                    <th class="col1">No data found..</th>
                                                    <th class="col1"></th>                                                  
                                                 </tr>
                                              </tbody>
                                           </table>
                                           
                                           
                                           </div>
                                        </div>                                             
                                </div>
                             </div>
                          
                            <div class="col-lg-12">
                                <div class="form-group">
                                    <label class="col-sm-2 control-label" for="inputEmail3"> Gross Weight <span class="asterisk">*</span></label>
                                    <div class="col-sm-2">
                                        <input type="number" min="0" placeholder="" id="gross_wt" class="form-control" name="gross_wt">
                                        <span class="text-danger" id="gross_wt_error"></span>
                                    </div>

                                    <label class="col-sm-2 control-label" for="inputEmail3"> Receive Weight<span class="asterisk">*</span></label>
                                    <div class="col-sm-2">
                                        <input type="number" min="0" placeholder="" id="receive_wt" class="form-control" name="receive_wt">
                                        <span class="text-danger" id="receive_wt_error"></span>
                                    </div>
                                 
                                </div>
                             </div>
                             
                            <!--  <div class="col-lg-12">
                                <div class="form-group">
                                    
                                    <label class="col-sm-2 control-label" for="inputEmail3">Quantity<span class="asterisk">*</span></label>
                                    <div class="col-sm-1">
                                        <input type="number" min="0" placeholder="" id="Quantity" class="form-control" name="quantity">
                                        <span class="text-danger" id="quantity_error"></span>
                                    </div>
                                </div>
                             </div><br/><br/> -->
                            <div class="col-lg-12">
                               <div class="col-lg-7" id="button_save">
	                                <div class="form-group">
	                                    <div class="col-sm-offset-5 col-sm-8 texalin">
	                                        <button class="btn btn-primary waves-effect waves-light  btn-md" name="commit" type="submit">Print Receipt</button>&nbsp;&nbsp;
	                                        <a href="<?php echo base_url('Received_orders');?>"><button class="btn btn-primary waves-effect waves-light  btn-md" name="commit" type="button">Cancel</button></a>
	                                    </div>
	                                </div>
                               </div>
                            </div>                                        
                  </form>
               </div>
               <div class="clearfix"></div>
            </div>
         </div>
      </div>
   </div>
</div>