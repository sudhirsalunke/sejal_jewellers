<div class="content-page">
<div class="content">
   <div class="container">
      <div class="row">
         <div class="col-sm-12 card-box-wrap">
            <div class="panel panel-default">
              <div class="panel-heading">
                <h4 class="inlineBlock"><span><?= $display_name;?></span></h4>
                <div class="pull-right m-t-5">
                  <!-- <button  type="button" onclick="export_qc_products()" class="add_senior_manager_button btn btn-primary  waves-effect w-md waves-light m-b-5 hidden-xs  btn-md">Export</button> -->
                <a onclick="accounting_receive()">
                 <button  type="button" class="add_senior_manager_button btn btn-primary  waves-effect w-md waves-light m-b-5 hidden-xs  btn-md">Receive</button>
                </a> 
                </div>                
              </div>
              <div class="panel-body">
                <div class="table-rep-plugin">
                  <div class="table-responsive b-0 scrollhidden">
                    <form enctype='multipart/form-data' class="check_all_qc" role="form" name="product_form" id="accounting_receive" action="Manufacturing_quality_control/check_all" method="post">
                      <table class="table table-bordered " id="Acconting_receive_orders_view">
                        <thead>
                          <tr>
                            <th>
                              <div class="checkbox checkbox-purpal">
                              <input value="0" type="checkbox" id="check_all"><label for="check_all"></label>
                              </div>
                            </th>
                            <th class="col4">Karigar Name</th>
                            <th class="col4">Parent Category</th>
                            <th class="col4">approx_weight</th>
                            <th class="col4">Order Quantity</th>
                            <th class="col4">Weight Range</th>
                            <th class="col4">Order Date</th>
                            <th class="col4">Delivery Date</th>
                            <!-- <th class="col4">Pending Quantity</th> -->
                            <!-- <th class="col4"></th> -->
                          </tr>
                        </thead>
                      </table>
                    </form>
                  </div>
                </div>
              </div>
            </div>
         </div>
      </div>
   </div>
</div>