<div class="content-page">
<div class="content">
  <div class="container">
    <div class="row">
      <div class="col-sm-12 card-box-wrap">
        <div class="panel panel-default">
          <div class="panel-heading">
           <h4 class="inlineBlock"><span>Engaged Karigar Details</span></h4>                
          </div>
          <div class="panel-body">
            <div class="table-rep-plugin">
              <div class="table-responsive b-0 scrollhidden">
                <table class="table table-bordered custdatatable">
                  <thead>
                    <tr>
                       <th class="col4">Product Code</th>
                       <th class="col4">Order Id</th>
                       <th>Karigar Engaged Date</th>
                       <th>Proposed delivery date </th>
                       <th class="col4">Quantity</th>
                       <th class="col4">Total Weight</th>
                       <th class="col4">Received Weight</th>
                       <th class="col4">Balance Weight</th>
                    </tr>
                  </thead>
                  <tbody>
                    <?php 
                   
                      foreach ($result as $key => $value) {
                      $total_weight     = $value['approx_weight']*$value['quantity'];
                      $received_weight  = $value['approx_weight']*$value['receive_qty'];
                      $balance_weight   = $total_weight - $received_weight;
                    ?>
                    <?php 
                     ?>
                    <tr>
                        <td><?=$value['product_code'];?></td>
                        <td><?=$value['order_id'];?></td>
                        <td><?=date('d-m-Y',strtotime($value['karigar_engaged_date']));?></td>
                        <td><?=date('d-m-Y',strtotime($value['delivery_date']));?></td>
                        <td><?=$value['quantity'];?></td>
                        <td><?=$total_weight;?></td>
                        <td><?=$received_weight;?></td>
                        <td><?=$balance_weight;?></td>
                    </tr>
                   <?php
                      }
                      if(empty($result))
                      {
                        ?>
                         <tr>
                          <td colspan="7">No data found</td>
                          
                      </tr>
                        <?php
                      }
                   ?>
                  </tbody>
                </table>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>