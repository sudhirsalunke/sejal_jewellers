<div class="content-page">
<div class="content">
   <div class="container">
      <div class="row">
         <div class="col-sm-12 card-box-wrap">
            <div class="panel panel-default">
               <div class="panel-heading frmHeading">
                  <h4 class="inlineBlock"><span><?= @$display_title?></span></h4>                  
               </div>
          
               <div class="panel-body">
                  <div class="col-lg-12">
                     <form name="category" id="Metal" role="form" class="form-horizontal" method="post">
                        <div class="form-group">
                           <label class="col-sm-2 control-label" for="inputEmail3">Order Number</label>
                           <div class="col-sm-4"><?php echo $result['order_id'];?>
                           </div>
                        </div>
                        <div class="form-group">
                           <label class="col-sm-2 control-label" for="inputEmail3">Assign Date</label>
                           <div class="col-sm-4"><?php echo $result['assign_date'];?>
                           </div>
                        </div>
                        <div class="form-group">
                           <label class="col-sm-2 control-label" for="inputEmail3">Delivery Date</label>
                           <div class="col-sm-4"><?php echo $result['view_delivery_date'];?>
                           </div>
                        </div>
                        <div class="form-group">
                           <label class="col-sm-2 control-label" for="inputEmail3">Karigar Name</label>
                           <div class="col-sm-4"><?php echo $result['karigar_name'];?>
                           </div>
                        </div>
                        <div class="form-group">
                           <label class="col-sm-2 control-label" for="inputEmail3">Product Code</label>
                           <div class="col-sm-4"><?php echo $result['product_code'];?>
                           </div>
                        </div>


                     <div class="table-rep-plugin col-lg-12">
                     <div class="table-responsive b-0 scrollhidden">
                        <table class="table table-bordered custdatatable">
                           <thead>
                              <tr>
                                 <th class="col4">Parent Category</th>
                                 <th class="col4">Weight Range</th>
                                 
                              <?php  if($result['department_id']=='2' || $result['department_id']=='3' || $result['department_id'] =='6' || $result['department_id']=='10'):?>
                                 <th class="col4">Weight</th>
                              <?php else:?>   
                                 <th class="col4">Quantity</th>
                                 <th class="col4">Weight</th>
                              <?php endif;?> 
                              </tr>
                           </thead>
                           <tbody>
                              <tr>
                                 <th class="col4"><?php echo $result['sub_cat_name'];?></th>
                                 <th class="col4"><span class="pull-right"><?php echo $result['from_weight'].' - '.$result['to_weight'];?></span></th>
                              <?php  if($result['department_id']=='2' || $result['department_id']=='3' || $result['department_id'] =='6' || $result['department_id']=='10'):?>
                                    <th class="col4"><span class="pull-right"><?php echo $result['total_weight'];?></span></th>
                              <?php else:?>   
                                 <th class="col4"><span class="pull-right"><?php echo $result['quantity'];?></span></th>
                                 <th class="col4"><span class="pull-right"><?php echo $result['approx_weight'];?></span></th>
                              <?php endif;?> 
                              </tr>
                           </tbody>
                        </table>
                        
                        
                        </div>

                     </div>
                     <div class="form-group">
                        <div class="col-sm-offset-4 col-sm-8 texalin">
                            <?php                  
                              foreach ($sub_cat_img as $key => $value) {
                                 if(!empty($value['path']))
                                    echo '<a href="'.UPLOAD_IMAGES.'parent_category/'.$value['name'].'/small/'.$value['path'].'" target="blank"><img src="'.UPLOAD_IMAGES.'parent_category/'.$value['name'].'/small/'.$value['path'].'" class="img-thumbnail img-check"></a>';

                               } 
                           ?>
                            
                        </div>
                     </div>
                     <div class="form-group">
                        <div class=" col-sm-8 texalin btnSection"><!--col-sm-offset-4-->
                           
                            <button class="btn btn-default waves-effect waves-light m-l-5  btn-md" onclick="window.location='<?= ADMIN_PATH?>Karigar_order_list'" type="reset">
                              BACK
                              </button>
                               <a href="<?= ADMIN_PATH?>Karigar_order_list/print_karigar_order/<?=$result['id']?>" target="_blank"><button class="btn btn-success waves-effect waves-light  btn-md pull-right" onclick="Redirect_url('Karigar_order_list')" name="commit" type="button">
                            Print
                            </button></a>
                        </div>
                     </div>
                     </form>
                  </div>
               </div>
               <div class="clearfix"></div>
            </div>
         </div>
      </div>
   </div>
</div>