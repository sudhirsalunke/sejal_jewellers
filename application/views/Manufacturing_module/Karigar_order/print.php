<style media="print">
@page {
  size: auto;
  margin: 0;
}
img {
  -webkit-print-color-adjust: exact;
}
table tr td, table tr th{
  border:1px solid #ddd;
}
</style>
<center>
<div class="content-page">
  <div class="content">
    <div class="container">
      <div class="row">
         <div class="col-sm-12 card-box-wrap">
            <div class="card-box padmobile">
               <h4 class="header-title business_manager_header_title m-t-0 m-b-30 bmsmheader bmsmheaderabm"><span><?= strtoupper($display_title);?></span>
               </h4>

               
                <table style="border:1px solid #ddd; border-collapse: collapse; margin-top: 15px; text-align: left;     font-size: 15px;">
                  <tr>
                    <td style="border:1px solid #ddd; padding: 10px;">Order Id:</td>
                    <th  style="border:1px solid #ddd; padding: 10px; text-align: right;"><?php echo $result[0]['order_id'];?></th>
                    <td style="border:1px solid #ddd; padding: 10px;">Date:</td>
                    <th  style="border:1px solid #ddd; padding: 10px; text-align: right;"><?php echo $result[0]['assign_date'];?></th>
                    <th style="border:1px solid #ddd; padding: 10px;"></th>
                  </tr>
                  <tr>
                    <td style="border:1px solid #ddd; padding: 10px;">Karigar Name:</td>
                    <th  style="border:1px solid #ddd; padding: 10px; "><?php echo $result[0]['karigar_name'];?></th>
                    <td style="border:1px solid #ddd; padding: 10px;">Delivery Date:</td>
                    <th  style="border:1px solid #ddd; padding: 10px; text-align: right;"><?php echo $result[0]['delivery_date'];?></th>
                     <th style="border:1px solid #ddd; padding: 10px;"></th>
                  </tr>
                  <tr>
                    <th style="border:1px solid #ddd; padding: 10px;">Product</th>
                    <th  style="border:1px solid #ddd; padding: 10px;">Varient</th>
                    <th  style="border:1px solid #ddd; padding: 10px;">Weight Range</th>                 
                    <?php if($result[0]['department_id']=='2' || $result[0]['department_id']=='3'|| 
                      $result[0]['department_id']=='6' || $result[0]['department_id']=='10'){  $display_name ='weight';
                    }else{ 
                        $display_name ='quantity'; 
                    }?>
                    <th style="border:1px solid #ddd; padding: 10px;"><?php echo $display_name;?></th>
                    <th  style="border:1px solid #ddd; padding: 10px;">Total Weight</th>
                  </tr>
                   <?php
                        $total_qty = 0;
                        $total_wt = 0;
                        $total_weight = 0;
                          foreach ($result as $key => $value) {      
                          if($value['p_variant'] !=''){
                              $variant=$value['p_variant'];
                          }else{
                              $variant='--';
                          }                     
                    ?>

                  <tr>
                      <td style="border:1px solid #ddd; padding: 10px;"><?php echo $value['sub_cat_name'];?></td>
                      <td  style="border:1px solid #ddd; padding: 10px;"><?php echo   $variant;?></td>
                      <td  style="border:1px solid #ddd; padding: 10px; text-align: right;"><?php echo $value['from_weight'].' - '.$value['to_weight'];?></td>
                    <?php if($value['department_id'] !='2' && $value['department_id']!='3' && 
                      $value['department_id']!='6' &&   $value['department_id']!='10'): 
                          $total_qty += $value['quantity'];
                          $total_weight += $value['approx_weight'];
                    ?>
                      <td style="border:1px solid #ddd; padding: 10px; text-align: right;"><?php echo $value['quantity'];?></td>
                      <td  style="border:1px solid #ddd; padding: 10px; text-align: right;"><?php echo $value['approx_weight'];?></td>
                    <?php else:
                          $total_wt += $value['total_weight'];
                          $total_weight += $value['weight'];
                    ?>
                      <td style="border:1px solid #ddd; padding: 10px; text-align: right;"><?php echo $value['total_weight'];?></td>
                      <td  style="border:1px solid #ddd; padding: 10px; text-align: right;"><?php echo $value['weight'];?></td>  

                  </tr>
                   <?php endif; } ?>
                  <tr>
                    <th style="border:1px solid #ddd; padding: 10px;">Total</th>
                    <td  style="border:1px solid #ddd; padding: 10px;"></td>
                    <td  style="border:1px solid #ddd; padding: 10px;"></td>
                    <?php if($result[0]['department_id'] !='2' && $result[0]['department_id']!='3' &&
                      $result[0]['department_id']!='6' && $result[0]['department_id']!='10'):?>
                    <td style="border:1px solid #ddd; padding: 10px; text-align: right;"><?= $total_qty?></td>
                    <?php else:?>
                    <td style="border:1px solid #ddd; padding: 10px; text-align: right;"><?= $total_wt?></td> <?php endif;?> 
                    <td  style="border:1px solid #ddd; padding: 10px; text-align: right;"><?= $total_weight?></td>
                  </tr>

                </table>
                <div class="clearfix"></div>
               <div class="product_listing">
               <div class="row" style="">
                  <div class="form-group">   
                   <?php                        
                  foreach ($sub_cat_img as $key => $value) {
                    if(!empty($value['path'])){
                      $path = UPLOAD_IMAGES.'parent_category/'.$value['name'].'/large/'.$value['path'];
                      ?>
                      <div class="col-md-2">
                            <label class="btn">
                               <img src="<?= $path?>" class="img-thumbnail img-check">
                               </label>
                        </div> 
                      <?php
                    }
                     ?>                                
                                                                                                                               
                     <?php               
                    }                       
                ?>  
                </div>                                      
              </div> 
             
              <div class="clearfix"></div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
</div></center>

<style>
.space
{
  height: 30px;
}
.space1
{
  height: 20px;
}
.karigar_name
{
  font-weight:bold;
}
.product_listing >div
{
  background-color: #fff;
}
.barcode_text {
    color: #000;
    font-size: 10px;
    font-weight: bold;
    text-align: left;
    margin-left: 10px;
}

table td{
  padding: 3px 5px;
}

@media print {
  /*body * {
    visibility: hidden;
  }*/
  .print_area, .print_area * {
    visibility: visible;
  }
  .print_area {
    
    left: 0;
    top: 0;
  }
  

}

</style>