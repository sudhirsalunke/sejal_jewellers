<div class="content-page">
<div class="content">
   <div class="container">
      <div class="row">
         <div class="col-sm-12 card-box-wrap padmobile">
            <div class="panel panel-default">
            <div class="panel-heading frmHeading">
               <h4 class="inlineBlock"><span><?= strtoupper($display_name);?></span></h4>
               </div>
				<div class="panel-body">
               <form name="category" id="qc_check" role="form" class="form-horizontal" method="post">
               	<input type="hidden" name="receive_product_id" value="<?= @$product_ids?>">
               	<input type="hidden" class="type" name="type" id="type" value="update">
              

               	
                                <div class="form-group">
                                    <label class="col-sm-2 control-label" for="inputEmail3"> Select Kariger<span class="asterisk">*</span></label>
                                    <div class="col-sm-2">
                                        <select class="form-control" name="kariger_id" onchange="get_receive_product_details(this.value)" id="kariger_id">
                                            <option value=''>Select Karigar</option>
                                            <?php 
                                            if(!empty($receive_from_kariger)){
                                             	foreach ($receive_from_kariger as $key => $rec_value) { 
                                             	    	 ?>
	                                                    <option value="<?= $rec_value['karigar_id']?>" <?= ($rec_value['karigar_id']== $karigar_id) ? 'selected' : ''?> ><?= $rec_value['karigar_name']?></option>
	                                                    <?php
                                             	    
                                             	}                                                                                                                            
                                            }  
                                            ?>
                                        </select>
                                        <span class="text-danger" id="kariger_id_error"></span>
                                    </div>
                                 
                                </div>
                           
					 <div class="row">
					  <div class="table-rep-plugin">
					    <div class="table-responsive b-0 scrollhidden">
					      <table class="table custdatatable table-bordered" id="receive_products">
					        <thead>
					           <tr>
					           		<th class="col1">Product</th>
					           		<th class="col1">Item Code</th>
					            	<th class="col1">Qty</th>
					               	<th class="col4">Nt Wt</th>
					              	<th class="col3">Few Wt</th>
					              	<th class="col3">Kun pure</th>
					              	<th class="col3">Meena Wt</th>
					              	<th class="col3">Wax Wt</th>
					              	<th class="col3">C/S Wt</th>
					              	<th class="col3">Moti Wt</th>
					               	<th class="col3">CH wt</th>
					               	<th class="col4">B Bead Wt</th>
					               	<th class="col4">Gr Wt</th>
					              	<th class="col3">CH PCS</th>
					               	<th class="col4">CH@</th>
					               	<th class="col3">CH amt</th>
					              <!--  	<th class="col4">Kun Wt</th> -->
					               	<th class="col4">Kun Pc</th>
					              	<th class="col3">Kun@</th>
					              	<th class="col3">Kun Amt</th>
					              	<th class="col3">stn wt</th>
					              	<th class="col3">stn@</th><!-- c/s@ -->
					              	<th class="col3">Stn Amt</th>					           
					              	<th class="col3">Other Amt</th>
					              	<th class="col3">Total Amt</th>
					              	<th class="col3"></th>
					           </tr>
					        </thead>
					        <tbody>
					        </tbody>
					     </table>
					   </div>
					  </div>
					</div>
					<div class="div_error col-sm-offset-5 col-sm-8"></div>	
					
					<div class="form-group">
						<div class="col-sm-offset-5 col-sm-8 texalin m-t-15">
							<!-- <button class="btn btn-success waves-effect waves-light  btn-md print_kundan_order_up" name="commit" type="button" onclick="update_kundan_karigar_order(this,'update');">Save &amp; Edit</button> -->
							<button class="btn btn-success waves-effect waves-light  btn-md print_kundan_order_up" name="commit" type="button" onclick="receive_kundan_karigar_order(this,'update');">Save &amp; Edit</button>
							<button class="btn btn-info waves-effect waves-light  btn-md print_kundan_order" name="commit" type="button" onclick="receive_kundan_karigar_order(this,'print'); ">print</button>
						</div>
          			</div>
         </div>
      </div>
      </form>
   </div>
</div>
<!-- <style type="text/css">
	.custdatatable .form-control{
		border:1px solid #000;
		margin-left:0px;
	}
</style> -->