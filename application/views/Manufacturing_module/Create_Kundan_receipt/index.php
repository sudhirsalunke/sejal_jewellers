<div class="content-page">
<div class="content">
   <div class="container">
      <div class="row">
         <div class="col-sm-12 card-box-wrap">
            <div class="panel panel-default">
            <div class="panel-heading">
               <h4 class="inlineBlock"><span><?= $page_heading;?></span></h4>
               <div class="pull-right single-receive btn-wrap3">           
               <a href="karigar_receive_order/karigar_receipt" class="add_senior_manager_button btn btn-primary  waves-effect w-md waves-light m-b-5 pull-right btn-md single-receive{" type="button">Enter Product Details</a>
                </div>
               </div>
               <div class="panel-body">
               <div class="table-rep-plugin">
                  <div class="table-responsive b-0 scrollhidden">
                  <table class="table table-bordered custdatatable" id="received_products">
                  <thead>
                   <?php
                          $data['heading']='received_products';
                          $this->load->view('master/table_header',$data);
                        ?>
                <!--      <tr>
                        <th class="col4">Karigar Name</th>
                        <th class="col4">Quantity</th>
                        <th class="col4">Product Code</th>
                        <th class="col4">Product</th> -->
                        <!-- <th class="col4">Weight Range</th>
                        <th class="col4">Approx Wt</th> -->
                   <!--   </tr> -->
                  </thead>
               </table>
               </div>
               </div>
            </div>
         </div>
      </div>
   </div>
</div>