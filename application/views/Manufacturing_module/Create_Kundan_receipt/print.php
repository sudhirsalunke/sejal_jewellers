<!DOCTYPE html>
<html>

<head>
    <meta name="google" content="notranslate" />
    <link href="<?=ADMIN_CSS_PATH?>bootstrap.min.css" rel="stylesheet" type="text/css" />
    <style type="text/css">
        .qc_print_img{width: 120px;height: auto;max-width: 120px;}

        @media print{
            /*.qc_print_img{width: 50px;height: 50px;}*/
            table tr td, table tr th{font-size: 10px;}
        }
    </style>
</head>

<body>

    <div style="width: 90%; margin: 0 auto;">
        <h2 style="text-align: center;">Receipt Voucher</h2>
        <div class="table-responsive">
            <table class="table" style="border: 1px solid black;   border-collapse: collapse;width: 100%;">
                
                <tr>
                    <th style="border: 1px solid black;  padding: 6px;text-align: left;" colspan="4">Chitti No:</th>
                    <td style="border: 1px solid black;  padding: 6px;" colspan="4"><?php echo strtotime(date('Y-m-d H:i:s'))?></td>                
                    <th style="border: 1px solid black;  padding: 6px;text-align: left;" colspan="4">Date</th>
                    <td style="border: 1px solid black;  padding: 6px;" colspan="4"><?= date('d-m-Y')?></td>                
                    <th style="border: 1px solid black;  padding: 6px;text-align: left;" colspan="4">Kudan Bill No</th>
                    <td style="border: 1px solid black;  padding: 6px;" colspan="4"></td>               
                </tr>
                <tr>
                    <th style="border: 1px solid black;  padding: 6px;text-align: left;" colspan="4">Detail</th>
                    <td style="border: 1px solid black;  padding: 6px;" colspan="4"></td>
                    <th style="border: 1px solid black;  padding: 6px;text-align: left;" colspan="4">Karigar Name</th>
                    <td style="border: 1px solid black;  padding: 6px;" colspan="4"><?=$karigar_name?></td>
                    <td style="border: 1px solid black;  padding: 6px;" colspan="4"></td>
                    <td style="border: 1px solid black;  padding: 6px;" colspan="4"></td>               

                </tr>
                <tr>
                    <th style=" text-align: left; border: 1px solid black; padding: 6px; width: 100px;">Sr .No</th>
                    <th style=" text-align: left; border: 1px solid black;  padding: 6px;  width: 100px;">Item Code</th>
                    <th style=" text-align: left; border: 1px solid black;  padding: 6px;  width: 100px;">Pcs</th>
                    <th style=" text-align: left; border: 1px solid black; padding: 6px;  width: 100px;">Net Wt</th>
                    <th style=" text-align: left; border: 1px solid black;  padding: 6px;  width: 100px;">Few Wt</th>
                    <th style=" text-align: left; border: 1px solid black;  padding: 6px;  width: 100px;"> Kun Pure</th>
                    <th style=" text-align: left; border: 1px solid black; padding: 6px;  width: 100px;">Mieena / Cz </th>
                    <th style=" text-align: left; border: 1px solid black;  padding: 6px;  width: 100px;">Wax Wt</th>
                    <th style=" text-align: left; border: 1px solid black;  padding: 6px;  width: 100px;"> C/S Wt</th>
                    <th style=" text-align: left; border: 1px solid black; padding: 6px;  width: 100px;">Moti Wt</th>
                    <th style=" text-align: left; border: 1px solid black;  padding: 6px;  width: 100px;"> CH wt</th>
                    <th style=" text-align: left; border: 1px solid black;  padding: 6px;  width: 100px;">Gr Wt </th>
                    <th style=" text-align: left; border: 1px solid black; padding: 6px;  width: 100px;">CH PCS</th>
                    <th style=" text-align: left; border: 1px solid black;  padding: 6px;  width: 100px;">CH@ </th>
                    <th style=" text-align: left; border: 1px solid black;  padding: 6px;  width: 100px;">CH amt </th>
                  <!--   <th style=" text-align: left; border: 1px solid black;  padding: 6px;  width: 100px;">Kun wt </th> -->
                    <th style=" text-align: left; border: 1px solid black;  padding: 6px;  width: 100px;">Kun Pc </th>
                    <th style=" text-align: left; border: 1px solid black;  padding: 6px;  width: 100px;">Kun@  </th>
                    <th style=" text-align: left; border: 1px solid black; padding: 6px;  width: 100px;">Kun Amt</th>
                    <th style=" text-align: left; border: 1px solid black;  padding: 6px;  width: 100px;">stn wt</th>
                    <th style=" text-align: left; border: 1px solid black;  padding: 6px;  width: 100px;">stn@</th>
                    <th style=" text-align: left; border: 1px solid black;  padding: 6px;  width: 100px;">Stn Amt</th>
                    <th style=" text-align: left; border: 1px solid black; padding: 6px;  width: 100px;">Other Amt</th>
                    <th style=" text-align: left; border: 1px solid black;  padding: 6px;  width: 100px;">Total Amt</th>

                    <th style=" text-align: left; border: 1px solid black;  padding: 6px;  width: 100px;"> Image</th>
                </tr>
                <?php
                $groos_wt = 0;
                $stone = 0;
                $quantity = 0;
                $kundan_amt = 0;
                $kundan_pure = 0;
                $checker_amt = 0;
                $item = 0;
                $few_wt = 0;
                $checker_wt=0;
                $stone_wt=0;
                foreach ($receipt_code as $key => $value) {
                    $groos_wt += $value['gross_wt'];
                    $stone += $value['stone_amt'];
                    $quantity += $value['quantity'];
                    $kundan_amt += $value['kundan_amt'];
                    $checker_amt += $value['checker_amt'];
                    $few_wt += $value['few_wt'];
                    $kundan_pure += $value['kundan_pure'];
                    $checker_wt += $value['checker_wt'];
                    $stone_wt += $value['stone_wt'];
                    $item = $key;
                    ?>
                    <tr>
                        <td style="border: 1px solid black;  padding: 6px; text-align: right;"><?= $key + 1?></td>
                        <td style="border: 1px solid black;  padding: 6px;"><?= $value['product_code']?></td>
                        <td style="border: 1px solid black;  padding: 6px; text-align: right;"><?= $value['quantity']?></td>
                        <td style="border: 1px solid black;  padding: 6px; text-align: right;"><?= $value['net_wt']?></td>
                        <td style="border: 1px solid black;  padding: 6px; text-align: right;"><?= $value['few_wt']?></td>
                        <td style="border: 1px solid black;  padding: 6px; text-align: right;"><?= $value['kundan_pure']?></td>
                        <td style="border: 1px solid black;  padding: 6px; text-align: right;"><?= $value['mina_wt']?></td>
                        <td style="border: 1px solid black;  padding: 6px; text-align: right;"><?= $value['wax_wt']?></td>
                        <td style="border: 1px solid black;  padding: 6px; text-align: right;"><?= $value['color_stone']?></td>
                        <td style="border: 1px solid black;  padding: 6px; text-align: right;"><?= $value['moti']?></td>
                        <td style="border: 1px solid black;  padding: 6px; text-align: right;"><?= $value['checker_wt']?></td>
                        <td style="border: 1px solid black;  padding: 6px; text-align: right;"><?= $value['gross_wt']?></td>
                        <td style="border: 1px solid black;  padding: 6px; text-align: right;"><?= $value['checker_pcs']?></td>
                        <td style="border: 1px solid black;  padding: 6px; text-align: right;"><?= $value['checker@']?></td>
                        <td style="border: 1px solid black;  padding: 6px; text-align: right;"><?= $value['checker_amt']?></td>
                        <td style="border: 1px solid black;  padding: 6px; text-align: right;"><?= $value['kundan_pc']?></td>
                    <!--     <td style="border: 1px solid black;  padding: 6px; text-align: right;"><?= $value['kundan_wt']?></td> -->
                        <td style="border: 1px solid black;  padding: 6px; text-align: right;"><?= $value['kundan_rate']?></td>
                        <td style="border: 1px solid black;  padding: 6px; text-align: right;"><?= $value['kundan_amt']?></td>
                        <td style="border: 1px solid black;  padding: 6px; text-align: right;"><?= $value['stone_wt']?></td>
                        <td style="border: 1px solid black;  padding: 6px; text-align: right;"><?= $value['color_stone_rate']?></td>
                        <td style="border: 1px solid black;  padding: 6px; text-align: right;"><?= $value['stone_amt']?></td>
                        <td style="border: 1px solid black;  padding: 6px; text-align: right;"><?= $value['other_wt']?></td>
                        <td style="border: 1px solid black;  padding: 6px; text-align: right;"><?= $value['checker_amt'] + $value['kundan_amt'] + $value['stone_amt'] + $value['other_wt']?>
                        </td>
                        <td style="border: 1px solid black;padding: 6px;"><?= (!empty( $value['image']) ? '<img class="qc_print_img" src="'.ADMIN_PATH.'/'.$value['image'].'">' : '')?>
                        </td>

                    </tr>
                
                <?php } ?>

            </table>
        </div>

        <div>

            <h2>Total</h2>

            <table style="border: 1px solid black;   border-collapse: collapse;
    width: 30%;">
                <tr row="1">
                    <th style="text-align: center; border: 1px solid black; padding: 6px; width: 100px;" colspan="2">Summary</th>
                </tr>

                <tr>
                    <td style="border: 1px solid black;  padding: 6px;">Gross wt</td>
                    <td style="border: 1px solid black;  padding: 6px; text-align: right;"><?= $groos_wt;?></td>
                </tr>
                <tr>
                    <td style="border: 1px solid black;  padding: 6px;">Total Stone wt</td>
                    <td style="border: 1px solid black;  padding: 6px; text-align: right;"><?= $stone_wt;?></td>
                </tr>
                <tr>
                    <td style="border: 1px solid black;  padding: 6px;">Total Stone Amount</td>
                    <td style="border: 1px solid black;  padding: 6px; text-align: right;"><?= $stone;?></td>
                </tr>
                <tr>
                    <td style="border: 1px solid black;  padding: 6px;">Total Kundan Pure</td>
                    <td style="border: 1px solid black;  padding: 6px; text-align: right;"><?= $kundan_pure;?></td>
                </tr>
                 <tr>
                    <td style="border: 1px solid black;  padding: 6px;">Total Few / Extra wt</td>
                    <td style="border: 1px solid black;  padding: 6px; text-align: right;"><?=$few_wt?></td>
                </tr>
                 <tr>
                    <td style="border: 1px solid black;  padding: 6px;">Checker  Amount</td>
                    <td style="border: 1px solid black;  padding: 6px; text-align: right;"><?= $checker_amt;?></td>
                </tr>
                <tr>
                    <td style="border: 1px solid black;  padding: 6px;">Kundan Amount</td>
                    <td style="border: 1px solid black;  padding: 6px; text-align: right;"><?= $kundan_amt;?></td>
                </tr>
                <tr>
                    <td style="border: 1px solid black;  padding: 6px;">Total Pcs</td>
                    <td style="border: 1px solid black;  padding: 6px; text-align: right;"><?=$quantity?></td>
                </tr>
                <tr>
                    <td style="border: 1px solid black;  padding: 6px;">Total Item</td>
                    <td style="border: 1px solid black;  padding: 6px; text-align: right;"><?=$item+1?></td>
                </tr>

            </table>
        </div>

    </div>