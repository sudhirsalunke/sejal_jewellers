<div class="content-page">
<div class="content">
   <div class="container">
      <div class="row">
        <div class="col-sm-12">
            <h4 class="inlineBlock"><span><?= $display_title;?></span>
            </h4>
        </div>
      </div>
      <div class="row">
         <div class="col-sm-12 card-box-wrap">
            <div class="card-box padmobile">
           <!--  <div class="panel-heading frmHeading">
              
            </div> -->
            <div class="panel-body">
              <div class="col-lg-12">                  
                  <form role="form" target="_blank" class="form-horizontal" method="post"  id="print_receipt_frm">
          
                                          
                            <div class="col-lg-6">
                                <div class="form-group">
                                    <label class="col-sm-5 col-xs-12 control-label" for="inputEmail3">Select Karigar<span class="asterisk">*</span></label>
                                    <div class="col-sm-7 col-xs-12">
                                          <select class="form-control sel_karigar" name="kariger_id" id="kariger_id" onchange="get_karigar_wastage(this);">
                                                <option value="">Please Select karigar</option>                                       
                                                <?php foreach ($karigar as $k_key => $k_value) { ?>
                                                    <option value="<?= $k_value['id'] ?>">
                                                        <?= $k_value['name'] ?>
                                                    </option>
                                                <?php } ?>
                                            </select>
                                        <span class="text-danger" id="kariger_id_error"></span>
                                    </div>
                                 
                                </div>
                            </div>
                            <div class="col-lg-6">
                                <div class="form-group">
                                    <label class="col-sm-5 col-xs-12 control-label" for="inputEmail3">Order Name<span class="asterisk">*</span></label>
                                    <div class="col-sm-7 col-xs-12">
                                      <input type="text" name="order_name" id="order_name" class="form-control">
                                        <span class="text-danger" id="order_name_error"></span>
                                    </div>
                                 
                                </div>
                            </div>
                            <div class="col-lg-12">
                                <div class="form-group">
                                     <div class="col-sm-12 table-rep-plugin col-lg-12">

                                        <div class="table-responsive b-0 scrollhidden">
                                           <table class="table table-bordered" id="new_recive_products_karigar">
                                              <thead>
                                                  <tr>
                                                    <?php if($dep_id =='11') { ?>
                                                    <th class="col1">Category</th>
                                                    <?php } ?>
                                                    <th class="col1">Product</th>
                                                    <th class="col1">Weight Range</th>
                                                    <?php if($dep_id !='2' && $dep_id!='3' && $dep_id !='6' && $dep_id!='10'):?>
                                                    <th class="col1">Quantity<!-- <span class="asterisk">*</span> --></th>
                                                    <th class="col1">Gross Wt</th>
                                                    <?php else:?>
                                                    <th class="col1">Weight<!-- <span class="asterisk">*</span> --></th>
                                                   <?php endif;?>
                                                    <th class="col1">Net Wt<!-- <span class="asterisk">*</span> --></th>
                                                    <th class="col1">Wastage %</th>
                                                    <th class="col1">Melting</th>
                                                    <th class="col1">Pure</th>
                                                    <?php if($dep_id !='2'&& $dep_id!='3' && $dep_id !='6' && $dep_id!='10'){?>
                                                    <th class="col1">Stone Wt</th>
                                                    <?php }?>
                                                    <th class="col1">Amount</th>
                                                    <!-- <?php  if($tagging_status=='' || $tagging_status =='0' ){
                                                      $action_name='Send To Tagging';
                                                    }else{
                                                      $action_name='Ready To Product';
                                                    }
                                                      ?>
                                                    <th class="col1"><div class="checkbox checkbox-purpal"><input value="0" type="checkbox" id="check_all" onclick="add_selected_hallmark(this)"><label for="check_all"></label><?= $action_name;?></div></th>
                                           -->
                                                    <th class="col1"></th>
                                                 </tr>
                                              </thead>
                                              <tbody id="product_details">
                                                <tr class="tr_clone">
                                                 <?php if($dep_id =='11') { ?>
                                                    <th class="col1">
                                                                                      

                                                        <select style="width: 140px" name="category_id[]" class="form-control category_id">
                                                        <option value="">Select Category</option>
                                                                    <?php
                                                                    foreach ($category as $c_key => $c_val) { ?>
                                                                        <option value="<?= $c_val['id'] ?>"><?= $c_val['name'] ?></option>
                                                                    <?php }?>
                                                                </select>

                                                    </th>
                                                    <?php }
                                                      ?>
                                                          <th class="col1">
                                                      <input type="hidden" name="dep_id" value="<?=$dep_id?>">
                                                       <input type="hidden" name="tagging_status" value="0">                                          

                                                        <select style="width: 140px" name="product_name[]" class="form-control product_code item_cat create_code">
                                                        <option value="">Select Product</option>
                                                                    <?php
                                                                    foreach ($parent_category as $p_key => $p_val) { ?>
                                                                        <option value="<?= $p_val['id'] ?>"><?= $p_val['name'] ?></option>
                                                                    <?php }?>
                                                                </select>

                                                    </th>
                                                    <th class="col1">
                                                     

                                                            <select class="form-control sel_department" name="weight_range_id[]" >
                                                                <option value=''>Select Weight</option>
                                                                        <?php
                                                                      if(!empty($weight_range)){
                                                                        foreach ($weight_range as $key => $value) {
                                                                          ?>
                                                                          <option value="<?=$value['id']?>"><?=$value['from_weight']."-".$value['to_weight']?></option>
                                                                          <?php
                                                                        }
                                                                      }
                                                                  ?>
                                                             </select>
                                                        
                                                    </th>
                                                    <th class="col1">
                                                       <?php if($dep_id !='2' && $dep_id!='3' && $dep_id !='6' && $dep_id!='10'){ ?>
                                                        <input type="text" class="form-control" name="quantity[]" value="">
                                                        <?php }else{ ?>
                                                         <input type="text"  class="form-control gr_wt" name="weight[]"  onkeyup="copy_mfg_gr_wt_recive_pro(this)">
                                                         <?php }?>
                                                        <p class="text-danger" id="quantity_error"></p>
                                                    </th>
                                                       <?php if($dep_id !='2' && $dep_id!='3' && $dep_id !='6' && $dep_id!='10'){ ?>
                                                    <th class="col1">
                                                        <input type="text"  class="form-control gr_wt" name="gross_wt[]" onkeyup="copy_mfg_gr_wt_recive_pro(this)">                                                        
                                                        <p class="text-danger" id="gross_wt_error_2"></p>
                                                    </th>
                                                    <?php }?>
                                                    <th class="col1">
                                                        <input type="text" onchange="calculate_pure_new_recive_pro(this)" class="form-control net_wt" id="net_wt0" name="net_wt[]">
                                                        <p class="text-danger" id="net_wt_error_2"></p>
                                                    </th>
                                                    <th class="col1">
                                                        <input id="wastage" onchange="calculate_pure_new_recive_pro(this)" type="text" class="form-control wastage" name="wastage[]" readonly="">
                                                        <p class="text-danger" id="wastage_error_2"></p>
                                                    </th>
                                                    <th class="col1">
                                                        <input onchange="calculate_pure_new_recive_pro(this)" id="melting" type="text" class="form-control melting" name="melting[]" value="92">
                                                        <p class="text-danger" id="melting_error_2"></p>
                                                    </th>
                                                    <th class="col1">
                                                        <input id="pure" type="text" class="form-control pure" name="pure[]" readonly="">
                                                    </th>
                                                    <?php if($dep_id !='2' && $dep_id!='3' && $dep_id !='6' && $dep_id!='10'){?>
                                                    <th class="col1">
                                                        <input type="text" class="form-control" name="stone_wt[]">
                                                        <p class="text-danger" id="stone_wt_error_2"></p>
                                                    </th>
                                                    <!-- <th class="col1">
                                                        <input type="text" class="form-control" name="few_wt[]">
                                                        <p class="text-danger" id="few_wt_error_2"></p>
                                                    </th> -->
                                                    <?php }?>
                                                    <th class="col1">
                                                    <input type="hidden" value="1" class="form-control shortlist_all is_skip_qc" id="is_skip_qc[]" name="is_skip_qc[]" >
                                                        <input type="text" class="form-control" name="amount[]">
                                                        <p class="text-danger" id="amount_error_2"></p>
                                                    </th>
                                                   
                                                    <th>
                                                        <input type="button" name="add" value="Add" class="btn btn-sm btn-success  btn-sm tr_clone_recive_pro">                         
                                                        <button class="btn btn-danger waves-effect waves-light  btn-sm" onclick="remove_tr(this)" name="commit" type="button">REMOVE</button>
                                                    </th>
                                                </tr>
                                            </tbody>
                                           </table>
                                           
                                           
                                           <span class="text-danger" id="qnty_error"></span>
                                           </div>
                                        </div>                                             
                                </div>
                             </div>
                          
                 
                             
    
                            
                               <div class="div_error col-sm-offset-5 col-sm-8"></div> 
                                                             <div class="col-lg-12">
                                   <div class="col-lg-12 sticky-btn" id="button_save">
                                           <div class="col-sm-8 texalin btnSection"> 
                                                <a href="<?= ADMIN_PATH?>Received_orders"><button class="btn btn-default waves-effect waves-light btn-md pull-left sticky-back" name="commit" type="button">Back</button></a>
                                              <button class="btn btn-success waves-effect waves-light btn-md pull-right" id="save_print" name="commit" type="button"  onclick="validate_print_karigar_from_new_prod_recive('store')"> Save and Print</button>&nbsp;&nbsp;
                                                
                                            </div>
                                   </div>
                                </div>
                                                                    
                  </form>
               </div>
               <div class="clearfix"></div>
               </div>
            </div>
         </div>
      </div>
   </div>
</div>  
</div>