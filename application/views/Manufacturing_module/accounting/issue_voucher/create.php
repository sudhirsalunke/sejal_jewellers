<div class="content-page">
<div class="content">
   <div class="container">
      <div class="row">
         <div class="col-sm-12 card-box-wrap">
            <div class="panel panel-default">
              <div class="panel-heading">
                <h4 class="inlineBlock"><span><?=$display_title?></span></h4>                
              </div>
              <div class="panel-body">
                <div class="col-lg-12">
                  <form name="issue_voucher_form" id="issue_voucher_form" role="form" class="form-horizontal" method="post">
                     <div class="form-group">
                        <label class="col-sm-2 control-label" for="inputEmail3"> Voucher No :</label>
                        <div class="col-sm-4">
                          <label><?=$voucher_no;?></label>
                        </div>
                     </div>
                     <div class="form-group">
                        <label class="col-sm-2 control-label" for="inputEmail3"> Date  </label>
                        <div class="col-sm-4">
                          <label><?=date('d-m-Y')?></label>
                        </div>
                     </div>
                     <div class="form-group">
                        <label class="col-sm-2 control-label" for="inputEmail3"> Party Type <span class="asterisk">*</span> </label>
                        <div class="col-sm-4 date">
                         <!--  <input type="text" name="voucher[party_name]" class="form-control " placeholder="Enter Party Name"> -->
                         <select class="form-control" name="voucher[party_type]" id="party_type">
                           <option value="">Select Party Type</option>
                           <?php foreach ($party_type as $key => $value) { ?>
                            <option value="<?=$value['id']?>"><?=$value['name']?></option>
                           <?php } ?>
                         </select>
                             <span class="text-danger" id="party_type_error"></span>

                        </div>
                     </div>
                      <div class="form-group">
                        <label class="col-sm-2 control-label" for="inputEmail3"> Party Name <span class="asterisk">*</span> </label>
                        <div class="col-sm-4 date">
                         <select class="form-control" name="voucher[party_name]" id="party_name_list">
                           <option value="">Select Party Name</option>
                         </select>
                             <span class="text-danger" id="party_name_error"></span>

                        </div>
                     </div>
                     <div class="form-group">
                      <div class="table-responsive">
                        <table id="issue_voucher_table" class="table">
                          <thead>
                            <tr>
                              <th>Buying Complexity <span class="asterisk">*</span></th>
                              <th>Gross Weight<span class="asterisk">*</span></th>
                              <th>Net Weight<span class="asterisk">*</span></th>
                              <th>Stone<span class="asterisk">*</span></th>
                              <th>Melting<span class="asterisk">*</span></th>
                              <th>Touch<span class="asterisk">*</span></th>
                              <th>Purity<span class="asterisk">*</span></th>
                              <th>amount<span class="asterisk">*</span></th>

                              <th></th>
                            </tr>
                          </thead> 
                              <tbody>
                                <tr id="tr0">
                                  <td> <input type="hidden" class="cnt_val" name="voucher_details[cnt][]" value="0" >
                                     <select class="form-control" name="voucher_details[buying_complexity_id][]">
                                        <option value="" id="category_0" >Select Buying Complexity</option>
                                        <div id="cat_html">
                                        <?php foreach ($buying_complexity as $c_key => $c_value) { ?>
                                            <option value="<?=$c_value['id']?>"><?=$c_value['name']?></option>
                                       <?php } ?>
                                     </div>
                                      </select>
                                      <span class="text-danger buying_complexity_error" id="buying_complexity_id_0_error"></span>
                                  </td>

                                  <td>
                                     <input type="text" placeholder="Enter Gross Weight"  class="form-control"  name="voucher_details[gr_wt][]" onkeyup="calculation_stone_weight(this)">
                                      <span class="text-danger gross_wt_error" id="gr_wt_0_error"></span>
                                  </td>
                                   <td>
                                     <input type="text" placeholder="Enter Net Weight"  class="form-control"  name="voucher_details[net_wt][]" onkeyup="calculation_stone_weight(this)" onchange="calculation_purity_weight(this)">
                                      <span class="text-danger net_wt_error" id="net_wt_0_error"></span>
                                  </td>
                                  
                                  <td>
                                    <input type="text" placeholder="Enter Stone" id="stone_weight"  class="form-control qty"  name="voucher_details[stone][]">
                                    <span class="text-danger stone_weight_error" id="stone_0_error"></span>
                                  </td>
                                   <td>
                                      <input list="melting_list" type="text" placeholder="Select Or Enter Melting"  class="form-control qty"  name="voucher_details[melting][]" onchange="calculation_purity_weight(this)">
                                      <datalist id="melting_list">
                                        <option value="92">
                                        <option value="75">
                                      </datalist>
                                    <span class="text-danger melting_error" id="melting_0_error"></span>
                                  </td>
                                  <td>
                                    <input type="text" placeholder="Enter Touch" class="form-control qty"  name="voucher_details[touch][]" onchange="calculation_purity_weight(this)">
                                    <span class="text-danger touch_error" id="touch_0_error"></span>
                                  </td>

                                  <td>
                                    <input type="text" placeholder="Enter Purity" class="form-control qty"  name="voucher_details[purity][]">
                                    <span class="text-danger purity_error" id="purity_0_error"></span>
                                  </td>

                                  <td>
                                    <input type="text" placeholder="Enter Amount" class="form-control qty"  name="voucher_details[amount][]">
                                    <span class="text-danger amount_error" id="amount_0_error"></span>
                                  </td>
                                   
                                   <td><a id="add_more_voucher_details" class="btn more-btn btn-sm waves-effect waves-light m-l-5" href="javascript:void(0)" title="Add More">Add More</a></td>
                                </tr>
                           </tbody>
                        </table>   
                      </div>
                     </div>                   

                  <div class="form-group btnSection">  
                     
                      <button type="button" onclick="window.location='<?= ADMIN_PATH?>Manufacture_accounting/Issue_voucher'" class="btn btn-default waves-effect waves-light">BACK</button>
                       <button type="button" class="btn btn-md btn-success waves-effect waves-light pull-right" name="commit" type="button" onclick="save_issue_voucher('store')">Submit</button>
                    </div>




                  <div class="col-sm-12">
                    <div class="col-lg-offset-4 errors col-lg-6">
                    </div>
                  </div>  
                  </form>
               </div>
              </div>
               <div class="clearfix"></div>
            </div>
         </div>
      </div>
   </div>
</div>
