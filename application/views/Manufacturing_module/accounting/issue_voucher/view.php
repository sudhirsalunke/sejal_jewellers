<div class="content-page">
<div class="content">
   <div class="container" id="print_page">
      <div class="row">
         <div class="col-sm-12 card-box-wrap">
            <div class="panel panel-default">
              <div class="panel-heading">
               <h4 class="inlineBlock"><span><?=$display_title?></span></h4>                
              </div>
              <div class="panel-body">
                <div class="col-lg-12">
                  <form name="issue_voucher_form" id="issue_voucher_form" role="form" class="form-horizontal" method="post">
                     <div class="form-group">
                        <label class="col-sm-2 control-label" for="inputEmail3"> Voucher No :</label>
                        <div class="col-sm-4">
                          <label><?=$voucher['id']?></label>
                        </div>
                     </div>
                     <div class="form-group">
                        <label class="col-sm-2 control-label" for="inputEmail3"> Date  :</label>
                        <div class="col-sm-4">
                          <label><?=date('d-m-Y',strtotime($voucher['voucher_date']))?></label>
                        </div>
                     </div>
                     <div class="form-group">
                        <label class="col-sm-2 control-label" for="inputEmail3"> Party Type  :</label>
                        <div class="col-sm-4 date">
                         <label><?=$voucher['type_name']?></label>
                        </div>
                     </div>
                     <div class="form-group">
                        <label class="col-sm-2 control-label" for="inputEmail3"> Party Name  :</label>
                        <div class="col-sm-4 date">
                         <label><?=$voucher['party_name']?></label>
                        </div>
                     </div>
                     <div class="form-group">
                        <div class="table-responsive">
                          <table class="table">
                          <thead>
                            <tr>
                              <th>Buying Complexity </th>
                              <th>Gross Weight</th>
                              <th>Net Weight</th>
                              <th>Stone</th>
                              <th>Melting</th>
                              <th>Touch</th>
                              <th>Purity</th>
                              <th>amount</th>
<!-- 
                              <th></th> -->
                            </tr>
                          </thead> 
                              <tbody>
                                <?php 
                                $total_gr_wt = $total_net_wt = $total_stone = $total_melting = $total_touch = $total_purity = $total_amt =0.00;
                                foreach ($voucher_details as $key => $value) { 
                                    $total_gr_wt += $value['gr_wt'];
                                    $total_net_wt += $value['net_wt'];
                                    $total_stone += $value['stone']; 
                                    $total_melting += $value['melting'];
                                    $total_touch += $value['touch']; 
                                    $total_purity += $value['purity'];
                                    $total_amt += $value['amount'];
                                  ?>
                                    
                                <tr id="tr0">
                                  <td> 
                                    <label><?=$value['bc_name']?></label>
                                  </td>

                                  <td>
                                     <label style="float:right"><?=$value['gr_wt']?></label>
                                  </td>
                                   <td>
                                     <label style="float:right"><?=$value['net_wt']?></label>
                                  </td>
                                  
                                  <td>
                                   <label style="float:right"><?=$value['stone']?></label>
                                  </td>
                                   <td>
                                     <label style="float:right"><?=$value['melting']?></label>
                                  </td>
                                  <td>
                                    <label style="float:right"><?=$value['touch']?></label>
                                  </td>

                                  <td>
                                    <label style="float:right"><?=$value['purity']?></label>
                                  </td>

                                  <td>
                                    <label style="float:right"><?=$value['amount']?></label>
                                  </td>
                                   
                                </tr>
                                 <?php  } ?>
                                 <tr>
                                  <td> 
                                    <label>TOTAL</label>
                                  </td>

                                  <td>
                                     <label style="float:right"><?=$total_gr_wt?></label>
                                  </td>
                                   <td>
                                     <label style="float:right" ><?=$total_net_wt?></label>
                                  </td>
                                  
                                  <td>
                                   <label style="float:right" ><?=$total_stone?></label>
                                  </td>
                                   <td>
                                     <label style="float:right" ><?=$total_melting?></label>
                                  </td>
                                  <td>
                                    <label style="float:right" ><?=$total_touch?></label>
                                  </td>

                                  <td>
                                    <label style="float:right" ><?=$total_purity?></label>
                                  </td>

                                  <td>
                                    <label style="float:right" ><?=$total_amt?></label>
                                  </td>
                                   
                                </tr>
                           </tbody>
                        </table> 
                        </div>  
                     </div>
                    <div class="form-group btnSection"> 
                     <button class="btn btn-default waves-effect waves-light m-l-5  btn-md" onclick="window.location='<?= ADMIN_PATH?>Manufacture_accounting/Issue_voucher'" type="reset">
                               BACK
                          </button>                    
                         <button id="Receive_Product_btn" class="btn btn-info waves-effect waves-light btn-md pull-right" name="commit" type="button" onclick="print_issue_voucher()">
                         PRINT
                         </button>                                           
                    </div>


                                   
                  <div class="col-sm-12">
                    <div class="col-lg-offset-4 errors col-lg-6">
                    </div>
                  </div>  
                  </form>
                </div>
              </div>               
               <div class="clearfix"></div>
            </div>
         </div>
      </div>
   </div>
</div>
