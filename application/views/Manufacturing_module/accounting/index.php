<div class="content-page">
  <div class="content">
    <div class="container">
      <div class="row">
        <div class="col-sm-12 card-box-wrap">
          <div class="card-box padmobile dash-board">
           <h5 class=" m-b-20"><!--header-title business_manager_header_title-->
            <span>DASHBOARD</span>
            </h5> 
            <h4 class="header-title business_manager_header_title m-b-30 bmsmheader bmsmheaderabm"><span>Issue Vouchers</span></h4>

            <div class="row">              
              <div class="col-md-12 col-xs-12 section-3 product-box marg_top-bottom no_pad">
                <div class="col-lg-3 col-md-3 col-sm-6">
                  <a href="<?= ADMIN_PATH.'Manufacture_accounting/Issue_voucher'?>">
                     <div class="card-box dash-box box-shadow darkBlue_box dash_box_content text-center" data-aos="zoom-in" data-aos-easing="linear" data-aos-delay="800" data-aos-duration="800">
                    
                    <div class="widget-chart-1 white_color_font">
                      
                      <h4 class="">Receipt - Issue Voucher</h4>
                        <h2 class="number-size light-blue-font"><?=$vouchers?></h2>
                        <span class="label label-gray">Vouchers</span>
                     
                    </div>
                  </div></a>
                </div><!-- end col -->
                 <div class="col-lg-3 col-md-3 col-sm-6">
                  <a href="<?= ADMIN_PATH.'Manufacture_accounting/Metal_issue' ?>">
                     <div class="card-box dash-box box-shadow red_box dash_box_content text-center" data-aos="zoom-in" data-aos-easing="linear" data-aos-delay="800" data-aos-duration="800">
                   
                    <div class="widget-chart-1 white_color_font">
                      
                       <h4 class="">Metal Issue Voucher</h4>
                        <h2 class="number-size light-blue-font"><?=$metal;?></h2>
                        <span class="label label-gray">Vouchers</span>
                      </div>
                   
                  </div>
                </a>
              </div>
               
                 <div class="col-lg-3 col-md-3 col-sm-6">
                  <a href="<?= ADMIN_PATH.'Manufacture_accounting/Cash_bank_issue' ?>">
                   <div class="card-box dash-box box-shadow purple_box dash_box_content text-center" data-aos="zoom-in" data-aos-easing="linear" data-aos-delay="800" data-aos-duration="800">
                    
                    <div class="widget-chart-1 white_color_font">
                      
                      <h4 class="">Cash-Bank Issue Receipts</h4>
                        <h2 class="number-size light-blue-font"><?=$cash_bank?> </h2>
                        <span class="label label-gray">Receipts</span>
                      </div>
                    
                  </div>
                </a>
              </div>

              <div class="col-lg-3 col-md-3 col-sm-6">
                  <a href="<?= ADMIN_PATH.'Manufacturing_engaged_karigar' ?>">
                  <div class="card-box dash-box box-shadow yellow_box dash_box_content text-center" data-aos="zoom-in" data-aos-easing="linear" data-aos-delay="800" data-aos-duration="800">
                    
                    <div class="widget-chart-1 white_color_font">
                      
                      <h4 class="">Multiple Cash-Bank Issue Receipts</h4>                        
                        <h2 class="number-size light-blue-font"></h2>
                        <span class="label label-gray">Comming Soon..</span>
                      
                    </div>
                  </div>
                </a>
              </div>
              
              </div>
            </div>
          </div>
        </div>
      </div><!-- end row -->
    </div><!-- end container -->
  </div><!-- end content -->
</div><!-- end content-page -->