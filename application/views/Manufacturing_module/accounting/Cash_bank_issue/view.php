<div class="content-page">
<div class="content">
   <div class="container" id="print_page">
      <div class="row">
         <div class="col-sm-12 card-box-wrap">
            <div class="panel panel-default">
              <div class="panel-heading">
                <h4 class="inlineBlock"><span><?=$display_title?></span></h4>
              </div>
              <div class="panel-body">
                <div class="col-lg-12">                
                      <div class="form-group">
                        <label class="col-sm-4 control-label" for="inputEmail3"> Date  :</label>
                        <div class="col-sm-8">
                          <label><?=date('d-m-Y',strtotime($Receipt['created_at']))?></label>
                        </div>
                      </div>
                      <div class="form-group">
                        <label class="col-sm-4 control-label" for="inputEmail3"> Party Type  :</label>
                        <div class="col-sm-8 date">
                         <label><?=$Receipt['type_name']?></label>
                        </div>
                      </div>
                      <div class="form-group">
                        <label class="col-sm-4 control-label" for="inputEmail3"> Party Name  :</label>
                        <div class="col-sm-8 date">
                         <label><?=$Receipt['party_name']?></label>
                        </div>
                      </div>
                      <div class="form-group">
                        <label class="col-sm-4 control-label" for="inputEmail3"> Bank Name  :</label>
                        <div class="col-sm-8">
                         <label><?=$Receipt['bank_name']?></label>
                        </div>
                      </div>
                      <div class="form-group">
                        <label class="col-sm-4 control-label" for="inputEmail3"> Amount  :</label>
                        <div class="col-sm-8">
                         <label><?=$Receipt['amount']?></label>
                        </div>
                      </div>
                      
                      <div class="form-group btnSection">
                      <button class="btn btn-default waves-effect waves-light m-l-5  btn-md" onclick="window.location='<?= ADMIN_PATH?>Manufacture_accounting/Cash_bank_issue'" type="reset">
                           BACK
                           </button>
                      <button id="Receive_Product_btn" class="btn btn-info waves-effect waves-light btn-md pull-right" name="commit" type="button" onclick="print_issue_voucher()">
                         PRINT
                         </button>
                    </div>


                      <div class="col-sm-12">
                        <div class="col-lg-offset-4 errors col-lg-6">
                        </div>
                      </div> 
                  </div>
              </div>    
              <div class="clearfix"></div>
            </div>
         </div>
      </div>
   </div>
</div>
