<div class="content-page">
<div class="content">
   <div class="container">
      <div class="row">
         <div class="col-sm-12 card-box-wrap">
            <div class="panel panel-default">
              <div class="panel-heading">
               <h4 class="inlineBlock"><span><?=$display_title?></span></h4>                
              </div>
              <div class="panel-body">
                <div class="col-lg-12">
                  <form name="issue_voucher_form" id="issue_voucher_form" role="form" class="form-horizontal" method="post">
                     
                     <div class="form-group">
                        <label class="col-sm-2 col-xs-2 control-label" for="inputEmail3"> Date  </label>
                        <div class="col-sm-4">
                          <label><?=date('d-m-Y')?></label>
                        </div>
                     </div>
                    <div class="form-group">
                        <label class="col-sm-2 col-xs-2 control-label" for="inputEmail3"> Party Type <span class="asterisk">*</span> </label>
                        <div class="col-sm-4 date">
                         <!--  <input type="text" name="voucher[party_name]" class="form-control " placeholder="Enter Party Name"> -->
                         <select class="form-control" name="cash_bank[party_type]" id="party_type">
                           <option value="">Select Party Type</option>
                           <?php foreach ($party_type as $key => $value) { ?>
                            <option value="<?=$value['id']?>"><?=$value['name']?></option>
                           <?php } ?>
                         </select>
                             <span class="text-danger" id="party_type_error"></span>

                        </div>
                     </div>
                      <div class="form-group">
                        <label class="col-sm-2 col-xs-2 control-label" for="inputEmail3"> Party Name <span class="asterisk">*</span> </label>
                        <div class="col-sm-4 date">
                         <select class="form-control" name="cash_bank[party_name]" id="party_name_list">
                           <option value="">Select Party Name</option>
                         </select>
                             <span class="text-danger" id="party_name_error"></span>

                        </div>
                     </div>
                       <div class="form-group">
                        <label class="col-sm-2 col-xs-2 control-label" for="inputEmail3"> Bank Name <span class="asterisk">*</span> </label>
                        <div class="col-sm-4 date">
                          <input type="text" name="cash_bank[bank_name]" class="form-control " placeholder="Enter Bank Name">
                             <span class="text-danger" id="bank_name_error"></span>

                        </div>
                     </div>
                       <div class="form-group">
                        <label class="col-sm-2 col-xs-2 control-label" for="inputEmail3"> Amount <span class="asterisk">*</span> </label>
                        <div class="col-sm-4 date">
                          <input type="text" name="cash_bank[amount]" class="form-control " placeholder="Enter Amount">
                             <span class="text-danger" id="amount_error"></span>

                        </div>
                     </div>
                    

                    <div class="form-group btnSection">
                      <button class="btn btn-default waves-effect waves-light m-l-5  btn-md" onclick="window.location='<?= ADMIN_PATH?>Manufacture_accounting/Cash_bank_issue'" type="reset">Back
                      </button>
                      <button id="Receive_Product_btn" class="btn btn-success waves-effect waves-light  btn-md pull-right" name="commit" type="button" onclick="save_cash_bank_voucher('store')">
                      Submit</button>
                    </div>


                  <div class="col-sm-12">
                    <div class="col-lg-offset-4 errors col-lg-6">
                    </div>
                  </div>  
                  </form>
               </div>
              </div>               
               <div class="clearfix"></div>
            </div>
         </div>
      </div>
   </div>
</div>
