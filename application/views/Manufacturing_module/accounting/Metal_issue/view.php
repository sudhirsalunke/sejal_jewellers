<div class="content-page">
<div class="content">
   <div class="container" id="print_page">
      <div class="row">
         <div class="col-sm-12 card-box-wrap">
            <div class="panel panel-default">
              <div class="panel-heading">
                <h4 class="inlineBlock"><span><?=$display_title?></span></h4>                
              </div>
              <div class="panel-body">
                <div class="col-lg-12">
                    <form name="issue_voucher_form" id="issue_voucher_form" role="form" class="form-horizontal" method="post">
                       <div class="form-group">
                          <label class="col-sm-2 col-xs-2 control-label" for="inputEmail3"> Voucher No :</label>
                          <div class="col-sm-4 col-xs-6">
                            <label><?=$voucher['id']?></label>
                          </div>
                       </div>
                       <div class="form-group">
                          <label class="col-sm-2 col-xs-2 control-label" for="inputEmail3"> Date  :</label>
                          <div class="col-sm-4 col-xs-6">
                            <label><?=date('d-m-Y',strtotime($voucher['voucher_date']))?></label>
                          </div>
                       </div>
                       <div class="form-group">
                          <label class="col-sm-2 col-xs-2 control-label" for="inputEmail3"> Party Type  :</label>
                          <div class="col-sm-4 date col-xs-6">
                           <label><?=$voucher['type_name']?></label>
                          </div>
                       </div>
                       <div class="form-group">
                          <label class="col-sm-2 col-xs-2 control-label" for="inputEmail3"> Party Name  :</label>
                          <div class="col-sm-4 date col-xs-6">
                           <label><?=$voucher['party_name']?></label>
                          </div>
                       </div>
                       <div class="form-group">
                         <div class="table-responsive">
                            <table class="table">
                            <thead>
                              <tr>
                                <th>Item</th>
                                <th>Weight</th>
                                <th>Melting</th>
                                <th>Purity</th>
                              </tr>
                            </thead> 
                                <tbody>
                                  <?php 
                                  $tot_weight=$tot_melting=$tot_purity=0.00;
                                  foreach ($voucher_details as $key => $value) { 
                                  $tot_weight +=$value['weight'];
                                  $tot_melting +=$value['melting'];
                                  $tot_purity +=$value['purity'];?>
                                   
                                  <tr id="tr0">
                                    <td> 
                                      <label><?=$value['bc_name']?></label>
                                    </td>

                                 <td>
                                     <label style="float:right"><?=$value['weight']?></label>
                                  </td>
                                   <td>
                                     <label style="float:right" ><?=$value['melting']?></label>
                                  </td>
                                  
                                  <td>
                                   <label style="float:right"><?=$value['purity']?></label>
                                  </td>
                                  
                                </tr>
                                 <?php  } ?>
                                   <tr>
                                  <td> 
                                    <label>TOTAL</label>
                                  </td>

                                  <td>
                                     <label style="float:right"><?=$tot_weight?></label>
                                  </td>
                                   <td>
                                     <label style="float:right"><?=$tot_melting?></label>
                                  </td>
                                  
                                  <td>
                                   <label style="float:right"><?=$tot_purity?></label>
                                  </td>
                                  
                                </tr>
                           </tbody>
                        </table>
                       </div>   
                     </div>                   

                    <div class="form-group btnSection"> 
                     <button class="btn btn-default waves-effect waves-light m-l-5  btn-md" onclick="window.location='<?= ADMIN_PATH?>Manufacture_accounting/Metal_issue'" type="reset">
                             BACK
                             </button>                   
                         <button id="Receive_Product_btn" class="btn btn-info waves-effect waves-light btn-md pull-right" name="commit" type="button" onclick="print_issue_voucher()">
                         PRINT
                         </button>                                          
                    </div>


                    <div class="col-sm-12">
                      <div class="col-lg-offset-4 errors col-lg-6">
                      </div>
                    </div>  
                    </form>
                 </div>
              </div>                 
               <div class="clearfix"></div>
            </div>
         </div>
      </div>
   </div>
</div>
