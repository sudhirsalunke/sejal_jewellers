<div class="content-page">
<div class="content">
   <div class="container">
      <div class="row">
         <div class="col-sm-12 card-box-wrap">
            <div class="panel panel-default">
              <div class="panel-heading">
                <h4 class="inlineBlock"><span><?=$display_title?></span></h4>                
              </div>
              <div class="panel-body">
                <div class="col-lg-12">
                  <form name="issue_voucher_form" id="metal_issue_voucher_form" role="form" class="form-horizontal" method="post">
                     <div class="form-group">
                        <label class="col-sm-2 control-label" for="inputEmail3"> Voucher No :</label>
                        <div class="col-sm-4">
                          <label><?=$voucher_no;?></label>
                        </div>
                     </div>
                     <div class="form-group">
                        <label class="col-sm-2 control-label" for="inputEmail3"> Date  </label>
                        <div class="col-sm-4">
                          <label><?=date('d-m-Y')?></label>
                        </div>
                     </div>
                     <div class="form-group">
                        <label class="col-sm-2 control-label" for="inputEmail3"> Party Type <span class="asterisk">*</span> </label>
                        <div class="col-sm-4 date">
                         <select class="form-control" name="metal_issue[party_type]" id="party_type">
                           <option value="">Select Party Type</option>
                           <?php foreach ($party_type as $key => $value) { ?>
                            <option value="<?=$value['id']?>"><?=$value['name']?></option>
                           <?php } ?>
                         </select>
                             <span class="text-danger" id="party_type_error"></span>

                        </div>
                     </div>
                      <div class="form-group">
                        <label class="col-sm-2 control-label" for="inputEmail3"> Party Name <span class="asterisk">*</span> </label>
                        <div class="col-sm-4 date">
                         <select class="form-control" name="metal_issue[party_name]" id="party_name_list">
                           <option value="">Select Party Name</option>
                         </select>
                             <span class="text-danger" id="party_name_error"></span>

                        </div>
                     </div>
                     <div class="form-group">
                        <div class="table-responsive">
                          <table id="metal_issue_voucher_table" class="table">
                          <thead>
                            <tr>
                              <th>Item<span class="asterisk">*</span></th>
                              <th>Weight<span class="asterisk">*</span></th>
                              <th>Melting<span class="asterisk">*</span></th>
                              <th>Purity<span class="asterisk">*</span></th>
                              <th></th>
                            </tr>
                          </thead> 
                              <tbody>
                                <tr id="tr0">
                                  <td> <input type="hidden" class="cnt_val" name="metal_issue_details[cnt][]" value="0" >
                                     <select class="form-control" name="metal_issue_details[material_id][]">
                                        <option value="" id="category_0" >Select Item</option>
                                        <div id="cat_html">
                                        <?php foreach ($material_master as $c_key => $c_value) { ?>
                                            <option value="<?=$c_value['id']?>"><?=$c_value['short_code']?></option>
                                       <?php } ?>
                                     </div>
                                      </select>
                                      <span class="text-danger material_error" id="material_id_0_error"></span>
                                  </td>
                                   <td>
                                     <input type="text" placeholder="Enter Weight"  class="form-control"  name="metal_issue_details[weight][]" onkeyup="calculation_metal_purity(this)">
                                      <span class="text-danger weight_error" id="weight_0_error"></span>
                                  </td>
                                   <td>
                                      <input  type="text" placeholder="Enter Melting"  class="form-control"  name="metal_issue_details[melting][]" onkeyup="calculation_metal_purity(this)">
                                     
                                    <span class="text-danger melting_error" id="melting_0_error"></span>
                                  </td>

                                  <td>
                                    <input type="text purity_error" placeholder="Enter Purity" class="form-control qty"  name="metal_issue_details[purity][]">
                                    <span class="text-danger purity_error" id="purity_0_error"></span>
                                  </td>

                                
                                   <td><a id="add_more_metal_issue" class="btn more-btn btn-sm waves-effect waves-light m-l-5" href="javascript:void(0)" title="Add More">Add More</a></td>
                                </tr>
                           </tbody>
                        </table>  
                        </div> 
                     </div>                   

                        <div class="form-group btnSection"> 
                           
                           <button class="btn btn-default waves-effect waves-light m-l-5  btn-md" onclick="window.location='<?= ADMIN_PATH?>Manufacture_accounting/Metal_issue'" type="reset">
                                 Back
                                 </button>
                            <button id="Receive_Product_btn" class="btn btn-success waves-effect waves-light btn-md pull-right" name="commit" type="button" onclick="save_metal_issue_voucher('store')">
                               Submit
                               </button>
                        </div>


                  <div class="col-sm-12">
                    <div class="col-lg-offset-4 errors col-lg-6">
                    </div>
                  </div>  
                  </form>
               </div>
              </div>
               <div class="clearfix"></div>
            </div>
         </div>
      </div>
   </div>
</div>
