<div class="content-page">
<div class="content">
   <div class="container">
      <div class="row">
         <div class="col-sm-12 card-box-wrap">
            <div class="panel panel-default">
              <div class="panel-heading">
               <h4 class="inlineBlock"><span><?= $display_title;?></span></h4>
                <div class="pull-right m-t-5">
                  <a href="<?= ADMIN_PATH.'Manufacture_accounting/Metal_issue/create'?>">
                    <button type="button" class="add_senior_manager_button btn btn-primary waves-effect w-md waves-light  btn-md">Create Metal Issue Voucher </button>
                </a>
                </div>
              </div>
              <div class="panel-body">
                <div class="table-rep-plugin">
                  <div class="table-responsive b-0 scrollhidden">

               <table class="table table-bordered custdatatable" id="metal_issue_voucher">
                <thead>
                        <?php
                          $data['heading']='metal_issue_voucher';
                          $this->load->view('master/table_header',$data);
                        ?>
                    </thead>
                 <!--  <thead>
                     <tr>
                         <th class="col4">#</th>
                         <th class="col4">Voucher No</th>
                         <th class="col4">Date</th>
                         <th class="col4">Party Name</th>
                        <th class="col3"></th>
                     </tr>
                  </thead> -->
               </table>
               </div>
               </div>

            </div>
         </div>
      </div>
   </div>
</div>