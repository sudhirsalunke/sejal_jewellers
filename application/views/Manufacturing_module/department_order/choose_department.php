<div class="content-page">      
    <div class="content">
        <div class="container">
            <div class="row">
                <div class="col-sm-12 card-box-wrap">
                    <div class="card-box padmobile clearfix">
                        <h4 class="header-title business_manager_header_title m-t-0 m-b-30 bmsmheader bmsmheaderabm"><span><?= $page_title; ?></span></h4>
                        <form name="category" id="department_order" role="form" class="form-horizontal" method="post">
                            <div class="col-lg-12">
                                <div class="form-group">
                                    <label class="col-sm-2 control-label" for="inputEmail3"> Select Department <span class="asterisk">*</span></label>
                                    <div class="col-sm-8">
                                        <select class="form-control choose_department" name="department_order[sub_category_id]">
                                            <option value=''>Select Department</option>
                                            <?php if (!empty($departments)) { ?>
                                                <?php foreach ($departments as $s_key => $s_value) { ?>
                                                    <option value="<?= $s_value['id'] ?>"><?= $s_value['name'] ?></option>
                                                <?php } ?>
                                            <?php } ?>
                                        </select>
                                        <span class="text-danger" id="sub_category_id_error"></span>
                                    </div>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>