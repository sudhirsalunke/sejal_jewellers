<div class="content-page">
<div class="content">
   <div class="container">
      <div class="row">
         <div class="col-sm-12 card-box-wrap">
            <div class="panel panel-default">
              <div class="panel-body">
                <div class="col-lg-12">       
                  <form name="category" action="<?=ADMIN_PATH.'Karigar_order_list/print_karigar_order'?>"  id="assign_kamgar_order" role="form" class="form-horizontal" method="post" target="_blank">
                   <input type="hidden" name="dep_id" id="dep_id" class="dep_id" value="<?= $dep_id?>">
                    <div class="form-group">
                      <label  class="col-sm-3 control-label"> Select Karigar </label>
                      <div class="col-sm-4">
                        <select class="form-control" name="karigar_id">
                        <option value="">Please select Karigar</option>
                          <?php foreach($karigar as $key => $value){?>
                            <option value="<?php echo $value['id'] ?>"> <?php echo $value['name'] ?> </option>
                          <?php } ?>
                        </select>
                        <span class="text-danger" id="karigar_id_error"></span>
                      </div>
                    </div> 
                    <div class="form-group">
                        <label class="col-lg-3 col-sm-3 control-label" for="inputEmail3">Order id: </label>
                        <div class="col-lg-1 col-sm-3"><?php echo $order_details[0]['manufacturing_order_id'];?>
                        </div>
                     
                        <label class="col-lg-2 col-sm-3 control-label" for="inputEmail3">Order Name: </label>
                        <div class="col-sm-1"><?php echo $order_details[0]['order_name'];?>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-lg-3 col-sm-3 control-label" for="inputEmail3">Assign Date: </label>
                        <div class="col-lg-1 col-sm-3"><?php echo date('d-m-Y');?>
                        </div>
                    
                        <label class="col-lg-2 col-sm-3 control-label" for="inputEmail3">Delivery Date: </label>
                        <div class="col-lg-2 col-sm-3"><?php echo date('d-m-Y',strtotime("+12 day"));?>
                        </div>
                    </div>
                    <div class="table-rep-plugin col-lg-12">

                 	<div class="table-responsive b-0 scrollhidden">                  
        
					<table class="table table-bordered" id="assign_k">
			                        <thead>
			                           <tr>
			                              <!-- <th class="col4">Order_id</th> -->
			                                    <th class="col4">Product</th>
                                            <?php if($dep_id =='2' || $dep_id =='3' || $dep_id =='6' || $dep_id =='10') { ?>
                                            <th class="col4">Weight</th>
                                    
                                            <?php }else if($dep_id =='8'){ ?>
                                            <th class="col3">Weight</th>
                                            <th class="col3">Quantity</th>
                                          
                                            <?php }else{ ?>
                                            <th class="col3">Quantity</th>
                                            <th class="col4">Avg Wt</th>
                          
                                            <?php } ?>
                                          <th class="col4">Weight Range</th>
                                          <th class="col4">Image</th>
                                          <th class="col4"></th>
			                           </tr>
			                        </thead>
			                        <tbody>
			                        	<?php
                                              $total_gr_wt = 0;
                                              $total_qty = 0;
                                              $total_wt = 0;
                                              //print_r($order_details);
                                  foreach ($order_details as $key1 => $value1) {                                  
                                    ?>
                                  <tr>
                                    <!-- <td class="col4" ><?php echo $value1['manufacturing_order_id']; ?></td> -->
                                    <td class="col4" ><?php echo $value1['name'];?></td>
                                    <?php if($dep_id !='2' && $dep_id !='3' && $dep_id 
                                      !='6' && $dep_id !='10'): 
                                        $total_gr_wt += ((($value1['from_weight'] + $value1['to_weight'])/2) * ($value1['quantity'] - $value1['assign_quantity']));
                                        $total_qty += ($value1['quantity'] - $value1['assign_quantity'])
                                    ?>
                                    <td class="col4" >
                                      <input type="hidden" name="mop_id[]" value="<?=$value1['id']?>">
                                      <input type="hidden" name="department_id[]" value="<?=$value1['department_id']?>">
                                      <input type="hidden" id="total_qnt_<?=$key1+1?>" value="<?php echo $value1['quantity'] - $value1['assign_quantity'];?>">
                                      <input type="hidden" name="approx_weight[]" value="<?=($value1['from_weight'] + $value1['to_weight'])/2?>">
                                      <input type="text" class="form-control" id="entered_qnt_<?=$key1+1?>" name="quantity[]" value="<?php echo $value1['quantity'] - $value1['assign_quantity'];?>" >
                                      <p class="text-danger" id="quantity_<?=$key1?>_error"></p>
                                    </td>
                                    <td class="col4" ><?php echo '<span style="float:right">'.(($value1['from_weight'] + $value1['to_weight'])/2) * ($value1['quantity'] - $value1['assign_quantity']).'</span>'?></td>
                                    <?php else:
                                    $total_wt += ($value1['weight'] - $value1['total_weight'])
                                    ?>
                                    <td class="col4" >
                                      <input type="hidden" name="mop_id[]" value="<?=$value1['id']?>">
                                      <input type="hidden" name="department_id[]" value="<?=$value1['department_id']?>">
                                      <input type="hidden" id="total_wt_<?=$key1+1?>" value="<?php echo $value1['weight'] - $value1['total_weight'];?>">
                            
                                      <input type="text" class="form-control" id="entered_wt_<?=$key1+1?>" name="weight[]" value="<?php echo $value1['weight'] - $value1['total_weight'];?>" >
                                      <p class="text-danger" id="weight_<?=$key1?>_error"></p>
                                    </td> 
                                     <?php endif;?>   
                            
                                    <td class="col4" ><?php echo '<span style="float:right">'.$value1['from_weight'] .'-'. $value1['to_weight'].'</span>'?></td>
                                    <td class="col4" >
                                    <?php if(!empty($value1['path'])){?>
                                        <a href="<?= UPLOAD_IMAGES.'parent_category/'.$value1['name'].'/small/'.$value1['path']?>" target="blank"><img src="<?= UPLOAD_IMAGES.'parent_category/'.$value1['name'].'/small/'.$value1['path']?>" class="img-thumbnail img-check"></a>
                                        <?php }?>
                                    </td>
                                    <td><a><button class="btn btn-danger small loader-hide  btn-sm" name="commit" type="button" onclick="javascript:$(this).parents('tr').remove()">Remove</button></a></td>
                                 </tr>
                                <?php } ?>
                                 <tr>
                                    <!-- <th class="col4">Order_id</th> -->
                                    <td><b>Total</b></td>
                                    <?php if($dep_id !='2' && $dep_id !='3' && $dep_id !='6' && $dep_id !='10'): ?>
                                    <td><b style="float:right"><?=$total_qty;?></b></td>
                                    <td><b style="float:right"><?= $total_gr_wt;?></b></td>
                                    <?php else:?>
                                    <td><b style="float:right"><?= $total_wt;?></b></td>
                                    <?php endif;?> 
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                 </tr>
			                        </tbody>
			                     </table>
                             </div>
                  </div>
	            <div class="form-group">
                     <div class="col-sm-offset-4 col-sm-8 texalin">

                         <?php                  
                           foreach ($order_details as $key => $value) {
                              foreach($value as $k=>$v)
                              {
                       if(!empty($v['path']))
                                   echo '<a href="'.UPLOAD_IMAGES.'parent_category/'.$v['name'].'/small/'.$v['path'].'" target="blank"><img src="'.UPLOAD_IMAGES.'parent_category/'.$v['name'].'/small/'.$v['path'].'" class="img-thumbnail img-check"></a>';
                              }
                            } 
                        ?>
                      </div>
                    </div>                 
 

                    <div class="form-group btnSection">                       
         
                         <button class="btn btn-default waves-effect waves-light m-l-5  btn-md" onclick="window.location='<?= ADMIN_PATH?>Manufacturing_department_order/view/<?php echo $order_details[0]['manufacturing_order_id'];?>'" type="reset">
                           Back
                        </button>
                          <button id="engage_karigar" class="btn btn-success waves-effect waves-light btn-md pull-right" type="button" >Save</button>
                      </div>



                  </form>
                </div>
              </div>			          
                <div class="clearfix"></div>
            </div>
         </div>
      </div>
   </div>
</div>
<style type="text/css">
  .table img {
    max-height: none;
}
</style>
