
<div class="content-page">
<div class="content">
   <div class="container">
      <div class="row">
         <div class="col-sm-12 card-box-wrap">
            <div class="panel panel-default" id="department_view_order">
              <div class="panel-heading">
               <h4 class="inlineBlock"><span><?= $display_title;?></span>
               </h4>
               <!-- <button class="btn btn-primary small loader-hide  btn-sm pull-right" name="commit" type="button" onclick="create_karigar()">Create Karigar Order</button> -->
               <div class="pull-right single-assign btn-wrap2">
               <a type="button" class="add_senior_manager_button btn btn-primary small loader-hide btn-md pull-right single-assign" onclick="assign_to_karigar_manuf()">Assign to Karigar</a>
               </div>

               <!-- <button class="btn btn-primary small loader-hide  btn-md pull-right" type="button" onclick="assign_to_karigar_manuf()">Assign to Karigar</button> -->

                <!--<a href="<?=ADMIN_PATH?>Department"><button  type="button" class="add_senior_manager_button btn btn-purp  waves-effect w-md waves-light m-b-5  btn-md">Create New Order </button></a>-->
               </div>
               <div class="panel-body">
               <div class="table-rep-plugin">
                  <div class=" b-0 scrollhidden table">
              <form action="<?=ADMIN_PATH.'Manufacturing_department_order/show'?>" method="post" id="manuf_karigar_form">
              <?php if($dep_id == '2' || $dep_id =='3' || $dep_id =='6' || $dep_id =='10'){ $table_name='department_wise_view'; }elseif($dep_id == '8'){$table_name='department_mangalsutra_view';}else{ $table_name='department_all_view';}?>
               <div class="table-responsive">
                 <table class="table table-bordered custdatatable display select " id="<?php echo $table_name;?>">
                  <thead>
                       <?php
                          $data['heading']=$table_name;
                          $this->load->view('master/table_header',$data);
                        ?>
          <!--            <tr>
                        <th>
                          <div class="checkbox checkbox-purpal">
                                               <input class="select_all" value="0" type="checkbox" id="check_all">
                            <label for="check_all"></label>
                          </div>
                          
                        </th>
                         <th class="col4">#</th>
                         <th class="col4">Order Date</th>
                         <th class="col4">Parent Category</th>
                         <th class="col4">Weight Range</th>
                         <th class="col4">Gross Wt</th>
                         <th class="col4">Total Quantity</th>
                         <th class="col4">Assign Quantity</th>
                         <th class="col4">Assign Weight</th>                        
                         <th class="col4">Status</th>
                         <th class="col4">Image</th>
                        <th class="col3">Action</th>
                     </tr> -->
                  </thead>
               </table>
               </div>
               </div>
               </form>
               </div>
               </div>
            </div>
         </div>
      </div>
   </div>
</div>