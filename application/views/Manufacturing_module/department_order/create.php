<div class="content-page">      
    <div class="content">
        <div class="container">
            <div class="row">
                <div class="col-sm-12 card-box-wrap">
                    <div class="panel panel-default clearfix">
                        <div class="panel-heading frmHeading">
                            <h4 class="inlineBlock"><span><?= $page_title; ?></span></h4>
                        </div>
                        <div class="panel-body">
                            <form name="category" id="department_order" role="form" class="form-horizontal" method="post">
                            <input type="hidden" name="dep_id" id="dep_id" class="dep_id" value="<?= $dep_id?>">
                            <div class="col-lg-12">
                                <div class="form-group">
                                    <label class="col-sm-2 control-label" for="inputEmail3">Select Department<span class="asterisk">*</span></label>
                                    <div class="col-sm-2">
                                        <select class="form-control sel_department" name="department_order[department_id]">

                                            <!-- <option value=''>Select department</option> -->
                                            <?php  if (!empty($departments)) { ?>
                                                <?php foreach ($departments as $s_key => $d_value) { ?>
                                                    <option value="<?= $d_value['id'] ?>" <?= ($department_id == $d_value['id']) ? 'selected="selected"' : '' ?>>
                                                        <?= $d_value['name'] ?>
                                                    </option>
                                                <?php } ?>
                                            <?php } else{ ?>

                                            <input type="hidden" class="" name="department_order[department_id]" value="<?= $department_id ?>">
                                            <?php }?>
                                        </select>
                                        <span class="text-danger" id="department_id_error"></span>
                                    </div>

                                    <label class="col-sm-2 control-label" for="inputEmail3"> Enter Order Name<span class="asterisk">*</span></label>
                                    <div class="col-sm-2">
                                        <input type="text" placeholder="" id="order_name" class="form-control order_name" name="department_order[order_name]" value="<?= @$manufacturing_order['order_name']?>" <?=(!empty($manufacturing_order['order_name'])) ? 'readonly':''?>>
                                        <span class="text-danger" id="order_name_error"></span>
                                    </div>

                                    <label class="col-sm-2 control-label" for="inputEmail3"> Select Order Date <span class="asterisk">*</span></label>
                                    <div class="col-sm-2">
                                        <input type="text" placeholder="Enter Order Date" id="category" class="form-control datepickerInput order_date" name="department_order[order_date]" value="<?= date('d-m-Y') ?>">
                                        <span class="text-danger" id="order_date_error"></span>
                                    </div>
                                    
                                                
                                    
                                    <input type="hidden" id="order_id" name="department_order[order_id]" value="<?= @$order_id ?>">
                                </div>
                            </div>
                            <div class="col-lg-12">
                                <div class="form-group">
                                    <label class="col-sm-2 control-label" for="inputEmail3"> Select Product<span class="asterisk">*</span></label>
                                    <div class="col-sm-2">
                                        <!-- <select class="form-control sub_category" name=department_order[sub_category_id] onchange="get_sub_cat_images(this.value)"> -->
                                        <select onchange="get_sub_cat_images(this.value)" class="form-control sub_category" name=department_order[sub_category_id]>
                                            <option value=''>Select Product</option>
                                            <?php
                                            if (!empty($parent_category)) {
                                                foreach ($parent_category as $s_key => $s_value) {
                                                    ?>
                                                    <option value="<?= $s_value['id'] ?>"><?= $s_value['name'] ?></option>
                                                    <?php
                                                }
                                            }
                                            ?>
                                        </select>
                                        <span class="text-danger" id="sub_category_id_error"></span>
                                    </div>
                                    <div id="product_variation" style="display: none">
                                      <label class="col-sm-2 control-label" for="inputEmail3"> Select Variants</label> 
                                      <div class="col-sm-2">
                                          <select name="variant[]" class="form-control variant_select 3col" id="product_variation_sel" multiple>
                                          </select>
                                        </div>
                                    </div>
                           
                                    <label class="col-sm-2 control-label" for="inputEmail3"> Select Weight Range<span class="asterisk">*</span></label>
                                    <div class="col-sm-2">
                                        <select class="form-control weight_range" name=department_order[weight_range_id]>
                                            <option value=''>Select Weight Range</option>
                                            <?php
                                            if (!empty($weight_range)) {
                                                foreach ($weight_range as $w_key => $w_value) {
                                                    ?>
                                                    <option value="<?= $w_value['id'] ?>"><?= $w_value['from_weight'] . ' - ' . $w_value['to_weight'] ?></option>
                                                    <?php
                                                }
                                            }
                                            ?>
                                        </select>
                                        <span class="text-danger" id="weight_range_id_error"></span>
                                    </div>
<!-- 
                                    <?php if($dep_id =='2' || $dep_id =='8'){ 
                                             $display_name ='weight';           
                                            }else{ 
                                                $display_name ='quantity'; 
                                            } ?> -->
                                      <!--          <label class="col-sm-2 control-label" for="inputEmail3"> Enter <?= $display_name;?><span class="asterisk">*</span></label>
                                    <div class="col-sm-1">
                                        <input type="text" min="0" placeholder="" id="category" class="form-control <?= $display_name?>" name="department_order[<?= $display_name?>]">
                                        <span class="text-danger" id="<?= $display_name;?>_error"></span>
                                    </div> -->
                                 <?php if($dep_id =='2' || $dep_id =='3'  || $dep_id =='6' || $dep_id =='10'){  
                                             $display_qty ='block'; 
                                             $display_wt ='none';          
                                            }else if($dep_id =='8'){ 
                                                $display_wt ='block'; 
                                                $display_qty ='block';
                                            }else{
                                                $display_wt ='block'; 
                                                $display_qty ='none';
                                            }
                                    ?> 
                                <div style="display: <?=$display_qty ?>">
                                    <label class="col-sm-2 control-label" for="inputEmail3"> Enter weight<span class="asterisk">*</span></label>
                                    <div class="col-sm-1">
                                        <input type="text" min="0" placeholder="" id="category" class="form-control weight" name="department_order[weight]">
                                        <span class="text-danger" id="weight_error"></span>
                                    </div>
                                </div>
                    
                                <div style="display: <?=$display_wt?>">
                                        <label class="col-sm-2 control-label" for="inputEmail3"> Enter Quantity<span class="asterisk">*</span></label>
                                        <div class="col-sm-1">
                                            <input type="text" min="0" placeholder="" id="category" class="form-control quantity" name="department_order[quantity]">
                                            <span class="text-danger" id="quantity_error"></span>
                                        </div>
                               </div>
                                    <div class="col-sm-1 texalin">
                                        <button class="btn more-btn waves-effect waves-light  btn-md" name="commit" type="button" onclick="add_order();">
                                            ADD
                                        </button>
                                    </div>
                                </div>
                                <div class="form-group">
                                </div>
                                <label class="col-sm-7 control-label" for="inputEmail3"> <span class="text-danger" id="common_error"></span></label>
                            </div>
                            <div class="row" style="margin-left:15%;">
                                <div class="form-group" id="sub_image_div">    
                                              
                                </div>

                            </div>                   
                        </form>
                        
                        <h4 class="header-title business_manager_header_title m-t-0 m-b-30 bmsmheader bmsmheaderabm"><span>Added Products</span></h4>
                        <!-- <a onclick="show_preview()" id="order_preview" class="header-title business_manager_header_title m-t-0 m-b-30 bmsmheader bmsmheaderabm" style="display:none;margin-right: 5px;float:right;cursor:pointer;color:#4a59b4">Preview</a> -->
                        <div class="table-rep-plugin">
                            <div class="table-responsive b-0 scrollhidden">
                                <table class="table table-bordered custdatatable" id="department_order_table">
                                    <thead>
                                        <tr>
                                            <th class="col4">Product</th>
                                            <th class="col4">Variants</th>
                                            <th class="col4">Weight Range</th>
                                            <?php if($dep_id =='2' || $dep_id =='3'  || $dep_id =='6' || $dep_id =='10') { ?>
                                            <th class="col4">Weight</th>
                                            <th class="col3"></th>
                                            <?php }else if($dep_id =='8'){ ?>
                                            <th class="col3">Weight</th>
                                            <th class="col3">Quantity</th>
                                            <th class="col4">approx Wt</th>
                                            <th class="col3"></th>
                                            <?php }else{ ?>
                                            <th class="col3">Quantity</th>
                                            <th class="col4">approx Wt</th>
                                            <th class="col3"></th>
                                            <?php } ?>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <?php 
                                        // $total_w = 0;
                                        // $total_q = 0;
                                        //print_r($mapping_data);die;
                                        if (!empty($mapping_data)) : ?>
                                            <?php
                                             foreach ($mapping_data as $key => $value): 
                                                // $total_q += $total_q + $value['quantity'];
                                                    $avg_weight = ($value['from_weight']+$value['to_weight'])/2;
                                                    $appx_wt = $avg_weight * $value['quantity'];
                                                ?>
                                                <tr id="id_<?= $value['tr_count'] ?>">
                                                    <td><?= $value['name'] ?></td>
                                                    <td><?= $value['p_variant'] ?></td>
                                                    <td ><span style="float:right"><?= $value['from_weight'] ?>-<?= $value['to_weight'] ?></span></td>

                                                    <?php if($dep_id =='2'|| $dep_id =='3' || $dep_id =='6' || $dep_id =='10') { ?>
                                 
                                                    <td><span style="float:right"><?= $value['weight'] ?></span></td>
                                                   
                                                    <?php }else if($dep_id =='8'){ ?>
                                                    <td><span style="float:right"><?= $value['weight'] ?></span></td>
                                                    <td><span style="float:right"><?= $value['quantity'] ?></span></td>
                                                    <td><span style="float:right"><?= $appx_wt ?></span></td>
                                                   
                                                    <?php }else{ ?>
                                                    <td><span style="float:right"><?= $value['quantity'] ?></span></td>
                                                   <td><span style="float:right"><?= $appx_wt ?></span></td>
                                                    <?php } ?>




                                                <!--     <td><span style="float:right"><?= $value[$display_name] ?></span></td>
                                                    <?php if($dep_id !='2' || $dep_id !='8') : ?>
                                                    <td><span style="float:right"><?=$appx_wt?></span></td>
                                                    <?php endif; ?> -->


                                                    <?php if (empty($value['assign_quantity'])) : ?>
                                                        <td><button onclick="remove_existing_order(<?= $value['tr_count'] ?>)" type="button" name="commit" class="btn btn-danger small loader-hide  btn-sm">REMOVE</button></td>
                                                    <?php else: ?>
                                                        <td>This order is assigned to karigar</td>
                                                    <?php endif; ?>
                                                </tr>
                                            <?php endforeach; ?>
                                        <?php else: ?>
                                            <tr>
                                                <td colspan="5">No Product found</td>
                                            </tr>
                                        <?php endif; ?> 
                                        
                                    </tbody>

                                </table>
                               <!--  <table class="table table-bordered custdatatable">
                                    <tbody>
                                        <tr style="display:none;" class="order_params">
                                            <td>Total</td>
                                            <td>-</td>
                                            <td><span id="total_qty" >Total Weight:</span></td>
                                            <td><span id="total_weight" >Total Weight:</span></td>
                                            <td></td>
                                        </tr>
                                    </tbody>
                                </table> -->
                        
                            </div>
                        </div>
                        <div class="col-lg-12" id="button_save" style="display: none">
                            <div class="btnCenter">                                
                                    <button class="btn btn-success waves-effect waves-light  btn-md" id="save_order" name="commit" type="button" onclick="save_order();">Submit</button>
                            </div>
                        </div>
                        </div>
                    </div>
                    <div class="clearfix"></div>
                </div>
            </div>
        </div>
    </div>
</div>