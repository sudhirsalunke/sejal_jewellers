<div class="content-page">
<div class="content">
   <div class="container">
      <div class="row">
         <div class="col-sm-12 card-box-wrap">
            <div class="panel panel-default">
              <div class="panel-heading">
               <h4 class="inlineBlock"><span><?= $display_title;?></span></h4>
              <div class="pull-right single-add btn-wrap3">
               <a href="<?= ADMIN_PATH .'Manufacturing_department_order/create'?>" type="button" class="add_senior_manager_button btn btn-purp waves-effect w-md waves-light btn-md pull-right single-add">Create New Order</a>
               </div>
            </div>
               <div class="panel-body m-t-5">
                  

               <div class="clearfix"></div>
               <div class="table-rep-plugin">
                  <div class="b-0 scrollhidden table-responsive">
               <table class="table table-bordered custdatatable" id="department_order">
                  <thead>
                    <?php
                      $data['heading']='department_order';
                      $this->load->view('master/table_header',$data);
                    ?>
                     <!-- <tr>
                         <th class="col4">Order ID</th>
                         <th class="col4">Order Name</th>
                         <th class="col4">Order Date</th>
                         <th class="col4">Department Name</th>
                         <th class="col4"></th>
                        <th class="col3"></th>
                     </tr> -->
                  </thead>
               </table>
               </div>
               </div>

              </div>
            

         </div>
      </div>
   </div>
</div>