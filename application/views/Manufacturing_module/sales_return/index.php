<div class="content-page">
<div class="content">
   <div class="container">
      <div class="row">
         <div class="col-sm-12 card-box-wrap">
            <div class="panel panel-default">
              <div class="panel-heading">
                <h4 class="inlineBlock"><span><?= $display_name;?></span></h4>
                <div class="pull-right single-add btn-wrap3">
                
                  <a onclick="send_to_manufacture_all()" ><button  type="button" class="add_senior_manager_button btn btn-danger waves-effect w-md waves-light m-b-5  btn-md pull-right single-add">Reject</button></a>
           
                  <button onclick="Transfer_to_sales_return_voucher()" type="button" class="add_senior_manager_button btn btn-purp  waves-effect w-md waves-light btn-md pull-right single-add">Transfer To sale</button>
                </div>
              </div>
              <div class="panel-body">                
               <div class="clearfix"></div>
               <div class="table-rep-plugin">
                  <div class="table-responsive b-0 scrollhidden">
               
              <form enctype='multipart/form-data' role="form" name="sales_product_form" id="sales_product_form" >
              
              <?php     
                  if($department_id == '0'  || $department_id == '1' || $department_id == '11' ||  $department_id == '5' ||  $department_id == '7'  ||  $department_id == '8'){                        
                                   $table_col_name="sales_voucher_return_product";
                              }else{
                                 $table_col_name="mfg_ready_product_dept_wise";
                              } 
              ?>
               <table class="table table-bordered custdatatable stripe row-border order-column dataTable no-footer" id="<?php echo $table_col_name;?>">
                 <thead>
                  <?php
                          $data['heading']=$table_col_name;
                          $this->load->view('master/table_header',$data);
                        ?>
                  
                  </thead>
               </table>
              </form>
               </div>
               </div>
              </div>
            </div>
         </div>
      </div>
   </div>
</div>
