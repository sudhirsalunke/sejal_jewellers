<div class="content-page">
<div class="content">
   <div class="container">
      <div class="row">
         <div class="col-sm-6 card-box-wrap">
            <div class="panel panel-default">
              <div class="panel-heading frmHeading">
                <h4 class="inlineBlock"><span><?= $page_title;?></span></h4>                
              </div>
              <div class="panel-body">
                <div class="col-lg-12">
                  <form name="few_box" id="few_box_data" role="form" class="form-horizontal" method="post">
                  <input type="hidden" id="few_box[id]" class="form-control" name="few_box[id]" value="<?=$few_box['id']?>">
                   <div class="form-group">
                        <label class="col-sm-4 control-label" for="inputEmail3"> Select Product <span class="asterisk">*</span></label>
                        <div class="col-sm-8">
                          <select class="form-control" name="few_box[product]" disabled>
                          <option value=''>Select Product</option>
                          <?php
                            if(!empty($product_list)){
                              foreach ($product_list as $key => $value) {
                                ?>
                                <option value="<?=$value['id']?>"  <?= ($few_box['product_id'] == $value['id']) ? 'selected' : ''?>><?=$value['name']?></option>
                                <?php
                              }
                            }
                          ?>
                          </select>
                          <span class="text-danger" id="product_error"></span>
                        </div>
                     </div>
                     <div class="form-group">
                        <label class="col-sm-4 control-label" for="inputEmail3"> Enter Weight <span class="asterisk">*</span></label>
                        <div class="col-sm-8">
                           <input type="text" placeholder="Enter Few Weight" id="few_box[few_weight]" class="form-control" name="few_box[few_weight]" value="<?=$few_box['weight']?>">
                           <span class="text-danger" id="few_weight_error"></span>
                        </div>
                     </div>
         
                    <div class="btnSection">                    
                       <button class="btn btn-default waves-effect waves-light m-l-5 btn-md pull-left" onclick="window.location='<?= ADMIN_PATH?>Few_box'" type="reset">BACK</button>

                       <button class="btn btn-success waves-effect waves-light btn-md pull-right few_box_btn" name="commit" type="button" onclick="save_few_weight('update'); ">SAVE</button>                     
                    </div>
                  </form>
                </div>
              </div>
               <div class="clearfix"></div>
            </div>
         </div>
      </div>
   </div>
</div>