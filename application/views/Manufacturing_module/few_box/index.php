<div class="content-page">
<div class="content">
   <div class="container">
      <div class="row">
         <div class="col-sm-12 card-box-wrap">
            <div class="panel panel-default">
              <div class="panel-heading">
               <h4 class="inlineBlock">Few Weight Box</h4>
                <a href="<?= ADMIN_PATH?>Few_box/create" class="pull-right single-receive btn-wrap m-t-5"><button  type="button" class="add_senior_manager_button btn btn-primary waves-effect w-md waves-light m-b-5 btn-md single-receive">ADD Few Weight</button></a> 
              </div>
              <div class="panel-body">
                <div class="clearfix"></div>
                <div class="table-rep-plugin">
                  <div class="table-responsive b-0 scrollhidden">
                    <table class="table table-bordered custdatatable" id="few_box">
                      <thead>
                       <?php
                          $data['heading']='few_box';
                          $this->load->view('master/table_header',$data);
                        ?>
                         <!-- <tr>
                             <th class="col4">Sub Category Name</th>
                             <th class="col4">Category Name</th>
                             <th class="col4">Code</th>
                            <th class="col3"></th>
                         </tr> -->
                      </thead>
                    </table>
                  </div>
                </div>
              </div>              
            </div>
         </div>
      </div>
   </div>
</div>