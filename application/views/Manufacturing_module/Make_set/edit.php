<div class="content-page">      
<div class="content">
   <div class="container">
      <div class="row">
         <div class="col-sm-12 card-box-wrap">
            <div class="card-box padmobile">
               <h4 class="header-title business_manager_header_title m-t-0 m-b-30 bmsmheader bmsmheaderabm"><span><?=$page_title?></span>
               </h4>
               <div class="col-lg-12">
                  <form name="category" id="makeSet" role="form" class="form-horizontal" method="post">
                    <input type="hidden" name="id" value="<?=$id?>">
                    <div class="form-group" id="order_div">
                    <?php 
                    // $html = array("result" => $result, "data" => $data);
                    // $this->load->view('Manufacturing_module/Make_set/html',$html);
                    ?>
                     <?php foreach ($result as $value) { ?> 
                      <div class="col-sm-3" style="border: 1px solid #ccc;margin-top: 10px 10px;padding: 10px 5px;">
                        <div class="col-sm-2">
                          <input type="hidden" name="msr_id[<?=$value['id']?>]" value="<?=@$data[$value['id']]['id']?>">
                          <input type="checkbox" class="form-control pull-right" <?=(isset($data[$value['id']])) ? 'checked' : ''; ?> name="qch_id[<?=$value['id']?>]" value="<?=$value['id']?>">
                        </div>
                        <div class="col-sm-10">
                          <!-- <label class="col-sm-6 control-label " for="inputEmail3">Order ID :</label>
                          <div class="col-sm-6">
                            <label class="control-label" for="inputEmail3"><?=$value['order_id']?></label>
                          </div> -->
                          <label class="col-sm-6 control-label " for="inputEmail3">Sub Category :</label>
                          <div class="col-sm-6">
                            <label class="control-label" for="inputEmail3"><?=$value['sub_category_name']?></label>
                          </div>
                          <label class="col-sm-6 control-label " for="inputEmail3">Quantity :</label>
                          <div class="col-sm-6">
                           <!--  <input type="text" placeholder="Quantity" class="form-control" id="quantity_<?=$value['id']?>" name="quantity[<?=$value['id']?>]" value="<?=(isset($data[$value['id']])) ? $data[$value['id']]['quantity'] : $value['qc_quantity']; ?>" readonly> -->
                            <input type="text" placeholder="Quantity" class="form-control" id="quantity_<?=$value['id']?>" name="quantity[<?=$value['id']?>]" value="<?=$value['quantity']?>"  readonly>
                          </div>
                          <span class="text-danger" id="quantity_<?=$value['id']?>_error"></span>
                        </div>
                        
                      </div>
                      <?php } ?>
                    </div>
                    <div class="form-group">
                     <div class="col-sm-offset-5 col-sm-8 texalin">
                         <button class="btn btn-primary waves-effect waves-light  btn-md" name="commit" type="button" onclick="save_tag_product('update'); ">
                         SAVE
                         </button>
                         <button class="btn btn-default waves-effect waves-light m-l-5  btn-md" onclick="window.location='<?= ADMIN_PATH?>Category'" type="reset">
                           CANCEL
                           </button>
                     </div>
                  </div>
                  </form>
               </div>
               <div class="clearfix"></div>
            </div>
         </div>
      </div>
   </div>
</div>
<script type="text/javascript">
var page_id = 1, isPreviousEventComplete = true, isDataAvailable = true, msr_id='<?=$id?>';
</script>