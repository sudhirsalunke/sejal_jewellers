<div class="content-page">
<div class="content">
   <div class="container">
      <div class="row">
         <div class="col-sm-12 card-box-wrap">
            <div class="card-box padmobile">
               <h4 class="header-title business_manager_header_title m-t-0 m-b-30 bmsmheader bmsmheaderabm"><span><?= $display_name;?></span>
               </h4>
               <a href="<?=ADMIN_PATH.'Make_set/create'?>"><button  type="button" class="add_senior_manager_button btn btn-purp  waves-effect w-md waves-light m-b-5  btn-md">CREATE SET </button></a>
               <div class="clearfix"></div>
               <div class="table-rep-plugin">
                  <div class="table-responsive b-0 scrollhidden">

              <form enctype='multipart/form-data' role="form" name="product_form" id="export" >
               <table class="table table-bordered custdatatable stripe row-border order-column dataTable no-footer" id="manufacturing_make_set">
                 <thead>
                     <tr>
                        <th class="col4">#</th>
                        <th class="col4">Set Id</th>
                        <th class="col4">Date</th>
                        <th class="col3"></th>
                     </tr>
                  </thead>
               </table>
              </form>
               </div>
               </div>
            </div>
         </div>
      </div>
   </div>
</div>