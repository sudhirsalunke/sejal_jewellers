<div class="content-page">
    <div class="content">
        <div class="container">
            <div class="row">
                <div class="col-sm-12 card-box-wrap">
                    <div class="card-box padmobile">
                        <h4 class="header-title business_manager_header_title m-t-0 m-b-30 bmsmheader bmsmheaderabm"><span><?= $display_name; ?></span></h4>
                        <a onclick="Prepare_kundan_karigar_order()"><button  type="button" class="add_senior_manager_button btn btn-purp  waves-effect w-md waves-light m-b-5  btn-md">Prepared Order For Kundan Karigar </button></a>

                        <button onclick="ready_products()" type="button" class="add_senior_manager_button btn btn-purp  waves-effect w-md waves-light m-b-5  btn-md">Product Ready</button>
                        <div class="clearfix"></div>
                        <div class="table-rep-plugin">
                            <div class="table-responsive b-0 scrollhidden">

                                <form enctype='multipart/form-data' class="make_set_tag_products" role="form" name="product_form" id="make_set_tag_products" method="post">
                                    <table class="table table-bordered custdatatable stripe row-border order-column dataTable no-footer" id="manufacturing_make_set" data-id="<?= $id ?>">
                                        <thead>
                                            <tr>
                                                <th class="col4">#</th>
                                                
                                                <th class="col4">Subcategory</th>
                                                <th class="col4">Product Code</th>
                                                <th class="col4">Quantity</th>
                                                <th class="col3"></th>
                                            </tr>
                                        </thead>
                                    </table>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>