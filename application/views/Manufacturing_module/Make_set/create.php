<div class="content-page">      
<div class="content">
   <div class="container">
      <div class="row">
         <div class="col-sm-12 card-box-wrap">
            <div class="card-box padmobile">
                <h4 class="header-title business_manager_header_title m-t-0 m-b-30 bmsmheader bmsmheaderabm"><span><?=$page_title?></span>
                </h4>
                <a href="<?= ADMIN_PATH?>tag_products" style="float: right;"><button  type="button" class="btn btn-primary waves-effect waves-light  btn-md" >Tag Products</button></a>
                <div class="col-lg-12">
                <form action="<?= ADMIN_PATH?>Make_set/create" method="post" id="make_set">
                  <div class="form-group">
                  <label for="inputEmail3" style="text-align: left;" class="col-sm-2 control-label font-600"> Parent Category</label>
                    <div class="col-sm-8">
                    <?php
                      foreach ($get_filters as $g_key => $g_value) {
                        $checked = '';
                        if(!empty($_POST['parent_category'])){
                        foreach ($_POST['parent_category'] as $key => $value) {
                          if($value == $g_value['id']){
                              $checked = 'checked';
                          }
                          }
                        }
                          ?>
                            <div class="checkbox checkbox-purpal col-md-3">
                            <input type="checkbox" data-parsley-id="<?= $g_value['id']?>" data-parsley-mincheck="2" data-parsley-multiple="groups" class="from-control parent_category" value="<?= $g_value['id']?>" name="parent_category[]" <?= $checked?>>
                            <label for="inputEmail3"> <?php echo $g_value['name'];?> </label>
                          </div>
                        <?php
                      }
                    ?>
                    </div>
                  </div>
               
                    
               </div>
               <div class="col-lg-12">
                <?php
                if(isset($result) && !empty($result)){
                ?>
                  <form name="category" id="makeSet" role="form" class="form-horizontal" method="post">
                    <div class="form-group" id="order_div">
                    <?php 
                    // $html = array("result" => $result);
                    // $this->load->view('Manufacturing_module/Make_set/html',$html);

                    ?>
                    <?php foreach ($result as $value) { 
                        if (empty($value['image'])) {
                            $img ="";
                        }else{
                            $img = ADMIN_PATH.$value['image'];
                        }
                    ?>
                      <div class="col-sm-3" style="border: 1px solid #ccc;margin-top: 10px 10px;padding: 10px 5px;">
                        
                       <input type="file" name="prepare[<?=$value['id']?>]" onchange="save_tag_image(<?=$value['id']?>)">
                       <img src='<?=$img?>' id='img_<?=$value['id']?>' style='height:90px;width:150px'>
                       
                        <div class="col-sm-2">
                          <input type="hidden" name="msr_id[<?=$value['id']?>]" value="<?=@$data[$value['id']]['id']?>">
                          <input type="checkbox" class="form-control pull-right" <?=(isset($data[$value['id']])) ? 'checked' : ''; ?> name="qch_id[<?=$value['id']?>]" value="<?=$value['id']?>">
                        </div>
                        <div class="col-sm-10">
                          <!-- <label class="col-sm-6 control-label " for="inputEmail3">Order ID :</label>
                          <div class="col-sm-6">
                            <label class="control-label" for="inputEmail3"><?=$value['order_id']?></label>
                          </div> -->
                          <div class="col-sm-12">
                            <label class="col-sm-9 control-label " for="inputEmail3">Product Code :</label>
                            <div class="col-sm-2">
                              <label class="control-label" for="inputEmail3"><?=$value['product_code'].'-'.$value['id']?></label>
                            </div>
                          </div>
                          <div class="col-sm-12">
                            <label class="col-sm-9 control-label " for="inputEmail3">Parent Category :</label>
                            <div class="col-sm-2">
                              <label class="control-label" for="inputEmail3"><?=$value['sub_category_name']?></label>
                            </div>
                          </div>
                          <div class="col-sm-12">
                            <label class="col-sm-6 control-label " for="inputEmail3">Quantity :</label>
                            <div class="col-sm-6">
                              <!-- <input type="text" placeholder="Quantity" class="form-control" id="quantity_<?=$value['id']?>" name="quantity[<?=$value['id']?>]" value="<?=(isset($data[$value['id']])) ? $data[$value['id']]['quantity'] : $value['qc_quantity']; ?>"  readonly> -->
                              <input type="text" placeholder="Quantity" class="form-control" id="quantity_<?=$value['id']?>" name="quantity[<?=$value['id']?>]" value="<?=$value['quantity']?>"  readonly>
                             
                            </div>
                            <span class="text-danger" id="quantity_<?=$value['id']?>_error"></span>
                            </div>
                        </div>
                        
                      </div>
                      <?php } ?>
                    </div>
                    <div class="form-group">
                     <div class="col-sm-offset-5 col-sm-8 texalin">
                         <button class="btn btn-primary waves-effect waves-light  btn-md" name="commit" type="button" onclick="save_tag_product('store'); ">
                         SAVE
                         </button>
                         <button class="btn btn-default waves-effect waves-light m-l-5  btn-md" onclick="window.location='<?= ADMIN_PATH?>Category'" type="reset">
                           CANCEL
                           </button>
                     </div>
                  </div>
                  </form>
                <?php
                }
                else{
                ?>
                <p class="text-danger">No products found to make set</p>
                <?php
                }
                ?>
               </div>
                </form>
               <div class="clearfix"></div>
            </div>
         </div>
      </div>
   </div>
</div>
<script type="text/javascript">
var page_id = 1, isPreviousEventComplete = true, isDataAvailable = true, msr_id='';
</script>