<div class="content-page">
<div class="content">
   <div class="container">
      <div class="row">
         <div class="col-sm-12 card-box-wrap">
            <div class="card-box padmobile">
               <h4 class="header-title business_manager_header_title m-t-0 m-b-30 bmsmheader bmsmheaderabm"><span><?= @$display_name?></span>
               </h4>
               <div class="col-lg-12">
               <input type="hidden" value="<?= $is_kundan?>" id="is_kundan">
           
                        <form role="form" target="_blank" class="form-horizontal" method="post" action="print_accounting_receipt" id="print_acc_receipt_frm">
                                    
                            <div class="col-lg-12">
                                <div class="form-group">
                                    <label class="col-sm-2 control-label" for="inputEmail3"> Select Karigar<span class="asterisk">*</span></label>
                                    <div class="col-sm-2">
                                        <select class="form-control" name="kariger_id" onchange="get_receive_product_from_mnfg_accounting(this.value)" id="kariger_id">
                                            <option value=''>Select Karigar</option>
                                            <?php 
                                            if(!empty($all_karigars)){
                                            foreach ($all_karigars as $key => $value) {
                                             	foreach ($receive_from_kariger as $rec_value) {                                             	
                                             	    if($rec_value['karigar_id'] == $value['id']){
                                             	    	 ?>
	                                                    <option value="<?= $value['id']?>"><?= $value['name']?></option>
	                                                    <?php
                                             	    }
                                             	}                                                                                    
                                              }                                           
                                            }  
                                            ?>
                                        </select>
                                        <span class="text-danger" id="kariger_id_error"></span>
                                    </div>
                                 
                                </div>
                            </div>
                            <div class="col-lg-12">
                                <div class="form-group">
                                     <div class="col-sm-6 table-rep-plugin col-lg-offset-2 col-lg-6">

                                        <div class="b-0 scrollhidden">
                                           <table class="table table-bordered custdatatable">
                                              <thead>
                                                 <tr>
                                                    <th class="col1">Parent Category</th>
                                                    <th class="col1">Weight Range</th>
                                                    <th class="col1">Quantity<span class="asterisk">*</span></th>
                                                    <th class="col1">Net Weight<span class="asterisk">*</span></th>
                                                    <th class="col1"></th>
                                                 </tr>
                                              </thead>
                                              <tbody id="product_details">
                                                 <tr>
                                                    <th class="col1">No data found..</th>
                                                    <th class="col1"></th>         
                                                    <th class="col1"></th>            
                                                    <th class="col1"></th>
                                                    <th class="col1"></th>                                                  
                                                 </tr>
                                              </tbody>
                                           </table>
                                           
                                           
                                           <span class="text-danger" id="qnty_error"></span>
                                           </div>
                                        </div>                                             
                                </div>
                             </div>
                          
                                  <div class="col-lg-12">
                                   <div class="col-lg-7" id="button_save">
    	                                <div class="form-group">
    	                                    <div class="col-sm-offset-5 col-sm-8 texalin">
    	                                        <button class="btn btn-primary waves-effect waves-light  btn-md" name="commit" type="button"  onclick="print_manufactur_accounting_from()"> Save and Print</button>&nbsp;&nbsp;
    	                                        <a href="<?= ADMIN_PATH?>Mnfg_accounting_received_orders"><button class="btn btn-primary waves-effect waves-light  btn-md" name="commit" type="button">Cancel</button></a>
    	                                    </div>
    	                                </div>
                                   </div>
                                </div>
                                                                  
                  </form>
               </div>
               <div class="clearfix"></div>
            </div>
         </div>
      </div>
   </div>
</div>