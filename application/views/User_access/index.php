<div class="content-page">
<div class="content">
   <div class="container">
      <div class="row">
         <div class="col-sm-12 card-box-wrap">
            <div class="panel panel-default">
               <div class="panel-heading">
                  <h4 class="inlineBlock"><?= strtoupper($page_title);?></h4>
                  <div class="pull-right single-send m-t-5"><a href="<?= ADMIN_PATH?>User/create"><button  type="button" class="add_senior_manager_button btn btn-primary waves-effect w-md waves-light btn-md single-send">ADD USER </button></a></div>
               </div>
               <div class="panel-body">                  
                  <div class="clearfix"></div>
                  <div class="table-rep-plugin">
                     <div class="table-responsive b-0 scrollhidden">
                        <table class="table table-bordered custdatatable" id="user_access">
                           <thead>
                           
                           <?php
                          $data['heading']='user_access';
                          $this->load->view('master/table_header',$data);
                        ?>
                             <!--  <tr>
                                 <th class="col2">Name</th>
                                 <th class="col2">Email</th>
                                 <th class="col3">Access Modules</th>
                                 <th class="col4"></th>
                              </tr> -->
                           </thead>
                        </table>
                     </div>
                  </div>
               </div>                
            </div>
         </div>
      </div>
   </div>
</div>