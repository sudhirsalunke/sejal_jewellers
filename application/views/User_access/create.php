<div class="content-page">
<div class="content">
   <div class="container">
      <div class="row">
         <div class="col-sm-12 card-box-wrap">
            <div class="panel panel-default">
              <div class="panel-heading frmHeading">
                <h4 class="inlineBlock"><span><?= strtoupper($page_title);?></span></h4>
              </div>               
              <div class="panel-body">
                <div class="col-lg-12">
                  <form name="category" id="User_access" role="form" class="form-horizontal userFrm" method="post">
                     <div class="form-group">
                        <label class="col-sm-4 control-label" for="inputEmail3"> First Name <span class="asterisk">*</span></label>
                        <div class="col-sm-4">
                           <input type="text" placeholder="Enter First Name" id="name" class="form-control" name="User_access[first_name]">
                           <span class="text-danger" id="first_name_error"></span>
                        </div>
                     </div>
                     <div class="form-group">
                        <label class="col-sm-4 control-label" for="inputEmail3"> Last Name <span class="asterisk">*</span></label>
                        <div class="col-sm-4">
                           <input type="text" placeholder="Enter Last Name" id="name" class="form-control" name="User_access[last_name]">
                           <span class="text-danger" id="last_name_error"></span>
                        </div>
                     </div>
                     <div class="form-group">
                        <label class="col-sm-4 control-label" for="inputEmail3"> Email <span class="asterisk">*</span></label>
                        <div class="col-sm-4">
                           <input type="text" placeholder="Enter Email" id="name" class="form-control" name="User_access[email]">
                           <span class="text-danger" id="email_error"></span>
                        </div>
                     </div>
                    <div class="form-group">
                        <label class="col-sm-4 control-label" for="inputEmail3"> Password <span class="asterisk">*</span></label>
                        <div class="col-sm-4">
                           <input type="text" placeholder="Enter Password" id="name" class="form-control" name="User_access[password]">
                           <span class="text-danger" id="password_error"></span>
                        </div>
                    </div>
                    <!-- <div class="form-group">
                        <label class="col-sm-4 control-label" for="inputEmail3"> Is manufacuring User <span class="asterisk">*</span></label>
                        <div class="col-sm-2">
                          <input type="Radio" placeholder="Enter Password" id="name" class="form-control" name="User_access[is_manufacturing]">
                          <label class="control-label" for="inputEmail3"> YES<span class="asterisk">*</span></label>
                        </div>
                        <div class="col-sm-2">
                          <input type="Radio" placeholder="Enter Password" id="name" class="form-control" name="User_access[is_manufacturing]" value="1">
                          <label class="control-label" for="inputEmail3"> NO<span class="asterisk">*</span></label>
                        </div>
                    </div> -->
                     <div id="dept_div1" class="form-group">
                      <label class="col-sm-4 control-label" for="inputEmail3"> Department <span class="asterisk">*</span></label>
                      <div class="col-sm-4">
               <input type="hidden" name="User_access[department_id][]"  id="department_id" value="1">
                     <input type="hidden" name="User_access[show_dashboard]"  id="show_dashboard" value="Manufacturing_Dashboard">
                        <select class="form-control" name="User_access[location_id]"  id="location_id">
                           <option value="0">All Departments</option>      
                          <?php 
       
                          foreach($location as $loc) { ?>
                          <option value="<?=$loc['id']?>" class="pull-right"><?=$loc['name']?></option>
                          <?php } ?>
                        </select>
                      </div>
                     </div>
                   <!--  <div  class="form-group">
                      <label class="col-sm-4 control-label" for="inputEmail3"> Dashboard <span class="asterisk"></span></label>
                      <div class="col-sm-4">
                        <?php $dashboar=array(
                                            array ( 
                                              'id' => 'Category',
                                              'name' => 'Master',
                                              ),
                                            array ( 
                                              'id' => 'Dashboard',
                                              'name' => 'Design Catalog',
                                              ),
                                            array ( 
                                              'id' => 'Corporate_Dashboard',
                                              'name' => 'Corporate Module',
                                              ),
                                            array ( 
                                              'id' => 'User',
                                              'name' => 'Users',
                                              ),
                                            array ( 
                                              'id' => 'Manufacturing_Dashboard',
                                              'name' => 'MFG Module',
                                              ),
                                            array ( 
                                              'id' => 'sales_dashboard',
                                              'name' => 'Sales Module',
                                              ),
                                            array ( 
                                              'id' => 'Order_dashboard',
                                              'name' => 'Order Module',
                                              ),
                                            array ( 
                                              'id' => 'Dispatch_dashboard',
                                              'name' => 'Dispatch module',
                                              ),
                                             array ( 
                                              'id' => 'Dashboard',
                                              'name' => 'Manufacture Accounting',
                                              ),
                                            );
                     ?>
                        <select class="form-control" name="User_access[show_dashboard]">
                          <option value="">select Dashboard</option>
                           <?php foreach($dashboar as $dash) { ?>
                          <option value="<?=$dash['id']?>"><?=$dash['name']?></option>
                          <?php } ?>

                        </select>
                      </div>
                     </div> -->
                     <div class="form-group table-responsive">
                 <!--        <label class="col-sm-4 control-label" for="inputEmail3"> Master <span class="asterisk">*</span></label> -->
                        <!-- <div class="col-sm-8"></div> -->
                      <!-- <div class="table-responsive"> -->
                        <?php foreach ($master as $master_id => $val) { ?>
                        <div class="col-sm-4">
                          <table id="<?=($master_id=="5") ? 'manufacture' : ''?>" class="table table-bordered custdatatable">
                            <thead>
                              <tr>
                                <th><input type="checkbox" class="check_all"></th>
                                <th><?=$val[0]['master_display_name']?></th>
                              </tr>
                            </thead>
                            <tbody>
                            <?php foreach ($val as $controller) { ?>
                              <tr>
                                <td><input type="checkbox" value="<?=$controller['id']?>" class="checkbox" name="User_access[controller_id][]"></td>
                                <td><?=$controller['display_name']?></td>
                              </tr>
                            <?php } ?>
                            </tbody>
                          </table>
                        </div>
                        <?php } ?>
                        <span class="text-danger" id="controller_id_error"></span>
                      </div>
                     
                    <div class="btnSection">
                     <!-- <div class="col-sm-offset-5 col-sm-8 texalin"> -->
                        
                         <button class="btn btn-default waves-effect waves-light m-l-5 btn-md" onclick="window.location='<?= ADMIN_PATH?>User'" type="reset">
                           Back
                           </button>

                            <button class="btn btn-success waves-effect waves-light btn-md pull-right" name="commit" type="button" onclick="save_user_access(); ">
                         SAVE
                         </button>
                     <!-- </div> -->
                  </div>
                  </form>
               </div> 
              </div>
               <div class="clearfix"></div>
            </div>
         </div>
      </div>
   </div>
</div>