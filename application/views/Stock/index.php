<div class="content-page">
  <div class="content">
    <div class="container">
      <div class="row">
        <div class="col-sm-12 card-box-wrap">          
          <div class="card-box padmobile">
            <div class="panel panel-default">
              <div class="panel-heading">
                <h4 class="inlineBlock"><span><?=$display_title?></span></h4>                
              </div>
                <?php $status=@$_GET['status'];
              if($status=='stock'){
                 
                $table_col_name='Stock';
                }else{
                
                  $table_col_name='All_stock_products';
                }
                ?>                  
              <div class="clearfix"></div>
              <div class="panel-body">
                <div class="table-rep-plugin">
                  <div class="table-responsive b-0 scrollhidden">
                    <table class="table custdatatable table-bordered " id="<?php echo $table_col_name;?>">
                      <thead>
                        <?php
                              $data['heading']=$table_col_name;
                          $this->load->view('master/table_header',$data)?>
                      </thead>
                    </table>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>