
<div class="content-page">
<div class="content">
   <div class="container">
      <div class="row">
         <div class="col-sm-12 card-box-wrap">
            <div class="panel panel-default">
              <div class="panel-heading frmHeading">
                <h4 class="inlineBlock">Edit Party Master</h4>
              </div>               
               <div class="panel-body">
                <form name="category" id="party_master_form" role="form" class="form-horizontal" method="post">
                     <input type="hidden" id="controller_name" value="Party_master">
                     <input type="hidden" name="Karigar[encrypted_id]" value="<?= $karigar['encrypted_id'] ?>">
                     <input type="hidden" name="Karigar[customer_type_id]" value="<?= $karigar['customer_type_id'] ?>">
                    <?php $columns_array=array(
                                        array( 
                                                "display_name" => 'Party Type',
                                                "placeholder" => '',
                                                "name"=>'customer_type_id',
                                                "type" =>'dropdown',
                                                "dropdown_array"=>$customer_type,
                                                "ex_val"=>$karigar['customer_type_id'],
                                                'disabled'=>true,
                                                "required"=>'*',
                                                "id"=>"party_type",
                                              ), array( 
                                                "display_name" => 'Email',
                                                "placeholder" => 'Enter Email',
                                                "name"=>'email',
                                                "type" =>'text',
                                                //"required"=>'*',
                                                "ex_val"=>$karigar['email'],
                                              ),
                                          array( 
                                                "display_name" => 'Name',
                                                "placeholder" => 'Enter Name',
                                                "name"=>'name',
                                                "type" =>'text',
                                                "ex_val"=>$karigar['name'],
                                                "required"=>'*',
                                              ),
                                            array( 
                                                "display_name" => 'Code',
                                                "placeholder" => 'Enter Name',
                                                "name"=>'code',
                                                "type" =>'text',
                                                "ex_val"=>@$karigar['code'],
                                                "required"=>'*',
                                                "id"=>"karigar_code",

                                              ),
                                             array( 
                                                "display_name" => 'Address',
                                                "placeholder" => 'Enter Address',
                                                "name"=>'address',
                                                "type" =>'text',
                                                "ex_val"=>$karigar['address'],
                                                "required"=>'*',
                                              ),
                                            /*array( 
                                                "display_name" => 'Area',
                                                "placeholder" => 'Enter Area',
                                                "name"=>'area',
                                                "type" =>'text',
                                                "ex_val"=>$karigar['area'],
                                                "required"=>'*',
                                              ),*/
                                             array( 
                                                "display_name" => 'State',
                                                "placeholder" => '',
                                                "name"=>'state_id',
                                                "type" =>'dropdown',
                                                "dropdown_array"=>$state,
                                                "id"=>'state',
                                                "ex_val"=>$karigar['state_id'],
                                                "required"=>'',
                                              ),
                                              array( 
                                                "display_name" => 'City',
                                                "placeholder" => '',
                                                "name"=>'city_id',
                                                "type" =>'dropdown',
                                                "dropdown_array"=>$city,
                                                "id"=>'city',
                                                "ex_val"=>$karigar['city_id'],
                                                "required"=>'',
                                              ),
                                              array( 
                                                "display_name" => 'Contact Person',
                                                "placeholder" => 'Enter Contact Person Name',
                                                "name"=>'contact_person',
                                                "type" =>'text',
                                                "ex_val"=>$karigar['contact_person'],
                                              ),
                                               array( 
                                                "display_name" => 'Company Name',
                                                "placeholder" => 'Enter Company Name',
                                                "name"=>'company_name',
                                                "type" =>'text',
                                                "ex_val"=>@$karigar['company_name'],
                                              ),
                                            array( 
                                                "display_name" => 'Refer By',
                                                "placeholder" => 'Enter Refernce Name',
                                                "name"=>'refer_by',
                                                "type" =>'text',
                                                "ex_val"=>$karigar['refer_by'],
                                              ),
                                             
                                           /*  array( 
                                                "display_name" => 'Hallmarking Center',
                                                "placeholder" => '',
                                                "name"=>'hc_id',
                                                "type" =>'dropdown',
                                                "dropdown_array"=>$hallmarking_center,
                                                "ex_val"=>$karigar['hc_id'],
                                              ),*/
                                              array( 
                                                "display_name" => 'Phone Number',
                                                "placeholder" => 'Enter Phone Number',
                                                "name"=>'phone_no',
                                                "type" =>'text',
                                                "ex_val"=>@$karigar['phone_no'],
                                              ),
                                             array( 
                                                "display_name" => 'Mobile Number',
                                                "placeholder" => 'Enter Mobile Number',
                                                "name"=>'mobile_no',
                                                "type" =>'text',
                                                "ex_val"=>$karigar['mobile_no'],
                                              ),
                                            /*  array( 
                                                "display_name" => 'Limit Amount',
                                                "placeholder" => 'Enter Limit Amount',
                                                "name"=>'limit_amt',
                                                "type" =>'text',
                                                "ex_val"=>$karigar['limit_amt'],
                                              ),*/
                                              array( 
                                                "display_name" => 'Limit Metal',
                                                "placeholder" => 'Enter Limit Metal',
                                                "name"=>'limit_metal',
                                                "type" =>'text',
                                                "ex_val"=>$karigar['limit_metal'],
                                              ),
                                              array( 
                                                "display_name" => 'No Of Days',
                                                "placeholder" => 'Enter No Of days',
                                                "name"=>'days',
                                                "type" =>'text',
                                                "ex_val"=>$karigar['days'],
                                              ),
                                              array( 
                                                "display_name" => 'Sales Under',
                                                "placeholder" => '',
                                                "name"=>'sales_under',
                                                "type" =>'dropdown',
                                                "dropdown_array"=>$sales_under,
                                                "ex_val"=>$karigar['sales_under'],
                                              ),
                                                array( 
                                                "display_name" => 'Terms',
                                                "placeholder" => 'Enter Terms',
                                                "name"=>'terms',
                                                "type" =>'text',
                                                "ex_val"=>$karigar['terms'],
                                              ),
                                              array( 
                                                "display_name" => 'Bank A/c No',
                                                "placeholder" => 'Enter Bank A/c No',
                                                "name"=>'bank_ac',
                                                "type" =>'text',
                                                "ex_val"=>$karigar['bank_ac'],
                                              ),
                                              array( 
                                                "display_name" => 'IFSC No',
                                                "placeholder" => 'Enter IFSC No',
                                                "name"=>'bank_ifsc',
                                                "type" =>'text',
                                                "ex_val"=>$karigar['bank_ifsc'],
                                              ),
                                              array( 
                                                "display_name" => 'Bank Name',
                                                "placeholder" => 'Enter Bank Name',
                                                "name"=>'bank_name',
                                                "type" =>'text',
                                                "ex_val"=>$karigar['bank_name'],
                                              ),
                                              array( 
                                                "display_name" => 'Bank Branch',
                                                "placeholder" => 'Enter Bank Branch',
                                                "name"=>'bank_branch',
                                                "type" =>'text',
                                                "ex_val"=>$karigar['bank_branch'],
                                              ),
                                        ); ?>

                      <?php foreach ($columns_array as $c_key => $c_value) { 
                          $display_name =$c_value['display_name'];
                          $placeholder =$c_value['placeholder'];
                          $name =$c_value['name'];
                          $type=$c_value['type'];
                          $id=@$c_value['id'];
                          $old_value=$c_value['ex_val'];
                          $required=@$c_value['required'];

                          $name_field ='name="Karigar['.$name.']" ';

                          $disabled = @$c_value['disabled'];
                          $disabled_field="";
                          if ($disabled == true) {
                            $disabled_field ="disabled='true'";
                            $name_field="";

                          }
                      ?>
                        
                         <div class="form-group col-sm-6">
                        <label class="col-sm-4 control-label" for="inputEmail3"><?=$display_name?> <span class="asterisk"><?=$required?></span></label>
                        <div class="col-sm-8">
                          <?php if(@$type=="text"){ ?>

                              <input type="text" placeholder="<?=$placeholder?>" class="form-control" <?=$name_field?> value="<?=$old_value;?>" id="<?=$id?>">

                        <?php  } elseif(@$type=="dropdown") { ?>

                           <select <?=$name_field?> class="form-control" id="<?=$id?>" exvalue="<?=$old_value?>" <?=$disabled_field?>>
                            <option value="">Select <?=$display_name?></option>
                            <?php foreach ($c_value['dropdown_array'] as $key => $value) { ?>
                             <option value="<?=$value['id']?>" <?=($old_value ==$value['id']) ? 'selected' : ''; ?>><?=$value['name']?></option>
                            <?php } ?>
                            </select>

                         <?php } else if(@$type=="radio"){

                         }

                          ?>
                           
                           <span class="text-danger" id="<?=$name?>_error"></span>
                        </div>
                     </div>

                     <?php } ?>

                    <div class="btnSection col-sm-12">                      
                      
                      <button class="btn btn-default waves-effect waves-light btn-md" onclick="window.location='<?= ADMIN_PATH?>Party_master'" type="reset">BACK</button>
                      <button class="btn btn-success pull-right waves-effect waves-light btn-md" name="commit" type="button" onclick="save_party('update'); ">SAVE</button>                      
                    </div>
                  </form>
               </div>
               <div class="clearfix"></div>
            </div>
         </div>
      </div>
   </div>
</div>