<div class="content-page">
  <div class="content">
     <div class="container">
        <div class="row">
           <div class="col-sm-12 card-box-wrap">
              <div class="panel panel-default">
                <div class="panel-heading">
                  <h4 class="inlineBlock">All Parties</h4>
                  <div class="pull-right single-send">
                  <a href="<?= ADMIN_PATH?>Party_master/create" type="button" class="add_senior_manager_button btn btn-primary waves-effect w-md waves-light m-b-5 btn-md single-send pull-right">ADD Party</a>
                  </div>  
                </div>
                <div class="panel-body">
                  
                  <div class="clearfix"></div>
                   <div class="table-rep-plugin">
                      <div class="b-0 scrollhidden table-responsive">
                        <table class="table table-bordered custdatatable" id="party_master_table">
                          <thead>
                        <?php
                          $data['heading']='party_master_table';
                          $this->load->view('master/table_header',$data);
                        ?>                          
                           <!--   <tr>
                                 <th class="col4">Type</th>
                                 <th class="col4">Name</th>
                                 <th class="col4">Code</th>
                                 <th class="col3">Mobile Number</th>
                                 <th></th>
                             </tr> -->
                          </thead>
                        </table>
                      </div>
                   </div>
                </div>
              </div>
           </div>
        </div>
     </div>
  </div>
</div>