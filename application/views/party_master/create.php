<div class="content-page">
<div class="content">
   <div class="container">
      <div class="row">
         <div class="col-sm-12 card-box-wrap">
            <div class="panel panel-default">
              <div class="panel-heading frmHeading">
                <h4 class="inlineBlock"><span>Add Party</span></h4>                
              </div>
              <div class="panel-body">
                <div class="col-lg-12 clearfix">
                  <form name="party_master_form" id="party_master_form" role="form" class="form-horizontal" method="post">
                     <input type="hidden" id="controller_name" value="Party_master">
                  
                    <?php $columns_array=array(
                                            array( 
                                                "display_name" => 'Party Type',
                                                "placeholder" => '',
                                                "name"=>'customer_type_id',
                                                "type" =>'dropdown',
                                                "dropdown_array"=>$customer_type,
                                                "required"=>'*',
                                                "id"=>"party_type",
                                              ), array( 
                                                "display_name" => 'Email',
                                                "placeholder" => 'Enter Email',
                                                "name"=>'email',
                                                "type" =>'text',
                                                //"required"=>'*',
                                              ),
                                            array( 
                                                "display_name" => 'Name',
                                                "placeholder" => 'Enter Name',
                                                "name"=>'name',
                                                "type" =>'text',
                                                "required"=>'*',
                                              ),
                                            array( 
                                                "display_name" => 'Code',
                                                "placeholder" => 'Enter Code',
                                                "name"=>'code',
                                                "type" =>'text',
                                                "required"=>'*',
                                                "id"=>"karigar_code",
                                              ),
                                            array( 
                                                "display_name" => 'Address',
                                                "placeholder" => 'Enter Address',
                                                "name"=>'address',
                                                "type" =>'text',
                                                "required"=>'',
                                              ),
                                           /* array( 
                                                "display_name" => 'Area',
                                                "placeholder" => 'Enter Area',
                                                "name"=>'area',
                                                "type" =>'text',
                                                "required"=>'',
                                              ),*/
                                             array( 
                                                "display_name" => 'State',
                                                "placeholder" => '',
                                                "name"=>'state_id',
                                                "type" =>'dropdown',
                                                "dropdown_array"=>$state,
                                                "id"=>'state',
                                                "required"=>'',
                                              ),
                                              array( 
                                                "display_name" => 'City',
                                                "placeholder" => '',
                                                "name"=>'city_id',
                                                "type" =>'dropdown',
                                                "dropdown_array"=>$city,
                                                "id"=>'city',
                                                "required"=>'',
                                              ),
                                              array( 
                                                "display_name" => 'Contact Person',
                                                "placeholder" => 'Enter Contact Person Name',
                                                "name"=>'contact_person',
                                                "type" =>'text',
                                              ),
                                              array( 
                                                "display_name" => 'Company Name',
                                                "placeholder" => 'Enter Company Name',
                                                "name"=>'company_name',
                                                "type" =>'text',
                                                "required"=>'*',
                                              ),
                                             array( 
                                                "display_name" => 'Refer By',
                                                "placeholder" => 'Enter Reference Name',
                                                "name"=>'refer_by',
                                                "type" =>'text',
                                              ),
                                         /*    array( 
                                                "display_name" => 'Hallmarking Center',
                                                "placeholder" => '',
                                                "name"=>'hc_id',
                                                "type" =>'dropdown',
                                                "dropdown_array"=>$hallmarking_center,
                                              ),*/
                                             array( 
                                                "display_name" => 'Phone Number',
                                                "placeholder" => 'Enter Phone Number',
                                                "name"=>'phone_no',
                                                "type" =>'text',
                                              ),
                                             array( 
                                                "display_name" => 'Mobile Number',
                                                "placeholder" => 'Enter MObile Number',
                                                "name"=>'mobile_no',
                                                "type" =>'text',
                                                "required"=>'*'
                                              ),
                                            /*  array( 
                                                "display_name" => 'Limit Amount',
                                                "placeholder" => 'Enter Limit Amount',
                                                "name"=>'limit_amt',
                                                "type" =>'text',
                                              ),*/
                                              array( 
                                                "display_name" => 'Limit Metal',
                                                "placeholder" => 'Enter Limit Metal',
                                                "name"=>'limit_metal',
                                                "type" =>'text',
                                              ),
                                              array( 
                                                "display_name" => 'No Of Days',
                                                "placeholder" => 'Enter No Of days',
                                                "name"=>'days',
                                                "type" =>'text',
                                              ),
                                              array( 
                                                "display_name" => 'Sales Under',
                                                "placeholder" => '',
                                                "name"=>'sales_under',
                                                "type" =>'dropdown',
                                                "dropdown_array"=>$sales_under,
                                              ),
                                                array( 
                                                "display_name" => 'Terms',
                                                "placeholder" => 'Enter Terms',
                                                "name"=>'terms',
                                                "type" =>'text',
                                              ),
                                              array( 
                                                "display_name" => 'Bank A/c No',
                                                "placeholder" => 'Enter Bank A/c No',
                                                "name"=>'bank_ac',
                                                "type" =>'text',
                                              ),
                                              array( 
                                                "display_name" => 'IFSC No',
                                                "placeholder" => 'Enter IFSC No',
                                                "name"=>'bank_ifsc',
                                                "type" =>'text',
                                              ),
                                              array( 
                                                "display_name" => 'Bank Name',
                                                "placeholder" => 'Enter Bank Name',
                                                "name"=>'bank_name',
                                                "type" =>'text',
                                              ),
                                              array( 
                                                "display_name" => 'Bank Branch',
                                                "placeholder" => 'Enter Bank Branch',
                                                "name"=>'bank_branch',
                                                "type" =>'text',
                                              ),
                                        ); ?>

                      <?php foreach ($columns_array as $c_key => $c_value) { 
                          $display_name =$c_value['display_name'];
                          $placeholder =$c_value['placeholder'];
                          $name =$c_value['name'];
                          $type=$c_value['type'];
                          $id=@$c_value['id'];
                          $required=@$c_value['required'];
                      ?>
                        
                         <div class="form-group col-sm-6">
                        <label class="col-sm-4 control-label" for="inputEmail3"><?=$display_name?> 
                        <span class="asterisk"><?=$required?></span></label>
                        <div class="col-sm-8">
                          <?php if(@$type=="text"){ ?>

                              <input type="text" placeholder="<?=$placeholder?>" class="form-control" name="Karigar[<?=$name?>]" id="<?=$id?>">

                        <?php  } elseif(@$type=="dropdown") { ?>

                           <select name="Karigar[<?=$name?>]" class="form-control" id="<?=$id?>">
                            <option value="">Select <?=$display_name?></option>
                            <?php foreach ($c_value['dropdown_array'] as $key => $value) { ?>
                             <option value="<?=$value['id']?>"><?=$value['name']?></option>
                            <?php } ?>
                            </select>

                         <?php } else if(@$type=="radio"){

                         }

                          ?>
                           
                           <span class="text-danger" id="<?=$name?>_error"></span>
                        </div>
                     </div>

                     <?php } ?>


                  </form>

               </div>
               <div class="btnSection">                     
                     
                      <button class="btn btn-default waves-effect waves-light m-l-5 btn-md" onclick="window.location='<?= ADMIN_PATH?>Party_master'" type="reset">BACK</button>
                       <button class="btn btn-success pull-right waves-effect waves-light btn-md" name="commit" type="button" onclick="save_party('store'); ">SAVE</button>                     
                    </div>
               <div class="clearfix"></div>
              </div>
            </div>
         </div>
      </div>
   </div>
</div>