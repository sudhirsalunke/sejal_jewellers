<?php //print_r($product_list);exit; ?>

<!DOCTYPE html>
<html>
<head>
<link rel="stylesheet" href="<?=ADMIN_PLUGINS_PATH?>chartist/dist/chartist.min.css">
        

        <!-- mscrollbar css -->
        <link href="<?=ADMIN_CSS_PATH?>jquery.mCustomScrollbar.min.css" rel="stylesheet" type="text/css" />
        
        <!-- App css -->
        <link href="<?=ADMIN_CSS_PATH?>bootstrap.min.css" rel="stylesheet" type="text/css" />
        <link href="<?=ADMIN_CSS_PATH?>core.css?<?=time()?>" rel="stylesheet" type="text/css" />
        <link href="<?=ADMIN_CSS_PATH?>customtable.css" rel="stylesheet" type="text/css" />
        <link href="<?=ADMIN_CSS_PATH?>components.css" rel="stylesheet" type="text/css" />
        <link href="<?=ADMIN_CSS_PATH?>icons.css" rel="stylesheet" type="text/css" />
        <link href="<?=ADMIN_CSS_PATH?>pages.css?<?=time()?>" rel="stylesheet" type="text/css" />
        <link href="<?=ADMIN_CSS_PATH?>menu.css?<?=time()?>" rel="stylesheet" type="text/css" />
        <link href="<?=ADMIN_CSS_PATH?>jquery-ui.css" rel="stylesheet" type="text/css" />
        <link href="<?=ADMIN_CSS_PATH?>style-ui.css" rel="stylesheet" type="text/css" />
        <!-- <link href="<?=ADMIN_CSS_PATH?>aos.css" rel="stylesheet" type="text/css" /> -->
        <link href="<?=ADMIN_CSS_PATH?>font-face.css" rel="stylesheet" type="text/css" />
        <link href="<?=ADMIN_CSS_PATH?>jquery.multiselect.css" rel="stylesheet" type="text/css" />
       <!--  <link href="<?=ADMIN_CSS_PATH?>timepicker.css" rel="stylesheet"> -->
        <!-- DataTables -->
        <link href="<?=ADMIN_PLUGINS_PATH?>datatables/jquery.dataTables.min.css" rel="stylesheet" type="text/css" />
        <link href="<?=ADMIN_PLUGINS_PATH?>datatables/buttons.bootstrap.min.css" rel="stylesheet" type="text/css" />
        <link href="<?=ADMIN_PLUGINS_PATH?>datatables/fixedHeader.bootstrap.min.css" rel="stylesheet" type="text/css" />
        <link href="<?=ADMIN_PLUGINS_PATH?>datatables/responsive.bootstrap.min.css" rel="stylesheet" type="text/css" />
        <link href="<?=ADMIN_PLUGINS_PATH?>datatables/scroller.bootstrap.min.css" rel="stylesheet" type="text/css" />

        <!-- datepicker -->
        <link href="<?=ADMIN_PLUGINS_PATH?>bootstrap-datepicker/dist/css/bootstrap-datepicker.min.css" rel="stylesheet">
        <link href="<?=ADMIN_PLUGINS_PATH?>bootstrap-daterangepicker/daterangepicker.css" rel="stylesheet">
        <link href="<?=ADMIN_PLUGINS_PATH?>timepicker/bootstrap-timepicker.min.css" rel="stylesheet">
        
        <link href="<?=ADMIN_PLUGINS_PATH?>datetimepicker/jquery.datetimepicker.css" rel="stylesheet" media="screen">
        <!-- datepicker -->

        <!-- select2 -->
        <link href="<?=ADMIN_PLUGINS_PATH?>select2/dist/css/select2.css" rel="stylesheet" type="text/css">
        <link href="<?=ADMIN_PLUGINS_PATH?>select2/dist/css/select2-bootstrap.css" rel="stylesheet" type="text/css">
        <!-- select2 -->

        <!--- Chosen-->
        <link href="<?=ADMIN_PLUGINS_PATH?>chosen_v1.7.0/chosen.css" rel="stylesheet" type="text/css">

        <!-- chosen -->

        <link href="<?=ADMIN_CSS_PATH?>style.css?<?=time()?>" rel="stylesheet" type="text/css" />
        <link href="<?=ADMIN_CSS_PATH?>responsive.css?<?=time()?>" rel="stylesheet" type="text/css" />
         <!-- Table css -->
       
       <?php if(isset($page_title) && $page_title!="ARF") { ?>
        <link href="<?=ADMIN_PLUGINS_PATH?>RWD-Table-Patterns/dist/css/rwd-table.min.css" rel="stylesheet" type="text/css" media="screen">
       <?php } ?>

        <!-- Notification css (Toastr) -->
        <link href="<?=ADMIN_PLUGINS_PATH?>toastr/toastr.min.css" rel="stylesheet" type="text/css" />
       <link rel="stylesheet" href="//cdn.materialdesignicons.com/2.1.99/css/materialdesignicons.min.css">

        <script src="<?=ADMIN_JS_PATH?>jquery.min.js"></script>
        <script src="<?=ADMIN_JS_PATH?>modernizr.min.js"></script>

  <style type="text/css">
  .m-t-15{margin-top: 15px;}
  @media print{
    .print_detail{margin-left: 15%;}
    .single_product_div{margin-left: 15%;page-break-after: always;}
    hr{display: none;}
    .barcode_img img {height: 30px;}
  }
  table td {padding: 5px;}
     @page {
    size: auto;   /* auto is the initial value */
    margin: 0;  /* this affects the margin in the printer settings */
}
  </style>
</head>
<body>

      <div class="row">
         <div class="col-sm-12 card-box-wrap">
            <div class="card-box padmobile">
             <!--   <h4 class="header-title business_manager_header_title m-t-0 m-b-30 bmsmheader bmsmheaderabm"><span><?= strtoupper($page_title);?></span> -->
               </h4>
<!--                 <button type="button" onclick="PrintDiv()">print</button>
 -->                <div class="clearfix"></div>
               <div class="product_listing">
                <div class="print_area">
               <?php
              // print_r($product_list);

                $i=0; $cnt = sizeof($product_list); foreach($product_list as $val){ 
                  $new_master =array();
               /*size master*/
               
                    $size_master = explode('/', @$val['size_master']);
                    $line_numbers = explode(',', $val['line_numbers']);
                    $finding_cost = explode('/', @$val['finding_cost']);
                    $weight_band = explode('/', @$val['weight_band']);
                     $cp_weight = explode('/', @$val['cp_weight_band']);
                    /*setting type*/
                    if (count(array_unique($size_master)) >= 2) {
                        $size_array =1;
                      }else{
                        $size_array =0;
                        $val['size_master'] = (!empty($val['size_master'])) ? $size_master[0] : $val['cp_size']; 
                      }

                      if (count(array_unique($finding_cost)) >= 2) {
                        $finding_cost_array =1;
                      }else{
                        $finding_cost_array =0;
                        $val['finding_cost'] = (!empty($val['finding_cost'])) ? $finding_cost[0] : '-' ; 
                      }

                      if (count(array_unique($weight_band)) >= 2) {
                        $weight_band_array =1;
                      }else{
                        $weight_band_array =0;
                        $val['weight_band'] = (!empty($val['weight_band'])) ? $weight_band[0] : $val['net_weight']; 
                      }
                      if (count(array_unique($cp_weight)) >= 2) {
                        $avg_weight_array =1;
                      }else{                        
                        $avg_weight_array =0;
                        $val['cp_weight'] = (!empty($val['cp_weight'])) ? $cp_weight[0] : $val['net_weight']; 
                     }

                    foreach ($line_numbers as $u_k => $u_v) {
                      if ($size_array ==1) {
                        $new_master['size'][$size_master[$u_k]][] =$u_v;
                      }
                      if ($finding_cost_array ==1) {
                       $new_master['finding_cost'][$finding_cost[$u_k]][] = $u_v;
                      }
                      if ($weight_band_array ==1) {
                        $new_master['weight_band'][$weight_band[$u_k]][] = $u_v;
                      }
                       if ($avg_weight_array ==1) {
                        $new_master['cp_weight'][$cp_weight[$u_k]][] =$u_v;
                      }
                          
                    }
                  
                   /*calcualation of pcs*/
                    if($val['corporate']==1){
                     $pcs = count(explode(',',$val['line_numbers']));
                      if (in_array($val['product_master'], array('SET','PST'))) {
                        $pcs = $pcs/2;
                      }
                    }else{
                      $pcs = $val['ppl_quantity'].' '.$val['unit'];
                    } 

                ?>
                
              <div class="m-t-15 single_product_div clearfix">
                <div class="col-sm-6 image_div" style="float:left;text-align:center">
                  <div class="img pull-right">
                    <?php
                        /*image check*/
                         if(file_exists('uploads/product/'.$val["product_code"].'/'.$val["product_code"].'.jpg')){
                           $image_path=ADMIN_PATH.'uploads/product/'.$val["product_code"].'/'.$val["product_code"].'.jpg';

                         }else if(file_exists('uploads/product/'.$val["product_code"].'/large/'.$val["image"])){
                          $image_path=ADMIN_PATH.'uploads/product/'.$val["product_code"].'/large/'.$val["image"];

                         }else{
                          $image_path="";
                         }

                         if (!empty($image_path)) { ?>
                            <img src="<?=$image_path?>" alt="" height="250px" width="250px">
                        <?php } ?>

                     
                      <!--<img src="<?=ADMIN_PATH.'uploads/product/'.$val["product_code"].'/large/'.$val["image"]?>" alt="">-->
                  </div>
                  </div>
                 <div class="col-sm-6">                
                  <table class="print_table">
                    <tbody>
                      <tr>
                        <td><div class="karigar_name control-label">Karigar Name / Stamp </div></td>
                        <!-- <td><span class="">-</span></td> -->
                        <td><div class=""><?=$val['name']; ?>(<?=$val['karigar_code'];?>)</div></td>
                      </tr>
                      <?php if(!empty($val['line_numbers'])){ ?>
                       <tr>
                        <td><label class="karigar_name control-label" for="inputEmail3"> Line Number </label></td>
                        <td><div class=" karigar_name"><?= $val['line_numbers'];?></div></td>
                      </tr>
                      <?php } ?>
                      <tr>
                        <td><label class=" control-label" for="inputEmail3"> Order ID </label></td>
                        <td><div class=""><?=$val['order_id']; ?></div></td>
                      </tr>
                      <tr>
                        <td><label class=" control-label" for="inputEmail3"> Design Code </label></td>
                        <td><div class=""><?=$val['product_code']; ?></div></td>
                      </tr>
                     <tr>
                       <?php 
                       if(@$val['pair_pcs']=='0' || @$val['product_master'] == 'BAN' || $val['product_master'] == 'PST' || $val['product_master'] == 'SET'){$label='Pair'; }else{$label='PCS';} ?>
                        <td><label class=" control-label" for="inputEmail3"><?php echo $label?></label></td>
                        <td><div class=""><?=$pcs?></div></td>
                      </tr>
                      
                     <!--  <tr>
                        <td><label class=" control-label" for="inputEmail3"> Size / Length</label>
                          </td><td><span class="">-</span></td>
                        <td><div class=""><?=$val['size_master']; ?></div></td>
                      </tr>
                       <tr>
                        <td><label class=" control-label" for="inputEmail3"> Screw</label>
                          </td><td><span class="">-</span></td>
                        <td style="white-space:nowrap"><div class=""><?=$val['finding_cost']; ?></div></td>
                      </tr>
                      <tr>
                        <td><label class="control-label" for="inputEmail3"> Weight Range </label>
                          </td><td><span class="">-</span></td>
                        <td><div class=""><?=$val['weight_band']; ?></div></td>
                      </tr> -->

               <!--  REMOVE SIZE/LENGTH
              NKL, ECN, NOP, NOR ,PST, PDT   
              if(!in_array($val['product_master'], array('NKL', 'ECN', 'NOP', 'NOR' ,'PST', 'PDT')))  

              SHOW SIZE/LENGTH
              if(in_array($val['product_master'], array('BAN', 'RNG', 'CHN')))
               if(in_array($val['sub_category'], array('BAN', 'RNG', 'CHN')))

               BAN, RNG,CHN -->
                <?php if(in_array($val['product_master'], array('BAN', 'RNG', 'CHN')) || $val['size_master']!=''){ ?>
                       <tr>
                        <td><label class=" control-label" for="inputEmail3">  <?php if(in_array($val['product_master'], array('BAN', 'RNG'))):echo"Size"; else: echo"Length"; endif; ?></label>
                          </td>
                        <td><div class=""><?php if($size_array==1){
                           end($new_master['size']);
                          $last_key_sz = key($new_master['size']);
                         foreach ($new_master['size'] as $sz_key => $sz_value) {
                           echo "<b>".$sz_key." = </b>".implode(',', $sz_value);
                           if($last_key_sz!=$sz_key){
                            echo " || ";
                           }
                          }
                        }
                        else{ echo $val['size_master']; } ?></div></td>
                      </tr>
                      <?php } ?>

                       <?php 
                     //if(in_array($val['product_master'], array('NKL', 'ERG')) || $val['finding_cost']){  
                      if(in_array($val['product_master'], array('PST', 'SET', 'ERG'))){
                     /*   if($val['product_master'] == 'BAN'){
                        $screw_label = 'Pieces';                          
                        }else{
                          $screw_label = 'Screw';
                        }*/
                        ?>
                       <tr>
                        <td><label class=" control-label" for="inputEmail3"> Screw </label></td>
                        <td><div class=""><?php if($finding_cost_array==1){
                          end($new_master['finding_cost']);
                          $last_key_fc = key($new_master['finding_cost']);
                          foreach ($new_master['finding_cost'] as $fc_key => $fc_value) {
                          echo "<b>".implode(',', $fc_value)." = </b>".$fc_key;
                            if($last_key_fc!=$fc_key){
                            echo " || ";
                           }
                          }
                         }else { echo $val['finding_cost']; } ?></div></td>
                      </tr>
                       <?php }else if(in_array($val['product_master'], array('BAN'))) { ?>
                          <tr>
                        <td><label class=" control-label" for="inputEmail3"> Pieces </label></td>
                        <td><div class=""><?php if ($finding_cost_array==1){
                          end($new_master['finding_cost']);
                          $last_key_fc = key($new_master['finding_cost']);
                          foreach ($new_master['finding_cost'] as $fc_key => $fc_value) {
                             echo "<b>".implode(',', $fc_value)." = </b>".$fc_key;
                            if($last_key_fc!=$fc_key){
                            echo " || ";
                           }
                          }
                         }else { echo $val['finding_cost']; } ?></div></td>
                      </tr>
                
                       <?php } ?>

                <!--       <tr>
                        <td><label class="control-label" for="inputEmail3"><?=(!empty($val['weight_band'])) ? 'Weight Range' : 'Net Weight'; ?>  </label>
                          </td><td><span class="">-</span></td>
                        <td><div class="" style="white-space:nowrap"><?php if ($weight_band_array==1){
                          end($new_master['weight_band']);
                          $last_key_wb = key($new_master['weight_band']);
                          foreach ($new_master['weight_band'] as $wb_key => $wb_value) {
                           echo "<b>".$wb_key." = </b>".implode(',', $wb_value);
                           if($last_key_wb!=$wb_key){
                            echo " || ";
                           }
                          }
                         }else { echo $val['weight_band']; } ?></div></td>
                      </tr> -->
                      <!--   <tr>
                        <td><label class="control-label" for="inputEmail3">Avg Weight</label>
                          </td><td><span class="">-</span></td>
                        <td><div class="" style="white-space:nowrap"><?php if (!empty($val['cp_weight'])){
                          echo $val['cp_weight'];
                          
                         }else { echo $val['net_weight']; } ?></div></td>
                      </tr>  -->

                               <tr>
                     
                          <td><label class="control-label" for="inputEmail3"><?=(!empty($val['cp_weight'])) ? 'Avg Weight' : 'Net Weight'; ?> </label></td>
                          <td>
                          <div class="" >
                            <?php if ($avg_weight_array==1){
                            end($new_master['cp_weight']);
                            $last_key_cw = key($new_master['cp_weight']);
                            foreach ($new_master['cp_weight'] as $cw_key => $cw_value) {
                            echo "<b>".implode(',', $cw_value)." = </b>".$cw_key;
                             if($last_key_cw!=$cw_key){
                              echo " || ";
                             }
                            }
                           }else { echo $val['cp_weight']; } ?></div></td>
                      </tr>
                      <?php if(!empty($val['tolerance_wt'])){
                        ?>
                       <tr>
                        <td>
                          <label class="control-label" for="inputEmail3">Weight Range</label></td>
                        <td><div class="">
                        <?php if ($avg_weight_array==1){
                          end($new_master['cp_weight']);
                          $last_key_cw = key($new_master['cp_weight']);
                            foreach ($new_master['cp_weight'] as $cw_key => $cw_value) {
                              $cp_wt=implode(',', $cw_value);
                              if(!empty($cw_key)){
                                  $from_wt_torlen= $cw_key-$val['tolerance_wt'];
                                  $to_wt_torlen=$cw_key+$val['tolerance_wt'];
                                  $tolerance_wt_range= number_format($from_wt_torlen , 2, '.', '') .' - '. number_format($to_wt_torlen , 2, '.', '');  
                              }

                               echo "<b>".$cp_wt." = </b>".$tolerance_wt_range;
                               if($last_key_cw!=$cw_key){
                                echo " || ";
                               }
                            }
                         }else { 
                                $from_wt_torlen= $val['cp_weight']-$val['tolerance_wt'];
                                                    $to_wt_torlen=$val['cp_weight']+$val['tolerance_wt'];
                                                    $tolerance_wt_range= number_format($from_wt_torlen , 2, '.', '') .' - '. number_format($to_wt_torlen , 2, '.', '');  
                            echo $tolerance_wt_range; } ?> 

                         </div>
                         </td>
                         </tr>
                      <?php }else if(!empty($val['tolerance_c_wt'])){

                                 $from_wt_torlen= $val['net_weight']-$val['tolerance_c_wt'];
                                 $to_wt_torlen=$val['net_weight']+$val['tolerance_c_wt'];
                                 $tolerance_wt_range= number_format($from_wt_torlen , 2, '.', '') .' - '. number_format($to_wt_torlen , 2, '.', '');


                        ?>
                      <tr>
                        <td><label class="control-label" for="inputEmail3">Weight Range</label></td>
                        <td><div class="" style="white-space:nowrap"><?php if (!empty($tolerance_wt_range)){
                          echo $tolerance_wt_range;
                          
                         }else { echo '-'; } ?></div></td>
                      </tr>
                      <?php } ?>
                      <?php /*if($val['ppl_status']=='0'){
                        $dt = date("Y-m-d");
                        $order_date1 =  date( "d M Y", strtotime( "$dt" ) );
                        $due_date1 = date( "d M Y", strtotime( "$dt +12 day" ) );
                      }
                      else if($val['ppl_status']=='1'){ 
                        if($val['karigar_engaged_date']!='0000-00-00' || $val['karigar_engaged_date']!='')
                        $dt = $val['karigar_engaged_date'];
                        else
                        $dt = date("Y-m-d");
                        $order_date1 =  date( "d M Y", strtotime( "$dt" ) );
                        $due_date1 = date( "d M Y", strtotime( "$dt +12 day" ) );
                      }*/
                      ?>
                      <!-- <tr>
                        <td><label class=" control-label" for="inputEmail3"> Order Date </label>
                          </td><td><span class="">-</span></td>
                        <td><div class=""><?=$order_date1;?></div></td>
                      </tr>
                      <tr>
                        <td><label class=" control-label" for="inputEmail3"> Delivery Date </label>
                          </td><td><span class="">-</span></td>
                        <td><div class=""><?=$due_date1;?></div></td>
                      </tr>  -->
                          <tr>
                        <td><label class=" control-label" for="inputEmail3"> Order Date </label>
                        <td><div class=""><?=$val['karigar_engaged_date'];?></div></td>
                      </tr>
                      <tr>
                        <td><label class=" control-label" for="inputEmail3"> Delivery Date </label></td>
                        <td><div class=""><?=$val['karigar_delivery_date'];?></div></td>
                      </tr>
                       <tr><td colspan="3">
                      <?php
                       if($val['barcode_path'] == "" || $val['barcode_path'] == null){
                             $val['barcode_path'] = $val['cp_barcode_path'];
                        }
                          $codes =explode(',', $val['barcode_path']);
                          //$count = 1;
                         foreach ($codes as $key => $value) {
                       ?>
                            <div class="barcode_img" style="display: inline-block;"><img src="<?=ADMIN_PATH?>/uploads/barcode/<?=trim($value)?>"></div>
                          <?php
                         //  if($count == 3){
                         //     echo "<br>"; 
                         //     $count = 0; 
                         //  }
                         // $count++;
                        }
                      ?>
                          </td></tr>
                     
                    </tbody>  
                  </table>
                </div>

                <div class="row m-t-20 clearfix">
                <div class="col-sm-4 col-sm-offset-4 print_detail">
                  <div class="note karigar_name" style="font-weight:bold;padding-left: 10px;">
                    <p style="line-height: 10px;margin: 10px 0;">NO OTHER STAMP</p>
                    <p style="line-height: 10px;margin: 10px 0;">Note : Without Xerox Design will not be accepted.</p>
                    <p style="line-height: 10px;margin: 10px 0;">অন্য কোন স্ট্যাম্প</p>
                    <p style="line-height: 10px;margin: 10px 0;">নোট: জেরক্স পণ্য ছাড়া গ্রহণ করা হবে না।</p>
                  </div>                  
                </div>
              </div>
            </div>
              
               
              <hr />
<?php $i++; } if($i < $cnt){ ?> <?php } ?>
            </div>
</div></div></div>      
</body>
</html>