<style>
.karigar_name
{
  font-weight:bold;
}
.product_listing >div
{
  background-color: #fff;
}
.image_div
{
  text-align: center;
  float: left;
}
table td{
  padding: 5px;
}
hr{
  margin-bottom: 50px;
}

@media print {
 

  body * {
    visibility: hidden;
  }
  .print_area, .print_area * {
    visibility: visible;
  }
  .print_area {
    
    left: 0;
    top: 0;
  }
  .pgbrk 
  {
    page-break-after: always;
  }
}

</style>
<div class="content-page">
<div class="content">
   <div class="container">
      <div class="row">
         <div class="col-sm-12 card-box-wrap">
            <div class="card-box padmobile reprint-blank">
             <div class="panel-heading frmHeading">
              <h4 class="inlineBlock"><span>Print Order </span></h4>
              </div>
               <div class="clearfix"></div>
               <div class="product_listing">
                <?php  //if(!empty($product_list)){ ?>
                <div class="stikySection" data-spy="affix" data-offset-top="150">                 
                  <div class="row">
                    <form method="post" action="" class="col-sm-12 form-group form-inline print_form">
                      <input type="hidden" id="karigar_id_filter" value="<?=$karigar_id?>">

                      <input type="hidden" name="filter_form" value="1">
                          <div class="col-lg-2 col-md-3 m-b-5">
                          <select class="form-control 3col active" name="filter[order_id][]" id="order_filter" onchange="filter_karigar('Print_Order')" multiple>
             
                            <?php foreach ($order_ids as  $oi_value) {     
                              if(in_array($oi_value['order_id'],@$ex_filter['ex_order_id'])){
                                $selected='selected';
                              }else{
                                $selected='';
                              }
                             ?>
                          <option value="<?=$oi_value['order_id']?>" <?php echo $selected;?>><?=$oi_value['order_id']?></option>

                                <!-- <option value="<?=$oi_value['order_id']?>" <?=($ex_filter['ex_order_id']==$oi_value['order_id']) ? 'selected' : '';?>><?=$oi_value['order_id']?></option> -->
                          <?php } ?>
                          </select>
                          </div>    
                         <div class="col-lg-2 col-md-3 m-b-5">                
                          <select class="form-control select_active 3col" name="filter[product_code][]" id="product_code_filter" multiple> 
                          </select>
                          </div>

                         <!--  Add filters weight range, category and size start --> 
                     <!--    <div class="col-md-2 m-t-5">
                          <select class="form-control 3col select_wtr" name="filter[weight_range][]" id="weight_range_filter"  multiple>             
                              <?php  
                              foreach ($weight_range as  $wr_value) {  
                                $weight=number_format($wr_value['from_weight'])."-".number_format($wr_value['to_weight']);
                              if(in_array($weight,@$ex_filter['ex_weight_range'])){
                                $selected='selected';
                              }else{
                                $selected='';
                              }                        
                               ?>
                                <option value="<?=number_format($wr_value['from_weight'])."-".number_format($wr_value['to_weight'])?>" <?php echo $selected;?>><?=$wr_value['from_weight']."-".$wr_value['to_weight']?></option>
                            <?php } ?>
                          </select>
                          </div>  --> 
           <!--              <div class="col-md-2">
                          <select class="form-control 3col select_pcs" name="filter[pcs][]" id="psc_filter" multiple>
             
                            <?php foreach ($pcs as  $pcs_value) {     
                              if(in_array($pcs_value['id'],@$ex_filter['ex_pcs'])){
                                $selected='selected';
                              }else{
                                $selected='';
                              }
                             ?>
                          <option value="<?=$pcs_value['id']?>" <?php echo $selected;?>><?=$pcs_value['name']?></option>
                          <?php } ?>
                          </select>
                          </div>  -->
                           <div class="col-lg-2 col-md-3 m-b-5">
                          <select class="form-control 3col select_size" name="filter[size][]" id="size_filter" multiple>
                       <?php  
                             $size= array('Adjustable'=>'Adjustable' ,  'FREE SIZE'=>'FREE SIZE', '0.0' => '0.0' , '1.6' =>'1.6' ,  '1.7'=>'1.7' , '1.8' =>'1.8' ,  '1.9'=>'1.9' ,'1.10' => '1.10' , '1.11' =>'1.11' , 
                              '1.12'=>'1.12' , '1.13'=>'1.13', '1.14'=>'1.14' , '1.15' =>'1.15' ,  '2.0'=>'2.0' , '2.1'=> '2.1', '2.2'=>'2.2' ,  '2.3'=>'2.3' ,  '2.4'=>'2.4' ,  '2.5'=>'2.5' , '2.6'=>'2.6' , '2.7'=>'2.7' , 
                              '2.8' =>'2.8' ,  '2.9'=>'2.9' ,'2.10'=>'2.10' , '2.11'=>'2.11' ,  '2.12'=>'2.12' ,  '2.13'=>'2.13' , '2.14'=>'2.14' ,  '2.15'=>'2.15' ,  '3.0'=>'3.0' , '3.1' =>'3.1' , '3.2'=>'3.2' , '3.3'=>'3.3' ,  '3.4'=>'3.4' ,  '5.0'=>'5.0' , '5.5'=>'5.5' ,  '6.0'=>'6.0' ,  '6.5'=>'6.5' ,  '7.0'=>'7.0' ,  '7.5'=>'7.5' ,  '8.0'=>'8.0' ,  '8.5'=>'8.5' ,  '9.0'=>'9.0' ,  '9.5'=>'9.5' ,  '10.0'=>'10.0' ,  '11.0'=>'11.0' ,  '06'=>'06' ,  '07'=>'07' ,  '08'=>'08' ,  '09'=>'09' ,  '6-9'=>'6-9' ,  
                              '10-13' =>'10-13' ,  '14-17'=>'14-17' ,  '16-19'=>'16-19' ,  '18-19'=>'18-19' ,  '20-23'=>'20-23' , '24-29'=>'24-29' ,'30-34' =>'30-34' ,  '9'=>'9'  , '10'=>'10' ,'11'=>'11' , '12'=>'12' , '13'=>'13' , '14'=>'14' , '15'=>'15' , '16'=>'16' , '17'=>'17' , '18'=>'18' ,'19'=> '19' ,'20'=>'20' , '21'=>'21' , '22'=>'22' , '23'=>'23' , '24'=>'24' , '25'=>'25' , '26'=>'26' , '27'=>'27' , '28'=>'28' , '29'=>'29' , '30'=>'30' , '31'=>'31' , '32'=>'32' ,'33'=>'33' , '34'=>'34' , '35'=>'35' , '36'=>'36' , '37'=>'37' , '38'=>'38' , '39'=>'39' , '40'=>'40' , '41'=>'41' ,'42'=> '42' , '43'=>'43' , '44'=>'44' , '45'=>'45' , '46'=>'46' , '47'=>'47' ,'48'=>'48');
    
                               
                              foreach ($size as  $size_value) {                              
                              if(in_array($size_value,@$ex_filter['ex_size'])){
                                $selected='selected';
                              }else{
                                $selected='';
                              }                        
                               ?>
                                <option value="<?=$size_value;?>" <?php echo $selected;?>><?=$size_value;?></option>
                            <?php } ?>
                                            
                      
                          </select>
                          </div>
                           <div class="col-lg-2 col-md-3 m-b-5">
                          <select class="form-control 3col select_category" name="filter[category][]" id="category_filter" multiple>
             
                            <?php 
                            foreach ($category as  $category_value) {     
                              if(in_array($category_value['product_master'],@$ex_filter['ex_category'])){
                                $selected='selected';
                              }else{
                                $selected='';
                              }
                             ?>
                          <option value="<?=$category_value['product_master']?>" <?php echo $selected;?>><?=$category_value['product_master']?></option>
                          <?php } ?>
                          </select>
                          </div> 
                           <!--  Add filters weight range, category and size end -->  
                     <div class="col-lg-2 col-md-12 filter_btn">
                       <button type="submit" class="btn btn-info btnFilter" style="margin-top: 0;">Apply Filter</button> 
                       <span class="text-danger col-sm-12" id="filter_error"></span>
                    </div>
                   </form> 
                  </div>
                   <?php if(!empty($product_list)){ 
                        $btn_print ='style="visibility: visible"';
                        }else{
                          $btn_print ='style="visibility: hidden"'; 
                        }?>   
                  <div class="col-sm-12" <?php echo $btn_print;?>>
                    <a href="<?=ADMIN_PATH.'Karigar_product_list' ?>">
                      <button class="btn btn-danger small loader-hide btn-sm pull-right m-l-10 cancel-karigar" name="commit" type="button">Cancel</button>
                    </a>
                   <form action="<?=ADMIN_PATH.'Print_Order/createPdf/'.$karigar_id.'?order_id='.$_GET['order_id']?>" method="POST" target="_blank"> 
                   <!--  <form action="" method="POST"  id="print_order_data"> -->

                    <!--   <input type="hidden" name="filter[order_id]" value="<?=@$ex_filter['ex_order_id']?>" id="pdf_order_filter">
                      <input type="hidden" name="filter[product_code]" value="<?=@$ex_filter['ex_product_code']?>" id="pdf_product_filter"> -->
                    <input type="hidden" name="filter_o[order_id]" value="<?=@$ex_order_id;?>" id="pdf_order_filter">
                    <input type="hidden" name="filter_o[product_code]" value="<?=@$ex_product_code;?>" id="pdf_product_filter">
                    <input type="hidden" name="filter_o[weight_range]" value="<?=@$ex_weight_range;?>" id="pdf_weight_range_filter">
                    <input type="hidden" name="filter_o[pcs]" value="<?=@$ex_pcs;?>" id="pdf_psc_filter">
                    <input type="hidden" name="filter_o[size]" value="<?=@$ex_size;?>" id="pdf_size_filter">
                     <input type="hidden" name="filter_o[category]" value="<?=@$ex_category;?>" id="pdf_category_filter">
                        
                    <button class="btn btn-success small loader-hide btn-sm pull-right m-l-10 send-karigar" name="commit" type="submit" onclick="Redirect('Engaged_karigar_list')">Print &amp; Send To Karigar</button><!-- 
                      <button class="btn btn-success small loader-hide btn-sm pull-right m-l-10 send-karigar" name="commit" type="button" onclick="send_to_karigar_corporate_product()">Print &amp; Send To Karigar</button> -->
                     <a href="<?=ADMIN_PATH.'Prepare_order/'.$karigar_id
                    ?>"><button class="btn btn-primary small loader-hide btn-sm pull-right m-l-10 change-karigar" name="commit" type="button">Change Karigar</button></a>                  
                  </div>
                </div>
                    <!-- </form> -->

               <div class="print_area clearfix">
               <?php //print_r($product_list); 
             if(empty($product_list)){
                  //  echo "No design found";
                    echo '<div class="" style="padding:20px; text-align:center"><p>Design Not Found !!</p></div>';
                  } 
               $i=0;  
               foreach($product_list as $key =>  $val){
                 $new_master =array();
               /*size master*/
               
                    $size_master = explode('/', @$val['size_master']);
                    $line_numbers = explode(',', $val['line_numbers']);
                    $finding_cost = explode('/', @$val['finding_cost']);
                    $weight_band = explode('/', @$val['weight_band']);
                    $cp_weight = explode('/', @$val['cp_weight_band']);

                    /*setting type*/
                    if (count(array_unique($size_master)) >= 2) {
                        $size_array =1;
                      }else{
                        $size_array =0;
                        $val['size_master'] = (!empty($val['size_master'])) ? $size_master[0] : $val['cp_size']; 
                      }

                      if (count(array_unique($finding_cost)) >= 2) {
                        $finding_cost_array =1;
                      }else{
                        $finding_cost_array =0;
                        $val['finding_cost'] = (!empty($val['finding_cost'])) ? $finding_cost[0] : '-' ; 
                      }

                      if (count(array_unique($weight_band)) >= 2) {
                        $weight_band_array =1;
                      }else{
                        $weight_band_array =0;
                        $val['weight_band'] = (!empty($val['weight_band'])) ? $weight_band[0] : $val['net_weight']; 
                      }
                    if (count(array_unique($cp_weight)) >= 2) {
                        $avg_weight_array =1;
                      }else{                        
                        $avg_weight_array =0;
                        $val['cp_weight'] = (!empty($val['cp_weight'])) ? $cp_weight[0] : $val['net_weight']; 
                     }

                    foreach ($line_numbers as $u_k => $u_v) {
                      if ($size_array ==1) {
                        $new_master['size'][$size_master[$u_k]][] = $u_v;
                      }
                      if ($finding_cost_array ==1) {
                       $new_master['finding_cost'][$finding_cost[$u_k]][] = $u_v;
                      }
                      if ($weight_band_array ==1) {
                        $new_master['weight_band'][$weight_band[$u_k]][] = $u_v;
                      }
                      if ($avg_weight_array ==1) {
                        $new_master['cp_weight'][$cp_weight[$u_k]][] =$u_v;
                      }
                        $karigar_wt+=$val['karigar_wt'];  
                        $karigar_net_wt+=$val['net_weight'];  
                    }
                  
                  /*calcualation of pcs*/
                    if($val['corporate']==1){
                     $pcs = count(explode(',',$val['line_numbers']));
                      if (in_array(trim($val['product_master']), array('SET','PST'))) {
                        $pcs = $pcs/2;
                      }
                    }else{  
                      $pcs = $val['ppl_quantity'].' '.$val['unit'];
                    } 

                ?>
             <div class="m-t-20 single_product_div">                         
                <input type="hidden" name="karigar" id="karigar" value="<?=$karigar_id;?>">
                <input type="hidden" name="karigar_wt" id="karigar_wt" value="<?=$karigar_wt;?>">

                <input type="hidden" name="karigar_net_wt" id="karigar_net_wt" value="<?=$karigar_net_wt;?>">

               <input type="hidden" name="karigar_wt_limit" id="karigar_wt_limit" value="<?=$val["limit_wt"]?>">
               <input type="hidden" name="corporate" id="corporate" value="<?=$product_list[0]["corporate"]?>">
                <div class="col-sm-6 col-xs-12 image_div">
                  <div class="img pull-right">
                   <!--  <img src="<?=ADMIN_PATH.'uploads/product/'.$val["product_code"].'/large/'.$val["product_code"].'.jpg'?>" alt="" width="250" height="250"> -->
                <img onclick="img_popup(<?php echo $key?> )" id='<?php echo "img_".$key?>' src="<?=UPLOAD_IMAGES.'product/'.$val["product_code"].'/large/'.$val["image"]?>" alt="" width="250" height="250"> 

                  </div>
                </div>
                <div class="col-sm-6 col-xs-12">
                  <table class="print_table">
                    <tbody>
                      <tr>
                        <td><label class="karigar_name control-label" for="inputEmail3" style="font-weight:bold"> Karigar Name / Stamp </label></td>
                        <!-- <td><span class="">-</span></td> -->
                        <td><div class=" karigar_name"><?=$val['name']; ?>(<?=$val['karigar_code'];?>)</div></td>
                      </tr>
                      <?php if(!empty($val['line_numbers'])){ ?>
                      <tr>
                        <td><label class="karigar_name control-label" for="inputEmail3"> Line Number </label></td>
                        <td><div class=" karigar_name"><?=$val['line_numbers'];?></div></td>
                      </tr>
                      <?php } ?>
                      <tr>
                        <td><label class=" control-label" for="inputEmail3"> Order ID </label></td>
                        <td><div class=""><?=$val['order_id']; ?></div></td>
                      </tr>
                      <tr>
                        <td><label class=" control-label" for="inputEmail3"> Design Code </label></td>
                        <td><div class=""><?=$val['product_code']; ?></div></td>
                      </tr>
                      <tr>
                      <?php if($val['pair_pcs']=='0' || $val['product_master'] == 'BAN' || $val['product_master'] == 'PST' || $val['product_master'] == 'SET'){$label='Pair'; }else{$label='PCS';} ?>
                        <td><label class=" control-label" for="inputEmail3"><?php echo $label?></label></td>
                        <td><div class=""><?=$pcs?></div></td>
                      </tr>
                       <!--  REMOVE SIZE/LENGTH
              NKL, ECN, NOP, NOR ,PST, PDT   
              if(!in_array($val['product_master'], array('NKL', 'ECN', 'NOP', 'NOR' ,'PST', 'PDT')))  

              SHOW SIZE/LENGTH
              if(in_array($val['product_master'], array('BAN', 'RNG', 'CHN')))
              if(in_array($val['sub_category'], array('BAN', 'RNG', 'CHN')))

               BAN, RNG,CHN 


               print_r($val['product_master']);    
               -->


                <?php  if(in_array($val['product_master'], array('BAN', 'RNG', 'CHN')) || $val['size_master']!=''){?>

                      <tr>
                        <td><label class=" control-label" for="inputEmail3"> <?php if(in_array($val['product_master'], array('BAN', 'RNG'))):echo"Size"; else: echo"Length"; endif; ?> </label></td>
                        <td><div class=""><?php if($size_array==1){
                           end($new_master['size']);
                          $last_key_sz = key($new_master['size']);
                         foreach ($new_master['size'] as $sz_key => $sz_value) {
                           echo "<b>".$sz_key." = </b>".implode(',', $sz_value);
                           if($last_key_sz!=$sz_key){
                            echo " || ";
                           }
                          }
                        }
                        else{ echo $val['size_master']; } ?></div></td>
                      </tr>
                      <?php } ?>
                     <?php 
                     //if(in_array($val['product_master'], array('NKL', 'ERG')) || $val['finding_cost']){  
                      if(in_array($val['product_master'], array('PST', 'SET', 'ERG'))){
                     /*   if($val['product_master'] == 'BAN'){
                        $screw_label = 'Pieces';                          
                        }else{
                          $screw_label = 'Screw';
                        }*/
                        ?>
                       <tr>
                        <td><label class=" control-label" for="inputEmail3"> Screw </label></td>
                        <td><div class=""><?php if($finding_cost_array==1){
                          end($new_master['finding_cost']);
                          $last_key_fc = key($new_master['finding_cost']);
                          foreach ($new_master['finding_cost'] as $fc_key => $fc_value) {
                             echo "<b>".implode(',', $fc_value)." = </b>".$fc_key;
                            if($last_key_fc!=$fc_key){
                            echo " || ";
                           }
                          }
                         }else { echo $val['finding_cost']; } ?></div></td>
                      </tr>
                       <?php }else if(in_array($val['product_master'], array('BAN'))) {?>
                          <tr>
                        <td><label class=" control-label" for="inputEmail3"> Pieces </label></td>
                        <td><div class=""><?php if($finding_cost_array==1){
                          end($new_master['finding_cost']);
                          $last_key_fc = key($new_master['finding_cost']);
                          foreach ($new_master['finding_cost'] as $fc_key => $fc_value) {
                              echo "<b>".implode(',', $fc_value)." = </b>".$fc_key;
                            if($last_key_fc!=$fc_key){
                            echo " || ";
                           }
                          }
                         }else { echo $val['finding_cost']; } ?></div></td>
                      </tr>
                         
                       <?php } ?>
                 <!--      <tr>
                        <td><label class="control-label" for="inputEmail3"><?=(!empty($val['weight_band'])) ? 'Weight Range' : 'Net Weight'; ?>  </label>
                          </td><td><span class="">-</span></td>
                        <td><div class="" style="white-space:nowrap"><?php if ($weight_band_array==1){
                          end($new_master['weight_band']);
                          $last_key_wb = key($new_master['weight_band']);
                          foreach ($new_master['weight_band'] as $wb_key => $wb_value) {
                           echo "<b>".$wb_key." = </b>".implode(',', $wb_value);
                           if($last_key_wb!=$wb_key){
                            echo " || ";
                           }
                          }
                         }else { echo $val['weight_band']; } ?></div></td>
                      </tr> -->
                    <!--     <tr>
                        <td><label class="control-label" for="inputEmail3">Avg Weight</label>
                          </td><td><span class="">-</span></td>
                        <td><div class="" style="white-space:nowrap"><?php if (!empty($val['cp_weight'])){
                          echo $val['cp_weight'];
                          
                         }else { echo $val['net_weight']; } ?></div></td>
                      </tr> -->

                        <tr>
                     
                          <td><label class="control-label" for="inputEmail3"><?=(!empty($val['cp_weight'])) ? 'Avg Weight' : 'Net Weight'; ?> </label></td>
                          <td>
                            <div class="" >
                              <?php if ($avg_weight_array==1){
                              end($new_master['cp_weight']);
                              $last_key_cw = key($new_master['cp_weight']);
                              foreach ($new_master['cp_weight'] as $cw_key => $cw_value) {
                               echo "<b>".implode(',', $cw_value)." = </b>".$cw_key;
                               if($last_key_cw!=$cw_key){
                                echo " || ";
                               }
                              }
                             }else { echo $val['cp_weight']; } ?></div>
                          </td>
                      </tr>
                      <?php if(!empty($val['tolerance_wt'])){
                        ?>
                      <tr>
                        <td>
                          <label class="control-label" for="inputEmail3">Weight Range</label>
                        </td>
                        <td><div class="">
                        <?php if ($avg_weight_array==1){
                          end($new_master['cp_weight']);
                          $last_key_cw = key($new_master['cp_weight']);
                            foreach ($new_master['cp_weight'] as $cw_key => $cw_value) {
                              $cp_wt=implode(',', $cw_value);
                              if(!empty($cw_key)){
                                  $from_wt_torlen= $cw_key-$val['tolerance_wt'];
                                  $to_wt_torlen=$cw_key+$val['tolerance_wt'];
                                  $tolerance_wt_range= number_format($from_wt_torlen , 2, '.', '') .' - '. number_format($to_wt_torlen , 2, '.', '');  
                              }

                               echo "<b>".$cp_wt." = </b>".$tolerance_wt_range;
                               if($last_key_cw!=$cw_key){
                                echo " || ";
                               }
                            }
                         }else { 
                                $from_wt_torlen= $val['cp_weight']-$val['tolerance_wt'];
                                                    $to_wt_torlen=$val['cp_weight']+$val['tolerance_wt'];
                                                    $tolerance_wt_range= number_format($from_wt_torlen , 2, '.', '') .' - '. number_format($to_wt_torlen , 2, '.', '');  
                            echo $tolerance_wt_range; } ?> 

                         </div>
                         </td>
                      </tr>
                      <?php }else if(!empty($val['tolerance_c_wt'])){

                                 $from_wt_torlen= $val['net_weight']-$val['tolerance_c_wt'];
                                 $to_wt_torlen=$val['net_weight']+$val['tolerance_c_wt'];
                                 $tolerance_wt_range= number_format($from_wt_torlen , 2, '.', '') .' - '. number_format($to_wt_torlen , 2, '.', '');


                        ?>
                      <tr>
                        <td><label class="control-label" for="inputEmail3">Weight Range</label></td>
                        <td><div class="" style="white-space:nowrap"><?php if (!empty($tolerance_wt_range)){
                          echo $tolerance_wt_range;
                          
                         }else { echo '-'; } ?></div></td>
                      </tr>
                      <?php } ?>
                      <?php
                        $dt = date("Y-m-d");
                        $order_date1 =  date( "d-m-Y", strtotime( "$dt" ) );
                        $due_date1 = date( "d-m-Y", strtotime( "$dt +12 day" ) );
                        ?>
                      <tr>
                        <td><label class=" control-label" for="inputEmail3"> Order Date </label></td>
                        <td><div class=""><?=$order_date1;?></div></td>
                      </tr>
                      <tr>
                        <td><label class=" control-label" for="inputEmail3"> Delivery Date </label></td>
                        <td><div class=""><?=$due_date1;?></div></td>
                      </tr> 
                       <tr><td colspan="3">
                      <?php
                      
                       if($val['barcode_path'] == "" || $val['barcode_path'] == null){
                             $val['barcode_path'] = $val['cp_barcode_path'];
                        }
                          $codes =explode(',', $val['barcode_path']);
                          $count = 1;
                         foreach ($codes as $key => $value) {
                       ?>
                            <div class="barcode_img" style="display: inline-block;"><img src="<?=UPLOAD_IMAGES?>barcode/<?=trim($value)?>"></div>
                          <?php
                          if($count == 3){
                             echo "<br>"; 
                             $count = 0; 
                          }
                         $count++;
                         }
                      ?>
                            </td></tr>
                      
                    </tbody> 
                  </table>
                </div>                  
            </div>

            <div class="clearfix"></div>
              <div class="row m-t-20 clearfix">
                <div class="col-sm-8 pull-right print_detail">
                  <div class="note karigar_name" style="font-weight:bold;">
                    <p>NO OTHER STAMP</p>
                    <p>Note : Without Xerox Design will not be accepted.</p>
                    <p>অন্য কোন স্ট্যাম্প</p>
                    <p>নোট: জেরক্স পণ্য ছাড়া গ্রহণ করা হবে না।
                    </p>
                  </div>                  
                </div>
              </div>


              <hr />
              <div class="clearfix"></div>


              <?php $i++; if($i%2==0){ ?> <pagebreak /><div class="pgbrk"></div> <?php } ?>
              <?php }
              //}else{?>
            <!--   <div class="" style="padding:20px; text-align:center"><p>Design Not Found !!</p></div> -->
              <?php// }?>
            </div>
            </pagebreak>
            </div>
          </form>
        </div>
      </div>
    </div>
  </div>
</div>
</div>
</div>
</div>
</div>
              