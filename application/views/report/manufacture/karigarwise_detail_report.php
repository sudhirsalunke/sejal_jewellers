<div class="content-page">
<div class="content">
   <div class="container">
      <div class="row">
         <div class="col-sm-12 card-box-wrap">
            <div class="card-box padmobile">
               <h4 class="header-title business_manager_header_title m-t-0 m-b-30 bmsmheader bmsmheaderabm"><span><?=$display_title;?></span>
               </h4>
               <form action="<?=ADMIN_PATH.'Manufacture_report/order_ageing/download'?>" method="post">
                    <!-- <button type="submit" class="btn btn-md btn-purple pull-right">Export</button> -->
               </form>
               <div class="clearfix"></div>
               <div class="table-rep-plugin">
                <div class="table-responsive">
                 <table class="table table-bordered">
                    <thead>
                      <th>#</th>
                      <th>Karigar Name</th>
                      <th>Parent Category</th>
                      <th>Balance Qty</th>
                      <th>Balance Weight</th>
                    </thead>
                
                 <tbody>
                 <?php $i=0; foreach ($result as $key => $value) { $i++;?>
                   <tr>
                    <td><span style="float:right"><?=$i?></span></td>
                     <td colspan="6"><b><?=$karigar_names[$key]?></b></td>
                   </tr>
                     <?php foreach ($value as $p_key => $p_value) { ?>
                       <tr>
                      <td colspan="2"></td>
                      <td><?=$parent_category_ids[$p_key]?></td>
                      <td><span style="float:right"><?=$p_value['bal_qty']?></span></td>
                      <td><span style="float:right"><?=$p_value['bal_weight'];?></span></td>
                   </tr>
                    <?php } ?>
                <?php } ?>
                </tbody>
                 </table>
                 </div>
                 </div>
               </div>
            </div>
         </div>
      </div>
   </div>
</div>
