<div class="content-page">
<div class="content">
   <div class="container">
      <div class="row">
         <div class="col-sm-12 card-box-wrap">
            <div class="panel panel-default">
              <div class="panel-heading">
               <h4 class="inlineBlock"><?=$display_title;?>
               </h4>
               <div class="pull-right btn-wrap2">
                <button type="submit" class="add_senior_manager_button btn btn-warning waves-effect w-md waves-light m-b-5 btn-md pull-right">Export</button>
                </div>
               </div>
               <div class="panel-body">
               <form action="<?=ADMIN_PATH.'Manufacture_report/order_status/download_detailed_report/'.$order_id?>" method="post">
                  <input type="hidden" name="search" id="search_textbox">
                   
               </form>
               <div class="clearfix"></div>
               <div class="table-rep-plugin table-responsive">
                 <table class="table custdatatable table-bordered " id="order_status_view">
                    <thead>
                       <tr>
                          <th class="col4">#</th>
                          <th class="col4">Order No</th>
                          <th class="col4">Order Name</th>
                          <th class="col4">Item Name</th>
                          <th class="col4">Weight Range</th>
                          <th class="col4">Issued Wt</th>
                          <th class="col4">Balance Wt.</th>
                          <th class="col4">Total Pcs</th>
                          <th class="col4">Balance Pcs</th>
                          <th class="col4">Delivery Date</th>
                          <th class="col4">Due in days</th>
                       </tr>
                    </thead>
                 </table>
                 </div>
               </div>
               </div>
            </div>
         </div>
      </div>
   </div>
</div>
