<div class="content-page">
<div class="content">
   <div class="container">
      <div class="row">
         <div class="col-sm-12 card-box-wrap">
            <div class="card-box padmobile">
               <h4 class="header-title business_manager_header_title m-t-0 m-b-30 bmsmheader bmsmheaderabm"><span><?=$display_title;?></span>
               </h4>
               <form action="<?=ADMIN_PATH.'Manufacture_report/stock_summary/download'?>" method="post">
                   <!--  <button type="submit" class="btn btn-md btn-purple pull-right">Export</button> -->
               </form>
               <div class="clearfix"></div>
               <div class="table-rep-plugin">
                <div class="table-responsive">
                 <table class="table custdatatable table-bordered">
                    <thead>
                          <th class="col4" >#</th>
                          <th class="col4" >Karigar Name</th>
                          <th class="col4" >Weight</th>
                          <th class="col4" >Total Qty</th>
                              <?php foreach ($result as $key => $value) {?>
                                <tr>
                                  <td><span style="float:right"><?=$key+1?></span></td>
                                  <td><?=$value['karigar_name']?></td>
                                  <td><span style="float:right"><?=$value['weight']?></span></td>
                                  <td><span style="float:right"><?=$value['total_qty']?></span></td>
                                </tr>
                                 
                      <?php } /*result*/ ?>
                       
                    </thead>
                 </table>
                 </div>
                 </div>
               </div>
            </div>
         </div>
      </div>
   </div>
</div>
