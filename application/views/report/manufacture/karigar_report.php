<div class="content-page">
<div class="content">
   <div class="container">
      <div class="row">
         <div class="col-sm-12 card-box-wrap">
            <div class="card-box padmobile">
               <h4 class="header-title business_manager_header_title m-t-0 m-b-30 bmsmheader bmsmheaderabm"><span><?=$display_title;?></span>
               </h4>
               <form action="<?=ADMIN_PATH.'Report/Karigar_report_manufacture/download'?>" method="post">
                    <input type="hidden" name="search" id="search_textbox">
                    <button type="submit" class="btn btn-md btn-purple pull-right">Export</button>
               </form>
               <div class="clearfix"></div>
               <div class="table-rep-plugin">
                 <table class="table custdatatable table-bordered " id="karigar_report_table">
                    <thead>
                     <?php
                          $data['heading']='karigar_report_table';
                          $this->load->view('master/table_header',$data);
                        ?>
                <!--        <tr>
                          <th class="col4">#</th>
                          <th class="col4">Karigar Name</th>
                          <th class="col4">Order Weight</th>
                          <th class="col4">Total Pcs</th>
                       </tr> -->
                    </thead>
                 </table>
                 </div>
               </div>
            </div>
         </div>
      </div>
   </div>
</div>
