<div class="content-page">
<div class="content">
   <div class="container">
      <div class="row">
         <div class="col-sm-12 card-box-wrap">
            <div class="panel panel-default">
               <div class="panel-heading">
                 <h4 class="inlineBlock"><?=$display_title;?>
                 </h4>
              
               </div>
               <div class="panel-body">
                <div class="clearfix"></div>
               <div class="table-rep-plugin table-responsive">
               <form name="daily_status" method="GET" action="<?= ADMIN_PATH ?>Daily_stock_summary_report">
                <?php if(isset($_GET['from'])){
                       $from_date=date('d-m-Y',strtotime($_GET['from']));
                  } 
                  if(isset($_GET['to'])){
                       $to_date=date('d-m-Y',strtotime($_GET['to']));
                  }
                  ?>
                        <div class="col-md-2">
                            <input type="text" name="from" value="<?= $from_date;?>" class="form-control order_name   datepickerInput datepicker-autoclose" placeholder="From Date">
                        </div>
                 
                        <div class="col-md-2">
                            <input type="text" name="to" value="<?= $to_date;?>" class="form-control     datepickerInput datepicker-autoclose" placeholder="To Date">

                            <small class="form-control-feedback error_msg" id="order_name_error"></small>
                        </div> 
                         <input type="hidden" name="department_id" id="department_id" value="<?=$_SESSION['department_id'];?>" class="form-control">
                          <div class="col-sm-2">
                 <input type="submit"  class="btn btn-primary pull-left " name="search" value="SEARCH">
                </div>
               
                </form>
                 <table class="table custdatatable table-bordered " id="daily_stock_report_table">
                    <thead>
                      <tr>
                          <th></th>
                          <th colspan="2" style="text-align: center" >Inward</th>
                          <th colspan="2" style="text-align: center" >Outward</th>
                      </tr>
                   
                       <tr>
                          <th class="col4">Product</th>
                          <th class="col4">Weight</th>
                          <th class="col4">Pcs</th>
                          <th class="col4">Weight</th>
                          <th class="col4">Pcs</th>
                          
                       </tr>
                    </thead>
           
                 </table>
                 </div>
               </div>
               </div>
            </div>
         </div>
      </div>
   </div>
</div>
