<div class="content-page">
<div class="content">
   <div class="container">
      <div class="row">
         <div class="col-sm-12 card-box-wrap">
            <div class="card-box padmobile">
               <h4 class="header-title business_manager_header_title m-t-0 m-b-30 bmsmheader bmsmheaderabm"><span><?=$display_title;?></span>
               </h4>
               <form action="<?=ADMIN_PATH.'Manufacture_report/stock_summary/download'?>" method="post">
                   <!--  <button type="submit" class="btn btn-md btn-purple pull-right">Export</button> -->
               </form>
               <div class="clearfix"></div>
               <div class="table-rep-plugin">
                <div class="table-responsive">
                 <table class="table custdatatable table-bordered">
                    <thead>
                      <tr>
                      <?php
                      $i=0;
                       foreach ($weight_range as $wr_key => $wr_value) { 
                        $key= $wr_value['id'];
                          ?>
                         <th class="col4" colspan="2"></th>
                          <th class="col4" colspan="2">Weight Range : <?=$wr_value['from_weight'].'-'.$wr_value['to_weight']?></th>
                       
                      
                       </tr>
                       <?php if($i==0){ ?>
                          <th class="col4">Parent Category</th>
                          <th class="col4">Codes</th>
                          <th class="col4" >Gross Weight</th>
                          <th class="col4" >Pcs</th>
                         
                      <?php } ?>
                              <?php foreach ($parent_categories as $pc_key => $pc_value) {
                                // if (@$result[$key][$pc_value['id']]['diff'] >=0) {
                                //   $clr="green";
                                // }else{
                                //   $clr="red";
                                // }
                               ?>
                                  <tr><td colspan="4"><b><?=$pc_value['name']?></b></td></tr>
                                  <?php foreach ($pc_value['codes'] as $pc_code_key => $pc_code_val) { ?>
                                  <tr>
                                    <td style="text-align: right"><?=$pc_code_key+1?>.</td>
                                    <td><span style="float:right"><?=$pc_code_val['code_name']?></td>
                                    <td><span style="float:right"><?=abs(@$result[$key][$pc_value['id']][$pc_code_val['id']]['gross_wt'])?></span></td>
                                    <td><span style="float:right"><?=abs(@$result[$key][$pc_value['id']][$pc_code_val['id']]['qty'])?></span></td>
                                    <!-- <td style="color:<?=$clr?>"><?=abs(@$result[$key][$pc_value['id']][$pc_code_val['id']]['diff'])?></td> -->
                                  </tr>
                                   <?php } ?>

                              <?php } /*parent categories*/?>
                      <?php } /*result*/ ?>
                       
                    </thead>
                 </table>
                 </div>
                 </div>
               </div>
            </div>
         </div>
      </div>
   </div>
</div>
