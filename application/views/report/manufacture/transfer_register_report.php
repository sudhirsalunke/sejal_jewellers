<div class="content-page">
<div class="content">
   <div class="container">
      <div class="row">
         <div class="col-sm-12 card-box-wrap">
            <div class="card-box padmobile">
               <h4 class="header-title business_manager_header_title m-t-0 m-b-30 bmsmheader bmsmheaderabm"><span><?=$display_title;?></span>
               </h4>
               <form action="<?=ADMIN_PATH.'Manufacture_report/stock_summary/download'?>" method="post">
                   <!--  <button type="submit" class="btn btn-md btn-purple pull-right">Export</button> -->
               </form>
               <div class="clearfix"></div>
               <div class="table-rep-plugin">
                <div class="table-responsive">
                 <table class="table custdatatable table-bordered">
                    <thead>
                          <th class="col4" >#</th>
                          <th class="col4" >Transfer Memo No</th>
                          <th class="col4" >Product Code</th>
                          <th class="col4" >Net Weight</th>
                          <th>Quantity</th>
                          <th class="col4" >Receipt date</th>
                              <?php $i=0; foreach ($result as $key => $val) { $i++;?>
                                <tr style="border-top-style: solid">
                                  <td><span style="float:right"><?=$i?></span></td>
                                  <td colspan="5"><?=$key?></td>
                                </tr>
                                <?php foreach ($val as $value) { ?>
                                 <tr>
                                    <td colspan="2"></td>
                                    <td><span style="float:right"><?=$value['product_code']?></span></td>
                                    <td><span style="float:right"><?=$value['net_wt']?></span></td>
                                    <td><span style="float:right"><?=$value['quantity']?></span></td>
                                    <td><span style="float:right"><?=$value['rec_date']?></span></td>
                                  </tr>
                                <?php } ?>
                              
                      <?php } /*result*/ ?>
                       
                    </thead>
                 </table>
                 </div>
                 </div>
               </div>
            </div>
         </div>
      </div>
   </div>
</div>
