<div class="content-page">
<div class="content">
   <div class="container">
      <div class="row">
         <div class="col-sm-12 card-box-wrap">
            <div class="card-box padmobile">
               <h4 class="header-title business_manager_header_title m-t-0 m-b-30 bmsmheader bmsmheaderabm"><span><?=$display_title;?></span>
               </h4>
               <form action="<?=ADMIN_PATH.'Manufacture_report/order_ageing/download'?>" method="post">
                    <!-- <button type="submit" class="btn btn-md btn-purple pull-right">Export</button> -->
               </form>
               <div class="clearfix"></div>
               <div class="table-rep-plugin">
                <div class="table-responsive">
                 <table class="table table-bordered">
                    <thead>
                      <th>#</th>
                      <th>Karigar Name</th>
                      <th>Accept</th>
                      <th>Reject</th>
                      <th>Total Qty</th>
                      <th>% of acceptance</th>
                    </thead>
                
                 <tbody>
                 <?php $i=0; foreach ($result as $key => $value) { $i++;?>
                  <tr>
                      <td><span style="float:right"><?=$i?></span></td>
                      <td><?=$value['karigar_name']?></td>
                      <td><?=$value['accept']?></td>
                      <td><?=$value['reject']?></td>
                      <td><span style="float:right"><?=$value['total_qty'];?></span></td>
                      <td><span style="float:right"><?=$value['percentages']?></span></td>
                   </tr>
                <?php } ?>
                </tbody>
                 </table>
                 </div>
                 </div>
               </div>
            </div>
         </div>
      </div>
   </div>
</div>
