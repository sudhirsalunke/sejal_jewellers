<div class="content-page">
<div class="content">
   <div class="container">
      <div class="row">
         <div class="col-sm-12 card-box-wrap">
            <div class="panel panel-default">
              <div class="panel-heading">
                <h4 class="inlineBlock"><span><?=$display_title;?></span></h4>
                <div class="pull-right m-t-5">
                  <form action="<?=ADMIN_PATH.'corporate_report/Overall_report/download'?>" method="post" name="filter_overall">
                <input type="hidden" name="search" id="search_textbox">
                   <!--  <div class="form-group">
                        <div class="col-sm-3">
                          <select name="corporate_filter" class="form-control search-input-select" id="corporate_id_filter">
                            <option value="">Select Corporate</option>
                            <?php foreach ($corporates as $c_value) { ?>
                              <option value="<?=$c_value['id']?>" <?=(@$_GET['status']==$c_value['id']) ? 'selected' : ''?>><?=$c_value['name']?></option>
                            <?php } ?>
                          </select>
                        </div>
                        <div class="col-sm-3">
                          <button type="button" onclick="overall_report_filter()" class="btn btn-info">Apply Filter</button>
                        </div>
                    </div>  -->
                     <button type="submit" class="add_senior_manager_button btn btn-warning waves-effect w-md waves-light btn-md">Export</button>                   
                </form>
                </div>
              </div>
              <div class="panel-body">
                <div class="clearfix"></div>
                <div class="table-rep-plugin table-responsive">
                  <table class="table custdatatable table-bordered" id="overall_report_table">
                    <thead>
                       <tr>
                          <th class="col4">#</th>
                          <th class="col4">Particular</th>
                          <th class="col4">Quantity</th>
                          <th class="col4">Weight</th>
                          <th class="col4"></th>
                       </tr>
                    </thead>
                    <tbody>
                    <?php $i=0; foreach ($result as $key => $value) { $i++; ?>
                      <tr>
                        <td><span style="float:right"><?=$i;?></span></td>
                        <td><?=ucfirst(str_replace('_',' ',$key))?></td>
                        <td><span style="float:right"><?=$value['quantity']?></span></td>
                        <td><span style="float:right"><?=round(@$value['gross_weight'],3)?></span></td>
                        <td >
                         <a style="float: right" class="btn btn-sm btn-warning" href="<?=ADMIN_PATH.'corporate_report/Overall_report/show/'.strtolower($key)?>">View</a>
                        </td>
                      </tr>
                      <?php } ?>
                    </tbody>
                  </table>
                </div>
              </div>
                
            </div>
            </div>
         </div>
      </div>
   </div>
</div>
