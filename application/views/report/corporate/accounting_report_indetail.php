<div class="content-page">
<div class="content">
   <div class="container">
      <div class="row">
         <div class="col-sm-12 card-box-wrap">
            <div class="panel panel-default">
              <div class="panel-heading">
                <h4 class="inlineBlock"><span><?=$display_title;?></span></h4>
                <!--   <form action="<?=ADMIN_PATH.'corporate_report/Accounting_report/download'?>" method="post">
                <input type="hidden" name="search" id="search_textbox">
                   
                    <button type="submit" class="btn btn-md btn-purple pull-right">Export</button>
               </form> -->
              </div>
              <div class="panel-body">
                <div class="clearfix"></div>
                  <div class="table-rep-plugin table-responsive">
                    <table class="table custdatatable table-bordered" id="accounting_report_table_indetail">
                      <thead>
                        <tr>
                          <th class="col4">#</th>
                          <th class="col4">Category</th>
                          <th class="col4">Gross Weight</th>
                          <th class="col4">Stone</th>
                          <th class="col4">Net Weight</th>
                          <th class="col4">Touch</th>
                          <th class="col4">Purity</th>
                          <th class="col4">Gold</th>
                          <th class="col4">Gold Amount</th>
                          <th class="col4">Lab</th>
                          <th class="col4">Lab Amount</th>
                          <th class="col4">Total Amount</th>
                          <th>Date</th>

                       </tr>
                    </thead>
                    <tbody>
                      <?php foreach ($result as $key => $value) { ?>
                        <tr>
                          <td><span style="float:right"><?=$key+1?></span></td>
                          <td><?=$value['category_name']?></td>
                          <td><span style="float:right"><?=$value['gr_wt']?></span></td>
                          <td><span style="float:right"><?=$value['stone']?></span></td>
                          <td><span style="float:right"><?=$value['net_wt']?></span></td>
                          <td><span style="float:right"><?=$value['touch']?></span></td>
                          <td><span style="float:right"><?=$value['purity']?></span></td>
                          <td><span style="float:right"><?=$value['gold']?></span></td>
                          <td><span style="float:right"><?=$value['gold_amt']?></span></td>
                          <td><span style="float:right"><?=$value['lab']?></span></td>
                          <td><span style="float:right"><?=$value['lab_amt']?></span></td>
                          <td><span style="float:right"><?=$value['total_amt']?></span></td>
                          <td><span style="float:right"><?=date('d-m-Y',strtotime($value['created_at']));?></span></td>

                        </tr>
                   <?php } ?>
                  
                      </tbody>
                    </table>
                  </div>
              </div>
            </div>
            </div>
         </div>
      </div>
   </div>
</div>
