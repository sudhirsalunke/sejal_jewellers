<div class="content-page">
<div class="content">
   <div class="container">
      <div class="row">
         <div class="col-sm-12 card-box-wrap">
            <div class="panel panel-default">
              <div class="panel-heading">
                <h4 class="inlineBlock"><span><?=$display_title;?></span></h4>
                <div class="pull-right m-t-5">
                  <form action="<?=ADMIN_PATH.'corporate_report/jewerly_report/download'?>" method="post">   <input type="hidden" name="search" id="search_textbox">
                    <button type="submit" class="add_senior_manager_button btn btn-warning waves-effect w-md waves-light  btn-md">Export</button>
                  </form>
                </div>                
              </div>
              <div class="panel-body">                
                <div class="clearfix"></div>
                <div class="table-rep-plugin table-responsive">
                  <table class="table custdatatable table-bordered" id="jewerly_report_table">
                    <thead>
                      <?php
                          $data['heading']='jewerly_report_table';
                          $this->load->view('master/table_header',$data);
                        ?>
                     <!--   <tr>
                          <th class="col4">#</th>
                          <th class="col4">Corporate</th>
                          <th class="col4">Stock In-Hand</th>
                          <th class="col4">Order Receipts</th>
                          <th class="col4">Order In-Progress</th>
                          <th class="col4">Order Pending</th>
                          <th>Metal Received</th>
                          <th>Metal Required</th>
                       </tr> -->
                    </thead>
                  </table>
                </div>
              </div>               
            </div>
            </div>
         </div>
      </div>
   </div>
</div>
