<div class="content-page">
  <div class="content">
    <div class="container">
      <div class="row">
        <div class="col-sm-12 card-box-wrap">
          <div class="">
            <div class="card-box padmobile dash-board">
              <h4 class="m-b-20 bmsmheader bmsmheaderabm"><span>Corporate Reports</span></h4>              
            </div>
            <div class="">
              <div class="row">              
                <div class="col-md-12 col-xs-12 section-3 product-box marg_top-bottom no_pad">
                   <div class="col-lg-3 col-md-3 col-sm-6" data-aos="fade-down" data-aos-easing="linear" data-aos-delay="400" data-aos-duration="800">
                    <a href="<?= ADMIN_PATH.'corporate_report/Jewerly_report' ?>">
                      <div class="card-box dash-box box-shadow orange_box dash_box_content text-center">
                       
                        <div class="widget-chart-1 white_color_font">
                         <!--  <div class="widget-detail-1"> -->
                            <h4 class="">jewellery Report</h4>
                            <h2 class="number-size">1</h2>
                            <span class="label label-gray">Report</span>
                          <!-- </div> -->
                        </div>
                      </div>
                    </a>
                  </div><!--end col-->
                  <div class="col-lg-3 col-md-3 col-sm-6" data-aos="fade-down" data-aos-easing="linear" data-aos-delay="500"  data-aos-duration="800">
                    <a href="<?= ADMIN_PATH.'corporate_report/Accounting_report' ?>">
                      <div class="card-box dash-box box-shadow green_box dash_box_content text-center">
                       
                        <div class="widget-chart-1 white_color_font">
                         <!--  <div class="widget-detail-1"> -->
                             <h4 class="">Accounting Report</h4>
                            <h2 class="number-size">1</h2>
                            <span class="label label-gray">Report</span>
                          <!-- </div> -->
                        </div>
                      </div>
                    </a>
                  </div><!--end col-->
                  <div class="col-lg-3 col-md-3 col-sm-6"  data-aos="fade-down"
     data-aos-easing="linear" data-aos-delay="600" 
     data-aos-duration="800">
                    <a href="<?= ADMIN_PATH.'corporate_report/Overall_report' ?>">
                      <div class="card-box dash-box box-shadow yellow_box dash_box_content text-center">
                       
                        <div class="widget-chart-1 white_color_font">
                         <!--  <div class="widget-detail-1"> -->
                             <h4 class="">Overall Report</h4>
                            <h2 class="number-size">1</h2>
                            <span class="label label-gray">Report</span>
                          <!-- </div> -->
                        </div>
                      </div>
                    </a>
                  </div><!--end col-->
                </div>
              </div>
            </div>            
          </div>
        </div>
      </div><!-- end row -->
    </div><!-- end container -->
  </div><!-- end content -->
</div><!-- end content-page -->
