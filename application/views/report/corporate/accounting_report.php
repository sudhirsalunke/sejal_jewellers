<div class="content-page">
<div class="content">
   <div class="container">
      <div class="row">
          <div class="col-sm-12 card-box-wrap">
            <div class="panel panel-default">
              <div class="panel-heading">
                <h4 class="inlineBlock"><span><?=$display_title;?></span></h4>
                <div class="pull-right m-t-5">
                  <form action="<?=ADMIN_PATH.'corporate_report/Accounting_report/download'?>" method="post">
                    <button type="submit" class="add_senior_manager_button btn btn-warning waves-effect w-md waves-light btn-md">Export</button>
                  </form> 
                </div>             
              </div>
              <div class="panel-body">
                
                  <input type="hidden" name="search" id="search_textbox">
                  <div class="form-group">
                      <div class="col-sm-3">
                        <select name="type_filter" class="form-control search-input-select" filtercol="1">
                          <option value="">Select Type</option>
                          <option value="1">Karigar</option>
                          <option value="2">Customer</option>
                        </select>
                      </div>
                  </div>
                  
                <div class="clearfix"></div>
                <div class="table-rep-plugin table-responsive">
                  <table class="table custdatatable table-bordered" id="accounting_report_table">
                    <thead>
                    <?php
                          $data['heading']='accounting_report_table';
                          $this->load->view('master/table_header',$data);
                        ?>
                      <!--  <tr>
                          <th class="col4">#</th>
                          <th class="col4">Type</th>
                          <th class="col4">Name</th>
                          <th class="col4">Issue Weight</th>
                          <th class="col4">Receipt Weight</th>
                          <th class="col4">Balance Weight</th>
                       </tr> -->
                    </thead>
                  </table>
                </div>                
              </div>
            </div>
          </div>
         </div>
      </div>
   </div>
</div>
