<div class="content-page">
<div class="content">
   <div class="container">
      <div class="row">
         <div class="col-sm-12 card-box-wrap">
            <div class="panel panel-default">
              <div class="panel-heading">
                <h4 class="inlineBlock"><span><?=$display_title;?></span></h4>
                <div class="pull-right">
                  <!-- <form action="<?=ADMIN_PATH.'corporate_report/Overall_report/download'?>" method="post" name="filter_overall">
                <input type="hidden" name="search" id="search_textbox">
                    
                     <button type="submit" class="btn btn-md btn-purple pull-right">Export</button>
                   
               </form> -->
                </div>
              </div>
              <div class="panel-body">
                <div class="clearfix"></div>
                <div class="table-rep-plugin table-responsive">
                  <input type="hidden" id="report_name_overall" value="<?=$report_name?>">
                  <table class="table table-bordered" id="overall_report_view_table">
                    <thead>
                       <tr>
                       <?php
                        $overall_view_table_headers=overall_view_table_headers($report_name);
                        foreach ($overall_view_table_headers as $key => $value) { ?>
                          <th class="col<?=$key?>"><?=$value?></th>
                       <?php } ?>
                       </tr>
                    </thead>
                    <tbody>
                    </tbody>
                  </table>
                </div>
              </div>               
                
               </div>
            </div>
         </div>
      </div>
   </div>
</div>
