

<div class="content-page">
<div class="content">
   <div class="container">
      <div class="row">
         <div class="col-sm-12 card-box-wrap">
            <div class="panel panel-default">
               <div class="panel-heading">
                <h4 class="inlineBlock"><span><?= $display_title;?></span></h4>                
              </div>
               <div class="panel-body">
                 <form name="daily_status" method="GET" action="<?= ADMIN_PATH ?>Order_report/sales_purchase_report">
                <?php 
                    if(isset($_GET['from'])){
                       $from_date=date('d-m-Y',strtotime($_GET['from'])); 
                    }
                    if(isset($_GET['to'])){
                       $to_date=date('d-m-Y',strtotime($_GET['to'])); 
                    }
                    if(isset($_GET['type'])){
                       $type_wise=$_GET['type']; 
                    }
                   ?>
                  <div class="row">
                <div class="col-sm-3">  
                 <input class="form-control txtfilter datepickerInput delievery_date" type="text"  readonly="" placeholder="Select Date Range" name="from" style="width:100%" value="<?=@$from_date?>" >
                 </div>
                 <div class="col-sm-3">  
                 <input class="form-control txtfilter datepickerInput delievery_date" type="text"  readonly="" placeholder="Select Date Range" name="to" style="width:100%" value="<?=@$to_date?>"  >
                 </div>
                 
                 <div class="col-sm-3">
                 <input type="submit"  class="btn btn-primary pull-left " name="search" value="SEARCH">
                </div>
                </div>
                </form>
              
               <div class="table-rep-plugin">
                  <div class="b-0 scrollhidden table-responsive">
               <table class="table custdatatable table-bordered " id="sales_purchase_report">
                    <thead>
                      <tr>
                          <th>Part/Karigar</th>
                          <th colspan="2" style="text-align: center" >Receipt</th>
                          <th colspan="2" style="text-align: center" >Issue</th>
                      </tr>
                   
                       <tr>
                          <th></th> 
                          <th class="col4">Net Weight</th>
                          <th class="col4">Quantity</th>
                          <th class="col4">Net Weight</th>
                          <th class="col4">Quantity</th>
                          
                          
                       </tr>
                    </thead>

                     <?php if (!empty($result)) { ?>
                            <tfoot>
                                <tr>
                                    <td class="text-right" colspan=""><b>Total</b></td>
                                    <td class="text-right"><b><?= overwrite_number_format($total_receipt_net_wt, 3) ?></b></td>
                                    <td class="text-right"><b><?= $total_receipt_quantity ?></b></td>
                                    <td class="text-right"><b><?= overwrite_number_format($total_issue_net_wt, 3) ?></b></td>
                                    <td class="text-right"><b><?= $total_issue_quantity ?></b></td>
                                    <td></td>
                                </tr>
                                <tr>
                                    <td class="text-right" colspan=""><b>Balance</b></td>
                                    <td class="text-right"><b><?= overwrite_number_format($balance_wt, 3) ?></b></td>
                                    <td class="text-right"><b><?= $balance_qty ?></b></td>
                                    <td></td>
                                    <td></td>
                                </tr>
                            </tfoot>
                        <?php } ?>           
                 </table>
               </div>
               </div>
               </div>
            </div>
         </div>
      </div>
   </div>
</div>