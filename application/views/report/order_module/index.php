<div class="content-page">
<div class="content">
   <div class="container">
      <div class="row">
        <div class="panel-heading ">
         <h4 class="inlineBlock"><?=$display_title;?>
         </h4>
        </div>
      </div>
      <div class="row">
         <div class="col-sm-12 card-box-wrap">
            <div class="panel panel-default">
              
             
               <div class="clearfix"></div>
               <div class="panel-body">
               <div class="table-rep-plugin table-responsive">
                 <table class="table custdatatable table-bordered " id="karigar_report_table">
                    <thead>
                       <tr>
                          <th class="col4">#</th>
                          <th class="col4">Report Name</th>
                          <th class="col4"></th>
                       </tr>
                    </thead>
                    <tbody>
                      <?php foreach ($all_reports as $key => $value) { ?>
                       <tr>
                          <td><span style="float:right"><?=$key+1?></span></td>
                          <td><?=$value['name']?></td>
                          <td><a href="<?=ADMIN_PATH.$value['path']?>" class="btn btn-link view_link btn-sm large"> View</a></td>
                       </tr>
                     <?php } ?>
                      
                    </tbody>
                 </table>
                 </div>
               </div>
               </div>
            </div>
         </div>
      </div>
   </div>
</div>
