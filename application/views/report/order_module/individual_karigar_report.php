<div class="content-page">
<div class="content">
   <div class="container">
      <div class="row">
         <div class="col-sm-12 card-box-wrap">
            <div class="panel panel-default">
               <div class="panel-heading">
                <h4 class="inlineBlock"><span><?= $display_title;?></span></h4>                
              </div>
               <div class="panel-body">
               <div class="btnSection">
               
               <button onclick="history.back();" class="btn btn-md btn-default waves-effect waves-light pull-right" >BACK</button>
               </div>
               <h3>Karigar Name : <?=@$karigar_name?></h3>
               <div class="table-rep-plugin">
                  <div class="b-0 scrollhidden table-responsive">
               <table class="table table-bordered custdatatable" id="individual_karigar_order_tracking_tbl" value="<?=$karigar_id?>" >
                  <thead>
                      <?php
                          $data['heading']='individual_karigar_order_tracking_tbl';
                          $this->load->view('master/table_header',$data);
                        ?>
                  </thead>
               </table>
               </div>
               </div>
               </div>
            </div>
         </div>
      </div>
   </div>
</div>