

<div class="content-page">
<div class="content">
   <div class="container">
      <div class="row">
         <div class="col-sm-12 card-box-wrap">
            <div class="panel panel-default">
               <div class="panel-heading">
                <h4 class="inlineBlock"><span><?= $display_title;?></span></h4>                
              </div>
               <div class="panel-body">
                 <form name="daily_status" method="GET" action="<?= ADMIN_PATH ?>Order_report/delivery_report_in_quantity">
                <?php 
                    if(isset($_GET['from'])){
                       $from_date=date('d-m-Y',strtotime($_GET['from'])); 
                    }
                    if(isset($_GET['to'])){
                       $to_date=date('d-m-Y',strtotime($_GET['to'])); 
                    }
                    if(isset($_GET['type'])){
                       $type_wise=$_GET['type']; 
                    }
                   ?>
                  <div class="row">
                <div class="col-sm-3">  
                 <input class="form-control txtfilter datepickerInput delievery_date" type="text"  readonly="" placeholder="Select Date Range" name="from" style="width:100%" value="<?=@$from_date?>" >
                 </div>
                 <div class="col-sm-3">  
                 <input class="form-control txtfilter datepickerInput delievery_date" type="text"  readonly="" placeholder="Select Date Range" name="to" style="width:100%" value="<?=@$to_date?>"  >
                 </div>
                  <div class="col-sm-3">  
                 <select class="form-control txtfilter" name="type" style="width:100%" >
                   <option value="">Select Type</option>
                   <option value="department" <?php if($type_wise == "department"){ echo "selected";} ?> >Department Wise</option>
                   <option value="category" <?php if($type_wise == "category"){ echo "selected";} ?>>Category Wise</option>
                 </select>
                 </div>
                 <div class="col-sm-3">
                 <input type="submit"  class="btn btn-primary pull-left " name="search" value="SEARCH">
                </div>
                </div>
                </form>
              
               <div class="table-rep-plugin">
                  <div class="b-0 scrollhidden table-responsive">
               <table class="table table-bordered custdatatable" id="delivery_report_in_qty_tbl" >
                  <thead>
                      <?php
                          $data['heading']='delivery_report_in_qty_tbl';
                          $this->load->view('master/table_header',$data);
                        ?>
                  </thead>
               </table>
               </div>
               </div>
               </div>
            </div>
         </div>
      </div>
   </div>
</div>