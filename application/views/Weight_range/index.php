<div class="content-page">
<div class="content">
   <div class="container">
      <div class="row">
         <div class="col-sm-12 card-box-wrap">
            <div class="panel panel-default">
              <div class="panel-heading">
                <h4 class="inlineBlock"><?= strtoupper($page_title);?></h4>
                <a href="<?= ADMIN_PATH?>Weight_range/create" class="pull-right single-add btn-wrap m-t-5"><button  type="button" class="add_senior_manager_button btn btn-primary waves-effect w-md waves-light btn-md single-add">ADD Weight </button></a>
              </div>
              <div class="panel-body">
                <div class="clearfix"></div>
                <div class="table-rep-plugin">
                  <div class="table-responsive b-0 scrollhidden">
                    <table class="table table-bordered custdatatable" id="weights">
                      <thead>
                         <?php
                          $data['heading']='weights';
                          $this->load->view('master/table_header',$data);
                        ?>
                        <!--  <tr>
                             <th class="col4">From</th>
                             <th class="col4">To</th>
                            <th class="col3"></th>
                         </tr> -->
                      </thead>
                    </table>
                  </div>
                </div>
              </div>
            </div>
         </div>
      </div>
   </div>
</div>