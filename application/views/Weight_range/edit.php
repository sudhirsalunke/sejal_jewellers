<div class="content-page">
<div class="content">
   <div class="container">
      <div class="row">
         <div class="col-sm-6 card-box-wrap">
            <div class="panel panel-default">
              <div class="panel-heading frmHeading">
                <h4 class="inlineBlock"><span><?= strtoupper($page_title);?></span></h4>                
              </div>
              <div class="panel-body">
                <div class="col-lg-12">
                  <form name="category" id="Weight_range" role="form" class="form-horizontal" method="post">
                   <input type="hidden" name="Weight_range[encrypted_id]" value="<?= $weight_range['encrypted_id'] ?>">
                     <div class="form-group">
                        <label class="col-sm-3 control-label" for="inputEmail3"> From <span class="asterisk">*</span></label>
                        <div class="col-sm-9">
                           <input type="text" placeholder="From" id="from" class="form-control" name="Weight_range[from]" value="<?= $weight_range['from_weight'] ?>">
                           <span class="text-danger" id="from_error"></span>
                        </div>
                     </div>
                     <div class="form-group">
                        <label class="col-sm-3 control-label" for="inputEmail3"> To </label>
                        <div class="col-sm-9">
                           <input type="text" placeholder="To" id="to" class="form-control" name="Weight_range[to]" value="<?= $weight_range['to_weight'] ?>">
                           <span class="text-danger" id="to_error"></span>
                        </div>
                     </div>
                    <div class="btnSection">                     
                      
                      <button class="btn btn-default waves-effect waves-light m-l-5 btn-md" onclick="window.location='<?= ADMIN_PATH?>Weight_range'" type="reset">back</button>

                      <button class="btn btn-success pull-right waves-effect waves-light btn-md" name="commit" type="button" onclick="Update_weight_range(); ">SAVE</button>                    
                    </div>
                  </form>
                </div>
              </div>
               <div class="clearfix"></div>
            </div>
         </div>
      </div>
   </div>
</div>