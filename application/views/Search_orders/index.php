<div class="content-page">
<div class="content">
   <div class="container">
      <div class="row">
         <div class="col-sm-6 card-box-wrap">
            <div class="card-box padmobile">
              <div class="panel-heading frmHeading">
                <h4 class="inlineBlock"><span>Upload New Order</span></h4>                
              </div>
               <div class="col-lg-12">
                <form name="serach_text" id="serach_text" role="form" class="form-horizontal" method="post">
                       
                        <div class="form-group">
                         <label class="col-sm-4 control-label" style="text-align: right;" for="inputEmail3"> Search Corporate <span class="asterisk">*</span></label>
                          <div class="col-sm-8">
                          <select class="form-control" name="corporate" id="corporate_wise_excel">
                          <option value="0">Please select corporate</option>
                            <?php
                              foreach ($corporates as $key => $value) {
                            ?>
                              <option value="<?= $value['id']?>"><?= $value['name']?></option>
                            <?php } ?>  
                            ?>
                          </select>
                          <span class="text-danger" id="corporate_error"></span>
                          </div>

                        </div>
                    
                  <div class="form-group col-lg-12" id="download_template">
                      <a class="col-sm-offset-4 col-sm-4" href="<?=ADMIN_PATH.'assets/sample/caratlane_template.xlsx'?>">Download Template</a>
                  </div>                 
                         <div class="form-group search_order_div">
                        <label class="col-sm-4 control-label" style="text-align: right;" for="inputEmail3"> Search Order No. <span class="asterisk">*</span></label>
                        <div class="col-sm-8">
                          <input type="text" placeholder="Search Order No" id="serach_order" class="form-control" name="serach_text">
                             <span class="text-danger" id="name_error"></span>
                        </div>                       

                     </div>

                     <div class="col-sm-12 text-center" style="margin-bottom: 20px;">
                           <button class="btn btn-info waves-effect waves-light btn-md" name="commit" type="button" onclick="search_order();">
                           Search
                           </button>
                        </div>
                  </form>
               </div>
               <div class="clearfix"></div>
            </div>
         </div>
         <div class="clearfix"></div>
          <div class="form-group m-t-20 m-b-20" style="text-align:right;">
              <label class="col-sm-7 control-label" for="inputEmail3"> 
              <span id="already_exist" style="display:none; "> </span></label>
          </div>
         <div class="clearfix"></div>
         <div class="col-sm-6 card-box-wrap c_class" id="order_found" style="display: none;">
            <div class="card-box padmobile clearfix">
              <div class="col-lg-12">
                  <form name="serach_text" id="serach_found" role="form" class="form-horizontal" method="post">
                    <div class="form-group">
                        <label class="col-sm-7 order_found control-label" for="inputEmail3">Your order No. was Found</label>
                    </div>
                    <br />
                     <div class="form-group">
                        <label class="col-sm-4 control-label" for="inputEmail3"> Order Number <span class="asterisk">*</span></label>
                        <div class="col-sm-4 order_id">
                          <input type="hidden" name="Order[order_id]">
                        </div>
                     </div>
                     <div class="form-group">
                        <label class="col-sm-4 control-label" for="inputEmail3"> Date <span class="asterisk">*</span></label>
                        <div class="col-sm-4 date">
                          <input type="hidden" name="Order[date]">
                        </div>
                     </div>
                     <div class="form-group">
                        <label class="col-sm-4 control-label" for="inputEmail3"> Corporate <span class="asterisk">*</span></label> 
                        <div class="col-sm-4 corporate">
                          <input type="hidden" name="Order[corporate]">
                        </div>
                     </div>
                     <div class="form-group">
                        <label class="col-sm-4 control-label" for="inputEmail3"> Gross Weight <span class="asterisk">*</span></label>
                        <div class="col-sm-4 gross_weight">
                          <input type="hidden" name="Order[gross_weight]">
                        </div>
                     </div>
                     <div class="form-group">
                        <label class="col-sm-4 control-label" for="inputEmail3"> Net Weight <span class="asterisk">*</span></label>
                        <div class="col-sm-4 net_weight">
                          <input type="hidden" name="Order[net_weight]">
                        </div>
                     </div>
                     <div class="form-group">
                        <label class="col-sm-4 control-label" for="inputEmail3"> Less Weight <span class="asterisk">*</span></label>
                        <div class="col-sm-4 less_weight">
                          <input type="hidden" name="Order[less_weight]">
                        </div>
                     </div>
                     <div class="form-group">
                        <label class="col-sm-4 control-label" for="inputEmail3"> Uploads Design <span class="asterisk">*</span></label>
                       <div class="col-sm-4">
                          <input type="file" name="file" class="form-control" id="file" >
                        </div>
                     </div>
                    <div class="form-group">
                     <div class="col-sm-12 texalin text-center">
                         <button class="btn btn-primary waves-effect waves-light  btn-md" name="commit" type="button" onclick="Upload_products('serach_found')">
                         Upload Products
                         </button>
                     </div>
                  </div>
                  </form>
                </div>
              </div>    
         </div>
         <div class="clearfix"></div>
         <div class="col-sm-6 card-box-wrap c_class" id="order_not_found" style="display: none;">
            <div class="card-box padmobile whiteBg clearfix">
                 <div class="col-lg-12">
                  <form name="serach_text" id="serach_not_found" role="form" class="form-horizontal" method="post">
                    <div class="form-group order_not_found_text_div">
                        <label class="col-sm-7 order_not_found control-label" for="inputEmail3">Your order No. was not Found</label>
                    </div>
                    <br />
                     <div class="form-group order_txt">
                        <label class="col-sm-4 control-label" for="inputEmail3"> Order Number <span class="asterisk">*</span></label>
                        <div class="col-sm-8 order_id">
                        <input type="hidden" name="Order[order_id]">
                        <span class="text-danger" id="order_id_error"></span>
                        </div>
                     </div>
                     <div class="form-group">
                        <label class="col-sm-4 control-label" for="inputEmail3"> Date <span class="asterisk">*</span></label>
                        <div class="col-sm-8">
                          <input type="text" value="" required="" id="datepickerInput_search" name="Order[date]" class="form-control datepickerInput" data-provide="datepicker"  placeholder="Select Month" data-date-start-date="-7d" data-date-end-date="0d">
                          <span class="text-danger" id="date_error"></span>
                        </div>

                     </div>
                     <input type="hidden" name="Order[corporate]" id="corporate_selected_id">
                    <!--  <div class="form-group">
                        <label class="col-sm-4 control-label" for="inputEmail3">  <span class="asterisk"></span></label>
                        <div class="col-sm-4">
                          
                        <select class="form-control" name="Order[corporate]">
                          <?php
                            foreach ($corporates as $key => $value) {
                          ?>
                            <option value="<?= $value['id']?>"><?= $value['name']?></option>
                          <?php } ?>  
                          ?>
                        </select> 
                        <span class="text-danger" id="corporate_error"></span>
                        </div>
                     </div> -->
                     <div class="form-group">
                        <label class="col-sm-4 control-label" for="inputEmail3"> Gross Weight <span class="asterisk">*</span></label>
                        <div class="col-sm-8">
                          <input type="text" placeholder="Enter Gross Weight" id="gross_weight" class="form-control cal_weight" name="Order[gross_weight]">
                          <span class="text-danger" id="gross_weight_error"></span>
                        </div>
                      </div>
                        <div class="form-group">
                        <label class="col-sm-4 control-label" for="inputEmail3"> Less Weight <span class="asterisk">*</span></label>
                        <div class="col-sm-8">
                          <input type="text" placeholder="Enter Less Weight" id="less_weight" class="form-control cal_weight" name="Order[less_weight]" value="0">
                           <span class="text-danger" id="less_weight_error"></span>
                        </div>
                       
                     </div>
                     <div class="form-group">
                        <label class="col-sm-4 control-label" for="inputEmail3"> Net Weight <span class="asterisk">*</span></label>
                        <div class="col-sm-8">
                          <input type="text" placeholder="Enter Net Weight" id="net_weight" class="form-control" name="Order[net_weight]" readonly> 
                          <span class="text-danger" id="net_weight_error"></span>
                        </div>
                        
                     </div>
                   
                     <div class="form-group">
                        <label class="col-sm-4 control-label" for="inputEmail3"> Uploads Design <span class="asterisk">*</span></label>
                        <div class="col-sm-8">
                          <input type="file" name="file" class="form-control" id="file" >
                           <span class="text-danger" id="file_error"></span>
                        </div>
                       
                     </div>
                    <div class="form-group">
                     <div class="col-sm-12 texalin text-center">
                         <button class="btn btn-primary waves-effect waves-light  btn-md" name="commit" type="button" onclick="Upload_products('serach_not_found',this)">
                         Upload Products
                         </button>
                     </div>
                  </div>
                   
                    <div class="col-lg-offset-4 errors col-lg-6">
                    </div>
                  </form>

              </div>   

         </div>

               
      </div>
   </div>
</div>
