  <div class="content-page">
<div class="content">
   <div class="container">
      <div class="row">
         <div class="col-sm-6 card-box-wrap">
            <div class="panel panel-default">
              <div class="panel-heading frmHeading">
               <h4 class="inlineBlock"><span>Add Product Category</span></h4>                
              </div>
              <div class="panel-body">
                 <div class="col-lg-12">
                    <form name="parent_category" id="parent_category" role="form" class="form-horizontal" method="post">
                      <div class="form-group">
                        <label class="col-sm-5 control-label" for="inputEmail3"> Enter Product Category <span class="asterisk">*</span></label>
                        <div class="col-sm-7">
                           <input type="text" placeholder="Enter Parent Category" class="form-control" name="parent_category[name]">
                           <span class="text-danger" id="name_error"></span>
                        </div>
                      </div>
                      <div class="form-group">
                        <label class="col-sm-5 control-label" for="inputEmail3"> Select Parent Product Category <span class="asterisk">*</span></label>
                        <div class="col-sm-7">
                          <select name="parent_category[category_id]" class="form-control">
                            <option value="0">Please Select Parent Product Category</option>
                            <?php 
                              foreach ($categories as $key => $value) {
                                ?>
                                  <option value="<?= $value['id']?>"><?= $value['name']?></option>
                                <?php
                              }
                            ?>
                          </select> 
                          <span class="text-danger" id="category_id_error"></span>
                        </div>
                      </div>
                <div class="form-group">
                        <label class="col-sm-5 control-label" for="inputEmail3">Kundan category <span class="asterisk"></span></label>
                        <div class="col-sm-7">
                         <select name="parent_category[kundan_category]" class="form-control">
                            <option value="0">Please Select Kundan category</option>
                            <?php 
                              foreach ($product_category_rate as $key => $value) {
                                ?>
                                  <option value="<?= $value['percentage']?>"><?= $value['name']?></option>
                                <?php
                              }
                            ?>
                          </select> 
                          <span class="text-danger" id="category_id_error"></span>
                        </div>
                           <span class="text-danger" id="name_error"></span>
                        </div>
                 <div class="form-group">
                        <label class="col-sm-5 control-label" for="inputEmail3">stone category <span class="asterisk"></span></label>
                        <div class="col-sm-7">
                              <select name="parent_category[stone_category]" class="form-control">
                            <option value="0">Please Select Kundan category</option>
                            <?php 
                              foreach ($product_category_rate as $key => $value) {
                                ?>
                                  <option value="<?= $value['percentage']?>"><?= $value['name']?></option>
                                <?php
                              }
                            ?>
                          </select> 
                           <span class="text-danger" id="name_error"></span>
                        </div>
                </div> 
                 <div class="form-group">
                        <label class="col-sm-5 control-label" for="inputEmail3">black beads category <span class="asterisk"></span></label>
                        <div class="col-sm-7">
                            <select name="parent_category[black_beads_category]" class="form-control">
                            <option value="0">Please Select black beads category</option>
                            <?php 
                              foreach ($product_category_rate as $key => $value) {
                                ?>
                                  <option value="<?= $value['percentage']?>"><?= $value['name']?></option>
                                <?php
                              }
                            ?>
                          </select> 
                           <span class="text-danger" id="name_error"></span>
                        </div>
                </div>
                      <div class="form-group">
                        <label class="col-sm-5 control-label" for="inputEmail3"> Add Code <span class="asterisk">*</span></label>
                        <div class="col-sm-7">
                           <input type="text" placeholder="Enter Code" class="form-control" name="parent_category[code][]">  
                           <span class="text-danger" id="product_code_0_error"> </span>                       
                        </div>
                        <!-- <div class="col-sm-2">
                          <a id="add_more" class="btn btn-primary waves-effect waves-light m-l-5" href="javascript:void(0)" onclick="add_more_code()" title="Add More">Add More</a>
                        </div> -->
                      </div>
                      <div class="form-group">
                        <label class="col-sm-3 control-label" for="inputEmail3">Add Few box<span class="asterisk"></span></label>
                        <div class="col-sm-3">
                          <div class="checkbox checkbox-purpal">
                             <input id="checkbox_few_box"  name="parent_category[few_box]" type="checkbox" data-parsley-multiple="groups" data-parsley-mincheck="2" data-parsley-id="69" value="1">
                              <label for="checkbox1"> Yes </label>
                          </div>
                        </div>

                        <label class="col-sm-3 control-label" for="inputEmail3">App Order<span class="asterisk"></span></label>
                        <div class="col-sm-3">
                          <div class="checkbox checkbox-purpal">
                             <input id="checkbox_few_box"  name="parent_category[app_order]" type="checkbox" data-parsley-multiple="groups" data-parsley-mincheck="2" data-parsley-id="69" value="1">
                              <label for="checkbox1"> Yes </label>
                          </div>
                        </div>
                      </div>
                      <div class="form-group">
                        <label class="col-sm-5 control-label" for="inputEmail3"> Add variant</label>
                        <div class="col-sm-4">
                           <input type="text" placeholder="Enter Variant" class="form-control" name="parent_category[variant][]">  
                           <span class="text-danger" id="product_code_0_error"> </span>                       
                        </div>
                        <div class="col-sm-1">
                          <a id="add_more" class="btn btn-primary waves-effect waves-light m-l-5" href="javascript:void(0)" onclick="add_more_variant()" title="Add More">Add More</a>
                        </div>
                      </div>
                      <div class="add_more">
                      </div>  
                      <div class="add_more_variant">
                      </div>                     
                </div> 
                      <div class="btnSection">
                        
                          
                           <button class="btn btn-default waves-effect waves-light m-l-5 btn-md" onclick="window.location='<?= ADMIN_PATH?>parent_category'" type="reset">
                             back
                             </button>
                              <button class="btn btn-success pull-right waves-effect waves-light btn-md" name="commit" type="button" onclick="save_parent_category('store'); ">
                           SAVE
                           </button>
                       
                      </div>
                    </form>
                  </div>
              </div>                 
               <div class="clearfix"></div>
            </div>
         </div>
      </div>
   </div>
</div>