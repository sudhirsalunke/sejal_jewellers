<div class="content-page">
<div class="content">
   <div class="container">
      <div class="row">
         <div class="col-sm-12 card-box-wrap">
            <div class="panel panel-default">
               <div class="panel-heading">                  
                  <h4 class="inlineBlock">All Product Category</h4>
                  <div class="pull-right btn-wrap m-t-5">
                     <a href="<?= ADMIN_PATH?>parent_category/create" class="add_senior_manager_button btn  btn-warning waves-effect w-md waves-light btn-md">ADD PRODUCT CATEGORY</a>
                     <a onclick="download_parent_category()" href="javascript:void(0)" class="add_senior_manager_button btn btn-primary waves-effect w-md waves-light btn-md">Fetch From Dropbox</a>
                  </div>
               </div>
               <div class="panel-body">
                  <div class="clearfix"></div>
                  <div class="table-rep-plugin">
                     <div class="table-responsive b-0 scrollhidden">
                        <table class="table table-bordered custdatatable" id="parent_category_table">
                           <thead>
                           <?php
                          $data['heading']='parent_category_table';
                          $this->load->view('master/table_header',$data);
                        ?>

                             <!--  <tr>
                                 <th class="col4">Name</th>
                                 <th class="col4">Code</th>
                                 <th class="col4">Parent Product Category</th>
                                 <th class="col3"></th>
                              </tr> -->
                           </thead>
                        </table>
                     </div>
                  </div>
               </div>
               
               
            </div>
         </div>
      </div>
   </div>
</div>