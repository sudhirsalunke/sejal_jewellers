<div class="content-page">
<div class="content">
   <div class="container">
      <div class="row">
         <div class="col-sm-6 card-box-wrap">
            <div class="panel panel-default">
               <div class="panel-heading frmHeading">
                 <h4 class="inlineBlock"><span>Edit Product Category</span></h4>
               </div>               
               <div class="panel-body">
                 <div class="col-lg-12">
                  <form name="parent_category" id="parent_category" role="form" class="form-horizontal" method="post">
                    <input type="hidden" name="parent_category[encrypted_id]" value="<?=$parent_category['encrypted_id']?>">
                    <input type="hidden" name="parent_category_id" value="<?=$parent_category['id']?>">
                     <div class="form-group">
                        <label class="col-sm-5 control-label" for="inputEmail3"> Enter Product Category <span class="asterisk">*</span></label>
                        <div class="col-sm-7">
                           <input type="text" placeholder="Enter Parent Category" class="form-control" name="parent_category[name]" value="<?=$parent_category['name']?>">
                           <span class="text-danger" id="name_error"></span>
                        </div>
                     </div>
                     <div class="form-group">
                        <label class="col-sm-5 control-label" for="inputEmail3"> Select Parent Product Category<span class="asterisk">*</span></label>
                        <div class="col-sm-7">
                          <select name="parent_category[category_id]" class="form-control">
                            <option value="0">Please Select ect Parent Product Category</option>
                            <?php 
                              foreach ($categories as $key => $value) {
                                if($value['id'] == $parent_category['category_id'])
                                  $selected = 'selected';
                                else
                                  $selected = '';
                                ?>
                                  <option value="<?= $value['id']?>" <?= $selected;?>><?= $value['name']?></option>
                                <?php
                              }
                            ?>
                          </select> 
                          <span class="text-danger" id="category_id_error"></span>
                        </div>
                      </div>
                        <div class="form-group">
                        <label class="col-sm-5 control-label" for="inputEmail3">Kundan category <span class="asterisk"></span></label>
                        <div class="col-sm-7">
                         <select name="parent_category[kundan_category]" class="form-control">
                            <option value="0">Please Select Kundan category</option>
                            <?php 
                              foreach ($product_category_rate as $key => $value) {
                                 if($value['percentage'] == $parent_category['kundan_category'])
                                  $selected = 'selected';
                                else
                                  $selected = '';
                                ?>
                                  <option value="<?= $value['percentage']?>" <?= $selected;?>><?= $value['name']?></option>
                                <?php
                              }
                            ?>
                          </select> 
                          <span class="text-danger" id="category_id_error"></span>
                        </div>
                           <span class="text-danger" id="name_error"></span>
                        </div>
                 <div class="form-group">
                        <label class="col-sm-5 control-label" for="inputEmail3">stone category <span class="asterisk"></span></label>
                        <div class="col-sm-7">
                              <select name="parent_category[stone_category]" class="form-control">
                            <option value="0">Please Select Kundan category</option>
                            <?php 
                              foreach ($product_category_rate as $key => $value) {
                                if($value['percentage'] == $parent_category['stone_category'])
                                  $selected = 'selected';
                                else
                                  $selected = '';
                                ?>
                                  <option value="<?= $value['percentage']?>" <?= $selected;?>><?= $value['name']?></option>
                                <?php
                              }
                            ?>
                          </select> 
                           <span class="text-danger" id="name_error"></span>
                        </div>
                </div> 
                 <div class="form-group">
                        <label class="col-sm-5 control-label" for="inputEmail3">black beads category <span class="asterisk"></span></label>
                        <div class="col-sm-7">
                            <select name="parent_category[black_beads_category]" class="form-control">
                            <option value="0">Please Select black beads category</option>
                            <?php 
                              foreach ($product_category_rate as $key => $value) {
                                if($value['percentage'] == $parent_category['black_beads_category'])
                                  $selected = 'selected';
                                else
                                  $selected = '';
                                ?>
                                  <option value="<?= $value['percentage']?>" <?= $selected;?>><?= $value['name']?></option>
                                <?php
                              }
                            ?>
                          </select> 
                           <span class="text-danger" id="name_error"></span>
                        </div>
                </div>
                     <?php
                      if(!empty($parent_category_codes)){
                        foreach ($parent_category_codes as $key => $value) {
                          $label_txt="";
                          $title_btn ="Remove";
                          $btn_class="btn-danger";
                          $btn_onclick="remove_code(this)";
                          if ($key==0) {
                           $label_txt='Add Code <span class="asterisk">*</span>';
                           $title_btn ="Add More";
                           $btn_class="btn-primary";
                           $btn_onclick="add_more_code()";
                          }
                         ?>
                         <div class="form-group">
                            <label class="col-sm-5 control-label" for="inputEmail3"><?=$label_txt?></label>
                            <div class="col-sm-7">
                               <input type="text" placeholder="Enter Code" class="form-control" name="parent_category[code][]" value="<?=$value['code_name']?>" readonly>
                               <input type="hidden" name="parent_category[ids][]" value="<?=$value['id']?>">
                               <span class="text-danger" id="product_code_<?=$key?>_error"> </span>                         
                            </div>
                        </div>
                         <?php
                        }
                      }else{
                        ?>
                        <div class="form-group">
                          <label class="col-sm-5 control-label" for="inputEmail3"> Add Code <span class="asterisk">*</span></label>
                          <div class="col-sm-7">
                             <input type="text" placeholder="Enter Code" class="form-control" name="parent_category[code][]">
                             <span class="text-danger" id="product_code_0_error"> </span>                       
                          </div>
                          <div class="col-sm-1">
                          <a id="add_more" class="btn btn-primary waves-effect waves-light m-l-5" href="javascript:void(0)" onclick="add_more_variant()" title="Add More">Add More</a>
                        </div>
                        </div>
                        <?php
                      }
                     ?>
                      <div class="form-group">
                        <label class="col-sm-3 control-label" for="inputEmail3">Add Few box<!-- <span class="asterisk">*</span> --></label>
                        <div class="col-sm-3">
                           <div class="checkbox checkbox-purpal">
                             <input id="checkbox_few_box"  name="parent_category[few_box]" type="checkbox" data-parsley-multiple="groups" data-parsley-mincheck="2" data-parsley-id="69" value="1"  <?=$parent_category['few_box']=='1' ? 'checked' : '';?> >
                              <label for="checkbox1"> Yes </label>
                            </div>
                            </div>


                            <label class="col-sm-3 control-label" for="inputEmail3">App Order<!-- <span class="asterisk">*</span> --></label>
                        <div class="col-sm-3">
                           <div class="checkbox checkbox-purpal">
                             <input id="checkbox_app_order"  name="parent_category[app_order]" type="checkbox" data-parsley-multiple="groups" data-parsley-mincheck="2" data-parsley-id="69" value="1"  <?=$parent_category['app_order']=='1' ? 'checked' : '';?> >
                              <label for="checkbox1"> Yes </label>
                            </div>
                            </div>
               
                     </div>
                      <?php
                      $add_more_variant = true;
                      if(!empty($parent_category_variants)){
                        foreach ($parent_category_variants as $pc_key => $pc_value) {
                          $label_txt='';
                          $add_more_variant = false;
                          if ($pc_key==0) {
                           $label_txt='Add variants';
                          }
                         ?>
                         <div class="form-group">
                            <label class="col-sm-5 control-label" for="inputEmail3"><?=$label_txt?></label>
                            <div class="col-sm-4">
                               <input type="text" placeholder="Enter Code" class="form-control" name="parent_category[variant][]" value="<?=$pc_value['name']?>">
                               <input type="hidden" name="parent_category[variant_ids][]" value="<?=$pc_value['id']?>">
                            </div>
                            <div class="col-sm-1"><a  class="btn btn-danger small loader-hide  btn-sm" href="javascript:void(0)" onclick="remove_code(this)" title="Add More">Remove</a></div>
                        </div>
                         <?php
                       }
                     }
                      //}else{
                        ?>
                        <div class="form-group">
                        <?php 
                          if($add_more_variant == true){
                        ?>
                          <label class="col-sm-5 control-label" for="inputEmail3"> Add variants</label>
                          <?php 
                        }else{
                          ?>
                          <label class="col-sm-5 control-label" for="inputEmail3"></label>
                          <?php 
                        }
                          ?>
                          <div class="col-sm-4">
                             <input type="text" placeholder="Enter Variant" class="form-control" name="parent_category[variant][]">
                             <!-- <span class="text-danger" id="product_code_0_error"> </span>                        -->
                          </div>
                          <div class="col-sm-1">
                            <a id="add_more" class="btn btn-primary waves-effect waves-light m-l-5" onclick="add_more_variant()" title="Add More">Add More</a>
                          </div>
                        </div>
                        <?php
                      //}
                     ?>
                      <div class="add_more">
                      </div>
                      <div class="add_more_variant">
                      </div>
                     <?php
                      if(!empty($images)){
                        ?>
                        <div class="form-group">
                        <div class="col-sm-3">
                        </div>
                        <?php
                        foreach ($images as $key => $value) {
                        ?>
                          <div class="col-sm-3">
                           <img src="<?= ADMIN_PATH?>uploads/parent_category/<?= $parent_category['name']?>/small/<?= $value['path']?>">
                        </div>
                        <?php
                        }
                        ?>
                        </div>
                        <?php
                      }
                     ?>
                    <div class="btnSection">
                         
                         <button class="btn btn-default waves-effect waves-light m-l-5 btn-md" onclick="window.location='<?= ADMIN_PATH?>parent_category'" type="reset">
                           back
                           </button>
                           <button class="btn btn-success pull-right waves-effect waves-light btn-md" name="commit" type="button" onclick="save_parent_category('update'); ">
                         SAVE
                         </button>
                     
                  </div>
                  </form>
               </div>
               </div>
            </div>
         </div>
      </div>
   </div>
</div>