<div class="content-page">
    <div class="content">
        <div class="container">
            <div class="row">
                <div class="col-sm-12 card-box-wrap">
                    <div class="panel panel-default">
                        <div class="panel-heading frmHeading">
                            <h4 class="inlineBlock"><span>Create Minimum Stock</span></h4>
                        </div>                        
                        <div class="panel-body">
                            <div class="col-lg-12">
                            <form name="minimum_stock" id="minimum_stock" role="form" class="form-horizontal" method="post">
                                <div class="form-group">

                                </div>
                                <input type="hidden" name="category_id" value="">
                                <div class="row">
                                    <div class="col-sm-12">
                                        <div class="card-box">
                                            <div class="dropdown pull-right">

                                            </div>

                                            <h4 class="header-title m-t-0 m-b-30">Parent Category : <?=$parent_category['name']?></h4>

                                            <div class="row">
                                                <div class="col-lg-12">
                                                    <div class="panel-group" id="accordion" role="tablist" aria-multiselectable="true">
                                                        <?php foreach ($codes as $key=> $value) { ?>
                                                        <input type="hidden" name="minimum_stock[<?=$key?>][pc_code_id]" value="<?=$value['id']?>">
                                                        <div class="panel panel-default bx-shadow-none">
                                                            <div class="panel-heading" role="tab" id="heading<?=$key?>">
                                                                <h4 class="panel-title">
                                                            <a role="button" data-toggle="collapse"
                                                               data-parent="#accordion" href="#collapse<?=$key?>"
                                                               aria-expanded="true" aria-controls="collapse<?=$key?>">
                                                               <?=$value['code_name']?>
                                                            </a>
                                                        </h4>
                                                            </div>
                                                            <div id="collapse<?=$key?>" class="panel-collapse collapse <?=($key==0) ? 'in' : ''?>" role="tabpanel" aria-labelledby="heading<?=$key?>">
                                                                <div class="panel-body">
                                                                    <div class="form-group col-sm-6">
                                                                        <label class="col-sm-4 control-label" for="inputEmail3"> Weight Range</label>
                                                                        <label class="col-sm-2 control-label" for="inputEmail3">Stock</label>
                                                                    </div>
                                                                    <div class="form-group col-sm-6">
                                                                        <label class="col-sm-4 control-label" for="inputEmail3"> Weight Range</label>
                                                                        <label class="col-sm-2 control-label" for="inputEmail3">Stock</label>
                                                                    </div>
                                                                    <?php foreach ($weight_ranges as $w_key=> $w_value) { ?>
                                                                    <div class="form-group col-sm-6">
                                                                        <label class="col-sm-4 control-label" for="inputEmail3">
                                                                            <?php echo $w_value[ 'from_weight']. ' - '.$w_value[ 'to_weight']?>
                                                                        </label>
                                                                        <?php $i_value='' ; foreach (@$value[ 'stock'] as $s_key=> $s_value) { if($s_value['weight_range_id'] == $w_value['id']){ $i_value = $s_value['stock']; } } ?>
                                                                        <div class="col-sm-4">
                                                                            <input type="text" placeholder="Enter Minimum Stock" id="code" class="form-control" name="minimum_stock[<?=$key?>][weight_range_id][<?=$w_value['id']?>]" required="" value="<?= $i_value;?>">
                                                                            <span class="text-danger col-sm-12" id="<?=$value['id'].'_'.$w_value['id']?>_error"></span>
                                                                        </div>

                                                                    </div>
                                                                    <?php } ?>

                                                                </div>
                                                            </div>
                                                        </div>
                                                        <?php } ?>
                                                    </div>
                                                </div>
                                                <!-- end col -->

                                            </div>
                                            <!-- end row -->

                                        </div>
                                    </div>
                                    <!-- end col -->
                                </div>
                                <!-- end row -->

                                <div class="btnSection">
                                    
                                        <button class="btn btn-default waves-effect waves-light m-l-5 btn-md" onclick="window.location='<?= ADMIN_PATH?>parent_category'" type="reset">
                                            back
                                        </button>
                                        <button class="btn btn-success pull-right waves-effect waves-light btn-md" name="commit" type="button" onclick="save_parent_minimum_stock(); ">
                                            SAVE
                                        </button>
                                    
                                </div>
                            </form>
                        </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>