<div class="content-page">
   <div class="content">
      <div class="container">
         <div class="row">
            <div class="col-sm-12 card-box-wrap">
               <div class="panel panel-default">
                  <div class="panel-heading">
                     <h4 class="inlineBlock"><span>ALL COLOR STONE</span></h4>
                     <div class="pull-right btn-wrap3">
                        <a href="<?= ADMIN_PATH?>color_stone/create" class="add_senior_manager_button btn btn-primary  waves-effect w-md waves-light m-b-5  btn-md">ADD COLOR STONE</a>
                     </div>
                  </div>
                  <div class="panel-body">
                     <div class="table-rep-plugin">
                        <div class="table-responsive b-0 scrollhidden">
                           <table class="table table-bordered custdatatable" id="color_stone_table">
                              <thead>
                                 <tr>
                                    <th class="col4">Name</th>
                                    <th></th>
                                 </tr>
                              </thead>
                           </table>
                        </div>
                     </div>
                  </div>
               </div>
            </div>
         </div>
      </div>
   </div>
</div>