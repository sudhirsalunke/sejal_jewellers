
<div class="content-page">
<div class="content">
   <div class="container">
      <div class="row">
         <div class="col-sm-6 card-box-wrap">
            <div class="panel panel-default">
              <div class="panel-heading frmHeading">
                <h4 class="inlineBlock"><span>Add salesman</span></h4>
              </div>                
              <div class="panel-body">
                <div class="col-lg-12">
                  <form name="salesman_master_form" id="salesman_master_form" role="form" class="form-horizontal" method="post">
                    <input type="hidden" id="controller_name" value="salesman_master">
                    <?php $columns_array=array(
                                            array( 
                                                "display_name" => 'Name',
                                                "placeholder" => 'Enter Name',
                                                "name"=>'name',
                                                "type" =>'text',
                                              ),
                                            array( 
                                                "display_name" => 'Mobile Number',
                                                "placeholder" => 'Enter Mobile Number',
                                                "name"=>'mobile_no',
                                                "type" =>'text',
                                              ),
                                            array( 
                                                "display_name" => 'Address',
                                                "placeholder" => 'Enter Address',
                                                "name"=>'address',
                                                "type" =>'text',
                                              ),
                                             array( 
                                                "display_name" => 'State',
                                                "placeholder" => '',
                                                "name"=>'state_id',
                                                "type" =>'dropdown',
                                                "dropdown_array"=>$state,
                                                "id"=>'state',
                                              ),
                                              array( 
                                                "display_name" => 'City',
                                                "placeholder" => '',
                                                "name"=>'city_id',
                                                "type" =>'dropdown',
                                                "dropdown_array"=>$city,
                                                "id"=>'city',
                                              ),
                                        ); ?>

                      <?php foreach ($columns_array as $c_key => $c_value) { 
                          $display_name =$c_value['display_name'];
                          $placeholder =$c_value['placeholder'];
                          $name =$c_value['name'];
                          $type=$c_value['type'];
                          $id=@$c_value['id'];
                      ?>
                        
                         <div class="form-group">
                        <label class="col-sm-4 control-label" for="inputEmail3"><?=$display_name?> <span class="asterisk">*</span></label>
                        <div class="col-sm-8">
                          <?php if(@$type=="text"){ ?>

                              <input type="text" placeholder="<?=$placeholder?>" class="form-control" name="salesmans[<?=$name?>]">

                        <?php  } elseif(@$type=="dropdown") { ?>

                           <select name="salesmans[<?=$name?>]" class="form-control" id="<?=$id?>">
                            <option value="">Select <?=$display_name?></option>
                            <?php foreach ($c_value['dropdown_array'] as $key => $value) { ?>
                             <option value="<?=$value['id']?>"><?=$value['name']?></option>
                            <?php } ?>
                            </select>

                         <?php } else if(@$type=="radio"){

                         }

                          ?>
                           
                           <span class="text-danger" id="<?=$name?>_error"></span>
                        </div>
                     </div>

                     <?php } ?>
                     
                     
                    <div class="btnSection">
                    
                        <button class="btn btn-default waves-effect waves-light m-l-5 btn-md" onclick="window.location='<?= ADMIN_PATH?>salesman_master'" type="reset">
                           back
                           </button>
                           <button class="btn btn-success waves-effect waves-light btn-md pull-right" name="commit" type="button" onclick="save_salesman_master('store'); ">
                         SAVE
                         </button>
                     
                  </div>
                  </form>
                </div>
              </div>
            </div>
         </div>
      </div>
   </div>
</div>