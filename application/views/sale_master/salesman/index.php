<div class="content-page">
<div class="content">
   <div class="container">
      <div class="row">
         <div class="col-sm-12 card-box-wrap">
            <div class="panel panel-default">
               <div class="panel-heading">
                  <h4 class="inlineBlock">ALL SALESMANS</h4>
                  <div class="pull-right single-add btn-wrap3">
                  <a href="<?= ADMIN_PATH?>salesman_master/create" class="pull-right add_senior_manager_button btn btn-primary waves-effect w-md waves-light m-t-5 btn-md single-add">ADD SALESMAN</a>
                  </div>
               </div>
               <div class="panel-body">
                  <div class="clearfix"></div>
                  <div class="table-rep-plugin">
                     <div class="table-responsive b-0 scrollhidden">
                        <table class="table table-bordered custdatatable" id="salesman_master_table">
                           <thead>
                        <?php
                          $data['heading']='salesman_master_table';
                          $this->load->view('master/table_header',$data);
                        ?>
                             <!--  <tr>
                                 <th class="col4">Name</th>
                                 <th class="col4">Mobile Number</th>
                                 <th class="col4">Address</th>
                                 <th class="col4">City</th>
                                 <th class="col4">State</th>
                                 <th></th>
                              </tr> -->
                           </thead>
                        </table>
                     </div>
                  </div>
               </div>
            </div>
         </div>
      </div>
   </div>
</div>