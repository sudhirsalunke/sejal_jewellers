<div class="content-page">
<div class="content">
   <div class="container">
      <div class="row">
         <div class="col-sm-12 card-box-wrap">
            <div class="panel panel-default">
               <div class="panel-heading">
                  <h4 class="inlineBlock">ALL VARIATIONS CATEGORIES</h4>
                  <div class="btn-wrap pull-right">
                  <a href="<?= ADMIN_PATH?>Variations_category/create" class="pull-right add_senior_manager_button btn btn-primary waves-effect w-md waves-light m-t-5 btn-md">ADD VARIATION CATEGORY</a>
                  </div>
               </div>
               <div class="panel-body">
                  <div class="clearfix"></div>
                  <div class="table-rep-plugin">
                     <div class="table-responsive b-0 scrollhidden">
                        <table class="table table-bordered custdatatable" id="variation_category_table">
                           <thead>
                           <?php
                          $data['heading']='variation_category_table';
                          $this->load->view('master/table_header',$data);
                        ?>
                             <!--  <tr>
                                 <th class="col4">Name</th>
                                 <th></th>
                              </tr> -->
                           </thead>
                        </table>
                     </div>
                  </div>
               </div>
                           
            </div>
         </div>
      </div>
   </div>
</div>