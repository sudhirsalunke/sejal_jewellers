<div class="content-page">
<div class="content">
   <div class="container">
      <div class="row">
         <div class="col-sm-6 card-box-wrap">
            <div class="panel panel-default">
              <div class="panel-heading frmHeading">
                <h4 class="inlineBlock"><span>Add Variation Category</span></h4>
              </div>
              <div class="panel-body">
                <div class="col-lg-12">
                  <form name="Variations_category_form" id="Variations_category_form" role="form" class="form-horizontal" method="post">
                    <?php $columns_array=array(
                                            array( 
                                                "display_name" => 'Variation Type',
                                                "placeholder" => 'Enter Variation Type',
                                                "name"=>'type',
                                                "type" =>'text',
                                              ),
                                             array( 
                                                "display_name" => 'Variation Name',
                                                "placeholder" => 'Enter Variation Name',
                                                "name"=>'name',
                                                "type" =>'text',
                                              ),
                                        ); ?>

                      <?php foreach ($columns_array as $c_key => $c_value) { 
                          $display_name =$c_value['display_name'];
                          $placeholder =$c_value['placeholder'];
                          $name =$c_value['name'];
                          $type=$c_value['type'];
                      ?>
                        
                         <div class="form-group">
                        <label class="col-sm-4 control-label" for="inputEmail3"><?=$display_name?> <span class="asterisk">*</span></label>
                        <div class="col-sm-4">
                          <?php if(@$type=="text"){ ?>

                              <input type="text" placeholder="<?=$placeholder?>" class="form-control" name="Variations_category[<?=$name?>]">

                        <?php  } elseif(@$type=="dropdown") { ?>

                           <select name="Variations_category[<?=$name?>]" class="form-control" >
                            <option value="">Select <?=$display_name?></option>
                            <?php foreach ($c_value['dropdown_array'] as $key => $value) { ?>
                             <option value="<?=$value['id']?>"><?=$value['name']?></option>
                            <?php } ?>
                            </select>

                         <?php } else if(@$type=="radio"){

                         }

                          ?>
                           
                           <span class="text-danger" id="<?=$name?>_error"></span>
                        </div>
                     </div>

                     <?php } ?>
                     
                     
                    <div class="btnSection">
                         
                         <button class="btn btn-default waves-effect waves-light m-l-5 btn-md" onclick="window.location='<?= ADMIN_PATH?>Variations_category'" type="reset">
                           Back
                           </button>
                           <button class="btn btn-success waves-effect waves-light btn-md pull-right" name="commit" type="button" onclick="save_Variations_category('store'); ">
                         SAVE
                         </button>
                  </div>
                  </form>
                </div>
              </div>
               <div class="clearfix"></div>
            </div>
         </div>
      </div>
   </div>
</div>