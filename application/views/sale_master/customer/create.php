
<div class="content-page">
<div class="content">
   <div class="container">
      <div class="row">
         <div class="col-sm-12 card-box-wrap">
            <div class="panel panel-default">
              <div class="panel-heading frmHeading">
                <h4 class="inlineBlock"><span>Add Customer</span></h4>
              </div>
              <div class="panel-body">
                <div class="col-lg-12">
                  <form name="customer_form" id="customer_form" role="form" class="form-horizontal" method="post">
                  <input type="hidden" id="controller_name" value="Karigar">
                    <?php $columns_array=array(
                                            array( 
                                                "display_name" => 'Name',
                                                "placeholder" => 'Enter Name',
                                                "name"=>'name',
                                                "type" =>'text',
                                              ),
                                            array( 
                                                "display_name" => 'Refer By',
                                                "placeholder" => 'Enter Refernce Name',
                                                "name"=>'refer_by',
                                                "type" =>'text',
                                              ),
                                             array( 
                                                "display_name" => 'Customer Type',
                                                "placeholder" => '',
                                                "name"=>'customer_type_id',
                                                "type" =>'dropdown',
                                                "dropdown_array"=>$customer_type,
                                              ),
                                             array( 
                                                "display_name" => 'Hallmarking Center',
                                                "placeholder" => '',
                                                "name"=>'hc_id',
                                                "type" =>'dropdown',
                                                "dropdown_array"=>$hallmarking_center,
                                              ),
                                             array( 
                                                "display_name" => 'Corporate',
                                                "placeholder" => '',
                                                "name"=>'corporate_id',
                                                "type" =>'dropdown',
                                                "dropdown_array"=>$corporate,
                                              ),
                                            array( 
                                                "display_name" => 'Contact Name',
                                                "placeholder" => 'Enter Contact Person Name',
                                                "name"=>'contact_person',
                                                "type" =>'text',
                                              ),
                                             array( 
                                                "display_name" => 'Mobile Number',
                                                "placeholder" => 'Enter MObile Number',
                                                "name"=>'mobile_no',
                                                "type" =>'text',
                                              ),
                                            array( 
                                                "display_name" => 'Address',
                                                "placeholder" => 'Enter Address',
                                                "name"=>'address',
                                                "type" =>'text',
                                              ),
                                            array( 
                                                "display_name" => 'Area',
                                                "placeholder" => 'Enter Area',
                                                "name"=>'area',
                                                "type" =>'text',
                                              ),
                                             array( 
                                                "display_name" => 'State',
                                                "placeholder" => '',
                                                "name"=>'state_id',
                                                "type" =>'dropdown',
                                                "dropdown_array"=>$state,
                                                "id"=>'state',
                                              ),
                                              array( 
                                                "display_name" => 'City',
                                                "placeholder" => '',
                                                "name"=>'city_id',
                                                "type" =>'dropdown',
                                                "dropdown_array"=>$city,
                                                "id"=>'city',
                                              ),
                                              array( 
                                                "display_name" => 'Limit Amount',
                                                "placeholder" => 'Enter Limit Amount',
                                                "name"=>'limit_amt',
                                                "type" =>'text',
                                              ),
                                              array( 
                                                "display_name" => 'Limit Metal',
                                                "placeholder" => 'Enter Limit Metal',
                                                "name"=>'limit_metal',
                                                "type" =>'text',
                                              ),
                                              array( 
                                                "display_name" => 'No Of Days',
                                                "placeholder" => 'Enter No Of days',
                                                "name"=>'days',
                                                "type" =>'text',
                                              ),
                                              array( 
                                                "display_name" => 'Sales Under',
                                                "placeholder" => '',
                                                "name"=>'sales_under',
                                                "type" =>'dropdown',
                                                "dropdown_array"=>$sales_under,
                                              ),
                                              array( 
                                                "display_name" => 'Terms',
                                                "placeholder" => 'Enter Terms',
                                                "name"=>'terms',
                                                "type" =>'text',
                                              ),
                                              array( 
                                                "display_name" => 'Bank A/c No',
                                                "placeholder" => 'Enter Bank A/c No',
                                                "name"=>'bank_ac',
                                                "type" =>'text',
                                              ),
                                              array( 
                                                "display_name" => 'IFSC No',
                                                "placeholder" => 'Enter IFSC No',
                                                "name"=>'bank_ifsc',
                                                "type" =>'text',
                                              ),
                                              array( 
                                                "display_name" => 'Bank Name',
                                                "placeholder" => 'Enter Bank Name',
                                                "name"=>'bank_name',
                                                "type" =>'text',
                                              ),
                                              array( 
                                                "display_name" => 'Bank Branch',
                                                "placeholder" => 'Enter Bank Branch',
                                                "name"=>'bank_branch',
                                                "type" =>'text',
                                              ),
                                        ); ?>

                      <?php foreach ($columns_array as $c_key => $c_value) { 
                          $display_name =$c_value['display_name'];
                          $placeholder =$c_value['placeholder'];
                          $name =$c_value['name'];
                          $type=$c_value['type'];
                          $id=@$c_value['id'];
                      ?>
                        
                         <div class="form-group col-sm-6">
                        <label class="col-sm-4 control-label" for="inputEmail3"><?=$display_name?> <span class="asterisk">*</span></label>
                        <div class="col-sm-8">
                          <?php if(@$type=="text"){ ?>

                              <input type="text" placeholder="<?=$placeholder?>" class="form-control" name="customer[<?=$name?>]">

                        <?php  } elseif(@$type=="dropdown") { ?>

                           <select name="customer[<?=$name?>]" class="form-control" id="<?=$id?>">
                            <option value="">Select <?=$display_name?></option>
                            <?php foreach ($c_value['dropdown_array'] as $key => $value) { ?>
                             <option value="<?=$value['id']?>"><?=$value['name']?></option>
                            <?php } ?>
                            </select>

                         <?php } else if(@$type=="radio"){

                         }

                          ?>
                           
                           <span class="text-danger" id="<?=$name?>_error"></span>
                        </div>
                     </div>

                     <?php } ?>
                     
                     
                    <div class="btnSection">
                      <button class="btn btn-default waves-effect waves-light m-l-5  btn-md" onclick="window.location='<?= ADMIN_PATH?>customer'" type="reset">
                           back
                      </button>
                      <button class="btn btn-success waves-effect waves-light btn-md pull-right" name="commit" type="button" onclick="save_customer('store'); ">
                         SAVE
                      </button>                         
                    </div>                 
                  </form>
               </div>
            </div>
            <div class="clearfix"></div>
            </div>
         </div>
      </div>
   </div>
</div>
</div>