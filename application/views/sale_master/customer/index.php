<div class="content-page">
   <div class="content">
      <div class="container">
         <div class="row">
            <div class="col-sm-12 card-box-wrap">
               <div class="panel panel-default">
                  <div class="panel-heading">
                     <h4 class="inlineBlock"><span>ALL CUSTOMERS</span></h4>
                     <div class="pull-right wrap-320">
                        <a href="<?= ADMIN_PATH?>customer/create" class="add_senior_manager_button btn btn-primary  waves-effect w-md waves-light m-b-5  btn-md">ADD CUSTOMER</a>
                     </div>
                  </div>
                  <div class="panel-body">                 
                     <div class="table-rep-plugin">
                        <div class="table-responsive b-0 scrollhidden">
                           <table class="table table-bordered custdatatable" id="customer_table">
                              <thead>
                                 <tr>
                                    <th class="col4">Name</th>
                                    <th class="col4">Corporate</th>
                                    <th>Contact Person</th>
                                    <th>Address</th>
                                    <th>Mobile No</th>
                                    <th></th>
                                 </tr>
                              </thead>
                           </table>
                        </div>
                     </div>
                  </div>
               </div>
            </div>
         </div>
      </div>
   </div>
</div>