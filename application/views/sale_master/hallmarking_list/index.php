<div class="content-page">
<div class="content">
   <div class="container">
      <div class="row">
         <div class="col-sm-12 card-box-wrap">
            <div class="panel panel-default">
               <div class="panel-heading">
                  <h4 class="inlineBlock"><?=$page_title;?></h4>
                  <div class="pull-right btn-wrap m-t-5">
                     <button  type="button" onclick="export_hallmarking_list()" class="add_senior_manager_button btn btn-warning waves-effect w-md waves-light btn-md">EXPORT</button>
                     <a href="<?= ADMIN_PATH?>hallmarking_list/create" class="add_senior_manager_button btn btn-primary waves-effect w-md waves-light btn-md">ADD HALLMARKING CENTER</a>
                  </div>
               </div>
               <div class="panel-body">
                  <div class="clearfix"></div>
                  <div class="table-rep-plugin">
                     <div class="table-responsive b-0 scrollhidden">
                        <table class="table table-bordered custdatatable" id="hallmarking_list_table">
                        <thead>
                                  <?php
                          $data['heading']='hallmarking_list_table';
                      $this->load->view('master/table_header',$data)?>
                          <!--  <tr>
                              <th class="col4">HM Center</th>
                              <th class="col4">HM Code</th>
                              <th class="col4">Company Name</th>
                              <th class="col4">Owner</th>
                              <th class="col4">Authorized Person</th>                     
                              <th class="col4">Mobile No</th>
                              <th class="col4">Phone No</th>
                              <th class="col3">Address</th>
                              <th></th>
                           </tr> -->
                        </thead>
                        </table>
                     </div>
                  </div>
               </div>
            </div>
         </div>
      </div>
   </div>
</div>