<!DOCTYPE html>
<html>
  <head><title>Hallmarking Center</title>
  <style>
  body {
    background: rgb(204,204,204);
    font-family: arial; 
    font-size: 14px;
    text-align: left;
  }
  th {
    vertical-align: text-bottom;
    
  }
  page {
    background: white;
    display: block;
    margin: 0 auto;
    margin-bottom: 0.5cm;
    box-shadow: 0 0 0.5cm rgba(0,0,0,0.5);
  }
  page[size="A4"] {  
    width: 21cm;
    height: 29.7cm;
    /*  height:  auto;*/
    padding: 3em; 
    box-sizing: border-box;
  }
  page[size="A4"][layout="portrait"] {
    width: 29.7cm;
    height: 21cm;  
  }
  @media print {
    body, page {
      margin: 0;
      box-shadow: 0;    
    }

  }


  table {
    border-collapse: collapse;
    width: 60%;
    border: 1px solid black;

  }

  th, th ,td ,tr{
    padding: 5px;
    text-align: left;
    line-height: 100%;
    border: 1px solid black;
    border-color:#cccccc; /*grey*/
    font-size:10px


  }
  td{
    vertical-align: top;
    font-size: 10px;
  }
  th{
    vertical-align: top;
    font-size: 12px;
  }

  @page {
    size: auto;   /* auto is the initial value */
    margin: 0;  /* this affects the margin in the printer settings */
  }
  .print_area *{
    font-size:12px;
    left: 0;
    top: 0;
  }

  @media print {



    .print_area, .print_area * {
      visibility: visible;
      font-size: 12px;
    }
    .input_data
    {
      display: none;
    }
    .button_html
    {
      display: none;
    }
    .print_area {
      font-size: 12px;
      left: 0;
      top: 0;
    }
    .pgbrk 
    {
      page-break-after: always;
    }
  }
</style>  
  </head>

<body onload="window.print()">
  
<page size="A4">
  <div class="content-page">
    <div class="content">
      <div class="container">
        <div class="row">
          <div class="col-sm-12 card-box-wrap">
            <div class="card-box padmobile  clearfix">
              <div class="print_area stick">
                <div class="col-sm-4">              
                  <div >                    
                      <?php if($Hallmarking_details['phone_no']=='' && $Hallmarking_details['mobile_no'] == ''){
                        $ContactNumber="";
                      }elseif ($Hallmarking_details['phone_no']!='' && $Hallmarking_details['mobile_no'] != '') {
                        $ContactNumber=$Hallmarking_details['phone_no'].' / '.$Hallmarking_details['mobile_no'];
                      }elseif ($Hallmarking_details['phone_no']!='' && $Hallmarking_details['mobile_no'] == '') {
                        $ContactNumber=$Hallmarking_details['phone_no'];
                      }elseif ($Hallmarking_details['phone_no']=='' && $Hallmarking_details['mobile_no'] != '') {
                        $ContactNumber=$Hallmarking_details['mobile_no'];
                      } 
                      ?>
                      <table style="width:7.5cm">
                        <thead>
                          <tr><th colspan="2" style="text-align: center;">Hallmarking Center</th></tr>
                        </thead>
                        <tbody>
                            <tr>
                              <td>                        
                                <label class="karigar_name control-label" for="inputEmail3"> Name : </label>
                              </td>
                              <td>
                                <div class=" ">
                                  <?=$Hallmarking_details['hc_name']; ?>
                                </div>
                              </td>
                            </tr>
                            <tr>                          
                                <td>
                                  <label class=" control-label" for="inputEmail3"> Address : </label>
                                </td>
                                <td>
                                  <div class="">
                                    <?=$Hallmarking_details['address']; ?>
                                  </div>
                                </td>
                            </tr>
                            <tr>
                              <td>
                                <label class=" control-label" for="inputEmail3"> City :</label>
                              </td>
                              <td>
                                <div class="">
                                  <?=$Hallmarking_details['city']; ?>
                                </div>
                              </td>
                            </tr>
                            <tr>                            
                              <td>
                                <label class=" control-label" for="inputEmail3"> State : </label>
                              </td>
                              <td>
                                <div class="">
                                  <?=$Hallmarking_details['state']; ?>
                                </div>
                              </td>                            
                            </tr>                            
                            <tr>                                
                                <td>
                                  <label class="control-label" for="inputEmail3"> Pincode : </label>
                                </td>
                                <td>
                                  <div class="">
                                    <?=$Hallmarking_details['pincode']; ?>
                                  </div>
                                </td>                                
                            </tr>
                            <tr>                                  
                              <td>
                                <label class=" control-label" for="inputEmail3"> Contact Number : </label>
                              </td>
                              <td>
                                <div class="">
                                  <?=$ContactNumber; ?>
                                </div>
                              </td>
                            </tr>
                        </tbody>
                      </table>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</page>
  
</body>
</html>