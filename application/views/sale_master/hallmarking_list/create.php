<div class="content-page">
<div class="content">
   <div class="container">
      <div class="row">
         <div class="col-sm-12 card-box-wrap">
            <div class="card-box padmobile padding0">
              
                <div class="panel-heading frmHeading">
                 <h4 class="inlineBlock"><span>Add Hallmarking Center</span></h4>                
                </div>
                <div class="panel-body">
                   <div class="col-lg-12">
                      <form name="hallmarking_list" id="hallmarking_list" role="form" class="form-horizontal" method="post">
                      <input type="hidden" name="hallmarking_list[id]" id="id" value="">
                      <input type="hidden" name="controller_name" id="controller_name" value="hallmarking_list">
                        <?php $columns_array=array(
                                                  array( 
                                                    "display_name" => 'Hallmarking Center',
                                                    "placeholder" => 'Enter Hallmarking Center',
                                                    "name"=>'hc_id',
                                                    "type" =>'text',
                                                    //'dropdown_array'=>$hallmarking_center,
                                                    'id'=>"hc_id", 
                                                    'required'=>"*",                                               
                                                  ),
                                                  array( 
                                                    "display_name" => 'Hallmarking Center Code',
                                                    "placeholder" => 'Enter Center Code',
                                                    "name"=>'code',
                                                    "id"=>"code",
                                                    "type" =>'text',
                                                    'required'=>"*",
                                                  ),        
                                                array( 
                                                    "display_name" => 'Company Name',
                                                    "placeholder" => 'Enter Company name',
                                                    "name"=>'c_name',
                                                    "type" =>'text',
                                                    "id"=>'c_name',
                                                  ),
                                                 array( 
                                                    "display_name" => 'Owner',
                                                    "placeholder" => 'Enter Owner name',
                                                    "name"=>'owner',
                                                    "type" =>'text',
                                                  ),
                                                  array( 
                                                   "display_name" => 'Contact Person Name',
                                                    "placeholder" => 'Enter Contact Person Name',
                                                    "name"=>'ap_name',
                                                    "type" =>'text',
                                                  ),
                                                   
                                                  array( 
                                                    "display_name" => 'Address',
                                                    "placeholder" => 'Enter Address',
                                                    "name"=>'address',
                                                    "type" =>'text',
                                                  ),
                                                  
                                                   
                                                   array( 
                                                    "display_name" => 'State',
                                                    "placeholder" => '',
                                                    "name"=>'state',
                                                    "type" =>'dropdown',
                                                    "dropdown_array"=>$state,
                                                    "id"=>'state',
                                                    "required"=>'',
                                                  ),
                                                 array( 
                                                    "display_name" => 'City',
                                                    "placeholder" => '',
                                                    "name"=>'city',
                                                    "type" =>'dropdown',
                                                    "dropdown_array"=>$city,
                                                    "id"=>'city',
                                                    "required"=>'',
                                                  ), 
                                                  array( 
                                                    "display_name" => 'Pincode',
                                                    "placeholder" => 'Enter pincode',
                                                    "name"=>'pincode',
                                                    "type" =>'text',
                                                    "maxlength"=>'6',
                                                    
                                                  ), 
                                                  array( 
                                                    "display_name" => 'Email',
                                                    "placeholder" => 'Enter Email',
                                                    "name"=>'email',
                                                    "type" =>'text',
                                                  ),
                                                    array( 
                                                    "display_name" => 'Mobile Number',
                                                    "placeholder" => 'Enter Mobile Number',
                                                    "name"=>'mobile_no',
                                                    "type" =>'text',
                                                  ),
                                                    array( 
                                                    "display_name" => 'Phone Number',
                                                    "placeholder" => 'Enter Phone Number',
                                                    "name"=>'phone_no',
                                                    "type" =>'text',
                                                  ),
                                                     array( 
                                                    "display_name" => 'ICOM Number',
                                                    "placeholder" => 'Enter ICOM Number',
                                                    "name"=>'icom_no',
                                                    "type" =>'text',
                                                  ),
                                                      array( 
                                                    "display_name" => 'Limit',
                                                    "placeholder" => 'Enter Limit',
                                                    "name"=>'limit',
                                                    "type" =>'text',
                                                  ),
                                            ); ?>

                          <?php foreach ($columns_array as $c_key => $c_value) { 
                              $display_name =$c_value['display_name'];
                              $placeholder =$c_value['placeholder'];                                            
                              $name =$c_value['name'];
                              $type=$c_value['type'];
                              $id="";
                              $required="";
                              $maxlength="";
                              if (!empty($c_value['id'])) {
                                $id=$c_value["id"];
                              }
                               if (!empty($c_value['required'])) {
                                $required=$c_value["required"];
                              }
                              if (!empty($c_value['maxlength'])) {
                                $maxlength=$c_value["maxlength"];
                              }
                          ?>

                          <div class="col-md-6">
                             <div class="form-group">

                            <label class="col-sm-4 control-label" for="inputEmail3"><?=$display_name?> <span class="asterisk"><?=$required?></span></label>
                            <div class="col-sm-8">
                              <?php if(@$type=="text"){ ?>

                                  <input type="text" placeholder="<?=$placeholder?>" class="form-control" name="hallmarking_list[<?=$name?>]" id="<?=$id?>" maxlength="<?=$maxlength?>">

                            <?php  } elseif(@$type=="dropdown") { ?>

                               <select name="hallmarking_list[<?=$name?>]" class="form-control" id="<?=$id?>">
                                <option value="">Select <?=$display_name?></option>
                                <?php foreach ($c_value['dropdown_array'] as $key => $value) { ?>
                                 <option value="<?=$value['id']?>"><?=$value['name']?></option>
                                <?php } ?>
                                </select>

                             <?php } else if(@$type=="radio"){

                             }

                              ?>
                               
                               <span class="text-danger" id="<?=$name?>_error"></span>
                            </div>
                         </div>
                         </div>

                         <?php } ?>
                         
                         
                        <div class="form-group btnSection">
                         <!-- <div class="col-sm-12 texalin"> --><!--col-sm-offset-5-->
                             
                             <button class="btn btn-default waves-effect waves-light m-l-5 btn-md" onclick="window.location='<?= ADMIN_PATH?>hallmarking_list'" type="reset">
                               Back
                               </button>
                               <button class="btn btn-success waves-effect waves-light btn-md pull-right" name="commit" type="button" onclick="save_hallmarking_list('store'); ">
                             SAVE
                             </button>
                         <!-- </div> -->
                      </div>
                      </form>
                   </div>
               </div>
               <div class="clearfix"></div>
            </div>
         </div>
      </div>
   </div>
</div>