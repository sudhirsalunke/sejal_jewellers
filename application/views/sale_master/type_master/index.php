<div class="content-page">
<div class="content">
   <div class="container">
      <div class="row">
         <div class="col-sm-12 card-box-wrap">
            <div class="panel panel-default">
               <div class="panel-heading">                  
                <h4 class="inlineBlock">ALL Types</h4>

                <a href="<?= ADMIN_PATH?>type_master/create" class="pull-right add_senior_manager_button btn btn-primary waves-effect w-md waves-light m-t-5 btn-md single-send">ADD Type</a>

               </div>
               <div class="panel-body">
                  <div class="clearfix"></div>
                  <div class="table-rep-plugin">
                     <div class="table-responsive b-0 scrollhidden">
                        <table class="table table-bordered custdatatable" id="type_master_table">
                           <thead>
                           <?php
                          $data['heading']='type_master_table';
                          $this->load->view('master/table_header',$data);
                        ?>
                             <!--  <tr>
                                 <th class="col4">Name</th>
                                 <th></th>
                              </tr> -->
                           </thead>
                        </table>
                     </div>
                  </div>
               </div>               
            </div>
         </div>
      </div>
   </div>
</div>