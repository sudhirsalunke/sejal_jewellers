<div class="content-page">
<div class="content">
   <div class="container">
      <div class="row">
         <div class="col-sm-6 card-box-wrap">
            <div class="panel panel-default">
              <div class="panel-heading frmHeading">
                 <h4 class="inlineBlock"><span>Edit Parent Product Category</span></h4>
              </div>               
              <div class="panel-body">
                <div class="col-lg-12">
                  <form name="product_category" id="product_category" role="form" class="form-horizontal" method="post">
                    <input type="hidden" name="product_category[encrypted_id]" value="<?=$product_category['encrypted_id']?>">
                     <div class="form-group">
                        <label class="col-sm-4 control-label" for="inputEmail3">Product Category Name<span class="asterisk">*</span></label>
                        <div class="col-sm-8">
                           <input type="text" placeholder="Product Category Name" class="form-control" name="product_category[name]" value="<?=$product_category['name']?>" readonly>
                           <span class="text-danger" id="name_error"></span>
                        </div>
                     </div>
                     <div class="form-group">
                        <label class="col-sm-4 control-label" for="inputEmail3">Select Category<span class="asterisk">*</span></label>
                        <div class="col-sm-8">
                          <select name="product_category[parent_category]" class="form-control" >
                            <option value="">Select Category</option>
                            <?php foreach ($departments as $p_key => $p_value) { ?>
                             <option value="<?=$p_value['id']?>" <?=($product_category['parent_category']==$p_value['id']) ? 'selected' : ''; ?>><?=$p_value['name']?></option>
                            <?php } ?>
                            </select>
                          
                           <span class="text-danger" id="parent_category_error"></span>
                        </div>
                     </div>

                      <!-- <div class="form-group">
                        <label class="col-sm-4 control-label" for="inputEmail3">Touch</label>
                        <div class="col-sm-4">
                           <input type="text" placeholder="Enter Touch" class="form-control" name="product_category[touch]" value="<?=$product_category['touch']?>">
                           <span class="text-danger" id="touch_error"></span>
                        </div>
                     </div> -->

                      <!-- <div class="form-group">
                        <label class="col-sm-4 control-label" for="inputEmail3">Select Department<span class="asterisk">*</span></label>
                        <div class="col-sm-4">
                          <select name="product_category[department_id]" class="form-control" >
                            <option value="">Select Department</option>
                            <?php foreach ($departments as $p_key => $p_value) { ?>
                             <option value="<?=$p_value['id']?>" <?=($product_category['department_id']==$p_value['id']) ? 'selected' : ''; ?>><?=$p_value['name']?></option>
                            <?php } ?>
                            </select>
                          
                           <span class="text-danger" id="parent_category_error"></span>
                        </div>
                     </div> -->
                     
                    <div class="btnSection">
                     
                         <button class="btn btn-default waves-effect waves-light m-l-5 btn-md" onclick="window.location='<?= ADMIN_PATH?>product_category'" type="reset">
                           Back
                           </button>
                        <button class="btn btn-success waves-effect waves-light btn-md pull-right" name="commit" type="button" onclick="save_product_category('update'); ">
                         SAVE
                         </button>   
                     
                  </div>
                  </form>
               </div>
              </div>
            </div>
         </div>
      </div>
   </div>
</div>