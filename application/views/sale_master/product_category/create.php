<div class="content-page">
<div class="content">
   <div class="container">
      <div class="row">
         <div class="col-sm-6 card-box-wrap">
            <div class="panel panel-default">
              <div class="panel-heading frmHeading">
                <h4 class="inlineBlock"><span>Add Parent Product Category</span></h4>
              </div>
              <div class="panel-body">
                <div class="col-lg-12">
                  <form name="product_category" id="product_category" role="form" class="form-horizontal" method="post">
                     <div class="form-group">
                        <label class="col-sm-5 control-label" for="inputEmail3">Parent Product Category Name<span class="asterisk">*</span></label>
                        <div class="col-sm-7">
                           <input type="text" placeholder="Product Category Name" class="form-control" name="product_category[name]">
                           <span class="text-danger" id="name_error"></span>
                        </div>
                     </div>
                     <div class="form-group">
                        <label class="col-sm-5 control-label" for="inputEmail3">Select  Category<span class="asterisk">*</span></label>
                        <div class="col-sm-7">

                          <select name="product_category[parent_category]" class="form-control" >
                            <option value="">Select  Category</option>
                            <?php foreach ($parent_category as $p_key => $p_value) { ?>
                             <option value="<?=$p_value['id']?>"><?=$p_value['name']?></option>
                            <?php } ?>
                            </select>
                          
                           <span class="text-danger" id="parent_category_error"></span>
                        </div>
                     </div>

                    <!--  <div class="form-group">
                        <label class="col-sm-4 control-label" for="inputEmail3">Touch</label>
                        <div class="col-sm-4">
                           <input type="text" placeholder="Enter Touch" class="form-control" name="product_category[touch]">
                           <span class="text-danger" id="touch_error"></span>
                        </div>
                     </div> -->
                      <div class="form-group">
                    <!--     <label class="col-sm-5 control-label" for="inputEmail3">Select Item Category<span class="asterisk">*</span></label> -->
                        <div class="col-sm-7">&nbsp;</div>
                      <!-- <div class="form-group">
                        <label class="col-sm-4 control-label" for="inputEmail3">Select Item Category<span class="asterisk">*</span></label>
                        <div class="col-sm-4">

                          <select name="product_category[parent_category]" class="form-control" >
                            <option value="">Select Item Category</option>
                            <?php foreach ($parent_category as $p_key => $p_value) { ?>
                             <option value="<?=$p_value['id']?>"><?=$p_value['name']?></option>
                            <?php } ?>
                            </select>
                          
                           <span class="text-danger" id="parent_category_error"></span>
                        </div>
                     </div> -->
                   <!--   <div class="form-group">
                        <label class="col-sm-4 control-label" for="inputEmail3">Select Department<span class="asterisk">*</span></label>
                        <div class="col-sm-4">
                          <select name="product_category[department_id]" class="form-control" >
                            <option value="">Select Departments</option>
                            <?php foreach ($departments as $d_key => $d_value) { ?>
                             <option value="<?=$d_value['id']?>"><?=$d_value['name']?></option>
                            <?php } ?>
                            </select>
                          
                           <span class="text-danger" id="parent_category_error"></span>
                        </div>
                     </div> -->
                    </div>
                    <div class="btnSection">
                     
                         <button class="btn btn-default waves-effect waves-light m-l-5 btn-md" onclick="window.location='<?= ADMIN_PATH?>product_category'" type="reset">
                          back
                           </button>
                            <button class="btn btn-success pull-right waves-effect waves-light btn-md" name="commit" type="button" onclick="save_product_category('store'); ">
                         SAVE
                         </button>
                     
                  </div>
                  </form>
                </div>
              </div>
            </div>
         </div>
      </div>
   </div>
</div>