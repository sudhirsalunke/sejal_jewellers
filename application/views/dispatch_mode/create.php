<div class="content-page">
<div class="content">
   <div class="container">
      <div class="row">
         <div class="col-sm-6 card-box-wrap">
            <div class="panel panel-default">
              <div class="panel-heading frmHeading">
               <h4 class="inlineBlock"><span>Add Dispatch Mode</span></h4>                
              </div>
              <div class="panel-body">
                <div class="col-lg-12">
                  <form name="dispatch_mode_form" id="dispatch_mode_form" role="form" class="form-horizontal" method="post">
                    <?php $columns_array=array(
                                            array( 
                                                "display_name" => 'Name',
                                                "placeholder" => 'Enter Name',
                                                "name"=>'mode_name',
                                                "type" =>'text',
                                              ),
                                            array( 
                                                "display_name" => 'Type',
                                                "placeholder" => '',
                                                "name"=>'mode_type',
                                                "type" =>'dropdown',
                                                "dropdown_array"=>array(1=>"Textbox",
                                                                      2=>"Dropdown",
                                                                      3=>"Checkbox"),
                                                "array_type"=>1,/*1:indexed: 2:assoictaive*/
                                              ),
                                            array( 
                                                "display_name" => 'Image Upload',
                                                "placeholder" => '',
                                                "name"=>'is_img',
                                                "type" =>'checkbox',
                                              ),
                                        ); ?>

                      <?php foreach ($columns_array as $c_key => $c_value) { 
                          $display_name =$c_value['display_name'];
                          $placeholder =$c_value['placeholder'];
                          $name =$c_value['name'];
                          $type=$c_value['type'];
                      ?>
                        
                         <div class="form-group">
                        <label class="col-sm-4 control-label" for="inputEmail3"><?=$display_name?> <span class="asterisk">*</span></label>
                        <div class="col-sm-8">
                          <?php if(@$type=="text"){ ?>

                              <input type="text" placeholder="<?=$placeholder?>" class="form-control" name="dispatch_mode[<?=$name?>]">

                        <?php  } elseif(@$type=="dropdown") { ?>

                           <select name="dispatch_mode[<?=$name?>]" class="form-control" >
                            <option value="">Select <?=$display_name?></option>
                            <?php foreach ($c_value['dropdown_array'] as $key => $value) {

                                if ($c_value['array_type']==1) {
                                  $id=$key;
                                  $value=$value;
                                }else{
                                  $id=$value['id'];
                                  $value=$value['name'];
                                }

                             ?>
                             <option value="<?=$id?>"><?=$value?></option>
                            <?php } ?>
                            </select>

                         <?php } else if(@$type=="checkbox"){ ?>
                            <div class="checkbox checkbox-purple">
                                  <input type="checkbox" name="dispatch_mode[<?=$name?>]" value="1" id="check">
                                    <label for="check"></label>
                              </div>
                        <?php }

                          ?>
                           
                           <span class="text-danger" id="<?=$name?>_error"></span>
                        </div>
                     </div>

                     <?php } ?>
                     
                     
                    <div class="btnSection">
                         <button class="btn btn-default waves-effect waves-light m-l-5 btn-md" onclick="window.location='<?= ADMIN_PATH?>dispatch_mode'" type="reset">
                           back
                           </button>
                          <button class="btn btn-success waves-effect waves-light btn-md pull-right" name="commit" type="button" onclick="save_dispatch_mode('store'); ">
                         SAVE
                         </button>   
                     
                  </div>
                  </form>
                </div>
              </div>                
               <div class="clearfix"></div>
            </div>
         </div>
      </div>
   </div>
</div>