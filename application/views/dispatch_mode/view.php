<div class="content-page">
<div class="content">
   <div class="container">
      <div class="row">
         <div class="col-sm-6 card-box-wrap">
            <div class="panel panel-default">
              <div class="panel-heading frmHeading">
                <h4 class="inlineBlock"><span><?=$display_title?></span></h4>                
              </div>
              <div class="panel-body">
               <div class="col-lg-12">
                  <div class="col-sm-8">
                    <h4>Mode Name : <?=$dispatch_mode['mode_name']?></h4>
                    <h4>Mode Type : <?=input_types($dispatch_mode['mode_type'])?></h4>
                    <h4>Image Upload : <?=($dispatch_mode['is_img']==1) ?'Yes' : 'No';?></h4>                    
                  </div>
                  <div class="col-sm-12">
                    <form name="category" id="dispatch_mode_view" role="form" class="form-horizontal" method="post">
                     <div class="form-group">
                      <input id="enc_id" type="hidden" name="dispatch_mode_view[dispatch_mode_id]" value="<?=$dispatch_mode['encrypted_id']?>">
                        <label class="col-sm-3 control-label" for="inputEmail3">Enter Name <span class="asterisk">*</span></label>
                        <div class="col-sm-9">
                           <input type="text" placeholder="Enter Name" class="form-control" name="dispatch_mode_view[name]">
                           <span class="text-danger" id="name_error"></span>
                        </div>
                     </div>                 
                           <div class="form-group btnSection">   
                        <button type="button" onclick="window.history.go(-1); return false;" class="btn btn-md btn-default waves-effect waves-light" >BACK</button>                                 
                           <button type="button" class="btn btn-md btn-success pull-right" onclick="save_dispatch_mode_data('store')">SAVE</button>
                        
                        </div>
                   </form>
                  </div>
                  
                </div> 
              </div>                
               <div class="clearfix"></div>
              
            </div>
         </div>
         
        
      </div>
      <div class="clearfix">
      <div class="panel panel-default">
              
               <div class="panel-body">
                 <table class="table">
                    <thead>
                      <th>#</th>
                      <th>Name</th>
                      <th></th>
                    </thead>
                    <tbody>
                        <?php 
                        if (empty($dispatch_mode_data)) { ?>
                         <tr><td colspan="3">No Data Found</td><tr>
                       <?php }else{
                        foreach ($dispatch_mode_data as $key => $value) { ?>
                          <tr>
                            <td><?=$key+1?></td>
                            <td><?=$value['name']?></td>
                            <td></td>
                          </tr>
                       <?php } } ?>
                    </tbody>
                  </table>
               </div>               
               <div class="clearfix"></div>
              
            </div>
      </div>
   </div>
</div>