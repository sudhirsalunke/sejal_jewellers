<div class="content-page">
<div class="content">
   <div class="container">
      <div class="row">
         <div class="col-sm-12 card-box-wrap">
            <div class="panel panel-default">
               <div class="panel-heading">                  
                  <h4 class="inlineBlock"><?=$display_title;?></h4>
                  <div class="pull-right single-add btn-wrap3">
                  <a href="<?=ADMIN_PATH.'dispatch_mode/create';?>" class="pull-right add_senior_manager_button btn btn-primary waves-effect w-md waves-light m-t-5 btn-md single-add">Add Mode</a>
                  </div>
               </div>
               <div class="panel-body">
                  <div class="clearfix"></div>
                  <div class="table-rep-plugin">
                     <div class="table-responsive b-0 scrollhidden">         
                       <table class="table custdatatable table-bordered " id="dispatch_mode_table">
                           <thead>
                             <?php
                          $data['heading']='dispatch_mode_table';
                          $this->load->view('master/table_header',$data);
                        ?>
                              <!-- <tr>                            
                                 <th>#</th>
                                 <th>Mode Name</th>
                                 <th>Mode Type</th>
                                 <th>Image Upload</th>
                                 <th></th>
                              </tr> -->
                           </thead>
                        </table>
                     </div>
                  </div>
               </div>               
            </div>
         </div>
      </div>
   </div>
</div>