<div class="content-page">
<div class="content">
   <div class="container">
      <div class="row">
          <div class="col-sm-12 card-box-wrap">
            <div class="panel panel-default">
              <div class="panel-heading">
                <h4 class="inlineBlock"><span><?=$display_title;?></span></h4>
                       
              </div>
              <div class="panel-body">             

                  
                <div class="clearfix"></div>
                <div class="table-rep-plugin table-responsive">           

                  <table class="table table-bordered" id='design_tracking_table'>
                       <thead>
                        <tr>
                           <th>Order</th>
                           <th>Order Date</th>
                           <th>Delivery Date</th>
                           <th>Designs Code</th>
                           <th>Karigar</th>                     
                           <th>Sent To </th>
                           <th>Received From </th>              
                           <th>QC Passed</th>
                           <th>QC Rejected</th>
                           <th>Sent For HM</th>
                           <th>HM QC Accepted</th>
                           <th>Product Sent</th>
                        </tr>
             
                </thead>
         
              </table>
                </div>                
              </div>
            </div>
          </div>
         </div>
      </div>
   </div>
</div>
