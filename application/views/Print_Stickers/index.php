<div class="content-page">
<div class="content">
   <div class="container">
      <div class="row">
         <div class="col-sm-6 card-box-wrap">
            <div class="card-box padmobile  clearfix">
              <div class="panel-heading frmHeading">
               <h4 class="inlineBlock"><span>Print Stickers</span>
               </h4>
               </div>
                <div class="clearfix"></div>
                <div class="panel-body">
               <div class="product_listing">
                <?php if(!empty($product_data)){ 
                 $i=0; foreach($product_data as $val){ ?>
             <div class="col-sm-12 single_product_div">
                <div class=" input_data">
                  <form name="CreateStickersForm" id="CreateStickersForm" role="form" class="form-horizontal" method="post">
                    <input type="hidden" name="qc_id" value="<?=$qc_id;?>">
                  <table style="">
                    <tbody>
                      <tr>
                        <td><label class="karigar_name control-label" for="inputEmail3"> Product Code  </label>
                        </td><td><span class="">-</span></td>
                        <td><div class=" karigar_name"><?=$val['product_code']; ?></div></td>
                      </tr>
                      <tr>
                        <td><label class=" control-label" for="inputEmail3"> Category </label>
                          </td><td><span class="">-</span></td>
                        <td><div class=""><?=$val['category_name']; ?></div></td>
                      </tr>
                       <tr>
                        <td><label class=" control-label" for="inputEmail3"> Order ID </label>
                          </td><td><span class="">-</span></td>
                        <td><div class=""><?=$val['order_id']; ?></div></td>
                      </tr>
                       <tr>
                        <td><label class=" control-label" for="inputEmail3"> Date </label>
                          </td><td><span class="">-</span></td>
                        <td><div class=""><?=date("d-m-Y",strtotime($val['created_at'])); ?></div></td>
                      </tr>
                      <tr>
                        <td><label class="control-label" for="inputEmail3"> Size </label>
                          </td><td><span class="">-</span></td>
                        <td><div class=""><?=$val['size']; ?></div></td>
                      </tr>
                      <tr>
                        <td><label class=" control-label" for="inputEmail3"> Quantity </label>
                          </td><td><span class="">-</span></td>
                        <td><div class=""><?=$val['quantity']; ?></div></td>
                      </tr>
                      <tr>
                        <td><label class=" control-label" for="inputEmail3">Total Gr. Weight </label>
                          </td><td><span class="">-</span></td>
                        <td><?=$val['gr_wt']; ?></td>
                      </tr> 
                      <tr>
                        <td><label class=" control-label" for="inputEmail3"> Total Net Weight </label>
                          </td><td><span class="">-</span></td>
                        <td><?=$val['net_wt']; ?></td>
                      </tr> 
                      <tr>
                        <td><label class=" control-label" for="inputEmail3"> Purity </label>
                          </td><td><span class="">-</span></td>
                        <td><input type="text" class="form-control" id="purity_input" name="purity" value="<?=$val['purity']; ?>" placeholder="enter purity"></td>
                      </tr>
                      <tr>
                        <td><label class=" control-label" for="inputEmail3"> Done By </label>
                          </td><td><span class="">-</span></td>
                        <td><div class=""><?=$user_id; ?> - <?=$user_name; ?></div></td>
                      </tr>
                    <!--   <tr>
                      <td colspan="3" style="text-align: center;"><button class="btn btn-primary waves-effect waves-light  btn-md" name="commit" type="button" onclick="Create_Stickers(); ">
                       SAVE
                       </button>
                     <button class="btn btn-danger waves-effect waves-light m-l-5  btn-md" onclick="window.location='<?= ADMIN_PATH?>Corporate_Dashboard'" type="reset">
                         CANCEL
                     </button></td>
                                        </tr> -->
                    </tbody>  
                  </table>
                  <div class="form-group btnSection m-t-15 clearfix">
                     <button class="btn btn-default waves-effect waves-light m-l-5  btn-md" onclick="window.location='<?= ADMIN_PATH?>Corporate_Dashboard'" type="reset">
                           Back
                       </button>
                    <button class="btn btn-success waves-effect waves-light  btn-md pull-right" name="commit" type="button" onclick="Create_Stickers(); ">
                         SAVE
                         </button>
                  </div>
                </form>
                </div>
              
              
              
            <div class="print_area stick" style="display:none;">  
              <div class="col-sm-6">
                  <table style="">
                    <tbody>
                      <tr>
                        <td><label class="karigar_name control-label" for="inputEmail3"> Product Code  </label>
                        </td><td><span class="">-</span></td>
                        <td><div class=" karigar_name"><?=$val['product_code']; ?></div></td>
                      </tr>
                      <tr>
                        <td><label class=" control-label" for="inputEmail3"> Category  </label>
                          </td><td><span class="">-</span></td>
                        <td><div class=""><?=$val['category_name']; ?></div></td>
                      </tr>
                      <tr>
                        <td><label class=" control-label" for="inputEmail3"> Order ID </label>
                          </td><td><span class="">-</span></td>
                        <td><div class=""><?=$val['order_id']; ?></div></td>
                      </tr>
                       <tr>
                        <td><label class=" control-label" for="inputEmail3"> Date </label>
                          </td><td><span class="">-</span></td>
                        <td><div class=""><?=date("d-m-Y",strtotime($val['created_at'])); ?></div></td>
                      </tr>
                      <tr>
                        <td><label class="control-label" for="inputEmail3"> Size </label>
                          </td><td><span class="">-</span></td>
                        <td><div class=""><?=$val['size']; ?></div></td>
                      </tr>
                      <tr>
                        <td><label class=" control-label" for="inputEmail3"> Quantity </label>
                          </td><td><span class="">-</span></td>
                        <td><div class=""><?=$val['quantity']; ?></div></td>
                      </tr>
                      <tr>
                        <td><label class=" control-label" for="inputEmail3">Total Gr. Weight </label>
                          </td><td><span class="">-</span></td>
                        <td><?=$val['gr_wt']; ?></td>
                      </tr> 
                      <tr>
                        <td><label class=" control-label" for="inputEmail3"> Total Net Weight </label>
                          </td><td><span class="">-</span></td>
                        <td><?=$val['net_wt']; ?></td>
                      </tr>  
                      <tr>
                        <td><label class=" control-label" for="inputEmail3"> Purity </label>
                          </td><td><span class="">-</span></td>
                        <td><div id="purity"><?=$val['purity']; ?></div></td>
                      </tr>
                      <tr>
                        <td><label class=" control-label" for="inputEmail3"> Done By </label>
                          </td><td><span class="">-</span></td>
                        <td><div class=""><?=$user_id; ?> - <?=$user_name; ?></div></td>
                      </tr>
                      <tr class="button_html">

                        <td colspan="3" style="text-align: center;">
                        <a href="<?=ADMIN_PATH.'Print_Stickers/createPdf/'.$qc_id;?>" target="_blank" class="btn btn-primary small loader-hide  btn-md"><!--  <button class="btn btn-primary waves-effect waves-light  btn-md"  name="commit" type="button"> -->
                         PRINT
                         <!-- </button> --></a>
                       <button class="btn btn-danger waves-effect waves-light m-l-5  btn-md" onclick="window.location='<?= ADMIN_PATH?>Print_Stickers/index/<?=$qc_id?>'" type="reset">
                           EDIT
                       </button></td>
                    </tr>
                    </tbody>  
                  </table>
                </div>
            </div>
            <?php } } ?>
            </div>
               </div>
               </div>
             </div>
            </div>
         </div>
      </div>
   </div>
</div>

<style>

.product_listing >div
{
  background-color: #fff;
}

table td{
  padding: 5px;
}

@media print {
 

  body * {
    visibility: hidden;
  }
  .print_area, .print_area * {
    visibility: visible;
  }
  .input_data
  {
    display: none;
  }
  .button_html
  {
    display: none;
  }
  .print_area {
    
    left: 0;
    top: 0;
  }
  .pgbrk 
  {
    page-break-after: always;
  }
}

</style>