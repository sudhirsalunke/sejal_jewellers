<div class="content-page">
<div class="content">
   <div class="container">
      <div class="row">
         <div class="col-sm-12 card-box-wrap">
            <div class="panel panel-default">
              <div class="panel-heading">
               <h4 class="inlineBlock"><span>Edit Hallmarking Center</span></h4>                
              </div>
              <div class="panel-body">
                <div class="col-lg-12">
                  <form name="hallmarking_center" id="hallmarking_center" role="form" class="form-horizontal" method="post">
                  <input type="hidden" name="hallmarking_center[encrypted_id]" value="<?=@$hallmarking_center['encrypted_id']?>">
                      <div class="form-group">
                        <label class="col-sm-4 control-label" for="inputEmail3"> Enter Hallmarking Center <span class="asterisk">*</span></label>
                        <div class="col-sm-4">
                           <input type="text" placeholder="Enter Hallmarking Center" class="form-control" name="hallmarking_center[name]" value="<?=@$hallmarking_center['name']?>">
                           <span class="text-danger" id="name_error"></span>
                        </div>
                     </div>
                     <div class="form-group">
                        <label class="col-sm-4 control-label" for="inputEmail3"> Enter Center Code </label>
                        <div class="col-sm-4">
                           <input type="text" placeholder="Enter Center Code" class="form-control" name="hallmarking_center[code]" value="<?=@$hallmarking_center['code']?>">
                           <span class="text-danger" id="code_error"></span>
                        </div>
                     </div>
                    <div class="form-group">
                     <div class="col-sm-offset-5 col-sm-8 texalin">
                         <button class="btn btn-primary waves-effect waves-light  btn-md" name="commit" type="button" onclick="save_hallmarking_center('update'); ">
                         SAVE
                         </button>
                         <button class="btn btn-default waves-effect waves-light m-l-5  btn-md" onclick="window.location='<?= ADMIN_PATH?>hallmarking_center'" type="reset">
                           CANCEL
                           </button>
                     </div>
                  </div>
                  </form>
                </div>
              </div>              
               <div class="clearfix"></div>
            </div>
         </div>
      </div>
   </div>
</div>