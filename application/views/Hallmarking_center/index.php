<div class="content-page">
<div class="content">
   <div class="container">
      <div class="row">
         <div class="col-sm-12 card-box-wrap">
            <div class="panel panel-default">
              <div class="panel-heading">                
               <h4 class="inlineBlock">All Hallmarking Center</h4>
                <a href="<?= ADMIN_PATH?>hallmarking_center/create" class="pull-right add_senior_manager_button btn btn-primary  waves-effect w-md waves-light m-t-5 btn-md">ADD HALLMARKING CENTER</a>
              </div>
              <div class="panel-body">
                <div class="clearfix"></div>
                <div class="table-rep-plugin">
                  <div class="table-responsive b-0 scrollhidden">
                    <table class="table table-bordered custdatatable" id="hallmarking_center_table">
                      <thead>
                        <tr>
                          <th class="col4">Center Name</th>
                          <th class="col4">Code</th>
                          <th class="col3"></th>
                        </tr>
                      </thead>
                    </table>
                  </div>
                </div>
              </div>
            </div>
         </div>
      </div>
   </div>
</div>
</div>