<div class="content-page">
<div class="content">
   <div class="container">
      <div class="row">
         <div class="col-sm-6 card-box-wrap">
            <div class="panel panel-default">
               <div class="panel-heading frmHeading">
                 <h4 class="inlieBlock"><span>Edit Term</span></h4>    
               </div>          
              <div class="panel-body">
                <div class="col-lg-12">
                  <form name="term_master_form" id="term_master_form" role="form" class="form-horizontal" method="post">
                   <input type="hidden" name="encrypted_id" value="<?=$terms['encrypted_id']?>">
                    <?php $columns_array=array(
                                            array( 
                                                "display_name" => 'Parent Category',
                                                "placeholder" => '',
                                                "name"=>'parent_category_id',
                                                "type" =>'dropdown',
                                                "dropdown_array"=>$parent_Category,
                                                "required"=>'*',
                                                "ex_val"=>$terms['parent_category_id'],
                                              ),
                                            array( 
                                                "display_name" => 'Code',
                                                "placeholder" => 'Enter Code',
                                                "name"=>'code',
                                                "type" =>'text',
                                                //"required"=>'*',
                                                "ex_val"=>$terms['code'],
                                              ),
                                            array( 
                                                "display_name" => 'Hard Cash',
                                                "placeholder" => 'Enter Hard Cash',
                                                "name"=>'hard_cash',
                                                "type" =>'text',
                                                "required"=>'*',
                                                "ex_val"=>$terms['hard_cash'],
                                              ),
                                            array( 
                                                "display_name" => 'Normal',
                                                "placeholder" => 'Enter Normal',
                                                "name"=>'normal',
                                                "type" =>'text',
                                                "required"=>'*',
                                                "ex_val"=>$terms['normal'],
                                              ),
                                            array( 
                                                "display_name" => '7 Days',
                                                "placeholder" => '7 days',
                                                "name"=>'7days',
                                                "type" =>'text',
                                                "required"=>'*',
                                                "ex_val"=>$terms['7days'],
                                              ),
                                             array( 
                                                "display_name" => '15 Days',
                                                "placeholder" => '15 days',
                                                "name"=>'15days',
                                                "type" =>'text',
                                                "required"=>'*',
                                                "ex_val"=>$terms['15days'],
                                              ),
                                              array( 
                                                "display_name" => '1 Month',
                                                "placeholder" => '1 Month',
                                                "name"=>'1month',
                                                "type" =>'text',
                                                "required"=>'*',
                                                "ex_val"=>$terms['1month'],
                                              ),
                                              array( 
                                                "display_name" => '2 Month',
                                                "placeholder" => '2 month',
                                                "name"=>'2month',
                                                "type" =>'text',
                                                "required"=>'*',
                                                "ex_val"=>$terms['2month'],
                                              ),
                                            
                                        ); ?>

                      <?php foreach ($columns_array as $c_key => $c_value) { 
                          $display_name =$c_value['display_name'];
                          $placeholder =$c_value['placeholder'];
                          $name =$c_value['name'];
                          $type=$c_value['type'];
                        
                          $required=@$c_value['required'];
                          $old_value=$c_value['ex_val'];
                      ?>
                        
                         <div class="form-group col-sm-12">
                        <label class="col-sm-3 control-label" for="inputEmail3"><?=$display_name?> 
                        <span class="asterisk"><?=$required?></span></label>
                        <div class="col-sm-8">
                          <?php if(@$type=="text"){ ?>

                              <input type="text" placeholder="<?=$placeholder?>" class="form-control" name="terms[<?=$name?>]"  value="<?=$old_value;?>">

                        <?php  } elseif(@$type=="dropdown") { ?>

                           <select name="terms[<?=$name?>]" class="form-control">
                            <option value="">Select <?=$display_name?></option>
                            <?php foreach ($c_value['dropdown_array'] as $key => $value) { ?>
                             <option value="<?=$value['id']?>" <?=($old_value ==$value['id']) ? 'selected' : ''; ?>><?=$value['name']?></option>
                            <?php } ?>
                            </select>

                         <?php } else if(@$type=="radio"){

                         }

                          ?>
                           
                           <span class="text-danger" id="<?=$name?>_error"></span>
                        </div>
                     </div>

                     <?php } ?>

                    <div class="btnSection">
                       
                         <button class="btn btn-default waves-effect waves-light m-l-5 btn-md" onclick="window.location='<?= ADMIN_PATH?>Term'" type="reset">
                           back
                           </button>
                            <button class="btn btn-success pull-right waves-effect waves-light btn-md" name="commit" type="button" onclick="save_terms('update'); ">
                         SAVE
                         </button>
                  </div>
                  </form>
              </div>
              </div>
            </div>
         </div>
      </div>
   </div>
</div>