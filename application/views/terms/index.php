<div class="content-page">
<div class="content">
   <div class="container">
      <div class="row">
         <div class="col-sm-12 card-box-wrap">
            <div class="panel panel-default">
               <div class="panel-heading">                  
                  <h4 class="inlineBlock">All Terms</h4>
                  <a href="<?=ADMIN_PATH.'Term/create'?>" class="pull-right add_senior_manager_button btn btn-purp waves-effect w-md waves-light m-t-5 btn-md single-send">ADD TERM</a>
               </div>
               <div class="panel-body">
                  <div class="clearfix"></div>
                  <div class="table-rep-plugin">
                     <div class="table-responsive b-0 scrollhidden">
                        <table class="table table-bordered ustdatatable" id="terms_table">
                        <thead>
                        <?php
                          $data['heading']='terms_table';
                          $this->load->view('master/table_header',$data);
                        ?>
                           <!-- <tr>
                              <th class="col4">#</th>
                              <th class="col3">Parent Category</th>
                              <th class="col3">Code</th>
                              <th class="col3">Hard Cash</th>
                              <th class="col3">Normal</th>
                              <th class="col3">7 days</th>
                              <th class="col3">15 days</th>
                              <th class="col3">1 Month</th>
                              <th class="col3">2 Month</th>
                              <th></th>
                           </tr> -->
                        </thead>
                        </table>
                     </div>
                  </div>
               </div>
            </div>
         </div>
      </div>
   </div>
</div>