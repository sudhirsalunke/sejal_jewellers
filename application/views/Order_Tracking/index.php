<div class="content-page">
<div class="content">
   <div class="container">
      <div class="row">
          <div class="col-sm-12 card-box-wrap">
            <div class="panel panel-default">
              
                <h4 class="inlineBlock"><span><?=$display_title;?></span></h4>
                       
              
              <div class="panel-body">             

                  
                <div class="clearfix"></div>
                <div class="table-rep-plugin table-responsive">           

                  <table class="table table-bordered" id='order_tracking_table'>
                       <thead>
                        <tr>
                           <th>Order #</th>
                           <th>Order Date</th>
                           <th>Delivery Date</th>
                           <th>Total Designs</th>
                           <th colspan="2" style="text-align: center" >Assigned To Karigar</th>
                           <th colspan="2" style="text-align: center" >Sent To Karigar</th>
                           <th colspan="2" style="text-align: center" >Received From Karigar</th>
                           <th colspan="2" style="text-align: center" >QC Pending</th>
                           <th colspan="2" style="text-align: center" >QC Passed</th>
                           <th colspan="2" style="text-align: center" >QC Rejected</th>
                           <th colspan="2" style="text-align: center" >Pending For Hallmarking</th>
                           <th colspan="2" style="text-align: center" >Sent For Hallmarking</th>
                           <th colspan="2" style="text-align: center" >Hallmarking QC Accepted</th>
                           <th colspan="2" style="text-align: center" >Product sent</th>
                        </tr>
                        <tr>                         
                           <th></th>
                           <th></th>
                           <th></th>
                           <th></th>
                           <th class="col4">Completed</th>
                           <th class="col4">Total Pending</th>
                           <th class="col4">Completed</th>
                           <th class="col4">Total Pending</th>
                           <th class="col4">Completed</th>
                           <th class="col4">Total Pending</th>
                           <th class="col4">Completed</th>
                           <th class="col4">Total Pending</th>
                           <th class="col4">Completed</th>
                           <th class="col4">Total Pending</th>
                           <th class="col4">Completed</th>
                           <th class="col4">Total Pending</th>
                           <th class="col4">Completed</th>
                           <th class="col4">Total Pending</th>
                           <th class="col4">Completed</th>
                           <th class="col4">Total Pending</th>
                           <th class="col4">Completed</th>
                           <th class="col4">Total Pending</th>
                           <th class="col4">Completed</th>
                           <th class="col4">Total Pending</th>
                        </tr>
                </thead>
            <!--     <tbody>
                
                     </tbody> -->
              </table>
                </div>                
              </div>
            </div>
          </div>
         </div>
      </div>
   </div>
</div>
