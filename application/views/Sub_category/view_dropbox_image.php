<div class="content-page">
<div class="content">
   <div class="container">
      <div class="row">
         <div class="col-sm-12 card-box-wrap">
            <div class="card-box padmobile">
               <h4 class="header-title business_manager_header_title m-t-0 m-b-30 bmsmheader bmsmheaderabm"><span>Submit Sub Category Images</span>
               </h4>
               <div class="col-lg-12">
                  <form name="category" id="Sub_category" role="form" class="form-horizontal" method="post">
                  <input type="hidden" name="Sub_category[encrypted_id]" value="<?= $sub_category['encrypted_id'] ?>">

                    <div class="form-group">
                        <label class="col-sm-4 control-label" for="inputEmail3"> Select Category :</label>
                        <div class="col-sm-4">
                          
                          <?php
                            if(!empty($category)){
                              foreach ($category as $key => $value) {
                                 if( $sub_category['category_id'] == $value['id'] )
                                 {
                                    ?>
                                      <label class="col-sm-4 control-label" for="inputEmail3"> <?=$value['name']?> </label>
                                    <?php
                                 }
                              }
                            }
                          ?>
                          
                        </div>
                     </div>

                     <div class="form-group">
                        <label class="col-sm-4 control-label" for="inputEmail3"> Enter Sub Category Name :</label>
                        <div class="col-sm-4">
                           <label class="col-sm-4 control-label" for="inputEmail3"> <?= $sub_category['name'] ?> </label>
                        </div>
                     </div>
                     <div class="form-group">
                        <label class="col-sm-4 control-label" for="inputEmail3"> Enter Sub Category Code :</label>
                        <div class="col-sm-4">               
                            <label class="col-sm-4 control-label" for="inputEmail3"> <?= $sub_category['code']?> </label>
                        </div>
                     </div>
                     <br/><br/> 
                      <!-- <div class="form-group">
                        <div class="col-sm-offset-4 col-sm-8 texalin">
                        </div>
                      </div> -->
                      <div class="row" style="margin-left:18%;">
                          <div class="form-group" id="sub_image_div">                                                                   
                           <?php 
                            // echo UPLOAD_IMAGES.'sub_category/'.';
                              $handle = opendir("uploads/sub_category/".$sub_category['name']."/thumb/");
                              /*echo $_SERVER['DOCUMENT_ROOT']."/shilpi_jewellers/uploads/sub_category/".$sub_category['name']."/thumb/";*/
                              $count = 1;
                              while($file = readdir($handle)){
                                  if($file !== '.' && $file !== '..'){
                                      ?>
                                      <div class="col-md-2">
                                              <label class="btn mul_image_check_lbl"  id="check_image<?= $count ?>">
                                                  <img src="<?= UPLOAD_IMAGES.'sub_category/'.$sub_category['name'].'/small/'.$file ?>" alt="..." class="img-thumbnail img-check">
                                                  <input type="checkbox" name="sub_cat_img[]" id="item4" value="<?= $file ?>" autocomplete="off" class="hidden check_image<?= $count ?>">
                                              </label>
                                      </div>   
                                      <?php
                                       $count++;
                                      /*echo '<input type="checkbox" name="sub_cat_img[]" value="'.$file.'"/>';
                                      echo '<a href="'.UPLOAD_IMAGES.'sub_category/'.$sub_category['name'].'/thumb/'.$file.'" target="blank"><img src="'.UPLOAD_IMAGES.'sub_category/'.$sub_category['name'].'/thumb/'.$file.'" border="0" /></a>&nbsp; ';         
                                      */   
                                  }
                              }                      
                          ?>                         
                          </div>                                        
                     </div>
                    <div class="row" style="margin-left:18%;">                           
                          <span class="text-danger" id="sub_cat_img_error" style="display:none;">Select subcategory image</span>          
                     </div>  
                     <br/><br/>
                    <div class="form-group">
                     <div class="col-sm-offset-3 col-sm-4 texalin">
                         <button class="btn btn-primary waves-effect waves-light  btn-md" name="commit" type="button" onclick="update_sub_category_images()">
                         SAVE
                         </button>
                         <button class="btn btn-default waves-effect waves-light m-l-5  btn-md" onclick="window.location='<?= ADMIN_PATH?>Sub_category'" type="reset">
                           CANCEL
                           </button>
                     </div>
                  </div>
                  </form>
               </div>
               <div class="clearfix"></div>
            </div>
         </div>
      </div>
   </div>
</div>