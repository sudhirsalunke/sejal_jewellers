<div class="content-page">
<div class="content">
   <div class="container">
      <div class="row">
         <div class="col-sm-6 card-box-wrap">
            <div class="panel panel-default">
              <div class="panel-heading frmHeading">
                <h4 class="inlineBlock"><span>Edit Sub Category</span></h4>
              </div>
              <div class="panel-body">
                <div class="col-lg-12">
                  <form name="category" id="Sub_category" role="form" class="form-horizontal" method="post">
                  <input type="hidden" name="Sub_category[encrypted_id]" value="<?= $sub_category['encrypted_id'] ?>">

                    <div class="form-group">
                        <label class="col-sm-4 control-label" for="inputEmail3"> Select Category <span class="asterisk">*</span></label>
                        <div class="col-sm-8">
                          <select class="form-control chosen-select" name=Sub_category[category_id]>
                          <option value=''>Select Category</option>
                          <?php
                            if(!empty($category)){
                              foreach ($category as $key => $value) {
                                ?>
                                <option value="<?=$value['id']?>" <?= ($sub_category['category_id'] == $value['id']) ? 'selected' : ''?>><?=$value['name']?></option>
                                <?php
                              }
                            }
                          ?>
                          </select>
                          <span class="text-danger" id="category_id_error"></span>
                        </div>
                     </div>

                     <div class="form-group">
                        <label class="col-sm-4 control-label" for="inputEmail3"> Enter Sub Category Name <span class="asterisk">*</span></label>
                        <div class="col-sm-8">
                           <input type="text" placeholder="Enter Sub Category Name" id="name" class="form-control" name="Sub_category[name]" value="<?= $sub_category['name'] ?>">
                           <span class="text-danger" id="name_error"></span>
                        </div>
                     </div>
                     <div class="form-group">
                        <label class="col-sm-4 control-label" for="inputEmail3"> Enter Sub Category Code <span class="asterisk">*</span></label>
                        <div class="col-sm-8">
                           <input type="text" placeholder="Enter Sub Category Code" id="code" class="form-control" name="Sub_category[code]" value="<?= $sub_category['code']?>" readonly>
                           <span class="text-danger" id="code_error"></span>
                        </div>
                     </div>
                      <div class="form-group">
                        <label class="col-sm-4 control-label" for="inputEmail3"> Select Pair/PCS <span class="asterisk">*</span> </label>
                        <?php if($sub_category['pair_pcs'] =='0'){
                          $PAIR='checked'; $PCS=''; 
                          }else{
                          $PAIR=''; $PCS='checked';
                          } ?>
                        <div class="col-sm-8">                  
                      <input type="radio" name="Sub_category[pair_pcs]" value="0" <?php echo $PAIR;?> > PAIR &nbsp;&nbsp;&nbsp;&nbsp; 
                      <input type="radio" name="Sub_category[pair_pcs]" value="1" <?php echo $PCS;?> > PCS 
                           <span class="text-danger" id="code_error"></span>
                        </div>
                      </div>
                    <div class="btnSection">                      
                      
                      <button class="btn btn-default waves-effect waves-light m-l-5 btn-md" onclick="window.location='<?= ADMIN_PATH?>Sub_category'" type="reset">BACK</button>

                      <button class="btn btn-success pull-right waves-effect waves-light btn-md" name="commit" type="button" onclick="update_sub_category(); ">SAVE</button>                      
                    </div>
                  </form>
               </div>
               <div class="clearfix"></div>
              </div>
            </div>
         </div>
      </div>
   </div>
</div>