<div class="content-page">
<div class="content">
   <div class="container">
      <div class="row">
         <div class="col-sm-6 card-box-wrap">
            <div class="card-box padmobile">
                <div class="panel-heading frmHeading">
                   <h4 class="header-title business_manager_header_title bmsmheader bmsmheaderabm "><span>Sub Category Details</span>
                   </h4>
               </div>
               <div class="col-lg-12">
                  <form name="category" id="Sub_category" role="form" class="form-horizontal" method="post">
                  <input type="hidden" name="Sub_category[encrypted_id]" value="<?= $sub_category['encrypted_id'] ?>">

                    <div class="form-group">
                        <label class="col-sm-4 control-label" for="inputEmail3"> Select Category :</label>
                        <div class="col-sm-8">
                          
                          <?php
                            if(!empty($category)){
                              foreach ($category as $key => $value) {
                                 if( $sub_category['category_id'] == $value['id'] )
                                 {
                                    ?>
                                      <label class="col-sm-4 control-label text-left" for="inputEmail3"> <?=$value['name']?> </label>
                                    <?php
                                 }
                              }
                            }
                          ?>
                          
                        </div>
                     </div>

                     <div class="form-group">
                        <label class="col-sm-4 control-label" for="inputEmail3"> Enter Sub Category Name :</label>
                        <div class="col-sm-8 text-left">
                           <label class="col-sm-4 control-label" for="inputEmail3"> <?= $sub_category['name'] ?> </label>
                        </div>
                     </div>
                 
                        <div class="form-group">
                        <label class="col-sm-4 control-label" for="inputEmail3"> Enter Sub Category Code :</label>
                        <div class="col-sm-8">
                    
                            <label class="col-sm-4 control-label text-left" for="inputEmail3"> <?= $sub_category['pair_pcs']?> </label>
                        </div>
                     </div>
                         <div class="form-group">
                        <label class="col-sm-4 control-label" for="inputEmail3"> Select Pair/PCS :</label>
                        <div class="col-sm-8">
                     <?php if($sub_category['pair_pcs'] =='0'){
                          $pair_pcs='PAIR'; 
                          }else{
                          $pair_pcs='PCS'; 
                          } ?>
                            <label class="col-sm-4 control-label text-left" for="inputEmail3"> <?php echo $pair_pcs;?> </label>
                        </div>
                     </div>
                     <br/><br/>   
                     <div class="form-group">
                     <!-- <div class="col-sm-offset-4 col-sm-8 texalin">                       
                         
                     </div> -->
                     <div class="row" style="margin-left:18%">
                            <div class="form-group">   
                             <?php                        
                             foreach($sub_category_images as $val) {
                               //echo '<a class="col-sm-2" href="'.UPLOAD_IMAGES.'sub_category/'.$sub_category['name'].'/thumb/'.$val['sub_cat_image'].'" target="blank"><img src="'.UPLOAD_IMAGES.'sub_category/'.$sub_category['name'].'/thumb/'.$val['sub_cat_image'].'"></a>';
                               ?>                                
                                  <div class="col-md-2">
                                      <label class="btn">
                                         <a href="<?php echo UPLOAD_IMAGES.'sub_category/'.$sub_category['name'].'/large/'.$val['sub_cat_image'];?>" target="blank"><img src="<?php echo UPLOAD_IMAGES.'sub_category/'.$sub_category['name'].'/small/'.$val['sub_cat_image'];?>" class="img-thumbnail img-check"></a>
                                          <!-- <img src="http://content.nike.com/content/dam/one-nike/globalAssets/menu_header_images/OneNike_Global_Nav_Icons_Running.png" alt="..." class="img-thumbnail img-check"> -->
                                         </label>
                                  </div>                                                                                                        
                               <?php               
                              }                       
                          ?>  
                          </div>                                      
                     </div> 
                    </div> 
                     <br/><br/>
                  </form>
               </div>
               <div class="clearfix"></div>
            </div>
         </div>
      </div>
   </div>
</div>