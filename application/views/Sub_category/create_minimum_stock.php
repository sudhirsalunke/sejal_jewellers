<div class="content-page">
<div class="content">
   <div class="container">
      <div class="row">
         <div class="col-sm-12 card-box-wrap">
            <div class="panel panel-default">
               <div class="panel-heading">
                 <h4 class="inlineBlock"><span>Create Minimum Stock</span></h4>
               </div>               
               <div class="panel-body">
                 <div class="col-lg-12">
                  <form name="sub_category" id="minimum_stock" role="form" class="form-horizontal" method="post">
                   <div class="form-group">
                        <label class="col-sm-4 control-label" for="inputEmail3"><h4 class="header-title business_manager_header_title m-t-0 m-b-30 bmsmheader bmsmheaderabm"> Category Name : <?= $category['name']?></h4></label>
                        <label class="col-sm-4 control-label" for="inputEmail3"><h4 class="header-title business_manager_header_title m-t-0 m-b-30 bmsmheader bmsmheaderabm"> Sub Category Name : <?= $sub_category['name']?> </h4></label>
                        
                     </div>
                     <input type="hidden" name="category_id" value="<?= $sub_category['id']?>">
                     <div class="form-group">
                        <label class="col-sm-4 control-label" for="inputEmail3"> Weight Range</label>
                        <label class="col-sm-2 control-label" for="inputEmail3">Stock</label>
                     </div>
                     <?php
                        foreach ($weight_ranges as $key => $value) {
                          ?>
                              <div class="form-group">
                                <label class="col-sm-4 control-label" for="inputEmail3"> <?php echo  $value['from_weight'].' - '.$value['to_weight']?></label>
                                <?php 
                                  $i_value = '';
                                  foreach ($stock as $s_key => $s_value) {
                                    if($s_value['weight_range_id'] == $value['id']){
                                      $i_value = $s_value['stock'];
                                    }
                                  }
                                  ?>
                                    <div class="col-sm-4">
                                       <input type="text" placeholder="Enter Minimum Stock" id="code" class="form-control" name="<?= $value['id']?>" required="" value="<?= $i_value;?>">
                                       <span class="text-danger" id="<?= $value['id']?>_error"></span>
                                    </div>
                                
                              </div>
                          <?php
                        }
                     ?>
                     
                    <div class="btnCenter">
                     
                         <button class="btn btn-primary waves-effect waves-light  btn-md" name="commit" type="button" onclick="save_minimum_stock(); ">
                         SAVE
                         </button>
                         <button class="btn btn-default waves-effect waves-light m-l-5  btn-md" onclick="window.location='<?= ADMIN_PATH?>Sub_category'" type="reset">
                           CANCEL
                           </button>
                     
                  </div>
                  </form>
               </div>
               </div>
            </div>
         </div>
      </div>
   </div>
</div>