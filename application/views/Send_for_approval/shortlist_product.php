<div class="table-responsive">

            <table class="table table-bordered custdatatable" id="sortlist_table" <?=@$add_class?>>
             <h4 class="col-sm-12 header-title business_manager_header_title m-t-0 m-b-30 bmsmheader bmsmheaderabm"  id="shortlist_title" <?=@$add_class?>><span>Shortlisted Designs:-<span id="shortlist_count" style="font-size: 15px; font-weight: bold;"> <?= (!empty($shortlisted) ? count($shortlisted) : '0')?> </span></span>
               <div class="col-sm-6 pull-right">
                <div class="col-sm-6">
                  <input type="hidden" id="" class="form-control" name="user_id"  >
                  <input type="hidden" id="count_value_hidden" class="form-control" name="total_count">
                </div>
              </div>
          </h4>
          <thead>
           <tr>
            <th class="col4">Sr No</th>
             <th class="col4">Date || Time</th>
             <th class="col4">Code</th>
             <th class="col4">Size</th>
             <th class="col4">Karigar</th>
             <th class="col4">Metal</th>
             <th class="col4">Category</th>
             <th class="col4">Sub Category</th>
             <th class="col4">Carat</th>
             <th class="col4">WT Range</th>
             <th class="col4">Image</th>
             <th class="col3"></th>
           </tr>
         </thead>
         <?php 
         if(!empty($shortlisted)){
          foreach ($shortlisted as $key => $value) { 
            ?>                   
            <tbody id="table_html">
              <tr id="<?=$value['id']?>">

                <td class="col4"><span style="float:right"><?= $key+1;?></span></td>
                <td class="col4"><span style="float:right"><?= date("d-m-Y",strtotime($value['created_at']));?></span></td>
                <td class="col4"><?=$value["product_code"];?></td>
                <td class="col4"><?=$value["size"];?></td>
                <td class="col4"><?=$value["karigar_name"]?></td>
                <td class="col4"><?=$value["metal_name"];?></td>
                <td class="col4"><?=$value["category_name"]?></td>
                <?php 
                  if(empty($value["sub_category_name"])){
                ?>
                  <td class="col4"></td>
                <?php }else{?>
                <td class="col4"><?=$value["sub_category_name"]?></td>
                <?php }?>
                <td class="col4"><?=$value["carat_name"];?></td>
                <td class="col4"><?='<span style="float:right">'. $value["from_weight"]."-".$value['to_weight'].'</span>'?></td>
                <td class="col4"><?php if($value['image']!='') {?>
                 <!-- <img src="<?= UPLOAD_IMAGES.'product'.'/'.$value['product_code'].'/'.'thumb'.'/'.$value['image']?>" > -->
                 <img onclick="img_popup(<?=$key?>)" id="<?='img_'.$key?>"" src="<?= UPLOAD_IMAGES.'product'.'/'.$value['product_code'].'/'.'thumb'.'/'.$value['image']?>"></td>
                 <?php }else { ?>
                 No-Imgae
                 <?php } ?>
                 <td class="col3"><button class="btn btn-danger waves-effect waves-light btn-sm" name="commit" type="button" onclick="removeButton(this)">REMOVE</button></td>
               </tr>
             </tbody>
             <?php }
           }else{ 
            ?>
            <tr id="empty_tr">
              <td>No Designs shortlisted</td>
              <td></td>
              <td></td>
              <td></td>
              <td></td>
              <td></td>
              <td></td>
              <td></td>
              <td></td>
              <td></td>

            </tr>
            <?php 
          }
          ?>
        </table>
        </div>