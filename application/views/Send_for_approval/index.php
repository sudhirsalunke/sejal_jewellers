<div class="content-page">
  <div class="content">
    <div class="container">
      <div class="row">
        <div class="col-sm-12 card-box-wrap marg_bottom">         
          <div class="panel panel-default">
            <div class="panel-heading frmHeading">
              <h4 class="inlineBlock">Send Designs For Approval</h4>

              <div class="pull-right wrap-big">
              <button class="btn btn-primary waves-effect waves-light btn-md" name="shortlisted" type="button" id="finalize_and_send" onclick="remove_sortlisted_product();" <?=@$add_class?>> Remove All Shortlisted Design</button>
                <a href="javascript:void(0);"><button class="btn btn-success waves-effect waves-light btn-md pull-right buton_bottom" name="shortlisted" type="button" id="finalize_and_send" style="margin-left:10px" onclick="sortlisted_product_store();" <?=@$add_class?>> Finalize and Send </button></a>&nbsp;
                  

              </div>            
            </div>
            <div class="panel-body">
              <div class="form-group">
               <form id="product_shortlist_search" role="form" class="form-horizontal mrg-top" method="post">

                  <div class="form-group">
                    <div class="col-sm-5">
                      <label class="col-sm-5 control-label label_bottom" for="inputEmail3"> Category</label>
                      <div class="col-sm-5">
                        <input type="text" placeholder="" class="form-control" name="category">
                      </div>
                    </div>
                    <div class="col-sm-5">
                      <label class="col-sm-5 control-label" for="inputEmail3">Weight range </label>
                      <div class="col-sm-5">
                        <select class="form-control" name="weight_range">
                          <option value=''>Select Weight range</option>
                          <?php
                          if(!empty($weight_range)){
                            foreach ($weight_range as $key => $value) {
                              ?>
                              <option value="<?=$value['from_weight']."-".$value['to_weight']?>" <?=($value['from_weight']."-".$value['to_weight'] == @$search['weight_range']) ? 'selected' : ''?>><?=$value['from_weight']."-".$value['to_weight']?></option>
                              <?php
                            }
                          }
                          ?>
                        </select>
                      </div>
                    </div>
                  </div>
                  <div class="form-group">
                      <div class="col-sm-5">
                        <label class="col-sm-5 control-label" for="inputEmail3"> Search Design Code </label>
                        <div class="col-sm-5">
                         <input type="text" placeholder="" class="form-control" name="product_code">
                       </div>
                     </div>
                     <div class="col-sm-5">
                      <label class="col-sm-5 control-label" for="inputEmail3"> Karigar </label>
                      <div class="col-sm-5">
                       <input type="text" placeholder="" class="form-control" name="karigar">
                     </div>
                   </div>
                     <div class="col-sm-5" style="margin-top: 15px">
                      <label class="col-sm-5 control-label" for="inputEmail3"> Corporate <span class="asterisk">*</span></label>
                      <div class="col-sm-5" >
                        <select class="form-control" name="corporate" id="corporate" >
                          <option value=''>Select Corporate</option>
                          <?php
                          if(!empty($corporates)){
                            foreach ($corporates as $key => $value) {
                              ?>
                              <option value="<?=$value['id'] ?>"><?=$value['name'] ?></option>
                              <?php
                            }
                          }
                          ?>
                        </select>
                     </div>
                   </div>
                 </div> 
               </form>
              </div>
              <form name="sortlist_product_form" id="sortlist_product_form" method="post" action="<?=ADMIN_PATH?>finalize_and_send">
              <?php $data['shortlisted'] = $shortlisted; $this->load->view('Send_for_approval/shortlist_product',$data); ?>
              </form>
            </div>
          </div>
        </div>
      </div>
      <div class="row">
        <div class="col-sm-12 card-box-wrap">
          <div class="panel panel-default">
            <div class="panel-heading frmHeading">                
              <h4 class="inlineBlock">Shortlist Designs For Approval</h4>
            </div>
            <div class="panel-body">
              <form name="product_approval" id="product_approval_search" role="form" class="form-horizontal mrg-top" method="post" action="<?=ADMIN_PATH?>Send_for_approval">
                <input type="hidden" name='page_title' value='<?=trim($page_title);?>'>
                <div class="form-group">
                  <div class="col-sm-5">
                    <label class="col-sm-5 control-label" for="inputEmail3"> Category</label>
                    <div class="col-sm-5">
                     <input type="text" placeholder="" id="category_search" class="form-control category_add search-input-text" name="category_search" required=""  value="<?=@$search['category_search']?>"  filtercol="0">
                     <span class="text-danger" id="code_error"></span>
                    </div>
                  </div>
                  <div class="col-sm-5">
                    <label class="col-sm-5 control-label" for="inputEmail3">Weight range </label>
                    <div class="col-sm-5">
                      <select class="form-control search-input-text weight-range" name="weight_range"  filtercol="1">
                        <option value=''>Select Weight range</option>
                        <?php
                        if(!empty($weight_range)){
                          foreach ($weight_range as $key => $value) {
                            ?>
                            <option value="<?=$value['from_weight']."-".$value['to_weight']?>" <?=($value['from_weight']."-".$value['to_weight'] == @$search['weight_range']) ? 'selected' : ''?>><?=$value['from_weight']."-".$value['to_weight']?></option>
                            <?php
                          }
                        }
                        ?>
                      </select>
                      <span class="text-danger" id="weight_range_id_error"></span>
                    </div>
                  </div>
                     <!-- <div class="col-sm-2">
                              <div class="form-group">
                               <div class="col-sm-5 control-label ">
                                   <button class="btn btn-primary waves-effect waves-light  btn-md" name="commit" type="button" onclick="search_product(); ">
                                   Submit
                                   </button>
                               </div>
                            </div>
                          </div> -->
                </div>
                <div class="form-group">
                  <div class="col-sm-5">
                    <label class="col-sm-5 control-label" for="inputEmail3"> Search Design Code </label>
                    <div class="col-sm-5">
                     <input type="text" placeholder="" id="product_code" class="form-control product_add search-input-text" name="product_code" required="" value="<?=@$search['product_code']?>"  filtercol="2">
                     <span class="text-danger" id="code_error"></span>
                    </div>
                  </div>
                  <div class="col-sm-5">
                    <label class="col-sm-5 control-label" for="inputEmail3"> Karigar </label>
                    <div class="col-sm-5">
                      <input type="text" placeholder="" class="form-control karigar_add search-input-text" name="karigar" required="" value="<?=@$search['karigar']?>"  filtercol="3">
                      <span class="text-danger" id="code_error"></span>
                    </div>
                  </div>
                  <div class="col-sm-5" style="margin-top: 15px">
                    <label class="col-sm-5 control-label" for="inputEmail3"> Corporate </label>
                    <div class="col-sm-5" >
                      <select class="form-control search-input-text" name="corporate"  filtercol="4" id="corporate">
                        <option value=''>Select Corporate</option>
                        <?php
                        if(!empty($corporates)){
                          foreach ($corporates as $key => $value) {
                            ?>
                            <option value="<?=$value['id'] ?>"><?=$value['name'] ?></option>
                            <?php
                          }
                        }
                        ?>
                      </select>
                    </div>
                  </div>
                </div>                      
              </form>
              <div class="form-group ">
                <button class="btn  waves-effect waves-light btn-shortlist" name="commit" type="button" onclick="shortlisted_all_products();" style="float: right;margin-bottom: 10px">Shortlist All Design</button>
              </div>  
                <!-- <form name="search_product_form" id="search_product_form"> -->
              <div class="clearfix"></div>
              <div class="table-rep-plugin">         
                  <div class="table-responsive aman b-0 scrollhidden">
                    <table class="table custdatatable table-bordered  aman" id="search_products" width="100%">
                      <thead>
                       <tr>
                         <th class="col4">#</th>
                         <th class="col4">Date || Time</th>
                         <th class="col4">Code</th>
                         <th class="col4">Size</th>
                         <th class="col4">Karigar</th>
                         <th class="col4">Metal</th>
                         <th class="col4">Category</th>
                         <th class="col4">Sub Category</th>
                         <th class="col4">Carat</th>
                         <th class="col4">WT Range</th>
                         <th class="col4">Image</th>
                         <th class="col3"></th>
                       </tr>
                      </thead>
                    </table>
                  </div>         
              </div>
               <!-- </form><br> -->
            </div>    
          </div>     
        </div>
      </div>
    </div>
  </div>
</div>

<script>
//  $(function() {
//   var category = <?php echo json_encode($category); ?>;
//   $(".category_add").autocomplete({
//       source: category
//   });
//   var karigar = <?php echo json_encode($karigar); ?>;
//   $(".karigar_add").autocomplete({
//       source: karigar
//   });
//    var product = <?php echo json_encode($product); ?>;
//   $(".product_add").autocomplete({
//       source: product
//   });
// });
var shortlist_count = "<?php echo $shortlist_count=0; ?>";

</script>
<style type="text/css">
  .dataTables_filter{
    display:none;
  }
</style>
