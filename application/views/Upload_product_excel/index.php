<div class="content-page">
<div class="content">
   <div class="container">
      <div class="row">
         <div class="col-md-10 col-sm-10 col-sm-12 card-box-wrap">
            <div class="panel panel-default">

              <div class="panel-heading frmHeading">                
               <h4 class="inlineBlock">Upload Design Excel</h4>

              </div>
               <div class="panel-body">
                  <form name="category" id="Upload_product_excel" role="form" class="form-horizontal" method="post" enctype="multipart/form-data">
                    
                     <div class="form-group">
                        <label class="col-sm-3 control-label" for="inputEmail3"> Import Excel <span class="asterisk">*</span></label>

                        <div class="col-sm-4 m-t-0  Choose-new">
                           <!-- <input type="file" placeholder="Enter Name" id="category" class="" name="Upload_product_excel[file]"> -->
                           <label class="custom-file-upload">
                          <input type="file" placeholder="Enter Name" id="category" class="" name="Upload_product_excel[file]">
                          <i class="fa fa-upload"></i> Choose File
                        </label>
                        <div class='r'>
                        </div> 
                        
                           <span class="text-danger" id="file_empty_error"></span>
                        </div>


                        <label class="col-sm-3 control-label" for="inputEmail3">
                          <a href="<?= UPLOAD_IMAGES?>template/Design_Import.xlsx" class="btn btn-primary  waves-effect w-md waves-light m-b-5"><i class="fa fa-download" aria-hidden="true"></i> Download Template</a>
                         <!-- <a href="<?= ADMIN_PATH?>Product/export_product_template" class="btn btn-primary  waves-effect w-md waves-light m-b-5"><i class="fa fa-download" aria-hidden="true"></i> Download Template</a>-->

                        </label>
                     </div> 
                       <div class="form-group">
                        <label class="col-sm-3 control-label" for="inputEmail3"> Corporate <span class="asterisk">*</span></label>
                        <div class="col-sm-4">
                           <select name="corporate" class="form-control corporate" name="corporate">
                           <option value="">Select Corporate</option>
                           <?php
                              foreach ($corporates as $key => $value) {
                                 ?>
                                     <option value="<?= $value['id']?>"><?= $value['name']?></option>
                                 <?php 
                              }
                            ?>
                             
                           </select>
                           <span class="text-danger" id="corporate_error"></span>
                        </div>
                     </div>               
                     <!-- <div class="form-group ">
                     <div class="col-sm-offset-5 col-sm-8 texalin">
                      <button class="btn btn-primary waves-effect waves-light  btn-md" name="commit" type="button" onclick="Save_product_excel();">
                         UPLOAD
                         </button>
                                                

                        <div class="col-sm-4" style="margin-top:-5px">
                          <input type="file" placeholder="Enter Name" id="category" class="" name="Upload_product_excel[file]">
                          
                           <span class="text-danger" id="file_empty_error"></span>
                        </div>
                        <a href="<?= ADMIN_PATH?>Product/export_product_template">
                          
                          <button class="btn btn-primary waves-effect waves-light  btn-md" name="commit" type="button" >
                            Download Template
                         </button>
                         </a>
                     </div>
                     -->  
                    <div class="texalin btnSection">       
                   <button class="btn btn-default waves-effect waves-light btn-md" name="commit" type="button" onclick="history.back();">Back</button>           
                      <button class="btn upload-btn waves-effect waves-light btn-md pull-right" name="commit" type="button" onclick="Save_product_excel();" style="width:97px;" >
                         UPLOAD
                      </button>
                    </div>                  
                  </form>
                  <div class="col-lg-offset-4 errors col-lg-6">
                  </div>
               </div>
               <div class="clearfix"></div>
            </div>
         </div>
      </div>
   </div>
</div>
