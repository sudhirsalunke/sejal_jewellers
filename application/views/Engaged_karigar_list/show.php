<div class="content-page">
<div class="content">
   <div class="container">
      <div class="row">
         <div class="col-sm-12 card-box-wrap">
            <div class="panel panel-default">
              <div class="panel-heading">
               <h4 class="inlineBlock"><span>Engaged Karigar Details </span></h4> 
               <a href="<?=ADMIN_PATH.'Engaged_karigar_list/export_karigar_engaged/'.$karigar_id?>" class="pull-right single-btn btn-wrap2">
                <button type="button" class="add_senior_manager_button btn btn-warning  waves-effect w-md waves-light m-b-5  btn-md pull-right single-btn">Export</button></a>               
              </div>
              <div class="panel-body">
                <h4>Karigar Name :- <b><?= $karigar_name[0]['karigar_name'];?></b>                
              </h4>

               <div class="clearfix"></div>
                <div class="table-rep-plugin">
                  <div class="table-responsive b-0 scrollhidden">
                    <table class="table table-bordered custdatatable display" id="Engaged_karigar_Details_list"   cellspacing="0" >
                      <thead>
                        <?php
                          $data['heading']='Engaged_karigar_Details_list';
                          $this->load->view('master/table_header',$data);
                        ?>
                      </thead>
                    </table>
                  </div>              
                </div>
              </div>               
            </div>
         </div>
      </div>
   </div>
</div>