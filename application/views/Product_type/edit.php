<div class="content-page">
<div class="content">
   <div class="container">
      <div class="row">
         <div class="col-sm-12 card-box-wrap">
            <div class="card-box padmobile">
               <h4 class="header-title business_manager_header_title m-t-0 m-b-30 bmsmheader bmsmheaderabm"><span><?= strtoupper($page_title);?></span>
               </h4>
               <div class="col-lg-12">
                  <form name="category" id="Product_type" role="form" class="form-horizontal" method="post">
                  <input type="hidden" name="Product_type[encrypted_id]" value="<?= $product_type['encrypted_id'] ?>">
                     <div class="form-group">
                        <label class="col-sm-4 control-label" for="inputEmail3"> Enter Name <span class="asterisk">*</span></label>
                        <div class="col-sm-4">
                           <input type="text" placeholder="Enter Name" id="category" class="form-control" name="Product_type[name]" value="<?= $product_type['name'] ?>">
                           <span class="text-danger" id="name_error"></span>
                        </div>
                     </div>
                     <div class="form-group">
                        <label class="col-sm-4 control-label" for="inputEmail3"> Enter Descripition <span class="asterisk">*</span></label>
                        <div class="col-sm-4">
                           <textarea class="form-control" name="Product_type[description]"><?= $product_type['description'] ?></textarea>
                           <span class="text-danger" id="description_error"></span>
                        </div>
                     </div>
                    <div class="form-group">
                     <div class="col-sm-offset-5 col-sm-8 texalin">
                         <button class="btn btn-primary waves-effect waves-light  btn-md" name="commit" type="button" onclick="update_product_type();">
                         SAVE
                         </button>
                         <button class="btn btn-default waves-effect waves-light m-l-5  btn-md" onclick="window.location='<?= ADMIN_PATH?>Product_type'" type="reset">
                           CANCEL
                           </button>
                     </div>
                  </div>
                  </form>
               </div>
               <div class="clearfix"></div>
            </div>
         </div>
      </div>
   </div>
</div>