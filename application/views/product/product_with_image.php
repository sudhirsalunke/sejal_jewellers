<div class="content-page">
<div class="content">
   <div class="container">
      <div class="row">
         <div class="col-sm-12 card-box-wrap">
            <div class="panel panel-default">
              <div class="panel-heading">                
                <h4 class="inlineBlock">Designs With Images</h4>
                <div class="pull-right wrap-1024">

                  <a href="javascript:void(0)"><button  type="button" onclick="fetch_from_dropbox();" class="add_senior_manager_button btn btn-fetch waves-effect w-md waves-light btn-md">FETCH FROM DROPBOX</button></a> 
                  <button  type="button" onclick="export_products_with_image()" class="add_senior_manager_button btn btn-warning waves-effect w-md waves-lightbtn-md">Export</button>
                  <a href="<?=ADMIN_PATH.'Upload_design_excel'?>"><button  type="button" class="add_senior_manager_button btn btn-import waves-effect w-md waves-light btn-md">IMPORT </button></a>
                  <a href="<?= ADMIN_PATH?>Design/create"><button  type="button" class="add_senior_manager_button btn btn-success waves-effect w-md waves-light btn-md">ADD  DESIGN  </button></a>

                </div>
              </div>
              <div class="panel-body">
                <div class="clearfix"></div>
                 <div class="table-rep-plugin">
                    <div class="table-responsive b-0 scrollhidden">
                      <form enctype='multipart/form-data' role="form" >
                        <table class="table table-bordered custdatatable stripe row-border order-column" id="viewProduct" width="100%">
                            <thead>
                    <?php
                        $data['heading']='viewProduct';
                        $this->load->view('master/table_header',$data);?>
                  </thead>
                   <!--        <thead>
                             <tr>
                                 <th class="col4">#</th>
                                 <th class="col4">Date || Time</th>
                                 <th class="col4">Code</th>
                                 <th class="col4">Karigar</th>
                                 <th class="col4">Size</th>
                                 <th class="col4">Metal</th>
                                 <th class="col4">Category</th>
                                 <th class="col4">Sub Category</th>
                                 <th class="col4">Carat</th>
                                 <th class="col4">WT Range</th>
                                 <th class="col4">Image</th>
                                 <th class="col3"></th>
                             </tr>
                          </thead> -->
                        </table>
                      </form>
                    </div>
                 </div>
              </div>
            </div>
         </div>
      </div>
   </div>
</div>
