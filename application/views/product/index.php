<div class="content-page">
<div class="content">
   <div class="container">
      <div class="row">
         <div class="col-sm-12 card-box-wrap">
            <div class="panel panel-default">
              <div class="panel-heading">
                <h4 class="inlineBlock">Designs Without Images</h4>
                <div class="pull-right m-t-5 wrap-1024">
                  <?php 
                  if($page_title == 'DESIGNS WITHOUT IMAGES'){
                    ?>

                  <a href="javascript:void(0)"><button  type="button" onclick="fetch_from_dropbox();" class="add_senior_manager_button btn btn-fetch waves-effect w-md waves-light m-b-5 btn-md">FETCH FROM DROPBOX </button></a>
                  <button  type="button" onclick="export_products_without_image()" class="add_senior_manager_button btn btn-warning waves-effect w-md waves-light m-b-5 btn-md">Export</button>
                  <a href="<?=ADMIN_PATH.'Upload_design_excel'?>"><button  type="button" class="add_senior_manager_button btn btn-import waves-effect w-md waves-light m-b-5 btn-md">IMPORT </button></a>
                  <a href="<?= ADMIN_PATH?>Design/create"><button  type="button" class="add_senior_manager_button btn btn-success waves-effect w-md waves-light m-b-5 btn-md">ADD  DESIGN  </button></a>                  

                  <?php } ?>
                </div>
              </div>
              <div class="panel-body">
                <div class="clearfix"></div>
                 <?php 
              if($page_title=="DESIGNS WITH IMAGES" || $page_title=="ALL PRODUCTS"){
                 
                $table_col_name='viewProduct';
                }else{
                
                  $table_col_name='viewProductwithoutimg';
                }
                ?>
                <div class="table-rep-plugin">
                  <div class="table-responsive b-0 scrollhidden">
                   <table class="table table-bordered custdatatable" id="<?php echo $table_col_name;?>">
                  <thead>
                    <?php
                        $data['heading']=$table_col_name;
                        $this->load->view('master/table_header',$data);?>
                  </thead>
                 <!--      <thead>
                         <tr>
                             <th class="col4">#</th>
                             <th class="col4">Date || Time</th>
                             <th class="col4">Code</th>
                             <th class="col4">Karigar</th>
                             <th class="col4">Size</th>
                             <th class="col4">Metal</th>
                             <th class="col4">Category</th>
                             <th class="col4">Sub Category</th>
                             <th class="col4">Carat</th>
                             <th class="col4">WT Range</th>
                             <?php if($page_title=="DESIGNS WITH IMAGES" || $page_title=="ALL PRODUCTS") { ?>
                              <th class="col4">Images</th>
                             <?php } 
                            if($page_title=="ADD PRODUCTS IMAGES") { ?>
                              <th class="col3">Upload images</th>
                             <?php } else { ?>
                            <th class="col3"></th>
                            <?php } ?>
                         </tr> -->
                      </thead>
                   </table>
                  </div>
                </div>
              </div>                
            </div>
         </div>
      </div>
   </div>
</div>

