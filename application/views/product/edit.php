<div class="content-page">
<div class="content">
   <div class="container">
      <div class="row">
         <div class="col-sm-12 card-box-wrap">
            <div class="panel panel-default">

              <div class="panel-heading frmHeading">                
               <h4 class="inlineBlock">Edit Product<?php print_r($product['category_id']);?></h4>

              </div>
              <div class="panel-body">
                <form name="product" id="product" role="form" class="form-horizontal" method="post">
                    <input type="hidden" name="product[encrypted_id]" value="<?=$product['encrypted_id'] ?>">
                    <div class="form-group">
                      <div class="col-sm-6">
                          <label class="col-sm-5 control-label" for="inputEmail3"> Select Karigar <span class="asterisk">*</span></label>
                          <div class="col-sm-5">
                            <select class="form-control chosen-select" name=product[karigar_id]>
                              <option value=''>Select Karigar</option>
                              <?php
                                if(!empty($karigar)){
                                  foreach ($karigar as $key => $value) {
                                    ?>
                                    <option <?=($product['karigar_id'] == $value['id']) ? 'selected' : ''?> value="<?=$value['id']?>"><?=$value['name']." (".$value['code'].")"?></option>
                                    <?php
                                  }
                                }
                              ?>
                            </select>
                             <span class="text-danger" id="karigar_id_error"></span>
                          </div>
                      </div>
                      <div class="col-sm-6">
                          <label class="col-sm-5 control-label" for="inputEmail3"> Enter Design Code <span class="asterisk">*</span></label>
                          <div class="col-sm-5">
                             <input type="text" placeholder="Enter Design Code" id="code" class="form-control" value="<?=$product['product_code']?>" name="product[product_code]" required="" >
                             <span class="text-danger" id="code_error"></span>
                          </div>
                      </div>
                    </div>
                    <div class="form-group">
                      <div class="col-sm-6">
                          <label class="col-sm-5 control-label" for="inputEmail3"> Size </label>
                          <div class="col-sm-5">
                             <input type="text" placeholder="Enter Size" id="category" class="form-control" value="<?=$product['size']?>" name="product[size]">
                             <span class="text-danger" id="size_error"></span>
                          </div>
                      </div>
                      <div class="col-sm-6">
                          <label class="col-sm-5 control-label" for="inputEmail3"> Select Category <span class="asterisk">*</span></label>
                          <div class="col-sm-5">
                            <select class="form-control category_id chosen-select" sub_category_id="<?=$product['sub_category_id']?>" name=product[category_id]>
                              <option value=''>Select Category</option>
                              <?php
                                if(!empty($category)){
                                  foreach ($category as $key => $value) {
                                    ?>
                                    <option <?=($product['category_id'] == $value['id']) ? 'selected' : ''?> value="<?=$value['id']?>"><?=$value['name']." (".$value['code'].")"?></option>
                                    <?php
                                  }
                                }
                              ?>
                            </select>
                            <span class="text-danger" id="category_id_error"></span>
                          </div>
                      </div>
                    </div>
                    <div class="form-group">
                      <div class="col-sm-6">
                          <label class="col-sm-5 control-label" for="inputEmail3"> Select Sub Category <span class="asterisk">*</span> </label>
                          <div class="col-sm-5">
                            <select class="form-control sub_category_id chosen-select" name=product[sub_category_id]>
                              <option value=''>Select Sub Category</option>
                            </select>
                            <span class="text-danger" id="sub_category_id_error"></span>
                          </div>
                      </div>
                      <div class="col-sm-6">
                        <label class="col-sm-5 control-label" for="inputEmail3"> Select Pcs </label>
                        <div class="col-sm-5">
                          <select class="form-control" name=product[pcs_id]>
                            <option value=''>Select Pcs</option>
                            <?php
                            if(!empty($pcs)){
                              foreach ($pcs as $key => $value) {
                                ?>
                                <option <?=($product['pcs_id'] == $value['id']) ? 'selected' : ''?> value="<?=$value['id']?>"><?=$value['name']?></option>
                                <?php
                              }
                            }
                            ?>
                          </select>
                          <span class="text-danger" id="pcs_id_error"></span>
                        </div>
                      </div>
                      <!-- <div class="col-sm-6">
                          <label class="col-sm-5 control-label" for="inputEmail3"> Select Product Type <span class="asterisk">*</span></label>
                          <div class="col-sm-5">
                            <select class="form-control" name=product[product_type_id]>
                              <option value=''>Select Product Type</option>
                              <?php
                                if(!empty($product_type)){
                                  foreach ($product_type as $key => $value) {
                                    ?>
                                    <option <?=($product['product_type_id'] == $value['id']) ? 'selected' : ''?> value="<?=$value['id']?>"><?=$value['name']?></option>
                                    <?php
                                  }
                                }
                              ?>
                            </select>
                            <span class="text-danger" id="product_type_id_error"></span>
                          </div>
                      </div> -->
                    </div>
                    <div class="form-group">
                      <div class="col-sm-6">
                          <label class="col-sm-5 control-label" for="inputEmail3"> Select Article </label>
                          <div class="col-sm-5">
                            <select class="form-control" name=product[article_id]>
                              <option value=''>Select Article</option>
                              <?php
                                if(!empty($article)){
                                  foreach ($article as $key => $value) {
                                    ?>
                                    <option <?=($product['article_id'] == $value['id']) ? 'selected' : ''?> value="<?=$value['id']?>"><?=$value['name']?></option>
                                    <?php
                                  }
                                }
                              ?>
                            </select>
                            <span class="text-danger" id="article_id_error"></span>
                          </div>
                      </div>
                      <div class="col-sm-6">
                          <label class="col-sm-5 control-label" for="inputEmail3"> Select Carat </label>
                          <div class="col-sm-5">
                            <select class="form-control" name=product[carat_id]>
                              <option value=''>Select Carat</option>
                              <?php
                                if(!empty($carat)){
                                  foreach ($carat as $key => $value) {
                                    ?>
                                    <option <?=($product['carat_id'] == $value['id']) ? 'selected' : ''?> value="<?=$value['id']?>"><?=$value['name']?></option>
                                    <?php
                                  }
                                }
                              ?>
                            </select>
                            <span class="text-danger" id="carat_id_error"></span>
                          </div>
                      </div>
                    </div>
                    <div class="form-group">
                      <div class="col-sm-6">
                          <label class="col-sm-5 control-label" for="inputEmail3"> Select Metal </label>
                          <div class="col-sm-5">
                            <select class="form-control" name=product[metal_id]>
                              <option value=''>Select Metal</option>
                              <?php
                                if(!empty($metal)){
                                  foreach ($metal as $key => $value) {
                                    ?>
                                    <option <?=($product['metal_id'] == $value['id']) ? 'selected' : ''?> value="<?=$value['id']?>"><?=$value['name']?></option>
                                    <?php
                                  }
                                }
                              ?>
                            </select>
                            <span class="text-danger" id="metal_id_error"></span>
                          </div>
                      </div>
                      <div class="col-sm-6">
                          <label class="col-sm-5 control-label" for="inputEmail3"> Select Weight/Weight range <span class="asterisk">*</span></label>
                          <div class="col-sm-5">
                            <!-- <select class="form-control" name=product[weight_band_id]>
                              <option value=''>Select Weight range</option>
                              <?php
                                if(!empty($weight_range)){
                                  foreach ($weight_range as $key => $value) {
                                    ?>
                                    <option <?=($product['weight_band_id'] == $value['id']) ? 'selected' : ''?> value="<?=$value['id']?>"><?=$value['from_weight']."-".$value['to_weight']?></option>
                                    <?php
                                  }
                                }
                              ?>
                            </select> -->
                            <input type="text" placeholder="Enter Weight Band" class="form-control" name="product[weight_band_id]" list="cars" value="<?= @$band_wt;?>">
                            <datalist id="cars">
                              <?php
                                if(!empty($weight_range)){
                                  foreach ($weight_range as $key => $value) {
                                    ?>
                                    <option value="<?=$value['from_weight']."-".$value['to_weight']?>"><?=$value['from_weight']."-".$value['to_weight']?></option>
                                    <?php
                                  }
                                }
                            ?>
                            </datalist>
                            <span class="text-danger" id="weight_band_id_error"></span>
                          </div>
                      </div>
                    </div>
                    <div class="form-group">
                      <div class="col-sm-6">
                        <label class="col-sm-5 control-label" for="inputEmail3"> Select Buying Complexity </label>
                        <div class="col-sm-5">
                          <select class="form-control" name=product[buying_complexity_id]>
                            <option value=''>Select Buying Complexity</option>
                            <?php
                            if(!empty($buying_complexity)){
                              foreach ($buying_complexity as $key => $value) {
                                ?>
                                <option <?=($product['buying_complexity_id'] == $value['id']) ? 'selected' : ''?> value="<?=$value['id']?>"><?=$value['name']?></option>
                                <?php
                              }
                            }
                            ?>
                          </select>
                          <span class="text-danger" id="buying_complexity_id_error"></span>
                        </div>
                      </div>
                      <div class="col-sm-6">
                        <label class="col-sm-5 control-label" for="inputEmail3"> Select Manufacturing Type </label>
                        <div class="col-sm-5">
                          <select class="form-control" name=product[manufacturing_type_id]>
                            <option value=''>Select Manufacturing Type</option>
                            <?php
                            if(!empty($manufacturing_type)){
                              foreach ($manufacturing_type as $key => $value) {
                                ?>
                                <option <?=($product['manufacturing_type_id'] == $value['id']) ? 'selected' : ''?> value="<?=$value['id']?>"><?=$value['name']?></option>
                                <?php
                              }
                            }
                            ?>
                          </select>
                          <span class="text-danger" id="manufacturing_type_id_error"></span>
                        </div>
                      </div>
                    </div>
                    <div class="form-group">
                      <div class="col-sm-6">
                        <label class="col-sm-5 control-label" for="inputEmail3">Select Corporate<span class="asterisk">*</span></label>
                        <div class="col-sm-5">
                          <select class="form-control" name=product[corporate]>
                            <option value=''>Select Corporate</option>
                            <?php
                            if(!empty($corporate)){
                              foreach ($corporate as $c_key => $c_value) {
                                ?>
                                <option <?=($product['corporate'] == $c_value['id']) ? 'selected' : ''?> value="<?=$c_value['id']?>"><?=$c_value['name']?></option>
                                <?php
                              }
                            }
                            ?>
                          </select>
                          <span class="text-danger" id="corporate_error"></span>
                        </div>
                      </div>
                    </div>
                    <div class="form-group">
                      <div class="col-sm-6">
                          <label class="col-sm-5 control-label" for="inputEmail3"> Upload Image </label>
                          <div class="col-sm-5 m-t-2">
                            <input type="file" id="image" class="change_image" name="product[image]" required="" place_id="imgLogo">
                            <span class="text-danger" id="image_error"></span>
                          </div>
                      </div> <?php if(!empty($product['image'])) { ?>
                       <div class="col-sm-6">
                          <label class="col-sm-5 control-label" for="inputEmail3"></label>
                          <div class="col-sm-5">
                            <img id="imgLogo" style="height: 140px; width: 150px;" src="<?= UPLOAD_IMAGES.'product'.'/'.$product['product_code'].'/'.'small'.'/'.$product['image']?>" class="img-rounded">
                             <input type="hidden" name="old_image" value="<?=$product['image'] ?>">
                          </div>
                      </div>
                      <?php }?>
                      <div class="col-sm-6">
                        <label class="col-sm-5 control-label" for="inputEmail3"></label>
                          <div class="col-sm-5">
                            <img id="imgLogo" src="" style="height: 140px; width: 150px;" class="img-rounded" alt="Logo Image" hidden/>
                          </div>
                      </div>
                    </div>

                    <div class="btnSection">
                       <!--  <button class="btn btn-primary waves-effect waves-light  btn-md" name="commit" type="button" onclick="update_product(); ">
                          SAVE
                       </button> -->
                        <button class="btn btn-default waves-effect waves-light m-l-5  btn-md" onclick="window.location='<?= ADMIN_PATH?>Design/Design_with_images'" type="reset">
                             Back
                        </button>
                        <button class="btn btn-success pull-right waves-effect waves-light  btn-md" name="commit" type="button" onclick="update_product(); ">
                           SAVE
                        </button>
                    </div>
                </form>
              </div>
               <div class="clearfix"></div>
            </div>
         </div>
      </div>
   </div>
</div>