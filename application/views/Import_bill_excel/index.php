<div class="content-page">
<div class="content">
   <div class="container">
      <div class="row">
         <div class="col-sm-6 card-box-wrap">
            <div class="panel panel-default">
              <div class="panel-heading frmHeading">
               <h4 class="inlineBlock"><span>Import GT Excel</span></h4>                
              </div>
              <div class="panel-body">
                <div class="col-lg-12">
                  <form name="category" id="import_bill_excel" role="form" class="form-horizontal" method="post" enctype="multipart/form-data">
                    <div class="form-group">
                      <label class="col-xs-3 control-label" for="inputEmail3"> GT Number <span class="asterisk">*</span></label>
                        <div class="form-group col-xs-9">
                          <input type="text" placeholder="Enter GT Number" id="challan_no" class="form-control" name="challan_no">
                          <span class="text-danger" id="challan_no_error"></span>
                        </div>
                    </div>
                      <div class="form-group">
                        <label class="col-xs-3 control-label" for="inputEmail3"> Import GT Excel <span class="asterisk">*</span></label>

                        <div class="col-xs-9 m-t-0  Choose-new">
                           <!-- <input type="file" placeholder="Enter Name" id="category" class="" name="Upload_product_excel[file]"> -->
                           <label class="custom-file-upload">
                          <input type="file" placeholder="Enter Name" id="category" class="" name="Upload_product_excel[file]">
                          <i class="fa fa-upload"></i> Choose File
                        </label>
                        <div class='r'>
                        </div> 
                           <span class="text-danger" id="heading_col_error"></span>
                           <span class="text-danger" id="file_empty_error"></span>
                        </div>
                      </div>
                      <div class="form-group">
                        <div class="col-sm-12 texalin btnSection">
                          <button class="btn btn-default waves-effect waves-light btn-md pull-left" name="commit" type="button" onclick="history.back();">Back</button>  
                          <button class="btn waves-effect waves-light btn-md upload-btn pull-right" name="commit" type="button" onclick="import_billing_excel();" style="width:97px;" >
                           UPLOAD
                          </button>
                        </div>
                      </div>                  
                  </form>
                  <div class="col-lg-offset-4 errors col-lg-6">
                  </div>
                </div>
              </div>
               <div class="clearfix"></div>
            </div>
         </div>
      </div>
   </div>
</div>
