<div class="content-page">
  <div class="content">
    <div class="container">
      <div class="row">
        <div class="col-sm-12 card-box-wrap">
          <div class="card-box padmobile">
            <h4 class="header-title business_manager_header_title m-b-30 bmsmheader bmsmheaderabm">
            <span>PRODUCT CATELOG DASHBOARD</span>
            </h4>              
            <div class="catlog-details">
              <div class="row marg_bottom">
                <div class="col-md-10 col-xs-12">
                  <div class="col-md-5">
                    <div class="card-box box-shadow">
                      <h4 class="header-title m-t-0 m-b-30">DESIGN WITH IMAGES</h4>
                      <div class="widget-chart-1">
                        <div class="widget-detail-1">
                          <h2 class="number-size light-blue-font"> 945 </h2>
                          <span class="label label-gray">Images</span>
                        </div>
                      </div>
                    </div>
                  </div><!-- end col -->

                  <div class="col-md-5">
                    <div class="card-box box-shadow">
                    <h4 class="header-title m-t-0 m-b-30">DESIGN WITHOUT IMAGES</h4>
                      <div class="widget-chart-1">
                        <div class="widget-detail-1">
                          <h2 class="number-size light-red-font"> 547 </h2>
                          <span class="label label-gray">Images</span>
                        </div>
                      </div>
                    </div>
                  </div><!-- end col -->
                </div>
              </div>
              
              <!-- End row -->
              <div class="row but-sec">
                <div class="col-md-10 col-xs-12">
                  <div class="col-md-4">
                    <a href="<?= ADMIN_PATH.'product/create' ?>" class="btn btn-purple waves-effect w-md m-b-5">ADD PRODUCTS</a>
                  </div>
                  <div class="col-md-4">
                    <a href="button" class="btn btn-purple waves-effect w-md m-b-5">IMPORT PRODUCTS</a>
                  </div>
                  <div class="col-md-4">
                    <a href="button" class="btn btn-purple waves-effect w-md m-b-5">UPLOAD IMAGES</a>
                  </div>
                </div>
              </div>
              <!-- End row -->
            </div>  
            <div class="row">
              <div class="col-md-12 col-xs-12 section-2 bg-light-gray">
                <img src="assets/images/reliance-logo.png" alt="reliance-logo" class="img-responsive">
              </div>
              <div class="col-md-10 col-xs-12 section-3 product-box marg_top-bottom">
                <div class="col-lg-4 col-md-6">
                  <div class="card-box dash-box box-shadow">
                    <h4 class="header-title m-t-0 m-b-30">APPROVED DESIGNS</h4>
                    <div class="widget-chart-1">
                      <div class="widget-detail-1">
                        <h2 class="number-size light-red-font"> 450 </h2>
                        <span class="label label-gray">Images</span>
                      </div>
                    </div>
                  </div>
                </div><!-- end col -->
                <div class="col-lg-4 col-md-6">
                  <div class="card-box dash-box box-shadow">
                    <h4 class="header-title m-t-0 m-b-30">SHORTLISTED DESIGNS TO BE SENT</h4>
                    <div class="widget-chart-1">
                      <div class="widget-detail-1">
                        <h2 class="number-size light-orange-font"> 80 </h2>
                        <span class="label label-gray">Images</span>
                      </div>
                    </div>
                  </div>
                </div><!-- end col -->
                <div class="col-lg-4 col-md-6">
                  <div class="card-box dash-box box-shadow">
                    <h4 class="header-title m-t-0 m-b-30">SENT DESIGNS FOR APPROVAL</h4>
                    <div class="widget-chart-1">
                      <div class="widget-detail-1">
                        <h2 class="number-size light-orange-font"> 298 </h2>
                        <span class="label label-gray">Images</span>
                      </div>
                    </div>
                  </div>
                </div><!-- end col -->
                <div class="col-lg-4 col-md-6">
                  <div class="card-box dash-box box-shadow">
                    <h4 class="header-title m-t-0 m-b-30">rejected Designs</h4>
                    <div class="widget-chart-1">
                      <div class="widget-detail-1">
                        <h2 class="number-size light-blue-font"> 162 </h2>
                        <span class="label label-gray">Images</span>
                      </div>
                    </div>
                  </div>
                </div><!-- end col -->
              </div>
            </div>
            <!-- End row -->
            <div class="row">
              <div class="col-md-12 col-xs-12 section-2 bg-light-gray">
                <img src="assets/images/carat-logo.png" alt="reliance-logo" class="img-responsive">
              </div>
              <div class="col-md-10 col-xs-12 section-3 product-box marg_top-bottom">
                <div class="col-lg-4 col-md-6">
                  <div class="card-box dash-box box-shadow">
                    <h4 class="header-title m-t-0 m-b-30">APPROVED DESIGNS</h4>
                    <div class="widget-chart-1">
                      <div class="widget-detail-1">
                        <h2 class="number-size light-red-font">450</h2>
                        <span class="label label-gray">Images</span>
                      </div>
                    </div>
                  </div>
                </div><!-- end col -->
                <div class="col-lg-4 col-md-6">
                  <div class="card-box dash-box box-shadow">
                    <h4 class="header-title m-t-0 m-b-30">SHORTLISTED DESIGNS TO BE SENT</h4>
                    <div class="widget-chart-1">
                      <div class="widget-detail-1">
                        <h2 class="number-size light-orange-font"> 80 </h2>
                        <span class="label label-gray">Images</span>
                      </div>
                    </div>
                  </div>
                </div><!-- end col -->
                <div class="col-lg-4 col-md-6">
                  <div class="card-box dash-box box-shadow">
                    <h4 class="header-title m-t-0 m-b-30">SENT DESIGNS FOR APPROVAL</h4>
                    <div class="widget-chart-1">
                      <div class="widget-detail-1">
                        <h2 class="number-size light-orange-font"> 298 </h2>
                        <span class="label label-gray">Images</span>
                      </div>
                    </div>
                  </div>
                </div><!-- end col -->
                <div class="col-lg-4 col-md-6">
                  <div class="card-box dash-box box-shadow">
                    <h4 class="header-title m-t-0 m-b-30">rejected Designs</h4>
                    <div class="widget-chart-1">
                      <div class="widget-detail-1">
                        <h2 class="number-size light-blue-font"> 162 </h2>
                        <span class="label label-gray">Images</span>
                      </div>
                    </div>
                  </div>
                </div><!-- end col -->
              </div>
            </div>
          </div><!-- end card-box padmobile -->
        </div><!-- end col-sm-12 card-box-wrap -->
      </div><!-- end row -->
    </div><!-- end container -->
  </div><!-- end content -->
</div><!-- end content-page -->