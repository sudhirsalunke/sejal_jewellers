<div class="content-page">
<div class="content">
   <div class="container">
      <div class="row">
         <div class="col-sm-12 card-box-wrap">
            <div class="panel panel-default">
              <div class="panel-heading">
               <h4 class="inlineBlock">All Approved Designs</h4> 
                 <div class="pull-right m-t-5">
                  <a href="<?= ADMIN_PATH?>Approved_Design/export_images/<?= @$vendor?>"><button  type="button" class="add_senior_manager_button btn btn-warning  waves-effect w-md waves-light btn-md">Export Images</button></a>
                  <a href="<?= ADMIN_PATH?>Approved_Design/export_products/<?= @$vendor?>"><button  type="button" class="add_senior_manager_button btn btn-primary  waves-effect w-md waves-light btn-md">Export Designs</button></a>
                </div>               
              </div>
              <div class="panel-body">                  
                <div class="clearfix"></div>
                <div class="table-rep-plugin">
                  <div class="table-responsive b-0 scrollhidden">
                    <form enctype='multipart/form-data' role="form" name="product_form" >
                      <table class="table table-bordered custdatatable" id="Approved_products">
                        <thead>
                          <!--  <tr>
                               <th class="col4">#</th>
                               <th class="col4">Order Id</th>
                               <th class="col4">Corporate</th>
                               <th class="col4">No Of Designs</th>
                               <th class="col4">Date</th>
                              <th class="col3"></th>
                           </tr> -->
                              <tr>
                               <th class="col4">#</th>
                               <th class="col4">Order Id</th>
                                <th class="col4">Order Date</th>
                               <th class="col4">Code</th>
                               <th class="col4">Size</th>
                               <th class="col4">Karigar</th>
                               <th class="col4">Metal</th>
                               <th class="col4">Category</th>
                               <th class="col4">Sub Category</th>
                               <th class="col4">Carat</th>
                               <th class="col4">WT Range</th>
                              <th class="col4"><span class="pull-left">Image</span></th>
                           </tr>
                        </thead>
                      </table>
                    </form>
                  </div>
                </div>
              </div>                
            </div>
         </div>
      </div>
   </div>
</div>