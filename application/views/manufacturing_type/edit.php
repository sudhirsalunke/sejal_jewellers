<div class="content-page">
<div class="content">
   <div class="container">
      <div class="row">
         <div class="col-sm-6 card-box-wrap">
            <div class="panel panel-default">
                <div class="panel-heading frmHeading">
                  <h4 class="inlineBlock"><span>Edit Manufacturing Type</span></h4>
                </div>
                <div class="panel-body">
                  <div class="col-lg-12">
                  <form name="manufacturing_type" id="manufacturing_type" role="form" class="form-horizontal" method="post">
                  <input type="hidden" name="manufacturing_type[encrypted_id]" value="<?= $manufacturing_type['encrypted_id'] ?>">
                     <div class="form-group">
                        <label class="col-sm-4 control-label" for="inputEmail3"> Enter Manufacturing Type <span class="asterisk">*</span></label>
                        <div class="col-sm-4">
                           <input type="text" placeholder="Enter Manufacturing Type" id="manufacturing_type" class="form-control" name="manufacturing_type[name]" value="<?= $manufacturing_type['name'] ?>">
                           <span class="text-danger" id="name_error"></span>
                        </div>
                     </div>
                    <div class="btnSection">
                        
                         <button class="btn btn-default waves-effect waves-light m-l-5 btn-md" onclick="window.location='<?= ADMIN_PATH?>Manufacturing_type'" type="reset">
                           back
                           </button>
                            <button class="btn btn-success pull-right waves-effect waves-light btn-md" name="commit" type="button" onclick="update_manufacturing_type(); ">
                         SAVE
                         </button>
                     
                  </div>
                  </form>
               </div>
                </div>
            </div>
         </div>
      </div>
   </div>
</div>