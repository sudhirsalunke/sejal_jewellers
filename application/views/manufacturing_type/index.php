<div class="content-page">
<div class="content">
   <div class="container">
      <div class="row">
         <div class="col-sm-12 card-box-wrap">
            <div class="panel panel-default">
               <div class="panel-heading">                  
                  <h4 class="inlineBlock">All Manufacturing Types</h4>
                  <div class="pull-right btn-wrap">
                  <a href="Manufacturing_type/create" class="add_senior_manager_button btn btn-purp waves-effect w-md waves-light m-b-5 btn-md pull-right">ADD MANUFACTURING TYPE</a>
                  </div>
               </div>
               <div class="panel-body">                  
                  <div class="clearfix"></div>
                  <div class="table-rep-plugin">
                     <div class="table-responsive b-0 scrollhidden">
                        <table class="table table-bordered custdatatable" id="manufacturing_type">
                           <thead>
                        <?php
                          $data['heading']='manufacturing_type';
                          $this->load->view('master/table_header',$data);
                        ?>        
                             <!--  <tr>
                                  <th class="col4">Manufacturing Types Name</th>
                                 <th class="col3"></th>
                              </tr> -->
                           </thead>
                        </table>
                     </div>
                  </div>
               </div>
            </div>
         </div>
      </div>
   </div>
</div>