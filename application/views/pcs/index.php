<div class="content-page">
<div class="content">
   <div class="container">
      <div class="row">
         <div class="col-sm-12 card-box-wrap">
            <div class="panel panel-default">
               <div class="panel-heading">                  
                  <h4 class="inlineBlock">All PCS</h4>
                  <a href="Pcs/create" class="pull-right single-send m-t-5"><button  type="button" class="add_senior_manager_button btn btn-purp waves-effect w-md waves-light btn-md single-send">ADD PCS </button></a>
               </div>
               <div class="panel-body">                  
                  <div class="clearfix"></div>
                  <div class="table-rep-plugin">
                     <div class="table-responsive b-0 scrollhidden">
                        <table class="table table-bordered custdatatable" id="pcs">
                           <thead>
                           <?php
                          $data['heading']='pcs';
                          $this->load->view('master/table_header',$data);
                        ?>
                          <!--     <tr>
                                  <th class="col4">Pcs Number</th>
                                 <th class="col3"></th>
                              </tr> -->
                           </thead>
                        </table>
                     </div>
                  </div>
               </div>
            </div>
         </div>
      </div>
   </div>
</div>