<div class="content-page">
<div class="content">
   <div class="container">
      <div class="row">
         <div class="col-sm-6 card-box-wrap">
            <div class="panel panel-default">
               <div class="panel-heading frmHeading">                 
                <h4 class="inlineBlock"><span>Add PCS</span></h4>
               </div>
               <div class="panel-body">
                 <div class="col-lg-12">
                  <form name="pcsForm" id="pcsForm" role="form" class="form-horizontal" method="post">
                     <div class="form-group">
                        <label class="col-sm-4 control-label" for="inputEmail3"> Enter Pcs Number <span class="asterisk">*</span></label>
                        <div class="col-sm-8">
                           <input type="text" placeholder="Enter Pcs Number" id="pcs" class="form-control" name="pcs[name]">
                           <span class="text-danger" id="name_error"></span>
                        </div>
                     </div>
                    <div class="btnSection">
                     <!-- <div class="col-sm-offset-5 col-sm-8 texalin"> -->
                        
                         <button class="btn btn-default waves-effect waves-light m-l-5 btn-md" onclick="window.location='<?= ADMIN_PATH?>Pcs'" type="reset">
                           back
                           </button>
                            <button class="btn btn-success waves-effect waves-light btn-md pull-right" name="commit" type="button" onclick="save_pcs(); ">
                         SAVE
                         </button>
                     <!-- </div> -->
                  </div>
                  </form>
               </div>
               </div>
            </div>
         </div>
      </div>
   </div>
</div>