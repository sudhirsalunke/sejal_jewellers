<div class="content-page">      
<div class="content">
   <div class="container">
      <div class="row">
         <div class="col-sm-6 card-box-wrap">
            <div class="panel panel-default">
              <div class="panel-heading frmHeading">
               <h4 class="inlineBlock"><span>Add Category</span></h4>                
              </div>
               <div class="panel-body">
                 <div class="col-lg-12">
                  <form name="category" id="category" role="form" class="form-horizontal" method="post">
                      <div class="form-group">
                        <label class="col-sm-4 control-label" for="inputEmail3"> Enter Category Name <span class="asterisk">*</span></label>
                        <div class="col-sm-8">
                           <input type="text" placeholder="Enter Category Name" id="category" class="form-control" name="Category[name]">
                           <span class="text-danger" id="name_error"></span>
                        </div>
                      </div>
                      <div class="form-group">
                        <label class="col-sm-4 control-label" for="inputEmail3"> Enter Category Code <span class="asterisk">*</span></label>
                        <div class="col-sm-8">
                           <input type="text" placeholder="Enter Category Code" id="code" class="form-control" name="Category[code]" required="" >
                           <span class="text-danger" id="code_error"></span>
                        </div>
                      </div>  
                      
                       <input type="hidden" id="dept_name" name="Category[department_id]" value="1">         
                  <!--     <div class="form-group">
                        <label class="col-sm-4 control-label" for="inputEmail3">Select Department </label>
                        <div class="col-sm-8">
                             <select class="form-control" id="dept_name" name="Category[department_id]">
                          <option value=''>Select Department</option>
                          <?php
                             if(!empty($department)){
                              foreach ($department as $key => $value) {
                                ?>
                                <option value="<?=$value['id']?>"><?=$value['name']?></option>
                                <?php
                              }
                            }
                          ?>
                          </select>
                           <span class="text-danger" id="code_error"></span>
                        </div>
                      </div> -->
                            <!--       <div class="form-group">
                            <label class="col-sm-4 control-label" for="inputEmail3"> Select Pair/PCS <span class="asterisk">*</span> </label>
                            <div class="col-sm-4">                  
                          <input type="radio" name="Category[pair_pcs]" value="0" checked="checked" > PAIR &nbsp;&nbsp;&nbsp;&nbsp; 
                          <input type="radio" name="Category[pair_pcs]" value="1" > PCS 
                               <span class="text-danger" id="code_error"></span>
                            </div>
                          </div> -->
                      <div class="form-group">
                        <label class="col-sm-4 control-label" for="inputEmail3"> Enter Category Purity</label>
                        <div class="col-sm-8">
                           <input type="text" placeholder="Enter Category Purity" id="code" class="form-control" name="Category[purity]" required="" >
                           <span class="text-danger" id="code_error"></span>
                        </div>
                      </div>
                      <div class="form-group">
                        <label class="col-sm-4 control-label" for="inputEmail3"> Enter Category Wastage</label>
                        <div class="col-sm-8">
                           <input type="text" placeholder="Enter Category Wastage" id="code" class="form-control" name="Category[wastage]" required="" >
                           <span class="text-danger" id="code_error"></span>
                        </div>
                      </div>                 
                      <div class="form-group">
                        <label class="col-sm-4 control-label" for="inputEmail3"> Enter Category Labore/Gm</label>
                        <div class="col-sm-8">
                           <input type="text" placeholder="Enter Category Labore/Gm" id="code" class="form-control" name="Category[labore]" required="" >
                           <span class="text-danger" id="code_error"></span>
                        </div>
                      </div>
                      <div class="form-group btnSection">                       
                       
                        <button class="btn btn-default waves-effect waves-light m-l-5 btn-md" onclick="window.location='<?= ADMIN_PATH?>Category'" type="reset">
                         Back
                         </button>
                          <button class="btn btn-success waves-effect waves-light btn-md pull-right" name="commit" type="button" onclick="save_category(); ">
                       SAVE
                        </button>
                      </div>
                  </form>
               </div>
               </div>
               <div class="clearfix"></div>
            </div>
         </div>
      </div>
   </div>
</div>