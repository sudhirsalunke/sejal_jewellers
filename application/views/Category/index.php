<div class="content-page">
<div class="content">
   <div class="container">
      <div class="row">
         <div class="col-sm-12 card-box-wrap">
            <div class="panel panel-default">
              <div class="panel-heading"><h4 class="inlineBlock">All Categories</h4>
              <div class="pull-right single-add btn-wrap">
                <a href="Category/create" class="add_senior_manager_button btn btn-purp waves-effect w-md waves-light btn-md single-add">ADD CATEGORY</a>
                </div>
              </div>
              <div class="panel-body">                
                <div class="clearfix"></div>
                <div class="table-rep-plugin">
                  <div class="table-responsive b-0 scrollhidden">
                   <table class="table table-bordered custdatatable" id="Catagory">
                      <thead>
                        <?php
                          $data['heading']='Catagory';
                          $this->load->view('master/table_header',$data);
                        ?>
                        <!--  <tr>
                             <th class="col4">Category Name</th>
                             <th class="col4">Code</th>
                            <th class="col3"></th>
                         </tr> -->
                      </thead>
                   </table>
                  </div>
                </div>
              </div>
            </div>
         </div>
      </div>
   </div>
</div>