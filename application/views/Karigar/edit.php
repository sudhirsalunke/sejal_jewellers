<div class="content-page">
  <div class="content">
     <div class="container">
        <div class="row">
           <div class="col-sm-12 card-box-wrap">
              <div class="panel panel-default">
                <div class="panel-heading frmHeading">
                 <h4 class="inlineBlock"><span>Edit Karigar</span></h4>                
                </div>
                <div class="panel-body">
                  <div class="col-lg-12">
                    <form name="category" id="Karigar" role="form" class="form-horizontal" method="post">
                         <input type="hidden" id="controller_name" value="Karigar">
                         <input type="hidden" name="Karigar[encrypted_id]" value="<?= $karigar['encrypted_id'] ?>">
                        <?php $columns_array=array(
                                              array( 
                                                    "display_name" => 'Name',
                                                    "placeholder" => 'Enter Name',
                                                    "name"=>'name',
                                                    "type" =>'text',
                                                     "mandatory" =>'*',
                                                    "ex_val"=>$karigar['name'],
                                                  ),
                                                array( 
                                                    "display_name" => 'Code',
                                                    "placeholder" => 'Enter Name',
                                                    "name"=>'code',
                                                    "type" =>'text',
                                                     "mandatory" =>'',
                                                    //"readonly" =>'readonly',
                                                    "ex_val"=>$karigar['code'],
                                                  ),
                                                 
                                                 array( 
                                                    "display_name" => 'Customer Type',
                                                    "placeholder" => '',
                                                    "name"=>'customer_type_id',
                                                    "type" =>'dropdown',
                                                     "mandatory" =>'',
                                                    "dropdown_array"=>$customer_type,
                                                    "ex_val"=>$karigar['customer_type_id'],
                                                  ),
                                                 
                                                 array( 
                                                    "display_name" => 'Mobile Number',
                                                    "placeholder" => 'Enter Mobile Number',
                                                    "name"=>'mobile_no',
                                                     "mandatory" =>'',
                                                    "type" =>'text',
                                                    "ex_val"=>$karigar['mobile_no'],
                                                  ),
                                                array( 
                                                    "display_name" => 'Address',
                                                    "placeholder" => 'Enter Address',
                                                    "name"=>'address',
                                                    "type" =>'text',
                                                     "mandatory" =>'',
                                                    "ex_val"=>$karigar['address'],
                                                  ),
                                                array( 
                                                    "display_name" => 'Area',
                                                    "placeholder" => 'Enter Area',
                                                    "name"=>'area',
                                                    "type" =>'text',
                                                     "mandatory" =>'',
                                                    "ex_val"=>$karigar['area'],
                                                  ),
                                                 array( 
                                                    "display_name" => 'State',
                                                    "placeholder" => '',
                                                    "name"=>'state_id',
                                                    "type" =>'dropdown',
                                                    "dropdown_array"=>$state,
                                                    "id"=>'state',
                                                     "mandatory" =>'',
                                                    "ex_val"=>$karigar['state_id'],
                                                  ),
                                                  array( 
                                                    "display_name" => 'City',
                                                    "placeholder" => '',
                                                    "name"=>'city_id',
                                                    "type" =>'dropdown',
                                                    "dropdown_array"=>$city,
                                                    "id"=>'city',
                                                     "mandatory" =>'',
                                                    "ex_val"=>$karigar['city_id'],
                                                  ),
                                                  array( 
                                                    "display_name" => 'Wastage',
                                                    "placeholder" => 'Enter Wastage',
                                                    "name"=>'wastage',
                                                    "type" =>'text',
                                                     "mandatory" =>'*',
                                                    "ex_val"=>$karigar['wastage'],
                                                  ),
                                                  array( 
                                                    "display_name" => 'Kund Rate',
                                                    "placeholder" => 'Enter Kund Rate',
                                                    "name"=>'kund_rate',
                                                    "type" =>'text',
                                                     "mandatory" =>'',
                                                    "ex_val"=>$karigar['kund_rate'],

                                                  ),
                                                 
                                                  
                                            ); ?>

                          <?php foreach ($columns_array as $c_key => $c_value) { 
                              $display_name =$c_value['display_name'];
                              $placeholder =$c_value['placeholder'];
                              $mandatory =$c_value['mandatory'];
                              $readonly =@$c_value['readonly'];
                              $name =$c_value['name'];
                              $type=$c_value['type'];
                              $id=@$c_value['id'];
                              $old_value=$c_value['ex_val'];

                          ?>
                            
                             <div class="form-group col-sm-6">
                            <label class="col-sm-4 control-label" for="inputEmail3"><?=$display_name?> <span class="asterisk"><?=$mandatory;?></span></label>
                            <div class="col-sm-8">
                              <?php if(@$type=="text"){ ?>

                                  <input type="text" placeholder="<?=$placeholder?>" class="form-control" name="Karigar[<?=$name?>]" value="<?=$old_value;?>" <?=$readonly;?>>

                            <?php  } elseif(@$type=="dropdown") { ?>

                               <select name="Karigar[<?=$name?>]" class="form-control" id="<?=$id?>" exvalue="<?=$old_value?>">
                                <option value="">Select <?=$display_name?></option>
                                <?php foreach ($c_value['dropdown_array'] as $key => $value) { ?>
                                 <option value="<?=$value['id']?>" <?=($old_value ==$value['id']) ? 'selected' : ''; ?>><?=$value['name']?></option>
                                <?php } ?>
                                </select>

                             <?php } else if(@$type=="radio"){

                             }

                              ?>
                               
                               <span class="text-danger" id="<?=$name?>_error"></span>
                            </div>
                         </div>

                         <?php } ?>

                        <div class="btnSection col-sm-12">
                          <button class="btn btn-default waves-effect waves-light m-l-5 btn-md" onclick="window.location='<?= ADMIN_PATH?>Karigar'" type="reset">
                               back
                               </button>
                             <button class="btn btn-success waves-effect waves-light btn-md pull-right" name="commit" type="button" onclick="update_karigar(); ">
                             SAVE
                             </button>                           
                        </div>                     
                      </form>
                  </div>
                </div>
                <div class="clearfix"></div>
              </div>
            </div>
          </div>
        </div>
      </div>
</div>