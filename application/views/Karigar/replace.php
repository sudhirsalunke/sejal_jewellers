<div class="content-page">
  <div class="content">
     <div class="container">
        <div class="row">
           <div class="col-sm-12 card-box-wrap">
              <div class="panel panel-default">
                <div class="panel-heading">
                 <h4 class="inlineBlock"><span><?=$page_title;?></span></h4>
             
                </div>
                <div class="panel-body">
                  <div class="table-rep-plugin">
                    <div class="table-responsive b-0 scrollhidden">
                      <table class="table table-bordered custdatatable" id="karigar_replace">
                        <thead>
                           <tr>
                               <th class="col4">Karigar Name</th>
                               <th class="col4">Code</th>
                               <th class="col4">Limit Weight</th>
                              <th class="col3"></th>
                           </tr>
                        </thead>
                      </table>
                    </div>
                  </div>
                </div>
                <div class="clearfix"></div>                
              </div>
           </div>
        </div>
     </div>
  </div>
</div>