<div class="content-page">
<div class="content">
   <div class="container">
      <div class="row">
         <div class="col-sm-12 card-box-wrap">
            <div class="panel panel-default">
               
               <div class="panel-heading ">
               <h4 class="inlineBlock"><?= $display_name;?></h4>
               <div class="pull-right single-add btn-wrap3">
               <!-- <a href="#" type="button" class="add_senior_manager_button btn btn-purp waves-effect w-md waves-light btn-md pull-right single-add">Accept</a> -->
               </div>
               </div>
               <!--  <button  type="button" onclick="export_qc_products()" class="add_senior_manager_button btn btn-primary  waves-effect w-md waves-light m-b-5 hidden-xs  btn-md">Export</button> -->
                <!-- <button  type="button" class="add_senior_manager_button btn btn-primary  waves-effect w-md waves-light m-b-5 hidden-xs  btn-md" onclick="check_overall_qc()">Check All Qc</button> -->
               
               
                <div class="panel-body">
               <div class="table-rep-plugin">
                  <div class="table-responsive b-0 scrollhidden">
   
                      <form enctype='multipart/form-data' class="check_all_qc" role="form" name="product_form" id="export" action="Manufacturing_quality_control/check_all" method="post">
                        <?php 
                      if ($dep_id != '2'&& $dep_id !='3' && $dep_id !='6' && $dep_id !='10') {
                        $table_name='received_receipt';
                      } else {
                        $table_name='received_receipt_bom_dept';
                      }
                    
                  ?>
                  <input type="hidden" name="dep_id" id="dep_id" value="<?php echo $dep_id; ?>">
                       <table class="table table-bordered custdatatable" id="<?php echo $table_name; ?>">
                          <thead>
                            <?php
                                  $data['heading']=$table_name;
                                  $this->load->view('master/table_header',$data);
                            ?>
                           </thead>
                       </table>
                      </form>
                   </div>
                  </div>
               </div>
            </div>
         </div>
      </div>
   </div>
</div>