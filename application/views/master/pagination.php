<div class="row">
	<div class="col-sm-3">
		<div class="dataTables_info" id="datatable_info" role="status" aria-live="polite">Showing <?=$pagination['showing']?> to <?=$pagination['end']?> of <?=$pagination['count']?> entries
		</div>
	</div>
	<div class="col-sm-9">
		<div class="paging_simple_numbers dataTables_paginate" id="datatable_paginate" style="overflow-x: auto;width:auto;">
			<ul class="pagination">
				<?php if($pagination['page_no']!=1){?>
					<li class="paginate_button previous" aria-controls="datatable" tabindex="0" id="datatable_previous">
						<a href="<?php echo $pagination['order_url'];?>&page_no=<?php echo $pagination['prev_page_id']?>">Previous</a>
					</li>
				<?php }?>
				<?php for($i=1;$i<=$pagination['pages'];$i++) {
					($i==$pagination['page_no']) ? $class = 'active' : $class = '';?>

						<li class="paginate_button <?php echo $class; ?>" aria-controls="datatable" tabindex="0">
						<a  href="<?php echo $pagination['order_url']?>&page_no=<?=$i;?>">
							<?=$i;?>
						</a></li>
				<?php } ?>

				<?php if($pagination['pages']!=$pagination['page_no'] && $pagination['pages']!=0){?>
					<li class="paginate_button next" aria-controls="datatable" tabindex="0" id="datatable_next">
						<a href="<?php echo $pagination['order_url']?>&page_no=<?php echo $pagination['next_page_id']?>">Next</a>
					</li>
				<?php }?>
			</ul>
		</div>
	</div>
</div>

