<!-- Modal -->

<?php $yesNo = yesNo(); ?>
  <div class="modal fade new_modal" id="searchModal" role="dialog">
    <div class="modal-dialog">
    
      <!-- Modal content-->
      <div class="modal-content request_comments">
        <div class="modal-header">
<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
          <h3 class="modal-title">Filter</h3>
        </div>
        <div class="modal-body">
          <form class="form-horizontal" id="searchFrom">
          <?php if(isset($param['key'])) { ?>
        			<div class="form-group">
        				<label class="col-sm-3 control-label"><?=$heading[$param['key']][0]?></label>
                <?php if($heading[$param['key']][0]=='Account Activation'){ ?>
                 <div class="col-md-8 text-left" >
                  <select class="form-control" name="<?=$heading[$param['key']][1] ?>">
                      <?php foreach ($status_type as $st => $status_type) { ?>
                          <option value="<?=$st?>" <?=(isset($param[$heading[$param['key']][1]]) && $param[$heading[$param['key']][1]] ==$status_type)?'selected':''?>><?=$status_type?></option>
                      <?php } ?>
                  </select>
               </div>
                <?php } else if($heading[$param['key']][0]=='Profile Status') { ?>
                <div class="col-md-8 text-left" >
                <select class="form-control" name="<?=$heading[$param['key']][1] ?>">
                    <?php foreach ($profile_status as $ps => $profile_status) { ?>
                        <option value="<?=$ps?>" <?=(isset($param[$heading[$param['key']][1]]) &&$param[$heading[$param['key']][1]]==$ps)?'selected':''?>><?=$profile_status?></option>
                    <?php } ?>
                </select>
               </div>
                <?php } else if($heading[$param['key']][0]=='Is Email Verify' || $heading[$param['key']][0]=='Premium') { ?>
                <div class="col-md-8 text-left" >
                <select class="form-control" name="<?=$heading[$param['key']][1] ?>">
                    <?php foreach ($yesNo as $ps => $yesNo) { ?>
                        <option value="<?=$ps?>" <?=(isset($param[$heading[$param['key']][1]]) && $param[$heading[$param['key']][1]]==$yesNo)?'selected':''?>><?=$yesNo?></option>
                    <?php } ?>
                </select>
               </div>
               <?php }  else if($heading[$param['key']][0]=='Engagement Type') { ?>
                <div class="col-md-8 text-left" >
                <select class="form-control" name="<?=$heading[$param['key']][1] ?>">
                    <?php foreach ($enagement as $en => $enagement ) { ?>
                        <option value="<?=$en?>" <?=(isset($param[$heading[$param['key']][1]]) && $param[$heading[$param['key']][1]]==$enagement)?'selected':''?>><?=$enagement?></option>
                    <?php } ?>
                </select>
               </div>
                <?php } else if($heading[$param['key']][0]=='Status'){?>
                        <div class="col-sm-8">
                          <select name="<?=$heading[$param['key']][1] ?>" class="form-control">
                            <option value="" selected="selected" disabled="disabled">Select Status</option>
                            <option value="1" <?php if(@$param[$heading[$param['key']][1]] =='Active'){?> selected="selected" <?php }?> >Active</option>
                            <option value="0" <?php if(@$param[$heading[$param['key']][1]] =='Inactive'){?> selected="selected" <?php }?> >Inactive</option>
                          </select>
                        </div>
                <?php }else if($heading[$param['key']][0]=='Date'){?>
                        <div class="col-sm-8">
                          <input type="text"  class="form-control datepicker-autoclose" name="<?=$heading[$param['key']][1];?>"  >
                        </div>


               <?php }  else {  ?>

        				<div class="col-sm-8">
        					<input type="text" value="<?=@$param[$heading[$param['key']][1]] ?>" placeholder="<?=$heading[$param['key']][0] ?>" class="form-control" name="<?=$heading[$param['key']][1] ?>" >
        				</div>
                <?php } ?>
                <div class="clearfix"></div>
        			</div>
            <?php } else { $param['key'] = ''; } ?>
      			<?php foreach($heading as $k => $val){
             if(($k!=$param['key'] || $param['key']=="")) {
               if( isset($param[$val[1]])  && $val[1] == 'status' && $param[$val[1]] !=""){
                ?>
                       <div class="form-group divInput">
              <label class="col-sm-3 control-label"><?=$val[0] ?></label>
              <div class="col-sm-8">
                <input readonly type="text" placeholder="<?=$val[0] ?>" value="<?=@$param[$val[1]] ?>" class="form-control" name="<?=$val[1] ?>">
              </div>
              <div class="col-md-1">
                <a href="javascript:void(0);" style="color: red;" class="removeInput"><i class="fa fa-times"></i></a>
              </div>
              <div class="clearfix"></div>
            </div>
              <?php }
              elseif(isset($param[$val[1]]) && !empty($param[$val[1]])){ 
               ?>
               <div class="form-group divInput">
              <label class="col-sm-3 control-label"><?=$val[0] ?></label>
              <div class="col-sm-8">
                <input readonly type="text" placeholder="<?=$val[0] ?>" value="<?=@$param[$val[1]] ?>" class="form-control" name="<?=$val[1] ?>">
              </div>
              <div class="col-md-1">
                <a href="javascript:void(0);" style="color: red;" class="removeInput"><i class="fa fa-times"></i></a>
              </div>
              <div class="clearfix"></div>
      			</div>
      			<?php } } } ?>
          </form>
        </div>
        <div class="modal-footer">
             <button type="button" class="btn btn-info waves-effect" data-dismiss="modal">Close</button>
          <button type="button" onclick="filterTableData('<?=ADMIN_PATH.@$param['search_url']?>');" class="btn btn-primary">Search</button>
        </div>
      </div>
      
    </div>
  </div>
<script type="text/javascript">
$( document ).ready(function(e) {
 $('.datepicker-autoclose').datepicker({autoclose: true,format: 'dd-mm-yyyy',orientation:'top auto'});
});

</script>