  <div class="content-page">
<div class="content">
   <div class="container">
      <div class="row">
         <div class="col-sm-6 card-box-wrap">
            <div class="panel panel-default">
              <div class="panel-heading frmHeading">
               <h4 class="inlineBlock"><span>Add Product Category </span></h4>                
              </div>
              <div class="panel-body">
                 <div class="col-lg-12">
                    <form name="product_category_rate" id="product_category_rate" role="form" class="form-horizontal" method="post">
                      <div class="form-group">
                        <label class="col-sm-5 control-label" for="inputEmail3"> Name <span class="asterisk">*</span></label>
                        <div class="col-sm-7">
                           <input type="text" placeholder="Enter Name" class="form-control" name="product_category[name]">
                           <span class="text-danger" id="name_error"></span>
                        </div>
                      </div>                      
                      <div class="form-group">
                        <label class="col-sm-5 control-label" for="inputEmail3"> Percentage <span class="asterisk">*</span></label>
                        <div class="col-sm-7">
                           <input type="text" placeholder="Enter Percentage" class="form-control" name="product_category[percentage]">  
                           <span class="text-danger" id="percentage_error"> </span>                       
                        </div>
                  
                      </div>
                              
                      <div class="btnSection">
                        
                          
                           <button class="btn btn-default waves-effect waves-light m-l-5 btn-md" onclick="window.location='<?= ADMIN_PATH?>product_category_rate'" type="reset">
                             back
                             </button>
                              <button class="btn btn-success pull-right waves-effect waves-light btn-md" name="commit" type="button" onclick="save_product_category_rate('store'); ">
                           SAVE
                           </button>
                       
                      </div>
                    </form>
                  </div>
              </div>                 
               <div class="clearfix"></div>
            </div>
         </div>
      </div>
   </div>
</div>