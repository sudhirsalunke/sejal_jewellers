<div class="content-page">
<div class="content">
   <div class="container">
      <div class="row">
         <div class="col-sm-12 card-box-wrap">
            <div class="panel panel-default">
               <div class="panel-heading">                  
                  <h4 class="inlineBlock">All Product Category Rate</h4>
                  <div class="pull-right btn-wrap m-t-5">
                     <a href="<?= ADMIN_PATH?>product_category_rate/create" class="add_senior_manager_button btn  btn-warning waves-effect w-md waves-light btn-md">ADD PRODUCT CATEGORY</a>
            
                  </div>
               </div>
               <div class="panel-body">
                  <div class="clearfix"></div>
                  <div class="table-rep-plugin">
                     <div class="table-responsive b-0 scrollhidden">
                        <table class="table table-bordered custdatatable" id="product_category_rate">
                           <thead>                  

                           <tr>
                              <th class="col4">Name</th>
                              <th class="col4">Percentage</th>
                              <th class="col3"></th>
                           </tr>
                           </thead>
                        </table>
                     </div>
                  </div>
               </div>
               
               
            </div>
         </div>
      </div>
   </div>
</div>