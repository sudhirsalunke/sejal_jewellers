<?php 
$page_details=get_page_title($master_name);
//print_r($filter_columns );die;
if($filter_columns!='' && is_array($filter_columns)){
      $tablehead = gettableheaders($filter_columns);
      $table_data = getTableData($html,$filter_columns);

}else{
      $tablehead = gettableheaders($page_details['table_heading']);
      $table_data = getTableData($html,$page_details['table_heading']);
}
?>

<div class="table-wrapper" style="overflow-x: auto !important; height: 20px !important  white-space: nowrap;overflow-y: auto !important;">

      <div class="container-fluid_table ">
            <table class="table-responsive customTable " id="customTableId">

            <thead class="header" id="myHeader">
            <tr class="header_row">
            <?php 
                  if($filter_columns != '' && $table_data !=''){
                        //print_r($tablehead );
                  foreach ($thead as $thkey => $thvalue) {
            ?>    
                  <th class="col4" class="customTableData"><span><?php echo $thvalue[0] ?></span><span><?php echo $thvalue[1] ?></span><?php echo $thvalue[2]?></th>
                  <?php }
            ?>  
            </tr>

            </thead>
            <tbody>
                   <?php   if(!empty($table_data) && $table_data !=''){ foreach ($table_data as $key => $value) {
                        ?>
                  <tr class="customTableData">

                  <?php  foreach($tablehead as $key => $colum){ 
                        
                              if($key == 'checkbox'){ ?>

                                    <td><input type="checkbox" class="from-control" value="<?php echo @$value['id']?>" name="receive_product_id[]" data-parsley-id="76" data-parsley-multiple="group1"></td>
                              <?php
                              }
                              else if($key == 'action'){
                                    ?>
                                    <td class="whiteSpace"><?php echo getActions($value,$page_details['table_name'],$url,$select_url,$table_name);?></td>
                              <?php
                              }else{
                                    ?>
                                    <td class="whiteSpace"><?php echo getColumnData(@$value[$key],$key,@$value['user_id']);?></td>
                              <?php }
                              }                 
                         ?>
                  </tr>
            <?php } }else{?> 
                        <tr><td colspan="12">No Record Found.</td></tr>
            <?php } } else{?>
                        <tr><td colspan="12">Please Select Atleast One Column.</td></tr>
            <?php } ?>
            </tbody>
            </table> 
            </form>
      </div>
</div>