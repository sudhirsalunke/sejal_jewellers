<div class="content-page">
<div class="content">
   <div class="container">
      <div class="row">
         <div class="col-sm-12 card-box-wrap">
            <div class="card-box padmobile">
               <h4 class="header-title business_manager_header_title m-t-0 m-b-30 bmsmheader bmsmheaderabm">All Sub Category</span>
               </h4>
                <a href="<?= ADMIN_PATH?>parent_sub_category/create" class="add_senior_manager_button btn btn-primary  waves-effect w-md waves-light m-b-5  btn-md">ADD PARENT SUB CATEGORY</a>
               <div class="clearfix"></div>
               <div class="table-rep-plugin">
                  <div class="table-responsive b-0 scrollhidden">
               <table class="table table-bordered custdatatable" id="parent_sub_category_table">
                  <thead>
                     <tr>
                        <th class="col4">Product Category</th>
                        <th class="col4">Parent Category</th>
                        <th class="col3"></th>
                     </tr>
                  </thead>
               </table>
               </div>
               </div>
            </div>
         </div>
      </div>
   </div>
</div>