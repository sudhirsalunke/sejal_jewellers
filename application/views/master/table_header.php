<?php
$page_header_details=getTableHeaderSettings($heading);
$is_clear_filter = false;
foreach ($page_header_details as $key => $value) { 
  if(isset($value['is_filter']) && $value['is_filter'] == true){
?>
     <?php if(isset($value['label']) && $value['label'] == 'drop_down'){     
     
             ?>
     <div class="col-sm-2 filters_filed<?=(@$value['id']);?>" id="filter_col<?=($key+1)?>" data-column="<?=$key?>" <?= (!empty($value['default_display']) && $value['default_display'] == true) ? $is_clear_filter = true : 'style="display: none"' ?>>
            <select id="col<?=$key?>_filter" class="form-control search-input-select column_filter <?=(@$value['label']);?>" name="<?=(@$value['column_name']);?>">
              <option value="">Select <?=(@$value['column_name']);?></option>
                 <?php
             $drop_down_list=$this->$value['model_name']->$value['function_name']();
            // print_r($value['model_name']);
                foreach ($drop_down_list as $key => $value) { ?>
                    <option value="<?=$value['name']?>"><?=$value['name']?></option>
               <?php } ?> 
            </select>
    </div> 
  <?php
    }else
    { 
  ?> 
    <div class="col-sm-2 filters_filed<?=(@$value['id']);?>" id="filter_col<?=($key+1)?>" data-column="<?=$key?>" style="display: none">
      <span >
        <input type="text" name="<?=(@$value['column_name']);?>" placeholder="<?=(@$value['column_name']);?>"  class="column_filter txtfilter form-control <?=(@$value['label']);?>" id="col<?=$key?>_filter">
      </span>
    </div> 

  <?php
    }
  }
} 
?> 
<span id="checked_count" style="display: none">Checked: </span><span id="checked"></span>
<div class="col-sm-2 clear_filters"  <?= ($is_clear_filter==false) ? 'style="display: none"' : ''?>>
  <span>

    <button type="button" onclick="clear_filters()" class="btn btn-primary pull-left waves-effect waves-light  btn-md">Clear Filters</button>
  </span>
</div>

<tr>
<?php 
  foreach ($page_header_details as $key => $value) {?>

<?php if(isset($_GET['status']) && $_GET['status'] =='due_date' ||  isset($_GET['status']) && $_GET['status'] =='stock'||  isset($_GET['status']) && $_GET['status'] =='sent' ||  isset($_GET['status']) && $_GET['status'] =='accepted'  ||  isset($_GET['status']) && $_GET['status'] =='rejected' || isset($value['table_column']) ){ 

  /*if(isset($_GET['status']) && $_GET['status'] =='stock'){
      if($value['table_column'] =='gt.created_at'){ 
        continue; 
      } 
     if($value['column_name'] =='Challan No'){ 
        continue; 
      } 
      if($value['column_name'] =='Total NEt.Wt'){ 
       continue; 
      }
      if($value['column_name'] =='Size'){ 
       continue; 
      }
    }else if(isset($_GET['status']) && $_GET['status'] !='stock'){  
      if($value['table_column'] =='s.created_at'){ 
        continue; 
      } 
      if($value['column_name'] =='Product Code'){ 
        continue; 
      } 
       if($value['column_name'] =='Category'){ 
        continue; 
      } 
      if($value['column_name'] =='Size'){ 
        continue; 
      }
    } */


 /* if($value['column_name'] =='Due Date' && isset($_GET['status']) && $_GET['status'] !='due_date'){ 
      continue; 
      } */
  }
 

  ?>
  <th class="col4">

    <?php if(isset($value['is_sort']) && $value['is_sort'] !='') { ?>
      <i class="fa fa-fw fa-sort" ></i> 
    <?php } ?>

    <?php if(isset($value['is_filter']) && $value['is_filter'] !='') { ?>
      <i class="fa fa-filter" onclick="getFilterData(this,'<?=(@$value['table_column']);?>','<?=(@$value['get_functions_name']);?>','<?=(@$value['column_name']);?>','<?=(@$value['id']);?>')" style="cursor: pointer;"></i>
    <?php } ?>
    <?php 
      
    if($heading =='Receive_products' && isset($_GET['status']) && $_GET['status'] =='pending' && isset( $value['type']) &&  $value['type']=='checkbox' || $heading =='Quality_control' && isset($_GET['status']) && $_GET['status'] =='complete' && isset( $value['type']) &&  $value['type']=='checkbox'|| $heading =='Hallmarking' && isset($_GET['status']) && $_GET['status'] =='exported'  && isset( $value['type']) &&  $value['type']=='checkbox'|| $heading =='Hallmarking' && isset($_GET['status']) && $_GET['status'] =='send' && isset( $value['type']) &&  $value['type']=='checkbox' || $heading =='AmendView' && isset( $value['type']) &&  $value['type']=='checkbox'|| $heading =='customer_received_order' && isset($_GET['status']) && $_GET['status'] !='sent' &&  isset( $value['type']) &&  $value['type']=='checkbox' || $heading =='customer_order_pending' && isset( $value['type']) &&  $value['type']=='checkbox' && isset($_GET['status']) && $_GET['status'] =='pending'|| $heading =='Generate_packing_list_create' && isset( $value['type']) &&  $value['type']=='checkbox' || $heading =='manufacturing_hallmarking' && isset( $value['type']) &&  $value['type']=='checkbox' && isset($_GET['status']) && $_GET['status'] =='send'  || $heading =='kundan_karigar' && isset( $value['type']) &&  $value['type']=='checkbox' && isset($_GET['status']) && $_GET['status'] =='complete'|| $heading =='kundan_karigar_pending' && isset( $value['type']) &&  $value['type']=='checkbox' || $heading =='manufacturing_ready_product' && isset( $value['type']) &&  $value['type']=='checkbox' ||  $heading =='department_all_view' && isset( $value['type']) &&  $value['type']=='checkbox' ||  $heading =='department_wise_view' && isset( $value['type']) &&  $value['type']=='checkbox' || $heading =='tag_products' && isset( $value['type']) &&  $value['type']=='checkbox' || $heading =='customer_order_not_send' && isset($_GET['status']) && $_GET['status'] =='not_send' && isset( $value['type']) &&  $value['type']=='checkbox' ||   $heading =='mfg_ready_product_dept_wise' && isset( $value['type']) &&  $value['type']=='checkbox' ||   $heading =='sales_recieved_product_show_stock_table' && isset( $value['type'])  &&  $value['type']=='checkbox'||   $heading =='kundan_qc_tbl' && isset( $value['type'])  &&  $value['type']=='checkbox'&& isset($_GET['status']) && $_GET['status'] !='rejected' && $_GET['status'] !='send_qc_pending'||   $heading =='department_mangalsutra_view' && isset( $value['type'])  &&  $value['type']=='checkbox' ||   $heading =='sales_recieved_product_show_stock_table_dep' && isset( $value['type'])  &&  $value['type']=='checkbox'||   $heading =='sales_recieved_product_show_table' && isset( $value['type'])  &&  $value['type']=='checkbox' ||   $heading =='sales_recieved_product_show_table_dep' && isset( $value['type'])  &&  $value['type']=='checkbox'  && isset($_GET['status']) && $_GET['status'] =='sended_to_manufacture' || $heading =='sales_recieved_product_table' && isset( $value['type'])  &&  $value['type']=='checkbox' || $heading =='sales_stock_table' && isset( $value['type'])  &&  $value['type']=='checkbox'|| $heading =='sales_stock_table_dep' && isset( $value['type'])  &&  $value['type']=='checkbox'|| $heading =='sales_recieved_product_table_dep' && isset( $value['type'])  &&  $value['type']=='checkbox' || $heading =='sales_voucher_return_product' && isset( $value['type'])  &&  $value['type']=='checkbox'){?>
      <div class="checkbox checkbox-purpal">
      <input value="0" type="checkbox" id="check_all" name="check_all">
      <label for="check_all"></label> 
      </div>
<?php }else{ ?>
    
    <?= $value['column_name'];?>
     <?php } ?> 
 
  </th>
  <?php } ?>
   <!-- <input type="text" id="selected_checkbox" name="selected_checkbox"> -->
</tr>

<script type="text/javascript">
$(document).ready(function(){
  $('.date').datepicker({autoclose: true,format: 'dd-mm-yyyy',orientation:'top auto'});

  var table_id =<?php echo ($heading);?>;
  var dataTable = $(table_id).DataTable();
  $('.search-input-select').on( 'change', function () {
    filterColumn( $(this).parents('div').attr('data-column') );
  } );

  $('input.column_filter').on( 'keyup click change', function () {
    filterColumn( $(this).parents('div').attr('data-column') );
  } );

  function filterColumn ( i ) {
    dataTable.column( i ).search(
    $('#col'+i+'_filter').val(),
    $('#col'+i+'_regex').prop('checked'),
    $('#col'+i+'_smart').prop('checked')
    ).draw();
  }


   var countChecked = function($table, checkboxClass) {
  if ($table) {
    // Find all elements with given class
    var chkAll = $table.find(checkboxClass);
    // Count checked checkboxes
    var checked = chkAll.filter(':checked').length;
    // Count total
    var total = chkAll.length;    
    // Return an object with total and checked values
     return {
      total: total,
      checked: checked

    }
  }
}
var check_array=[];
$(document).on('change', '.checkbox_ctn', function() {
  var result = countChecked($(table_id), '.checkbox_ctn');
  $('#checked').html(result.checked);

 if(result.checked !=0){
  $("#checked_count").show();
  $('#checked').html(result.checked);
  }else{
    $("#checked_count").hide();
     $('#checked').html('');
  }

 // alert(check_array.push($(this).val()));
/*  if ($(this).is( ":checked" )){
      //checkbox_checked($(this).val());    
      check_array.push($(this).val());
     }else{
        // checkbox_unchecked($(this).val());
        check_array.splice($.inArray($(this).val(), check_array),1);
     }
    $('#selected_checkbox').val(JSON.stringify(check_array));*/

});



$( ".checkbox-purpal" ).on( "click", function() {

 
  var checked = $('.checkbox-purpal [type="checkbox"]:checked').length-1;
    if(checked >0){
    $("#checked_count").show();
    $('#checked').html(checked);
    }else{
      $("#checked_count").hide();
      $('#checked').html('');
    }
});



});
</script>
