<div class="content-page">
  <div class="content">
    <div class="container">
      <div class="row">

        <div class="col-sm-12 card-box-wrap">
          <div class="card-box padmobile dash-board">
            <h5 class=" m-b-20"><!--header-title business_manager_header_title-->
            <span><?=$display_title;?></span>
            </h5>          
            
            <h4 class="m-b-20 header-title">
            <span>Order Received</span>
            </h4>

            <div class="row">              
              <div class="col-md-12 col-xs-12 section-3 product-box marg_top-bottom no_pad">
                <div class="col-lg-3 col-md-3 col-sm-6 aos-item" data-aos="fade-down"  data-aos-delay="500" data-aos-duration="800">
                  <a href="<?= ADMIN_PATH.'Search_order'?>">
                   <div class="card-box dash-box box-shadow darkBlue_box dash_box_content text-center">
                    
                    <div class="widget-chart-1 white_color_font">
                     <!--  <div class="widget-detail-1"> -->
                        <h4 class="">Upload New Orders</h4>
                        <h2 class="number-size"> 0 </h2>
                        <span class="label label-gray">ORDERS</span>
                     <!--  </div> -->
                    </div>
                  </div></a>
                </div><!-- end col -->
                <div class="col-lg-3 col-md-3 col-sm-6 aos-item" data-aos="fade-down"  data-aos-delay="550" data-aos-duration="800">
                  <a href="<?= ADMIN_PATH.'Prepare_order' ?>">
                  <div class="card-box dash-box box-shadow orange_box dash_box_content text-center">                    
                    <div class="widget-chart-1 white_color_font">
                      <!-- <div class="widget-detail-1"> -->
                        <h4 class="">Not Assigned To Karigar</h4>
                        <h2 class="number-size"> <?=$product_not_assign_to_karigar?>  </h2>
                        <span class="label label-gray">DESIGNS</span>
                     <!--  </div> -->
                    </div>
                  </div>
                  </a>
                </div><!-- end col -->
                <div class="col-lg-3 col-md-3 col-sm-6 aos-item" data-aos="fade-down"  data-aos-delay="600" data-aos-duration="800">
                  <a href="<?= ADMIN_PATH.'Karigar_product_list' ?>">
                  <div class="card-box dash-box box-shadow lightPink_box dash_box_content text-center">
                    
                    <div class="widget-chart-1  white_color_font">
                     <!--  <div class="widget-detail-1"> -->
                        <h4 class="">Not Sent To Karigar</h4>
                        <h2 class="number-size"> <?=$product_not_sent_to_karigar?>  </h2>
                        <span class="label label-gray">DESIGNS</span>
                      <!-- </div> -->
                    </div>
                  </div></a>
                </div><!-- end col --> 
                <div class="col-lg-3 col-md-3 col-sm-6 aos-item" data-aos="fade-down"  data-aos-delay="650" data-aos-duration="800">
                    <a href="<?= ADMIN_PATH.'Products_sent' ?>">
                    <div class="card-box dash-box box-shadow yellow_box dash_box_content text-center">
                    
                    <div class="widget-chart-1 white_color_font">
                     <!--  <div class="widget-detail-1"> -->
                        <h4 class="">Sent To Karigar</h4>
                        <h2 class="number-size"> <?=$product_sent_to_karigar?>  </h2> </h2>
                        <span class="label label-gray">DESIGNS</span>
                       <!--  </div> -->
                    </div>
                  </div></a>
                </div><!-- end col -->

                <div class="col-lg-3 col-md-3 col-sm-6 aos-item" data-aos="fade-up"  data-aos-delay="750" data-aos-duration="800">
                  <a href="<?= ADMIN_PATH.'Engaged_karigar_list' ?>">
                  <div class="card-box dash-box box-shadow green_box dash_box_content text-center">
                   
                      <div class="widget-chart-1 white_color_font">
                     
                         <h4 class="">Karigar Engaged</h4>
                        <h2 class="number-size"><?=$engaged_karigar;?> </h2>

                        <span class="label label-gray">KARIGAR</span>
                    
                    </div>
                  </div></a>
                </div> <!-- end col--> 

              <div class="col-lg-3 col-md-3 col-sm-6 aos-item" data-aos="fade-up"  data-aos-delay="750" data-aos-duration="800">
                  <a href="<?= ADMIN_PATH.'Corporate_order_list' ?>">
                  <div class="card-box dash-box box-shadow pink_box dash_box_content text-center">
                   
                      <div class="widget-chart-1 white_color_font">
                     
                         <h4 class="">Order Details</h4>
                        <h2 class="number-size"><?=$engaged_karigar_by_order;?> </h2>

                        <span class="label label-gray">Order</span>
                    
                    </div>
                  </div></a>
                </div> <!-- end col-->
             <!--    <div class="col-lg-3 col-md-3 col-sm-6 aos-item" data-aos="fade-up"  data-aos-delay="850" data-aos-duration="800">
                  <a href="<?= ADMIN_PATH.'Engaged_karigar_list?status=due_date' ?>">
                  <div class="card-box dash-box box-shadow dark_purple_box dash_box_content text-center">
                   
                      <div class="widget-chart-1 white_color_font">
                      
                         <h4 class="">Due Date Tracking</h4>
                        <h2 class="number-size"><?=$due_date_engaged_karigar;?></h2>
                        <span class="label label-gray">KARIGAR</span>
                      
                    </div>
                  </div></a>
                </div> --><!-- end col -->                 

              </div>
            </div>
            

              <!--==================================-->
              <h4 class="m-b-20  header-title">
                <span> Product Received</span>
              </h4>

            <div class="row">              
              <div class="col-md-12 col-xs-12 section-3 product-box marg_top-bottom no_pad">
                <div class="col-lg-3 col-md-3 col-sm-6  aos-item" data-aos="zoom-out"  data-aos-delay="500" data-aos-duration="800">
                  <a href="<?= ADMIN_PATH.'Receive_products' ?>">
                  <div class="card-box dash-box box-shadow grey_box dash_box_content text-center">
                    
                    <div class="widget-chart-1 white_color_font">
                     <!--  <div class="widget-detail-1"> -->
                        <h4 class="">Received From Karigar</h4>
                        <h2 class="number-size"> <?=$received_products;?> </h2>
                        <span class="label label-gray">PRODUCTS</span>
                      <!-- </div> -->
                    </div>
                  </div></a>
                </div><!-- end col -->
               <div class="col-lg-3 col-md-3 col-sm-6  aos-item" data-aos="zoom-out"  data-aos-delay="600" data-aos-duration="800">
                  <a href="<?= ADMIN_PATH.'Receive_products?status=pending' ?>">
                  <div class="card-box dash-box box-shadow yellowGold_box dash_box_content text-center">
                    
                    <div class="widget-chart-1 white_color_font">
                      <!-- <div class="widget-detail-1"> -->
                        <h4 class="">Pending For QC</h4>
                        <h2 class="number-size"> <?=$pending_qc_products;?> </h2>
                        <span class="label label-gray">PRODUCTS</span>
                      <!-- </div> -->
                    </div>
                  </div></a>
                </div><!-- end col -->
                <!-- <div class="col-lg-3 col-md-3">
                  <a href="<?= ADMIN_PATH.'Quality_control?status=complete' ?>"><div class="card-box dash-box box-shadow">
                    <h4 class="header-title m-t-0 m-b-30">QC Accepted</h4>
                    <div class="widget-chart-1">
                      <div class="widget-detail-1">
                        <h2 class="number-size light-green-font"> <?=sizeof($total_qc_products);?> </h2>
                        <span class="label label-gray">PRODUCTS</span>
                      </div>
                    </div>
                  </div></a>
                </div>--> 

                <div class="col-lg-3 col-md-3 col-sm-6  aos-item" data-aos="zoom-out"  data-aos-delay="650" data-aos-duration="800">
                  <a href="<?= ADMIN_PATH.'Quality_control?status=rejected' ?>">
                  <div class="card-box dash-box box-shadow red_box dash_box_content text-center">
                    
                    <div class="widget-chart-1 white_color_font">
                      <!-- <div class="widget-detail-1"> -->
                        <h4 class="">QC Rejected</h4>
                        <h2 class="number-size"> <?=$rejected_qc_products;?> </h2>

                        <span class="label label-gray">PRODUCTS</span>
                      <!-- </div> -->
                    </div>
                  </div></a>
                </div><!-- end col -->

                <div class="col-lg-3 col-md-3 col-sm-6  aos-item" data-aos="zoom-out"  data-aos-delay="700" data-aos-duration="800">
                  <a href="<?= ADMIN_PATH.'Amend_products' ?>">
                  <div class="card-box dash-box box-shadow pink_box dash_box_content text-center">
                   
                    <div class="widget-chart-1 white_color_font">
                      <!-- <div class="widget-detail-1"> -->
                        <h4 class="">QC Amended</h4>
                        <h2 class="number-size"> <?=$amended_qc_products;?> </h2>

                        <span class="label label-gray">PRODUCTS</span>
                     <!--  </div> -->
                    </div>
                  </div></a>
                </div><!-- end col -->
                
              </div></a>
            </div>
            <!-- End row -->
            <h4 class="m-b-20  header-title">
            <span>Product Sent</span>
            </h4>

            <div class="row">              
              <div class="col-md-12 col-xs-12 section-3 product-box marg_top-bottom no_pad">

                 <div class="col-lg-3 col-md-3 col-sm-6 aos-item" data-aos="zoom-in"  data-aos-delay="500" data-aos-duration="800">
                  <a href="<?= ADMIN_PATH.'Quality_control?status=complete' ?>">
                  <div class="card-box dash-box box-shadow yellow_box dash_box_content text-center">
                   
                    <div class="widget-chart-1 white_color_font">
                      <!-- <div class="widget-detail-1"> -->
                         <h4 class="">Pending For Hallmarking</h4>
                        <h2 class="number-size "> <?=$total_qc_products;?> </h2>

                        <span class="label label-gray">PRODUCTS</span>
                     <!--  </div> -->
                    </div>
                  </div></a>
                </div><!-- end col -->

                <div class="col-lg-3 col-md-3 col-sm-6 aos-item" data-aos="zoom-in"  data-aos-delay="600" data-aos-duration="800">
                  <a href="<?= ADMIN_PATH.'Hallmarking?status=send' ?>">
                  <div class="card-box dash-box box-shadow blue_box dash_box_content text-center">                     
                    <div class="widget-chart-1 white_color_font">
                      <!-- <div class="widget-detail-1"> -->
                        <h4 class="">Sent For Hallmarking</h4>
                        <h2 class="number-size "> <?= $send_qc_products;?> </h2>

                        <span class="label label-gray">PRODUCTS</span>
                      <!-- </div> -->
                    </div>
                  </div></a>
                </div><!-- end col -->
               
                <!-- <div class="col-lg-3 col-md-3">
                  <a href="<?= ADMIN_PATH.'Hallmarking?status=receive' ?>"><div class="card-box dash-box box-shadow">
                    <h4 class="header-title m-t-0 m-b-30">Hallmarking QC Accepted</h4>
                    <div class="widget-chart-1">
                      <div class="widget-detail-1">
                        <h2 class="number-size light-green-font"><?= sizeof($recieve_qc_products);?></h2>
                        <span class="label label-gray">PRODUCTS</span>
                      </div>
                    </div>
                  </div></a>
                </div> --><!-- end col -->
               <!--  <div class="col-lg-3 col-md-3">
                  <a href="<?= ADMIN_PATH.'Hallmarking?status=rejected' ?>"><div class="card-box dash-box box-shadow">
                    <h4 class="header-title m-t-0 m-b-30">Hallmarking QC Rejected</h4>
                    <div class="widget-chart-1">
                      <div class="widget-detail-1">
                        <h2 class="number-size light-red-font"><?= sizeof($rejected_hallmarking_qc_products);?></h2>
                        <span class="label label-gray">PRODUCTS</span>
                      </div>
                    </div>
                  </div></a>
                </div> --><!-- end col -->

                <div class="col-lg-3 col-md-3 col-sm-6 aos-item" data-aos="zoom-in" data-aos-delay="700" data-aos-duration="800">
                  <a href="<?= ADMIN_PATH.'Hallmarking?status=exported' ?>">
                  <div class="card-box dash-box box-shadow green_box dash_box_content text-center">
                    
                    <div class="widget-chart-1 white_color_font">
                     <!--  <div class="widget-detail-1"> -->
                        <h4 class="">Hallmarking QC Accepted</h4>
                        <h2 class="number-size"><?=$exported_qc_products;?></h2>

                        <span class="label label-gray">PRODUCTS</span>
                     <!--  </div> -->
                    </div>
                  </div></a>
                </div><!-- end col -->

                 <div class="col-lg-3 col-md-3 col-sm-6 aos-item" data-aos="zoom-in"  data-aos-delay="800" data-aos-duration="800">
                  <a href="<?= ADMIN_PATH.'Stock/1/?status=stock'?>">
                  <div class="card-box dash-box box-shadow dark_purple_box dash_box_content text-center">
                    
                    <div class="widget-chart-1 white_color_font">
                     <!--  <div class="widget-detail-1"> -->
                        <h4 class="">Stock</h4>
                        <h2 class="number-size"> <?=$stock_product;?> </h2>

                        <span class="label label-gray">PRODUCTS</span>
                      <!-- </div> -->
                    </div>
                  </div></a>
                </div><!-- end col -->

              </div></a>
            </div>
             <div class="row but-sec">
             <div class="col-md-10 col-xs-12 text-center">
             
                  
                </div>
              </div>
              <!--===================================================-->
              <div class="row">  
                  <div class="col-md-12 col-xs-12 section-2">
                  <img src="assets/images/reliance-logo.png" alt="reliance-logo" class="img-responsive">
                </div>            

              <div class="col-md-12 col-xs-12 section-3 product-box marg_top-bottom no_pad">

                 <div class="col-lg-3 col-md-3 col-sm-6 aos-item" data-aos="zoom-out-up"  data-aos-delay="500" data-aos-duration="800">
                  <a href="<?= ADMIN_PATH.'Stock/1/?status=sent' ?>">
                    <div class="card-box dash-box box-shadow blue_box dash_box_content text-center">
                    
                    <div class="widget-chart-1 white_color_font">
                      <!-- <div class="widget-detail-1"> -->
                        <h4 class="">Product Sent</h4>
                        <h2 class="number-size"><?=$sent_to_relience;?></h2>

                        <span class="label label-gray">PRODUCTS</span>
                     <!--  </div> -->
                    </div>
                  </div></a>
                </div><!-- end col -->


                <div class="col-lg-3 col-md-3 col-sm-6 aos-item" data-aos="zoom-out-up"  data-aos-delay="600" data-aos-duration="800">
                  <a href="<?= ADMIN_PATH.'Stock/1/?status=accepted' ?>">
                  <div class="card-box dash-box box-shadow darkGreen_box dash_box_content text-center">
                   
                    <div class="widget-chart-1 white_color_font">
                      <!-- <div class="widget-detail-1"> -->
                         <!-- <h4 class="">Product Accepted</h4> -->
                          <h4 class="">Product Approved</h4>
                        <h2 class="number-size"><?=$accepted_by_relience;?></h2>

                        <span class="label label-gray">PRODUCTS</span>
                      <!-- </div> -->
                    </div>
                  </div></a>
                </div><!-- end col -->
                <div class="col-lg-3 col-md-3 col-sm-6 aos-item" data-aos="zoom-out-up"  data-aos-delay="700" data-aos-duration="800">


                  <a href="<?= ADMIN_PATH.'Stock/1/?status=rejected' ?>">
                  <div class="card-box dash-box box-shadow orange_box dash_box_content text-center">
                   
                    <div class="widget-chart-1 white_color_font">
                      <!-- <div class="widget-detail-1"> -->
                        <h4 class="">Product Rejected</h4>
                        <h2 class="number-size"><?=$rejected_by_relience;?></h2>

                        <span class="label label-gray">PRODUCTS</span>
                      <!-- </div> -->
                    </div>
                  </div></a>
                </div><!-- end col -->
                
              </div></a>
            </div>
             <div class="row but-sec">
             <div class="col-md-10 col-xs-12 text-center">
             
                  
                </div>
              </div>
              <!--===================================================-->
              <!--===================================================-->
             

            <div class="row">  
             <div class="col-md-12 col-xs-12 section-2">
                  <img src="assets/images/carat-logo.png" alt="carat-logo" class="img-responsive">
                </div>            
              <div class="col-md-12 col-xs-12 section-3 product-box marg_top-bottom no_pad">

                 <div class="col-lg-3 col-md-3 col-sm-6 aos-item" data-aos="fade-right"  data-aos-delay="500" data-aos-duration="800">
                  <a href="<?= ADMIN_PATH.'Generate_packing_list/create' ?>">
                  <div class="card-box dash-box box-shadow purple_box dash_box_content text-center">
                    <div class="widget-chart-1 white_color_font">
                     <!--  <div class="widget-detail-1"> -->
                        <h4 class="">Generate Packing List</h4>
                        <h2 class="number-size"><?=$generate_packing_list;?></h2>

                        <span class="label label-gray">PRODUCTS</span>
                      <!-- </div> -->
                    </div>
                  </div></a>
                </div><!-- end col -->
                <div class="col-lg-3 col-md-3 col-sm-6 aos-item" data-aos="fade-right"  data-aos-delay="600" data-aos-duration="800">
                  <a href="<?= ADMIN_PATH.'Generate_packing_list' ?>">
                  <div class="card-box dash-box box-shadow pink_box dash_box_content text-center">
                    
                    <div class="widget-chart-1 white_color_font">
                      <!-- <div class="widget-detail-1"> -->
                        <h4 class="">Packing List</h4>
                        <h2 class="number-size"><?=$billing_list;?></h2>
                        <span class="label label-gray">PRODUCTS</span>
                      <!-- </div> -->
                    </div>
                  </div></a>
                </div><!-- end col -->
             <div class="col-lg-3 col-md-3 col-sm-6 aos-item" data-aos="fade-right"  data-aos-delay="700" data-aos-duration="800">
              <a href="<?= ADMIN_PATH.'Generate_packing_list?status=approved' ?>">
              <div class="card-box dash-box box-shadow green_box dash_box_content text-center">
               
                <div class="widget-chart-1 white_color_font">
                  <!-- <div class="widget-detail-1"> -->
                     <h4 class="">Product Approved</h4>
                    <h2 class="number-size"><?=$caratlane_approved;?></h2>
                    <span class="label label-gray">PRODUCTS</span>
                  <!-- </div> -->
                </div>
              </div></a>
            </div><!-- end col -->
            <div class="col-lg-3 col-md-3 col-sm-6 aos-item" data-aos="fade-right"  data-aos-delay="800" data-aos-duration="800">
              <a href="<?= ADMIN_PATH.'Generate_packing_list?status=rejected' ?>">
                <div class="card-box dash-box box-shadow red_box dash_box_content text-center">
                
                <div class="widget-chart-1 white_color_font">
                  <!-- <div class="widget-detail-1"> -->
                    <h4 class="">Product Rejected</h4>
                    <h2 class="number-size"><?=$caratlane_rejected;?></h2>
                    <span class="label label-gray">PRODUCTS</span>
                 <!--  </div> -->
                </div>
              </div></a>
            </div><!-- end col -->
                
               
             <div class="row but-sec">
             <div class="col-md-10 col-xs-12 text-center">
             
                  
                </div>
              </div>
              <!--===================================================-->
            
          </div><!-- end card-box padmobile -->
        </div><!-- end col-sm-12 card-box-wrap -->
      </div><!-- end row -->
    </div><!-- end container -->
  </div><!-- end content -->
</div><!-- end content-page -->