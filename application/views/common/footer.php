  <footer class="footer">
    <?=date('Y')?> © sejal jewellers.
  </footer>
</div>
            <!-- ============================================================== -->
            <!-- End Right content here -->
            <!-- ============================================================== -->
</div>
     <div id="myModal" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                    <h4 class="modal-title" id="myModalLabel">Scan product barcode</h4>
                </div>
                <div class="modal-body">
                   <!--  <h4>Text in a modal</h4> -->
                   <input type="text" name="barcode" class="form-control" placeholder="Enter Product Code" tabindex="1" autofocus="autofocus" id="barcode_text_box"> 
                    <p id="model_status"></p>
                </div>
                <div class="modal-footer">  
                    <button id="barcode_submit" type="button" class="btn btn-primary waves-effect waves-light" onclick="Barcode_submit()">Submit</button>
                </div>
            </div><!-- /.modal-content -->
        </div><!-- /.modal-dialog -->
    </div>
    <div id="create_karigar" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                    <h4 class="modal-title">Create Karigar Order</h4>
                </div>
                <div class="modal-body">
                    <form id="create_karigar" role="form" class="form-horizontal" method="post">
                            <div class="form-group">
                                <label class="col-sm-4 control-label" for="inputEmail3"> Enter Quantity <span class="asterisk">*</span></label>
                                <div class="col-sm-6">
                               <input type="text" name="quantity" class="form-control karigar_quantity" placeholder="Enter Quantity" tabindex="1" autofocus="autofocus" id="barcode_text_box" onkeyup="change_approx_weight(this.value)">
                                <span class="text-danger" id="quantity_error"></span>
                               </div>
                            </div>
                            <div class="form-group">
                                <label class="col-sm-4 control-label" for="inputEmail3"> Apprx. weight <span class="asterisk">*</span></label>
                                <div class="col-sm-6">
                               <input readonly type="text" name="approx_weight" class="form-control karigar_weight" placeholder="Enter Approximate weight" id="approx_weight" style="cursor: not-allowed;">
                                <span class="text-danger" id="karigar_weight_error"></span>
                               </div>
                            </div>
                            <div class="form-group">
                                <label class="col-sm-4 control-label" for="inputEmail3"> Product Code <span class="asterisk">*</span></label>
                                <div class="col-sm-6">
                                <label for="inputEmail3" id="product_code"></label>
                               </div>
                            </div>
                            <div class="form-group">   
                                <label class="col-sm-4 control-label" for="inputEmail3"> Select Karigar <span class="asterisk">*</span></label>
                                <div class="col-sm-6">
                                <input type="hidden" id="mop_id" name="mop_id" value="">
                                <select class="form-control change_kariger" name="karigar_id">
                                <option value=''>Select Karigar</option>
                                <?php 
                                if(!empty($all_karigars)){
                                    foreach ($all_karigars as $key => $value) {
                                    ?>
                                    <option value="<?= $value['id']?>"><?= $value['name']?></option>
                                    <?php

                                    }
                                }  
                                ?>
                                </select>
                                <span class="text-danger" id="karigar_id_error"></span>
                                <br/><a href="javascript:void(0);" style="display:none;" class="karigar_history" target="_blank">Show Kariger History</a>
                                </div>
                            </div>
                    <div class="modal-footer">  
                        <button id="barcode_submit" type="button" class="btn btn-primary waves-effect waves-light" onclick="create_mnfctrng_karigar()">Submit</button>
                    </div>
                   </form>
                   <input type="hidden" id="mid_wight_range_hidden"/>
               </div> 
            </div><!-- /.modal-content -->
        </div><!-- /.modal-dialog -->
    </div>
     <div id="change_karigar" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                    <h4 class="modal-title">Change Karigar</h4>
                </div>
                <div class="modal-body">
                    <form id="change_karigar" role="form" class="form-horizontal" method="post">
                            <div class="form-group">   
                                <label class="col-sm-4 control-label" for="inputEmail3"> Select Karigar <span class="asterisk">*</span></label>
                                <div class="col-sm-6">
                                <input type="hidden" id="mop_id" name="mop_id" value="">
                                <select class="form-control change_kariger" name="karigar_id" >
                                <option value=''>Select Karigar</option>
                                <?php 
                                if(!empty($all_karigars)){
                                    foreach ($all_karigars as $key => $value) {
                                    ?>
                                    <option value="<?= $value['id']?>"><?= $value['name']?></option>
                                    <?php

                                    }
                                }  
                                ?>
                                </select>
                                <span class="text-danger" id="change_karigar_id_error"></span>
                                <input type="hidden" value="" name="kmm_id" id="change_kmm_id"/>
                                <br/><a href="javascript:void(0);" style="display:none;" class="karigar_history" target="_blank">Show Kariger History</a>
                                </div>
                            </div>
                    <div class="modal-footer">  
                        <button id="barcode_submit" type="button" class="btn btn-success waves-effect waves-light" onclick="change_mnfctrng_karigar()">Submit</button>
                    </div>
                   </form>
                   <input type="hidden" id="mid_wight_range_hidden"/>
               </div> 
            </div><!-- /.modal-content -->
        </div><!-- /.modal-dialog -->
    </div>
    
    <div id="send_to_kundan_karigar" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                    <h4 class="modal-title">Send To Kundan Karigar</h4>
                </div>
                <div class="modal-body">
                <form id="send_to_kundan_karigar" role="form" class="form-horizontal" method="post">
                    <div class="form-group">   
                        <label class="col-sm-4 control-label" for="inputEmail3"> Select Karigar <span class="asterisk">*</span></label>
                        <div class="col-sm-6">
                        <input type="hidden" id="kundan_qnty" name="kundan_qnty" value="">
                        <input type="hidden" id="msqr" name="msqr" value="">
                        <select class="form-control change_kariger" id="kundan_karigar_id" >
                        <option value=''>Select Karigar</option>
                        <?php 
                        if(!empty($all_karigars)){
                            foreach ($all_karigars as $key => $value) {
                            ?>
                            <option value="<?= $value['id']?>"><?= $value['name']?></option>
                            <?php

                            }
                        }  
                        ?>
                        </select>
                        <span class="text-danger" id="change_karigar_id_error"></span>
                    </div>
                    <div class="modal-footer">  
                        <a onclick="send_to_karigar()"><button id="barcode_submit" type="button" class="btn btn-success waves-effect waves-light send_to_kun_karigar">Submit</button></a>
                    </div>
                </form>    
               </div> 
            </div><!-- /.modal-content -->
        </div><!-- /.modal-dialog -->
    </div>
    </div>
    <div id="mnfg_send_to_karigar" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                    <h4 class="modal-title">Send To Karigar</h4>
                </div>
                <div class="modal-body">
                <form role="form" class="form-horizontal" method="post">
                    <div class="form-group">   
                        <label class="col-sm-4 control-label" for="inputEmail3"> Select Karigar <span class="asterisk">*</span></label>
                        <div class="col-sm-6">
                        <input type="hidden" id="karigar_weight" name="kundan_qnty" value="">
                        <input type="hidden" id="karigar_qnt" name="kundan_qnty" value="">
                        <input type="hidden" id="original_qnt" name="kundan_qnty" value="">
                        <input type="hidden" id="qc_id" name="qc_id" value="">
                        <input type="hidden" id="department_id" name="department_id" value="">
                        <input type="hidden" id="weight" name="weight" value="">
                         <input type="hidden" id="original_wt" name="original_wt" value="">
                        
                        <select class="form-control change_kariger" id="mnfg_karigar_id" >
                        <option value=''>Select Karigar</option>
                        <?php 
                        if(!empty($all_karigars)){
                            foreach ($all_karigars as $key => $value) {
                            ?>
                            <option value="<?= $value['id']?>"><?= $value['name']?></option>
                            <?php

                            }
                        }  
                        ?>
                        </select>
                        <span class="text-danger" id="change_karigar_id_error"></span>
                    </div>
                    <div class="modal-footer">  
                        <a onclick="mnfg_send_to_karigar_submit()"><button id="barcode_submit" type="button" class="btn btn-success waves-effect waves-light" onclick="">Submit</button></a>
                    </div>
                </form>    
               </div> 
            </div><!-- /.modal-content -->
        </div><!-- /.modal-dialog -->
    </div>
    </div>


    <div id="receive_sub_category" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                    <h4 class="modal-title">Received Pieces</h4>
                </div>
                <div class="modal-body">
                    <form id="receive_mfg" role="form" class="form-horizontal" method="post">
                    <input type="hidden" id="kmm_id" name="kmm_id" value="">
                            <div class="form-group">
                                <label class="col-sm-4 control-label" for="inputEmail3"> Enter Quantity <span class="asterisk">*</span></label>
                                <div class="col-sm-6">
                               <input type="text" name="kmm_quantity" class="form-control k_quantity" placeholder="Enter Quantity" tabindex="1" autofocus="autofocus" id="barcode_text_box">
                                <span class="text-danger" id="kmm_quantity_error"></span>
                               </div>
                            </div>
                    <div class="modal-footer">  
                        <button id="barcode_submit" type="button" class="btn btn-primary waves-effect waves-light" onclick="receive_sub_category()">Submit</button>
                    </div>
                   </form>
               </div> 
            </div><!-- /.modal-content -->
        </div><!-- /.modal-dialog -->
    </div>
    <div id="product_ready" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                    <h4 class="modal-title">Product Ready</h4>
                </div>
                <div class="modal-body">
                    <form class="form-horizontal">
                        <input type="hidden" id="ms_id" name="ms_id" value="">
                        <div class="form-group">
                            <label class="col-sm-12" for="inputEmail3"> Is product ready? <span class="asterisk">*</span></label>
                        </div>
                        <div class="modal-footer">  
                            <button id="barcode_submit" type="button" class="btn btn-primary waves-effect waves-light" onclick="orderSentToDept(1)">Yes</button>
                            <button id="barcode_submit" type="button" class="btn btn-danger waves-effect waves-light" onclick="orderSentToDept(2)">No</button>
                        </div>
                   </form>
               </div> 
            </div><!-- /.modal-content -->
        </div><!-- /.modal-dialog -->
    </div>

    <div id="qc_check_modal" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                    <h4 class="modal-title">Sending TO Hallmarking</h4>
                </div>
                <div class="modal-body">
                    <form class="form-horizontal">
                        <input type="hidden" id="ms_id" name="ms_id" value="">
                        <div class="form-group">
                            <label class="col-sm-4" for="inputEmail3"> Hallmarking Center <span class="asterisk">*</span></label>
                            <div class="col-sm-8">
                                <select id="hallmarking_center_name" class="form-control">
                                    <option value="">Select Hallmarking Center</option>
                                    <?php if(!empty($hallmarking_centers)){
                                        foreach (@$hallmarking_centers as $h_key => $h_value) { ?>
                                        <option value="<?=$h_value['id']?>"><?=$h_value['name']?></option>
                                    <?php } } ?>
                                </select>
                                 <span class="text-danger" id="hallmarking_center_name_error"></span>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-sm-4" for="inputEmail3">Hallmarking logo <span class="asterisk">*</span></label>
                            <div class="col-sm-8">
                                <input type="text" id="hallmarking_logo" class="form-control">
                                <span class="text-danger" id="hallmarking_logo_error"></span>
                            </div>
                        </div>
                         <div class="form-group">
                            <label class="col-sm-4" for="inputEmail3">Gross weight<span class="asterisk">*</span></label>
                            <div class="col-sm-8">
                                <input type="text" id="gross_weight" class="form-control">
                                <span class="text-danger" id="gross_weight_error"></span>
                            </div>
                        </div>
                        <div class="modal-footer">  
                            <a onclick="check_all_to_hallmarking_after_modal()">
                                <button id="barcode_submit" type="button" class="btn btn-primary waves-effect waves-light" onclick="reload_page_hallmarking()">Yes</button>
                            </a>
                            <button id="barcode_submit" type="button" class="btn btn-danger waves-effect waves-light" data-dismiss="modal">No</button>
                        </div>
                   </form>
               </div> 
            </div><!-- /.modal-content -->
        </div><!-- /.modal-dialog -->
    </div>
        <!-- END wrapper -->

    <div id="img_modal" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>         
                </div>
                <div class="modal-body">
                     <div class="row">
                        <a id="prev_img" class=""><i class="fa fa-angle-left fa-2x" aria-hidden="true"></i></a> 
                        <img id="modal_img_tag" src="" class="col-md-12">
                        <a id="next_img" class=""><i class="fa fa-angle-right fa-2x" aria-hidden="true"></i></a>
                      </div>
                </div> 
            </div>
        </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
    
    <div id="pending_product_for_qc_remark" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                    <h4 class="modal-title">Pending Product For QC Remark</h4>
                </div>
                <form enctype='multipart/form-data' role="form" name="product_form" id="export">                   
                      <input type="hidden" id="type_product_for_qc" name="type_product_for_qc" value="">
                    <div class="modal-body">             
                            <label> Enter Remark <span class="asterisk">*</span></label>
                        <div class="form-group">
                            <div class="">
                         <textarea placeholder="Enter Special Remark" id="remark" class="form-control" name="remark" rows="4">                                 
                         </textarea>
                            <span class="text-danger" id="qc_remark_error"></span>
                           </div>
                        </div>
                    </div>
                    <div class="modal-footer">  
                        <button id="barcode_submit" type="button" class="btn btn-primary waves-effect waves-light" onclick="multiple_qc_send_to_process()">Submit</button>
                    </div>                     
                </form>
            </div> 
        </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->


<!-- Modal -->
  <div class="modal fade" id="change_delivery_date_modal" role="dialog">
    <div class="modal-dialog">
    
      <!-- Modal content-->
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h4 class="modal-title">Change Delivery Date</h4>
        </div>
        <div class="modal-body">
            <div class="form-group">   
 <form enctype='multipart/form-data' role="form" name="order_form" id="change_delivery_date_form">
                        <input type="hidden" id="order_id" name="order_id" value="">
                        <label class="col-sm-4 control-label" for="inputEmail3"> Select Delivery Date <span class="asterisk">*</span></label>
                        <div class="col-sm-6">
                        <input type="text"  name="change_delivery_date" id="change_delivery_date" class="datepicker_deilverydate form-control" >
                        </div>
                        <div class="modal-footer">  
                        <button id="barcode_submit" type="button" class="btn btn-success waves-effect waves-light" onclick="change_delivery_date_process()">Submit</button>
                    </div> 
                    </form>   
            </div>

         
        </div>
        
      </div>
      
    </div>
  </div>
<div id="send_to_transfer_stock" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                    <h4 class="modal-title">Transfer To Stock Department</h4>
                </div>
                <div class="modal-body">
                <form id="transfer_to_sales_location" role="form" class="form-horizontal" method="post">
                    <div class="form-group">   
                        <label class="col-sm-4 control-label" for="inputEmail3"> Select Department <span class="asterisk">*</span></label>
                        <div class="col-sm-6">
                
                        
                        <select class="form-control" id="location_id" name="location_id">
                        <option value=''>Select Department</option>
                        <?php 
                        if(!empty($location)){
                            foreach ($location as $key => $value) {
                            ?>
                            <option value="<?= $value['id']?>"><?= $value['name']?></option>
                            <?php

                            }
                        }  
                        ?>
                        </select>
                        <span class="text-danger" id="change_location_id_error"></span>
                    </div>
                    <div class="modal-footer">  
                        <button id="transfer_stock_submit" type="button" class="btn btn-success waves-effect waves-light send_to_stock_location" onclick="transfer_to_sales_location()">Submit</button>
                    </div>
                </form>    
               </div> 
            </div><!-- /.modal-content -->
        </div><!-- /.modal-dialog -->
    </div>
<div id="upload_image" class="modal fade" role="dialog">
  <div class="modal-dialog">
  
    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title">UPLOAD IMAGES AND VIDEOS</h4>
      </div>
      <div class="modal-body">
      <form name="received_order_images" id="received_order_images" method="POST" enctype="multipart/form-data" >
          <input type="hidden" name="customer_id" id="customer_id">
          <input type ="file" name="image" id="image" ?>         
          <input type ="file" name="video" id="video" ?> 
      
      </div>
      <div class="modal-footer">
       <button type="button" class="btn btn-warning btn-sm" name="upload_image" onclick="ready_product_images()">UPLOAD IMAGES</button>  
     </div>
     </form>
    </div>

  </div>
</div>

    
    <div class="tranBG">
        
    </div>


        <script>
            $("#barcode_text_box").focus();
            $('.repeat_refurb').hide();
            $('#Customer_search').hide();
            var resizefunc = [];
            var url = '<?= ADMIN_PATH?>';
            var increment_date = '<?php echo date('Y-m-d H:i:s',strtotime('+1 days')); ?>';
            var page_title='<?=@$page_title?>';
            var table_size='<?=@$table_size?>';
            var empty_check='<?=@$empty_check?>';
            var custom_check='<?=@$custom_check?>';
            
          
            var karigar_id='<?=@$karigar_id?>';
            var vendor='<?=@$vendor?>';
            var order_id='<?=@$order_id?>';
            var success_url='';
            var dataTable='';
            var page_form_title ='<?=@$page_form_title?>';
            var ids =[];//for selected cars
            var car_type ='Inventory';//for selected cars
            var refb_type='1';
            var bedcrumb_html = '';
            var get = '<?= @$_GET['status'];?>';
            var get_date = '<?= @$_GET['today_date'];?>';
            var from_date = '<?= @$_GET['from'];?>';
            var to_date = '<?= @$_GET['to'];?>';
            var type_wise = '<?= @$_GET['type'];?>';
            var order_id_status = '<?= @$_GET['order_id_status'];?>';
            var department_id = '<?= @$department_id;?>';
            var department = '<?= @$_GET['department'];?>';
            var order_object = [];
            var tr_count = 0;
        
            var upload_images = '<?=UPLOAD_IMAGES?>';
            var order_reminder='<?=@$type?>';
            var order_reminder_filter='<?=@$_GET['filter']?>';
            var product_code = '<?=@$product_code?>';
            /*filter parmeter*/
            if (page_title=="Print Order" || page_title=="Re-print Order") {
                var product_filter_code='<?=@$ex_filter["ex_product_code"]?>';
            }

            <?php if(!empty($mapping_data)){
            ?>
                var tr_count = '<?= count(@$mapping_data);?>';
                var mapping_object = '<?= json_encode(@$mapping_data);?>';
                var order_object = jQuery.parseJSON(mapping_object);
                $('#button_save').show();
            <?php
            }
            ?>
            <?php 
            if(!empty($this->breadcrumbs->show())){
            ?>
            breadcrumbs();
                function breadcrumbs(){
                    bedcrumb_html += '<div class="breadcrumbs"><ul><a class="ExpandMenu" id="ExpandMenu" href="javascript:void(0)"><i class="ti-menu"></i></a>'
                    var json = <?php echo json_encode($this->breadcrumbs->show())?>;
                    //var json = {};
                    var length = Object.keys(json).length;
                    var l = 1;
                    $.each(json,function(key,val){
                        
                        if( l== length){
                            bedcrumb_html += '<li>'+val.page+'</li>' 
                        }else{
                            if(val.page == 'Master' || val.page == 'Product Catalog'|| val.page == 'Corporate Module'){
                                bedcrumb_html += '<li><a>'+val.page+'</a></li>';
                            }else{

                                bedcrumb_html += '<li><a href="'+val.href+'">'+val.page+'</a></li>'
                            }
                        }
                        l++;
                    })
                    bedcrumb_html += '</ul></div>';
                }
            <?php
            }
            ?>
            $( ".bedcrumb_header" ).html(bedcrumb_html);
            if(page_title=='Edit Cars'){
               
             car_type = "<?=(@$car['car_type'] == 1) ? 'Inventory' : 'park_n_sell' ;?>";

             refb_type = "<?=(@$car['car_status'] == 2) ? '1' : '0' ;?>";
            

              }

            $(document).ready(function(){
                    var check=true;
                    $("#ExpandMenu").on("click", function(){
                        if(check==true){
                            $(".left.side-menu").animate({width: '45px'});
                            $("div.content-page").animate({marginLeft: '45px'});
                            $(".list-unstyled").css('display', 'none');
                            //$('.topbar .topbar-left').animate({width:'75px', height:'75px', marginLeft: '-30px'});
                            $('.topbar .topbar-left').addClass('small_logo');
                            $('.topbar .topbar-left').removeClass('large_logo');
                            $('.topbar .topbar-left a img.logo').animate({height:'40px', marginTop: '10px'});
                            $("#sidebar-menu ul li a.waves-effect").animate({left:'-250px'}).hide(); 
                            $("ul li.has_sub").children("ul.list-unstyled").addClass("hoverSubmenu");
                            $("ul li.has_sub").addClass("hoverSubmenuPatents");
                            $("body").addClass("fullscreen");
                            $(".slimScrollDiv").css("overflow", "visible");
                            $(".left.side-menu").css("overflow", "visible");
                            $(".sidebar-inner.slimscrollleft").css("overflow", "visible");                      
                            /*-------Sub Menu -------*/
                            check=false;
                        }else{
                            check=true;                            
                            $(".left.side-menu").animate({width: '250px'});
                            $("div.content-page").animate({marginLeft: '250px'});                            
                            //$('.topbar .topbar-left').animate({width:'215px', height:'75px', marginLeft: '0px'});
                            $('.topbar .topbar-left').addClass('large_logo');
                            $('.topbar .topbar-left').removeClass('small_logo');
                            $('.topbar .topbar-left a img.logo').animate({ marginTop: '0px'});
                            $("#sidebar-menu ul li a.waves-effect").animate({left:'0px'}).fadeIn(30);
                            $("ul li.has_sub").children("ul.list-unstyled").removeClass("hoverSubmenu");
                            $("ul li.has_sub").removeClass("hoverSubmenuPatents");
                            $(".slimScrollDiv").css("overflow", "hidden");
                            $(".left.side-menu").css("overflow", "hidden");
                            $(".sidebar-inner.slimscrollleft").css("overflow", "hidden");
                            $("body").removeClass("fullscreen");
                            };
                            localStorage.setItem("menuStts", check);
                    });
                    
                    if(localStorage.getItem("menuStts")){
                        var getStts = localStorage.getItem("menuStts");
                        if (getStts=="false") {
                            $(".left.side-menu").animate({width: '45px'});
                            $("div.content-page").animate({marginLeft: '45px'});
                            $(".list-unstyled").css('display', 'none');
                           // $('.topbar .topbar-left').animate({width:'75px', height:'75px', marginLeft: '-30px'});
                            $('.topbar .topbar-left').addClass('small_logo');
                            $('.topbar .topbar-left').removeClass('large_logo');
                            $('.topbar .topbar-left a img.logo').animate({height:'40px', marginTop: '10px'});
                            $("#sidebar-menu ul li a.waves-effect").animate({left:'-250px'}).hide(); 
                            $("ul li.has_sub").children("ul.list-unstyled").addClass("hoverSubmenu");
                            $("ul li.has_sub").addClass("hoverSubmenuPatents");
                            $("body").addClass("fullscreen");
                            $(".slimScrollDiv").css("overflow", "visible");
                            $(".left.side-menu").css("overflow", "visible");
                            $(".sidebar-inner.slimscrollleft").css("overflow", "visible");
                            check=false; 
                        }
                    }
            });
            

            $(document).ready(function(){
                var sliMenustts = "close"
                $("#openslidMenu").on("click", function(){
                    if(sliMenustts=="close"){
                        sliMenustts="open";
                        $(".tranBG").show();
                    $(".side-menu").animate({marginLeft: '0'});
                    }else{
                        $(".side-menu").animate({marginLeft: '-300px'});
                        sliMenustts="close";
                        $(".tranBG").hide();
                    }
                    if(sliMenustts=="open"){
                        $(".tranBG").on('click', function(){
                            $(this).hide();
                            $(".side-menu").animate({marginLeft: '-300px'});
                            sliMenustts="close";
                        });
                    }
                
                });
            });



            $(document).ready(function(){
            var check=true;
               $("button").click(function(){
               if(check==true){
                   $("p:first").addClass("intro");
                   check=false;
                   }else{
                   check=true;
                   $("p:first").removeClass("intro");
                   }
               });
            });


            (function ($) {
              $.fn.formNavigation = function () {
                $(this).each(function () {
                  $(this).find('input').on('keyup', function(e) {
                    switch (e.which) {
                      case 39:
                        $(this).closest('td').next().find('input').focus(); break;
                      case 37:
                        $(this).closest('td').prev().find('input').focus(); break;
                      case 40:
                        $(this).closest('tr').next().children().eq($(this).closest('td').index()).find('input').focus(); break;
                      case 38:
                        $(this).closest('tr').prev().children().eq($(this).closest('td').index()).find('input').focus(); break;
                    }
                  });
                });
              };
            })(jQuery);

</script>

        <!-- jQuery  -->
        <script src="<?=ADMIN_JS_PATH?>jquery.min.js"></script>
        <script src="<?=ADMIN_JS_PATH?>jquery-ui-1.10.4.min.js"></script>
        <script src="<?=ADMIN_JS_PATH?>bootstrap.min.js"></script>
        <script src="<?=ADMIN_JS_PATH?>detect.js"></script>
        <script src="<?=ADMIN_JS_PATH?>fastclick.js"></script>
        <script src="<?=ADMIN_JS_PATH?>jquery.slimscroll.js"></script>
        <script src="<?=ADMIN_JS_PATH?>jquery.blockUI.js"></script>
        <script src="<?=ADMIN_JS_PATH?>waves.js"></script>
        <script src="<?=ADMIN_JS_PATH?>jquery.nicescroll.js"></script>
        <script src="<?=ADMIN_JS_PATH?>jquery.scrollTo.min.js"></script>

        <script src="<?=ADMIN_JS_PATH?>moment-with-locales.js"></script>
        
        <!-- KNOB JS -->
        <!--[if IE]>
        <script type="text/javascript" src="<?=ADMIN_PLUGINS_PATH?>jquery-knob/excanvas.js"></script>
        <![endif]-->
        <script src="<?=ADMIN_PLUGINS_PATH?>jquery-knob/jquery.knob.js"></script>

        <?php if(isset($page_title) && $page_title=="Dashboard") { ?>
        <!-- Flot chart js -->
        <script src="<?=ADMIN_PLUGINS_PATH?>flot-chart/jquery.flot.js"></script>
        <script src="<?=ADMIN_PLUGINS_PATH?>flot-chart/jquery.flot.time.js"></script>
        <script src="<?=ADMIN_PLUGINS_PATH?>flot-chart/jquery.flot.tooltip.min.js"></script>
        <script src="<?=ADMIN_PLUGINS_PATH?>flot-chart/jquery.flot.resize.js"></script>
        <script src="<?=ADMIN_PLUGINS_PATH?>flot-chart/jquery.flot.pie.js"></script>
        <script src="<?=ADMIN_PLUGINS_PATH?>flot-chart/jquery.flot.selection.js"></script>
        <script src="<?=ADMIN_PLUGINS_PATH?>flot-chart/jquery.flot.stack.js"></script>
        <script src="<?=ADMIN_PLUGINS_PATH?>flot-chart/jquery.flot.crosshair.js"></script>
        <!-- flot init -->
        <script src="<?=ADMIN_PAGES_PATH?>jquery.flot.init.js"></script>
        <!-- Circliful -->
        <script src="<?=ADMIN_PLUGINS_PATH?>jquery-circliful/js/jquery.circliful.min.js"></script>
        <!--Morris Chart-->
        <script src="<?=ADMIN_PLUGINS_PATH?>morris/morris.min.js"></script>
        <script src="<?=ADMIN_PLUGINS_PATH?>raphael/raphael-min.js"></script>
        <script src="<?=ADMIN_PAGES_PATH?>jquery.morris.init.js"></script>
        <!-- Jquery-Ui -->
        <script src="<?=ADMIN_PLUGINS_PATH?>jquery-ui/jquery-ui.min.js"></script>
        <!-- BEGIN PAGE SCRIPTS -->
        <script src="<?=ADMIN_PLUGINS_PATH?>bootstrap-daterangepicker/daterangepicker.js"></script>

         
         
        <!-- datepicker -->
        <script src="<?=ADMIN_PLUGINS_PATH?>moment/moment.js"></script>
        <script src='<?=ADMIN_PLUGINS_PATH?>fullcalendar/dist/fullcalendar.min.js'></script>
        <script src="<?=ADMIN_PAGES_PATH?>jquery.fullcalendar.js"></script>
        <?php } ?>
            
       
        <!-- Graphical chart js -->
  <!--   <?php if (isset($order_wise_chart)) { ?>
        <script src="<?=ADMIN_PLUGINS_PATH?>chart.js/chart.min.js"></script>
        <script src="<?=ADMIN_PLUGINS_PATH?>chartist/dist/chartist.min.js"></script>
        <script src="<?=ADMIN_PLUGINS_PATH?>chartist-plugin-tooltip-master/dist/chartist-plugin-tooltip.min.js"></script>
        <script src="<?=ADMIN_PLUGINS_PATH?>chart.js/chartjs.init.js"></script>
        <script src="<?=ADMIN_PATH?>/assets/js/dashboard_chart.js"></script>
       <?php } ?>
 -->
        <!-- Graphical chart js -->
        <?php if (isset($order_wise_chart) || isset($design_wise_chart)) { ?>
        <script src="<?=ADMIN_PLUGINS_PATH?>chart.js/Chart.js"></script>
        <script src="<?=ADMIN_PLUGINS_PATH?>chartist/dist/chartist.min.js"></script>
        <script src="<?=ADMIN_PLUGINS_PATH?>chartist/dist/chartist-plugin-tooltip.min.js"></script>
        <script src="<?=ADMIN_PAGES_PATH?>jquery.chartist.init.js"></script>
     
        <?php } ?>
    
               

        <!-- Datatables-->
        <script src="<?=ADMIN_PLUGINS_PATH?>datatables/jquery.dataTables.min.js"></script>
        <script src="<?=ADMIN_PLUGINS_PATH?>datatables/dataTables.bootstrap.js"></script>
        <script src="<?=ADMIN_PLUGINS_PATH?>datatables/dataTables.buttons.min.js"></script>
        <script src="<?=ADMIN_PLUGINS_PATH?>datatables/buttons.bootstrap.min.js"></script>
        <script src="<?=ADMIN_PLUGINS_PATH?>datatables/jszip.min.js"></script>
        <script src="<?=ADMIN_PLUGINS_PATH?>datatables/pdfmake.min.js"></script>
        <script src="<?=ADMIN_PLUGINS_PATH?>datatables/vfs_fonts.js"></script>
        <script src="<?=ADMIN_PLUGINS_PATH?>datatables/buttons.html5.min.js"></script>
        <script src="<?=ADMIN_PLUGINS_PATH?>datatables/buttons.print.min.js"></script>
        <script src="<?=ADMIN_PLUGINS_PATH?>datatables/dataTables.fixedHeader.min.js"></script>
        <script src="<?=ADMIN_PLUGINS_PATH?>datatables/dataTables.keyTable.min.js"></script>
        <script src="<?=ADMIN_PLUGINS_PATH?>datatables/dataTables.responsive.min.js"></script>
        <script src="<?=ADMIN_PLUGINS_PATH?>datatables/responsive.bootstrap.min.js"></script>
        <script src="<?=ADMIN_PLUGINS_PATH?>datatables/dataTables.scroller.min.js"></script>
        <!-- mcsrollbar js -->
        <script src="<?=ADMIN_JS_PATH?>jquery.mCustomScrollbar.concat.min.js"></script>
        <script src="<?=ADMIN_JS_PATH?>jquery.multiselect.js"></script>
        
        <!-- datepicker -->
        <script src="<?=ADMIN_PLUGINS_PATH?>bootstrap-datepicker/dist/js/bootstrap-datepicker.min.js"></script>
        <script src="<?=ADMIN_PLUGINS_PATH?>timepicker/bootstrap-timepicker.min.js"></script>
        <script src="<?=ADMIN_PLUGINS_PATH?>bootstrap-daterangepicker/daterangepicker.js"></script>
        <script src="<?=ADMIN_PLUGINS_PATH?>datetimepicker/build/jquery.datetimepicker.full.js"></script>
       
        <!-- select2 js -->
        <script src="<?=ADMIN_PLUGINS_PATH?>select2/dist/js/select2.min.js" type="text/javascript"></script>

        <!-- Datatable init js -->
        <script src="<?=ADMIN_PAGES_PATH?>datatables.init.js"></script>
        
        <!-- chosen -->
        <script src="<?=ADMIN_PLUGINS_PATH?>chosen_v1.7.0/chosen.jquery.js" type="text/javascript"></script>
        <script src="<?=ADMIN_PLUGINS_PATH?>chosen_v1.7.0/docsupport/init.js" type="text/javascript"></script>
        <!-- Toastr js -->
        <script src="<?=ADMIN_PLUGINS_PATH?>toastr/toastr.min.js"></script>
        <!-- App js -->
        <!-- Modal-Effect -->
        <script src="<?=ADMIN_PLUGINS_PATH?>custombox/dist/custombox.min.js"></script>
        <script src="<?=ADMIN_PLUGINS_PATH?>custombox/dist/legacy.min.js"></script>
        
	<!-- Common Function Js-->
        <script src="<?=ADMIN_JS_PATH?>common.js?<?=time()?>"></script>
        <script src="<?=ADMIN_JS_PATH?>forms.js?<?=time()?>"></script>
        <script src="<?=ADMIN_JS_PATH?>formTable.js?<?=time()?>"></script>
        <script src="<?=ADMIN_JS_PATH?>jquery.core.js"></script>
        <script src="<?=ADMIN_JS_PATH?>jquery.app.js"></script>
         <script src="<?=ADMIN_JS_PATH?>custom_filterTable.js"></script>
         <script src="<?=ADMIN_JS_PATH?>aos.js"></script>



        <script type="text/javascript">
            $(window).load(function(){
            if ($(window).width() < 768) {
               setTimeout(function(){
                    $('#datatable').wrapAll('<div>');
                    $("#datatable").parent().addClass("table-responsive");
                }, 500);
            }
            
        })
            
        </script>



   <!--  <script src="<?=ADMIN_PLUGINS_PATH?>fancybox/jquery.fancybox.pack.js?v=2.1.5"></script>
    <script src="<?=ADMIN_PLUGINS_PATH?>fancybox/jquery.fancybox.css?v=2.1.5" media="screen" /></script> -->
<!--     <script type="text/javascript">
        $(document).ready(function() {          
            // Remove padding, set opening and closing animations, close if clicked and disable overlay
            $(".fancybox-effects-d").fancybox({
                padding: 0,

                openEffect : 'elastic',
                openSpeed  : 150,

                closeEffect : 'elastic',
                closeSpeed  : 150,

                closeClick : true,

                helpers : {
                    overlay : null
                }
            });
            // Disable opening and closing animations, change title type
            $(".fancybox-effects-b").fancybox({
                openEffect  : 'none',
                closeEffect : 'none',

                helpers : {
                    title : {
                        type : 'over'
                    }
                }
            });         

        });
    </script> -->
    <script type="text/javascript">
          
      AOS.init({
        easing: 'ease-in-out-sine'
      });    
    </script>

    

<script type="text/javascript">
    (function ($) {
        $.fn.formNavigation = function () {
            $(this).each(function () {
                $(this).find('input').on('keyup', function(e) {
                    switch (e.which) {
                        // case 39:
                        //     $(this).closest('td').next().find('input').focus(); break;
                        // case 37:
                        //     $(this).closest('td').prev().find('input').focus(); break;
                        case 40:
                            $(this).closest('tr').next().children().eq($(this).closest('td').index()).find('input').focus(); break;
                        case 38:
                            $(this).closest('tr').prev().children().eq($(this).closest('td').index()).find('input').focus(); break;
                    }
                });
            });
        };
    })(jQuery);    
</script>
</body>
