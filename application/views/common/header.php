<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta name="description" content="">
        <meta name="author" content="">

        <!-- <link rel="shortcut icon" href="assets/images/favicon.ico"> -->

        <title>Faber Lounge</title>
        <?php if(isset($page_title) && $page_title=="Dashboard") { ?>
        <!-- Circlifull chart css -->
        <link href="<?=ADMIN_PLUGINS_PATH?>jquery-circliful/css/jquery.circliful.css" rel="stylesheet" type="text/css"/>
        <!--Morris Chart CSS -->
        <link rel="stylesheet" href="<?=ADMIN_PLUGINS_PATH?>morris/morris.css">
        <!--calendar css-->
        <link href="<?=ADMIN_PLUGINS_PATH?>fullcalendar/dist/fullcalendar.css" rel="stylesheet" />
        <?php } ?>
               <!--Chartist Chart CSS -->
       <link rel="stylesheet" href="<?=ADMIN_PLUGINS_PATH?>chartist/dist/chartist.min.css">
        

        <!-- mscrollbar css -->
        <link href="<?=ADMIN_CSS_PATH?>jquery.mCustomScrollbar.min.css" rel="stylesheet" type="text/css" />
        
        <!-- App css -->
        <link href="<?=ADMIN_CSS_PATH?>bootstrap.min.css" rel="stylesheet" type="text/css" />
        <link href="<?=ADMIN_CSS_PATH?>core.css" rel="stylesheet" type="text/css" />
        <link href="<?=ADMIN_CSS_PATH?>customtable.css" rel="stylesheet" type="text/css" />
        <link href="<?=ADMIN_CSS_PATH?>components.css" rel="stylesheet" type="text/css" />
        <link href="<?=ADMIN_CSS_PATH?>icons.css" rel="stylesheet" type="text/css" />
        <link href="<?=ADMIN_CSS_PATH?>pages.css" rel="stylesheet" type="text/css" />
        <link href="<?=ADMIN_CSS_PATH?>menu.css" rel="stylesheet" type="text/css" />
        <link href="<?=ADMIN_CSS_PATH?>jquery-ui.css" rel="stylesheet" type="text/css" />
        <link href="<?=ADMIN_CSS_PATH?>style-ui.css" rel="stylesheet" type="text/css" />
        <!-- <link href="<?=ADMIN_CSS_PATH?>aos.css" rel="stylesheet" type="text/css" /> -->
        <link href="<?=ADMIN_CSS_PATH?>font-face.css" rel="stylesheet" type="text/css" />
        <link href="<?=ADMIN_CSS_PATH?>jquery.multiselect.css" rel="stylesheet" type="text/css" />
       <!--  <link href="<?=ADMIN_CSS_PATH?>timepicker.css" rel="stylesheet"> -->
        <!-- DataTables -->
        <link href="<?=ADMIN_PLUGINS_PATH?>datatables/jquery.dataTables.min.css" rel="stylesheet" type="text/css" />
        <link href="<?=ADMIN_PLUGINS_PATH?>datatables/buttons.bootstrap.min.css" rel="stylesheet" type="text/css" />
        <link href="<?=ADMIN_PLUGINS_PATH?>datatables/fixedHeader.bootstrap.min.css" rel="stylesheet" type="text/css" />
        <link href="<?=ADMIN_PLUGINS_PATH?>datatables/responsive.bootstrap.min.css" rel="stylesheet" type="text/css" />
        <link href="<?=ADMIN_PLUGINS_PATH?>datatables/scroller.bootstrap.min.css" rel="stylesheet" type="text/css" />

        <!-- datepicker -->
        <link href="<?=ADMIN_PLUGINS_PATH?>bootstrap-datepicker/dist/css/bootstrap-datepicker.min.css" rel="stylesheet">
        <link href="<?=ADMIN_PLUGINS_PATH?>bootstrap-daterangepicker/daterangepicker.css" rel="stylesheet">
        <link href="<?=ADMIN_PLUGINS_PATH?>timepicker/bootstrap-timepicker.min.css" rel="stylesheet">
        
        <link href="<?=ADMIN_PLUGINS_PATH?>datetimepicker/jquery.datetimepicker.css" rel="stylesheet" media="screen">
        <!-- datepicker -->

        <!-- select2 -->
        <link href="<?=ADMIN_PLUGINS_PATH?>select2/dist/css/select2.css" rel="stylesheet" type="text/css">
        <link href="<?=ADMIN_PLUGINS_PATH?>select2/dist/css/select2-bootstrap.css" rel="stylesheet" type="text/css">
        <!-- select2 -->

        <!--- Chosen-->
        <link href="<?=ADMIN_PLUGINS_PATH?>chosen_v1.7.0/chosen.css" rel="stylesheet" type="text/css">

        <!-- chosen -->

        <link href="<?=ADMIN_CSS_PATH?>style.css?<?=time()?>" rel="stylesheet" type="text/css" />
        <link href="<?=ADMIN_CSS_PATH?>responsive.css?<?=time()?>" rel="stylesheet" type="text/css" />
         <!-- Table css -->
       
       <?php if(isset($page_title) && $page_title!="ARF") { ?>
        <link href="<?=ADMIN_PLUGINS_PATH?>RWD-Table-Patterns/dist/css/rwd-table.min.css" rel="stylesheet" type="text/css" media="screen">
       <?php } ?>

        <!-- Notification css (Toastr) -->
        <link href="<?=ADMIN_PLUGINS_PATH?>toastr/toastr.min.css" rel="stylesheet" type="text/css" />
       <link rel="stylesheet" href="//cdn.materialdesignicons.com/2.1.99/css/materialdesignicons.min.css">

        <script src="<?=ADMIN_JS_PATH?>jquery.min.js"></script>
        <script src="<?=ADMIN_JS_PATH?>modernizr.min.js"></script>

    </head>
