 <body class="fixed-left">

 	<!-- preloader start-->
 	<div class="preloader">
		<div class="loading">
		  <div class="loading-bar"></div>
		  <div class="loading-bar"></div>
		  <div class="loading-bar"></div>
		  <div class="loading-bar"></div>
		</div>
	</div>
	<!-- preloader end-->

	<!-- preloader start-->
 	<div class="ajaxLoader">
		<div class="loading">
		  <div class="loading-bar"></div>
		  <div class="loading-bar"></div>
		  <div class="loading-bar"></div>
		  <div class="loading-bar"></div>
		</div>
	</div>
	<!-- preloader end-->

	<!-- Begin page -->

<div id="wrapper">

<!-- Top Bar Start -->
	<div class="topbar">			
		<!-- Button mobile view to collapse sidebar menu -->
		<div class="navbar navbar-default navbar-fixed-top ams-navbar-dashboard" role="navigation">
			<div class="container">			  	
				<!-- LOGO -->
				<div class="topbar-left navbar-header ams-dashboard-topbar-left logoBox">
					<!-- <a href="<?=ADMIN_PATH?>"><img src="<?= ADMIN_PATH?>assets/images/shilpilogo.png" alt="logo" class="logo desktopLogo"></a>	 -->
 <a href="#"><img src="<?= ADMIN_IMAGES_PATH ?>logo1.png" alt="logo" class="small_logo"></a>  
				 <a href="#"><img src="<?= ADMIN_IMAGES_PATH ?>FL.png" alt="logo" class="logo desktopLogo"></a>
				<!--  <a href="<?=ADMIN_PATH?>"><img src="<?= ADMIN_IMAGES_PATH ?>logo1.png" alt="logo" class="small_logo"></a>  
				 <a href="<?=ADMIN_PATH?>"><img src="<?= ADMIN_IMAGES_PATH ?>FL.png" alt="logo" class="logo desktopLogo"></a> -->
					<!-- <a class="navbar-brand" href="<?=ADMIN_PATH?>">
								            <b><img src="<?= ADMIN_IMAGES_PATH ?>logo1.png" alt="user" class="dark-logo"></b>
								            <span class="span_image_text">
								                <img src="<?= ADMIN_IMAGES_PATH ?>logo2.png" alt="user" class="dark-logo">
								            </span>
								         </a> -->

					<h4 class="page-title" style="font-size: 25px; color:#139fda"></h4>
				</div>

							<button class="button-menu-mobile open-left">
							<i class="ti-menu" id="openslidMenu"></i>
							</button>
				<div class="bedcrumb_header"></div>	

			<!--Right Lougout button -->
				<ul class=" nav navbar-nav navbar-right logOutDiv">
					<li>
						<!-- Start Notification Bar-->
						<a href="javascript:void(0);" class="right-bar-toggle notification_bell_wrap">
						<i class="zmdi zmdi-notifications-none"></i>
						<!-- <span  class="badge badge-primary btn-info notif_count"></span> -->
						<span id="not_count"><span id="notification_count">0</span></span></a>
					</li>	
					<li>
						<a class="ams-header-logout-link" href="javascript:void(0);">
						<i class="zmdi zmdi-settings"></i></a>
					</li>					 			
					<!-- <li class="dropdown">
						<a href="#" class="dropdown-toggle" data-toggle="dropdown"><span class="profile-text">Hello: <?=@$this->session->userdata('name'); ?></span> <b class="caret"></b></a> 
						<ul class="dropdown-menu profile-log navbar-right">
							<li><a class="ams-header-logout-link" href="<?=ADMIN_PATH?>auth/logout">
	            				<i class="zmdi zmdi-sign-in"></i> Log out</a>
	            			</li>
	            			<li><div class="divider"></div>
	            				<a href="javascript:void(0)">USER ID: <?=@$this->session->userdata('userid')?></a>
	    					</li>
						</ul>
					</li> -->
					<div class="dropdown">
					    <button class="btn btn-default dropdown-toggle" type="button" data-toggle="dropdown">
					    	<span>Hello: <?=@$this->session->userdata('name'); ?></span>
					    <span class="caret"></span></button>
					    <ul class="dropdown-menu">
					      <li><a href="<?=ADMIN_PATH?>auth/logout"><i class="zmdi zmdi-sign-in"></i>  Log out</a></li>					      
					      	<li>
					      		<div class="divider"></div>
	            				<a href="javascript:void(0)">USER ID : <?=@$this->session->userdata('user_id')?></a>
	    					</li>
					    </ul>
					</div>						
				</ul> 
			<!--Right Lougout button -->

				<!-- Page title -->														
			</div>				
			<!-- end container -->
		</div>
		<!-- end navbar -->
	</div>
<!-- Top Bar End -->

	<!-- ========== Left Sidebar Start ========== -->
	<div id="sidbarLeft" class="left side-menu <?php if($page_title=='Dashboard'){echo 'dashboard-side-menu';}?>">
		<div class="sidebar-inner slimscrollleft">
			<div id="sidebar-menu">
				<ul>
					<?php
					$department_id = $this->session->userdata('user_department');
					  //echo"<pre>"; print_r($department_id); echo"</pre>";die;
/*					echo "<pre>";
					print_r($navigation);
					echo"</pre>";die;*/
						if(sizeof($navigation)>0){
							//print_r($navigation); die();
							foreach($navigation as $key => $value) {
								$active_class="";
								$has_sub="";
								$page = ADMIN_PATH.$value[0]['name'];
								$page_name =$this->uri->segment(1);
								$user_modals = array_map(function ($ar) {return $ar['name'];}, $value);
								//print_r($value);
								//$this->load->library('view');
								//print_r($page_name);
								$ci =& get_instance();								
								$masterid = $ci->view->loadActiveNavigation($page_name);	
								// print_r($masterid);	
								//if(sizeof($value)>0)						
								if(sizeof($value)>0){
									$has_sub="has_sub";
									$page = "javascript:void(0);";
								}
								if(in_array($page_name, $user_modals) || $masterid==$value[0]['master_id']){
						 			$active_class ="subdrop";
								}
							
					?>
						<li class="menu_border <?=$has_sub?>">
							
							<span class="iconBox"><i class="<?=@$value[0]['icon']?>"></i></span>
							
							<a href="<?=@$page?>" class="waves-effect <?=$active_class?>">
								<span><?=@$key?></span>
								<?php if($has_sub!=""){ ?> 
									<span class="menu-arrow"></span>
								<?php } ?>
							</a>
							<?php if($has_sub!=""){ ?>  
							 	<ul class="list-unstyled" <?php if($active_class!=""){ ?>style="display:block;" <?php } ?>>
							 		<?php  foreach($value as $sub_key => $sub_value) { //print_r($value[0]['master_id'].'=='.$sub_value['master_id']); //echo '<pre> value'; print_r($value); echo '<pre> sub_value'; print_r($sub_value); /*echo '<pre> sub_value'; print_r($sub_value); echo '<pre> value'; print_r($value); print_r($value[0]['master_id'] .'=='. $sub_value['master_id']); echo $page_name .'=='. $sub_value['name']; die;*/
							 		//print_r($sub_value['name']);
							 		//print_r(count($_SESSION['user_department']));?>
							 	<li class="<?php echo ($page_name == $sub_value['name'] /*|| $value[0]['master_id'] == $sub_value['master_id']*/ ) ? 'active' : ''?>">
							<?php if($key=="MFG Module" && isset($_SESSION['user_department']) && count($_SESSION['user_department']) != 0){		
								foreach ($_SESSION['user_department'] as $key => $value) {
									 $department_name = $this->Department_model->find($value);
									 		//print_r($department_name);	 		
							 		?>
	                            	
						<!-- 			<li class="<?php echo ($page_name == $sub_value['name'] || isset($_SESSION['department_id']) && $_SESSION['department_id']==$value ) ? 'active' : ''?>"> -->
	                            	<a href="<?= ADMIN_PATH.$sub_value['name'].'/'.$value?>"> <?php echo $department_name ['name'].' Dashboard'; ?></a>
									<!-- </li> -->
									<?php } }else{?>
					
	                            	<a href="<?= ADMIN_PATH.$sub_value['name']?>"><?=$sub_value['display_name']?> </a>
									</li>
									<?php } } ?>
                        		</ul>
							<?php } ?>
							<!-- <a href="<?=ADMIN_PATH.$value[0]['name']?>" class="waves-effect <?=($this->uri->segment(1)==$value[0]['name'])? 'active':''?>">	<span><?=@$value[0]['display_name']?></span>
							</a> -->
						</li>
					<?php	
							} //die;
						}
						if(isset($_SESSION['user_role']) && $_SESSION['user_role']=='Admin'){
					?>
						<li class="menu_border <?=$has_sub?>">							   
						   		<span class="iconBox"><i class="mdi mdi-calculator"></i></span>
						   <a href="<?=ADMIN_PATH.'manage_user'?>" class="waves-effect <?=($this->uri->segment(1)=='manage_user')? 'active':''?>">
						   		<span>Manage Users</span>
						   </a>
						</li>
					<?php }?>
				</ul>
				<div class="clearfix"></div>
			</div>
			<!-- Sidebar -->
			<div class="clearfix"></div>
		</div>
	</div>
	<!-- ========== Left Sidebar Start ========== -->

	<div class="side-bar right-bar">
		<a href="javascript:void(0);" class="right-bar-toggle" id="notification_close">
		<i class="zmdi zmdi-close-circle-o"></i></a>
		<h4 class="">Notifications</h4>
		<div class="notification-list nicescroll">
			<ul class="list-group list-no-border user-list" id="notificationList">
				<div id="updated_notif"></div>
			</ul>
		</div>
	</div>

</div>
	
<script type="text/javascript">
$(document).ready(function(){
    $("select").change(function(){
        $(this).find("option:selected").each(function(){
            if($(this).attr("value")=="red"){
                $(".box").not(".red").hide();
                $(".red").show();
            }
            else if($(this).attr("value")=="green"){
                $(".box").not(".green").hide();
                $(".green").show();
            }
            else if($(this).attr("value")=="blue"){
                $(".box").not(".blue").hide();
                $(".blue").show();
            }
            else if($(this).attr("value")=="black"){
                $(".box").not(".black").hide();
                $(".black").show();
            }
            else{
                $(".box").hide();
            }
        });
    }).change();
    
 
});
</script>

