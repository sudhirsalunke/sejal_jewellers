<div class="content-page">
<div class="content">
   <div class="container">
      <div class="row">
         <div class="col-sm-6 card-box-wrap">
            <div class="panel panel-default">
              <div class="panel-heading frmHeading"><h4 class="inlineBlock"><span>Edit Carat</span></h4></div>
              <div class="panel-body">
                <div class="col-lg-12">
                  <form name="category" id="Carat" role="form" class="form-horizontal" method="post">
                   <input type="hidden" name="Carat[encrypted_id]" value="<?= $carat['encrypted_id'] ?>">
                    <div class="form-group">
                      <label class="col-sm-4 control-label" for="inputEmail3"> Enter Carat Name <span class="asterisk">*</span></label>
                      <div class="col-sm-8">
                         <input type="text" placeholder="Enter Carat Name" id="name" class="form-control" name="Carat[name]" value="<?= $carat['name'] ?>">
                         <span class="text-danger" id="name_error"></span>
                      </div>
                    </div>
                    <div class="btnSection">
                         
                         <button class="btn btn-default waves-effect waves-light m-l-5 btn-md" onclick="window.location='<?= ADMIN_PATH?>Carat'" type="reset">
                           back
                         </button>
                          <button class="btn btn-success pull-right waves-effect waves-light btn-md" name="commit" type="button" onclick="update_carat(); ">
                         SAVE
                         </button>
                    </div>
                  </form>
                </div>
              </div>                
               <div class="clearfix"></div>
            </div>
         </div>
      </div>
   </div>
</div>