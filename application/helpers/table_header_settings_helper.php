<?php
	function getTableHeaderSettings($heading){

		//print_r($_GET['status']);
$tableHeadParameters = array(									
						'Engaged_karigar_list'=> array(
														array(
															'column_name' =>'Karigar Name',
															'table_column_name'  => 'k.name',
															'table_column'  => 'k.name',
															'id'  => 'k_name',
															'is_filter'   => true,
															
															'label' =>'text',
														),
														/*array(
															'column_name' =>'Date',
															'table_column_name'  => 'DATE_FORMAT(karigar_engaged_date,"%d-%m-%Y")',
															'table_column'  => 'karigar_engaged_date',
															'id'  => 'karigar_engaged_date',
															'is_filter'   => true,
															
															'label' =>'date',
														),*/										
														array(
															'column_name' =>'No. of Design',
															//'table_column'  => '',
															'table_column_name'  => 'count(DISTINCT cp.sort_Item_number)',
															'table_column'  => 'cp.sort_Item_number',
															'id'  => 'cp_sort_Item_number',
															'is_filter'   => true,
															//'source'=>'Engaged_karigar_list',
															'label' =>'text',
															
														),

														array(	
															'column_name' =>'Total Pcs',
															'table_column'  => '',
															'table_column_name'  => 'sum(ppl.quantity)',
															'table_column'  => 'ppl.quantity',
															'id'  => 'total_pcs',
															'is_filter'   => true,
															'source'=>'Engaged_karigar_list',
															'label' =>'text',
														),
														array(
															'column_name' =>'Received Pcs',
															'table_column_name'  => 'sum(rp.quantity)',
															'table_column'  => 'rp.quantity',
															'id'  => 'received_pcs',
															'is_filter'   => true,
															'label' =>'text',
														),
														array(	
															'column_name' =>'Total Wt',
															'table_column_name'  => '',
															'table_column'  => 'total_wt',
															'id'  => 'total_wt',
															
															'label' =>'text',
														),
														array(
															'column_name' =>'Received Wt',
															'table_column_name'  => '',
															'table_column'  => 'received_wt',
															'id'  => 'received_wt',
															
															'label' =>'text',
														),
														array(
															'column_name' =>'Balance Wt',
															'table_column_name'  => '',
															'table_column'  => 'balance_wt',
															'id'  => 'balance_wt',
															
															'label' =>'text',
														),
														array(
															'column_name' =>'',	
															'table_column_name'=>'',
															'table_column'  => '',								
														)

							),
						'Due_date_tracking_list'=> array(
														array(
															'column_name' =>'Karigar Name',
															'table_column_name'  => 'k.name',
															'table_column'  => 'k.name',
															'id'  => 'k_name',
															'is_filter'   => true,
															
															'label' =>'text',
														),
														array(
															'column_name' =>'Date',
															'table_column_name'  => 'DATE_FORMAT(karigar_engaged_date,"%d-%m-%Y")',
															'table_column'  => 'karigar_engaged_date',
															'id'  => 'karigar_engaged_date',
															'is_filter'   => true,
															
															'label' =>'date',
														),								
																						
														array(
															'column_name' =>'Due Date',
															'table_column_name'  => 'DATE_FORMAT(karigar_delivery_date,"%d-%m-%Y")',
															'table_column'  => '',
															'id'  => 'karigar_delivery_date',
															'is_filter'   => true,
															
															'label' =>'date',
														),									
													
														array(
															'column_name' =>'No. of Design',
															'table_column'  => '',
															/*'table_column_name'  => 'count(DISTINCT cp.sort_Item_number)',
															'table_column'  => 'cp.sort_Item_number',
															'id'  => 'cp_sort_Item_number',
															'is_filter'   => true,
															'source'=>'Engaged_karigar_list',
															'label' =>'text',*/
															
														),

														array(	
															'column_name' =>'Total Pcs',
															'table_column'  => '',
															/*'table_column_name'  => 'sum(ppl.quantity)',
															'table_column'  => 'ppl.quantity',
															'id'  => 'total_pcs',
															'is_filter'   => true,
															'source'=>'Engaged_karigar_list',
															'label' =>'text',*/
														),
														array(
															'column_name' =>'Received Pcs',
															'table_column_name'  => '',
															'table_column'  => 'received_pcs',
															
															'label' =>'text',
														),
														array(	
															'column_name' =>'Total Wt',
															'table_column_name'  => '',
															'table_column'  => 'total_wt',
															'id'  => 'total_wt',
															
															'label' =>'text',
														),
														array(
															'column_name' =>'Received Wt',
															'table_column_name'  => '',
															'table_column'  => 'received_wt',
															'id'  => 'received_wt',
															
															'label' =>'text',
														),
														array(
															'column_name' =>'Balance Wt',
															'table_column_name'  => '',
															'table_column'  => 'balance_wt',
															'id'  => 'balance_wt',
															
															'label' =>'text',
														),
														array(
															'column_name' =>'',	
															'table_column_name'=>'',
															'table_column'  => '',								
														)

							),	
						'due_date_tracking'=> array(
														array(
															'column_name' =>'Order Number',
															'table_column_name'  => 'cp.order_id',
															'table_column'  => 'cp.order_id',
															'id'  => 'order_id',
															'is_filter'   => true,
															'label' =>'text',
														),

														array(
															'column_name' =>'Line Number',
															'table_column_name'  => 'cp.line_number',
															'table_column'  => 'cp.line_number',
															'id'  => 'line_number',
															'is_filter'   => true,
															'label' =>'text',
														),
														array(
															'column_name' =>'Karigar Name',
															'table_column_name'  => 'k.name',
															'table_column'  => 'k.name',
															'id'  => 'k_name',
															'is_filter'   => true,
															
															'label' =>'text',
														),
														array(
															'column_name' =>'Date',
															'table_column_name'  => 'DATE_FORMAT(karigar_engaged_date,"%d-%m-%Y")',
															'table_column'  => 'karigar_engaged_date',
															'id'  => 'karigar_engaged_date',
															'is_filter'   => true,
															
															'label' =>'date',
														),								
																						
														array(
															'column_name' =>'Due Date',
															'table_column_name'  => 'DATE_FORMAT(karigar_delivery_date,"%d-%m-%Y")',
															'table_column'  => '',
															'id'  => 'karigar_delivery_date',
															'is_filter'   => true,
															
															'label' =>'date',
														),									
													
														array(
															'column_name' =>'Product Code',
															'table_column'  => '',
															'table_column_name'  => 'cp.sort_Item_number',
															'table_column'  => 'cp.sort_Item_number',
															'id'  => 'cp_sort_Item_number',
															'is_filter'   => true,
															'source'=>'Engaged_karigar_list',
															'label' =>'text'
															
														),

														array(	
															'column_name' =>'Quantity',
															'table_column'  => 'ppl.quantity',
															'table_column_name'  => 'ppl.quantity',
															'table_column'  => 'ppl.quantity',
															'id'  => 'quantity',
															'is_filter'   => true,
															'source'=>'Engaged_karigar_list',
															'label' =>'text'
														),
														
														array(	
															'column_name' =>'Total weight',
															'table_column_name'  => '',
															'table_column'  => 'total_wt',
															'id'  => 'total_wt',
															
															'label' =>'text',
														),
														

							),	
					'Engaged_karigar_Details_list'=> array(
														array(
															'column_name' =>'Design Code',
															'table_column_name'  => 'cp.sort_Item_number',
															'table_column'  => 'cp.sort_Item_number',
															'id'  => 'sort_Item_numbe',
															'is_filter'   => true,
															
															'label' =>'text',
														),
														array(
															'column_name' =>'Order Id',
															'table_column_name'  => 'cp.order_id',
															'table_column'  => 'cp.order_id',
															'id'  => 'order_id',
															'is_filter'   => true,
															
															'label' =>'text',
															
															
														),
														array(
															'column_name' =>'Karigar Engaged Date',
															'table_column_name'  => 'DATE_FORMAT(ppl.karigar_engaged_date,"%d-%m-%Y")',
															'table_column'  => 'karigar_engaged_date',
															'id'  => 'karigar_engaged_date',
															'is_filter'   => true,
															
															'label' =>'date',
														),
														array(
														'column_name' =>'Proposed delivery date',
														'table_column_name'  => 'DATE_FORMAT(ppl.karigar_delivery_date,"%d-%m-%Y")',
														'table_column'  => 'karigar_delivery_date',
														'id'  => 'karigar_delivery_date',
														'is_filter'   => true,
														
														'label' =>'date',
														),

														array(	
															'column_name' =>'Quantity',
															'table_column'  => '',
															'table_column_name'  => '',
															'table_column'  => 'Quantity',	
															'id'=>'Quantity',
															'label' =>'text',
														),
														array(
															'column_name' =>'Total Weight',
															'table_column_name'  => '',
															'table_column'  => 'received_pcs',
															
															'label' =>'text',
														),
														array(	
															'column_name' =>'Received Weight',
															'table_column_name'  => '',
															'table_column'  => 'total_wt',
															'id'  => 'total_wt',
															
															'label' =>'text',
														),														
														array(
															'column_name' =>'Balance Wt',
															'table_column_name'  => '',
															'table_column'  => 'balance_wt',
															'id'  => 'balance_wt',
															
															'label' =>'text',
														),
														
							),
						'Karigar_product_list'=>array(
													array(
														'column_name' =>'Order Id',
														'table_column_name'  => 'cp.order_id',
														'table_column'  => 'cp.order_id',
														'id'  => 'order_id',
														'is_filter'   => true,
														//'source'=>'Karigar_product_list',
														'label' =>'text',
													),array(
														'column_name' =>'Karigar Name',
														'table_column_name'  => 'k.name',
														'table_column'  => 'k.name',
														'id'  => 'k_name',
														'is_filter'   => true,
														//'source'=>'Karigar_product_list',
														'label' =>'text',
													),
													array(
														'column_name' =>'No. of Design',
														'table_column_name'  => 'cp.sort_Item_number',
														'table_column'  => 'cp.sort_Item_number',
														'id'  => 'cp_sort_Item_number',
														//'source'=>'Karigar_product_list',
														'label' =>'text',
													),
													array(
														'column_name' =>'Weight',
														'table_column_name'  => 'weight_band',
														'table_column'  =>'weight_band', 
														'id'  => 'weight_band',
														'source'=>'Karigar_product_list',
														'label' =>'text',
													),
													// array(
													// 	'column_name' =>'',
													// 	'table_column_name'=>'',
													// 	'table_column'  => '',
													
													// )

								),

			'corporate_due_date_reminder'=>array(
													array(
														'column_name' =>'Order No',
														'table_column_name'  => 'cp.order_id',
														'table_column'  => 'cp.order_id',
														'id'  => 'order_id',
														'is_filter'   => true,
														'label' =>'text',
													),
													array(
														'column_name' =>'Order Weight',
														'table_column_name'  => 'eo.net_weight',
														'table_column'  => 'eo.net_weight',
														'id'  => 'net_weight',
														//'source'=>'Karigar_product_list',
														'is_filter'   => true,
														'label' =>'text',
													),
													array(
														'column_name' =>'Order Date',
														'table_column_name'  => 'DATE_FORMAT(eo.date,"%d-%m-%Y")',
														'table_column'  =>'DATE(eo.date)', 
														'id'  => 'order_date',
														'is_filter'   => true,
														'label'=>'date',
														//'source'=>'Karigar_product_list',
													),
													array(
														'column_name' =>'Delivery Date',
														'table_column_name' => 'DATE_FORMAT(cp.proposed_delivery_date,"%d-%m-%Y")',
														'table_column'  =>'cp.proposed_delivery_date', 
														'id'  => 'proposed_delivery_date',
														//'source'=>'Karigar_product_list',
														'is_filter'   => true,
														'label' =>'date',
													
													),
													array(
														'column_name' =>'No of products ',
														'table_column_name'  =>"count(cp.sort_Item_number)",
														'table_column'  => 'cp.sort_Item_number',
														'id'  => 'sort_Item_number',
														//'source'=>'Karigar_product_list',
														'is_filter'   => true,
														'label' =>'text',
													
													),

													array(
														'column_name' =>'Corporate ',
														'table_column_name'  => 'c.name',
														'table_column'  => 'c.name',
														'id'  => 'corporate_name',
														'is_filter'   => true,
														//'source'=>'Karigar_product_list',
														'label' =>'text',
													
													)

								),	

						'Products_sent'=>array(
													array(
															'column_name' =>'Order Id',
															'table_column_name'  => 'eo.order_id',
															'table_column'  => 'eo.order_id',
															'id'  => 'order_id',
															'is_filter'   => true,
															//'source'=>'Products_sent',
															'label' =>'text',
													),
													/*array(
														'column_name' =>'Order Date',
														'table_column_name'  => 'DATE_FORMAT(ppl.karigar_engaged_date,"%d-%m-%Y")',
														'table_column'  => 'ppl.karigar_engaged_date',
														'id'  => 'karigar_engaged_date',
														'is_filter'   => true,
														//'source'=>'Products_sent',
														'label' =>'date',
													),
													array(
														'column_name' =>'Delivery Date',
														'table_column_name'  => 'DATE_FORMAT(ppl.karigar_delivery_date,"%d-%m-%Y")',
														'table_column'  => 'ppl.karigar_delivery_date',
														'id'  => 'karigar_delivery_date',
														'is_filter'   => true,
														//'source'=>'Products_sent',
														'label' =>'date',
													),*/
													array(
														'column_name' =>'Corporate',
														'table_column_name'  => 'c.name',
														'table_column'  => 'c.name',
														'id'  => 'corporate_id',
														'is_filter'   => true,							/*		
														'model_name'=>'Corporate_Products_model',
														'function_name' =>'get_corporates',*/
														'label' =>'text',
													),
													array(
														'column_name' =>'Assign Design',	
														'table_column'  => '',								
													),
													array(
														'column_name' =>'',	
														'table_column'  => '',									
													
													)
							),
						'Products_sent_by_order'=>array(
														array(
															'column_name' =>'Design Id',
															'table_column_name'  => 'sort_Item_number',
															'table_column'  => 'sort_Item_number',
															'id'  => 'sort_Item_number',
															'is_filter'   => true,
															//'source'=>'Products_sent_by_order',
															'label' =>'text',
														),
														array(
															'column_name' =>'Karigar Name',
															'table_column_name'  => 'km.name',
															'table_column'  => 'km.name',
															'id'  => 'k_name',
															'is_filter'   => true,
															//'source'=>'Products_sent_by_order',
															'label' =>'text',
														),
														array(
														'column_name' =>'Order Date',
														'table_column_name'  => 'DATE_FORMAT(ppl.karigar_engaged_date,"%d-%m-%Y")',
														'table_column'  => 'ppl.karigar_engaged_date',
														'id'  => 'karigar_engaged_date',
														'is_filter'   => true,
														//'source'=>'Products_sent',
														'label' =>'date',
													),
														array(
															'column_name' =>'Proposed Delivery Date',
															'table_column_name'  => 'DATE_FORMAT(karigar_delivery_date,"%d-%m-%Y")',
															'table_column'  => 'karigar_delivery_date',
															'id'  => 'karigar_delivery_date',
															'is_filter'   => true,
															//'source'=>'Products_sent_by_order',
															'label' =>'date',
														),
														array(	
															'column_name' =>'Quantity',
															'table_column'  => '',
														),
														array(
														'column_name' =>'Line #',
														'table_column_name'=>'',
														'table_column'  => '',
														),
														array(
															'column_name' =>'Weight',
																'table_column_name'=>'',
															'table_column'  => '' 
												
														),
														array(
															'column_name' =>'Size',
															'table_column_name'=>'',
															'table_column'  => '',
														
														),
														array(
															'column_name' =>'Image',
															'table_column_name'=>'',
															'table_column'  => '',
														
														)
							),

						'Receive_products'=>array(
														array(
															'column_name' =>'#',
															'type' =>'checkbox',	
															'table_column_name'=>'sort_Item_number', 
															'table_column'  => 'sort_Item_number',	
														),											
														array(
															'column_name' =>'Product Code',
															'table_column_name'  => 'sort_Item_number',
															'table_column'  => 'sort_Item_number',
															'id'  => 'sort_Item_number',
															'is_filter'   => true,
															//'source'=>'Products_sent_by_order',
															'label' =>'text',
														),
														array(
															'column_name' =>'Order Id',
															'table_column_name'  => 'rp.order_id',
															'table_column'  => 'rp.order_id',
															'id'  => 'order_id',
															'is_filter'   => true,
															//'source'=>'Products_sent_by_order',
															'label' =>'text',
														),
														array(
															'column_name' =>'Line #',
															'table_column_name'  => 'cp.line_number',
															'table_column'  => 'cp.line_number',
															'id'  => 'line_number',
															//'is_filter'   => true,
															//'source'=>'Products_sent_by_order',
															'label' =>'text',
														),
														array(
															'column_name' =>'Corporate',
															'table_column_name'  => 'corp.name',
															'table_column'  => 'corp.name',
															'id'  => 'corp_name',
															'is_filter'   => true,								
															'label' =>'text',	
															/*'model_name'=>'Corporate_Products_model',
															'function_name' =>'get_corporates',
															'label' =>'drop_down',*/
														),
														array(	
															'column_name' =>'Category',
															'table_column_name'  => 'rp.category_name',
															'table_column'  => 'rp.category_name',
															'id'  => 'category_name',
															'is_filter'   => true,
															'label' =>'text',
															/*'model_name'=>'Category_model',
															'function_name' =>'get',
															'label' =>'drop_down',*/
														),										
														array(
															'column_name' =>'Sub Category',
															'table_column_name'  => 'sbm.name',
															'table_column'  => 'sbm.name',
															'id'  => 'sub_category_id',
															'is_filter'   => true,	
															'label' =>'text',								
														/*	'model_name'=>'Sub_category_model',
															'function_name' =>'get',
															'label' =>'drop_down',*/
														),
														array(	
															'column_name' =>'Date',
															'table_column_name'  => 'DATE_FORMAT(rp.date,"%d-%m-%Y")',
															'table_column'  => 'rp.date',
															'id'  => 'date',
															'is_filter'   => true,
															//'source'=>'Products_sent_by_order',
															'label' =>'date',
														),
														array(	
															'column_name' =>'Size',
															'table_column_name'  => 'rp.size',
															'table_column'  => 'rp.size',
															//'id'  => 'Size',
															//'is_filter'   => true,
															//'source'=>'Products_sent_by_order',
															'label' =>'text',
														),
														array(	
															'column_name' =>'Quantity',	
															'table_column_name'=>'',
															'table_column'  => '',										
														),
														array(
															'column_name' =>'',
															'table_column_name'=>'',
															'table_column'  => '',
														
														)
								),
						
							'Quality_control'=> array(
														array(
															'column_name' =>'',
															'type' =>'checkbox',	
															'table_column_name'=>'rp.product_code', 
															'table_column'  => 'rp.product_code',							
														),
															
														array(
															'column_name' =>'Product Code',
															'table_column_name'  => 'rp.Product_code',
															'table_column'  => 'rp.Product_code',
															'id'  => 'product_code',
															'is_filter'   => true,
															
															'label' =>'text',
															
														),
														array(
															'column_name' =>'Order Id',
															'table_column_name'  => 'cp.order_id',
															'table_column'  => 'cp.order_id ',
															'id'  => 'order_id',
															'is_filter'   => true,
															
															'label' =>'text',
															
														),
															
														array(
															'column_name' =>'Date || Time',
															'table_column_name'  => 'DATE_FORMAT(qc.created_at,"%d-%m-%Y")',
															'table_column'  => 'qc.created_at',
															'id'  => 'created_at',
															'is_filter'   => true,
															
															'label' =>'date',
														),
														array(	
															'column_name' =>'Category',
															'table_column_name'  => 'rp.category_name',
															'table_column'  => 'rp.category_name',
															'id'  => 'category_name',
															'is_filter'   => true,
															'label' =>'text',
													/*		'model_name'=>'Category_model',
															'function_name' =>'get',
															'label' =>'drop_down',*/
														),
														array(
															'column_name' =>'Corporate',
															'table_column_name'  => 'corp.name',
															'table_column'  => 'corp.name',
															'id'  => 'corporate_id',
															'is_filter'   => true,	
															'label' =>'text',								
															/*'model_name'=>'Corporate_Products_model',
															'function_name' =>'get_corporates',
															'label' =>'drop_down',*/
														),
														array(	
															'column_name' =>'Quantity',
															
														),
														array(
															'column_name' =>'Size',
															'table_column_name'  => 'Size',
															'table_column'  => 'Size',
															
															'label' =>'text',
														),
														array(	
															'column_name' =>'Total Gr.Wt',
															'table_column_name'  => '',
															'table_column'  => 'total_gr_wt',
															'id'  => 'total_gr_wt',
															
															'label' =>'text',
														),
														array(
															'column_name' =>'Total Net.Wt',
															'table_column_name'  => '',
															'table_column'  => 'total_nt_wt',
															'id'  => 'total_nt_wt',
															
															'label' =>'text',
														),
														array(
															'column_name' =>'',	
															'table_column_name'=>'',
															'table_column'  => '',								
														)
														

							),
					'Hallmarking'=> array(
										array(
											'column_name' =>'',
											'type' =>'checkbox',	
											'table_column_name'=>'rp.Product_code', 
											'table_column'  => 'rp.Product_code',							
										),
											
										array(
											'column_name' =>'Product Code',
											'table_column_name'  => 'rp.Product_code',
											'table_column'  => 'rp.Product_code',
											'id'  => 'product_code',
											'is_filter'   => true,
											
											'label' =>'text',
											
										),
										array(
											'column_name' =>'Order No',
											'table_column_name'  => 'cp.work_order_id',
											'table_column'  => 'cp.work_order_id ',
											'id'  => 'work_order_id',
											'is_filter'   => true,
											
											'label' =>'text',
											
										),
										array(
											'column_name' =>'line Number',
											'table_column_name'  => 'cp.line_number',
											'table_column'  => 'cp.line_number ',
											'id'  => 'line_number',
											//'is_filter'   => true,
											
											'label' =>'text',
											
										),
										array(
											'column_name' =>'HM Center',
											'table_column_name'  => 'hc.name',
											'table_column'  => 'hc.name ',
											'id'  => 'hc_name',
											'is_filter'   => true,
											
											'label' =>'text',
											
										),
											
										array(	
											'column_name' =>'Category',
											'table_column_name'  => 'rp.category_name',
											'table_column'  => 'rp.category_name',
											'id'  => 'category_name',
											'is_filter'   => true,
											'label' =>'text',
							/*				'model_name'=>'Category_model',
											'function_name' =>'get',
											'label' =>'drop_down',*/
										),
										array(
											'column_name' =>'Date || Time',
											'table_column_name'  => 'DATE_FORMAT(qch.created_at,"%d-%m-%Y")',
											'table_column'  => 'qch.created_at',
											'id'  => 'created_at',
											'is_filter'   => true,
											
											'label' =>'date',
										),
									
										array(	
											'column_name' =>'Quantity',
											'table_column'  => '',
											
										),
										array(
											'column_name' =>'Size',
											'table_column_name'  => 'Size',
											'table_column'  => 'Size',
											
											'label' =>'text',
										),
										array(
											'column_name' =>'Total Net.Wt',
											'table_column_name'  => '',
											'table_column'  => 'total_nt_wt',
											'id'  => 'total_nt_wt',
											
											'label' =>'text',
										),
										array(	
											'column_name' =>'Total Gross.Wt',
											'table_column_name'  => '',
											'table_column'  => 'total_gr_wt',
											'id'  => 'total_gr_wt',
											
											'label' =>'text',
										),
										array(
											'column_name' =>'',	
											'table_column_name'=>'',
											'table_column'  => '',								
										)
														

							),		

								
							'Stock'=> array(
										array(
											'column_name' =>'#',	
											'table_column_name'=>'cp.sort_Item_number',
											'table_column'  => 'cp.sort_Item_number',																
										),														
																				
										array(
											'column_name' =>'Product Code',
											'table_column_name'  => 'cp.sort_Item_number ',
											'table_column'  => 'cp.sort_Item_number ',
											'id'  => 'product_code',
											'is_filter'   => true,
											
											'label' =>'text',
											
										),
																								
										array(	
											'column_name' =>'Category',
											'table_column_name'  => 'cm.name',
											'table_column'  => 'cm.name',
											'id'  => 'category_name',
											'is_filter'   => true,
											'label' =>'text',
											/*'model_name'=>'Category_model',
											'function_name' =>'get',
											'label' =>'drop_down',*/
										),
									
										array(
											'column_name' =>'Date || Time ',
											'table_column_name'  => 'DATE_FORMAT(s.created_at,"%d-%m-%Y")',
											'table_column'  => 's.created_at',
											'id'  => 'created_at',
											'is_filter'   => true,
											//'source'=>'stock',
											'label' =>'date',
										),
									
										array(
											'column_name' =>'Size',
											'table_column_name'  => 'qc.cp_size ',
											'table_column'  => 'qc.cp_size ',
											'is_filter'   => true,											
											'label' =>'text',
										),
										array(	
											'column_name' =>'Quantity',
											'table_column'=>''
										),
									
										array(	
											'column_name' =>'Total Gross.Wt',
											'table_column_name'  => '',
											'table_column'  => 'total_gr_wt',
											'id'  => 'total_gr_wt',
											
											'label' =>'text',
										),				
										array(
											'column_name' =>'',	
											'table_column_name'=>'',
											'table_column'  => '',								
										)
														

							),	
						'All_stock_products'=> array(
											array(
											'column_name' =>'',	
											'table_column_name'=>'ge.challan_no',
											'table_column'  => 'ge.challan_no',																		
										),														
										array(
											'column_name' =>'Challan No',
											'table_column_name'  => 'ge.challan_no',
											'table_column'  => 'ge.challan_no',
											'id'  => 'Challan_no',
											'is_filter'   => true,
											
											'label' =>'text',
											
										),
										array(
											'column_name' =>'Date || Time',
											'table_column_name'  => 'DATE_FORMAT(created_at,"%d-%m-%Y")',
											'table_column'  => 'gt.created_at',
											'id'  => 'gt_excel_created_at',
											'is_filter'   => true,
											//'source'=>'gt_excel_created_at',
											'label' =>'date',
										),										
									
										array(	
											'column_name' =>'Quantity',
											'table_column'=>''
										),
									
										array(	
											'column_name' =>'Total Gross.Wt',
											'table_column_name'  => '',
											'table_column'  => 'total_gr_wt',
											'id'  => 'total_gr_wt',
											
											'label' =>'text',
										),
										array(	
											'column_name' =>'Total NEt.Wt',
											'table_column_name'  => '',
											'table_column'  => 'total_net_wt',
											'id'  => 'total_net_wt',
											
											'label' =>'text',
										),
										array(	
											'column_name' =>'MK Value',
											'table_column_name'  => '',
											'table_column'  => 'Amount',
											'id'  => 'Amount',
											
											'label' =>'text',
										),
										array(
											'column_name' =>'',	
											'table_column_name'=>'',
											'table_column'  => '',								
										)
														

							),	
		'Generate_packing_list_create'=> array(
											array(
											'column_name' =>'#',
											'type'=>'checkbox',
											'table_column_name'=>'rp.product_code', 
											'table_column'  => 'rp.product_code',							
											),														
																				
										array(
											'column_name' =>'Product Code',
											'table_column_name'  => 'rp.product_code',
											'table_column'  => 'rp.product_code',
											'id'  => 'product_code',
											'is_filter'   => true,
											
											'label' =>'text',
											
										),
										array(
											'column_name' =>'Order Id',
											'table_column_name'  => 'eo.order_id',
											'table_column'  => 'eo.order_id',
											'id'  => 'Order_Id',
											'is_filter'   => true,
											
											'label' =>'text',
											
										),
										array(
											'column_name' =>'Corporate',
											'table_column_name'  => 'corp.name',
											'table_column'  => 'corp.name',
											'id'  => 'corporate_id',
											'is_filter'   => true,	
											'label' =>'text',								
											/*'model_name'=>'Corporate_Products_model',
											'function_name' =>'get_corporates',
											'label' =>'drop_down',*/
											),
																								
										array(	
											'column_name' =>'Category',
											'table_column_name'  => 'rp.category_name',
											'table_column'  => 'rp.category_name',
											'id'  => 'category_name',
											'is_filter'   => true,
											'label' =>'text',
											/*'model_name'=>'Category_model',
											'function_name' =>'get',
											'label' =>'drop_down',*/
										),
										array(
											'column_name' =>'Date || Time',
											'table_column_name'  => 'DATE_FORMAT(qc.created_at,"%d-%m-%Y")',
											'table_column'  => 'qc.created_at',
											'id'  => 'created_at',
											'is_filter'   => true,											
											'label' =>'date',
										),
									
										array(	
											'column_name' =>'Quantity',
											
										),
									
										array(
											'column_name' =>'Size',
											'table_column_name'  => 'Size',
											'table_column'  => 'Size',
											
											'label' =>'text',
										),
										array(	
											'column_name' =>'Total Gross.Wt',
											'table_column_name'  => '',
											'table_column'  => 'total_gr_wt',
											'id'  => 'total_gr_wt',
											
											'label' =>'text',
										),
										array(	
											'column_name' =>'Total NEt.Wt',
											'table_column_name'  => '',
											'table_column'  => 'total_net_wt',
											'id'  => 'total_net_wt',
											
											'label' =>'text',
										),
																								
										array(
											'column_name' =>'',	
											'table_column_name'=>'',
											'table_column'  => '',								
										)
														

							),
					'Packing_list'=> array(
										array(
											'column_name' =>'Order Id',
											'table_column_name'  => 'gp.order_no',
											'table_column'  => 'gp.order_no',
											'id'  => 'Order_Id',
											'is_filter'   => true,
											
											'label' =>'text',
											
										),
										array(
											'column_name' =>'Date || Time',
											'table_column_name'  => 'DATE_FORMAT(gp.created_at,"%d-%m-%Y")',
											'table_column'  => 'gp.created_at',
											'id'  => 'created_at',
											'is_filter'   => true,
											
											'label' =>'date',
										),								
										array(
											'column_name' =>'No of Products',
											'table_column'  => '',
											
										),
										array(	
											'column_name' =>'Quantity',
											'table_column'  => '',
											
										),
									
																																		
										array(
											'column_name' =>'',	
											'table_column_name'=>'',
											'table_column'  => '',								
										)
														

							),

					'user_app_tbl'=> array(
										
										array(
											'column_name' =>'#',
											'table_column_name'=>'', 
											'table_column'  => '',							
											),	
										array(
											'column_name' =>'First Name',
											'table_column_name'  => 'name',
											'table_column'  => 'name',
											'id'  => 'name',
											'is_filter'   => true,
											'label' =>'text',
										),								
										array(
											'column_name' =>'Middle Name',
											'table_column_name'  => 'name',
											'table_column'  => 'name',
											'id'  => 'name',
											'is_filter'   => true,
											'label' =>'text',
										),			
											array(
											'column_name' =>'Last Name',
											'table_column_name'  => 'name',
											'table_column'  => 'name',
											'id'  => 'name',
											'is_filter'   => true,
											'label' =>'text',
										),																				array(
											'column_name' =>'Email',
											'table_column_name'  => 'email',
											'table_column'  => 'email',
											'id'  => 'email',
											'is_filter'   => true,
											'label' =>'text',
										),	
												array(
											'column_name' =>'Mobile Number',
											'table_column_name'  => 'mobile_no',
											'table_column'  => 'mobile_no',
											'id'  => 'mobile_no',
											'is_filter'   => true,
											'label' =>'text',
										),																				
										array(
											'column_name' =>'',	
											'table_column_name'=>'',
											'table_column'  => '',								
										)
														

							),
				'packing_order_view'=> array(
									/*array(
											'column_name' =>'',																	
											),	*/
									array(
										'column_name' =>'Product Code',
										'table_column_name'  => 'cp.sort_Item_number',
										'table_column'  => 'cp.sort_Item_number',
										'id'  => 'product_code',
										'is_filter'   => true,
										
										'label' =>'text',
										
									),
									array(
										'column_name' =>'Corporate Order Id',
										'table_column_name'  => 'cp.order_id ',
										'table_column'  => 'cp.order_id',
										'id'  => 'order_no',
										'is_filter'   => true,
										
										'label' =>'text',
										
									),
									array(	
										'column_name' =>'Category',
										'table_column_name'  => 'rp.category_name',
										'table_column'  => 'rp.category_name',
										'id'  => 'category_name',
										'is_filter'   => true,
										'label' =>'text',
								/*		'model_name'=>'Category_model',
										'function_name' =>'get',
										'label' =>'drop_down',*/
									),
									array(
										'column_name' =>'Date || Time',
										'table_column_name'  => 'DATE_FORMAT(gp.created_at,"%d-%m-%Y")',
										'table_column'  => 'gp.created_at',
										'id'  => 'created_at',
										'is_filter'   => true,
										
										'label' =>'date',
									),								
									array(	
										'column_name' =>'Quantity',
										'table_column'  => '',										
									),
								
									array(
										'column_name' =>'Size',	
										'table_column'  => '',									
									),
									array(	
										'column_name' =>'Total Gross.Wt',
										'table_column_name'  => '',
										'table_column'  => 'total_gr_wt',
										'id'  => 'total_gr_wt',
										
										'label' =>'text',
										),
									array(	
										'column_name' =>'Total NEt.Wt',
										'table_column_name'  => '',
										'table_column'  => 'total_net_wt',
										'id'  => 'total_net_wt',
										
										'label' =>'text',
										),
																																	
									array(
										'column_name' =>'',	
										'table_column_name'=>'',
										'table_column'  => '',								
									)
													

						),
		
				'AmendTbl'=> array(
									
									array(
										'column_name' =>'Karigar Name',
										'table_column_name'  => 'k.name',
										'table_column'  => 'k.name',
										'id'  => 'k_name',
										'is_filter'   => true,
										
										'label' =>'text',
										
									),
									array(
										'column_name' =>'Order Id',
										'table_column_name'  => 'cp.order_id',
										'table_column'  => 'cp.order_id',
										'id'  => 'cp_order_id',
										//'is_filter'   => true,
										
										'label' =>'text',
										
									),
									
																																	
									array(
										'column_name' =>'',	
										'table_column_name'=>'',
										'table_column'  => '',								
									)
													

						),
	'client_order_ledger'=> array(

		         //                     	array(
											// 'column_name' =>'#',
											// 'table_column_name'=>'cor.customer_id', 
											// 'table_column'  => '',							
											// ),	
									
									array(
										'column_name' =>'Client Name',
										'table_column_name'  => 'co.party_name',
										'table_column'  => 'co.party_name',
										'id'  => 'party_name',
										'is_filter'   => true,
										'label' =>'text',
										
									),
									array(
										'column_name' =>'Weight',
										'table_column_name'  => 'cor.net_weight',
										'table_column'  => 'cor.net_weight',
										'id'  => 'cor.net_weight',
										//'is_filter'   => true,
										
										'label' =>'text',
										
									),
									
																																	
									array(
										'column_name' =>'',	
										'table_column_name'=>'',
										'table_column'  => '',								
									)
													

						),

         'karigar_order_ledger'=> array(

		         //                     	array(
											// 'column_name' =>'#',
											// 'table_column_name'=>'cor.customer_id', 
											// 'table_column'  => '',							
											// ),	
									
									array(
										'column_name' =>'Karigar Name',
										'table_column_name'  => 'km.name',
										'table_column'  => 'km.name',
										'id'  => 'karigar_name',
										'is_filter'   => true,
										'label' =>'text',
										
									),
									array(
										'column_name' =>'Weight',
										'table_column_name'  => 'cor.net_weight',
										'table_column'  => 'cor.net_weight',
										'id'  => 'cor.net_weight',
										//'is_filter'   => true,
										
										'label' =>'text',
										
									),
									
																																	
									array(
										'column_name' =>'',	
										'table_column_name'=>'',
										'table_column'  => '',								
									)
													

						),
                 'order_ledger_by_customer'=> array(

		         //                     	array(
											// 'column_name' =>'#',
											// 'table_column_name'=>'cor.customer_id', 
											// 'table_column'  => '',							
											// ),	
									
									array(
										'column_name' =>'Department Name',
										'table_column_name'  => 'dp.name',
										'table_column'  => 'dp.name',
										'id'  => 'dp_name',
										'is_filter'   => true,
										'label' =>'text',
										
									),
									array(
										'column_name' =>'Weight',
										'table_column_name'  => 'cor.net_weight',
										'table_column'  => 'cor.net_weight',
										'id'  => 'cor.net_weight',
										//'is_filter'   => true,
										
										'label' =>'text',
										
									),

									array(
										'column_name' =>'On-time Delivery Rate',
										'table_column_name'  => '',
										'table_column'  => '',
										'id'  => '',
										//'is_filter'   => true,
										
										'label' =>'text',
										
									),
									
						),

                 

  'order_ledger_by_karigar'=> array(

		         //                     	array(
											// 'column_name' =>'#',
											// 'table_column_name'=>'cor.customer_id', 
											// 'table_column'  => '',							
											// ),	
									
									array(
										'column_name' =>'Department Name',
										'table_column_name'  => 'dp.name',
										'table_column'  => 'dp.name',
										'id'  => 'dp_name',
										'is_filter'   => true,
										'label' =>'text',
										
									),
									array(
										'column_name' =>'Weight',
										'table_column_name'  => 'cor.net_weight',
										'table_column'  => 'cor.net_weight',
										'id'  => 'cor.net_weight',
										//'is_filter'   => true,
										
										'label' =>'text',
										
									),

									array(
										'column_name' =>'On-time Delivery Rate',
										'table_column_name'  => '',
										'table_column'  => '',
										'id'  => '',
										//'is_filter'   => true,
										
										'label' =>'text',
										
									),
									
						),


						'AmendView'=>array(
									array(
										'column_name' =>'',
										'type' =>'checkbox',	
										'table_column_name'=>'sort_Item_number', 
										'table_column'  => 'sort_Item_number',	
									),											
									array(
										'column_name' =>'Product Code',
										'table_column_name'  => 'sort_Item_number',
										'table_column'  => 'sort_Item_number',
										'id'  => 'sort_Item_number',
										'is_filter'   => true,
										//'source'=>'Products_sent_by_order',
										'label' =>'text',
									),
									array(
										'column_name' =>'Order Id',
										'table_column_name'  => 'cp.order_id',
										'table_column'  => 'cp.order_id',
										'id'  => 'order_id',
										'is_filter'   => true,
										//'source'=>'Products_sent_by_order',
										'label' =>'text',
									),
									
									array(	
										'column_name' =>'Category',
										'table_column_name'  => 'rp.category_name',
										'table_column'  => 'rp.category_name',
										'id'  => 'category_name',
										'is_filter'   => true,
										'label' =>'text',
										/*'model_name'=>'Category_model',
										'function_name' =>'get',
										'label' =>'drop_down',*/
									),										
									
									array(	
										'column_name' =>'Date',
										'table_column_name'  => 'DATE_FORMAT(rp.date,"%d-%m-%Y")',
										'table_column'  => 'rp.date',
										'id'  => 'date',
										'is_filter'   => true,
										//'source'=>'Products_sent_by_order',
										'label' =>'date',
									),
									array(	
										'column_name' =>'Size',
										'table_column_name'  => 'rp.size',
										'table_column'  => 'rp.size',
										'id'  => 'Size',
										'is_filter'   => true,
										//'source'=>'Products_sent_by_order',
										'label' =>'text',
									),
									array(	
										'column_name' =>'Quantity',	
										'table_column_name'=>'',
										'table_column'  => '',										
									),
									array(
										'column_name' =>'',
										'table_column_name'=>'',
										'table_column'  => '',
									
									)
			),
				
			'customer_order'=>array(
									array(
										'column_name' =>'#',
										'table_column_name'  => 'co.id',
										'table_column'  => 'co.id',
										'label' =>'text',
									),
									array(
										'column_name' =>'Order Number',
										'table_column_name'  => 'co.id',
										'table_column'  => 'co.id',
										'id'  => 'co_id',
										'is_filter'   => true,
										//'get_functions_name'=>'Products_sent_by_order',
										'label' =>'text',
									),
								/*	array(
										'column_name' =>'Order Name',
										'table_column_name'  => 'co.order_name',
										'table_column'  => 'co.order_name',
										'id'  => 'order_name',
										'is_filter'   => true,
										//'get_functions_name'=>'Products_sent_by_order',
										'label' =>'text',
									),*/
									array(
										'column_name' =>'Date',
										'table_column_name'  => 'DATE_FORMAT(co.order_date,"%d-%m-%Y")',
										'table_column'  => 'co.order_date',
										'id'  => 'order_date',
										'is_filter'   => true,
										//'get_functions_name'=>'Products_sent_by_order',
										'label' =>'date',
									),

								array(
										'column_name' =>'Party Name',
										'table_column_name'  => 'co.party_name',
										'table_column'  => 'co.party_name',
										'id'  => 'party_name',
										'is_filter'   => true,
										'get_functions_name'=>'Products_sent_by_order',
										'label' =>'text',
									),
									array(
										'column_name' =>'Karigar',
										'table_column_name'  => 'km.name',
										'table_column'  => 'km.name',
										'id'  => 'karigar_name',
										'is_filter'   => true,
										//'get_functions_name'=>'Products_sent_by_order',
										'label' =>'text',
									),
									array(
										'column_name' =>'Product',
										'table_column_name'  => 'co.Parent_category_id',
										'table_column'  => 'co.Parent_category_id',
										'id'  => 'Parent_category_id',
										'is_filter'   => true,
										//'get_functions_name'=>'Products_sent_by_order',
										'label' =>'text',
									),
									
									array(
										'column_name' =>'Approx Nt Wt',
										'table_column_name'  => '',
										'table_column'  => '',
										'id'  => 'Approx_Nt_Wt',
										
									),
									array(
										'column_name' =>'Quantity',
										'table_column_name'  => '',
										'table_column'  => '',
										'id'  => '',
										
									),
									array(
										'column_name' =>'',
										'table_column_name'=>'',
										'table_column'  => '',
									)
				),
			'customer_app_order'=>array(
									array(
										'column_name' =>'#',
										'table_column_name'  => 'co.id',
										'table_column'  => 'co.id',
										'label' =>'text',
									),
									array(
										'column_name' =>'Order Number',
										'table_column_name'  => 'co.id',
										'table_column'  => 'co.id',
										'id'  => 'co_id',
										'is_filter'   => true,
										//'get_functions_name'=>'Products_sent_by_order',
										'label' =>'text',
									),
								
									array(
										'column_name' =>'Date',
										'table_column_name'  => 'DATE_FORMAT(co.order_date,"%d-%m-%Y")',
										'table_column'  => 'co.order_date',
										'id'  => 'order_date',
										'is_filter'   => true,
										//'get_functions_name'=>'Products_sent_by_order',
										'label' =>'date',
									),

								array(
										'column_name' =>'Party Name',
										'table_column_name'  => 'co.party_name',
										'table_column'  => 'co.party_name',
										'id'  => 'party_name',
										'is_filter'   => true,
										'get_functions_name'=>'Products_sent_by_order',
										'label' =>'text',
									),
								
									array(
										'column_name' =>'Product',
										'table_column_name'  => 'co.Parent_category_id',
										'table_column'  => 'co.Parent_category_id',
										'id'  => 'Parent_category_id',
										'is_filter'   => true,
										//'get_functions_name'=>'Products_sent_by_order',
										'label' =>'text',
									),
									
									array(
										'column_name' =>'Approx Nt Wt',
										'table_column_name'  => '',
										'table_column'  => '',
										'id'  => 'Approx_Nt_Wt',
										
									),
									array(
										'column_name' =>'Quantity',
										'table_column_name'  => '',
										'table_column'  => '',
										'id'  => '',
										
									),
									array(
										'column_name' =>'',
										'table_column_name'=>'',
										'table_column'  => '',
									)
				),
			'customer_cancel_order'=>array(
									array(
										'column_name' =>'#',
										'table_column_name'  => 'co.id',
										'table_column'  => 'co.id',
										'label' =>'text',
									),
									array(
										'column_name' =>'Order Number',
										'table_column_name'  => 'co.id',
										'table_column'  => 'co.id',
										'id'  => 'co_id',
										'is_filter'   => true,
										//'get_functions_name'=>'Products_sent_by_order',
										'label' =>'text',
									),
								
									array(
										'column_name' =>'Date',
										'table_column_name'  => 'DATE_FORMAT(co.order_date,"%d-%m-%Y")',
										'table_column'  => 'co.order_date',
										'id'  => 'order_date',
										'is_filter'   => true,
										//'get_functions_name'=>'Products_sent_by_order',
										'label' =>'date',
									),

								array(
										'column_name' =>'Party Name',
										'table_column_name'  => 'co.party_name',
										'table_column'  => 'co.party_name',
										'id'  => 'party_name',
										'is_filter'   => true,
										'get_functions_name'=>'Products_sent_by_order',
										'label' =>'text',
									),
								
									array(
										'column_name' =>'Product',
										'table_column_name'  => 'co.Parent_category_id',
										'table_column'  => 'co.Parent_category_id',
										'id'  => 'Parent_category_id',
										'is_filter'   => true,
										//'get_functions_name'=>'Products_sent_by_order',
										'label' =>'text',
									),
									
									array(
										'column_name' =>'Approx Nt Wt',
										'table_column_name'  => '',
										'table_column'  => '',
										'id'  => 'Approx_Nt_Wt',
										
									),
									array(
										'column_name' =>'Quantity',
										'table_column_name'  => '',
										'table_column'  => '',
										'id'  => '',
										
									),
									array(
										'column_name' =>'',
										'table_column_name'=>'',
										'table_column'  => '',
									)
				),
				'customer_order_not_send'=>array(
									array(
										'column_name' =>'#',
										'type'=>'checkbox',
										'table_column_name'  => 'co.id',
										'table_column'  => 'co.id',
										'label' =>'text',
									),array(
										'column_name' =>'Order Number',
										'table_column_name'  => 'co.id',
										'table_column'  => 'co.id',
										'id'  => 'co_id',
										'is_filter'   => true,
										//'get_functions_name'=>'Products_sent_by_order',
										'label' =>'text',
									),
								/*	array(
										'column_name' =>'Order Name',
										'table_column_name'  => 'co.order_name',
										'table_column'  => 'co.order_name',
										'id'  => 'order_name',
										'is_filter'   => true,
										//'get_functions_name'=>'Products_sent_by_order',
										'label' =>'text',
									),*/
									array(
										'column_name' =>'Date',
										'table_column_name'  => 'DATE_FORMAT(co.order_date,"%d-%m-%Y")',
										'table_column'  => 'co.order_date',
										'id'  => 'order_date',
										'is_filter'   => true,
										//'get_functions_name'=>'Products_sent_by_order',
										'label' =>'date',
									),

									array(
										'column_name' =>'Party',
										'table_column_name'  => 'co.party_name',
										'table_column'  => 'co.party_name',
										'id'  => 'party_name',
										'is_filter'   => true,
										//'get_functions_name'=>'Products_sent_by_order',
										'label' =>'text',
									),
									array(
										'column_name' =>'Karigar',
										'table_column_name'  => 'km.name',
										'table_column'  => 'km.name',
										'id'  => 'karigar_name',
										'is_filter'   => true,
										//'get_functions_name'=>'Products_sent_by_order',
										'label' =>'text',
									),
									array(
										'column_name' =>'Product',
										'table_column_name'  => 'co.Parent_category_id',
										'table_column'  => 'co.Parent_category_id',
										'id'  => 'Parent_category_id',
										'is_filter'   => true,
										//'get_functions_name'=>'Products_sent_by_order',
										'label' =>'text',
									),
									
									array(
										'column_name' =>'Approx Nt Wt',
										'table_column_name'  => '',
										'table_column'  => '',
										'id'  => 'Approx_Nt_Wt',
										
									),
									array(
										'column_name' =>'Quantity',
										'table_column_name'  => '',
										'table_column'  => '',
										'id'  => '',
										
									),
									array(
										'column_name' =>'',
										'table_column_name'=>'',
										'table_column'  => '',
									)
				),
				'customer_order_pending'=>array(
													/*array(
															'column_name' =>'',
															'type' =>'checkbox',	
															'table_column_name'=>'co.id', 
															'table_column'  => 'co.id',	
														),*/	
													array(
														'column_name' =>'Order Number',
														'table_column_name'  => 'co.id',
														'table_column'  => 'co.id',
														'id'  => 'co_id',
														'is_filter'   => true,
														//'get_functions_name'=>'Products_sent_by_order',
														'label' =>'text',
													),
											
													array(
														'column_name' =>'Date',
														'table_column_name'  => 'DATE_FORMAT(co.order_date,"%d-%m-%Y")',
														'table_column'  => 'co.order_date',
														'id'  => 'order_date',
														'is_filter'   => true,
														//'get_functions_name'=>'Products_sent_by_order',
														'label' =>'date',
													),
													array(
														'column_name' =>'Department',
														'table_column_name'  => 'd.name',
														'table_column'  => 'd.name',
														'id'  => 'd_name',
														'is_filter'   => true,
														//'get_functions_name'=>'Products_sent_by_order',
														'label' =>'text',
													),

													array(
														'column_name' =>'Party',
														'table_column_name'  => 'co.party_name',
														'table_column'  => 'co.party_name',
														'id'  => 'party_name',
														'is_filter'   => true,
														//'get_functions_name'=>'Products_sent_by_order',
														'label' =>'text',
													),
													array(
														'column_name' =>'Karigar',
														'table_column_name'  => 'km.name',
														'table_column'  => 'km.name',
														'id'  => 'karigar_name',
														'is_filter'   => true,
														//'get_functions_name'=>'Products_sent_by_order',
														'label' =>'text',
													),
													array(
														'column_name' =>'Karigar Date',
														'table_column_name'  => 'DATE_FORMAT(co.expected_date,"%d-%m-%Y")',
														'table_column'  => 'co.expected_date',
														'id'  => 'expected_date',
														'is_filter'   => true,
														//'get_functions_name'=>'Products_sent_by_order',
														'label' =>'date',
													),
													array(
														'column_name' =>'Product',
														'table_column_name'  => 'co.Parent_category_id',
														'table_column'  => 'co.Parent_category_id',
														'id'  => 'Parent_category_id',
														'is_filter'   => true,
														//'get_functions_name'=>'Products_sent_by_order',
														'label' =>'text',
													),
													array(
														'column_name' =>'Approx Nt Wt',
														'table_column_name'  => '',
														'table_column'  => '',
														
													),
													array(
														'column_name' =>'Qty',
														'table_column_name'  => '',
														'table_column'  => '',
														
														
													),
													array(
														'column_name' =>'Gross Wt',
														'table_column_name'=>'',
														'table_column'  => '',
													),
													array(
														'column_name' =>'Net Wt',
														'table_column_name'=>'',
														'table_column'  => '',
													),
													array(
														'column_name' =>'Category',
														'table_column_name'=>'',
														'table_column'  => '',
													),
													array(
														'column_name' =>'Delivery date',
														'table_column_name'=>'',
														'table_column'  => '',
													),
													array(
														'column_name' =>'',
														'table_column_name'=>'',
														'table_column'  => '',
													)
								),
	'customer_received_order'=>array(
									array(
										'column_name' =>'',
										'type' =>'checkbox',		
										'table_column_name'  => 'co.id',
										'table_column'  => 'co.id',
										'id'  => 'sort_Item_number',
									),											
									array(
										'column_name' =>'Order Number',
										'table_column_name'  => 'co.id',
										'table_column'  => 'co.id',
										'id'  => 'sort_Item_number',
										'is_filter'   => true,
										//'source'=>'Products_sent_by_order',
										'label' =>'text',
									),
									array(
										'column_name' =>'Party',
										'table_column_name'  => 'co.party_name',
										'table_column'  => 'co.party_name',
										'id'  => 'order_id',
										'is_filter'   => true,
										//'source'=>'Products_sent_by_order',
										'label' =>'text',
									),									
									array(
										'column_name' =>'Karigar Name',
										'table_column_name'  => 'km.name',
										'table_column'  => 'km.name',
										'id'  => 'k_name',
										'is_filter'   => true,
										
										'label' =>'text',
									),
									array(
											'column_name' =>'Product',
											'table_column_name'  => 'co.Parent_category_id',
											'table_column'  => 'co.Parent_category_id',
											'id'  => 'Parent_category_id',
											'is_filter'   => true,
											//'get_functions_name'=>'Products_sent_by_order',
											'label' =>'text',
										),
									array(	
										'column_name' =>'Category',
										'table_column_name'  => 'c.name',
										'table_column'  => 'c.name',
										'id'  => 'category_name',
										'is_filter'   => true,
										'label' =>'text',
										/*'model_name'=>'Category_model',
										'function_name' =>'get',
										'label' =>'drop_down',*/
									),	
									array(	
										'column_name' =>'Department',
										'table_column_name'  => 'd.name',
										'table_column'  => 'd.name',
										'id'  => 'department_name',
										'is_filter'   => true,
										'label' =>'text',
										/*'model_name'=>'Category_model',
										'function_name' =>'get',
										'label' =>'drop_down',*/
									),							
									array(	
										'column_name' =>'Qty',
										'table_column_name'  => '',
										'table_column'  => '',
										'id'  => '',
									
										//'source'=>'Products_sent_by_order',
										'label' =>'text',
									),
									array(	
										'column_name' =>'Gross Wt',	
										'table_column_name'=>'',
										'table_column'  => '',										
									),
									array(	
										'column_name' =>'Net Wt',	
										'table_column_name'=>'',
										'table_column'  => '',										
									),
									array(	
										'column_name' =>'Date',
										'table_column_name'  => '',
										'table_column'  => '',
										'id'  => '',
									
										//'source'=>'Products_sent_by_order',
										'label' =>'text',
									),
									array(
										'column_name' =>'',
										'table_column_name'=>'',
										'table_column'  => '',
									
									)
			),		

		'karigar_engaged'=>array(
									array(
										'column_name' =>'#',
										'type' =>'checkbox',	
										'table_column_name'  => 'km.name',
										'table_column'  => 'km.name',
										'id'  => 'k_name',
									),											
																	
									array(
										'column_name' =>'Karigar Name',
										'table_column_name'  => 'km.name',
										'table_column'  => 'km.name',
										'id'  => 'k_name',
										'is_filter'   => true,
										'label' =>'text',
									),
									array(	
										'column_name' =>'Total orders',	
										'table_column_name'=>'',
										'table_column'  => '',										
									),
									array(	
										'column_name' =>'Total Weight',	
										'table_column_name'=>'',
										'table_column'  => '',										
									),
										
									array(	
										'column_name' =>'Received Weight',	
										'table_column_name'=>'',
										'table_column'  => '',										
									),
										
									array(	
										'column_name' =>'Balance',	
										'table_column_name'=>'',
										'table_column'  => '',										
									),
										
									array(	
										'column_name' =>'Quantity',	
										'table_column_name'=>'',
										'table_column'  => '',										
									),
													
																	
									array(
										'column_name' =>'',
										'table_column_name'=>'',
										'table_column'  => '',
									
									)
			),	


'delivery_performance_karigar_tbl'=>array(
									
									array(
										'column_name' =>'#',
										'type' =>'checkbox',	
										'table_column_name'  => 'co.id',
										'table_column'  => 'co.id',
										'id'  => 'sort_Item_number',
										'is_filter'   => true,
									),											
																	
									array(
										'column_name' =>'Karigar Name',
										'table_column_name'  => 'karigar_name',
										'table_column'  => 'karigar_name',
										'id'  => 'karigar_name',
										'is_filter'   => true,
										'label' =>'text',
									),
									array(	
										'column_name' =>'Department',	
										'table_column_name'=>'co.parent_category_id',
										'table_column'  => '',										
									),
									array(	
										'column_name' =>'Ontime',	
										'table_column_name'=>'',
										'table_column'  => '',										
									),
										
									array(	
										'column_name' =>'Delayed',	
										'table_column_name'=>'',
										'table_column'  => '',										
									),
										
									array(	
										'column_name' =>'Quantity',	
										'table_column_name'=>'quantity',
										'table_column'  => 'cor.quantity',										
									),
													
								
			),	

'delivery_performance_party_tbl'=>array(
									 array(
										'column_name' =>'#',
										'type' =>'checkbox',	
										'table_column_name'  => 'co.id',
										'table_column'  => 'co.id',
										'id'  => 'sort_Item_number',
									),		

									array(
										'column_name' =>'Party Name',
										'table_column_name'  => 'party_name',
										'table_column'  => 'party_name',
										'id'  => 'party_name',
										'is_filter'   => false,
										'label' =>'text',
									),
									array(	
										'column_name' =>'Department',	
										'table_column_name'=>'parent_category_id',
										'table_column'  => 'parent_category_id',										
									),
									array(	
										'column_name' =>'Ontime',	
										'table_column_name'=>'',
										'table_column'  => '',										
									),
										
									array(	
										'column_name' =>'Delayed',	
										'table_column_name'=>'',
										'table_column'  => '',										
									),
										
									array(	
										'column_name' =>'Quantity',	
										'table_column_name'=>'cor.quantity',
										'table_column'  => 'cor.quantity',										
									),
													
								
			),	

		'Karigar_remainder_tbl'=>array(
									array(
										'column_name' =>'#',
										'table_column_name'  => 'co.id',
										'table_column'  => 'co.id',
										
									),	
									array(	
										'column_name' =>'Order Number',	
										'table_column_name'  => 'co.id',
										'table_column'  => 'co.id',
										'id'  => 'co_id',
										'is_filter'   => true,
										//'get_functions_name'=>'Products_sent_by_order',
										'label' =>'text',								
									),
									array(
										'column_name' =>'Date',
										'table_column_name'  => 'DATE_FORMAT(co.order_date,"%d-%m-%Y")',
										'table_column'  => 'co.order_date',
										'id'  => 'order_date',
										'is_filter'   => true,
										//'get_functions_name'=>'Products_sent_by_order',
										'label' =>'date',
									),
									array(
										'column_name' =>'Order Name',
										'table_column_name'  => 'co.order_name',
										'table_column'  => 'co.order_name',
										'id'  => 'order_name',
										'is_filter'   => true,
										//'get_functions_name'=>'Products_sent_by_order',
										'label' =>'text',
									),
									
																	
									array(
										'column_name' =>'Karigar',
										'table_column_name'  => 'km.name',
										'table_column'  => 'km.name',
										'id'  => 'k_name',
										'is_filter'   => true,
										'label' =>'text',
									),
									
									array(
										'column_name' =>'Due Date',
										'table_column_name'  => 'DATE_FORMAT(co.expected_date,"%d-%m-%Y")',
										'table_column'  => 'co.expected_date',
										'id'  => 'expected_date',
										'is_filter'   => true,
										//'get_functions_name'=>'Products_sent_by_order',
										'label' =>'date',
									),
									array(	
										'column_name' =>'Quantity',	
										'table_column_name'=>'',
										'table_column'  => '',										
									),
																				
									
			),

		'customer_remainder_tbl'=>array(
										array(
										'column_name' =>'#',
										'table_column_name'  => 'co.id',
										
										
									),	
									array(	
										'column_name' =>'Order Number',	
										'table_column_name'  => 'co.id',
										'table_column'  => 'co.id',
										'id'  => 'co_id',
										'is_filter'   => true,
										//'get_functions_name'=>'Products_sent_by_order',
										'label' =>'text',								
									),
									array(
										'column_name' =>'Date',
										'table_column_name'  => 'DATE_FORMAT(co.order_date,"%d-%m-%Y")',
										'table_column'  => 'co.order_date',
										'id'  => 'order_date',
										'is_filter'   => true,
										//'get_functions_name'=>'Products_sent_by_order',
										'label' =>'date',
									),
									array(
										'column_name' =>'Order Name',
										'table_column_name'  => 'co.order_name',
										'table_column'  => 'co.order_name',
										'id'  => 'order_name',
										'is_filter'   => true,
										//'get_functions_name'=>'Products_sent_by_order',
										'label' =>'text',
									),
									
																	
									array(
										'column_name' =>'Customer',
										'table_column_name'  => 'co.party_name',
										'table_column'  => 'co.party_name',
										'id'  => 'k_name',
										'is_filter'   => true,
										'label' =>'text',
									),
									
									array(
										'column_name' =>'Due Date',
										'table_column_name'  => 'DATE_FORMAT(co.expected_date,"%d-%m-%Y")',
										'table_column'  => 'co.expected_date',
										'id'  => 'expected_date',
										'is_filter'   => true,
										//'get_functions_name'=>'Products_sent_by_order',
										'label' =>'date',
									),
									array(	
										'column_name' =>'Quantity',	
										'table_column_name'=>'',
										'table_column'  => '',										
									),										
									
			),

			'karigar_email_tracking_tbl'=>array(
									array(
										'column_name' =>'#',
										'table_column_name'  => 'co.id',
										'table_column'  => 'co.id',
										
									),	
									array(	
										'column_name' =>'Order Number',	
										'table_column_name'  => 'co.id',
										'table_column'  => 'co.id',
										'id'  => 'co_id',
										'is_filter'   => true,
										//'get_functions_name'=>'Products_sent_by_order',
										'label' =>'text',								
									),
									array(
										'column_name' =>'Order Name',
										'table_column_name'  => 'co.order_name',
										'table_column'  => 'co.order_name',
										'id'  => 'order_name',
										'is_filter'   => true,
										//'get_functions_name'=>'Products_sent_by_order',
										'label' =>'text',
									),
                                    array(
										'column_name' =>'Date',
										'table_column_name'  => 'DATE_FORMAT(co.order_date,"%d-%m-%Y")',
										'table_column'  => 'co.order_date',
										'id'  => 'order_date',
										'is_filter'   => true,
										//'get_functions_name'=>'Products_sent_by_order',
										'label' =>'date',
									),
									array(
										'column_name' =>'Karigar',
										'table_column_name'  => 'km.name',
										'table_column'  => 'km.name',
										'id'  => 'k_name',
										'is_filter'   => true,
										'label' =>'text',
									),
									array(	
										'column_name' =>'Title',	
										'table_column_name'=>'',
										'table_column'  => '',										
									),
									array(	
										'column_name' =>'Status',	
										'table_column_name'=>'',
										'table_column'  => '',										
									),
																				
									
			),
			'customer_email_tracking_tbl'=>array(
									array(
										'column_name' =>'#',
										'table_column_name'  => 'co.id',
										'table_column'  => 'co.id',
										
									),	
									array(	
										'column_name' =>'Order Number',	
										'table_column_name'  => 'co.id',
										'table_column'  => 'co.id',
										'id'  => 'co_id',
										'is_filter'   => true,
										//'get_functions_name'=>'Products_sent_by_order',
										'label' =>'text',								
									),
									array(
										'column_name' =>'Order Name',
										'table_column_name'  => 'co.order_name',
										'table_column'  => 'co.order_name',
										'id'  => 'order_name',
										'is_filter'   => true,
										//'get_functions_name'=>'Products_sent_by_order',
										'label' =>'text',
									),
									    array(
										'column_name' =>'Date',
										'table_column_name'  => 'DATE_FORMAT(co.order_date,"%d-%m-%Y")',
										'table_column'  => 'co.order_date',
										'id'  => 'co.order_date',
										'is_filter'   => true,
										//'get_functions_name'=>'Products_sent_by_order',
										'label' =>'date',
									),
									array(
										'column_name' =>'Customer',
										'table_column_name'  => 'co.party_name',
										'table_column'  => 'co.party_name',
										'id'  => 'k_name',
										'is_filter'   => true,
										'label' =>'text',
									),
									array(	
										'column_name' =>'Title',	
										'table_column_name'=>'',
										'table_column'  => '',										
									),
									array(	
										'column_name' =>'Status',	
										'table_column_name'=>'',
										'table_column'  => '',										
									),
																				
									
			),
		'accounting'=> array(
									
								array(
									'column_name' =>'Type',
									'table_column_name'  => 'cp.sort_Item_number',
									'table_column'  => 'cp.sort_Item_number',
									'id'  => 'product_code',
									'is_filter'   => true,
									
									'label' =>'text',
									
								),
								array(
									'column_name' =>'Voucher No',
									'table_column_name'  => 'cp.order_id ',
									'table_column'  => 'cp.order_id',
									'id'  => 'order_no',
									'is_filter'   => true,
									
									'label' =>'text',
									
								),
								array(
									'column_name' =>'Name',
									'table_column_name'  => 'c.name',
									'table_column'  => 'c.name',
									'id'  => 'order_no',
									'is_filter'   => true,									
									
									'label' =>'text',
									
								),
								array(	
									'column_name' =>'Gross Weight',
									'table_column'  => '',										
								),
								array(
									'column_name' =>'Pure',	
									'table_column'  => '',									
								),
								array(
									'column_name' =>'Voucher Date',
									'table_column_name'  => 'DATE_FORMAT(gp.created_at,"%d-%m-%Y")',
									'table_column'  => 'gp.created_at',
									'id'  => 'created_at',
									'label' =>'date',
								),																								
								array(
									'column_name' =>'',	
									'table_column_name'=>'',
									'table_column'  => '',								
								)
													

						),	
'jewerly_report_table'=> array(
								array(
									'column_name' =>'#',	
									'table_column_name'=>'corp.name',
									'table_column'  => 'corp.name',								
								),
								
								array(
									'column_name' =>'Corporate',
									'table_column_name'  => 'corp.name',
									'table_column'  => 'corp.name',
									'id'  => 'corp_name',
									'is_filter'   => true,									
									'label' =>'text',
									
								),
								array(
									'column_name' =>'Stock In-Hand',
									'table_column_name'  => '',
									'table_column'  => '',									
								),
								array(	
									'column_name' =>'Order Receipts',
									'table_column'  => '',										
								),
								array(
									'column_name' =>'Order In-Progress',	
									'table_column'  => '',									
								),
								array(
									'column_name' =>'Order Pending',	
									'table_column'  => '',									
								),
								array(
									'column_name' =>'Metal Received',	
									'table_column'  => '',									
								),
																														
								array(
									'column_name' =>'Metal Required',	
									'table_column_name'=>'',
									'table_column'  => '',								
								),
													

						),	
'jewerly_report_table_detail'=> array(
								array(
									'column_name' =>'#',	
									'table_column_name'=>'cp.sort_Item_number',
									'table_column'  => 'cp.sort_Item_number',								
								),
								
								array(
									'column_name' =>'Product Code',
									'table_column_name'  => 'cp.sort_Item_number',
									'table_column'  => 'cp.sort_Item_number',
									'id'  => 'cp_sort_Item_number',
									'is_filter'   => true,									
									'label' =>'text',
									
								),
								array(
									'column_name' =>'Gross Weight',
									'table_column_name'  => '',
									'table_column'  => '',									
								),
								array(	
									'column_name' =>'Net Weight',
									'table_column'  => '',										
								),
								array(
									'column_name' =>'In Progress',	
									'table_column'  => '',									
								),							
													

						),
'jewerly_report_table_detail_order_receipts'=> array(
								array(
									'column_name' =>'#',	
									'table_column_name'=>'cp.order_id',
									'table_column'  => 'cp.order_id',								
								),
								
								array(
									'column_name' =>'Order Name',
									'table_column_name'  => 'cp.order_id',
									'table_column'  => 'cp.order_id',
									'id'  => 'cp_order_id',
									'is_filter'   => true,									
									'label' =>'text',
									
								),
								array(
									'column_name' =>'Gross Weight',
									'table_column_name'  => '',
									'table_column'  => '',									
								),
								array(	
									'column_name' =>'Net Weight',
									'table_column'  => '',										
								),
											

						),
'jewerly_report_table_detail_recived_weight'=> array(
								array(
									'column_name' =>'#',	
									'table_column_name'=>'cp.order_id',
									'table_column'  => 'cp.order_id',								
								),
								
								array(
									'column_name' =>'Order Name',
									'table_column_name'  => 'cp.order_id',
									'table_column'  => 'cp.order_id',
									'id'  => 'order_id',
									'is_filter'   => true,									
									'label' =>'text',
									
								),
								array(
									'column_name' =>'Order Date || Time',
									'table_column_name'  => 'DATE_FORMAT(eo.created_at,"%d-%m-%Y")',
									'table_column'  => 'eo.created_at',
									'id'  => 'order_date',
									'is_filter'   => true,
									'label' =>'date',
									),
								array(
									'column_name' =>'Gross Weight',
									'table_column_name'  => '',
									'table_column'  => '',									
								),
								array(	
									'column_name' =>'Net Weight',
									'table_column'  => '',										
								),
								array(
									'column_name' =>'Quantity Placed',	
									'table_column'  => '',									
								),	
								array(
									'column_name' =>'Quantity Received',	
									'table_column'  => '',									
								),							
													

						),
'jewerly_report_table_detail_rorder_pending'=> array(
								array(
									'column_name' =>'#',	
									'table_column_name'=>'cp.order_id',
									'table_column'  => 'cp.order_id',								
								),
								
								array(
									'column_name' =>'Order Name',
									'table_column_name'  => 'cp.order_id',
									'table_column'  => 'cp.order_id',
									'id'  => 'order_id',
									'is_filter'   => true,									
									'label' =>'text',
									
								),
								array(
									'column_name' =>'Order Date || Time',
									'table_column_name'  => 'DATE_FORMAT(eo.created_at,"%d-%m-%Y")',
									'table_column'  => 'eo.created_at',
									'id'  => 'order_date',
									'is_filter'   => true,
									'label' =>'date',
									),
								array(
									'column_name' =>'Gross Weight',
									'table_column_name'  => '',
									'table_column'  => '',									
								),
								array(	
									'column_name' =>'Net Weight',
									'table_column'  => '',										
								),
								array(
									'column_name' =>'Quantity Placed',	
									'table_column'  => '',									
								),	
								array(
									'column_name' =>'Quantity Pending',	
									'table_column'  => '',									
								),							
													

						),
'accounting_report_table'=> array(
								array(
									'column_name' =>'#',	
									'table_column_name'=>'cp.order_id',
									'table_column'  => 'cp.order_id',								
								),
								
								array(
									'column_name' =>'Type',
									'table_column_name'  => 'cp.order_id',
									'table_column'  => 'cp.order_id',
									
								),
								
								array(
									'column_name' =>'Name',
									'table_column_name'  => 'cp.name',
									'table_column'  => 'cp.name',
									'id'  => 'name',
									'is_filter'   => true,									
									'label' =>'text',								
								),
								array(	
									'column_name' =>'Issue Weight',
									'table_column'  => '',										
								),
								array(
									'column_name' =>'Receipt Weight',	
									'table_column'  => '',									
								),	
								array(
									'column_name' =>'Balance Weight',	
									'table_column'  => '',									
								),							
													

						),




		'Catagory'=> array(								
								array(
									'column_name' =>'Category Name',
									'table_column_name'  => 'cat.name',
									'table_column'  => 'cat.name',
									'id'  => 'cat_name',
									'is_filter'   => true,
									'label' =>'text',
									
								),
								array(
									'column_name' =>'Code',
									'table_column_name'  => 'dep.name',
									'table_column'  => 'dep.name',
									'id'  => 'dep_name',						
									
								),
							/*	array(
									'column_name' =>'Department',
									'table_column_name'  => 'dep.name',
									'table_column'  => 'dep.name',
									'id'  => 'dep_name',	
								    'is_filter'   => true,					
									
								),*/
								array(
									'column_name' =>'Purity',
									'table_column_name'  => '',
									'table_column'  => '',
									'id'  => '',						
									
								),
								array(
									'column_name' =>'wastage',
									'table_column_name'  => '',
									'table_column'  => '',
									'id'  => '',						
									
								),
								array(
									'column_name' =>'Labore/Gm',
									'table_column_name'  => '',
									'table_column'  => '',
									'id'  => '',						
									
								),

								array(
									'column_name' =>'',	
									'table_column_name'=>'',
									'table_column'  => '',								
								),

				),	
			'sub_catagory'=> array(								
								array(
									'column_name' =>'sub Category Name',
									'table_column_name'  => 'scm.name',
									'table_column'  => 'scm.name',
									'id'  => 'scm_name',
									'is_filter'   => true,
									
									'label' =>'text',
									
								),
								array(
										'column_name' =>'Category Name',
										'table_column_name'  => 'cm.name',
										'table_column'  => 'cm.name',
										'id'  => 'cm_name',
										'is_filter'   => true,
										
										'label' =>'text',
										
									),
								array(
									'column_name' =>'Code',
									'table_column_name'  => 'scm.code',
									'table_column'  => 'scm.code',
									'id'  => 'scm_code',
									'is_filter'   => true,
									'label' =>'text',
																
									
								),

								array(
									'column_name' =>'',	
									'table_column_name'=>'',
									'table_column'  => '',								
								),

			),	
			'weights'=> array(								
								array(
									'column_name' =>'From weights ',
									'table_column_name'  => 'from_weight',
									'table_column'  => 'from_weight',
									'id'  => 'from_weight',
									'is_filter'   => true,
									
									'label' =>'text',
									
								),
								array(
										'column_name' =>'To weights',
										'table_column_name'  => 'to_weight',
										'table_column'  => 'to_weight',
										'id'  => 'to_weight',
										'is_filter'   => true,
										
										'label' =>'text',
										
									),								

								array(
									'column_name' =>'',	
									'table_column_name'=>'',
									'table_column'  => '',								
								),

			),								
						
			'Article'=> array(								
								array(
									'column_name' =>'Article Name',
									'table_column_name'  => 'name',
									'table_column'  => 'name',
									'id'  => 'name',
									'is_filter'   => true,
									
									'label' =>'text',
									
								),
								array(
									'column_name' =>'Description',
									'table_column_name'  => 'description',
									'table_column'  => 'description',
									'id'  => 'description',
									'is_filter'   => true,									
									'label' =>'text',
									),								

								array(
									'column_name' =>'',	
									'table_column_name'=>'',
									'table_column'  => '',								
								),

			),			
			'Carat'=> array(								
								array(
									'column_name' =>'Carat Name',
									'table_column_name'  => 'name',
									'table_column'  => 'name',
									'id'  => 'name',
									'is_filter'   => true,									
									'label' =>'text',
									
								),
								array(
									'column_name' =>'',	
									'table_column_name'=>'',
									'table_column'  => '',								
								),

			),	
			'manufacturing_type'=> array(								
								array(
									'column_name' =>'Manufacturing Types Name',
									'table_column_name'  => 'name',
									'table_column'  => 'name',
									'id'  => 'name',
									'is_filter'   => true,									
									'label' =>'text',
									
								),
								array(
									'column_name' =>'',	
									'table_column_name'=>'',
									'table_column'  => '',								
								),

			),	
	        'buying_complexity'=> array(								
								array(
									'column_name' =>'Buying Complexity Name',
									'table_column_name'  => 'name',
									'table_column'  => 'name',
									'id'  => 'name',
									'is_filter'   => true,									
									'label' =>'text',
									
								),
								array(
									'column_name' =>'',	
									'table_column_name'=>'',
									'table_column'  => '',								
								),

			),	
			'pcs'=> array(								
								array(
									'column_name' =>'Pcs Number',
									'table_column_name'  => 'name',
									'table_column'  => 'name',
									'id'  => 'name',
									'is_filter'   => true,									
									'label' =>'text',
									
								),
								array(
									'column_name' =>'',	
									'table_column_name'=>'',
									'table_column'  => '',								
								),

			),			

			'department'=> array(								
								array(
									'column_name' =>'Name',
									'table_column_name'  => 'name',
									'table_column'  => 'name',
									'id'  => 'name',
									'is_filter'   => true,									
									'label' =>'text',
									
								),
								array(
									'column_name' =>'',	
									'table_column_name'=>'',
									'table_column'  => '',								
								),

			),	
			'hallmarking_list_table'=> array(								
								array(
									'column_name' =>'HM Center',
									'table_column_name'  => 'hc.name',
									'table_column'  => 'hc.name',
									'id'  => 'hc_name',
									'is_filter'  => true,
									
									'label' =>'text',
									
								),
								array(
										'column_name' =>'HM Code',
										'table_column_name'  => 'hc.code',
										'table_column'  => 'hc.code',
										'id'  => 'hc_code',
										'is_filter'   => true,
										
										'label' =>'text',
										
								),								
								array(
									'column_name' =>'Company Name',
									'table_column_name'  => 'hl.c_name',
									'table_column'  => 'hl.c_name',
									'id'  => 'hl_c_name',
									'is_filter'   => true,
									
									'label' =>'text',
									
								),
								array(
										'column_name' =>'Owner',
										'table_column_name'  => 'hl.owner',
										'table_column'  => 'hl.owner',
										'id'  => 'hl_owner',
										'is_filter'   => true,
										
										'label' =>'text',
										
									),	
								array(
										'column_name' =>'Authorized Person',
										'table_column_name'  => 'hl.ap_name',
										'table_column'  => 'hl.ap_name',
										'id'  => 'hl_ap_name',
										'is_filter'   => true,										
										'label' =>'text',
										
								),								
								array(
									'column_name' =>'Mobile No',
									'table_column_name'  => 'hl.mobile_no',
									'table_column'  => 'hl.mobile_no',
									'id'  => 'hl_mobile_no',
									'is_filter'   => true,
									
									'label' =>'text',
									
								),
								array(
									'column_name' =>'Phone No',
									'table_column_name'  => 'hl.phone_no',
									'table_column'  => 'hl.phone_no',
									'id'  => 'hl_phone_no',
									'is_filter'   => true,
									
									'label' =>'text',
									
									),
								array(
									'column_name' =>'Address',	
									'table_column_name'=>'',
									'table_column'  => '',								
								),
								array(
									'column_name' =>'',	
									'table_column_name'=>'',
									'table_column'  => '',								
								),

			),	
			'parent_category_table'=> array(								
								array(
									'column_name' =>'Name',
									'table_column_name'  => 'pc.name',
									'table_column'  => 'pc.name',
									'id'  => 'name',
									'is_filter'   => true,
									
									'label' =>'text',
									
								),
								array(
									'column_name' =>'Code',
									'table_column_name'  => 'pc.code',
									'table_column'  => 'pc.code',
									'id'  => 'pc_code',
									'is_filter'   => true,									
									'label' =>'text',
								),
								array(
									'column_name' =>'Parent Product Category',
									'table_column_name'  => 'cm.name ',
									'table_column'  => 'cm.name ',
									'id'  => 'cm_name ',
									'is_filter'   => true,									
									'label' =>'text',
								),									

								array(
									'column_name' =>'',	
									'table_column_name'=>'',
									'table_column'  => '',								
								),

			),	
			'party_master_table'=> array(								
								array(
									'column_name' =>'Type',
									'table_column_name'  => 'ct.name',
									'table_column'  => 'ct.name ',
									'id'  => 'ct_name',
									'is_filter'   => true,									
									'label' =>'text'
								
									
								),
								array(
									'column_name' =>'Name',
									'table_column_name'  => 'k.name',
									'table_column'  => 'k.name',
									'id'  => 'k_name',
									'is_filter'   => true,									
									'label' =>'text',
								),
								array(
									'column_name' =>'Code',
									'table_column_name'  => 'k.code',
									'table_column'  => 'k.code',
									'id'  => 'k_code',
									
								),
								array(
									'column_name' =>'Mobile Number',
									'table_column_name'  => 'Mobile ',
									'table_column'  => 'Mobile',
									'id'  => 'Mobile ',
									'is_filter'   => true,
									
								),										

								array(
									'column_name' =>'',	
									'table_column_name'=>'',
									'table_column'  => '',								
								),

			),	
			'customer_type_table'=> array(								
								array(
									'column_name' =>'Name',
									'table_column_name'  => 'c.name',
									'table_column'  => 'c.name',
									'id'  => 'name',
									'is_filter'   => true,									
									'label' =>'text',
									
								),
								array(
									'column_name' =>'',	
									'table_column_name'=>'',
									'table_column'  => '',								
								),

			),
'product_category_table'=> array(								
								array(
									'column_name' =>'Parent Product Category',
									'table_column_name'  => 'pr_c.name',
									'table_column'  => 'pr_c.name',
									'id'  => 'name',
									'is_filter'   => true,									
									'label' =>'text',
									
								),
								array(
									'column_name' =>'',	
									'table_column_name'=>'',
									'table_column'  => '',								
								),

			),
'type_master_table'=> array(								
								array(
									'column_name' =>'Name',
									'table_column_name'  => 't.name',
									'table_column'  => 't.name',
									'id'  => 'name',
									'is_filter'   => true,									
									'label' =>'text',
									
								),
								array(
									'column_name' =>'',	
									'table_column_name'=>'',
									'table_column'  => '',								
								),

			),

'department_order'=>array(

							array(
								'column_name' =>'Order ID',
								'table_column_name'  => 'mo.id',
								'table_column'  => 'mo.id',
								'id'  => 'mo_id',
								'is_filter'   => true,
								'label' =>'text',
							),
							array(
								'column_name' =>'Order Name',
								'table_column_name'  => 'mo.order_name',
								'table_column'  => 'mo.order_name',
								'id'  => 'mo_order_name',
								'is_filter'   => true,
								'label' =>'text',
							),
							array(
								'column_name' =>'Order Date',
								'table_column_name'  => 'DATE_FORMAT(mo.order_date,"%d-%m-%Y")',
								'table_column'  =>'mo.order_date', 
								'id'  => 'mo_order_date',
								'is_filter'   => true,
								'label' =>'date',
							),
							array(
								'column_name' =>'Department Name',
								'table_column_name'  => 'd.name',
								'table_column'  =>'d.name', 
								'id'  => 'd_name',
								'is_filter'   => true,
								'label' =>'text',
							),													
							array(
								'column_name' =>'',
								'table_column_name'=>'',
								'table_column'  => '',
							
							),
							array(
								'column_name' =>'',
								'table_column_name'=>'',
								'table_column'  => '',
							
							),

				),
'dispatch_mode_table'=>array(
							array(
								'column_name' =>'#',
								'table_column_name'=>'mode_name',
								'table_column'  => 'mode_name',
							
							),
							array(
								'column_name' =>'Mode Name',
								'table_column_name'  => 'mode_name',
								'table_column'  => 'mode_name',
								'id'  => 'mode_name',
								'is_filter'   => true,
								'label' =>'text',
							),
							array(
								'column_name' =>'Mode Type',
								'table_column_name'  => 'mode_type',
								'table_column'  => 'mode_type',
								'id'  => 'mode_type',
								//'is_filter'   => true,
								'label' =>'text',
							),						
							array(
								'column_name' =>'Image Upload',
								'table_column_name'  => '',
								'table_column'  =>'', 
								'id'  => '',
							),								
							array(
								'column_name' =>'',
								'table_column_name'=>'',
								'table_column'  => '',
							
							),
							

				),
'salesman_master_table'=>array(
							array(
								'column_name' =>'Name',
								'table_column_name'=>'s.name',
								'table_column'  => 's.name',
								'id'=>'s_name',
								'is_filter'   => true,
								'label' =>'text',
							
							),
							array(
								'column_name' =>'Mobile Number',
								'table_column_name'  => '',
								'table_column'  => '',
								
								
							),
							array(
								'column_name' =>'Address',
								'table_column_name'  => '',
								'table_column'  => '',
							
							),						
							array(
								'column_name' =>'City',
								'table_column_name'  => '',
								'table_column'  =>'', 
								'id'  => '',
							),								
							array(
								'column_name' =>'State',
								'table_column_name'=>'',
								'table_column'  => '',
							
							),
							array(
								'column_name' =>'',
								'table_column_name'=>'',
								'table_column'  => '',
							
							),
							

				),
'material_master_table'=>array(
						
							array(
								'column_name' =>'Description',
								'table_column_name'  => 'description',
								'table_column'  => 'description',
								'id'  => 'description',
								'is_filter'   => true,
								'label' =>'text',
							),
							array(
								'column_name' =>'Short Code',
								'table_column_name'  => 'short_code',
								'table_column'  => 'short_code',
								'id'  => 'short_code',
								'is_filter'   => true,
								'label' =>'text',
							),					
							array(
								'column_name' =>'',
								'table_column_name'=>'',
								'table_column'  => '',
							
							),
							

				),
	'variation_category_table'=>array(
						
							array(
								'column_name' =>'Type',
								'table_column_name'  => 'cs.type',
								'table_column'  => 'cs.type',
								'id'  => 'cs_type',
								'is_filter'   => true,
								'label' =>'text',
							),
							array(
								'column_name' =>'Name',
								'table_column_name'  => 'cs.name',
								'table_column'  => 'cs.name',
								'id'  => 'cs_name',
								'is_filter'   => true,
								'label' =>'text',
							),
										
							array(
								'column_name' =>'',
								'table_column_name'=>'',
								'table_column'  => '',
							
							),
							

				),
		'terms_table'=>array(
							array(
								'column_name' =>'#',
								'table_column_name'  => 'pc.name',
								'table_column'  => 'pc.name',
								
							),
							array(
								'column_name' =>'Parent Category',
								'table_column_name'  => 'pc.name',
								'table_column'  => 'pc.name',
								'id'  => 'pc_name',
								'is_filter'   => true,
								'label' =>'text',
							),
							
							array(
								'column_name' =>'Code',
								'table_column_name'  => 'tm.code',
								'table_column'  => 'tm.code',
								'id'  => 'tm_code',
								'is_filter'   => true,
								'label' =>'text',
							),
							array(
								'column_name' =>'Hard Cash',
								'table_column_name'  => 'tm.hard_cash',
								'table_column'  =>'tm.hard_cash', 
								'id'  => 'tm_hard_cash',
								//'is_filter'   => true,
								'label' =>'text',
							),													
							array(
								'column_name' =>'Normal',
								'table_column_name'=>'normal',
								'table_column'  => 'normal',
							
							),
							array(
								'column_name' =>'7 days',
								'table_column_name'=>'7days',
								'table_column'  => '7days',
							
							),
							array(
								'column_name' =>'15 days',
								'table_column_name'=>'15days',
								'table_column'  => '15days',
							
							),
							array(
								'column_name' =>'1 Month',
								'table_column_name'=>'1month',
								'table_column'  => '1month',
							
							),
							array(
								'column_name' =>'2 Month',
								'table_column_name'=>'2month',
								'table_column'  => '2month',
							
							),
							array(
								'column_name' =>'',
								'table_column_name'=>'',
								'table_column'  => '',
							
							),

				),
		
		'Karigar_order_bom_dep'=>array(
							array(
								'column_name' =>'Order ID',
								'table_column_name'  => 'mo.id',
								'table_column'  => 'mo.id',
								'id'  => 'mo_id',
								'is_filter'   => true,
								'label' =>'text',
							),
							array(
								'column_name' =>'Assign Date || Time',
								'table_column_name'  => 'DATE_FORMAT(kmm.created_at,"%d-%m-%Y")',
								'table_column'  =>'kmm.created_at', 
								'id'  => 'kmm_created_at',
								'is_filter'   => true,
								'label' =>'date',
							),
							array(
								'column_name' =>'Karigar Name',
								'table_column_name'  => 'km.name',
								'table_column'  => 'km.name',
								'id'  => 'km_name',
								'is_filter'   => true,
								'label' =>'text',
							),
							array(
								'column_name' =>'Product',
								'table_column_name'  => 'pm.name',
								'table_column'  =>'pm.name', 
								'id'  => 'pm_name',
								'is_filter'   => true,
								'label' =>'text',
							),													
							array(
								'column_name' =>'Weight Range',
								'table_column_name'=>'',
								'table_column'  => '',
							
							),
							array(
								'column_name' =>'Weight',
								'table_column_name'=>'',
								'table_column'  => '',
							
							),
							
							array(
								'column_name' =>'',
								'table_column_name'=>'',
								'table_column'  => '',
							
							),

				),
		'Karigar_order'=>array(
							array(
								'column_name' =>'Order ID',
								'table_column_name'  => 'mo.id',
								'table_column'  => 'mo.id',
								'id'  => 'mo_id',
								'is_filter'   => true,
								'label' =>'text',
							),
							array(
								'column_name' =>'Assign Date || Time',
								'table_column_name'  => 'DATE_FORMAT(kmm.created_at,"%d-%m-%Y")',
								'table_column'  =>'kmm.created_at', 
								'id'  => 'kmm_created_at',
								'is_filter'   => true,
								'label' =>'date',
							),
							array(
								'column_name' =>'Karigar Name',
								'table_column_name'  => 'km.name',
								'table_column'  => 'km.name',
								'id'  => 'km_name',
								'is_filter'   => true,
								'label' =>'text',
							),
							array(
								'column_name' =>'Product',
								'table_column_name'  => 'pm.name',
								'table_column'  =>'pm.name', 
								'id'  => 'pm_name',
								'is_filter'   => true,
								'label' =>'text',
							),													
							array(
								'column_name' =>'Weight Range',
								'table_column_name'=>'',
								'table_column'  => '',
							
							),
							array(
								'column_name' =>'Quantity',
								'table_column_name'=>'',
								'table_column'  => '',
							
							),
							array(
								'column_name' =>'Approx Weight',
								'table_column_name'=>'',
								'table_column'  => '',
							
							),
							array(
								'column_name' =>'',
								'table_column_name'=>'',
								'table_column'  => '',
							
							),

				),
		'order_sent'=>array(
							array(
								'column_name' =>'Order ID',
								'table_column_name'  => 'mo.id',
								'table_column'  => 'mo.id',
								'id'  => 'mo_id',
								'is_filter'   => true,
								'label' =>'text',
							),
							array(
								'column_name' =>'Order Date || Time',
								'table_column_name'  => 'DATE_FORMAT(kmm.created_at,"%d-%m-%Y")',
								'table_column'  =>'kmm.created_at', 
								'id'  => 'kmm_created_at',
								'is_filter'   => true,
								'label' =>'date',
							),
							array(
								'column_name' =>'Delivery Date',
								'table_column_name'  => 'DATE_FORMAT(kmm.delivery_date,"%d-%m-%Y")',
								'table_column'  =>'kmm.delivery_date', 
								'id'  => 'kmm_delivery_date',
								'is_filter'   => true,
								'label' =>'date',
							),							
							array(
								'column_name' =>'Karigar Name',
								'table_column_name'  => 'km.name',
								'table_column'  => 'km.name',
								'id'  => 'km_name',
								'is_filter'   => true,
								'label' =>'text',
							),
							array(
								'column_name' =>'Sub Category',
								'table_column_name'  => 'pm.name',
								'table_column'  =>'pm.name', 
								'id'  => 'pm_name',
								'is_filter'   => true,
								'label' =>'text',
							),													
							array(
								'column_name' =>'Weight Range',
								'table_column_name'=>'',
								'table_column'  => '',
							
							),
							array(
								'column_name' =>'Quantity',
								'table_column_name'=>'',
								'table_column'  => '',
							
							),
							array(
								'column_name' =>'Approx Weight',
								'table_column_name'=>'',
								'table_column'  => '',
							
							),							
							array(
								'column_name' =>'',
								'table_column_name'=>'',
								'table_column'  => '',
							
							),

				),

			'order_sent_oth_dep'=>array(
							array(
								'column_name' =>'Order ID',
								'table_column_name'  => 'mo.id',
								'table_column'  => 'mo.id',
								'id'  => 'mo_id',
								'is_filter'   => true,
								'label' =>'text',
							),
							array(
								'column_name' =>'Order Date || Time',
								'table_column_name'  => 'DATE_FORMAT(kmm.created_at,"%d-%m-%Y")',
								'table_column'  =>'kmm.created_at', 
								'id'  => 'kmm_created_at',
								'is_filter'   => true,
								'label' =>'date',
							),
							array(
								'column_name' =>'Delivery Date',
								'table_column_name'  => 'DATE_FORMAT(kmm.delivery_date,"%d-%m-%Y")',
								'table_column'  =>'kmm.delivery_date', 
								'id'  => 'kmm_delivery_date',
								'is_filter'   => true,
								'label' =>'date',
							),							
							array(
								'column_name' =>'Karigar Name',
								'table_column_name'  => 'km.name',
								'table_column'  => 'km.name',
								'id'  => 'km_name',
								'is_filter'   => true,
								'label' =>'text',
							),
							array(
								'column_name' =>'Sub Category',
								'table_column_name'  => 'pm.name',
								'table_column'  =>'pm.name', 
								'id'  => 'pm_name',
								'is_filter'   => true,
								'label' =>'text',
							),													
							array(
								'column_name' =>'Weight Range',
								'table_column_name'=>'',
								'table_column'  => '',
							
							),
							array(
								'column_name' =>'Weight',
								'table_column_name'=>'',
								'table_column'  => '',
							
							),
							array(
								'column_name' =>'Total Weight',
								'table_column_name'=>'',
								'table_column'  => '',
							
							),							
							array(
								'column_name' =>'',
								'table_column_name'=>'',
								'table_column'  => '',
							
							),

				),
					
			'received_orders_by_order_id'=>array(
							array(
								'column_name' =>'#',
								'table_column_name'=>'mo.id',
								'table_column'  => 'mo.id',
							
							),
							array(
								'column_name' =>'Order ID',
								'table_column_name'  => 'mo.id',
								'table_column'  => 'mo.id',
								'id'  => 'k_name',
								'is_filter'   => true,
								'label' =>'text',
							),
							array(
								'column_name' =>'Order Name',
								'table_column_name'  => 'mo.order_name',
								'table_column'  => 'mo.order_namer',
								'id'  => 'mo_order_name',
								'is_filter'   => true,
								'label' =>'text',
							),
							array(
								'column_name' =>'Order Date',
								'table_column_name'  => 'DATE_FORMAT(mo.order_date,"%d-%m-%Y")',
								'table_column'  =>'mo.order_date', 
								'id'  => 'mo_order_date',
								'is_filter'   => true,
								'label' =>'date',
							),																			
							array(
								'column_name' =>'',
								'table_column_name'=>'',
								'table_column'  => '',
							
							)
				),			
		'received_orders'=>array(
							array(
								'column_name' =>'#',
								'table_column_name'=>'mo.id',
								'table_column'  => 'mo.id',
							
							),
							array(
									'column_name' =>'Order ID',
									'table_column_name'  => 'mo.id',
									'table_column'  => 'mo.id',
									'id'  => 'mo_id',
									'is_filter'   => true,
									'label' =>'text',
								),
								array(
									'column_name' =>'Order Date || Time',
									'table_column_name'  => 'DATE_FORMAT(kmm.created_at,"%d-%m-%Y")',
									'table_column'  =>'kmm.created_at', 
									'id'  => 'kmm_created_at',
									'is_filter'   => true,
									'label' =>'date',
								),
								array(
									'column_name' =>'Karigar Name',
									'table_column_name'  => 'km.name',
									'table_column'  => 'km.name',
									'id'  => 'km_name',
									'is_filter'   => true,
									'label' =>'text',
								),
								array(
									'column_name' =>'Product',
									'table_column_name'  => 'pm.name',
									'table_column'  =>'pm.name', 
									'id'  => 'pm_name',
									'is_filter'   => true,
									'label' =>'text',
								),													
								array(
									'column_name' =>'Weight Range',
									'table_column_name'=>'',
									'table_column'  => '',
								
								),
								array(
									'column_name' =>'Order Quantity',
									'table_column_name'=>'',
									'table_column'  => '',
								
								),
								array(
									'column_name' =>'Pending Quantity',
									'table_column_name'=>'',
									'table_column'  => '',
								
								),										

				),	

		'received_orders_bombay'=>array(
							array(
								'column_name' =>'#',
								'table_column_name'=>'mo.id',
								'table_column'  => 'mo.id',
							
							),
							array(
									'column_name' =>'Order ID',
									'table_column_name'  => 'mo.id',
									'table_column'  => 'mo.id',
									'id'  => 'mo_id',
									'is_filter'   => true,
									'label' =>'text',
								),
								array(
									'column_name' =>'Order Date || Time',
									'table_column_name'  => 'DATE_FORMAT(kmm.created_at,"%d-%m-%Y")',
									'table_column'  =>'kmm.created_at', 
									'id'  => 'kmm_created_at',
									'is_filter'   => true,
									'label' =>'date',
								),
								array(
									'column_name' =>'Karigar Name',
									'table_column_name'  => 'km.name',
									'table_column'  => 'km.name',
									'id'  => 'km_name',
									'is_filter'   => true,
									'label' =>'text',
								),
								array(
									'column_name' =>'Product',
									'table_column_name'  => 'pm.name',
									'table_column'  =>'pm.name', 
									'id'  => 'pm_name',
									'is_filter'   => true,
									'label' =>'text',
								),													
								array(
									'column_name' =>'Weight Range',
									'table_column_name'=>'',
									'table_column'  => '',
								
								),
								array(
									'column_name' =>'Order Weight',
									'table_column_name'=>'',
									'table_column'  => '',
								
								),
								array(
									'column_name' =>'Pending Weight',
									'table_column_name'=>'',
									'table_column'  => '',
								
								),										

				),	
		'received_receipt'=>array(
								array(
										'column_name' =>'#',
										'table_column_name'=>'mo.id',
										'table_column'  => 'mo.id',
									
									),
								array(
										'column_name' =>'Order ID',
										'table_column_name'  => 'mo.id',
										'table_column'  => 'mo.id',
										'id'  => 'mo_id',
										'is_filter'   => true,
										'label' =>'text',
									),
								array(
									'column_name' =>'Receipt Code',
									'table_column_name'  => 'rp.receipt_code',
									'table_column'  => 'rp.receipt_code',
									'id'  => 'rp_receipt_code',
									'is_filter'   => true,
									'label' =>'text',
								),
								array(
										'column_name' =>'Order Date',
										'table_column_name'  => 'DATE_FORMAT(mo.order_date,"%d-%m-%Y")',
										'table_column'  =>'mo.order_date', 
										'id'  => 'mo_order_date',
										'is_filter'   => true,
											'label' =>'date',
								),
								array(
										'column_name' =>'Receipt Date',
										'table_column_name'  => 'DATE_FORMAT(rp.date,"%d-%m-%Y")',
										'table_column'  =>'rp.date', 
										'id'  => 'rp_date',
										'is_filter'   => true,
										'label' =>'date',
								),
							/*	array(
									'column_name' =>'Karigar Code',
									'table_column_name'  => 'km.code',
									'table_column'  => 'km.code',
									'id'  => 'km_code',
									'is_filter'   => true,
									'label' =>'text',
								),
								array(
									'column_name' =>'Product',
									'table_column_name'  => 'pm.name',
									'table_column'  =>'pm.name', 
									'id'  => 'pm_name',
									'is_filter'   => true,
									'label' =>'text',
								),*/	
								array(
									'column_name' =>'Gr wt',
									'table_column_name'=>'',
									'table_column'  => '',
								
								),												
								array(
									'column_name' =>'Net wt',
									'table_column_name'=>'',
									'table_column'  => '',
								
								),
								array(
									'column_name' =>'Quantity',
									'table_column_name'=>'',
									'table_column'  => '',
								
								),
								array(
									'column_name' =>'',
									'table_column_name'=>'',
									'table_column'  => '',
								
								),										

				),
		'sending_to_qc'=>array(
								array(
										'column_name' =>'#',
										'table_column_name'=>'mo.id',
										'table_column'  => 'mo.id',
									
									),
								array(
										'column_name' =>'Order ID',
										'table_column_name'  => 'mo.id',
										'table_column'  => 'mo.id',
										'id'  => 'mo_id',
										'is_filter'   => true,
										'label' =>'text',
									),
								array(
									'column_name' =>'Receipt Code',
									'table_column_name'  => 'rp.receipt_code',
									'table_column'  => 'rp.receipt_code',
									'id'  => 'rp_receipt_code',
									'is_filter'   => true,
									'label' =>'text',
								),
								array(
										'column_name' =>'Order Date',
										'table_column_name'  => 'DATE_FORMAT(mo.order_date,"%d-%m-%Y")',
										'table_column'  =>'mo.order_date', 
										'id'  => 'mo_order_date',
										'is_filter'   => true,
											'label' =>'date',
								),
								array(
										'column_name' =>'Receipt Date',
										'table_column_name'  => 'DATE_FORMAT(rp.date,"%d-%m-%Y")',
										'table_column'  =>'rp.date', 
										'id'  => 'rp_date',
										'is_filter'   => true,
										'label' =>'date',
								),
							/*	array(
									'column_name' =>'Karigar Code',
									'table_column_name'  => 'km.code',
									'table_column'  => 'km.code',
									'id'  => 'km_code',
									'is_filter'   => true,
									'label' =>'text',
								),
								array(
									'column_name' =>'Product',
									'table_column_name'  => 'pm.name',
									'table_column'  =>'pm.name', 
									'id'  => 'pm_name',
									'is_filter'   => true,
									'label' =>'text',
								),*/	
								array(
									'column_name' =>'Gr wt',
									'table_column_name'=>'',
									'table_column'  => '',
								
								),												
								array(
									'column_name' =>'Net wt',
									'table_column_name'=>'',
									'table_column'  => '',
								
								),
								array(
									'column_name' =>'Quantity',
									'table_column_name'=>'',
									'table_column'  => '',
								
								),
								array(
									'column_name' =>'',
									'table_column_name'=>'',
									'table_column'  => '',
								
								),										

				),		
		'received_receipt_bom_dept'=>array(
								array(
										'column_name' =>'#',
										'table_column_name'=>'mo.id',
										'table_column'  => 'mo.id',
									
									),
								array(
										'column_name' =>'Order ID',
										'table_column_name'  => 'mo.id',
										'table_column'  => 'mo.id',
										'id'  => 'mo_id',
										'is_filter'   => true,
										'label' =>'text',
									),
								array(
									'column_name' =>'Receipt Code',
									'table_column_name'  => 'rp.receipt_code',
									'table_column'  => 'rp.receipt_code',
									'id'  => 'rp_receipt_code',
									'is_filter'   => true,
									'label' =>'text',
								),
								array(
										'column_name' =>'Order Date',
										'table_column_name'  => 'DATE_FORMAT(mo.order_date,"%d-%m-%Y")',
										'table_column'  =>'mo.order_date', 
										'id'  => 'mo_order_date',
										'is_filter'   => true,
											'label' =>'date',
								),
								array(
										'column_name' =>'Receipt Date',
										'table_column_name'  => 'DATE_FORMAT(rp.date,"%d-%m-%Y")',
										'table_column'  =>'rp.date', 
										'id'  => 'rp_date',
										'is_filter'   => true,
										'label' =>'date',
								),
								// array(
								// 	'column_name' =>'Karigar Code',
								// 	'table_column_name'  => 'km.code',
								// 	'table_column'  => 'km.code',
								// 	'id'  => 'km_code',
								// 	'is_filter'   => true,
								// 	'label' =>'text',
								// ),
								// array(
								// 	'column_name' =>'Product',
								// 	'table_column_name'  => 'pm.name',
								// 	'table_column'  =>'pm.name', 
								// 	'id'  => 'pm_name',
								// 	'is_filter'   => true,
								// 	'label' =>'text',
								// ),
								array(
									'column_name' =>'Gr wt',
									'table_column_name'=>'',
									'table_column'  => '',
								
								),													
								array(
									'column_name' =>'Net wt',
									'table_column_name'=>'',
									'table_column'  => '',
								
								),
								array(
									'column_name' =>'Weight',
									'table_column_name'=>'',
									'table_column'  => '',
								
								),
								array(
									'column_name' =>'',
									'table_column_name'=>'',
									'table_column'  => '',
								
								),										

				),
		'sending_to_qc_bom_dept'=>array(
								array(
										'column_name' =>'#',
										'table_column_name'=>'mo.id',
										'table_column'  => 'mo.id',
									
									),
								array(
										'column_name' =>'Order ID',
										'table_column_name'  => 'mo.id',
										'table_column'  => 'mo.id',
										'id'  => 'mo_id',
										'is_filter'   => true,
										'label' =>'text',
									),
								array(
									'column_name' =>'Receipt Code',
									'table_column_name'  => 'rp.receipt_code',
									'table_column'  => 'rp.receipt_code',
									'id'  => 'rp_receipt_code',
									'is_filter'   => true,
									'label' =>'text',
								),
								array(
										'column_name' =>'Order Date',
										'table_column_name'  => 'DATE_FORMAT(mo.order_date,"%d-%m-%Y")',
										'table_column'  =>'mo.order_date', 
										'id'  => 'mo_order_date',
										'is_filter'   => true,
											'label' =>'date',
								),
								array(
										'column_name' =>'Receipt Date',
										'table_column_name'  => 'DATE_FORMAT(rp.date,"%d-%m-%Y")',
										'table_column'  =>'rp.date', 
										'id'  => 'rp_date',
										'is_filter'   => true,
										'label' =>'date',
								),
								// array(
								// 	'column_name' =>'Karigar Code',
								// 	'table_column_name'  => 'km.code',
								// 	'table_column'  => 'km.code',
								// 	'id'  => 'km_code',
								// 	'is_filter'   => true,
								// 	'label' =>'text',
								// ),
								// array(
								// 	'column_name' =>'Product',
								// 	'table_column_name'  => 'pm.name',
								// 	'table_column'  =>'pm.name', 
								// 	'id'  => 'pm_name',
								// 	'is_filter'   => true,
								// 	'label' =>'text',
								// ),
								array(
									'column_name' =>'Gr wt',
									'table_column_name'=>'',
									'table_column'  => '',
								
								),													
								array(
									'column_name' =>'Net wt',
									'table_column_name'=>'',
									'table_column'  => '',
								
								),
								array(
									'column_name' =>'Weight',
									'table_column_name'=>'',
									'table_column'  => '',
								
								),
								array(
									'column_name' =>'',
									'table_column_name'=>'',
									'table_column'  => '',
								
								),										

				),
		
		'mfg_qc_ctl_reje_bom_orders'=>array(
								array(
										'column_name' =>'#',
										'table_column_name'=>'qc.receipt_code',
										'table_column'  => 'qc.receipt_code',
									
									),
								array(
									'column_name' =>'Receipt Code',
									'table_column_name'  => 'qc.receipt_code',
									'table_column'  => 'qc.receipt_code',
									'id'  => 'qc_receipt_code',
									'is_filter'   => true,
									'label' =>'text',
								),
								array(
										'column_name' =>'Order ID',
										'table_column_name'  => 'mo.id',
										'table_column'  => 'mo.id',
										'id'  => 'mo_id',
										'is_filter'   => true,
										'label' =>'text',
									),
								
								array(
									'column_name' =>'Product',
									'table_column_name'  => 'pm.name',
									'table_column'  =>'pm.name', 
									'id'  => 'pm_name',
									'is_filter'   => true,
									'label' =>'text',
								),													
								array(
									'column_name' =>'Karigar Name',
									'table_column_name'  => 'km.name',
									'table_column'  => 'km.name',
									'id'  => 'km_name',
									'is_filter'   => true,
									'label' =>'text',
								),
							    array(
									'column_name' =>'Weight',
									'table_column_name'=>'',
									'table_column'  => '',
								
								),
								array(
									'column_name' =>'',
									'table_column_name'=>'',
									'table_column'  => '',
								
								),										

				),
		'manufacturing_quality_control_rejected_orders'=>array(
								array(
										'column_name' =>'#',
										'table_column_name'=>'qc.receipt_code',
										'table_column'  => 'qc.receipt_code',
									
									),
								array(
									'column_name' =>'Receipt Code',
									'table_column_name'  => 'qc.receipt_code',
									'table_column'  => 'qc.receipt_code',
									'id'  => 'qc_receipt_code',
									'is_filter'   => true,
									'label' =>'text',
								),
								array(
										'column_name' =>'Order ID',
										'table_column_name'  => 'mo.id',
										'table_column'  => 'mo.id',
										'id'  => 'mo_id',
										'is_filter'   => true,
										'label' =>'text',
									),
								
								array(
									'column_name' =>'Product',
									'table_column_name'  => 'pm.name',
									'table_column'  =>'pm.name', 
									'id'  => 'pm_name',
									'is_filter'   => true,
									'label' =>'text',
								),													
								array(
									'column_name' =>'Karigar Name',
									'table_column_name'  => 'km.name',
									'table_column'  => 'km.name',
									'id'  => 'km_name',
									'is_filter'   => true,
									'label' =>'text',
								),
								array(
									'column_name' =>'Quantity',
									'table_column_name'=>'',
									'table_column'  => '',
								
								),
								array(
									'column_name' =>'Weight',
									'table_column_name'=>'',
									'table_column'  => '',
								
								),
								array(
									'column_name' =>'',
									'table_column_name'=>'',
									'table_column'  => '',
								
								),										

				),
	'manufacturing_quality_control'=>array(
								array(
										'column_name' =>'#',
										'table_column_name'=>'qc.receipt_code',
										'table_column'  => 'qc.receipt_code',
									
									),
								array(
									'column_name' =>'Receipt Code',
									'table_column_name'  => 'qc.receipt_code',
									'table_column'  => 'qc.receipt_code',
									'id'  => 'qc_receipt_code',
									'is_filter'   => true,
									'label' =>'text',
								),
								array(
										'column_name' =>'Order ID',
										'table_column_name'  => 'mo.id',
										'table_column'  => 'mo.id',
										'id'  => 'mo_id',
										'is_filter'   => true,
										'label' =>'text',
									),
								
								array(
									'column_name' =>'Product',
									'table_column_name'  => 'pm.name',
									'table_column'  =>'pm.name', 
									'id'  => 'pm_name',
									'is_filter'   => true,
									'label' =>'text',
								),													
								array(
									'column_name' =>'Karigar Name',
									'table_column_name'  => 'km.name',
									'table_column'  => 'km.name',
									'id'  => 'km_name',
									'is_filter'   => true,
									'label' =>'text',
								),
								array(
									'column_name' =>'Quantity',
									'table_column_name'=>'',
									'table_column'  => '',
								
								),
								array(
									'column_name' =>'Gr. Wt',
									'table_column_name'=>'',
									'table_column'  => '',
								
								),	
								array(
									'column_name' =>'Net. Wt',
									'table_column_name'=>'',
									'table_column'  => '',
								
								),																

				),
		'manufacturing_hallmarking'=>array(
								/*array(
										'column_name' =>'#',
										'type' =>'checkbox',	
										'table_column_name'=>'mo.order_name', 
										'table_column'  => 'mo.order_name',	
									),	*/
								array(
										'column_name' =>'Order Name',
										'table_column_name'  => 'mo.order_name',
										'table_column'  => 'mo.order_name',
										'id'  => 'mo_id',
										'is_filter'   => true,
										'label' =>'text',
									),
								
								array(
									'column_name' =>'Karigar Name',
									'table_column_name'  => 'km.name',
									'table_column'  => 'km.name',
									'id'  => 'km_name',
									'is_filter'   => true,
									'label' =>'text',
								),
								array(
									'column_name' =>'Product',
									'table_column_name'  => 'pm.name',
									'table_column'  =>'pm.name', 
									'id'  => 'pm_name',
									'is_filter'   => true,
									'label' =>'text',
								),
								array(
									'column_name' =>'Receipt Code',
									'table_column_name'  => 'rp.receipt_code',
									'table_column'  =>'rp.receipt_code', 
									'id'  => 'rp_receipt_code',
									'is_filter'   => true,
									'label' =>'text',
								),													
								array(
									'column_name' =>'Quantity',
									'table_column_name'=>'',
									'table_column'  => '',
								
								),
								array(
									'column_name' =>'Weight Range',
									'table_column_name'=>'',
									'table_column'  => '',
								
								),
								array(
									'column_name' =>'Gr Wt',
									'table_column_name'=>'',
									'table_column'  => '',
								
								),
								array(
									'column_name' =>'Net Wt',
									'table_column_name'=>'',
									'table_column'  => '',
								
								),
								array(
									'column_name' =>'Weight',
									'table_column_name'=>'',
									'table_column'  => '',
								
								),							
								array(
									'column_name' =>'HM Send Date || Time',
									'table_column_name'  => 'DATE_FORMAT(qch.created_at,"%d-%m-%Y")',
									'table_column'  =>'qch.created_at', 
									'id'  => 'qch_created_at',
									'is_filter'   => true,
									'label' =>'date',
								),	
								array(
									'column_name' =>'',
									'table_column_name'=>'',
									'table_column'  => '',
								
								),																		

				),
'tag_products'=>array(
								array(
										'column_name' =>'#',
										'type' =>'checkbox',	
										'table_column_name'=>'tg.product_code', 
										'table_column'  => 'tg.product_code',	
									),	
								array(
										'column_name' =>'Product Code',
										'table_column_name'  => 'tg.product_code',
										'table_column'  => 'tg.product_code',
										'id'  => 'tg_product_code',
										'is_filter'   => true,
										'label' =>'text',
									),
								
								array(
									'column_name' =>'Karigar Name',
									'table_column_name'  => 'km.name',
									'table_column'  => 'km.name',
									'id'  => 'km_name',
									'is_filter'   => true,
									'label' =>'text',
								),
								array(
									'column_name' =>'Product',
									'table_column_name'  => 'pm.name',
									'table_column'  =>'pm.name', 
									'id'  => 'pm_name',
									'is_filter'   => true,
									'label' =>'text',
								),
																			
								array(
									'column_name' =>'Quantity',
									'table_column_name'=>'',
									'table_column'  => '',
								
								),
								array(
									'column_name' =>'Gr Wt',
									'table_column_name'=>'',
									'table_column'  => '',
								
								),
							
								array(
									'column_name' =>'Net Wt',
									'table_column_name'=>'',
									'table_column'  => '',
								
								),
								// array(
								// 	'column_name' =>'Pieces',
								// 	'table_column_name'=>'',
								// 	'table_column'  => '',
								
								// ),
								array(
									'column_name' =>'Image',
									'table_column_name'=>'',
									'table_column'  => '',
								
								),								
																									

				),


		'kundan_karigar'=>array(
								/*array(
										'column_name' =>'#',
										'type' =>'checkbox',	
										'table_column_name'=>'km.name ', 
										'table_column'  => 'km.name',	
									),	*/
														
								array(
									'column_name' =>'Karigar Name',
									'table_column_name'  => 'km.name',
									'table_column'  => 'km.name',
									'id'  => 'km_name',
									'is_filter'   => true,
									'label' =>'drop_down',
									'model_name'=>'Karigar_model',
									'function_name' =>'get_all_karigar',
									'default_display' => true
								),
							
																			
								array(
									'column_name' =>'Product Code',
									'table_column_name'=>'tp.product_code',
									'table_column'  => 'tp.product_code',
									'id'  => 'tp_product_code',
									'is_filter'   => true,
									'label' =>'text',
								
								),
								// array(
								// 	'column_name' =>'Pcs',
								// 	'table_column_name'=>'',
								// 	'table_column'  => '',
								
								// ),
								array(
									'column_name' =>'K Gold',
									'table_column_name'=>'',
									'table_column'  => '',
								
								),
								array(
									'column_name' =>'K Pcs',
									'table_column_name'=>'',
									'table_column'  => '',
								
								),
								array(
									'column_name' =>'K @',
									'table_column_name'=>'',
									'table_column'  => '',
								
								),
								array(
									'column_name' =>'K Amt',
									'table_column_name'=>'',
									'table_column'  => '',
								
								),
								array(
									'column_name' =>'Gr Wt.',
									'table_column_name'=>'',
									'table_column'  => '',
								
								),
							
								array(
									'column_name' =>'Net Wt.',
									'table_column_name'=>'',
									'table_column'  => '',
								
								),
							
								array(
									'column_name' =>'',
									'table_column_name'=>'',
									'table_column'  => '',
								
								),
																							

				),
		'kundan_karigar_pending'=>array(
								array(
										'column_name' =>'#',
										'type' =>'checkbox',	
										'table_column_name'=>'km.name ', 
										'table_column'  => 'km.name',	
									),	
														
								array(
									'column_name' =>'Karigar Name',
									'table_column_name'  => 'km.name',
									'table_column'  => 'km.name',
									'id'  => 'km_name',
									'is_filter'   => true,
									'label' =>'text',
								),
							
																			
								array(
									'column_name' =>'Issue Voucher No',
									'table_column_name'=>'po.issue_voucher',
									'table_column'  => 'po.issue_voucher',
									'id'  => 'tp_product_code',
									'is_filter'   => true,
									'label' =>'text',
								
								),
								array(
									'column_name' =>'Quantity',
									'table_column_name'=>'',
									'table_column'  => '',
								
								),
							
								array(
									'column_name' =>'',
									'table_column_name'=>'',
									'table_column'  => '',
								
								),
																							

				),
		'kundan_qc_tbl'=>array(
								array(
										'column_name' =>'#',
										'type' =>'checkbox',	
										'table_column_name'=>'km.name ', 
										'table_column'  => 'km.name',	
									),
														
								array(
									'column_name' =>'Karigar Name',
									'table_column_name'  => 'km.name',
									'table_column'  => 'km.name',
									'id'  => 'km_name',
									'is_filter'   => true,
									'label' =>'text',
								),
								array(
									'column_name' =>'Product Code',
									'table_column_name'=>'tp.product_code',
									'table_column'  => 'tp.product_code',
									'id'  => 'tp_product_code',
									'is_filter'   => true,
									'label' =>'text',
								
								),	
								array(
									'column_name' =>'Product',
									'table_column_name'=>'pm.name',
									'table_column'  => 'pm.name',
									'id'  => 'pm_name',
									'is_filter'   => true,
									'label' =>'text',
								
								),					
								
								array(
									'column_name' =>'Quantity',
									'table_column_name'=>'',
									'table_column'  => '',
								
								),
								array(
									'column_name' =>'Gr Wt',
									'table_column_name'=>'',
									'table_column'  => '',
								
								),
								array(
									'column_name' =>'Net Wt',
									'table_column_name'=>'',
									'table_column'  => '',
								
								),
							
							
								array(
									'column_name' =>'',
									'table_column_name'=>'',
									'table_column'  => '',
								
								),
																							

				),

			'kundan_QC'=>array(
								array(
										'column_name' =>'#',
										'table_column_name'=>'km.name ', 
										'table_column'  => 'km.name',	
									),	
														
								array(
									'column_name' =>'Karigar Name',
									'table_column_name'  => 'km.name',
									'table_column'  => 'km.name',
									'id'  => 'km_name',
									'is_filter'   => true,
									'label' =>'text',
								),
								array(
									'column_name' =>'Product Code',
									'table_column_name'=>'tp.product_code',
									'table_column'  => 'tp.product_code',
									'id'  => 'tp_product_code',
									'is_filter'   => true,
									'label' =>'text',
								
								),
								array(
									'column_name' =>'Product',
									'table_column_name'=>'pm.name',
									'table_column'  => 'pm.name',
									'id'  => 'pm_name',
									'is_filter'   => true,
									'label' =>'text',
								
								),							
								
								array(
									'column_name' =>'Quantity',
									'table_column_name'=>'',
									'table_column'  => '',
								
								),
							
								array(
									'column_name' =>'',
									'table_column_name'=>'',
									'table_column'  => '',
								
								),
																							

				),
		'received_products'=>array(
												
								array(
									'column_name' =>'Karigar Name',
									'table_column_name'  => 'km.name',
									'table_column'  => 'km.name',
									'id'  => 'km_name',
									'is_filter'   => true,
									'label' =>'text',
								),
								array(
									'column_name' =>'Product Code',
									'table_column_name'=>'tp.product_code',
									'table_column'  => 'tp.product_code',
									'id'  => 'tp_product_code',
									'is_filter'   => true,
									'label' =>'text',
								
								),
								array(
									'column_name' =>'Product',
									'table_column_name'=>'pm.name',
									'table_column'  => 'pm.name',
									'id'  => 'pm_name',
									'is_filter'   => true,
									'label' =>'text',
								
								),						
								
								array(
									'column_name' =>'Quantity',
									'table_column_name'=>'',
									'table_column'  => '',
								
								),
								array(
									'column_name' =>'Gr Wt',
									'table_column_name'=>'',
									'table_column'  => '',
								
								),
								array(
									'column_name' =>'Net Wt',
									'table_column_name'=>'',
									'table_column'  => '',
								
								),	
																				

				),

		'manufacturing_ready_product'=>array(
												
									array(
										'column_name' =>'',
										'type' =>'checkbox',
										'table_column_name'=>'drp.chitti_no ', 
										'table_column'  => 'drp.chitti_no ',	
										'id'  => 'chitti_no',
									),	
									array(
									'column_name' =>'Department',
									'table_column_name'=>'d.name',
									'table_column'  => 'd.name',
									'id'  => 'd_name',							
							
								
								),	
														
								array(
									'column_name' =>'Chitti No',
									'table_column_name'=>'drp.chitti_no ', 
									'table_column'  => 'drp.chitti_no ',	
									'id'  => 'drp_chitti_no',
									'is_filter'   => true,
									'label' =>'text',
								
								),
								array(
									'column_name' =>'Karigar Name',
									'table_column_name'  => 'km.name',
									'table_column'  => 'km.name',
									'id'  => 'km_name',
									'is_filter'   => true,
									'label' =>'text',
								),
								array(
									'column_name' =>'Product Code',
									'table_column_name'=>'drp.product_code',
									'table_column'  => 'drp.product_code',
									'id'  => 'product_code',
									'is_filter'   => true,
									'label' =>'text',
								
								),
								array(
									'column_name' =>'Product',
									'table_column_name'=>'drp.product',
									'table_column'  => 'drp.product',
									'id'  => 'product',
									'is_filter'   => true,
									'label' =>'text',
								
								),	
							
																			
								array(
									'column_name' =>'Gross Weight',
									'table_column_name'=>'',
									'table_column'  => '',
								
								),
								array(
									'column_name' =>'Net Weight',
									'table_column_name'=>'',
									'table_column'  => '',
								
								),
								array(
									'column_name' =>'Quantity',
									'table_column_name'=>'',
									'table_column'  => '',
								
								),
							
								/*array(
									'column_name' =>'Image',
									'table_column_name'=>'',
									'table_column'  => '',
								
								),*/
								array(
									'column_name' =>'Date',
									'table_column_name'=>'',
									'table_column'  => '',
								
								),
								array(
									'column_name' =>'',
									'table_column_name'=>'',
									'table_column'  => '',
								
								),
							
																				

				),
		'sales_voucher_return_product'=>array(
												
									array(
										'column_name' =>'',
										'type' =>'checkbox',
										'table_column_name'=>'drp.chitti_no ', 
										'table_column'  => 'drp.chitti_no ',	
										'id'  => 'chitti_no',
									),	
									array(
									'column_name' =>'Department',
									'table_column_name'=>'d.name',
									'table_column'  => 'd.name',
									'id'  => 'd_name',							
							
								
								),	
														
								array(
									'column_name' =>'Chitti No',
									'table_column_name'=>'drp.chitti_no ', 
									'table_column'  => 'drp.chitti_no ',	
									'id'  => 'drp_chitti_no',
									'is_filter'   => true,
									'label' =>'text',
								
								),
								array(
									'column_name' =>'Karigar Name',
									'table_column_name'  => 'km.name',
									'table_column'  => 'km.name',
									'id'  => 'km_name',
									'is_filter'   => true,
									'label' =>'text',
								),
								array(
									'column_name' =>'Product Code',
									'table_column_name'=>'drp.product_code',
									'table_column'  => 'drp.product_code',
									'id'  => 'product_code',
									'is_filter'   => true,
									'label' =>'text',
								
								),
								array(
									'column_name' =>'Product',
									'table_column_name'=>'drp.product',
									'table_column'  => 'drp.product',
									'id'  => 'product',
									'is_filter'   => true,
									'label' =>'text',
								
								),	
							
																			
								array(
									'column_name' =>'Gross Weight',
									'table_column_name'=>'',
									'table_column'  => '',
								
								),
								array(
									'column_name' =>'Net Weight',
									'table_column_name'=>'',
									'table_column'  => '',
								
								),
								array(
									'column_name' =>'Quantity',
									'table_column_name'=>'',
									'table_column'  => '',
								
								),
							
								/*array(
									'column_name' =>'Image',
									'table_column_name'=>'',
									'table_column'  => '',
								
								),*/
								array(
									'column_name' =>'Date',
									'table_column_name'=>'',
									'table_column'  => '',
								
								),
								array(
									'column_name' =>'',
									'table_column_name'=>'',
									'table_column'  => '',
								
								),
							
																				

				),
		'mfg_ready_product_dept_wise'=>array(
												
									array(
										'column_name' =>'',
										'type' =>'checkbox',
										'table_column_name'=>'drp.chitti_no ', 
										'table_column'  => 'drp.chitti_no ',	
										'id'  => 'chitti_no',
									),
									array(
									'column_name' =>'Department',
									'table_column_name'=>'d.name',
									'table_column'  => 'd.name',
									'id'  => 'd_name',			
									),					
								array(
									'column_name' =>'Chitti No',
									'table_column_name'=>'drp.chitti_no ', 
									'table_column'  => 'drp.chitti_no ',	
									'id'  => 'drp_chitti_no',
									'is_filter'   => true,
									'label' =>'text',
								
								),
								array(
									'column_name' =>'Karigar Name',
									'table_column_name'  => 'km.name',
									'table_column'  => 'km.name',
									'id'  => 'km_name',
									'is_filter'   => true,
									'label' =>'text',
								),
							/*	array(
									'column_name' =>'Product Code',
									'table_column_name'=>'drp.product_code',
									'table_column'  => 'drp.product_code',
									'id'  => 'product_code',
									'is_filter'   => true,
									'label' =>'text',
								
								),*/
								array(
									'column_name' =>'Product',
									'table_column_name'=>'drp.product',
									'table_column'  => 'drp.product',
									'id'  => 'product',
									'is_filter'   => true,
									'label' =>'text',
								
								),									
																			
								array(
									'column_name' =>'Gross Weight',
									'table_column_name'=>'',
									'table_column'  => '',
								
								),
								array(
									'column_name' =>'Net Weight',
									'table_column_name'=>'',
									'table_column'  => '',
								
								),
								/*array(
									'column_name' =>'',
									'table_column_name'=>'',
									'table_column'  => '',
								
								),
							*/			
							
							
														
																				

				),

		'mfg_ready_pro_aprv_by_sales_bom'=>array(
												
								array(
										'column_name' =>'#',
										'table_column_name'=>'msqr.make_set_id ', 
										'table_column'  => 'msqr.make_set_id ',	
										'id'  => 'msqr_make_set_id',
									),	
														
								array(
									'column_name' =>'Chitti No',
									'table_column_name'=>'msqr.make_set_id ',
									'table_column'  => 'msqr.make_set_id ',
									'id'  => 'msqr_make_set_id',
									'is_filter'   => true,
									'label' =>'text',
								
								),
								array(
									'column_name' =>'Karigar Name',
									'table_column_name'  => 'km.name',
									'table_column'  => 'km.name',
									'id'  => 'km_name',
									'is_filter'   => true,
									'label' =>'text',
								),
							
								array(
									'column_name' =>'Product',
									'table_column_name'=>'rp.Product_name',
									'table_column'  => 'rp.Product_name',
									'id'  => 'pm_name',
									'is_filter'   => true,
									'label' =>'text',
								
								),	
							
																			
								array(
									'column_name' =>'Weight',
									'table_column_name'=>'',
									'table_column'  => '',
								
								),
								array(
									'column_name' =>'Net Weight',
									'table_column_name'=>'',
									'table_column'  => '',
								
								),								
							
								array(
									'column_name' =>'Image',
									'table_column_name'=>'',
									'table_column'  => '',
								
								),
							
																				

				),
		
		'manufacturing_ready_product_approved_by_sales'=>array(
												
								array(
										'column_name' =>'#',
										'table_column_name'=>'msqr.make_set_id ', 
										'table_column'  => 'msqr.make_set_id ',	
									),	
														
								array(
									'column_name' =>'Chitti No',
									'table_column_name'=>'msqr.make_set_id ',
									'table_column'  => 'msqr.make_set_id ',
									'id'  => 'msqr_make_set_id',
									'is_filter'   => true,
									'label' =>'text',
								
								),
								array(
									'column_name' =>'Karigar Name',
									'table_column_name'  => 'km.name',
									'table_column'  => 'km.name',
									'id'  => 'km_name',
									'is_filter'   => true,
									'label' =>'text',
								),
								array(
									'column_name' =>'Product Code',
									'table_column_name'=>'tp.product_code',
									'table_column'  => 'tp.product_code',
									'id'  => 'tp_product_code',
									'is_filter'   => true,
									'label' =>'text',
								
								),
								array(
									'column_name' =>'Product',
									'table_column_name'=>'pm.name',
									'table_column'  => 'pm.name',
									'id'  => 'pm_name',
									'is_filter'   => true,
									'label' =>'text',
								
								),	
							
																			
								array(
									'column_name' =>'Gross Weight',
									'table_column_name'=>'',
									'table_column'  => '',
								
								),
								array(
									'column_name' =>'Net Weight',
									'table_column_name'=>'',
									'table_column'  => '',
								
								),
								array(
									'column_name' =>'Quantity',
									'table_column_name'=>'',
									'table_column'  => '',
								
								),
							
							/*	array(
									'column_name' =>'Image',
									'table_column_name'=>'',
									'table_column'  => '',
								
								),*/
							
									array(
									'column_name' =>'',
									'table_column_name'=>'',
									'table_column'  => '',
								
								),
							
																				

				),
		'sales_stock_table'=>array(
												
								array(
										'column_name' =>'',
										'type' =>'checkbox',
										'table_column_name'=>'drp.voucher_no', 
										'table_column'  => 'drp.voucher_no ',	
									),	
														
								array(
									'column_name' =>'Chitti No',
									'table_column_name'=>'drp.voucher_no ',
									'table_column'  => 'drp.voucher_no ',
									'id'  => 'drp_voucher_no',
									'is_filter'   => true,
									'label' =>'text',
								
								),
						/*		array(
									'column_name' =>'Karigar Name',
									'table_column_name'  => 'km.name',
									'table_column'  => 'km.name',
									'id'  => 'km_name',
									'is_filter'   => true,
									'label' =>'text',
								),
								array(
									'column_name' =>'Karigar Code',
									'table_column_name'=>'km.code',
									'table_column'  => 'km.code',
									'id'  => 'km_code',
									'is_filter'   => true,
									'label' =>'text',
								
								),*/
								array(
									'column_name' =>'Product Code',
									'table_column_name'=>'drp.product_code',
									'table_column'  => 'drp.product_code',
									'id'  => 'drp_product_code',
									'is_filter'   => true,
									'label' =>'text',
								
								),
								array(
									'column_name' =>'Product',
									'table_column_name'=>'pc.name',
									'table_column'  => 'pc.name',
									'id'  => 'pm_name',
									'is_filter'   => true,
									'label' =>'text',
								
								),	
							
																			
							
								array(
									'column_name' =>'Quantity',
									'table_column_name'=>'ss.created_at',
									'table_column'  => 'ss.created_at',
								
								),
							
								array(
									'column_name' =>'Gross Weight',
									'table_column_name'=>'ss.created_at',
									'table_column'  => 'ss.created_at',
								
								),
								array(
									'column_name' =>'Net Weight',
									'table_column_name'=>'ss.created_at',
									'table_column'  => 'ss.created_at',
								
								),
									array(
									'column_name' =>'Department',
									'table_column_name'=>'lm.name',
									'table_column'  => 'lm.name',
									'id'=>'lm_location_name',
									'is_filter'   => true,
									'label' =>'text',
								
								),
								array(
									'column_name' =>'Date || Time',
									'table_column_name'=>'DATE_FORMAT(ss.created_at,"%d-%m-%Y")',
									'table_column'  => 'ss.created_at',
									'id'=>'ss_created_at',
									'is_filter'   => true,
									'label' =>'date',								
								),
								array(
									'column_name' =>'',
									'table_column_name'=>'',
									'table_column'  => '',
								
								),
																				

				),

		'sales_stock_approve_table'=>array(
												
								array(
										'column_name' =>'',
										'type' =>'',
										'table_column_name'=>'drp.voucher_no', 
										'table_column'  => 'drp.voucher_no ',	
									),	
														
								array(
									'column_name' =>'Chitti No',
									'table_column_name'=>'drp.voucher_no ',
									'table_column'  => 'drp.voucher_no ',
									'id'  => 'drp_voucher_no',
									'is_filter'   => true,
									'label' =>'text',
								
								),
						/*		array(
									'column_name' =>'Karigar Name',
									'table_column_name'  => 'km.name',
									'table_column'  => 'km.name',
									'id'  => 'km_name',
									'is_filter'   => true,
									'label' =>'text',
								),
								array(
									'column_name' =>'Karigar Code',
									'table_column_name'=>'km.code',
									'table_column'  => 'km.code',
									'id'  => 'km_code',
									'is_filter'   => true,
									'label' =>'text',
								
								),*/
								array(
									'column_name' =>'Product Code',
									'table_column_name'=>'drp.product_code',
									'table_column'  => 'drp.product_code',
									'id'  => 'drp_product_code',
									'is_filter'   => true,
									'label' =>'text',
								
								),
								array(
									'column_name' =>'Product',
									'table_column_name'=>'pc.name',
									'table_column'  => 'pc.name',
									'id'  => 'pm_name',
									'is_filter'   => true,
									'label' =>'text',
								
								),	
							
																			
							
								array(
									'column_name' =>'Quantity',
									'table_column_name'=>'ss.created_at',
									'table_column'  => 'ss.created_at',
								
								),
							
								array(
									'column_name' =>'Gross Weight',
									'table_column_name'=>'ss.created_at',
									'table_column'  => 'ss.created_at',
								
								),
								array(
									'column_name' =>'Net Weight',
									'table_column_name'=>'ss.created_at',
									'table_column'  => 'ss.created_at',
								
								),
								array(
									'column_name' =>'Date || Time',
									'table_column_name'=>'DATE_FORMAT(ss.created_at,"%d-%m-%Y")',
									'table_column'  => 'ss.created_at',
									'id'=>'ss_created_at',
									'is_filter'   => true,
									'label' =>'date',								
								),
								array(
									'column_name' =>'',
									'table_column_name'=>'',
									'table_column'  => '',
								
								),
																				

				),
		'sales_stock_table_dep'=>array(
												
								array(
										'column_name' =>'',
										'type' =>'checkbox',
										'table_column_name'=>'drp.voucher_no', 
										'table_column'  => 'drp.voucher_no ',	
									),	
														
								array(
									'column_name' =>'Chitti No',
									'table_column_name'=>'drp.voucher_no ',
									'table_column'  => 'drp.voucher_no ',
									'id'  => 'drp_voucher_no',
									'is_filter'   => true,
									'label' =>'text',
								
								),
						/*		array(
									'column_name' =>'Karigar Name',
									'table_column_name'  => 'km.name',
									'table_column'  => 'km.name',
									'id'  => 'km_name',
									'is_filter'   => true,
									'label' =>'text',
								),
								array(
									'column_name' =>'Karigar Code',
									'table_column_name'=>'km.code',
									'table_column'  => 'km.code',
									'id'  => 'km_code',
									'is_filter'   => true,
									'label' =>'text',
								
								),*/
							/*	array(
									'column_name' =>'Product Code',
									'table_column_name'=>'drp.product_code',
									'table_column'  => 'drp.product_code',
									'id'  => 'drp_product_code',
									'is_filter'   => true,
									'label' =>'text',
								
								),*/
								array(
									'column_name' =>'Product',
									'table_column_name'=>'pc.name',
									'table_column'  => 'pc.name',
									'id'  => 'pm_name',
									'is_filter'   => true,
									'label' =>'text',
								
								),	
							
																			
							
							/*	array(
									'column_name' =>'Quantity',
									'table_column_name'=>'ss.created_at',
									'table_column'  => 'ss.created_at',
								
								),*/
							
								array(
									'column_name' =>'Gross Weight',
									'table_column_name'=>'ss.created_at',
									'table_column'  => 'ss.created_at',
								
								),
								array(
									'column_name' =>'Net Weight',
									'table_column_name'=>'ss.created_at',
									'table_column'  => 'ss.created_at',
								
								),
								array(
									'column_name' =>'Date || Time',
									'table_column_name'=>'DATE_FORMAT(ss.created_at,"%d-%m-%Y")',
									'table_column'  => 'ss.created_at',
									'id'=>'ss_created_at',
									'is_filter'   => true,
									'label' =>'date',								
								),
							
																				

				),			
		'sales_recieved_product_show_table'=>array(
												
									
								array(
											'column_name' =>'#',
											'type' =>'checkbox'	,											
											'table_column_name'=>'pr.product_code', 
											'table_column'  => 'pr.product_code',							
										),
																
								array(
									'column_name' =>'Product Code',
									'table_column_name'=>'pr.product_code',
									'table_column'  => 'pr.product_code',
									'id'  => 'pr_product_code',
									'is_filter'   => true,
									'label' =>'text',
								
								),
								array(
									'column_name' =>'Product',
									'table_column_name'=>'pc.name',
									'table_column'  => 'pc.name',
									'id'  => 'pm_name',
									'is_filter'   => true,
									'label' =>'text',
								
								),	
							
																			
							
								array(
									'column_name' =>'Gross Weight',
									'table_column_name'=>'',
									'table_column'  => '',
								
								),
								array(
									'column_name' =>'Net Weight',
									'table_column_name'=>'',
									'table_column'  => '',
								
								),
								array(
									'column_name' =>'Quantity',
									'table_column_name'=>'',
									'table_column'  => '',
								
								),
								array(
									'column_name' =>'Location',
									'table_column_name'=>'',
									'table_column'  => '',
								
								),
									array(
									'column_name' =>'',
									'table_column_name'=>'',
									'table_column'  => '',
								
								),
							
								
																				

				),	
		'sales_recieved_product_show_table_dep'=>array(
								array(
									'column_name' =>'#',
									'type' =>'checkbox'	,		
									'table_column_name'=>'',
									'table_column'  => '',
								
								),				
									
						
								array(
									'column_name' =>'Product',
									'table_column_name'=>'pc.name',
									'table_column'  => 'pc.name',
									'id'  => 'pm_name',
									'is_filter'   => true,
									'label' =>'text',
								
								),	
							
																			
							
								array(
									'column_name' =>'Gross Weight',
									'table_column_name'=>'',
									'table_column'  => '',
								
								),
								array(
									'column_name' =>'Net Weight',
									'table_column_name'=>'',
									'table_column'  => '',
								
								),
							
								
							
								
																				

				),	
		'sales_recieved_product_show_stock_table'=>array(
												
									
								array(
											'column_name' =>'',
											'type' =>'checkbox',	
											'table_column_name'=>'pr.product_code', 
											'table_column'  => 'pr.product_code',							
										),
								
								array(
									'column_name' =>'Product Code',
									'table_column_name'=>'pr.product_code',
									'table_column'  => 'pr.product_code',
									'id'  => 'pr_product_code',
									'is_filter'   => true,
									'label' =>'text',
								
								),
								array(
									'column_name' =>'Product',
									'table_column_name'=>'pc.name',
									'table_column'  => 'pc.name',
									'id'  => 'pm_name',
									'is_filter'   => true,
									'label' =>'text',
								
								),	
							
																			
							
								array(
									'column_name' =>'Gross Weight',
									'table_column_name'=>'',
									'table_column'  => '',
								
								),
								array(
									'column_name' =>'Net Weight',
									'table_column_name'=>'',
									'table_column'  => '',
								
								),
								array(
									'column_name' =>'Quantity',
									'table_column_name'=>'',
									'table_column'  => '',
								
								),

								array(
									'column_name' =>'Location',
									'table_column_name'=>'',
									'table_column'  => '',
								
								),
								array(
									'column_name' =>'',
									'table_column_name'=>'',
									'table_column'  => '',
								
								),
							
							
								
																				

				),
		'sales_recieved_product_show_stock_table_dep'=>array(
												
									
								array(
											'column_name' =>'',
											'type' =>'checkbox',	
											'table_column_name'=>'pr.product_code', 
											'table_column'  => 'pr.product_code',							
										),
								
							
								array(
									'column_name' =>'Product',
									'table_column_name'=>'pc.name',
									'table_column'  => 'pc.name',
									'id'  => 'pm_name',
									'is_filter'   => true,
									'label' =>'text',
								
								),	
							
																			
							
								array(
									'column_name' =>'Gross Weight',
									'table_column_name'=>'',
									'table_column'  => '',
								
								),
								array(
									'column_name' =>'Net Weight',
									'table_column_name'=>'',
									'table_column'  => '',
								
								),																				

				),
		
		'Manufacturing_Engaged_karigar_list'=>array(		
								array(
									'column_name' =>'Karigar Name',
									'table_column_name'  => 'km.name',
									'table_column'  => 'km.name',
									'id'  => 'km_name',
									'is_filter'   => true,
									'label' =>'text',
								),
								array(
									'column_name' =>'Total Pcs',
									'table_column_name'=>'',
									'table_column'  => '',
									'id'  => '',
								
								),										
								
								
								array(
									'column_name' =>'Received Pcs',
									'table_column_name'=>'',
									'table_column'  => '',
									'id'  => '',
								
								),
								array(
									'column_name' =>'Total Wt',
									'table_column_name'=>'',
									'table_column'  => '',
									'id'  => '',
									
								
								),	
							
																			
							
								array(
									'column_name' =>'Received Wt',
									'table_column_name'=>'',
									'table_column'  => '',
								
								),
								array(
									'column_name' =>'Balance Wt',
									'table_column_name'=>'',
									'table_column'  => '',
								
								),
								array(
									'column_name' =>'',
									'table_column_name'=>'',
									'table_column'  => '',
								
								),
							
								
																				

				),

			'Mfg_Eng_karigar_bom_list'=>array(		
								array(
									'column_name' =>'Karigar Name',
									'table_column_name'  => 'km.name',
									'table_column'  => 'km.name',
									'id'  => 'km_name',
									'is_filter'   => true,
									'label' =>'text',
								),
								
								array(
									'column_name' =>'Total Wt',
									'table_column_name'=>'',
									'table_column'  => '',
									'id'  => '',
									
								
								),											
							
								array(
									'column_name' =>'Received Wt',
									'table_column_name'=>'',
									'table_column'  => '',
								
								),
								array(
									'column_name' =>'Balance Wt',
									'table_column_name'=>'',
									'table_column'  => '',
								
								),
								array(
									'column_name' =>'',
									'table_column_name'=>'',
									'table_column'  => '',
								
								),
							
								
																				

				),
		

'manufacturing_details_of_engaged_karigar_list'=>array(		
								array(
									'column_name' =>'Karigar Name',
									'table_column_name'  => 'km.name',
									'table_column'  => 'km.name',
									'id'  => 'km_name',
									'is_filter'   => true,
									'label' =>'text',
								),
								array(
									'column_name' =>'Product',
									'table_column_name'  => 'pm.name',
									'table_column'  =>'pm.name', 
									'id'  => 'pm_name',
									'is_filter'   => true,
									'label' =>'text',
								),
								array(
									'column_name' =>'Order Id',
									'table_column_name'  => 'mo.id ',
									'table_column'  => 'mo.id ',
									'id'  => 'order_id',
									'is_filter'   => true,
									
									'label' =>'text',
									
									
								),
								array(
									'column_name' =>'Karigar Engaged Date',
									'table_column_name'  => 'DATE_FORMAT(kmm.created_at,"%d-%m-%Y")',
									'table_column'  => 'kmm.created_at',
									'id'  => 'kmm_created_at',
									'is_filter'   => true,
									
									'label' =>'date',
								),
								array(
								'column_name' =>'Proposed delivery date',
								'table_column_name'  => 'DATE_FORMAT(kmm.delivery_date,"%d-%m-%Y")',
								'table_column'  => 'delivery_date',
								'id'  => 'delivery_date',
								'is_filter'   => true,
								
								'label' =>'date',
								),

								array(	
									'column_name' =>'Quantity',
									'table_column'  => '',
									'table_column_name'  => '',
									'table_column'  => 'Quantity',	
									'id'=>'Quantity',
									'label' =>'text',
								),
								array(
									'column_name' =>'Total Weight',
									'table_column_name'  => '',
									'table_column'  => 'received_pcs',
									
									'label' =>'text',
								),
								array(
									'column_name' =>'',
									'table_column_name'=>'',
									'table_column'  => '',
								
								),
							
								
																				

				),

	
	'mfg_details_eng_karigar_bom_list'=>array(		
								array(
									'column_name' =>'Karigar Name',
									'table_column_name'  => 'km.name',
									'table_column'  => 'km.name',
									'id'  => 'km_name',
									'is_filter'   => true,
									'label' =>'text',
								),
								array(
									'column_name' =>'Product',
									'table_column_name'  => 'pm.name',
									'table_column'  =>'pm.name', 
									'id'  => 'pm_name',
									'is_filter'   => true,
									'label' =>'text',
								),
								array(
									'column_name' =>'Order Id',
									'table_column_name'  => 'mo.id ',
									'table_column'  => 'mo.id ',
									'id'  => 'order_id',
									'is_filter'   => true,
									
									'label' =>'text',
									
									
								),
								array(
									'column_name' =>'Karigar Engaged Date',
									'table_column_name'  => 'DATE_FORMAT(kmm.created_at,"%d-%m-%Y")',
									'table_column'  => 'kmm.created_at',
									'id'  => 'kmm_created_at',
									'is_filter'   => true,
									
									'label' =>'date',
								),
								array(
								'column_name' =>'Proposed delivery date',
								'table_column_name'  => 'DATE_FORMAT(kmm.delivery_date,"%d-%m-%Y")',
								'table_column'  => 'delivery_date',
								'id'  => 'delivery_date',
								'is_filter'   => true,
								
								'label' =>'date',
								),

								array(	
									'column_name' =>'Weight',
									'table_column'  => '',
									'table_column_name'  => '',
									'table_column'  => 'Weight',	
									'id'=>'Quantity',
									'label' =>'text',
								),
								array(
									'column_name' =>'Total Weight',
									'table_column_name'  => '',
									'table_column'  => 'received_pcs',
									
									'label' =>'text',
								),
								array(
									'column_name' =>'',
									'table_column_name'=>'',
									'table_column'  => '',
								
								),
							
								
																				

				),
		'accounting'=>array(
								array(
								'column_name' =>'Type',
								'table_column_name'  => 'ac.id',
								'table_column'  => 'ac.id',
								'id'  => 'ac.id',
								
								),
								array(
									'column_name' =>'Voucher No',
									'table_column_name'  => 'ac.id',
									'table_column'  => 'ac.id',
									'id'  => 'ac_id',
									'is_filter'   => true,
									//'source'=>'Products_sent_by_order',
									'label' =>'text',
								),
								
								array(
									'column_name' =>'Proposed Delivery Date',
									'table_column_name'  => 'DATE_FORMAT(ac.voucher_date,"%d-%m-%Y")',
									'table_column'  => 'voucher_date',
									'id'  => 'voucher_date',
									'is_filter'   => true,
									//'source'=>'Products_sent_by_order',
									'label' =>'date',
								),
								array(	
									'column_name' =>'Name',
									'table_column_name'  => 'k.name',
									'table_column'  => 'k.name',
									'id'  => 'name',
									'is_filter'   => true,
									'label' =>'text',
								),
								array(
									'column_name' =>'Gross Weight',
									'table_column_name'=>'',
									'table_column'  => '',
								
								),
								array(
									'column_name' =>'Pure',
									'table_column_name'=>'',
									'table_column'  => '',
								
								),
								array(
									'column_name' =>'',
									'table_column_name'=>'',
									'table_column'  => '',
								
								),
						),

			'user_access'=> array(
								
								array(
									'column_name' =>'Name',
									'table_column_name'  => 'a.email',
									'table_column'  => 'a.email',
									'id'  => 'name',
									'is_filter'   => true,									
									'label' =>'text',
									
								),
								array(
									'column_name' =>'Email',
									'table_column_name'  => 'a.email',
									'table_column'  => 'a.email',
									'id'  => 'email',
									'is_filter'   => true,									
									'label' =>'text',
									
								),
								array(
									'column_name' =>'Access Modules',
									'table_column_name'  => '',
									'table_column'  => '',
								
									
								),
								
																																
								array(
									'column_name' =>'',	
									'table_column_name'=>'',
									'table_column'  => '',								
								)
												

					),
		'sales_prepared_order_table'=>array(
								array(
									'column_name' =>'#',	
									'table_column_name'  => 'mo.id',
									'table_column'  => 'mo.id',							
								),
												
								array(
									'column_name' =>'Order Id',
									'table_column_name'  => 'mo.id',
									'table_column'  => 'mo.id',
									'id'  => 'mo_id',
									'is_filter'   => true,
									'label' =>'text',
								),
								array(
									'column_name' =>'Order Name',
									'table_column_name'=>'mo.order_name',
									'table_column'  => 'mo.order_name',
									'id'  => 'mo_order_name',
									'is_filter'   => true,
									'label' =>'text',
								
								),
								array(
									'column_name' =>'Department Name',
									'table_column_name'=>'d.name',
									'table_column'  => 'd.name',
									'id'  => 'pm_name',
									'is_filter'   => true,
									'label' =>'text',
								
								),					
								
								array(
									'column_name' =>'Categories',
									'table_column_name'=>'',
									'table_column'  => '',
								
								),
								array(
									'column_name' =>'Total Quantity',
									'table_column_name'=>'',
									'table_column'  => '',
								
								),
								array(
									'column_name' =>'',
									'table_column_name'=>'',
									'table_column'  => '',
								
								),												

				),	
	'department_wise_view'=>array(
								array(
									'column_name' =>'',
									'type' =>'checkbox',	
									'table_column_name'=>'pm.name', 
									'table_column'  => 'pm.name',	
								),	
								array(
									'column_name' =>'#',	
									'table_column_name'=>'pm.name', 
									'table_column'  => 'pm.name',	
								),
								array(	
									'column_name' =>'Order ID',
									'table_column_name'  => 'pm.name',
									'table_column'  => 'pm.name',
									
								),	
								array(	
									'column_name' =>'Order Date',
									'table_column_name'  => 'pm.name',
									'table_column'  => 'pm.name',
									
								),
								array(
									'column_name' =>'Product',
									'table_column_name'  => 'pm.name',
									'table_column'  => 'pm.name',
									'id'  => 'pm_name',
									'is_filter'   => true,
									'label' =>'text',
								),
								array(
									'column_name' =>'Weight Range',
									'table_column_name'=>'',
									'table_column'  => '',	
									
								),
							
								array(	
									'column_name' =>'Total Weight',
									'table_column_name'=>'',
									'table_column'  => '',	
									
								),										
					
								array(	
									'column_name' =>'Assign Weight',
									'table_column_name'=>'',
									'table_column'  => '',	
									
								),
								array(	
									'column_name' =>'Status',	
									'table_column_name'=>'',
									'table_column'  => '',										
								),
								array(
									'column_name' =>'Image',
									'table_column_name'=>'',
									'table_column'  => '',
								
								),
								array(	
									'column_name' =>'Action',	
									'table_column_name'=>'',
									'table_column'  => '',										
								),
		),
	'department_mangalsutra_view'=>array(
								array(
									'column_name' =>'',
									'type' =>'checkbox',	
									'table_column_name'=>'pm.name', 
									'table_column'  => 'pm.name',	
								),	
								array(
									'column_name' =>'#',	
									'table_column_name'=>'pm.name', 
									'table_column'  => 'pm.name',	
								),
								array(	
									'column_name' =>'Order ID',
									'table_column_name'  => 'pm.name',
									'table_column'  => 'pm.name',
									
								),	
								array(	
									'column_name' =>'Order Date',
									'table_column_name'  => 'pm.name',
									'table_column'  => 'pm.name',
									
								),
								array(
									'column_name' =>'Product',
									'table_column_name'  => 'pm.name',
									'table_column'  => 'pm.name',
									'id'  => 'pm_name',
									'is_filter'   => true,
									'label' =>'text',
								),
								array(
									'column_name' =>'Weight Range',
									'table_column_name'=>'',
									'table_column'  => '',	
									
								),
								array(
									'column_name' =>'Total Weight',
									'table_column_name'=>'',
									'table_column'  => '',	
									
								),
								array(	
									'column_name' =>'Total Quantity',
									'table_column_name'=>'',
									'table_column'  => '',	
									
								),										
								array(
									'column_name' =>'Assign Quantity',
									'table_column_name'=>'',
									'table_column'  => '',									
								),
								
								array(	
									'column_name' =>'Assign Weight',
									'table_column_name'=>'',
									'table_column'  => '',	
									
								),
								array(	
									'column_name' =>'Status',	
									'table_column_name'=>'',
									'table_column'  => '',										
								),
								array(
									'column_name' =>'Image',
									'table_column_name'=>'',
									'table_column'  => '',
								
								),
								array(	
									'column_name' =>'Action',	
									'table_column_name'=>'',
									'table_column'  => '',										
								),
		),

		'department_all_view'=>array(
								array(
									'column_name' =>'',
									'type' =>'checkbox',	
									'table_column_name'=>'pm.name', 
									'table_column'  => 'pm.name',	
								),	
								array(
									'column_name' =>'#',	
									'table_column_name'=>'pm.name', 
									'table_column'  => 'pm.name',	
								),
								array(	
									'column_name' =>'Order ID',
									'table_column_name'  => 'pm.name',
									'table_column'  => 'pm.name',
									
								),	
								array(	
									'column_name' =>'Order Date',
									'table_column_name'  => 'pm.name',
									'table_column'  => 'pm.name',
									
								),
								array(
									'column_name' =>'Product',
									'table_column_name'  => 'pm.name',
									'table_column'  => 'pm.name',
									'id'  => 'pm_name',
									'is_filter'   => true,
									'label' =>'text',
								),
								array(
									'column_name' =>'Weight Range',
									'table_column_name'=>'',
									'table_column'  => '',	
									
								),
								array(
									'column_name' =>'Gross Wt',
									'table_column_name'=>'',
									'table_column'  => '',	
									
								),
								array(	
									'column_name' =>'Total Quantity',
									'table_column_name'=>'',
									'table_column'  => '',	
									
								),										
								array(
									'column_name' =>'Assign Quantity',
									'table_column_name'=>'',
									'table_column'  => '',									
								),
								
								array(	
									'column_name' =>'Assign Weight',
									'table_column_name'=>'',
									'table_column'  => '',	
									
								),
								array(	
									'column_name' =>'Status',	
									'table_column_name'=>'',
									'table_column'  => '',										
								),
								array(
									'column_name' =>'Image',
									'table_column_name'=>'',
									'table_column'  => '',
								
								),
								array(	
									'column_name' =>'Action',	
									'table_column_name'=>'',
									'table_column'  => '',										
								),
		),
		'viewProduct'=>array(
									
								array(
									'column_name' =>'#',	
									'table_column_name'=>'p.created_at', 
									'table_column'  => 'p.created_at',	
								),	
								array(
									'column_name' =>'Date || Time',
									'table_column_name'  => 'DATE_FORMAT(p.created_at,"%d-%m-%Y")',
									'table_column'  => 'created_at',
									'id'  => 'created_at',
									'is_filter'   => true,
									//'source'=>'Products_sent_by_order',
									'label' =>'date',
								),
								
								array(
									'column_name' =>'Code',
									'table_column_name'  => 'p.product_code',
									'table_column'  => 'p.product_code',
									'id'  => 'product_code',
									'is_filter'   => true,
									'label' =>'text',
								),
								array(
									'column_name' =>'Karigar',
									'table_column_name'  => 'k.name',
									'table_column'  => 'k.name',
									'id'  => 'k_name',
									'is_filter'   => true,
									'label' =>'text',
								),
								array(
									'column_name' =>'Size',
									'table_column_name'=>'',
									'table_column'  => '',	
									
								),
								array(
									'column_name' =>'Metal',
									'table_column_name'=>'',
									'table_column'  => '',	
									
								),
								array(	
									'column_name' =>'Category',
									'table_column_name'=>'',
									'table_column'  => '',	
									
								),										
								array(
									'column_name' =>'Sub Category',
									'table_column_name'=>'',
									'table_column'  => '',									
								),
								
								array(	
									'column_name' =>'Carat',
									'table_column_name'=>'',
									'table_column'  => '',	
									
								),
								array(	
									'column_name' =>'WT Range',	
									'table_column_name'=>'',
									'table_column'  => '',										
								),
								array(
									'column_name' =>'Image',
									'table_column_name'=>'',
									'table_column'  => '',
								
								),
								array(	
									'column_name' =>'',	
									'table_column_name'=>'',
									'table_column'  => '',										
								),
		),
		'viewProductwithoutimg'=>array(
									
								array(
									'column_name' =>'#',	
									'table_column_name'=>'p.created_at', 
									'table_column'  => 'p.created_at',	
								),	
								array(
									'column_name' =>'Date || Time',
									'table_column_name'  => 'DATE_FORMAT(p.created_at,"%d-%m-%Y")',
									'table_column'  => 'created_at',
									'id'  => 'created_at',
									'is_filter'   => true,
									//'source'=>'Products_sent_by_order',
									'label' =>'date',
								),
								
								array(
									'column_name' =>'Code',
									'table_column_name'  => 'p.product_code',
									'table_column'  => 'p.product_code',
									'id'  => 'product_code',
									'is_filter'   => true,
									'label' =>'text',
								),
								array(
									'column_name' =>'Karigar',
									'table_column_name'  => 'k.name',
									'table_column'  => 'k.name',
									'id'  => 'k_name',
									'is_filter'   => true,
									'label' =>'text',
								),
								array(
									'column_name' =>'Size',
									'table_column_name'=>'',
									'table_column'  => '',	
									
								),
								array(
									'column_name' =>'Metal',
									'table_column_name'=>'',
									'table_column'  => '',	
									
								),
								array(	
									'column_name' =>'Category',
									'table_column_name'=>'',
									'table_column'  => '',	
									
								),										
								array(
									'column_name' =>'Sub Category',
									'table_column_name'=>'',
									'table_column'  => '',									
								),
								
								array(	
									'column_name' =>'Carat',
									'table_column_name'=>'',
									'table_column'  => '',	
									
								),
								array(	
									'column_name' =>'WT Range',	
									'table_column_name'=>'',
									'table_column'  => '',										
								),
								
								array(	
									'column_name' =>'',	
									'table_column_name'=>'',
									'table_column'  => '',										
								),
		),
		'sales_recieved_product_table'=>array(
									
								array(
									'column_name' =>'',
									'type'=>'checkbox',
									'table_column_name'=>'pr.voucher_no', 
									'table_column'  => 'pr.voucher_no',	
								),	
								/*array(
									'column_name' =>'Chitti No',
									'table_column_name'  => 'pr.chitti_no',
									'table_column'  => 'pr.chitti_no',
									'id'  => 'pr_chitti_no',
									'is_filter'   => true,
									'label' =>'text',
								),*/
								array(
									'column_name' =>'Voucher No',
									'table_column_name'  => 'pr.voucher_no',
									'table_column'  => 'pr.voucher_no',
									'id'  => 'pr_voucher_no',
									'is_filter'   => true,
									'label' =>'text',
								),

								array(
									'column_name' =>'Total Qty',
									'table_column_name'=>'',
									'table_column'  => '',	
									
								),
								array(
									'column_name' =>'Gross Weight',
									'table_column_name'=>'',
									'table_column'  => '',	
									
								),
								array(	
									'column_name' =>'Date',
									'table_column_name'=>'',
									'table_column'  => '',	
									
								),									
								array(	
									'column_name' =>'',	
									'table_column_name'=>'',
									'table_column'  => '',										
								),
		),
		'sales_recieved_product_table_dep'=>array(
									
								array(
									'column_name' =>'',
									'type'=>'checkbox',
									'table_column_name'=>'pr.voucher_no', 
									'table_column'  => 'pr.voucher_no',	

								),	
								array(
									'column_name' =>'Voucher No',
									'table_column_name'  => 'pr.voucher_no',
									'table_column'  => 'pr.voucher_no',
									'id'  => 'pr_voucher_no',
									'is_filter'   => true,
									'label' =>'text',
								),
								
								array(
									'column_name' =>'Gross Weight',
									'table_column_name'=>'',
									'table_column'  => '',	
									
								),
								array(	
									'column_name' =>'Date',
									'table_column_name'=>'',
									'table_column'  => '',	
									
								),									
								array(	
									'column_name' =>'',	
									'table_column_name'=>'',
									'table_column'  => '',										
								),
		),
		'sales_returned_product_table'=>array(
									
								array(
									'column_name' =>'#',	
									'table_column_name'=>'pr.chitti_no', 
									'table_column'  => 'pr.chitti_no',	
								),	
								array(
									'column_name' =>'Chitti No',
									'table_column_name'  => 'pr.chitti_no',
									'table_column'  => 'pr.chitti_no',
									'id'  => 'pr_chitti_no',
									'is_filter'   => true,
									'label' =>'text',
								),
								array(
									'column_name' =>'Rejected From Chitti No',
									'table_column_name'=>'',
									'table_column'  => '',	
									
								),
								array(
									'column_name' =>'Total Qty',
									'table_column_name'=>'',
									'table_column'  => '',	
									
								),
								array(
									'column_name' =>'Gross Weight',
									'table_column_name'=>'',
									'table_column'  => '',	
									
								),
								array(	
									'column_name' =>'Date',
									'table_column_name'=>'',
									'table_column'  => '',	
									
								),									
								array(	
									'column_name' =>'',	
									'table_column_name'=>'',
									'table_column'  => '',										
								),
		),
		'sales_returned_product_table_dep'=>array(
									
								array(
									'column_name' =>'#',	
									'table_column_name'=>'pr.chitti_no', 
									'table_column'  => 'pr.chitti_no',	
								),	
								array(
									'column_name' =>'Chitti No',
									'table_column_name'  => 'pr.chitti_no',
									'table_column'  => 'pr.chitti_no',
									'id'  => 'pr_chitti_no',
									'is_filter'   => true,
									'label' =>'text',
								),
								array(
									'column_name' =>'Rejected From Chitti No',
									'table_column_name'=>'',
									'table_column'  => '',	
									
								),
								
								array(
									'column_name' =>'Gross Weight',
									'table_column_name'=>'',
									'table_column'  => '',	
									
								),
								array(	
									'column_name' =>'Date',
									'table_column_name'=>'',
									'table_column'  => '',	
									
								),									
								array(	
									'column_name' =>'',	
									'table_column_name'=>'',
									'table_column'  => '',										
								),
		),
		'dispatch_invoice_table'=>array(
									
								array(
									'column_name' =>'#',	
									'table_column_name'=>'si.id', 
									'table_column'  => 'si.id',	
								),	
								array(
									'column_name' =>'Chitti No',
									'table_column_name'  => 'si.id',
									'table_column'  => 'si.id',
									'id'  => 'si_id',
									'is_filter'   => true,
									'label' =>'text',
								),
								array(
									'column_name' =>'Products',
									'table_column_name'=>'',
									'table_column'  => '',	
									
								),
								array(
									'column_name' =>'Customer Name',
									'table_column_name'=>'',
									'table_column'  => '',	
									
								),
								array(
									'column_name' =>'Color Stone',
									'table_column_name'=>'',
									'table_column'  => '',	
									
								),
								array(	
									'column_name' =>'Kundan Category',
									'table_column_name'=>'',
									'table_column'  => '',	
									
								),									
								array(	
									'column_name' =>'Gross Weight',	
									'table_column_name'=>'',
									'table_column'  => '',										
								),
								array(	
									'column_name' =>'Total Amount',	
									'table_column_name'=>'',
									'table_column'  => '',										
								),
								array(	
									'column_name' =>'Date',	
									'table_column_name'=>'',
									'table_column'  => '',										
								),
								array(	
									'column_name' =>'',	
									'table_column_name'=>'',
									'table_column'  => '',										
								),
		),
		'dispatch_hallmarking_table'=>array(
									
								array(
									'column_name' =>'#',	
									'table_column_name'=>'sid.product_code', 
									'table_column'  => 'sid.product_code',	
								),	
								array(
									'column_name' =>'Item No',
									'table_column_name'  => 'sid.product_code',
									'table_column'  => 'sid.product_code',
									'id'  => 'sid_product_code',
									'is_filter'   => true,
									'label' =>'text',
								),
								array(
									'column_name' =>'Quantity',
									'table_column_name'=>'',
									'table_column'  => '',	
									
								),
								array(
									'column_name' =>'Hallmarking Center',
									'table_column_name'=>'',
									'table_column'  => '',	
									
								),
								array(
									'column_name' =>'Invoice No',
									'table_column_name'=>'',
									'table_column'  => '',	
									
								),
								array(	
									'column_name' =>'Total Weight',
									'table_column_name'=>'',
									'table_column'  => '',	
									
								),									
								array(	
									'column_name' =>'Person Name',	
									'table_column_name'=>'',
									'table_column'  => '',										
								),
								
								array(	
									'column_name' =>'',	
									'table_column_name'=>'',
									'table_column'  => '',										
								),
		),
		'dispatch_hallmarking_rec_table'=>array(
									
								array(
									'column_name' =>'#',	
									'table_column_name'=>'sid.product_code', 
									'table_column'  => 'sid.product_code',	
								),	
								array(
									'column_name' =>'Item No',
									'table_column_name'  => 'sid.product_code',
									'table_column'  => 'sid.product_code',
									'id'  => 'sid_product_code',
									'is_filter'   => true,
									'label' =>'text',
								),
								array(
									'column_name' =>'Quantity',
									'table_column_name'=>'',
									'table_column'  => '',	
									
								),
								array(
									'column_name' =>'Hallmarking Center',
									'table_column_name'=>'',
									'table_column'  => '',	
									
								),
								array(
									'column_name' =>'Invoice No',
									'table_column_name'=>'',
									'table_column'  => '',	
									
								),
								array(	
									'column_name' =>'Recieved Weight',
									'table_column_name'=>'',
									'table_column'  => '',	
									
								),									
								array(	
									'column_name' =>'Amount Per Qty',	
									'table_column_name'=>'',
									'table_column'  => '',										
								),
								
								array(	
									'column_name' =>'Total Amount',	
									'table_column_name'=>'',
									'table_column'  => '',										
								),
								array(	
									'column_name' =>'',	
									'table_column_name'=>'',
									'table_column'  => '',										
								),
		),
		'dispatch_products_table'=>array(
									
								array(
									'column_name' =>'#',	
									'table_column_name'=>'sid.product_code', 
									'table_column'  => 'sid.product_code',	
								),	
								array(
									'column_name' =>'Order Id',
									'table_column_name'  => 'sid.product_code',
									'table_column'  => 'sid.product_code',
									'id'  => 'sid_product_code',
									'is_filter'   => true,
									'label' =>'text',
								),
								array(
									'column_name' =>'Product Code',
									'table_column_name'=>'',
									'table_column'  => '',	
									
								),
								array(
									'column_name' =>'Quantity',
									'table_column_name'=>'',
									'table_column'  => '',	
									
								),
								array(
									'column_name' =>'Hallmarking Done',
									'table_column_name'=>'',
									'table_column'  => '',	
									
								),
								array(	
									'column_name' =>'Dispatch Mode',
									'table_column_name'=>'',
									'table_column'  => '',	
									
								),									
								array(	
									'column_name' =>'Dispatch Data',	
									'table_column_name'=>'',
									'table_column'  => '',										
								),
								
								
		),
		'issue_voucher'=>array(
									
								array(
									'column_name' =>'#',	
									'table_column_name'=>'miv.id', 
									'table_column'  => 'miv.id',	
								),	
								array(
									'column_name' =>'Voucher No',
									'table_column_name'  => 'miv.id',
									'table_column'  => 'miv.id',
									'id'  => 'miv_id',
									'is_filter'   => true,
									'label' =>'text',
								),
								array(
									'column_name' =>'Date',
									'table_column_name'=>'',
									'table_column'  => '',	
									
								),
								array(
									'column_name' =>'Party Name',
									'table_column_name'=>'',
									'table_column'  => '',	
									
								),													
								array(	
									'column_name' =>'',	
									'table_column_name'=>'',
									'table_column'  => '',										
								),
								
								
		),
		'metal_issue_voucher'=>array(
									
								array(
									'column_name' =>'#',	
									'table_column_name'=>'mei.id', 
									'table_column'  => 'mei.id',	
								),	
								array(
									'column_name' =>'Voucher No',
									'table_column_name'  => 'mei.id',
									'table_column'  => 'mei.id',
									'id'  => 'mei_party_name',
									'is_filter'   => true,
									'label' =>'text',
								),
								array(
									'column_name' =>'Date',
									'table_column_name'=>'',
									'table_column'  => '',	
									
								),
								array(
									'column_name' =>'Party Name',
									'table_column_name'=>'',
									'table_column'  => '',	
									
								),													
								array(	
									'column_name' =>'',	
									'table_column_name'=>'',
									'table_column'  => '',										
								),
								
								
		),

		'cash_bank_voucher'=>array(
									
								array(
									'column_name' =>'#',	
									'table_column_name'=>'mcv.party_name', 
									'table_column'  => 'mcv.party_name',	
								),	
								array(
									'column_name' =>'Party Name',
									'table_column_name'  => 'mcv.party_name',
									'table_column'  => 'mcv.party_name',
									'id'  => 'mcv_party_name',
									'is_filter'   => true,
									'label' =>'text',
								),
								array(
									'column_name' =>'Date',
									'table_column_name'=>'',
									'table_column'  => '',	
									
								),
								array(
									'column_name' =>'Bank Name',
									'table_column_name'=>'',
									'table_column'  => '',	
									
								),													
								array(	
									'column_name' =>'Amount',	
									'table_column_name'=>'',
									'table_column'  => '',										
								),
								array(	
									'column_name' =>'',	
									'table_column_name'=>'',
									'table_column'  => '',										
								),
								
								
		),
		'order_status_report_table'=>array(
									
								array(
									'column_name' =>'#',	
									'table_column_name'=>'mo.order_name', 
									'table_column'  => 'mo.order_name',	
								),	
								array(
									'column_name' =>'Order Name',
									'table_column_name'  => 'mo.order_name',
									'table_column'  => 'mo.order_name',
									'id'  => 'mo_order_name',
									'is_filter'   => true,
									'label' =>'text',
								),
								array(
									'column_name' =>'Order ID',
									'table_column_name'=>'',
									'table_column'  => '',	
									
								),
								array(
									'column_name' =>'Order Weight',
									'table_column_name'=>'',
									'table_column'  => '',	
									
								),													
								array(	
									'column_name' =>'Total Pcs',	
									'table_column_name'=>'',
									'table_column'  => '',										
								),
								array(	
									'column_name' =>'',	
									'table_column_name'=>'',
									'table_column'  => '',										
								),
								
								
		),
		'karigar_report_table'=>array(
									
								array(
									'column_name' =>'#',	
									'table_column_name'=>'km.name', 
									'table_column'  => 'km.name',	
								),	
								array(
									'column_name' =>'Karigar Name',
									'table_column_name'  => 'km.name',
									'table_column'  => 'km.name',
									'id'  => 'km_name',
									'is_filter'   => true,
									'label' =>'text',
								),
								
								
								array(
									'column_name' =>'>Order Weight',
									'table_column_name'=>'',
									'table_column'  => '',	
									
								),													
								array(	
									'column_name' =>'Total Pcs',	
									'table_column_name'=>'',
									'table_column'  => '',										
								),							
								
								
		),

		'karigar_engaged_orders'=>array(
									
								array(
									'column_name' =>'#',	
									'table_column_name'=>'co.id', 
									'table_column'  => 'co.id',	
								),	
								array(
									'column_name' =>'Order Number',
									'table_column_name'  => 'co.id',
									'table_column'  => 'co.id',
									'id'  => 'co_id',
									'is_filter'   => true,
									'label' =>'text',
								),
								
								
								array(
									'column_name' =>'Name',
									'table_column_name'=>'co.order_name',
									'table_column'  => 'co.order_name',
									'id'  => 'co_order_name',
									'is_filter'   => true,
									'label' =>'text',	
									
								),													
								array(	
									'column_name' =>'Date',	
									'table_column_name'=>'',
									'table_column'  => '',										
								),	
								array(	
									'column_name' =>'Customer Name',	
									'table_column_name'=>'',
									'table_column'  => '',										
								),							
								
								array(	
									'column_name' =>'category',	
									'table_column_name'=>'',
									'table_column'  => '',										
								),							
								
								array(	
									'column_name' =>'Dept.',	
									'table_column_name'=>'',
									'table_column'  => '',										
								),							
								
								array(	
									'column_name' =>'Quantity',	
									'table_column_name'=>'',
									'table_column'  => '',										
								),		
								array(	
									'column_name' =>'',	
									'table_column_name'=>'',
									'table_column'  => '',										
								),	
						),			


'order_tracking_table'=> array(
								array(
									'column_name' =>'#',	
									'table_column_name'=>'cp.order_id',
									'table_column'  => 'cp.order_id',								
								),
								
								array(
									'column_name' =>'Type',
									'table_column_name'  => 'cp.order_id',
									'table_column'  => 'cp.order_id',
									
								),
								
								array(
									'column_name' =>'Name',
									'table_column_name'  => 'cp.name',
									'table_column'  => 'cp.name',
									'id'  => 'name',
									'is_filter'   => true,									
									'label' =>'text',								
								),
								array(	
									'column_name' =>'Issue Weight',
									'table_column'  => '',										
								),
								array(
									'column_name' =>'Receipt Weight',	
									'table_column'  => '',									
								),	
								array(
									'column_name' =>'Balance Weight',	
									'table_column'  => '',									
								),							
													

						),	


		'manufacturing_repair_bgtrios'=>array(
												
								array(
										'column_name' =>'#',
										'table_column_name'=>'product_code', 
										'table_column'  => 'product_code',	
									),	
														
							
								array(
									'column_name' =>'Product Code',
									'table_column_name'=>'product_code',
									'table_column'  => 'product_code',
									'id'  => 'product_code',
									'is_filter'   => true,
									'label' =>'text',
								
								),
								array(
									'column_name' =>'Qty',
									'table_column_name'=>'',
									'table_column'  => '',
								
								),
																											
							
								array(
									'column_name' =>'Nt Wt',
									'table_column_name'=>'',
									'table_column'  => '',
								
								),
								
								array(
									'column_name' =>'Gr WT',
									'table_column_name'=>'',
									'table_column'  => '',
								
								),
								
								array(
									'column_name' =>'',
									'table_column_name'=>'',
									'table_column'  => '',
								
								),					
													
															
																				

				),						

		'manufacturing_repair_product'=>array(
												
								array(
										'column_name' =>'#',
										'table_column_name'=>'product_code', 
										'table_column'  => 'product_code',	
									),	
														
							
								array(
									'column_name' =>'Product Code',
									'table_column_name'=>'product_code',
									'table_column'  => 'product_code',
									'id'  => 'product_code',
									'is_filter'   => true,
									'label' =>'text',
								
								),
								array(
									'column_name' =>'Qty',
									'table_column_name'=>'',
									'table_column'  => '',
								
								),
																											
							
								array(
									'column_name' =>'Nt Wt',
									'table_column_name'=>'',
									'table_column'  => '',
								
								),
								array(
									'column_name' =>'few Wt',
									'table_column_name'=>'',
									'table_column'  => '',
								
								),
								array(
									'column_name' =>'K pure',
									'table_column_name'=>'',
									'table_column'  => '',
								
								),
								array(
									'column_name' =>'M Wt',
									'table_column_name'=>'',
									'table_column'  => '',
								
								),
								array(
									'column_name' =>'Wax',
									'table_column_name'=>'',
									'table_column'  => '',
								
								),	
								array(
									'column_name' =>'C/S Wt',
									'table_column_name'=>'',
									'table_column'  => '',
								
								),
								array(
									'column_name' =>'Moti Wt',
									'table_column_name'=>'',
									'table_column'  => '',
								
								),
								array(
									'column_name' =>'CH Wt',
									'table_column_name'=>'',
									'table_column'  => '',
								
								),
								array(
									'column_name' =>'Gr WT',
									'table_column_name'=>'',
									'table_column'  => '',
								
								),
								array(
									'column_name' =>'CH PCS',
									'table_column_name'=>'',
									'table_column'  => '',
								
								),
								array(
									'column_name' =>'CH@',
									'table_column_name'=>'',
									'table_column'  => '',
								
								),
								array(
									'column_name' =>'CH amt',
									'table_column_name'=>'',
									'table_column'  => '',
								
								),
								array(
									'column_name' =>'Kun Pc',
									'table_column_name'=>'',
									'table_column'  => '',
								
								),
								array(
									'column_name' =>'Kun@',
									'table_column_name'=>'',
									'table_column'  => '',
								
								),
								array(
									'column_name' =>'Kun Amt',
									'table_column_name'=>'',
									'table_column'  => '',
								
								),
								array(
									'column_name' =>'stn wt',
									'table_column_name'=>'',
									'table_column'  => '',
								
								),
								array(
									'column_name' =>'stn@',
									'table_column_name'=>'',
									'table_column'  => '',
								
								),
								array(
									'column_name' =>'C/S Amt',
									'table_column_name'=>'',
									'table_column'  => '',
								
								),
								array(
									'column_name' =>'Oth Amt',
									'table_column_name'=>'',
									'table_column'  => '',
								
								),
								array(
									'column_name' =>'Ttl Amt',
									'table_column_name'=>'',
									'table_column'  => '',
								
								),
								array(
									'column_name' =>'',
									'table_column_name'=>'',
									'table_column'  => '',
								
								),					
													
															
																				

				),
		'manufacturing_repair_mangalsutra'=>array(
												
								array(
										'column_name' =>'#',
										'table_column_name'=>'product_code', 
										'table_column'  => 'product_code',	
									),	
														
							
								array(
									'column_name' =>'Product Code',
									'table_column_name'=>'product_code',
									'table_column'  => 'product_code',
									'id'  => 'product_code',
									'is_filter'   => true,
									'label' =>'text',
								
								),
								array(
									'column_name' =>'Qty',
									'table_column_name'=>'',
									'table_column'  => '',
								
								),
																											
							
								array(
									'column_name' =>'Nt Wt',
									'table_column_name'=>'',
									'table_column'  => '',
								
								),
								array(
									'column_name' =>'few Wt',
									'table_column_name'=>'',
									'table_column'  => '',
								
								),
								array(
									'column_name' =>'K pure',
									'table_column_name'=>'',
									'table_column'  => '',
								
								),
								array(
									'column_name' =>'M Wt',
									'table_column_name'=>'',
									'table_column'  => '',
								
								),
								array(
									'column_name' =>'Wax',
									'table_column_name'=>'',
									'table_column'  => '',
								
								),	
								array(
									'column_name' =>'C/S Wt',
									'table_column_name'=>'',
									'table_column'  => '',
								
								),
								array(
									'column_name' =>'Moti Wt',
									'table_column_name'=>'',
									'table_column'  => '',
								
								),
								array(
									'column_name' =>'CH Wt',
									'table_column_name'=>'',
									'table_column'  => '',
								
								),
								array(
									'column_name' =>'Beads Wt',
									'table_column_name'=>'',
									'table_column'  => '',
								
								),
								array(
									'column_name' =>'Gr WT',
									'table_column_name'=>'',
									'table_column'  => '',
								
								),
								array(
									'column_name' =>'CH PCS',
									'table_column_name'=>'',
									'table_column'  => '',
								
								),
								array(
									'column_name' =>'CH@',
									'table_column_name'=>'',
									'table_column'  => '',
								
								),
								array(
									'column_name' =>'CH amt',
									'table_column_name'=>'',
									'table_column'  => '',
								
								),
								array(
									'column_name' =>'Kun Pc',
									'table_column_name'=>'',
									'table_column'  => '',
								
								),
								array(
									'column_name' =>'Kun@',
									'table_column_name'=>'',
									'table_column'  => '',
								
								),
								array(
									'column_name' =>'Kun Amt',
									'table_column_name'=>'',
									'table_column'  => '',
								
								),
								array(
									'column_name' =>'stn wt',
									'table_column_name'=>'',
									'table_column'  => '',
								
								),
								array(
									'column_name' =>'stn@',
									'table_column_name'=>'',
									'table_column'  => '',
								
								),
								array(
									'column_name' =>'C/S Amt',
									'table_column_name'=>'',
									'table_column'  => '',
								
								),
								array(
									'column_name' =>'Oth Amt',
									'table_column_name'=>'',
									'table_column'  => '',
								
								),
								array(
									'column_name' =>'Ttl Amt',
									'table_column_name'=>'',
									'table_column'  => '',
								
								),
								array(
									'column_name' =>'',
									'table_column_name'=>'',
									'table_column'  => '',
								
								),					
													
															
																				

				),

		'manufacturing_repair_product_bgtrios_list'=>array(
												
								array(
										'column_name' =>'#',
										'table_column_name'=>'product_code', 
										'table_column'  => 'product_code',	
									),	
														
							
								array(
									'column_name' =>'Product Code',
									'table_column_name'=>'product_code',
									'table_column'  => 'product_code',
									'id'  => 'product_code',
									'is_filter'   => true,
									'label' =>'text',
								
								),
								array(
									'column_name' =>'Qty',
									'table_column_name'=>'',
									'table_column'  => '',
								
								),
																											
							
								array(
									'column_name' =>'Nt Wt',
									'table_column_name'=>'',
									'table_column'  => '',
								
								),
								
								array(
									'column_name' =>'Gr WT',
									'table_column_name'=>'',
									'table_column'  => '',
								
								),
								array(
									'column_name' =>'',
									'table_column_name'=>'',
									'table_column'  => '',
								
								),							
																				

				),


		'manufacturing_repair_product_list'=>array(
												
								array(
										'column_name' =>'#',
										'table_column_name'=>'product_code', 
										'table_column'  => 'product_code',	
									),	
														
							
								array(
									'column_name' =>'Product Code',
									'table_column_name'=>'product_code',
									'table_column'  => 'product_code',
									'id'  => 'product_code',
									'is_filter'   => true,
									'label' =>'text',
								
								),
								array(
									'column_name' =>'Qty',
									'table_column_name'=>'',
									'table_column'  => '',
								
								),
																											
							
								array(
									'column_name' =>'Nt Wt',
									'table_column_name'=>'',
									'table_column'  => '',
								
								),
								array(
									'column_name' =>'few Wt',
									'table_column_name'=>'',
									'table_column'  => '',
								
								),
								array(
									'column_name' =>'K pure',
									'table_column_name'=>'',
									'table_column'  => '',
								
								),
								array(
									'column_name' =>'M Wt',
									'table_column_name'=>'',
									'table_column'  => '',
								
								),
								array(
									'column_name' =>'Wax',
									'table_column_name'=>'',
									'table_column'  => '',
								
								),	
								array(
									'column_name' =>'C/S Wt',
									'table_column_name'=>'',
									'table_column'  => '',
								
								),
								array(
									'column_name' =>'Moti Wt',
									'table_column_name'=>'',
									'table_column'  => '',
								
								),
								array(
									'column_name' =>'CH Wt',
									'table_column_name'=>'',
									'table_column'  => '',
								
								),
								array(
									'column_name' =>'Gr WT',
									'table_column_name'=>'',
									'table_column'  => '',
								
								),
								array(
									'column_name' =>'CH PCS',
									'table_column_name'=>'',
									'table_column'  => '',
								
								),
								array(
									'column_name' =>'CH@',
									'table_column_name'=>'',
									'table_column'  => '',
								
								),
								array(
									'column_name' =>'CH amt',
									'table_column_name'=>'',
									'table_column'  => '',
								
								),
								array(
									'column_name' =>'Kun Pc',
									'table_column_name'=>'',
									'table_column'  => '',
								
								),
								array(
									'column_name' =>'Kun@',
									'table_column_name'=>'',
									'table_column'  => '',
								
								),
								array(
									'column_name' =>'Kun Amt',
									'table_column_name'=>'',
									'table_column'  => '',
								
								),
								array(
									'column_name' =>'stn wt',
									'table_column_name'=>'',
									'table_column'  => '',
								
								),
								array(
									'column_name' =>'stn@',
									'table_column_name'=>'',
									'table_column'  => '',
								
								),
								array(
									'column_name' =>'C/S Amt',
									'table_column_name'=>'',
									'table_column'  => '',
								
								),
								array(
									'column_name' =>'Oth Amt',
									'table_column_name'=>'',
									'table_column'  => '',
								
								),
								array(
									'column_name' =>'Ttl Amt',
									'table_column_name'=>'',
									'table_column'  => '',
								
								),
								array(
									'column_name' =>'',
									'table_column_name'=>'',
									'table_column'  => '',
								
								),					
													
															
																				

				),


			'mfg_repair_product_bom'=>array(
												
								array(
										'column_name' =>'#',
										'table_column_name'=>'product_code', 
										'table_column'  => 'product_code',	
									),	
														
							
								array(
									'column_name' =>'Chitti No',
									'table_column_name'=>'chitti_no',
									'table_column'  => 'chitti_no',
									'id'  => 'chitti_no',
									'is_filter'   => true,
									'label' =>'text',
								
								),
																											
								array(
									'column_name' =>'Weight',
									'table_column_name'=>'',
									'table_column'  => '',
								
								),
								array(
									'column_name' =>'Net Wt',
									'table_column_name'=>'',
									'table_column'  => '',
								
								),
							
								array(
									'column_name' =>'',
									'table_column_name'=>'',
									'table_column'  => '',
								
								),					
													
															
																				

				),		


		'few_box'=>array(
												
								array(
										'column_name' =>'#',
										'table_column_name'=>'product_name', 
										'table_column'  => 'product_name',	
									),	
														
							
								array(
									'column_name' =>'Product',
									'table_column_name'=>'product_name',
									'table_column'  => 'product_name',
									'id'  => 'product_name',
									'is_filter'   => true,
									'label' =>'text',
								
								),
																											
								array(
									'column_name' =>'weight',
									'table_column_name'=>'',
									'table_column'  => '',
								
								),		
								array(
									'column_name' =>'',
									'table_column_name'=>'',
									'table_column'  => '',
								
								),										
								

				),		

		'Karigar_order_tracking_tbl'=>array(
									array(
										'column_name' =>'#',
										'table_column_name'  => 'co.id',
										'table_column'  => 'co.id',
										
									),	
									array(
										'column_name' =>'Karigar',
										'table_column_name'  => 'name',
										'table_column'  => 'name',
										'id'  => 'k_name',
										'is_filter'   => true,
										'label' =>'text',
									),
									array(	
										'column_name' =>'Weight',	
										'table_column_name'  => 'weight_range_id',
										'table_column'  => 'weight_range_id',
										'id'  => 'weight_range_id',
										//'is_filter'   => true,
										//'get_functions_name'=>'Products_sent_by_order',
										'label' =>'text',								
									),
									
									// array(
									// 	'column_name' =>'Order Date',
									// 	'table_column_name'  => 'DATE_FORMAT(co.order_date,"%d-%m-%Y")',
									// 	'table_column'  => 'co.order_date',
									// 	'id'  => 'order_date',
									// 	//'is_filter'   => true,
									// 	//'get_functions_name'=>'Products_sent_by_order',
									// 	'label' =>'date',
									// ),
									// array(
									// 	'column_name' =>'Order Name',
									// 	'table_column_name'  => 'co.order_name',
									// 	'table_column'  => 'co.order_name',
									// 	'id'  => 'order_name',
									// 	'is_filter'   => true,
									// 	//'get_functions_name'=>'Products_sent_by_order',
									// 	'label' =>'text',
									// ),
									
																	
									// array(
									// 	'column_name' =>'Expected Date',
									// 	'table_column_name'  => 'DATE_FORMAT(co.expected_date,"%d-%m-%Y")',
									// 	'table_column'  => 'co.expected_date',
									// 	'id'  => 'expected_date',
									// 	//'is_filter'   => true,
									// 	//'get_functions_name'=>'Products_sent_by_order',
									// 	'label' =>'date',
									// ),
									// array(	
									// 	'column_name' =>'Quantity',	
									// 	'table_column_name'=>'',
									// 	'table_column'  => '',										
									// ),

										array(
									'column_name' =>'',
									'table_column_name'=>'',
									'table_column'  => '',
								
								),		
																				
									
			),

		'customer_order_tracking_tbl'=>array(
										array(
										'column_name' =>'#',
										'table_column_name'  => 'co.id',										
										
									),	
									array(
										'column_name' =>'Customer',

										'table_column_name'  => 'party_name',
										'table_column'  => 'party_name',
										'id'  => 'k_name',
										'is_filter'   => true,
										'label' =>'text',
									),
									// array(
									// 	'column_name' =>'Product',
									// 	'table_column_name'  => 'co.parent_category_id',
									// 	'table_column'  => 'co.parent_category_id',
									// 	'id'  => 'parent_category_id',

									// 	'is_filter'   => true,
									// 	'label' =>'text',
									// ),
									array(
										'column_name' =>'Weight',
										'table_column_name'  => 'weight_range_id',
										'table_column'  => 'weight_range_id',
										'id'  => 'k_name',
										'is_filter'   => false,
										'label' =>'text',
									),
									// array(	
									// 	'column_name' =>'Order Number',	
									// 	'table_column_name'  => 'co.id',
									// 	'table_column'  => 'co.id',
									// 	'id'  => 'co_id',
									// 	//'is_filter'   => true,
									// 	//'get_functions_name'=>'Products_sent_by_order',
									// 	'label' =>'text',								
									// ),
									// array(
									// 	'column_name' =>'Date',
									// 	'table_column_name'  => 'DATE_FORMAT(co.order_date,"%d-%m-%Y")',
									// 	'table_column'  => 'co.order_date',
									// 	'id'  => 'order_date',
									// 	//'is_filter'   => true,
									// 	//'get_functions_name'=>'Products_sent_by_order',
									// 	'label' =>'date',
									// ),			
																	
									
									// array(
									// 	'column_name' =>'Delivery Date',
									// 	'table_column_name'  => 'DATE_FORMAT(co.delievery_date,"%d-%m-%Y")',
									// 	'table_column'  => 'co.delievery_date',
									// 	'id'  => 'delievery_date',
									// 	//'is_filter'   => true,
									// 	//'get_functions_name'=>'Products_sent_by_order',
									// 	'label' =>'date',
									// ),
									// array(	
									// 	'column_name' =>'Quantity',	
									// 	'table_column_name'=>'',
									// 	'table_column'  => '',										
									// ),	
									// array(	
									// 	'column_name' =>'status',	
									// 	'table_column_name'=>'',
									// 	'table_column'  => '',										
									// ),	

									array(
									'column_name' =>'',
									'table_column_name'=>'',
									'table_column'  => '',
								
								),												
									
			),	

		'in_stock_report_tbl'=>array(
										array(
										'column_name' =>'#',
										'table_column_name'  => 'co.id',										
										
									),	
									array(
										'column_name' =>'Customer Name',

										'table_column_name'  => 'party_name',
										'table_column'  => 'party_name',
										'id'  => 'party_name',
										'is_filter'   => true,
										'label' =>'text',
									),
									array(
										'column_name' =>'Karigar Name',
										'table_column_name'  => 'name',
										'table_column'  => 'name',
										'id'  => 'name',
										'is_filter'   => true,
										'label' =>'text',
									),
									array(
										'column_name' =>'Product',
										'table_column_name'  => 'co.parent_category_id',
										'table_column'  => 'co.parent_category_id',
										'id'  => 'parent_category_id',
										'is_filter'   => true,
										'label' =>'text',
									),
									array(
										'column_name' =>'Weight',
										'table_column_name'  => 'weight_range_id',
										'table_column'  => 'weight_range_id',
										'id'  => 'weight_range_id',
										'is_filter'   => true,
										'label' =>'text',
									),
									
						
									
			),	
		

			'individual_customer_order_tracking_tbl'=>array(
										array(
										'column_name' =>'#',
										'table_column_name'  => 'id',
										
										
									),	
									array(
										'column_name' =>'Order Number',
										'table_column_name'  => 'co.id',
										'table_column'  => 'co.id',
										'id'  => 'id',
										'is_filter'   => true,
										'label' =>'text',
									),
									array(
										'column_name' =>'Karigar Name',
										'table_column_name'  => 'km.name',
										'table_column'  => 'km.name',
										'id'  => 'name',
										'is_filter'   => true,
										'label' =>'text',
									),
									array(
										'column_name' =>'Order Date',
										'table_column_name'  => 'DATE_FORMAT(co.order_date,"%d-%m-%Y")',
										'table_column'  => 'co.order_date',
										'id'  => 'order_date',
										'is_filter'   => true,
										'label' =>'date',
									),
                                    array(
										'column_name' =>'Expected Date',
										'table_column_name'  => 'DATE_FORMAT(co.expected_date,"%d-%m-%Y")',
										'table_column'  => 'co.expected_date',
										'id'  => 'expected_date',
										'is_filter'   => true,
										'label' =>'date',
									),
									array(
										'column_name' =>'Quantity',
										'table_column_name'  => 'co.quantity',
										'table_column'  => 'co.quantity',
										'id'  => 'quantity',
										'is_filter'   => true,
										'label' =>'text',
									),
									array(	
										'column_name' =>'status',	
										'table_column_name'=>'',
										'table_column'  => '',										
									),										
									
			),	

				'daily_order_received_tbl'=>array(
										array(
										'column_name' =>'#',
										'table_column_name'  => 'id',
										
										
									),	
									array(
										'column_name' =>'Order Number',
										'table_column_name'  => 'co.id',
										'table_column'  => 'co.id',
										'id'  => 'id',
										'is_filter'   => true,
										'label' =>'text',
									),
									array(
										'column_name' =>'Customer Name',
										'table_column_name'  => 'co.party_name',
										'table_column'  => 'co.party_name',
										'id'  => 'party_name',
										'is_filter'   => true,
										'label' =>'text',
									),
                                    array(
										'column_name' =>'Department Name',
										'table_column_name'  => 'dp.name',
										'table_column'  => 'dp.name',
										'id'  => 'name',
										'is_filter'   => true,
										'label' =>'text',
									),
									array(
										'column_name' =>'Karigar Name ',
										'table_column_name'  => 'km.name',
										'table_column'  => 'km.name',
										'id'  => 'name',
										'is_filter'   => true,
										'label' =>'text',
									),
									array(
										'column_name' =>'Product',
										'table_column_name'  => 'co.parent_category_id',
										'table_column'  => 'co.parent_category_id',
										'id'  => 'k_name',
										'is_filter'   => true,
										'label' =>'text',
									),
									array(
										'column_name' =>'Weight',
										'table_column_name'  => 'co.weight_range_id',
										'table_column'  => 'co.weight_range_id',
										'id'  => 'weight_range_id',
										'is_filter'   => true,
										'label' =>'text',
									),
									array(	
										'column_name' =>'status',	
										'table_column_name'=>'',
										'table_column'  => '',										
									),	

									// array(	
									// 	'column_name' =>'co.order_date',	
									// 	'table_column_name'=>'co.order_date',
									// 	'table_column'  => 'co.order_date',
									// 	'is_filter'   => true,
									// 	'label' =>'text',										
									// ),										
									
			),			
				'daily_order_received_from_karigar_tbl'=>array(
										array(
										'column_name' =>'#',
										'table_column_name'  => 'id',
										
										
									),	
									array(
										'column_name' =>'Order Number',
										'table_column_name'  => 'co.id',
										'table_column'  => 'co.id',
										'id'  => 'id',
										'is_filter'   => true,
										'label' =>'text',
									),
									
                                   
									array(
										'column_name' =>'Karigar Name ',
										'table_column_name'  => 'km.name',
										'table_column'  => 'km.name',
										'id'  => 'name',
										'is_filter'   => true,
										'label' =>'text',
									),
									 array(
										'column_name' =>'Department Name',
										'table_column_name'  => 'dp.name',
										'table_column'  => 'dp.name',
										'id'  => 'name',
										'is_filter'   => true,
										'label' =>'text',
									),
									array(
										'column_name' =>'Product',
										'table_column_name'  => 'co.parent_category_id',
										'table_column'  => 'co.parent_category_id',
										'id'  => 'parent_category_id',
										'is_filter'   => true,
										'label' =>'text',
									),
									array(
										'column_name' =>'Customer Name',
										'table_column_name'  => 'co.party_name',
										'table_column'  => 'co.party_name',
										'id'  => 'party_name',
										'is_filter'   => true,
										'label' =>'text',
									),
									array(
										'column_name' =>'Weight',
										'table_column_name'  => 'co.weight_range_id',
										'table_column'  => 'co.weight_range_id',
										'id'  => 'weight_range_id',
										'is_filter'   => true,
										'label' =>'text',
									),
									array(
										'column_name' =>'Quantity',
										'table_column_name'  => 'co.quantity',
										'table_column'  => 'co.quantity',
										'id'  => 'quantity',
										'is_filter'   => true,
										'label' =>'text',
									),

									array(	
										'column_name' =>'On-time',	
										'table_column_name'=>'co.order_date',
										'table_column'  => 'co.order_date',
										'is_filter'   => true,
										'label' =>'text',										
									),										
									
			),			
				
			'individual_karigar_order_tracking_tbl'=>array(
										array(
										'column_name' =>'#',
										'table_column_name'  => 'id',
										
										
									),	
									array(
										'column_name' =>'Order Number',
										'table_column_name'  => 'id',
										'table_column'  => 'id',
										'id'  => 'id',
										'is_filter'   => true,
										'label' =>'text',
									),

									array(
										'column_name' =>'Party Name',
										'table_column_name'  => 'party_name',
										'table_column'  => 'party_name',
										'id'  => 'party_name',
										'is_filter'   => true,
										'label' =>'text',
									),
									array(
										'column_name' =>'Order Date',
										'table_column_name'  => 'DATE_FORMAT(order_date,"%d-%m-%Y")',
										'table_column'  => 'order_date',
										'id'  => 'order_date',
										'is_filter'   => true,
										'label' =>'date',
									),
                                    array(
										'column_name' =>'Expected Date',
										'table_column_name'  => 'DATE_FORMAT(expected_date,"%d-%m-%Y")',
										'table_column'  => 'expected_date',
										'id'  => 'expected_date',
										'is_filter'   => true,
										'label' =>'date',
									),
									array(
										'column_name' =>'Quantity',
										'table_column_name'  => 'quantity',
										'table_column'  => 'quantity',
										'id'  => 'quantity',
										'is_filter'=> true,
										'label' =>'text',
									),
									array(	
										'column_name' =>'status',	
										'table_column_name'=>'',
										'table_column'  => '',										
									),										
									
			     ),	
			'delivery_report_in_qty_tbl'=>array(
										array(
										'column_name' =>'#',
										'table_column_name'  => 'id',
										
										
									),	
									array(
										'column_name' =>'Department/Category',
										'table_column_name'  => 'name',
										'table_column'  => 'name',
										'id'  => 'name',
										'is_filter'   => true,
										'label' =>'text',
									),

									array(
										'column_name' =>'0-15',
										'table_column_name'  => 'quantity',
										'table_column'  => 'quantity',
										'id'  => 'quantity',
										'is_filter'   => false,
										'label' =>'text',
									),
									array(
										'column_name' =>'16-30',
										'table_column_name'  => 'quantity',
										'table_column'  => 'quantity',
										'id'  => 'quantity',
										'is_filter'   => false,
										'label' =>'date',
									),
                                    array(
										'column_name' =>'31-45',
										'table_column_name'  => 'quantity',
										'table_column'  => 'quantity',
										'id'  => 'quantity',
										'is_filter'   => false,
										'label' =>'date',
									),
									array(
										'column_name' =>'45+',
										'table_column_name'  => 'quantity',
										'table_column'  => 'quantity',
										'id'  => 'quantity',
										'is_filter'=> false,
										'label' =>'text',
									),
									// array(	
									// 	'column_name' =>'status',	
									// 	'table_column_name'=>'',
									// 	'table_column'  => '',										
									// ),										
									
			     ),			
					


				'report_summary_departmentWise_tbl'=>array(
										array(
										'column_name' =>'#',
										'table_column_name'  => 'id',	
										
									),	
									array(
										'column_name' =>'Department Name',
										'table_column_name'  => 'name',
										'table_column'  => 'name',
										'id'=> 'name',
										'is_filter'   => true,
										'label' =>'text',
									),array(
										'column_name' =>'Opening Stock',
										'table_column_name'  => 'opening_stock',
										'table_column'  => 'opening_stock',
										'id'  => 'opening_stock',
										'is_filter'   => false,
										'label' =>'text',
									),
                                      array(
										'column_name' =>'Received Stock',
										'table_column_name'  => 'received_stock',
										'table_column'  => 'received_stock',
										'id'  => 'received_stock',
										'is_filter'   => false,
										'label' =>'text',
									), 
									 array(
										'column_name' =>'Issued Stock',
										'table_column_name'  => 'issued_stock',
										'table_column'  => 'issued_stock',
										'id'  => 'issued_stock',
										'is_filter'   => false,
										'label' =>'text',
									),
                                    array(
										'column_name' =>'Closing Stock',
										'table_column_name'  => 'closing_stock',
										'table_column'  => 'closing_stock',
										'id'  => 'closing_stock',
										'is_filter'   => false,
										'label' =>'text',
									),
                                 array(
										'column_name' =>'Received Order',
										'table_column_name'  => 'received_order',
										'table_column'  => 'received_order',
										'id'  => 'received_order',
										'is_filter'   => false,
										'label' =>'text',
									),  array(
										'column_name' =>'Issued Order',
										'table_column_name'  => 'issued_order',
										'table_column'  => 'issued_order',
										'id'  => 'issued_order',
										'is_filter'   => false,
										'label' =>'text',
									),

									// array(
									// 	'column_name' =>'Party Name',
									// 	'table_column_name'  => 'party_name',
									// 	'table_column'  => 'party_name',
									// 	'id'  => 'k_name',
									// 	'is_filter'   => true,
									// 	'label' =>'text',
									// ),
									// array(
									// 	'column_name' =>'Order Date',
									// 	'table_column_name'  => 'DATE_FORMAT(order_date,"%d-%m-%Y")',
									// 	'table_column'  => 'order_date',
									// 	'id'  => 'k_name',
									// 	'is_filter'   => true,
									// 	'label' =>'date',
									// ),
         //                            array(
									// 	'column_name' =>'Expected Date',
									// 	'table_column_name'  => 'DATE_FORMAT(expected_date,"%d-%m-%Y")',
									// 	'table_column'  => 'expected_date',
									// 	'id'  => 'k_name',
									// 	'is_filter'   => true,
									// 	'label' =>'date',
									// ),
									// array(
									// 	'column_name' =>'Quantity',
									// 	'table_column_name'  => 'quantity',
									// 	'table_column'  => 'quantity',
									// 	'id'  => 'k_name',
									// 	'is_filter'   => true,
									// 	'label' =>'text',
									// ),
									// array(	
									// 	'column_name' =>'status',	
									// 	'table_column_name'=>'',
									// 	'table_column'  => '',										
									// ),										
									
			),		
			'mfg_ready_product_details'=> array(
														/*array(
															'column_name' =>'#',
															'table_column_name'=>'',
															'table_column'  => '',	
														),*/
														array(
															'column_name' =>'Qty',
															'table_column_name'=>'',
															'table_column'  => '',	
														),
														array(
															'column_name' =>'Net WT',
															'table_column_name'=>'',
															'table_column'  => '',	
																										
														),
														array(	
															'column_name' =>'Few WT',
															'table_column_name'=>'',
															'table_column'  => '',	
													
														),
														array(
															'column_name' =>'k Pure',
															'table_column_name'=>'',
															'table_column'  => '',	
														),
														array(
															'column_name' =>'M Wt',
															'table_column_name'=>'',
															'table_column'  => '',	
														),
														array(	
															'column_name' =>'Wax',
															'table_column_name'=>'',
															'table_column'  => '',	
														),
														array(
															'column_name' =>'C/S Wt',
															'table_column_name'=>'',
															'table_column'  => '',	
														),
														array(
															'column_name' =>'Moti wt',	
															'table_column_name'=>'',
															'table_column'  => '',								
														),
														
														array(
															'column_name' =>'CH wt',	
															'table_column_name'=>'',
															'table_column'  => '',								
														),
														array(
															'column_name' =>'Gr WT',
															'table_column_name'=>'',
															'table_column'  => '',	
																										
														),

														array(
															'column_name' =>'CH PCS',
															'table_column_name'=>'',
															'table_column'  => '',	
																										
														),
														array(
															'column_name' =>'CH@',
															'table_column_name'=>'',
															'table_column'  => '',	
																										
														),
														array(
															'column_name' =>'CH amt',
															'table_column_name'=>'',
															'table_column'  => '',	
																										
														),
														array(
															'column_name' =>'Kun Pc',
															'table_column_name'=>'',
															'table_column'  => '',	
																										
														),
														array(
															'column_name' =>'Kun@',
															'table_column_name'=>'',
															'table_column'  => '',	
																										
														),array(
															'column_name' =>'Kun Amt',
															'table_column_name'=>'',
															'table_column'  => '',	
																										
														),
														array(
															'column_name' =>'stn wt',
															'table_column_name'=>'',
															'table_column'  => '',	
																										
														),
														array(
															'column_name' =>'stn@',
															'table_column_name'=>'',
															'table_column'  => '',	
																										
														),
														array(
															'column_name' =>'C/S Amt',
															'table_column_name'=>'',
															'table_column'  => '',	
																										
														),
														array(
															'column_name' =>'Black Beads wt',
															'table_column_name'=>'',
															'table_column'  => '',	
																										
														),
														array(
															'column_name' =>'Oth Amt',
															'table_column_name'=>'',
															'table_column'  => '',	
																										
														),
														array(
															'column_name' =>'Ttl Amt',
															'table_column_name'=>'',
															'table_column'  => '',	
																										
														),

							),

						'mfg_sales_ready_product_details'=> array(
														/*array(
															'column_name' =>'#',
															'table_column_name'=>'',
															'table_column'  => '',	
														),*/
														array(
															'column_name' =>'Qty',
															'table_column_name'=>'',
															'table_column'  => '',	
														),
														array(
															'column_name' =>'Net WT',
															'table_column_name'=>'',
															'table_column'  => '',	
																										
														),
														array(	
															'column_name' =>'Few WT',
															'table_column_name'=>'',
															'table_column'  => '',	
													
														),
														array(
															'column_name' =>'k Pure',
															'table_column_name'=>'',
															'table_column'  => '',	
														),
														array(
															'column_name' =>'M Wt',
															'table_column_name'=>'',
															'table_column'  => '',	
														),
														array(	
															'column_name' =>'Wax',
															'table_column_name'=>'',
															'table_column'  => '',	
														),
														array(
															'column_name' =>'C/S Wt',
															'table_column_name'=>'',
															'table_column'  => '',	
														),
														array(
															'column_name' =>'Moti wt',	
															'table_column_name'=>'',
															'table_column'  => '',								
														),
														
														array(
															'column_name' =>'CH wt',	
															'table_column_name'=>'',
															'table_column'  => '',								
														),
														array(
															'column_name' =>'Gr WT',
															'table_column_name'=>'',
															'table_column'  => '',	
																										
														),

														array(
															'column_name' =>'CH PCS',
															'table_column_name'=>'',
															'table_column'  => '',	
																										
														),
														array(
															'column_name' =>'CH@',
															'table_column_name'=>'',
															'table_column'  => '',	
																										
														),
														array(
															'column_name' =>'CH amt',
															'table_column_name'=>'',
															'table_column'  => '',	
																										
														),
														array(
															'column_name' =>'Kun Pc',
															'table_column_name'=>'',
															'table_column'  => '',	
																										
														),
														array(
															'column_name' =>'Kun@',
															'table_column_name'=>'',
															'table_column'  => '',	
																										
														),array(
															'column_name' =>'Kun Amt',
															'table_column_name'=>'',
															'table_column'  => '',	
																										
														),
														array(
															'column_name' =>'stn wt',
															'table_column_name'=>'',
															'table_column'  => '',	
																										
														),
														array(
															'column_name' =>'stn@',
															'table_column_name'=>'',
															'table_column'  => '',	
																										
														),
														array(
															'column_name' =>'C/S Amt',
															'table_column_name'=>'',
															'table_column'  => '',	
																										
														),
														array(
															'column_name' =>'Black Beads wt',
															'table_column_name'=>'',
															'table_column'  => '',	
																										
														),
														array(
															'column_name' =>'Oth Amt',
															'table_column_name'=>'',
															'table_column'  => '',	
																										
														),
														array(
															'column_name' =>'Ttl Amt',
															'table_column_name'=>'',
															'table_column'  => '',	
																										
														),

							),	

			'mfg_ready_product_details_mangalsutra'=> array(
													
														array(
															'column_name' =>'Qty',
															'table_column_name'=>'',
															'table_column'  => '',	
														),
														array(
															'column_name' =>'Net WT',
															'table_column_name'=>'',
															'table_column'  => '',	
																										
														),
														array(	
															'column_name' =>'Few WT',
															'table_column_name'=>'',
															'table_column'  => '',	
													
														),
														array(
															'column_name' =>'k Pure',
															'table_column_name'=>'',
															'table_column'  => '',	
														),
														array(
															'column_name' =>'M Wt',
															'table_column_name'=>'',
															'table_column'  => '',	
														),
														array(	
															'column_name' =>'Wax',
															'table_column_name'=>'',
															'table_column'  => '',	
														),
														array(
															'column_name' =>'C/S Wt',
															'table_column_name'=>'',
															'table_column'  => '',	
														),
														array(
															'column_name' =>'Moti wt',	
															'table_column_name'=>'',
															'table_column'  => '',								
														),
														
														array(
															'column_name' =>'CH wt',	
															'table_column_name'=>'',
															'table_column'  => '',								
														),
														array(
															'column_name' =>'Beads wt',	
															'table_column_name'=>'',
															'table_column'  => '',								
														),
														array(
															'column_name' =>'Gr WT',
															'table_column_name'=>'',
															'table_column'  => '',	
																										
														),

														array(
															'column_name' =>'CH PCS',
															'table_column_name'=>'',
															'table_column'  => '',	
																										
														),
														array(
															'column_name' =>'CH@',
															'table_column_name'=>'',
															'table_column'  => '',	
																										
														),
														array(
															'column_name' =>'CH amt',
															'table_column_name'=>'',
															'table_column'  => '',	
																										
														),
														array(
															'column_name' =>'Kun Pc',
															'table_column_name'=>'',
															'table_column'  => '',	
																										
														),
														array(
															'column_name' =>'Kun@',
															'table_column_name'=>'',
															'table_column'  => '',	
																										
														),array(
															'column_name' =>'Kun Amt',
															'table_column_name'=>'',
															'table_column'  => '',	
																										
														),
														array(
															'column_name' =>'stn wt',
															'table_column_name'=>'',
															'table_column'  => '',	
																										
														),
														array(
															'column_name' =>'stn@',
															'table_column_name'=>'',
															'table_column'  => '',	
																										
														),
														array(
															'column_name' =>'C/S Amt',
															'table_column_name'=>'',
															'table_column'  => '',	
																										
														),
														array(
															'column_name' =>'Oth Amt',
															'table_column_name'=>'',
															'table_column'  => '',	
																										
														),
														array(
															'column_name' =>'Ttl Amt',
															'table_column_name'=>'',
															'table_column'  => '',	
																										
														),

							),

	'mfg_ready_product_details_bangl'=> array(
													
														array(
															'column_name' =>'Qty',
															'table_column_name'=>'',
															'table_column'  => '',	
														),
														array(
															'column_name' =>'Net WT',
															'table_column_name'=>'',
															'table_column'  => '',	
																										
														),
														
														array(
															'column_name' =>'Gr WT',
															'table_column_name'=>'',
															'table_column'  => '',	
																										
														),

														

							),								

			'corporate_engaged_order_list'=>array(
						
							array(
								'column_name' =>'Order Id',
								'table_column_name'  => 'cp.order_id',
								'table_column'  => 'cp.order_id',
								'id'  => 'cp_order_id',
								'is_filter'   => true,
								'label' =>'text',
							),
							array(
								'column_name' =>'Karigar',
								'table_column_name'  => 'k.name',
								'table_column'  => 'k.name',
								'id'  => 'k_name',
								'is_filter'   => true,
								'label' =>'text',
							),
							array(
								'column_name' =>'Order Date',
								'table_column_name'  => 'DATE_FORMAT(eo.date,"%d-%m-%Y")',
								'table_column'  =>'DATE(eo.date)', 
								'id'  => 'order_date',
								'is_filter'   => true,
								'label'=>'date',
							),						
							array(
								'column_name' =>'No of products',
								'table_column_name'  =>"count(cp.sort_Item_number)",
								'table_column'  => 'cp.sort_Item_number',
								'id'  => 'sort_Item_number',														
							
							),
						  array(
								'column_name' =>'Total WT',
								'table_column_name'  =>"sum(cp.quantity)",
								'table_column'  => 'cp.quantity',
								'id'  => 'quantity',
								
							
							),

							array(
								'column_name' =>'Corporate',
								'table_column_name'  => 'c.name',
								'table_column'  => 'c.name',
								'id'  => 'corporate_name',
								'is_filter'   => true,
								//'source'=>'Karigar_product_list',
								'label' =>'text',
							
							)


				),

			'corporate_engaged_order_detail_list'=>array(
						
							array(
								'column_name' =>'Work order Id',
								'table_column_name'  => 'cp.order_id',
								'table_column'  => 'cp.order_id',
								'id'  => 'cp_order_id',
								'is_filter'   => true,
								'label' =>'text',
							),
							array(
								'column_name' =>'Status',
								'table_column_name'  => 'k.Status',
								'table_column'  => 'k.Status',
								'id'  => 'Status',
								'label' =>'text',
							),				
							array(
								'column_name' =>'Design code',
								'table_column_name'  => 'sort_Item_number',
								'table_column'  => 'sort_Item_number',
								'id'  => 'cp_sort_Item_number',
								'is_filter'   => true,
								'label' =>'text',
							),
							array(
								'column_name' =>'Karigar',
								'table_column_name'  => 'k.name',
								'table_column'  => 'k.name',
								'id'  => 'k_name',
								'is_filter'   => true,
								'label' =>'text',
							),
							array(
								'column_name' =>'line number',
								'table_column_name'  => 'cp.line_number',
								'table_column'  => 'cp.line_number',
								'id'  => 'line_number',
								'label' =>'text',
							),
							array(
								'column_name' =>'Reference Type',
								'table_column_name'  => 'cp.reference_type',
								'table_column'  => 'cp.reference_type',
								'id'  => 'reference_type',
								'label' =>'text',
							),					
							array(
								'column_name' =>'WO Reference Type',
								'table_column_name'  => 'cp.wo_reference_type',
								'table_column'  => 'cp.wo_reference_type',
								'id'  => 'wo_reference_type',
								'label' =>'text',
							),
							array(
								'column_name' =>'Intended WH',
								'table_column_name'  => 'cp.intended_wh',
								'table_column'  => 'cp.intended_wh',
								'id'  => 'intended_wh',
								'label' =>'text',
							),	
							array(
								'column_name' =>'Item number',
								'table_column_name'  => 'cp.item_number',
								'table_column'  => 'cp.item_number',
								'id'  => 'item_number',
								'label' =>'text',
							),	
							array(
								'column_name' =>'Proposed Delivery Date',
								'table_column_name'  => 'cp.proposed_delivery_date',
								'table_column'  => 'cp.proposed_delivery_date',
								'id'  => 'proposed_delivery_date',
								'label' =>'text',
							),					
							array(
								'column_name' =>'Product Name',
								'table_column_name'  => 'cp.product_name',
								'table_column'  => 'cp.product_name',
								'id'  => 'product_name',
								'label' =>'text',
							),		
							array(
								'column_name' =>'External item number',
								'table_column_name'  => 'cp.External_item_number',
								'table_column'  => 'cp.External_item_number',
								'id'  => 'External_item_number',
								'label' =>'text',
							),	
							array(
								'column_name' =>'Temp SKU Number',
								'table_column_name'  => 'cp.temp_sku_number',
								'table_column'  => 'cp.temp_sku_number',
								'id'  => 'temp_sku_number',
								'label' =>'text',
							),
							array(
								'column_name' =>'Set SKU Number',
								'table_column_name'  => 'cp.set_sku_number',
								'table_column'  => 'cp.set_sku_number',
								'id'  => 'set_sku_number',
								'label' =>'text',
							),	
							array(
								'column_name' =>'Set Parent Id',
								'table_column_name'  => 'cp.set_parent_id',
								'table_column'  => 'cp.set_parent_id',
								'id'  => 'set_parent_id',
								'label' =>'text',
							),	
							array(
								'column_name' =>'Unit',
								'table_column_name'  => 'cp.unit',
								'table_column'  => 'cp.unit',
								'id'  => 'unit',
								'label' =>'text',
							),		
							array(
								'column_name' =>'Quantity',
								'table_column_name'  => 'cp.quantity',
								'table_column'  => 'cp.quantity',
								'id'  => 'cp_quantity',
								'label' =>'text',
							),	
							array(
								'column_name' =>'CW unit',
								'table_column_name'  => 'cp.CW_unit',
								'table_column'  => 'cp.CW_unit',
								'id'  => 'CW_unit',
								'label' =>'text',
							),
							array(
								'column_name' =>'Weight',
								'table_column_name'  => 'cp.weight',
								'table_column'  => 'cp.weight',
								'id'  => 'cp_name',
								'label' =>'text',
							),	
							array(
								'column_name' =>'Confirm Status',
								'table_column_name'  => 'cp.confirm_status',
								'table_column'  => 'cp.confirm_status',
								'id'  => 'confirm_status',
								'label' =>'text',
							),		
							array(
								'column_name' =>'Special Remarks',
								'table_column_name'  => 'cp.special_remarks',
								'table_column'  => 'cp.special_remarks',
								'id'  => 'special_remarks',
								'label' =>'text',
							),	
													
							

				),

			'Corporate_order_Details_list'=> array(
														array(
															'column_name' =>'Design Code',
															'table_column_name'  => 'cp.sort_Item_number',
															'table_column'  => 'cp.sort_Item_number',
															'id'  => 'sort_Item_numbe',
															'is_filter'   => true,
															
															'label' =>'text',
														),
														array(
															'column_name' =>'Order Id',
															'table_column_name'  => 'cp.order_id',
															'table_column'  => 'cp.order_id',
															'id'  => 'order_id',
															'is_filter'   => true,
															
															'label' =>'text',
															
															
														),
														array(
															'column_name' =>'Karigar Engaged Date',
															'table_column_name'  => 'DATE_FORMAT(ppl.karigar_engaged_date,"%d-%m-%Y")',
															'table_column'  => 'karigar_engaged_date',
															'id'  => 'karigar_engaged_date',
															'is_filter'   => true,
															
															'label' =>'date',
														),
														array(
														'column_name' =>'Proposed delivery date',
														'table_column_name'  => 'DATE_FORMAT(ppl.karigar_delivery_date,"%d-%m-%Y")',
														'table_column'  => 'karigar_delivery_date',
														'id'  => 'karigar_delivery_date',
														'is_filter'   => true,
														
														'label' =>'date',
														),

														array(	
															'column_name' =>'Quantity',
															'table_column'  => '',
															'table_column_name'  => '',
															'table_column'  => 'Quantity',	
															'id'=>'Quantity',
															'label' =>'text',
														),
														array(
															'column_name' =>'Total Weight',
															'table_column_name'  => '',
															'table_column'  => 'received_pcs',
															
															'label' =>'text',
														),
														array(	
															'column_name' =>'Received Weight',
															'table_column_name'  => '',
															'table_column'  => 'total_wt',
															'id'  => 'total_wt',
															
															'label' =>'text',
														),														
														array(
															'column_name' =>'Balance Wt',
															'table_column_name'  => '',
															'table_column'  => 'balance_wt',
															'id'  => 'balance_wt',
															
															'label' =>'text',
														),
														array(
															'column_name' =>'status',
															'table_column_name'  => '',
															'table_column'  => 'status',
															'id'  => 'status',
															
															'label' =>'text',
														),
														
							),
			'product_category_rate'=> array(
														array(
															'column_name' =>'Name',
															'table_column_name'  => 'name',
															'table_column'  => 'name',
															'id'  => 'name',
															'is_filter'   => true,
															
															'label' =>'text',
														),
														array(
															'column_name' =>'percentage',
															'table_column_name'  => 'percentage',
															'table_column'  => 'percentage',
															'id'  => 'percentage',
															'is_filter'   => true,
															
															'label' =>'text',
															
															
														),
														array(
															'column_name' =>'',	
															'table_column_name'=>'',
															'table_column'  => '',								
														),
														
														
							),
														
								
								
		

		);		
		return $tableHeadParameters[$heading];
	}

?>
   
