<?php

if (!function_exists('createHeading')) {
	function createHeading($heading=array(),$new_order=''){
		$html = '';
		if(!empty($heading)){
			$html.='<tr role="row">';
			foreach ($heading as $key => $value) {
				if($value['sort'])
					$html.='<th><a onclick="sortTable(\''.$new_order.'\',\''.$value['sort_field'].'\');" id="ascsort" class="fa fa-sort asc customsort"></a> &nbsp; <a onclick="open_search_form(\'auth_popup\',\'search\',\''.$key.'\');"" id=""class="fa fa-search" aria-hidden="true" ></a><span class="tabletitle">'.$value["name"].'</span></th>';
				else
					$html.='<th><span class="tabletitle">'.$value["name"].'</span></th>';
			}
			$html.='</tr>';
		}
		return $html;
	}
}

if (!function_exists('printResult')) {
	function printResult($heading=array(),$results1=array()){
		$ci = &get_instance();
		$ci->load->model(array('common_model','contractor_model'));
		$html = '<tbody>';
		if(!empty($results1) && is_array($results1)){
           foreach($results1 as $row){
           		$html.='<tr>';
           			foreach ($heading as $key => $value) {
           				if('personal_files'==$value['field']){
           					$row[@$value['field']] = $ci->contractor_model->fileswithname($row,$row['id']);
           				}
           				else if('Action'==$value['name']){
           					$row[@$value['field']]='<a target="_blank" href="'.FRONTEND_PATH.'contractor/index/'.$row['id'].'">View</a><br /><a target="_blank" href="'.FRONTEND_PATH.'contractor/dashboard/'.$row['id'].'">Dashboard</a><br /><a href="javascript:void(0);" onclick="save_friendslist('.$ci->session->userdata("user_id").',\''.$row['id'].'\',\'\',\'\');" class="chat_to"><span class="icon-Ask-a-Question"></span>Ask A Question</a><br /><a href="javascript:void(0);" onclick="get_comment(\''.FRONTEND_PATH.'\',\''.$row['id'].'\',\'contractor\');" >Profile Record</a>';

           					if (ENVIRONMENT != 'production') {
           						$row[@$value['field']].='<br /><a onclick="send_cron_mail(\''.$row["id"].'\')" href="javascript:void(0);">Send Mail</a>';
           					}
           				}
           				else if('about_me'==$value['field']){
           					$row[$value['field']] = truncateTableColumn($row[$value['field']],MAX_LEG,MIN_LEG);
           				}
           				else if('premium'==$value['field']){
           					if($row['premium']=='1')
           						$row[$value['field']] = 'Yes';
           					else
           						$row[$value['field']] = 'No';
           				}
           				else if('degree'==$value['field']){
           					$row[$value['field']] = $ci->common_model->getUserDegree($row['id']);
           					$row[$value['field']] = truncateTableColumn($row[$value['field']],MAX_LEG,MIN_LEG);
           				}
           				else if('school'==$value['field']){
           					$row[$value['field']] = $ci->common_model->getUserSchool($row['id']);
           					$row[$value['field']] = truncateTableColumn($row[$value['field']],MAX_LEG,MIN_LEG);
           				}
           				else if('specializations'==$value['field']){
           					$row[$value['field']] = $ci->common_model->getUserSpecializations($row['id']);
           					$row[$value['field']] = truncateTableColumn($row[$value['field']],MAX_LEG,MIN_LEG);
           				}
           				else if('companies'==$value['field']){
           					$row[$value['field']] = $ci->common_model->getUserCompanies($row['id']);
           					$row[$value['field']] = truncateTableColumn($row[$value['field']],MAX_LEG,MIN_LEG);
           				}
           				else if('we_occupations'==$value['field']){
           					$row[$value['field']] = $ci->common_model->getWEOccupations($row['id']);
           					$row[$value['field']] = truncateTableColumn($row[$value['field']],MAX_LEG,MIN_LEG);
           				}
           				else if('software_licenses'==$value['field']){
           					$row[$value['field']] = $ci->common_model->getSoftwareLicenses($row['id']);
           					$row[$value['field']] = truncateTableColumn($row[$value['field']],MAX_LEG,MIN_LEG);
           				}
           				else if('tools_owned'==$value['field']){
           					$row[$value['field']] = $ci->common_model->getUserTools($row['id']);
           					$row[$value['field']] = truncateTableColumn($row[$value['field']],MAX_LEG,MIN_LEG);
           				}
           				else if('industries'==$value['field']){
           					$row[$value['field']] = $ci->common_model->getUserIndustries($row['id']);
           					$row[$value['field']] = truncateTableColumn($row[$value['field']],MAX_LEG,MIN_LEG);
           				}
           				else if('occupations'==$value['field']){
           					$row[$value['field']] = $ci->common_model->getUserOccupations($row['id']);
           					$row[$value['field']] = truncateTableColumn($row[$value['field']],MAX_LEG,MIN_LEG);
           				}
           				else if('applications'==$value['field']){
           					$row[$value['field']] = $ci->common_model->getUserApplications($row['id']
           						);
           					$row[$value['field']] = truncateTableColumn($row[$value['field']],MAX_LEG,MIN_LEG);
           				}
           				else if('status'==$value['field']){
           					$row[$value['field']] = ucfirst($row[$value['field']]);
           				}
           				else if('published_profile'==$value['field']){
           					$row[$value['field']] = str_replace('_', ' ', $row['published_profile1']);
           				}
           				else if('professional_competencies'==$value['field']){
           					$row[$value['field']] = $ci->common_model->getUserProfessionalCompetencies($row['id']);
           					$row[$value['field']] = truncateTableColumn($row[$value['field']],MAX_LEG,MIN_LEG);
           				}
           				else if('skills_trades'==$value['field']){
           					$row[$value['field']] = $ci->common_model->getUserSkilledTrades($row['id']);
           					$row[$value['field']] = truncateTableColumn($row[$value['field']],MAX_LEG,MIN_LEG);
           				}
           				else if('manufacturers'==$value['field']){
           					$row[$value['field']] = $ci->common_model->getManufacturers($row['id']);
           					$row[$value['field']] = truncateTableColumn($row[$value['field']],MAX_LEG,MIN_LEG);
           				}
           				else if('manufacturer_model'==$value['field']){
           					$row[$value['field']] = $ci->common_model->getUserManufacturerModel($row['id']);
           					$row[$value['field']] = truncateTableColumn($row[$value['field']],MAX_LEG,MIN_LEG);
           				}
           				else if('created_at'==$value['field'] || 'updated_at'==$value['field'] || 'last_sign_in_at_u'==$value['field']){
           					$row[$value['field']] = date("m/d/Y H:i",strtotime($row[$value['field']]));
           				}
           				else if('available_date'==$value['field']){
           					$row[$value['field']] = date("m/d/Y",strtotime($row[$value['field']]));
           				}
           				else if('photo_file_name'==$value['field']){
           					$row[$value['field']] = '<img onerror="this.src=\''.FRONTEND_PATH.'assets/frontend/images/contractor-thumb-image-icon.jpg\';" src="https://s3.amazonaws.com/'.S3_BUCKET.'/user_photo/thumb/'.$row['id'].'-'.$row['photo_file_name'].'" class="mCS_img_loaded">';
           				}
           				else if('aa_experience'==$value['field']){
           					$row[$value['field']] = '';
           					if($row['total_experience_year']!="0" && $row['total_experience_year']!='')
           						$row[$value['field']].=$row['total_experience_year']." Years";
           					if($row['total_experience_month']!="0" && $row['total_experience_month']!='')
           						$row[$value['field']].=$row['total_experience_month']." Months";
           				}
           				$html.='<td class="whiteSpace" align="left">'.@$row[$value['field']].'</td>';
           			}
           		$html.='</tr>';
           }
       	}
       	else{
       		$html.='<tr><td  colspan="12">No Record Found.</td></tr>';
       	}
		$html.='</tbody>';
		return $html;
	}

	if (!function_exists('allContractorHeading')) {
		function allContractorHeading(){
			return array(
	                       "Action" => array(
	                          "name" => 'Action',
	                          "sort" => false,
	                          "field" => "",
	                          "sort_field" => "",
	                       ),
	                       "u.id" => array(
	                          "name" => 'ID',
	                          "sort" => true,
	                          "field" => "id",
	                          "sort_field" => "u.id",
	                       ),
	                       "photo_file_name" => array(
	                          "name" => 'Profile Photo',
	                          "sort" => false,
	                          "field" => "photo_file_name",
	                          "sort_field" => "photo_file_name",
	                       ),
	                       "u.name" => array(
	                          "name" => 'Name',
	                          "sort" => true,
	                          "field" => "name",
	                          "sort_field" => "u.name",
	                       ),
	                       "u.profile_status" => array(
	                          "name" => 'Completeness',
	                          "sort" => true,
	                          "field" => "profile_status",
	                          "sort_field" => "u.profile_status",
	                       ),
	                       "u.email" => array(
	                          "name" => 'Email',
	                          "sort" => true,
	                          "field" => "email",
	                          "sort_field" => "u.email",
	                       ),
	                       "u.status" => array(
	                          "name" => 'Account Activation',
	                          "sort" => true,
	                          "field" => "status",
	                          "sort_field" => "u.status",
	                       ),
	                       "u.published_profile" => array(
	                          "name" => 'Profile Status',
	                          "sort" => true,
	                          "field" => "published_profile",
	                          "sort_field" => "published_profile1",
	                       ),
	                       "u.next_availability_date" => array(
	                          "name" => 'Next Availability Date',
	                          "sort" => true,
	                          "field" => "available_date",
	                          "sort_field" => "available_date",
	                       ),
	                       "personal_files" => array(
	                          "name" => 'Personal Files',
	                          "sort" => false,
	                          "field" => "personal_files",
	                          "sort_field" => "personal_files",
	                       ),
	                       "u.likend_in_link" => array(
	                          "name" => 'Linkedin Profile',
	                          "sort" => true,
	                          "field" => "likend_in_link",
	                          "sort_field" => "u.likend_in_link",
	                       ),
	                       "u.phone_no" => array(
	                          "name" => 'Phone No',
	                          "sort" => true,
	                          "field" => "phone_no",
	                          "sort_field" => "u.phone_no",
	                       ),
	                       "po.name" => array(
	                          "name" => 'Primary Occupation',
	                          "sort" => true,
	                          "field" => "primary_occupation",
	                          "sort_field" => "primary_occupation",
	                       ),
	                       "aa_experience" => array(
	                          "name" => 'Automation Experience',
	                          "sort" => false,
	                          "field" => "aa_experience",
	                          "sort_field" => "aa_experience",
	                       ),
	                       "u.base_hourly_rate" => array(
	                          "name" => 'Base Rate',
	                          "sort" => true,
	                          "field" => "base_hourly_rate",
	                          "sort_field" => "convert(u.base_hourly_rate, decimal)",
	                       ),
	                       "ul.country" => array(
	                          "name" => 'Country',
	                          "sort" => true,
	                          "field" => "country",
	                          "sort_field" => "ul.country",
	                       ),
	                       "ul.state" => array(
	                          "name" => 'State',
	                          "sort" => true,
	                          "field" => "state",
	                          "sort_field" => "ul.state",
	                       ),
	                       "ul.city" => array(
	                          "name" => 'City',
	                          "sort" => true,
	                          "field" => "city",
	                          "sort_field" => "ul.city",
	                       ),
	                       "ul.zip" => array(
	                          "name" => 'Zipcode',
	                          "sort" => true,
	                          "field" => "zip",
	                          "sort_field" => "ul.zip",
	                       ),
	                       "u.about_me" => array(
	                          "name" => 'About Me',
	                          "sort" => true,
	                          "field" => "about_me",
	                          "sort_field" => "u.about_me",
	                       ),
	                       "u.premium" => array(
	                          "name" => 'Premium',
	                          "sort" => true,
	                          "field" => "premium",
	                          "sort_field" => "u.premium",
	                       ),
	                       "u.engagement_type" => array(
	                          "name" => 'Engagement Type',
	                          "sort" => true,
	                          "field" => "engagement_type",
	                          "sort_field" => "u.engagement_type",
	                       ),
	                       "uo1.name" => array(
	                          "name" => 'Owner',
	                          "sort" => true,
	                          "field" => "con_owner",
	                          "sort_field" => "con_owner",
	                       ),
	                       /*array(
	                          "name" => 'Interview Questions',
	                          "sort" => true,
	                          "field" => "about_me",
	                       ),*/
	                       "manufacturers" => array(
	                          "name" => 'Manufacturers',
	                          "sort" => true,
	                          "field" => "manufacturers",
	                          "sort_field" => "manufacturers",
	                       ),
	                       "manufacturer_model" => array(
	                          "name" => 'Manufacturer Models',
	                          "sort" => true,
	                          "field" => "manufacturer_model",
	                          "sort_field" => "manufacturer_model",
	                       ),
	                       "skills_trades" => array(
	                          "name" => 'Skills Trades',
	                          "sort" => true,
	                          "field" => "skills_trades",
	                          "sort_field" => "skills_trades",
	                       ),
	                       "professional_competencies" => array(
	                          "name" => 'Professional Competencies',
	                          "sort" => true,
	                          "field" => "professional_competencies",
	                          "sort_field" => "professional_competencies",
	                       ),
	                       "applications" => array(
	                          "name" => 'Applications',
	                          "sort" => true,
	                          "field" => "applications",
	                          "sort_field" => "applications",
	                       ),
	                       "occupations" => array(
	                          "name" => 'Occupations',
	                          "sort" => true,
	                          "field" => "occupations",
	                          "sort_field" => "occupations",
	                       ),
	                       "industries" => array(
	                          "name" => 'Industries',
	                          "sort" => true,
	                          "field" => "industries",
	                          "sort_field" => "industries",
	                       ),
	                       "tools_owned" => array(
	                          "name" => 'Tools, Machines and Equipment Competencies',
	                          "sort" => true,
	                          "field" => "tools_owned",
	                          "sort_field" => "tools_owned",
	                       ),
	                       "software_licenses" => array(
	                          "name" => 'Software Licenses',
	                          "sort" => true,
	                          "field" => "software_licenses",
	                          "sort_field" => "software_licenses",
	                       ),
	                       "languages" => array(
	                          "name" => 'Languages',
	                          "sort" => true,
	                          "field" => "languages",
	                          "sort_field" => "languages",
	                       ),
	                       "degree" => array(
	                          "name" => 'Degree',
	                          "sort" => true,
	                          "field" => "degree",
	                          "sort_field" => "degree",
	                       ),
	                       "school" => array(
	                          "name" => 'School',
	                          "sort" => true,
	                          "field" => "school",
	                          "sort_field" => "school",
	                       ),
	                       "specializations" => array(
	                          "name" => 'Specializations',
	                          "sort" => true,
	                          "field" => "specializations",
	                          "sort_field" => "specializations",
	                       ),
	                       "companies" => array(
	                          "name" => 'WE Companies',
	                          "sort" => true,
	                          "field" => "companies",
	                          "sort_field" => "companies",
	                       ),
	                       "we_occupations" => array(
	                          "name" => 'WE Occupations',
	                          "sort" => true,
	                          "field" => "we_occupations",
	                          "sort_field" => "we_occupations",
	                       ),
	                       "u.created_at" => array(
	                          "name" => 'Created At',
	                          "sort" => true,
	                          "field" => "created_at",
	                          "sort_field" => "u.created_at",
	                       ),
	                       "u.updated_at" => array(
	                          "name" => 'Updated At',
	                          "sort" => true,
	                          "field" => "updated_at",
	                          "sort_field" => "u.updated_at",
	                       ),
	                       "u.last_sign_in_at" => array(
	                          "name" => 'Last Sign In At',
	                          "sort" => true,
	                          "field" => "last_sign_in_at_u",
	                          "sort_field" => "u.last_sign_in_at",
	                       ),
	                    );
		}
	}
}


if (!function_exists('StatusType')) {

    function StatusType() {
        $result = array(
            "" => "Select Status Type",
            "Enabled" => "Enabled",
            "Disabled" => "Disabled",
        );
        return $result;
    }

}


if (!function_exists('ProfileStatus')) {

    function ProfileStatus() {
        $result = array(
            "" => "Select Profile Status ",
            "draft" => "Draft",
            "published" => "Published",
            "sent for approval" => "Sent for approval",
        );
        return $result;
    }

}

if (!function_exists('yesNo')) {

	function yesNo(){
	    return array(
	        "" => "Select", 
	        "Yes" => "Yes", 
	        "No" => "No", 
	    );
	}
}
?>








