<?php
	function getDashboardCartSettings($session_dep_id=''){
		//print_r($session_dep_id);die;

		$dashboardCartParameters = array(									
							
								'Antique'=> array(
												'Orders'=> array(
																array(													
																	'process_link' =>'Manufacturing_department_order',
																	'process_class' =>'darkBlue_box',
																	'process' =>'Deparment Orders',
																	'process_count' =>'orders',
																	'label_gray' =>'ORDERS',
																	'is_process'=> TRUE,
																	

																),

																array(
																	'process_link' =>'Karigar_order_list',
																	'process_class' =>'orange_box',
																	'process' =>'Not Sent To Karigar',
																	'process_count' =>'not_sent_to_karigar',
																	'label_gray' =>'ORDERS',
																	'is_process'=> TRUE,
																												
																),
																
																array(
																	'process_link' =>'Order_sent',
																	'process_class' =>'blue_box',
																	'process' =>'Sent To Karigar',
																	'process_count' =>'sent_to_karigar',
																	'label_gray' =>'ORDERS',
																	'is_process'=> TRUE,
																												
																),
																array(		
																	'process_link' =>'Manufacturing_engaged_karigar',
																	'process_class' =>'green_box',
																	'process' =>'Karigar Engaged',
																	'process_count' =>'engaged_karigar',
																	'label_gray' =>'ORDERS',
																	'is_process'=> TRUE,
																												
																),
													),			
														
								 'Products'=> array(
								 							array(
																	'process_link' =>'Received_orders',
																	'process_class' =>'purple_box',
																	'process' =>'Pending To Be Received',
																	'process_count' =>'received_orders',
																	'label_gray' =>'ORDERS',
																	'is_process'=> TRUE,
																												
																),
																
																/*array(								'process_link' =>'Sending_to_qc_mfg',
																	'process_class' =>'green_box',
																	'process' =>'Received From Karigar',
																	'process_count' =>'sending_to_qc_mfg',
																	'label_gray' =>'PRODUCTS',
																	'is_process'=> TRUE,
																												
																),

															array(												'process_link' =>'Received_receipt',
																	'process_class' =>'pink_box',
																	'process' =>'Pending QC',
																	'process_count' =>'received_receipt_mfg',
																	'label_gray' =>'PRODUCTS',
																	'is_process'=> TRUE,
																												
																),
																array(
																	'process_link' =>'Manufacturing_quality_control?status=rejected',
																	'process_class' =>'red_box',
																	'process' =>'QC Rejected',
																	'process_count' =>'rejected_qc',
																	'label_gray' =>'PRODUCTS',
																	'is_process'=> TRUE,
																												
																),*/
																/*array(
																	'process_link' =>'repair_product_list',
																	'process_class' =>'yellow_box',
																	'process' =>'Repair Stock',
																	'process_count' =>'repair_product',
																	'label_gray' =>'PRODUCTS',
																	'is_process'=> TRUE,
																												
																),
																array(
																	'process_link' =>'repair_product',
																	'process_class' =>'dark_purple_box',
																	'process' =>'Sent for Repair',
																	'process_count' =>'sent_to_repair_product',
																	'label_gray' =>'PRODUCTS',
																	'is_process'=> TRUE,
																												
																),
																array(
																	'process_link' =>'difference_product',
																	'process_class' =>'orange_box',
																	'process' =>'Difference',
																	'process_count' =>'difference_product',
																	'label_gray' =>'PRODUCTS',
																	'is_process'=> TRUE,
																												
																),*/
												),				
							
																
						   	   'Tagging'=> array(	
						   	   								array(
						   	 										'process_link' =>'Manufacturing_quality_control?status=complete',
																	'process_class' =>'yellow_box',
																	'process' =>'Pending For Tagging',
																	'process_count' =>'approved_qc',
																	'label_gray' =>'PRODUCTS',
																	'is_process'=> TRUE,
																												
																),
																array(
																	'process_link' =>'tag_products/create',
																	'process_class' =>'dark_purple_box',
																	'process' =>'Tagged Products',
																	'process_count' =>'tag_products',
																	'label_gray' =>'PRODUCTS',
																	'is_process'=> TRUE,
																												
																),
																array(
																	'process_link' =>'Few_box',
																	'process_class' =>'darkBlue_box',
																	'process' =>'Few box',
																	'process_count' =>'few_weight',
																	'label_gray' =>'PRODUCTS',
																	'is_process'=> TRUE,
																												
																),
												),				
			    'Kundan Karigar Products'=> array(
																array(
																	'process_link' =>'kundan_karigar?status=complete',
																	'process_class' =>'blue_box',
																	'process' =>'Sent To Karigar',
																	'process_count' =>'kundan_complete',
																	'label_gray' =>'ORDERS',
																	'is_process'=> TRUE,
																												
																),
																/*array(
																	'process_link' =>'kundan_QC?status=send_qc_pending',
																	'process_class' =>'green_box',
																	'process' =>'Send To QC',
																	'process_count' =>'sending_to_kundan_qc_mfg',
																	'label_gray' =>'ORDERS',
																	'is_process'=> TRUE,
																												
																),
																array(
																	'process_link' =>'kundan_QC?status=pending',
																	'process_class' =>'yellowGold_box',
																	'process' =>'Pending QC',
																	'process_count' =>'received_receipt',
																	'label_gray' =>'ORDERS',
																	'is_process'=> TRUE,
																												
																),*/
																array(
																	'process_link' =>'karigar_receive_order',
																	'process_class' =>'darkGreen_box',
																	'process' =>'Received From Karigar',
																	'process_count' =>'receive_order',
																	'label_gray' =>'ORDERS',
																	'is_process'=> TRUE,
																												
																),
																/*array(
																	'process_link' =>'kundan_QC?status=rejected',
																	'process_class' =>'orange_box',
																	'process' =>'QC Rejected',
																	'process_count' =>'kun_qc_rejected',
																	'label_gray' =>'PRODUCTS',
																	'is_process'=> TRUE,
																												
																),*/
											),	
							 'Ready Products'=> array(
																array(
																	'process_link' =>'ready_product',
																	'process_class' =>'dark_purple_box',
																	'process' =>'Ready Products',
																	'process_count' =>'ready_products',
																	'label_gray' =>'PRODUCTS',
																	'is_process'=> TRUE,
																												
																),
																/*array(
																	'process_link' =>'ready_product?status=pending',
																	'process_class' =>'yellow_box',
																	'process' =>'Pending To Be Approved By Sales',
																	'process_count' =>'ready_products_pending_approved',
																	'label_gray' =>'PRODUCTS',
																	'is_process'=> TRUE,
																												
																),
																array(
																	'process_link' =>'ready_product/approved_by_sale',
																	'process_class' =>'green_box',
																	'process' =>'Approved By Sales',
																	'process_count' =>'sales_approved',
																	'label_gray' =>'PRODUCTS',
																	'is_process'=> TRUE,
																),
																array(
																	'process_link' =>'ready_product/rejected_by_sale',
																	'process_class' =>'red_box',
																	'process' =>'Rejected By Sales',
																	'process_count' =>'sales_rejected',
																	'label_gray' =>'PRODUCTS',
																	'is_process'=> TRUE,
																),*/
											),	

								 'Sales Return Products'=> array(
																array(
																	'process_link' =>'sales_return_product',
																	'process_class' =>'purple_box',
																	'process' =>'Sales return',
																    'process_count'=>'sales_return_product',
																	'label_gray' =>'PRODUCTS',
																	'is_process'=> TRUE,
																												
																),
															
											),						
								),

						
				);			


		return $dashboardCartParameters[$session_dep_id];

	}
?>	


