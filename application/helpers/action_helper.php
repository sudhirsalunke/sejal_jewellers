<?php
  
    function bannerAction($row){
        $ci = &get_instance();
        $controllername = $ci->uri->segment(2);
        $html = '<a onclick="open_master_form('."'$controllername'".','."'edit'".','.$row['id'].');" href="javascript:void(0);">Edit</a></br>
            <a href="javascript:void(0);" onclick="delete_master('.$row['id'].',this,'."'$controllername'".')"  id="'.$controllername.$row['id'].'">Delete</a>';
        return $html;
    }

    function bannerImage($row){
        $ci = &get_instance();
        if($row['file_name'] != ''){
            $html = '<img style="width: 50%;height: auto;" src="'.FRONTEND_PATH.'cover_banner/'.$row['file_name'].'" >';
        }
        else{
            $html = 'No Image';
        }
        return $html;
    }

    function workStatusAction($row){
      $ci = &get_instance();
      return $ci->load->View('admin/work_status/action',array('data' => $row),true);
    }
    function requestedAction($row){
      $ci = &get_instance();
      return $ci->load->View('admin/requested/action',array('data' => $row),true);
    }
    function viewedAction($row){
      $ci = &get_instance();
      return $ci->load->View('admin/viewed/action',array('data' => $row),true);
    }
    function filtersAction($row){
      $ci = &get_instance();
      return $ci->load->View('admin/filters/action',array('data' => $row),true);
    }
    function sharedAction($row){
      $ci = &get_instance();
      return $ci->load->View('admin/shared/action',array('data' => $row),true);
    }

    function workrequestAction($row){
        $ci = &get_instance();
        $html='<a target="_blank" href="'.FRONTEND_PATH.'work_request/index/'.$row['wr_id'].'">View Profile</a><br /><a target="_blank" href="'.FRONTEND_PATH.'work_request/dashboard/'.$row['wr_id'].'">Dashboard</a><br /><a href="javascript:void(0);" onclick=meChat_Open('.$row['business_id'].',\''.$row['wr_id'].'\'); class="chat_to"><span class="icon-Ask-a-Question"></span>Ask A Question</a><br /><a href="javascript:void(0);" onclick="get_comment(\''.FRONTEND_PATH.'\',\''.$row['wr_id'].'\',\'wr\');" >Profile Record</a>';
        if (ENVIRONMENT != 'production') {
            $html.='';
        }
        return $html;
    }

/*    function statusAction($row,$controllername){
        $ci = &get_instance();
        $html = '';
        if(isset($row['status']) && $row['status'] !='' && isset($row['id']) && $row['id'] !=''){
            if($controllername !='user'){
                if($row['status'] == 'confirmed'){
                    $html .= '</br><a href="javascript:void(0);" onclick="changeStatus('.$row['id'].',this,'."'rejected'".',\''.$controllername.'\')"  id="reject_'.$row['id'].'">Reject</a>';
                }
                else if($row['status'] == 'pending'){
                    $html .= '</br><a href="javascript:void(0);" onclick="changeStatus('.$row['id'].',this,'."'confirmed'".',\''.$controllername.'\')" id="confim_'.$row['id'].'">Confirm</a></br>
                    <a href="javascript:void(0);" onclick="changeStatus('.$row['id'].',this,'."'rejected'".',\''.$controllername.'\')"  id="reject_'.$row['id'].'">Reject</a>';
                }
                else if($row['status'] == 'rejected'){
                    $html .= '</br><a href="javascript:void(0);"  onclick="changeStatus('.$row['id'].',this,'."'confirmed'".',\''.$controllername.'\')"  id="confim_'.$row['id'].'">Confirm</a>';
                }
            }
        }
        return $html;
    }*/

    function actualContractorCount($row){
        $ci = &get_instance();
        $html = '';
        if($row['contractor_count_report'] == null || $row['contractor_count_report'] ==''){
            $html = 0;
        }
        else{
            $html = $row['contractor_count_report'];
        }
        return $html;
    }
    function actualWrCount($row){
        $ci = &get_instance();
        $html = '';
        if($row['wr_count_report'] == null || $row['wr_count_report'] ==''){
            $html = 0;
        }
        else{
            $html = $row['wr_count_report'];
        }
        return $html;
    }
    function actualManuCount($row){
        $ci = &get_instance();
        $html = '';
        if($row['manufacturer_model_count_report'] == null || $row['manufacturer_model_count_report'] ==''){
            $html = 0;
        }
        else{
            $html = $row['manufacturer_model_count_report'];
        }
        return $html;
    }

    function contractorLinkDetails($row,$controllername){
        $html = '';
        if(isset($row['contractor_count_report']) && $row['contractor_count_report'] != 0 && isset($row['id']) && $row['id'] !=''){
            $html = '<br /><a target="_blank" href="'.ADMIN_PATH.'contractor/index/'.$controllername.'/'.$row['id'].'"> View Proffessionals </a>';
        }
        return $html;
    }
    function workRequestLinkDetails($row,$controllername){
        $html = '';
        if(isset($row['wr_count_report']) && $row['wr_count_report'] != 0){
            $html = '<br /><a target="_blank" href="'.ADMIN_PATH.'work_request/index/'.$controllername.'/'.$row['id'].'"> View Works </a>';
        }
        return $html;
    }
    function manuModelLinkDetails($row,$controllername){
        $html = '';
        if(isset($row['manufacturer_model_count_report']) && $row['manufacturer_model_count_report'] != 0){
            $html = '<br /><a target="_blank" href="'.ADMIN_PATH.'manufacture_model/index/'.$row['id'].'"> View Manufacture Models </a>';
        }
        return $html;
    }
    function manuLogo($row){
        $ci = &get_instance();
        if($row['manufacture_logo_name'] != ''){
            $html = '<img src="https://s3.amazonaws.com/'.S3_BUCKET.'/admin/manufacture/thumb/'.$row['manufacture_logo_name'].'" >';
        }
        else{
            $html = 'No Image';
        }
        return $html;
    }
    function schoolLogo($row){
        $ci = &get_instance();
        if($row['school_logo_name'] != ''){
            $html = '<img src="https://s3.amazonaws.com/'.S3_BUCKET.'/admin/school/thumb/'.$row['school_logo_name'].'" >';
        }
        else{
            $html = 'No Image';
        }
        return $html;
    }  
    function companyLogo($row){
        $ci = &get_instance();
        if($row['company_logo_name'] != ''){
            $html = '<img src="https://s3.amazonaws.com/'.S3_BUCKET.'/admin/company/thumb/'.$row['company_logo_name'].'" >';
        }
        else{
            $html = 'No Image';
        }
        return $html;
    }  


    function fileswithname($fileData){
    if(!is_array($fileData)){
        $fileData = getallFileData($fileData);
    }
    $id = $fileData['id'];
    $concate_files = '';
    if(!empty($fileData['resume_file_name']) && is_url_exist('https://s3.amazonaws.com/'.S3_BUCKET.'/resume/'.$id."-".$fileData['resume_file_name']))
        $concate_files .= '<li><a target="_blank" href="https://s3.amazonaws.com/'.S3_BUCKET.'/resume/'.$id."-".$fileData['resume_file_name'].'" target="_blank">Resume</a></li>';
    else
        $concate_files .= '';
    if(!empty($fileData['license_or_state_doc_file_name']) && is_url_exist('https://s3.amazonaws.com/'.S3_BUCKET.'/license_or_state_doc/original/'.$id."-".$fileData['license_or_state_doc_file_name']))
        $concate_files .= '<li><a target="_blank" href="https://s3.amazonaws.com/'.S3_BUCKET.'/license_or_state_doc/original/'.$id."-".$fileData['license_or_state_doc_file_name'].'" target="_blank">License or State ID</a></li>';
    else
         $concate_files .= '';
         
    if(!empty($fileData['drug_test_document_file_name']) && is_url_exist('https://s3.amazonaws.com/'.S3_BUCKET.'/drug_test_document/'.$id."-".$fileData['drug_test_document_file_name']))
         $concate_files .= '<li><a target="_blank" href="https://s3.amazonaws.com/'.S3_BUCKET.'/drug_test_document/'.$id."-".$fileData['drug_test_document_file_name'].'" target="_blank">Drug Test Certificate</a></li>';
    else
         $concate_files .= '';
    if(!empty($fileData['background_test_document_file_name']) && is_url_exist('https://s3.amazonaws.com/'.S3_BUCKET.'/background_test_document/'.$id."-".$fileData['background_test_document_file_name']))
         $concate_files .= '<li><a target="_blank" href="https://s3.amazonaws.com/'.S3_BUCKET.'/background_test_document/'.$id."-".$fileData['background_test_document_file_name'].'" target="_blank">Background Check Certificate</a></li>';
    else
         $concate_files .= '';
     //echo $concate_files; exit;
    return $concate_files;
}
function getallFileData($id){
    $ci = &get_instance();
    $ci->db->select('resume_file_name,license_or_state_doc_file_name,drug_test_document_file_name,background_test_document_file_name,user_id id');
    $ci->db->where('user_id',$id);
    $result = $ci->db->get('contractors_reports')->row_array();
    return $result;

}

function adminAction($row){
    $ci = &get_instance();
    $html = '<a onclick=open_master_form("user","edit",'.$row['id'].'); href="javascript:void(0)">Edit</a></br>';
    if($ci->session->userdata('user_id')!=$row['id']){
        $html.= '<a href="javascript:void(0);" onclick="save_friendslist('.$ci->session->userdata("user_id").',\''.$row['id'].'\',\'\',\'\');" class="chat_to"><span class="icon-Ask-a-Question"></span>Ask A Question</a><br />';
    }
    $html.= '<a href="javascript:void(0);" onclick=delete_master('.$row['id'].',this,"user") id="user'.$row["id"].'">Delete</a>';
    return $html;
}
/*function businessAction($row){
    $ci = &get_instance();
    $html='<a onclick=open_master_form("auth_popup","business_edit",'.$row['business_id'].')  href="javascript:void(0)">Edit</a><br /><a target="_blank" href="'.FRONTEND_PATH.'customers/dashboard/'.$row['business_id'].'">Dashboard</a><br />';
    if($ci->session->userdata('admin_role')=="Business admin" || $ci->session->userdata('admin_role')=="Super admin"){
        $html.='<a href="javascript:void(0);" onclick="save_friendslist('.$ci->session->userdata("user_id").',\''.$row['business_id'].'\',\'\',\'\');" class="chat_to"><span class="icon-Ask-a-Question"></span>Ask A Question</a><br />';
    }
    if (ENVIRONMENT != 'production') {
       $html.=  '<a onclick="customer_send_cron_mail(\''.$row['business_id'].'\')" href="javascript:void(0);">Send Mail</a><br />';
    }
    $html.='<a onclick=delete_record_admin("'.$row['business_id'].'","customer",this)  href="javascript:void(0)">Delete</a>';
    return $html;
}
*/


function gettableheaders($table_heading){
    if(is_array($table_heading) && $table_heading !=''){
        $headers = $table_heading;
    }
    else{
        $headers = $table_heading();
    }
    $headings = array();
    foreach($headers as $key => $value){
        $headings[$value[1]] = $value[0];
    }
    return $headings;
}
function gettablecheckbox($table_heading){
    if(is_array($table_heading) && $table_heading !=''){
        $headers = $table_heading;
    }
    else{
        $headers = $table_heading();
    }
    $headings = array();
    foreach($headers as $key => $value){
        $headings[$value[1]] = $value[0];
    }
    return $headings;
}

function getTableData($tabledata,$table_heading){
   // echo "<pre>";print_r($tabledata);exit;
    if(is_array($table_heading) && $table_heading !=''){
       $headingdata =  $table_heading;
    }else{
        $headingdata = $table_heading();
    }
    $databaseheading = array();
    $table_record = array();
 //echo "<pre>";print_r($headingdata);exit;
    foreach ($tabledata as $key1 => $value) {
        foreach ($value as $datafield => $fieldvalue) {
            foreach ($headingdata as $key => $headingdatabasename) {
                $databaseheading[$key] = $headingdatabasename[1];
                // echo "<pre>";print_r($table_record[$key1]);exit;
                if($datafield == 'qc_id'){
                    $table_record[$key1][$datafield] = 0;
                }
                else{
                    $table_record[$key1][$datafield] = $fieldvalue;
                }
            }
            if($datafield=='featured'){
                $table_record[$key1][$datafield] = $fieldvalue;
            }
        }
    }
    return $table_record;
}

function getActions($row,$table_name,$url,$select_url){
    // echo "<pre>";print_r($ci->uri->segment(2));exit;
    $html = '';
    $ci = &get_instance();
    $controllername = $ci->uri->segment(1);
    $ci = &get_instance();
    $segment = $ci->uri->segment(2);
    //print_r($segment);die;
   //echo "<pre>";print_r($table_name);exit;
    $getData = $ci->input->get();
    if($table_name =='Amend_products' && $segment==''){
     $html .= getView($row,$controllername);  
       return $html;      
    }else if($table_name =='Amend_products' && $segment =='view'){ 

    $html .= getAmendAction($controllername,$row,$table_name);        
    return $html;
    }else{
         return $html; 
        } 
}

function getView($row,$controllername){
    $html ='';

    if(isset($row['id'])){
        $html.='<a class="btn btn-primary small loader-hide  btn-sm" href="'.ADMIN_PATH.$controllername.'/view/'.$row['id'].'">VIEW</a>';
  

    }
    return $html;
}

function getAmendAction($controllername,$row,$table_name)
{    //print_r($table_name);die;
    if($table_name=='Amend_products')
    return sendToKarigar($row,$controllername);
 if($table_name=='Amend_products')
    return amendCheckbox ($row,$controllername);
  
}
function sendToKarigar($row,$controllername){
    $html ='';
    if(isset($row['id'])){
         $html .= '<a onclick="Send_amend_products('.$row['id'].')"><button id="b_'.$row['id'].'" class="btn btn-info small loader-hide  btn-sm" name="commit" type="button">SEND TO KARIGAR</button></a>&nbsp;&nbsp;<a onclick="add_to_stock('.$row['id'].')"><button id="s_'.$row['id'].'" class="btn btn-info small loader-hide  btn-sm" name="commit" type="button">ADD TO STOCK</button></a>';
   }
    return $html;
}
function amendCheckbox($row,$controllername){
    $html ='';
    if(isset($row['id'])){
         $html .= '<a onclick="Send_amend_products('.$row['id'].')"><button id="b_'.$row['id'].'" class="btn btn-info small loader-hide  btn-sm" name="commit" type="button">SEND TO KARIGAR</button></a>&nbsp;&nbsp;<a onclick="add_to_stock('.$row['id'].')"><button id="s_'.$row['id'].'" class="btn btn-info small loader-hide  btn-sm" name="commit" type="button">ADD TO STOCK</button></a>';
   }
    return $html;
}


function deleteRecord($row,$controllername){
    $ci = &get_instance();
    $html ='';
    if($controllername !='user' && $controllername !='manufacturers'){
        if(isset($row['contractor_count_report']) && $row['contractor_count_report'] == 0 && ($controllername =='referrals' || $controllername =='degree' || $controllername=='specializations' || $controllername=='professional_competencies' || $controllername=='companies' || $controllername=='school')){
            $html.= '<br/><a href="javascript:void(0);" onclick="delete_master('.$row['id'].',this,'."'$controllername'".')"  id="'.$controllername.$row['id'].'">Delete</a>';
        }
        else if(isset($row['contractor_count_report']) && isset($row['wr_count_report']) && $row['contractor_count_report'] == 0 && $row['wr_count_report'] == 0 ){
           $html.= '<br/><a href="javascript:void(0);" onclick="delete_master('.$row['id'].',this,'."'$controllername'".')"  id="'.$controllername.$row['id'].'">Delete</a>';
        }
    }
    else if(isset($row['contractor_count_report']) && isset($row['wr_count_report']) && isset($row['contractor_count_report']) && $controllername =='manufacturers' && $row['contractor_count_report'] == 0 && $row['wr_count_report'] == 0 && $row['manufacturer_model_count_report'] == 0){
        $html.= '<br/><a href="javascript:void(0);" onclick="delete_master('.$row['id'].',this,'."'$controllername'".')"  id="'.$controllername.$row['id'].'">Delete</a>';
    }
    else if($controllername=='user'){
        $html.= '<br/><a href="javascript:void(0);" onclick="delete_master('.$row['id'].',this,'."'$controllername'".')"  id="'.$controllername.$row['id'].'">Delete</a>';
        if($ci->session->userdata('user_id')!=$row['id'])
            $html.= '<br/><a href="javascript:void(0);" onclick="save_friendslist('.$ci->session->userdata('user_id').','.$row['id'].',\'\',\'\')"  id="'.$controllername.$row['id'].'">Ask A Question</a>';
    }
    return $html;    
}
function getEdit($row,$controllername){
    $html ='';
    if(isset($row['id']) || isset($row['ip_address_id'])){
        $html.='<a onclick="open_master_form('."'$controllername'".','."'edit'".','.$row['id'].');" href="javascript:void(0);">Edit</a>';
    }
    return $html;
}
function getFeatured($row,$controllername){
    $html ='';
    if(($controllername == 'skilled_trades' || $controllername =='application')&& $row['featured'] == 1){
        $html.='<input onclick="mark_as_fetured(this,'.$row['id'].','."'$controllername'".')" checked="checked" type="checkbox">Featured<br />';
    }
    else if(($controllername == 'skilled_trades' || $controllername =='application')&& $row['featured'] == 0){
        $html.='<input onclick="mark_as_fetured(this,'.$row['id'].','."'$controllername'".')" type="checkbox">Featured<br />';
    }
    return $html;
}

function getRemoveAndReplace($row,$getData,$controllername,$url,$select_url){
    $html = '';
    if(isset($row['id']) && $controllername!='user'){
        if(isset($getData['remove_id'])){
        if($getData['remove_id']!=$row['id'])
            $html.= '<a class="btn btn-replace small" onclick="replace_with_this('.$getData['remove_id'].','.$row['id'].',\''.$controllername.'\',\''.$select_url.'\')" href="javascript:void(0);" >Select</a></br><a href="'.$select_url.'">cancel</a><br />';
        }
        else{
            $html.= '<a curent_id="'.$row['id'].'" href="'.$url.'&remove_id='.$row['id'].'" id="replace_'.$controllername.$row['id'].'">Replace & Remove</a></br>';
        }
    }
    return $html;
}

function getColumnData($value,$key,$id=''){
  /*  print_r($key);
    print_r($value);
     die;*/
    if($value == 'confirmed' && $value!=''){
        $value = "Confirmed";
    }
    if($value == 'pending' && $value!=''){
        $value = "Pending";
    }
    if($value == 'rejected' && $value!=''){
        $value = "Rejected";
    }
    if($key == 'manufacture_logo_name' || $key == 'company_logo_name' ||$key =='school_logo_name'){
        $value = getLogoImage($value,$key);
    }
    if($key=='personal_files' && $value!=''){
        $value = fileswithname($id);
    }
    if($key=='query' && $value!=''){
        $value = filter_concat($value);
    }
    if($key=='product_code' && $value!=''){
       
        $value=getprodut_id($value,$key);
     }  
//$product_code = '<a href="'.ADMIN_PATH.'Quality_control/view_remark/'.$product_id.'">'.$value.'</a>';
    return $value;

}
function getprodut_id($value,$key){
        $ci = &get_instance();
        $karigar_id=$ci->uri->segment(3);
        $ci->db->select('ap.qc_id');
        $ci->db->from('Amend_products ap');   
        $ci->db->join('quality_control qc','qc.id = ap.qc_id','left');
        $ci->db->join('corporate_products cp','cp.id = ap.corporate_product_id','left');
        $ci->db->join('Receive_products rp','rp.id = qc.receive_product_id');
        $ci->db->join('email_orders eo','eo.order_id = cp.order_id');
        $ci->db->join('corporate corp','corp.id = eo.corporate');
        $ci->db->join('karigar_master k','k.id = rp.karigar_id');
        $ci->db->where('ap.status','1');
        $ci->db->where('k.id',$karigar_id);
        $result = $ci->db->get()->row_array();          
        $value = '<a href="'.ADMIN_PATH.'Quality_control/view_remark/'.$result['qc_id'].'">'.$value.'</a>';
         return $value;
}

function getLogoImage($value,$key){
    if($key=='manufacture_logo_name' && $value != ''){
        $logo ='<img src="https://s3.amazonaws.com/'.S3_BUCKET.'/admin/manufacture/thumb/'.$value.'" >';
    }
    else if($key=='company_logo_name' && $value != ''){
        $logo = '<img src="https://s3.amazonaws.com/'.S3_BUCKET.'/admin/company/thumb/'.$value.'" >';
    }
    else if($key=='school_logo_name' && $value != ''){
        $logo = '<img src="https://s3.amazonaws.com/'.S3_BUCKET.'/admin/school/thumb/'.$value.'" >';
    }
    else{
        $logo ='No Image';
    }
    return $logo;
}

function getMasterRecords($master_name,$extension,$type='',$type_id='',$count=false){
    $ci = &get_instance();
    $ci->load->library('listing');
    $table_settings = getTableSettings($master_name,$type,$type_id);
   // echo "<pre>";print_r($table_settings);die;
    if(isset($table_settings['headingParam']))
        $old_header = $table_settings['headingFunction']($table_settings['headingParam']);
    else
        $old_header = $table_settings['headingFunction']();
    if(is_array($ci->input->post('selected_columns')) && $ci->input->post('selected_columns') !=''){
        $filter_session_column = getFilterColumns($ci->input->post('selected_columns'));
    }
    // else if($ci->input->post('selected_columns') ==''){
    //     echo "<pre>";print_r("post empty");exit;
    //     $filter_session_column = getFilterColumns($filter_columns='');
    // }
    if(is_array($ci->input->post('ordered_columns')) && $ci->input->post('ordered_columns') !=''){
        $arrange_session_columns = getArrangeColumns($ci->input->post('ordered_columns'));
    }
    if($ci->input->post('selected_columns') == ''){
        $empty_headers = '';
    }
    // echo "<pre>";print_r($old_header);exit;
    // echo "<pre>";print_r($ci->session->userdata('filtered_columns'));exit;
    $new_headers='';
    if(!($ci->session->userdata('arranged_columns'))=='' && ($ci->input->get('ordered_columns') ==1)){
        foreach ($ci->session->userdata('arranged_columns') as $newkey => $newkeyvalue) {
            foreach ($old_header as $oldkey => $oldvalue) {
                if($oldvalue[0] == $newkeyvalue){
                    $new_headers[]=$oldvalue;
                }
            }
        }
    }
    else if($ci->input->post('selected_columns') == '' && $ci->input->get('selected_column') ==1 &&($ci->input->get('order_column') != '' || $ci->input->get('order_by') != '' || $ci->input->get('search')!='')){
        foreach ($ci->session->userdata('filtered_columns') as $newkey => $newkeyvalue) {
            foreach ($old_header as $oldkey => $oldvalue) {
                if($oldkey == $newkeyvalue){
                    $new_headers[]=$oldvalue;
                }
            }
        }
    }
    else if(($ci->input->post('selected_columns') =='') && $ci->input->get('selected_column') ==1){
        // echo "<pre>";print_r($ci->session->userdata('filtered_columns'));exit;
        $new_headers = '';
    }
    else if(!($ci->session->userdata('filtered_columns'))=='' && ($ci->input->get('selected_column') ==1)){
        foreach ($ci->session->userdata('filtered_columns') as $newkey => $newkeyvalue) {
            foreach ($old_header as $oldkey => $oldvalue) {
                if($oldkey == $newkeyvalue){
                    $new_headers[]=$oldvalue;
                }
            }
        }
    }
    
    else{
        $new_headers = $old_header;
    }
    // echo "<pre>";print_r($new_headers);exit;
    $table_settings['new_headers'] = $new_headers;
    if($master_name == 'manufacture_model' && $ci->uri->segment(4) !='')
    {
        $table_settings['where'] = 'manufacturer_id="'.$ci->uri->segment(4).'"';    
    }

    if(isset($count) && $count==true){
       return $ci->listing->getRecordCounts($table_settings);
    }
    $records = $ci->listing->getRecords($table_settings,$extension);
    $records['master_name']=$master_name;
    $records['page_title'] = get_page_title($master_name,$type,$type_id)['index'];
    $records['table_columns'] = $table_settings['headingFunction']();
    $records['filter_columns'] = $new_headers;
    return $records;
}

function getMasterName($type='',$type_id='',$count=false){
    $ci = &get_instance();
    $seg =$ci->uri->segment(2);
    //print_r($seg);die;
    if($seg !=''){
    $masters['name'] =$ci->router->fetch_class().'/'.$seg; 
    }else{
    $masters['name'] = $ci->router->fetch_class();
    }
    //print_r($masters);die;
    $masters['extension'] = isset($_GET['extension']) ? $_GET['extension'] :'';
    $records_count = isset($count) ? $count : '';
    return getMasterRecords($masters['name'],$masters['extension'],$type,$type_id,$records_count);
}

function getFilterColumns($filter_columns){
    $ci = &get_instance();
    $ci->session->set_userdata('filtered_columns',$filter_columns);
}

function getArrangeColumns($arrang_columns){
    $ci = &get_instance();
    $ci->session->set_userdata('arranged_columns',$arrang_columns);
}

?>