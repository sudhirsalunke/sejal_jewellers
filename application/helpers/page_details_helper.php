<?php 
    function get_page_title($page,$type='',$type_id='')
    {
        $titles=array(
            'Amend_products'=>array(

                'index'=>'All QC Amended',
                'add' =>'',           

                'edit'=>'',
                'table_name'=>'Amend_products',
                'refresh_function'=>'',
                'table_heading' => 'AmendHeading',
              
            ),
            'Amend_products/view'=>array(
                'index'=>'QC Amended',

                'add' => array('SEND TO KARIGAR','ADD TO STOCK'),            

                'edit'=>'',
                'table_name'=>'Amend_products',
                'refresh_function'=>'',
                'table_heading' => 'VwAmendHeading',
                'checkbox'=>'checkbox',
            ),
        );
        return $titles[$page];
    }

?>