<?php
defined('BASEPATH') OR exit('No direct script access allowed.');

if (!function_exists('userdataFromUsername')) {

    function userdataFromUsername($username) {
        $ci = &get_instance();
        $ci->db->select('*');
        $ci->db->where('email', $username);
        $query_result = $ci->db->get('admin')->row_array();
        return $query_result;
    }

}
if (!function_exists('insert_max_count')) {

    function insert_max_count($category_id,$item_category_code,$sub_code,$department_id,$category_name) {


        $ci = &get_instance();
        $ci->db->select('max_count');
        $ci->db->from('product_code_max_count');
        $ci->db->where("product_code like concat('%','".$sub_code."' ,'".$item_category_code."','%')");
        $ci->db->where('item_category_code',$item_category_code);
        $ci->db->where('sub_code',$sub_code);
        $ci->db->where('department_id',$department_id);
        $result = $ci->db->get()->row_array();
        // echo $ci->db->last_query();
        // print_r($result);die;
        if(!empty($result)){
            $ci->db->set('max_count',$result['max_count']+1);
            $ci->db->where("product_code like concat('%','".$sub_code."' ,'".$item_category_code."','%')");
            $ci->db->where('item_category_code',$item_category_code);
            $ci->db->where('sub_code',$sub_code);
            $ci->db->where('department_id',$department_id);
            if($ci->db->update('product_code_max_count')){
                return get_successMsg();
                }else{
                  return get_errorMsg();
                }
        }else{
            $add_max_count=array();
            $add_max_count=array(
                    'category_id'=>$category_id,
                    'category'=>$category_name,
                    'item_category_code'=>$item_category_code,
                    'sub_code'=>$sub_code,
                    'product_code'=>$sub_code.$item_category_code,
                    'max_count'=>1,
                    'department_id'=>$department_id,
                );
             if($ci->db->insert('product_code_max_count',$add_max_count)){
                return get_successMsg();
                }else{
                  return get_errorMsg();
                }
        }
    
    }

}

if (!function_exists('get_max_code')) {

    function get_max_code($department_id,$code_name) {
        $ci = &get_instance();
        $ci->db->select("cm.*, ifnull((select max_count+1 from product_code_max_count where product_code like concat('%','".$code_name."' ,cm.code,'%') and department_id='".$department_id."' and  sub_code='".$code_name."' limit 1),1) as max_count");
        $ci->db->from('category_master cm');
        $ci->db->where('cm.department_id',$department_id);
        $result = $ci->db->get()->result_array();
       //echo $ci->db->last_query();die;
       return $result;

    
    }

}

if (!function_exists('order_history_update')) {

    function order_history_update($postdata_id) {
            $ci = &get_instance();
            $order_history_data[]=$postdata_id;
            //print_r($order_history_data);
            $insert_array=array();
            $arr_key=0;
            foreach ($order_history_data as $key => $value) {
                $order_id=$value['order_id'];
                $added_by=$value['added_by'];
                unset($value['k_code']);
                unset($value['k_mob_no']);
                unset($value['order_id']);
                unset($value['added_by']);
                unset($value['created_at']);
                unset($value['updated_at']);
                //print_r($value);
                if(!empty($value)){
                    foreach ($value as $key_o => $value_o) {
                 
                     $insert_array[$arr_key]['filed_name']=$key_o;
                     $insert_array[$arr_key]['filed_value']=$value_o;
                     $insert_array[$arr_key]['order_id']=$order_id;
                     $insert_array[$arr_key]['added_by']=$added_by;
                     $insert_array[$arr_key]['order_change_date']=date('Y-m-d H:i:s');
                     $insert_array[$arr_key]['created_at']=date('Y-m-d H:i:s');
                     $insert_array[$arr_key]['updated_at']=date('Y-m-d H:i:s');
                     $arr_key++;
                    }
                }
            }


            if(!empty($insert_array)){
                $ci->db->insert_batch('customer_orders_history',$insert_array);
                return get_successMsg();
                }else{
                  return get_errorMsg();
                }
           // print_r($insert_array);
           //  die;
        
        
    
    }

}

if (!function_exists('get_default_controller')) {

    function get_default_controller($user_id) {
        $ci = &get_instance();
        $ci->db->select('c.name');
        $ci->db->from('admin a');
        $ci->db->join('admin_controllers_mapping acm','acm.admin_id = a.id','left');
        $ci->db->join('controllers c','c.id = acm.controller_id','left');
        $ci->db->join('menu_master mm','mm.id = c.master_id','left');
        $ci->db->where('a.id',$user_id);
        $ci->db->group_by('a.id');
        $result = $ci->db->get()->row_array();
        return $result['name'];
    }

}

if (!function_exists('get_dashbord_cntrl')) {

    function get_dashbord_cntrl($user_id) {
        $ci = &get_instance();
        $ci->db->select('show_dashboard');
        $ci->db->from('admin a');
        $ci->db->join('admin_controllers_mapping acm','acm.admin_id = a.id','left');
        $ci->db->join('controllers c','c.id = acm.controller_id','left');
        $ci->db->join('menu_master mm','mm.id = c.master_id','left');
        $ci->db->where('a.id',$user_id);
        $ci->db->group_by('a.id');
        $result = $ci->db->get()->row_array();
        return $result['show_dashboard'];
    }

}


if (!function_exists('get_department_map')) {

    function get_department_map($user_id) {
        $data=array();
        $ci = &get_instance();
        $ci->db->select('department_id');
        $ci->db->from('user_department_mapping');
        $ci->db->where('admin_id',$user_id);
        //$ci->db->group_by('admin_id');
        $result = $ci->db->get()->result_array();
        foreach ($result as $val)
       $data['department_id'][] = $val['department_id'];
        return $data;
    }

}
if (!function_exists('user_session')) {

    function user_session($user) {
        $ci = &get_instance();
        $sess_array = array();
        foreach ($user as $row) {
            $sess_array = array(
                'id' => $row->id,
                'email' => $row->email,
                'profilepic' => $row->profilepic,
                'firstname' => $row->firstname,
                'lastname' => $row->lastname,
                'logged_in' => TRUE,
                'isverify' => $row->isverify,
                'mobno' => $row->mobno
            );
            $ci->session->set_userdata($sess_array);
        }
    }

}
if (!function_exists('query_record')) {

    function set_data_table_header($record, $params) {
        $data = array();
        foreach ($record['list'] as $key => $value) {
            $i = 0;
            foreach ($value as $v) {
                $data[$key][$i] = $v;
                $i++;
            }
        }
       
        $json_data = array(
            "draw" => intval($params['draw']),
            "recordsTotal" => intval($record['totalRecords']),
            "recordsFiltered" => intval($record['totalRecords']),
            "data" => $data
        );
         echo '<pre>';
        print_r($json_data);
        echo '</pre>';die;
        return json_encode($json_data);
    }

}
if (!function_exists('get_successMsg')) {

    function get_successMsg($id = "") {
        $msg = 'Added';
        if ($id != "")
            $msg = 'Updated';
        $success_msg = array(
            "status" => "success",
            "data" => $msg . " Successfully!!!"
        );
        return $success_msg;
    }

}
if (!function_exists('get_sendtoModifiedMsg')) {

    function get_sendtoModifiedMsg($id = "") {
        $msg = 'Successfully Send to Modified Orders';
        $success_msg = array(
            "status" => "modified",
            "msg" => $msg,        );
        return $success_msg;
    }

}
if (!function_exists('get_errorMsg')) {

    function get_errorMsg($msg = "") {
        if ($msg == "")
            $msg = "Oops! Error.  Please try again later!!!";
        $error_msg = array(
            "status" => "error",
            "data" => $msg
        );
        return $error_msg;
    }

}

if (!function_exists('!findObjinArray')) {

function findObjinArray($obj, $array) {
        $object = json_decode($obj, true);
        foreach ($array as $key => $value) {
            $value = json_decode($value, true);
            if ($value['lat'] == $object['lat'] && $value['lon'] == $object['lon']) {
                return true;
            }
        }
        return false;
    }

}

if(!function_exists('check_parent_exist')){
    function check_parent_exist($id){
        $ci = &get_instance();
        $ci->db->select('user_mapping_id');
        $ci->db->where('parent_user_id', $id);
        $query_result = $ci->db->get('user_mapping')->row_array();
        return $query_result;

    }
}

if(!function_exists('send_mail')){
    function send_mail($email_id,$role,$password,$name=''){
        $ci = &get_instance();
        $ci->load->model('Emailer_model');
        $ci->load->model('role_model');
        $message['password'] = $password;
        $message['email'] = $email_id;
        $message['name'] = $name;
        $view = 'Emailer/reset_emailer';
        $subject = 'Registration successful on AMS';
        if($role != '6' && $role != '5'){
            $type = $ci->role_model->get_manager($role);
            $message['type'] = $type['name'];
        }elseif ($role == '6') {
            $message['type'] = 'BCC';
        }elseif($role == '5'){
            $message['type'] = 'Money Matter';
        }
        $status = $ci->Emailer_model->send($subject, $message, $view);
        return $status;
    }
}

if(!function_exists('send_approve_emails')){
    function send_approve_emails($id){
        $ci = &get_instance();
        $ci->load->model('Emailer_model');
        $data = $ci->Emailer_model->get_details($id);
        $message = $data;
        $view = 'Emailer/approve_emailer';
        $subject = 'Created activity approved';
        $status = $ci->Emailer_model->send_approve_mails($subject, $message, $view);
    }
}

if(!function_exists('send_email_to_abm')){
    function send_email_to_abm($id){
        $ci = &get_instance();
        $ci->load->model('Emailer_model');
        $ci->db->select('*');
        $ci->db->from('activity_master');
        $ci->db->where('activity_master_id',$id);
        $result = $ci->db->get()->row_array();
        $data = $ci->Emailer_model->get_details($id);
        $abm = $ci->Emailer_model->get_abm($result['abm_id']);
        $data['email_data'] = '';
        $data['email_data']['abm'] = $abm;
        $message = $data;
        $view = 'Emailer/approve_emailer';
        $subject = 'Planned activity approved';
        $status = $ci->Emailer_model->send_approve_mails($subject, $message, $view);
    }
}

if(!function_exists('pagination')){
    function pagination($model,$function,$id){
        $ci = &get_instance();
        $ci->load->model($model);
        $config["base_url"] = base_url() . "Senior_manager/index";
        $config["total_rows"] = $ci->$model->get_count();
        $config["per_page"] = 10;
        $config["uri_segment"] = 3;
        $config['first_link'] = 'First';
        $config['prev_link'] = '&#8592; Previous';
        $config['next_link'] = 'Next &#8594;';
        $config['prev_tag_open'] = '<span class="previous_page" >';
        $config['prev_tag_close'] = '</span>';
        $config['next_tag_open'] = '<span class="next_page">';
        $config['next_tag_close'] = '</span>';
        $config['attributes'] = array('class' => 'myclass');
        $ci->pagination->initialize($config);

        $page = ($ci->uri->segment(3)) ? $ci->uri->segment(3) : 0;
        $data["results"] = $ci->$model->$function($config["per_page"], $page,$id);
        $data["links"] = $ci->pagination->create_links();
        $data["total_rows"] = $config["total_rows"];
        return $data;
    }
}
if(!function_exists('get_av_requirments')){
    function get_av_requirments(){
        $req=array(
            "lcd"=>"LCD",
            "screen"=>"Screen",
            "pointer"=>"Pointer",
            "codeless_mike"=>"Codeless Mike",
            "podium_mike"=>"Podium Mike",
            "collar_mike"=>"Collar Mike",
            "hand_mike"=>"Hand Mike",
            "laptop"=>"Laptop",
            "PA_system"=>"PA system",
        );
        return $req;
    }
}
if(!function_exists('add_comma_in_price')){
    function add_comma_in_price($amount){
        setlocale(LC_MONETARY, 'en_IN');
        $amount = money_format('%!.0n', $amount);
        return $amount;
    }
}

if(!function_exists('imageValidation')){
    function imageValidation($files,$allowed_extensions){
        if(empty($postData['id']) && empty($files)){
            return array("status"=>"fail","msg" => "<p>Please upload product image.</p>");
        }
        else{
            if(!empty($files)){
                $product_resolution = getimagesize($files["tmp_name"]);
                $product_img_extension = pathinfo($files["name"], PATHINFO_EXTENSION);
                if(!in_array($product_img_extension, $allowed_extensions,TRUE)){
                    return array("status"=>"fail","msg" => "<p>Invalid file format of an uploaded file ,Only '".implode(',',$allowed_extensions)." allowed'</p>");
                }
            }
        }
        //return array("status" => "success" ,'msg' => '');
    }
}

if(!function_exists('validate_images')){
 function validate_images($file,$allowed_extensions,$update=false){
    $error['status']=true;
    $error['msg']='';
    if ($update ==false) {
         if (empty($file['name'])) {
        $error['msg']="Please select Image";
        $error['status']=false;
        }
    }
    if (!empty($file['name'])) {
        $img_path = pathinfo($file["name"], PATHINFO_EXTENSION);
        if(!in_array(strtolower($img_path), $allowed_extensions)){
            $error['msg']="Invalid file format, Please Upload ".implode('/',$allowed_extensions)." files.";
            $error['status']=false;
        }
    }
    return $error;
  }
}

if(!function_exists('product_validate_images')){
 function product_validate_images($file,$allowed_extensions){
    $error['status']=true;
    $error['msg']='';
 
         if(empty($file['name']['image'])) {
        $error['msg']="Please select Image";
        $error['status']=false;
        }
  
    if (!empty($file['name']['image'])) {
        $img_path = pathinfo($file["name"]['image'], PATHINFO_EXTENSION);
        if(!in_array(strtolower($img_path), $allowed_extensions)){
            $error['msg']="Invalid file format, Please Upload ".implode('/',$allowed_extensions)." files.";
            $error['status']=false;
        }
    }
    return $error;
  }
}

if(!function_exists('uploadImage')){
    function uploadImage($folder_name,$files)
    {   
        $data=array();
        $ci = &get_instance();
        $filename=array();
            CreateFolderbyname($folder_name);
            if (!empty($files['name'])) {
                $uplaodFileName = RenameUploadFile($files['name']);
                $_FILES['userfile']['name']= date("YmdHis")."_".$uplaodFileName;
                $_FILES['userfile']['type']= $files['type'];
                $_FILES['userfile']['tmp_name']= $files['tmp_name'];
                $_FILES['userfile']['error']= $files['error'];
                $_FILES['userfile']['size']= $files['size'];
                $ci->upload->initialize(set_upload_options($folder_name));
                $upload=$ci->upload->do_upload();
                if($upload)
                {
                   $data=$ci->upload->data();

                }else{
                    echo "<pre>"; print_r($ci->upload->display_errors()); echo "</pre>";
                }
            }
        
          return $data; 
    }
}


if(!function_exists('uploadVideo')){
    function uploadVideo($folder_name,$files)
    {  //  print_r($files);exit;
        $data=array();
        $ci = &get_instance();
        $filename=array();
            CreateFolderbyname($folder_name);
            if (!empty($files['name'])) {
                $uplaodFileName = RenameUploadFile($files['name']);
                $_FILES['userfile']['name']= date("YmdHis")."_".$uplaodFileName;
                $_FILES['userfile']['type']= $files['type'];
                $_FILES['userfile']['tmp_name']= $files['tmp_name'];
                $_FILES['userfile']['error']= $files['error'];
                $_FILES['userfile']['size']= $files['size'];
                $_FILES['userfile']['allowed_type']= $files['allowed_type'];
                $ci->upload->initialize(set_upload_options($folder_name));
                $upload=$ci->upload->do_upload();
                if($upload)
                {
                   $data=$ci->upload->data();

                }else{
                    echo "<pre>"; print_r($ci->upload->display_errors()); echo "</pre>";
                }
            }
        
          return $data; 


    }
}

if (!function_exists('get_Month')) {
    function get_Month($no="") {
        $month_arr = array(
            "01" => "Jan",
            "02" => "Feb",
            "03" => "Mar",
            "04" => "Apr",
            "05" => "May",
            "06" => "Jun",
            "07" => "Jul",
            "08" => "Aug",
            "09" => "Sep",
            "10" => "Oct",
            "11" => "Nov",
            "12" => "Dec",
        );
        if($no!="")
            return $month_arr[$no];
        return $month_arr;
    }
}
if(!function_exists('uploadExcel')){
function uploadExcel($folder_name,$files)
{   
    $ci = &get_instance();
    CreateFolderbyname($folder_name);
        $uplaodFileName = RenameUploadFile($files['file']['name']);
        $_FILES['userfile']['name']= date("YmdHis")."_".$uplaodFileName;
        $_FILES['userfile']['type']= $files['file']['type'];
        $_FILES['userfile']['tmp_name']= $files['file']['tmp_name'];
        $_FILES['userfile']['error']= $files['file']['error'];
        $_FILES['userfile']['size']= $files['file']['size']; 
            $ci->upload->initialize(set_upload_options($folder_name));
            $upload=$ci->upload->do_upload();
            if($upload)
            {
                $filename=$_FILES['userfile']['name'];
            }else{
                echo "<pre>"; print_r($ci->upload->display_errors()); echo "</pre>";
            }
        return $ci->upload->data(); 
    }
}
if(!function_exists('statussort')){
function statussort($a, $b) {
  $a = $a['status'];
  $b = $b['status'];
  if ($a == $b)
    return 0;
  return ($b > $a) ? -1 : 1;
    }
}


if(!function_exists('uploadFiles')){
function uploadFiles($folder_name,$files)
 {   
        $data=array();
        $ci = &get_instance();
        $filename=array();
       
        $limit=sizeof($files);

            for($i=0;$i<$limit;$i++)
            {
                CreateFolderbyname($folder_name[$i]);
                if (!empty($files['file_'.$i]['name'])) {
                    $uplaodFileName = RenameUploadFile($files['file_'.$i]['name']);
                    $_FILES['userfile']['name']= date("YmdHis")."_".$uplaodFileName;
                    $_FILES['userfile']['type']= $files['file_'.$i]['type'];
                    $_FILES['userfile']['tmp_name']= $files['file_'.$i]['tmp_name'];
                    $_FILES['userfile']['error']= $files['file_'.$i]['error'];
                    $_FILES['userfile']['size']= $files['file_'.$i]['size'];
                    $ci->upload->initialize(set_upload_options($folder_name[$i]));
                    $upload=$ci->upload->do_upload();

                    if($upload)
                    {
                            $data[]=$ci->upload->data();
                    }else{
                       $data[] = $ci->upload->display_errors();
                    }
                }
            }
          return $data; 
    }
}



function resize_image($temp_path,$new_path,$size,$name =''){
    $width = $size['width'];
    $height = $size['height'];
    
    $image = new Imagick($temp_path);
    $blur = 1;
    
    //$image->setRegistry($temp_path, '/efs');

    $bestFit = true;
    $image->resizeImage($width, $height, Imagick::FILTER_LANCZOS, $blur, $bestFit);
   // unlink($path_img);
    $image->writeImage($new_path);
    if(!empty($name))
        return $name;
    else
        return $new_path;
}

 function upload_images($folder,$code,$file_name,$tmp_name)
  {
      list($width, $height) = getimagesize($tmp_name);
      $folder_name[0] = $folder.'/'.$code;
      $folder_name[1] = $folder.'/'.$code.'/large';
      $folder_name[2] = $folder.'/'.$code.'/small';
      $folder_name[3] = $folder.'/'.$code.'/thumb';
      $sizes[0] = array('width' => $width,'height' => $height);
      $sizes[1] = array('width' => '640','height' => '640');
      $sizes[2] = array('width' => '200','height' => '200');
      $sizes[3] = array('width' => '60','height' => '60');
      $name =  RenameUploadFile($file_name);
      foreach ($sizes as $k => $size) {
        $new_name =$name;
        if ($k==0) {
            $new_name=$file_name;
        }
        makeDirectory($folder_name[$k]);
        $new_path = resize_image($tmp_name,'./uploads/'.$folder_name[$k].'/'.$new_name,$size,$new_name);
      }
      return $new_path;
  }

function download_shortlisted_images($data){
    if(!is_dir(FCPATH."uploads/images")){
        mkdir(FCPATH."uploads/images/",0777);
    }
    $zip = new ZipArchive();
    if ($zip->open(FCPATH."uploads/images.zip", ZIPARCHIVE::CREATE )!==TRUE) {
        exit("cannot open images.zip");
    }
    if(!empty($data)){

        foreach ($data as $key => $value) {
            // copy(FCPATH."uploads/product/".$value['product_code']."/thumb/".$value['image'], FCPATH."uploads/images/".$value['image']);
        $image = explode('/', $value['image']);
        if(count($image)==3){
            $value['image'] = $image[2];
        }
        // print_r($value['product_code']);
        $ext = pathinfo($value['image'], PATHINFO_EXTENSION);
        $zip->addFile(FCPATH."/uploads/product/".$value['product_code']."/large/".$value['image'],$value['product_code'].'.'.$ext);
      //  $zip->addFile('http://54.251.34.140:9030/uploads/product/SBL13P624/large/sbl13p624.jpg');
//print_r(FCPATH."uploads/product/".$value['product_code']."/large/");
     
        }
    }
    $zip->close();
    ob_clean();
    header("Cache-Control: no-cache, must-revalidate"); // HTTP/1.1
    header('Content-Type: application/x-download');
    //header("Content-type: application/zip"); 
    header("Content-Disposition: attachment; filename=images.zip");
    header("Content-length: " . filesize(FCPATH."uploads/images.zip"));
    header("Pragma: no-cache"); 
    header("Expires: 0"); 
    flush();
    readfile(FCPATH."uploads/images.zip");
    unlink(FCPATH."uploads/images.zip");
    
}
function create_excel_array($data,$header_array){
    foreach ($data as $key => $value) {
        foreach ($header_array as $h_key => $h_value) {
            if(array_key_exists($h_value, $value)){
                $result[$key][$h_value] = $value[$h_value];
            }
        }
    }
    return $result;
  }
  function check_master_in_used($field_name,$id,$table_name)
  {
        $ci = &get_instance();
        $ci->db->select('count(*) as cnt');
        $ci->db->from($table_name);
        $ci->db->where($field_name,$id);
        $result = $ci->db->get()->row_array();
       // echo $ci->db->last_query(); die();
        return $result['cnt'];
  }

  function date_update($table_name,$pk_id,$action,$updated_date,$act)
  {
    $insert_array=array();
    $date = date('Y-m-d H:i:s');
    $ci = &get_instance();
    if($act=='insert')
    {
        $insert_array['table_name'] = $table_name;
        $insert_array['pk_id'] = $pk_id;
        $insert_array['action'] = $action;
        $insert_array[$updated_date] = $date;
        $ci->db->insert('date_update',$insert_array); 
    }
    elseif($act=='update')
    {
        $insert_array[$updated_date] = $date;
        $ci->db->where('pk_id',$pk_id);
        $ci->db->update('date_update',$insert_array); 
    }
    
  }

function corporate_product_update($email_order_id,$status,$sort_Item_number)
  {
    $insert_array=array();
    $ci = &get_instance();
    $insert_array['status'] = $status;
    $ci->db->where('email_order_id',$email_order_id);
    $ci->db->where('sort_Item_number',$sort_Item_number);
    $ci->db->update('corporate_products',$insert_array); 
    
  }

  function get_mid_value($weight_band){
    $median_value="error";
    $weight_band = explode('-',$weight_band);
    if (count($weight_band) == 2) {
       $start = trim($weight_band[0]);
       $end = trim($weight_band[1]);
       $med = ($end-$start)/2;
     
       $median_value = $start+ $med;
    }
    return $median_value;

  }

  function karagir_engaged_total_qty($net_weight,$weight_band,$quantity){
    $mid_value="error";
    if (!empty($net_weight) && $net_weight != 0.00){ 
        $mid_value = $net_weight;
        $total_qty = $quantity*$mid_value;
    }else{
        $mid_value=get_mid_value($weight_band);
        $total_qty = $quantity*$mid_value;
        }       
    
    return $mid_value;
  } 

  function get_corporates(){
    $ci = &get_instance();
    $result = $ci->db->get('corporate')->result_array();
    return $result;
  }

  function date_dmy($date,$empty=""){
     if (!empty($date) && $date != '0000-00-00'){
        return date('d-m-Y',strtotime($date));
     }else{
        return $empty;
     }
  }

function input_types($type=""){
    $input_types=array(1=>"Textbox",2=>"Dropdown",3=>"Checkbox");

     if (!empty($type)){
        return $input_types[$type];
     }else{
        return $input_types;
     }
  }
  
function overall_view_table_headers($key=""){
      $headers = array(
                        'total_qc_goods' => array('#','Product Code','Corporate','Quantity','Weight'),
                        'total_qc_fail_goods' => array('#','Product Code','Corporate','Quantity','Weight'),
                        'total_amended_products'=>array('#','Product Code','Corporate','Quantity','Weight'),
                        'total_goods_in_hallmarking'=>array('#','Product Code','Hallmarking Center','Corporate','Quantity','Weight'),
                        'total_goods_in_qc'=>array('#','Product Code','Corporate','Quantity','Weight'),
                        'total_received_goods_today'=> array('#','Product Code','Corporate','Quantity','Weight'),
                        'total_out_goods_today'=>array('#','Product Code','Corporate','Quantity','Weight'),
                        'total_placed_order'=>array('#','Order Name','Corporate','Total Products','Weight'),
                        'total_pending_order'=>array('#','Order Name','Corporate','Total Products','Weight'),
                        'total_received_order'=>array('#','Order Name','Corporate','Total Products','Weight'),
                        );
      if (empty($headers[$key])) {
         return array('#');
      }else{
        return $headers[$key];
      }
  }

  function jewerlly_detailed_table($key=""){
      $headers = array(
                    'stock_in_hand' => array('#','Product Code','Gross Weight','Net Weight','In Process'),
                    'order_receipts' => array('#','Order Name','Gross Weight','Net Weight'),
                    'recived_weight' => array('#','Order Name','Order Date','Gross Weight','Net Weight','Quantity Placed','Quantity Received'),
                    'order_pending'=>array('#','Order Name','Order Date','Gross Weight','Net Weight','Quantity Placed','Quantity Pending'),
                        );
      if (empty($headers[$key])) {
         return array('#');
      }else{
        return $headers[$key];
      }
  }
function blank_value($value=""){   
     if (!empty($value)){
        return $value;
     }else{
        return $value='-';
     }
  }
function remove_empty_values($data){
    $filters = array();
    if(!empty($data)){
        foreach ($data as $key => $value) {
            if(isset($value['value'])){
                $filters[] = $value;
            }
        }
    }
    return $filters;
  }

function filter_column_name($data){
    $filters_key = array();
    $column_name=getTableHeaderSettings($data);     
    foreach ($column_name as $key => $value){ 
        if(isset($value ['table_column_name']) && !empty($value ['table_column_name']))      
        $filters_key[] = $value ['table_column_name'];           
    }

    return $filters_key;
}

function email_order_id($cp_id)
{
  $ci = &get_instance();  
  $ci ->db->select('email_order_id');
  $ci ->db->where('id',$cp_id);
  $ci ->db->from('corporate_products');
  $result = $ci ->db->get()->row_array();
  return $result['email_order_id'];  
}
function blank_total($value=""){   
     if (!empty($value)){
        return $value;
     }else{
        return $value='0';
     }
  }
function create_excel_error_msg($msg='',$key){
    if(!empty($msg)){
      return $msg.' for row '.($key+1);
    }else{
      return $msg;
    }
}  
if(!function_exists('create_voucher_number')){
   function create_voucher_number($postData, $suffix, $voucher_type) {
    //print_r($postData);die;
        if (!isset($postData['date']) || (isset($postData['date']) && $postData['date'] == '')) {
            return '';
        }
        $date = $postData['date'];
        $voucher_number = '';
        $all_vouchers = get_all_voucher($date, $voucher_type);
        $number = 001;
        if (!$all_vouchers === false) {
            foreach ($all_vouchers as $voucher) {
                if (empty($voucher['voucher_number'])) {
                    continue;
                }

                $parts = array();
                $parts = explode('/', $voucher['voucher_number']);

                if ($parts[1] > $number) {
                    $number = $parts[1];
                }
            }
            $number = ++$number;
        }

        $number = sprintf('%03d', $number);
        $voucher_number = $suffix . '/' . $number . '/' . date('dmy', strtotime($date));
        return $voucher_number;
    }
 }    

if(!function_exists('get_all_voucher')){
     function get_all_voucher($date, $voucher_type) {
        $ci = &get_instance();
        $ci->DB1 = $ci->load->database('account', TRUE);
        $ci->DB1->select('voucher_number');
        $ci->DB1->from('ac_sales_purchase_voucher');
        $ci->DB1->where('date', $date);
        $ci->DB1->where('voucher_type', $voucher_type);
        $query = $ci->DB1->get();
        //echo $ci->DB1->last_query();
        if($query->num_rows() > 0){
            return $query->result_array();
        }else{
            return false;
        }
    }
 }     

if(!function_exists('filter_reponse_array')){
     function filter_reponse_array($data,$column_name){
         $response=array();
         $return_array=array();
         $columns = array('order name','delievery date','parent category id','weight range id','width','size','length','quantity','purity','remark');
         if(!empty($data)){
             foreach ($data as $key => $value) {
                if(in_array($value['filed_name'],$columns)){
                    $response[strtotime($value[$column_name])]['date_str']=strtotime($value[$column_name]);
                    $response[strtotime($value[$column_name])]['date']=$value[$column_name];
                    $response[strtotime($value[$column_name])]['details'][]=$value;
                }
             }
             // print_r($response);die;
         }
         $json_array = array_values($response);
         return $json_array;
     }
}
 

if(!function_exists('get_product_id')){
    function get_product_id($name){
        $ci = &get_instance();
        $ci->db->select('id');
        $ci->db->where('name', $name);
        $query_result = $ci->db->get('parent_category')->row_array();
        return $query_result;

    }
}
if(!function_exists('get_category_id')){
    function get_category_id($code){
        $ci = &get_instance();
        $ci->db->select('id');
        $ci->db->where('code', $code);
        $query_result = $ci->db->get('category_master')->row_array();
        return $query_result;

    }
}