<?php
defined('BASEPATH') OR exit('No direct script access allowed.');

if (!function_exists('decode_item')) {
    function decode_item($item_no) {
      $karatage =  karatage(substr($item_no,0,1));
      $product_master = product_master(substr($item_no,1,3));
      $complexity = complexity(substr($item_no,4,3));
      $weight_band = weight_band(substr($item_no,1,3),substr($item_no,7,1));
      
      $finding_cost = finding_cost(substr($item_no, 16,2));
      $stone_cold = stone_cold(substr($item_no,18,2));
      if (in_array(substr($item_no,1,3), array('BLT','CHN','RNG','BAN'))) {
      	$size_master = size_master(substr($item_no,1,3),substr($item_no, 14,2));
      }else{
      	$size_master="0.0";
      }
      

      return array('karatage' => $karatage,
      				'product_master'=>(!empty($product_master)) ? substr($item_no,1,3) : '',
      				'complexity'=>substr($item_no,4,3),
      				// substr($item_no,4,3)=>$complexity,
      				'weight_band'=>$weight_band,
      				'vendor_code'=>substr($item_no,8,2),
      				'serial_no'=>substr($item_no,10,4),
      				'size_master'=>$size_master,
      				'finding_cost'=>$finding_cost,
      				'stone_cold'=>$stone_cold,
      				);
    }
}

if (!function_exists('karatage')) {
    function karatage($key) {
       $karatage = array(
						'2'=>array('Code' =>'2','Metal'=>'Gold','Purity'=>'916','Karatage'=>'22K'),
						'3'=>array('Code' =>'3','Metal'=>'Gold','Purity'=>'958','Karatage'=>'23k'),
						'6'=>array('Code' =>'6','Metal'=>'Gold','Purity'=>'995','Karatage'=>'24K(99.5)'),
						'4'=>array('Code' =>'4','Metal'=>'Gold','Purity'=>'999','Karatage'=>'24K(99.9)'),
						'7'=>array('Code' =>'7','Metal'=>'Gold','Purity'=>'583','Karatage'=>'14K'),
						'8'=>array('Code' =>'8','Metal'=>'Gold','Purity'=>'750','Karatage'=>'18K'),
						'9'=>array('Code' =>'9','Metal'=>'Gold','Purity'=>'375','Karatage'=>'9K'),
						'S'=>array('Code' =>'S','Metal'=>'Silver','Purity'=>'999','Karatage'=>'999'),
						'A'=>array('Code' =>'A','Metal'=>'Silver','Purity'=>'925','Karatage'=>'92'),
						'P'=>array('Code' =>'P','Metal'=>'Platinum','Purity'=>'950','Karatage'=>'95'),
						'W'=>array('Code' =>'W','Metal'=>'Gold - White','Purity'=>'750','Karatage'=>'18kt'),
						'Y'=>array('Code' =>'Y','Metal'=>'Gold - White','Purity'=>'583','Karatage'=>'14kt'),
						'Z'=>array('Code' =>'Z','Metal'=>'Gold - White','Purity'=>'375','Karatage'=>'9kt'),
						);
		if(!empty($key)){
        	if(array_key_exists($key, $karatage)){
        		return $karatage[$key];
	        }else{
	        	return '';
	        }
        }else{
        	return $karatage;
        }
        
    }
}

if (!function_exists('product_master')) {
    function product_master($key) {
      $product_master=array(
							'CHN'=>'CHAIN',
							'RNG'=>'RING',
							'KAN'=>'KANGAN',
							'MSR'=>'MANGALSUTRA',
							'SET'=>'SET',
							'MIS'=>'Miscellaneous',
							'PST'=>'PENDANT SET',
							'ERG'=>'EARRING',
							'NKL'=>'NECKLACE',
							'BAN'=>'BANGLE',
							'PDT'=>'PENDANT',
							'MSP'=>'MANGALSUTRA PENDANT',
							'MCN'=>'MENS COLLECTION',
							'BLT'=>'BRACELET',
							'ECN'=>'EAR CHAIN',
							'SCD'=>'SET CHHEDIYA',
							'WBT'=>'WAIST BELT',
							'CON'=>'Coin',
							'SPS'=>'Spares',
							'ASC'=>'ASSESORIES',
							'IDL'=>'IDOL',
							'BAO'=>'Oval Bangle',
							'NOP'=>'Nose Pin',
							'NOR'=>'Nose Ring',
							'TAN'=>'Tanmania',
							);
		if(!empty($key)){
        	if(array_key_exists($key, $product_master)){
        		return $product_master[$key];
	        }else{
	        	return '';
	        }
        }else{
        	return $product_master;
        }
        
    }
}

if (!function_exists('complexity')) {
    function complexity($code) {
      $complexity = array(
							'ANS'=>'Antique Special',
							'ATQ'=>'Antique Regular',
							'BCN'=>'Box Chain',
							'CAS'=>'Casting Fancy',
							'CAS'=>'Casting Light Weight',
							'CAS'=>'Casting Superior Rhodium',
							'CAS'=>'Casting Stamping',
							'CAS'=>'Casting',
							'CAS'=>'Casting Rhodium',
							'CAS'=>'Casting Superior',
							'CBR'=>'Coimbatore',
							'CBR'=>'Coimbatore Light Weight',
							'CBR'=>'Coimbatore',
							'CBR'=>'Coimbatore Regular',
							'CBR'=>'Coimbatore Fancy',
							'CBR'=>'Coimbatore Kasu',
							'CBR'=>'Coimbatore Kasu Studded',
							'CBR'=>'Coimbatore Rhodium',
							'CBR'=>'Coimbatore Superior',
							'CBR'=>'Coimbatore Set',
							'CBR'=>'Coimbator Thali Chains',
							'CBR'=>'Coimbator Light Wt',
							'CHR'=>'Chandan Haar',
							'CLT'=>'Calcutti Superior',
							'CLT'=>'Kolkata',
							'CNC'=>'Cnc Machine Cut + Enamel',
							'CNC'=>'Cnc Inner Cut',
							'CNC'=>'Cnc Inner Cut + Rodium',
							'CNC'=>'Cnc + Lazer Joint + Rodium',
							'CNC'=>'Cnc Vakya Style',
							'CNC'=>'Dancing Diamond Bangle + Rodium',
							'CNC'=>'Enamel',
							'CNC'=>'Joint',
							'CNC'=>'CNC Cut Machine',
							'CNC'=>'Cnc Machine Cut + Enamel + Rodium',
							'CNC'=>'CNC Cut Machine Rhodium',
							'CNC'=>'Machine Made - Raiara',
							'CNC'=>'Cnc Superior',
							'CNC'=>'Cnc Superior + Rodium',
							'CNC'=>'Cnc Vakya Style + Rodium',
							'CNC'=>'Diamond',
							'CO2'=>'Gold Coin',
							'CO3'=>'Gold Coin',
							'CO4'=>'Silver Coins',
							'CRT'=>'Cartier',
							'CRT'=>'Cartier Rhodium',
							'CZA'=>'Cubiz Zirconia (Stone) Bangle',
							'CZA'=>'Cubiz Zirconia (Stone) Half Set',
							'CZA'=>'Cubiz Zirconia (Stone) Light Weight',
							'CZA'=>'Cubiz Zirconia (Stone) Fusion',
							'CZA'=>'Cubiz Zirconia (Stone) Fusion Half Set',
							'CZA'=>'Cubic Zirconia',
							'CZA'=>'Cubic Zirconia Superior',
							'CZA'=>'Cubiz Zirconia (Stone) Superior Bangle',
							'CZA'=>'Cubiz Zirconia (Stone) Stamping',
							'DIK'=>'Diamond Kanti',
							'DRA'=>'Antique Designer',
							'DRA'=>'Designer Antique Superior',
							'DRA'=>'Antique Designer',
							'DSR'=>'Designer',
							'DUB'=>'Dubai',
							'ECO'=>'Economy Rhd',
							'ECO'=>'Economy',
							'ECO'=>'Economy',
							'ECO'=>'Economy',
							'EFIG'=>'Electro Form Jewellery',
							'FCY'=>'Import',
							'FCY'=>'Import- Rhd',
							'FCY'=>'Fancy',
							'FCY'=>'Fancy Rhodium',
							'FCY'=>'Fancy Superior',
							'FCY'=>'Fancy - Sup Rhd',
							'GRU'=>'Gheru Polish',
							'HMD'=>'Handmade',
							'HOL'=>'Hollow Superior Rhodium',
							'HOL'=>'Hollow Tube Rodium',
							'HOL'=>'Hollow',
							'HOL'=>'Hollow  Rhodium',
							'HOL'=>'Hollow Tube Yellow',
							'HOL'=>'Hollow Light Weight',
							'IMS'=>'Mesh Chain Italian',
							'IMS'=>'Italian Mesh Chain + Rodium',
							'IMS'=>'Italian Mesh Rhodium',
							'IMS'=>'Mesh Chain',
							'INT'=>'Indo Italian Studded',
							'INT'=>'Indo Italian Yellow',
							'INT'=>'Indo Italian Yellow',
							'INT'=>'Indo Italian Rhodium',
							'INT'=>'Indo Italian Superior',
							'INT'=>'Indo Italian Superior Rhodium',
							'KER'=>'Kerela Hollow',
							'KER'=>'Kerala',
							'KER'=>'Kerela Style + Rodium',
							'KRG'=>'Koorg',
							'KUN'=>'Polki Chakri',
							'KUN'=>'Kundan Studded',
							'KWR'=>'Karwar Studded',
							'MKA'=>'Machine Kanas Yellow',
							'MKA'=>'Machine Kanas Patpatli',
							'MKA'=>'Machine Kanas Rhodium',
							'MNC'=>'Machine',
							'MNC'=>'Economy Rhodium',
							'MNC'=>'Machine Sup',
							'MNC'=>'Machine Made Superior',
							'MNC'=>'Machine Made Superior + Rodium',
							'MNC'=>'Machine',
							'MNC'=>'Machine Made Rhodium',
							'MOH'=>'Mohan Mala',
							'MST'=>'Mumbai Sheet-Std',
							'MST'=>'Mumbai Sheet',
							'MST'=>'Mumbai Sheet Belt',
							'MST'=>'Mumbai Sheet Kangan',
							'MST'=>'Mumbai Sheet Set',
							'MST'=>'Mumbai sheet Nose Pin',
							'NEL'=>'Nellore',
							'NKS'=>'Designer Temple',
							'NKS'=>'Designer Temple Superior',
							'NKS'=>'Temple - Nakashi',
							'NWB'=>'Nawabi Rhodium',
							'NWB'=>'Nawabi',
							'OMG'=>'Omega Fancy Rhodium',
							'OMG'=>'Queen Omega Rhodium',
							'OMG'=>'Omega Fancy Yellow',
							'OMG'=>'Omega',
							'OMG'=>'Omega Rhodium',
							'OMG'=>'Queen Omega Yellow',
							'ORR'=>'Orafa Rhodium1',
							'ORR'=>'Orafa Rhodium',
							'ORY'=>'Orafa Yellow',
							'ORY'=>'Metal Orafa Chains Rodhium',
							'ORY'=>'Orafa Rhodium',
							'ORY'=>'Orafa Yellow',
							'ORY'=>'Metal Orafa Chains Yellow',
							'PLB'=>'Plain Band',
							'POH'=>'Poha Mala',
							'PRO'=>'Profiler Enamel + Rhodium',
							'PRO'=>'Profiler Superior',
							'PRO'=>'Profiler Superior Rhodium',
							'PRO'=>'Profiler',
							'PRO'=>'Profiler Rhodium',
							'PRO'=>'high line collection',
							'PST'=>'Navaratna',
							'PST'=>'Precious Stone Nellur',
							'PST'=>'HYD CZ',
							'PST'=>'Precious Stone',
							'PST'=>'Precious Stone Superior',
							'RJT'=>'Rajkot',
							'RJT'=>'Rajkot Light Weight',
							'RJT'=>'Rajkot Rhodium',
							'ROP'=>'Rope Hollow',
							'ROP'=>'Rope Hollow Rhodium',
							'ROP'=>'Rope chain',
							'ROP'=>'Rope Chains',
							'RUB'=>'Ruby Cut bangles',
							'RUB'=>'Ruby Cut bangles - RHD',
							'RUD'=>'Rudraksh',
							'RWD'=>'Rawa Die',
							'RWD'=>'Rawa Die Rhodium',
							'RWS'=>'Rawa Spring bangles',
							'RWS'=>'Rawa Spring bangles - RHD',
							'SAV'=>'Savithri',
							'SCT'=>'Side Cut',
							'SCT'=>'Side Cut Rhodium',
							'SHK'=>'Pola',
							'SHK'=>'Shaka Pola',
							'SIS'=>'South Indian  Studded  Band',
							'SIS'=>'South Indian Studded',
							'SIS'=>'South Indian Style Superior',
							'SKD'=>'Sardar Kada',
							'SPL'=>'Side Plain Bangles',
							'SPL'=>'Side Plain RHD',
							'SPT'=>'Spares',
							'SUT'=>'Sutralu - Mangalsutra',
							'TMP'=>'Temple',
							'TMP'=>'Temple Gheru',
							'TMP'=>'Temple Superior',
							'TMP'=>'Temple Wax',
							'VAL'=>'Misc - Vali',
							'VIL'=>'Vilandi',
							'VIL'=>'Villandi Pachi',
							'VST'=>'V Style Bangles',
							'VST'=>'V Style Bangles - RHD',
							'XBB'=>'Black bead Chain',
							'XER'=>'Diamond Economy Rhodium',
							'XEY'=>'Diamond Economy Yellow',
							'XFR'=>'Diamond Fancy Rhodium',
							'XFY'=>'Diamond Fancy Yellow',
							'XGR'=>'Diamond Singapore Rhodium',
							'XGY'=>'Diamond Singapore Yellow',
							'XHK'=>'Diamond Micro Prong',
							'XIIY'=>'Diamond Indo Italian',
							'XXB'=>'Diamond Bangle',
							'XXC'=>'Close Setting',
							'XXD'=>'Diamond Regular',
							'XXR'=>'Diamond Rings',
							'XXW'=>'Mount',
							'XCC'=>'Diamond Changeable colour stone',
							'XCS'=>'My First Solitaire',
							'XDC'=>'Diamond And colour Stone',
							'XDN'=>'Diamond Noori Collection',
							'XDP'=>'Diamond And Pearl',
							'XHE'=>'Diamond High End',
							'XHH'=>'Diamond Exclusive High End',
							'XHK'=>'Diamond Hong Kong ( Irish )',
							'XMB'=>'Diamond',
							'XMB'=>'18K Rhodium',
							'XMY'=>'XMY',
							'XSI'=>'Diamond Illusion',
							'XWW'=>'Diamond',
							'XXS'=>'Zodiac Collection',
							'XXY'=>'Diamond Nose Pins',
							'FIG'=>'Figgaro',
							'IND'=>'Indori pattern',
							'MIR'=>'Mirror pattern',
							'COM'=>'Computer rings',
							'RWA'=>'Side rawa chain',
							'XBC'=>'Diamond Baby Collection',
							'XHP'=>'Diamond Haath Phool',
							'XXD'=>'Diamond Regular Superior'
						);
		if (!empty($code)) {
			if(array_key_exists($code, $complexity)){
        		return $complexity[$code];
	        }else{
	        	return '';
	        }
			
		}else{
			return $complexity;
		}
        
    }
}
function stone_cold($key){
        $stone_cold = array(
            'AP'=>'Plain Jewellery - Without Stone',
            'AS'=>'Plain Jewellery - Assorted Stones',
            '00'=>'VVS GH',
            '01'=>'VVS EF',
            '03'=>'VS GH',
            '05'=>'SI JK',
            '06'=>'SI GH',
            '10'=>'VVS GH +IMITATION PEARL',
            '11'=>'VVS EF+IMITATION PEARL',
            '13'=>'VS GH +IMITATION PEARL',
            '16'=>'SI GH + IMITATION PEARL',
            '99'=>'MOUNT',
            '07'=>'I1-I2',
            '08'=>'SI-I-LMN',
            '09'=>'VVS-VS-IJK'
        );
        if(!empty($key)){
        	if(array_key_exists($key, $stone_cold)){
        		return $stone_cold[$key];
	        }else{
	        	return '';
	        }
        }else{
        	return $stone_cold;
        }
        
    }
    function finding_cost($key){
        $finding_cost = array(
                '70'=>'Spiral Wire or Screw Wire',
                // '71'=>'North Indian Post & Nut',
                // '72'=>'South Indian Post & Nut',
                '71'=>'English Screw',
                '72'=>'South Screw',
                '73'=>'Push type Post & butterfly',
                '74'=>'North post & Nut Omega Clip',
                '75'=>'Wire Hook - Bails',
                '00'=>'No Finding Cost',
                'B2'=>'2 Pieces',
                'B4'=>'4 Pieces',
                'B6'=>'6 Pieces',
                'B8'=>'8 Pieces',
            );
        if(!empty($key)){
        	if(array_key_exists($key, $finding_cost)){
        		return $finding_cost[$key];
	        }else{
	        	return '';
	        }
        }else{
        	return $finding_cost;
        }
    }
    function size_master($size_type,$key){
    	$size_master  = array(
	    			'BLT' => array(
	    						'11'=>'5.0',
								'12'=>'5.5',
								'13'=>'6.0',
								'14'=>'6.5',
								'15'=>'7.0',
								'16'=>'7.5',
								'17'=>'8.0',
								'18'=>'8.5',
								'19'=>'9.0',
								'20'=>'9.5',
								'21'=>'10.0',
								'22'=>'11.0',
								'FS'=>'Adjustable'),
	    			'CHN' => array( 
	    						'02'=>'9',
								'03'=>'10',
								'04'=>'11',
								'05'=>'12',
								'06'=>'13',
								'07'=>'14',
								'08'=>'15',
								'09'=>'16',
								'10'=>'17',
								'11'=>'18',
								'12'=>'19',
								'13'=>'20',
								'14'=>'21',
								'15'=>'22',
								'16'=>'24',
								'17'=>'26',
								'18'=>'28',
								'19'=>'30',
								'20'=>'32',
								'21'=>'34',
								'22'=>'36',
								'23'=>'38',
								'24'=>'40',
								'25'=>'42',
								'26'=>'44',
								'27'=>'46',
								'28'=>'48',
								'FS'=>'FREE SIZE' ),
	    			'RNG' => array(
	    						'0L'=>'6-9',
								'L1'=>'10-13',
								'L2'=>'14-17',
								'0G'=>'16-19',
								'L3'=>'18-19',
								'G1'=>'20-23',
								'G2'=>'24-29',
								'G3'=>'30-34',
								'G4'=>'35',
								'FS'=>'Adjustable',
								'06'=>'06',
								'07'=>'07',
								'08'=>'08',
								'09'=>'09',
								'10'=>'10',
								'11'=>'11',
								'12'=>'12',
								'13'=>'13',
								'14'=>'14',
								'15'=>'15',
								'16'=>'16',
								'17'=>'17',
								'18'=>'18',
								'19'=>'19',
								'20'=>'20',
								'21'=>'21',
								'22'=>'22',
								'23'=>'23',
								'24'=>'24',
								'25'=>'25',
								'26'=>'26',
								'27'=>'27',
								'28'=>'28',
								'29'=>'29',
					            '30'=>'30',
					            '31'=>'31',
					            '32'=>'32',
					            '33'=>'33',
					            '34'=>'34',
					            '35'=>'35',
					            '36'=>'36',
					            '37'=>'37',
					            '38'=>'38',
					            '39'=>'39',
					            '40'=>'40',
								),
	    			'BAN' => array(
					    		'00'=>'0.0',
					    		'01'=>'0.0',
					            '02'=>'0.0',
					            '03'=>'0.0',
					            '04'=>'1.6',
					            '05'=>'1.7',
					            '06'=>'1.8',
					            '07'=>'1.9',
					            '08'=>'1.10',
					            '09'=>'1.11',
					            '10'=>'1.12',
					            '11'=>'1.13',
					            '12'=>'1.14',
					            '13'=>'1.15',
					            '14'=>'2.0',
					            '15'=>'2.1',
					            '16'=>'2.2',
					            '17'=>'2.3',
					            '18'=>'2.4',
					            '19'=>'2.5',
					            '20'=>'2.6',
					            '21'=>'2.7',
					            '22'=>'2.8',
					            '23'=>'2.9',
					            '24'=>'2.10',
					            '25'=>'2.11',
					            '26'=>'2.12',
					            '27'=>'2.13',
					            '28'=>'2.14',
					            '29'=>'2.15',
					            '30'=>'3.0',
					            '31'=>'3.1',
					            '32'=>'3.2',
					            '33'=>'3.3',
					            '34'=>'3.4',
					            'FS'=>'FREE SIZE'
					        ),
	    			
						
						);
        if(!empty($key)){

        	if(array_key_exists($key, $size_master[$size_type])){
        		return $size_master[$size_type][$key];
	        }else{
	        	return '0.0';
	        }
        }else{
        	return $size_master[$size_type];
        }
    }
    function vendor_code($key){
        $vendor_code = array(
                'EX' => 'EX'
            );
        if(!empty($key)){
        	if(array_key_exists($key, $vendor_code)){
        		return $vendor_code[$key];
	        }else{
	        	return '';
	        }
        }else{
        	return $vendor_code;
        }
    }
    function weight_band($product,$p_key){
        $weight_band = array(
            'BAN'=>array(
                'A'=>'0-20',
                'B'=>'20-40',
                'C'=>'40-60',
                'D'=>'60-80',
                'E'=>'80-100',
                'F'=>'100-120'
                ),
            'BLT'=>array(
                'A'=>'0-10',
                'B'=>'10-20',
                'C'=>'20-30',
                'D'=>'30-40',
                'E'=>'40-50',
                'F'=>'50-60',
                'G'=>'60-70',
                'H'=>'70-80',
                'I'=>'80-90'
                ),
            'CHN'=>array(
                'A'=>'0-5',
                'B'=>'5-10',
                'C'=>'10-15',
                'D'=>'15-20',
                'E'=>'20-25',
                'F'=>'25-30',
                'G'=>'30-40',
                'H'=>'40-50',
                'I'=>'50-60',
                'J'=>'60-70',
                'K'=>'70-80',
                'L'=>'80-90',
                'M'=>'90-100',
                'N'=>'100'
                ),
            'ECN'=>array(
                'A'=>'0-5',
                'B'=>'5-10',
                'C'=>'10-15',
                'D'=>'15-20',
                'E'=>'20-25'
                ),
            'ERG'=>array(
                'A'=>'0-5',
                'B'=>'5-10',
                'C'=>'10-15',
                'D'=>'15-20',
                'E'=>'20-25',
                'F'=>'25-30',
                'G'=>'30-35',
                'H'=>'35-40'
                ),
            'KAN'=>array(
                'A'=>'0-20',
                'B'=>'20-40',
                'C'=>'40-60',
                'D'=>'60-80',
                'E'=>'80-100',
                'F'=>'100-120',
                'G'=>'120-140'
                ),
            'MIS'=>array(
                'A'=>'0-5',
                'B'=>'5-10',
                'C'=>'10-15',
                'D'=>'15-20',
                'E'=>'20-30',
                'F'=>'30-40',
                'G'=>'40-50',
                'H'=>'50-60'
                ),
            'MSP'=>array(
                'A'=>'0-5',
                'B'=>'5-10',
                'C'=>'10-15',
                'C'=>'10-15',
                'D'=>'15-20',
                'E'=>'20-25'

                ),
            'MSR'=>array(
                'A'=>'0-10',
                'B'=>'10-20',
                'C'=>'20-30',
                'D'=>'30-40',
                'E'=>'40-50',
                'F'=>'50-60',
                'G'=>'60-70',
                'H'=>'70-80'
                ),
            'NKL'=>array(
                'A'=>'5-10',
                'B'=>'10-15',
                'C'=>'15-25',
                'D'=>'25-50',
                'E'=>'50-75',
                'F'=>'75-100'
                ),
            'PDT'=>array(
                'A'=>'0-5',
                'B'=>'5-10',
                'C'=>'10-15',
                'D'=>'15-20',
                'E'=>'20-25',
                'F'=>'25-30'
                ),
            'PST'=>array(
                'A'=>'0-10',
                'B'=>'10-20',
                'C'=>'20-30',
                'D'=>'30-40',
                'E'=>'40-50'
                ),
            'RNG'=>array(
                'A'=>'0-5',
                'B'=>'5-10',
                'C'=>'10-15',
                'D'=>'15-20'
                ),
            'SCD'=>array(
                'A'=>'0-5',
                'B'=>'5-10',
                'C'=>'10-15',
                'D'=>'15'

                ),
            'SET'=>array(
                'A'=>'0-15',
                'B'=>'15-25',
                'C'=>'25-50',
                'D'=>'50-75',
                'E'=>'75-100',
                'F'=>'100-125',
                'G'=>'125-150',
                'H'=>'150-175',
                'I'=>'175-200',
                'J'=>'200-225',
                'K'=>'225-250'
                ),
            'WBT'=>array(
                'C'=>'50-75',
                'D'=>'75-100',
                'E'=>'100-125',
                'F'=>'125-150',
                'G'=>'150-175',
                'H'=>'175-200',
                'I'=>'200-225',
                'J'=>'225-250',
                'K'=>'250-275',
                'L'=>'275-300'

                ),
            'NOR' => array(
	    		'A'=>'0-5',
	    		'B'=>'5-10',
	            'C'=>'10-15',
	            'D'=>'15-20',
	            'E'=>'20-30',
	            'F'=>'30-40',
	            'G'=>'40-50',
	            'H'=>'50-60',
	            ),
            );
		if(!empty($product) && !empty($p_key)){
			// print_r($weight_band[$product]);
        	if(array_key_exists($product, $weight_band) && array_key_exists($p_key,$weight_band[$product])){
        		return $weight_band[$product][$p_key];
	        }else{
	        	return '';
	        }
        }else{
        	return $weight_band;
        }
    }

     function itemNum_subStr($str_data)
		{
		  $result_data=substr($str_data,1,13);

		  return $result_data;
		}
