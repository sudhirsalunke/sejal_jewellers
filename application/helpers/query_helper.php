<?php

defined('BASEPATH') OR exit('No direct script access allowed.');

if (!function_exists('get_checkExist')) {

    function get_checkExist($table, $field, $field_value, $exist_field, $id) {
        $ci = &get_instance();
        $user_id = $ci->session->userdata("unique_identifier");
        $error_msg = array(
            "status" => "error",
            "data" => "Oops! Error. " . $exist_field . " already exist!!!"
        );
        $sql = "select count(*) as count from " . $table . " where user_id='" . $user_id . "' and " . $field . "='" . $field_value . "'";
        if ($id != "")
            $sql.=' and ' . $table . '_id !=' . $id;
        $response = $ci->db->query($sql)->row_array();
        if ($response['count'] > 0)
            return $error_msg;
        else
            return 0;
    }

}

if (!function_exists('addEdit')) {

    function addEdit($table, $data, $id) {
        $field = $table . "_id";
        $ci = &get_instance();
        if ($id == "") {
            if ($ci->db->insert($table, $data))
                return TRUE;
        }else {
            if ($ci->db->update($table, $data, array($field => $id)))
                return TRUE;
        }
        return FALSE;
    }

}

if (!function_exists('get_allRecord')) {

    function get_allRecord($table, $options, $role_id) {
        $ci = &get_instance();
        $orderBy = $table . "_id";

        $sql = "select * from " . $table . " where role_master_id='" . $role_id . "'";
        if (!empty($options))
            $sql.=" ORDER BY " . $orderBy . " DESC LIMIT " . $options['length'] . " offset " . $options['start'];
        $response = $ci->db->query($sql)->result_array();
        if ($response)
            return $response;
        else
            return 0;
    }

}

if (!function_exists('get_records')) {

    function get_records($table, $options) {
        $ci = &get_instance();
        $orderBy = $table . "_id";

        $sql = "select * from " . $table;
        if (!empty($options))
            $sql.=" ORDER BY " . $orderBy . " DESC LIMIT " . $options['length'] . " offset " . $options['start'];
        $response = $ci->db->query($sql)->result_array();
        if ($response)
            return $response;
        else
            return 0;
    }

}

if (!function_exists('get_count')) {

    function get_count($table, $role_id) {
        $ci = &get_instance();
        $sql = "select count(*) as count from " . $table . " where role_master_id='" . $role_id . "'";
        $response = $ci->db->query($sql)->row_array();
        return $response['count'];
    }

}
if (!function_exists('get_single_count')) {

    function get_single_count($table) {
        $ci = &get_instance();
        $sql = "select count(*) as count from " . $table;
        $response = $ci->db->query($sql)->row_array();
        return $response['count'];
    }

}
if (!function_exists('get_customer_name_by_id')) {

    function get_customer_name_by_id($customer_id) {
        $ci = &get_instance();
        $sql = "select name from customer where id='".$customer_id."'";
        $response = $ci->db->query($sql)->row_array();
        return $response['name'];
    }

}

if (!function_exists('get_order_image')) {

    function get_order_image($id) {
        $ci = &get_instance();
        $sql = "select id,order_image,orders_video from customer_orders where id='".$id."'";
        $response = $ci->db->query($sql)->row_array();
        return $response;
    }

}

if (!function_exists('get_karigar_name_by_id')) {

    function get_karigar_name_by_id($karigar_id) {
        $ci = &get_instance();
        $sql = "select name from karigar_master where id='".$karigar_id."'";
        $response = $ci->db->query($sql)->row_array();
        return $response['name'];
    }

}

if (!function_exists('get_recordByCondition')) {

    function get_recordByCondition($table, $id, $field = "", $fieldValue = "", $role_id) {
        $ci = &get_instance();

        $sql = "select * from " . $table . " where role_id='" . $role_id . "'";
        if ($id != "") {
            $field = $table . "_id";
            $sql.=" and " . $field . "=" . $id;
        } else
            $sql.=" and " . $field . "='" . $fieldValue . "'";
        $response = $ci->db->query($sql)->row_array();
        if (!empty($response))
            return $response;
        else
            return 0;
    }

}

if (!function_exists('delete_Record')) {

    function delete_Record($table, $id) {
        $ci = &get_instance();
        $field = $table . "_id";
        $user_id = $ci->session->userdata('unique_identifier');
        $sql = "delete from " . $table . " where " . $field . "=" . $id ;
        $response = $ci->db->query($sql);
        if (!empty($response))
            return $response;
        else
            return 0;
    }

}

if (!function_exists('send_notification')) {

    function send_notification($data) {
        $table = 'notification_tracking';
        $ci = &get_instance();
        if ($ci->db->insert($table, $data)) {
            return $ci->db->insert_id();
        }
    }

}

if (!function_exists('receive_notification')) {

    function receive_notification($id, $data, $user_data='') {
        $table = 'notification_tracking';
        $ci = &get_instance();
        if(!empty($user_data)){
            $data['resolution'] = $user_data['resolution'];
            $data['device'] = $user_data['device'];
            $data['browser'] = $user_data['browser'];
            $data['ip'] = $user_data['ip'];
            $data['os'] = $user_data['os'];
            $data['country'] = $user_data['country'];
        }
        if ($ci->db->update($table, $data, array('id' => $id)))
            return TRUE;
    }

}

if(!function_exists('get_statastic_count')){
    function get_statastic_count($table,$count_term, $engagement_id){
        $ci = &get_instance();
        if($count_term == '*')
            $sql = "select count(*) as count from " . $table . " where engagement_id='" . $engagement_id ."'";
        else
            $sql = "select count(".$count_term.") as ".$count_term." from " . $table . " where engagement_id='" . $engagement_id . "' and ".$count_term." = true";

        $response = $ci->db->query($sql)->row_array();
        if (!empty($response))
            return $response;
        else
            return 0;
    }
}

if(!function_exists('get_statastics')){
    function get_statastics($table, $params, $engagement_id){
        $ci = &get_instance();
        $sql = "select * from " . $table . " where engagement_id='" . $engagement_id . "'";
        if (!empty($params))
            $sql.=" ORDER BY id DESC LIMIT " . $params['length'] . " offset " . $params['start'];
        $response = $ci->db->query($sql)->result_array();
        if (!empty($response))
            return $response;
        else
            return 0;
    }
}

if (!function_exists('delete_Record_activity')) {

    function delete_activity($table, $id) {
        $ci = &get_instance();
        $field = $table . "_id";
        $user_id = $ci->session->userdata('unique_identifier');
        $sql = "delete from " . $table . " where " . $field . "=" . $id ;
        $response = $ci->db->query($sql);
        if (!empty($response))
            return $response;
        else
            return 0;
    }

}

if (!function_exists('deduct_msqr_quantity')) {

    function deduct_msqr_quantity($msqr_id, $qty) {
        $ci = &get_instance();
        $ci->db->where('id',$msqr_id);
        $result =  $ci->db->get('make_set_quantity_relation')->row_array();
       
        if (!empty($result)){
             $new_qty =$result['qty_remaining']-$qty;
             $ci->db->set('qty_remaining',$new_qty);
             $ci->db->where('id',$msqr_id);
             $ci->db->update('make_set_quantity_relation');

             return true;
        }else{
            return false;
        }
    }

}


function get_user_by_email($email){
        $ci = &get_instance();
        $ci->db->select('*');
        $ci->db->from('customer');
        $ci->db->where('email',$email);
        $result= $ci->db->get()->row_array(); 
      return $result;
}

function get_opening_stock($dept_id,$today_date){
        $today = $today_date;
        $status_id= array(1,2);
        $ci = &get_instance();
        $ci->db->select('sum(cor.net_weight)as opening_stock');
        $ci->db->from('customer_orders co');
        $ci->db->join('customer_order_received cor','co.id = cor.order_id');
        $ci->db->where('department_id',$dept_id);
        $ci->db->where_in('cor.status',$status_id);
        $ci->db->where('DATE(cor.created_at) <',$today);
        $ci->db->where('DATE(receive_date)<',$today);
        $result= $ci->db->get()->row_array(); 
    //  echo $ci->db->last_query();exit;
        if($result['opening_stock'] == ""){
            $result['opening_stock'] = 0;
        }
      return $result['opening_stock'];
}

function get_today_send_opening_stock($dept_id,$today_date){
        $today = $today_date;
        $ci = &get_instance();
        $ci->db->select('sum(cor.net_weight)as prev_opening_stock');
        $ci->db->from('customer_orders co');
        $ci->db->join('customer_order_received cor','co.id = cor.order_id');
        $ci->db->where('department_id',$dept_id);
        $ci->db->where('cor.status','3');
        $ci->db->where('DATE(cor.created_at)',$today);
        $ci->db->where('DATE(receive_date)<',$today);
        $result= $ci->db->get()->row_array(); 
    //  echo $ci->db->last_query();exit;
        if($result['prev_opening_stock'] == ""){
            $result['prev_opening_stock'] = 0;
        }
      return $result['prev_opening_stock'];
}

function get_received_stock($dept_id,$today_date){
         $today = $today_date;
        $status_id= array(1,2);
        $ci = &get_instance();
        $ci->db->select('sum(cor.net_weight)as received_stock');
        $ci->db->from('customer_orders co');
        $ci->db->join('customer_order_received cor','co.id = cor.order_id');
        $ci->db->where('department_id',$dept_id);
        $ci->db->where_in('cor.status',$status_id);
        $ci->db->where('DATE(cor.created_at)',$today);
          
        $result= $ci->db->get()->row_array(); 
      
        if($result['received_stock'] == ""){
           $result['received_stock'] = 0;
        }
      return $result['received_stock'];
}

function get_today_send_received_stock($dept_id,$today_date){
     $today = $today_date;
        $ci = &get_instance();
        $ci->db->select('sum(cor.net_weight)as prev_received_stock');
        $ci->db->from('customer_orders co');
        $ci->db->join('customer_order_received cor','co.id = cor.order_id');
        $ci->db->where('department_id',$dept_id);
        $ci->db->where('cor.status','3');
        $ci->db->where('DATE(co.receive_date)',$today);
        $ci->db->where('DATE(cor.created_at)',$today);
          
        $result= $ci->db->get()->row_array(); 
      //  echo $ci->db->last_query();print_r($result);exit;
        if($result['prev_received_stock'] == ""){
            $result['prev_received_stock'] = 0;
        }
      return $result['prev_received_stock'];
}


function get_order_received($dept_id,$today_date){
       $today = $today_date;
      //status = array(4);
        $ci = &get_instance();
        $ci->db->select('sum(cor.net_weight)as received_order');
        $ci->db->from('customer_orders co');
        $ci->db->join('customer_order_received cor','co.id = cor.order_id');
        $ci->db->where('department_id',$dept_id);
        $ci->db->where('DATE(cor.created_at)',$today);
        $result= $ci->db->get()->row_array(); 
        if($result['received_order'] == ""){
            $result['received_order'] = 0;
        }
      return $result['received_order'];
}

// function get_issued_stock($dept_id){
//        $today = date('Y-m-d');
//         $ci = &get_instance();
//         $ci->db->select('sum(weight_range_id)as issued_stock');
//         $ci->db->from('customer_orders');
//         $ci->db->where('department_id',$dept_id);
//         $ci->db->where('status','4');
//         $ci->db->where('DATE(actual_delivery_date)',$today);
//         $result= $ci->db->get()->row_array(); 
//         if($result['issued_stock'] == ""){
//             $result['issued_stock'] = 0;
//         }
//       return $result['issued_stock'];
// }

function get_issued_stock($dept_id,$today_date){
       $today =$today_date;
      //status = array(4);
        $ci = &get_instance();
        $ci->db->select('sum(cor.net_weight)as issued_stock');
        $ci->db->from('customer_orders co');
        $ci->db->join('customer_order_received cor','co.id = cor.order_id');
        $ci->db->where('department_id',$dept_id);
        $ci->db->where('cor.status','3');
        $ci->db->where('DATE(cor.created_at)',$today);
        $result= $ci->db->get()->row_array(); 
        if($result['issued_stock'] == ""){
           $result['issued_stock'] = 0;
        }
      return $result['issued_stock'];
}


function get_order_issued($dept_id,$today_date){
     $today = $today_date;
      //status = array(4);
        $ci = &get_instance();
        $ci->db->select('sum(cor.net_weight)as issued_order');
        $ci->db->from('customer_orders co');
        $ci->db->join('customer_order_received cor','co.id = cor.order_id');
        $ci->db->where('department_id',$dept_id);
        $ci->db->where('cor.status','3');
        $ci->db->where('DATE(cor.created_at)',$today);
        $result= $ci->db->get()->row_array(); 

        if($result['issued_order'] == ""){
            $result['issued_order'] = 0;
        }
      return $result['issued_order'];
}



function get_delivered_quantity_with_range($weight_from,$weight_to,$type,$from_date,$to_date,$id){
      //status = array(4);
        $ci = &get_instance();
        $ci->db->select('sum(cor.received_qty)as send_qty');
        $ci->db->from('customer_orders co');
        $ci->db->join('customer_order_received cor','co.id = cor.order_id');
         if($type == "department"){
            $ci->db->where('department_id',$id);
         }else{
             $ci->db->where('category_id',$id);
         }   
        $ci->db->where('cor.status','3');
        $ci->db->where('DATE(cor.send_at) >=', $from_date);
        $ci->db->where('DATE(cor.send_at) <=', $to_date);
        $ci->db->where('cor.net_weight >=', $weight_from);
        if($weight_to != ""){
         $ci->db->where('cor.net_weight <=', $weight_to);  
        }
        $result= $ci->db->get()->row_array(); 
     // echo $ci->db->last_query();print_r($result);
        if($result['send_qty'] == ""){
            $result['send_qty'] = 0;
        }
      return $result['send_qty'];
}



