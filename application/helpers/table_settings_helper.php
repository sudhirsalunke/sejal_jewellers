<?php
	function getTableSettings($controllername,$type='',$type_id=''){
		$ci = &get_instance();
		$karigar_id = $ci->uri->segment(3);
		//print_r($segment);die;
		$tableParameters = array(
						'Amend_products' =>array  (
										'default_column'=> 'k.id',
										'table' => array('Amend_products as ap','quality_control as qc','corporate_products as cp','Receive_products as rp','email_orders as eo','corporate as corp','karigar_master k'),
										'join_columns' => array('qc.id = ap.qc_id','cp.id = ap.corporate_product_id',
														'rp.id = qc.receive_product_id','eo.order_id = cp.order_id',
														'corp.id = eo.corporate','k.id = rp.karigar_id'
											),
										'join_type' => 'join',
										'limit' => "50",
										'actionFunction' => 'AmendProductsAction',
										'headingFunction' =>'AmendHeading',
										'search_url' => 'Amend_products',
										'where' =>"ap.status='1'",
										'where_ids'=>'',
										'group_by'=>'k.id',
										'export'=>'',
										'extra_select_column'=>'k.id,ap.qc_id',
									),
						'Amend_products/view' =>array  (
										'default_column'=> 'ap.id',
										'table' => array('Amend_products as ap','quality_control as qc','corporate_products as cp','Receive_products as rp','email_orders as eo','corporate as corp','karigar_master k'),
										'join_columns' => array('qc.id = ap.qc_id','cp.id = ap.corporate_product_id',
														'rp.id = qc.receive_product_id','eo.order_id = cp.order_id',
														'corp.id = eo.corporate','k.id = rp.karigar_id'
											),

										'join_type' => 'join',

										'limit' => "50",
										'actionFunction' => 'VwAmendProductsAction',
										'headingFunction' =>'VwAmendHeading',
										'search_url' => 'Amend_products/view/'.$karigar_id,
										'where' =>"ap.status='1' and k.id='$karigar_id'",
										'where_ids'=>'',
										'export'=>'',
										'extra_select_column'=>'ap.id,ap.qc_id',
									)
				);		
		return $tableParameters[$controllername];
	}
?>
