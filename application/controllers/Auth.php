<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Auth extends CI_Controller {

    public function __construct() {
        parent::__construct();
        $this->load->model(array('auth_model'));
        $this->load->helper(array('common_helper','core_helper'));
    }

    public function index()
    {
        if ($this->session->userdata('logged_in') == TRUE &&  $this->session->userdata('site_name') == 'sejal_jewellers') {
            redirect(ADMIN_PATH . 'Dashboard');
        } else {
            if ($this->input->post('submit') != '') {
                    $login_validation_result = $this->auth_model->validateAdminLogin();
                    if ($login_validation_result == FALSE) {
                        $this->load->view('auth/login');
                    }
                    else{
                        $getuserdetails = userdataFromUsername($this->input->post('username'));
                        $get_default_cntrl = get_default_controller($getuserdetails['id']);
                        $get_dashbord_cntrl = get_dashbord_cntrl($getuserdetails['id']);
                        $get_department_map = get_department_map($getuserdetails['id']);
                        $selwt_ids = '';
                        //$department_map='';
                        if(!empty($get_department_map)){
                          foreach ($get_department_map as $key => $value) {       
                            $selwt_ids = $value;
                            
                          }
                        $department_map = $selwt_ids;
                        }
                      //echo  implode(",", $department_map);
                   //echo"<pre>"; print_r($department_map); echo"</pre>";die;
                        $session_data = array(
                            'user_id' => $getuserdetails['id'],
                            'first_name' => $getuserdetails['first_name'],
                            'last_name' => $getuserdetails['last_name'],
                            'email' => $getuserdetails['email'],
                            'name' => $getuserdetails['first_name'].' '.$getuserdetails['last_name'],
                            'department_id' => $getuserdetails['department_id'],
                            'location_id' => $getuserdetails['location_id'],
                            'role' => $getuserdetails['1'],
                            'user_department' => $department_map,
                            'site_name' => 'sejal_jewellers',
                            'logged_in' => TRUE
                        );
                        $this->session->set_userdata($session_data);
                        if ($this->session->userdata('logged_in') == TRUE) {                                 
                            if(!empty($get_dashbord_cntrl)){
                                redirect(ADMIN_PATH . $get_dashbord_cntrl);                                
                            }else{
                                redirect(ADMIN_PATH . $get_default_cntrl);
                            }
                    }
                }
            }else{
                $data['page_title'] = 'Sign In';
                $this->load->view('auth/login',$data);
            }
            
        }
    }   
    public function logout() {
        session_destroy();
        $data['page_title'] = 'Sign In';
        $this->load->view('auth/login',$data);
    }

    public function upload_pic()
    {
       $response=array();
       $files=$_FILES;
       if(empty($files))
       {
            $response['status']="error";
            $response['data']="Please choose a profile pic";
       }else{
            $file_parts = pathinfo($files['file']['name']);
            if($file_parts['extension']=='jpg' || $file_parts['extension']=='jpeg' || $file_parts['extension']=='png'){
                $response=$this->auth_model->upload_pic($files['file']);
            }else{
                $response['status']="error";
                $response['data']="Invalid file format";
            }
       }
       echo json_encode($response);
    }
}