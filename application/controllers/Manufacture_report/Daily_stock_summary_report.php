<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class Daily_stock_summary_report extends CI_Controller {

  public function __construct() {
    parent::__construct();
     if(empty($this->session->userdata('user_id'))){
        redirect(ADMIN_PATH . 'auth/logout');
     	}
      $this->breadcrumbs->push("Manufacture Report", "Manufacture_report");
      $this->load->model(array('report/manufacture_module/daily_stock_summary_report_model','Manufacturing_module/Department_model'));
    	$this->load->config('admin_validationrules', TRUE);
    	$this->load->library(array('Data_encryption','excel_lib'));
    }

    public function index(){
      $dep=@$_GET['department_id'];
        if(!empty($dep)){
        $_SESSION['department_id']=$dep;
        }
        $from_date=date('Y-m-d',strtotime(@$_GET['from']));
      
      
      $to_date=date('Y-m-d',strtotime(@$_GET['to']));   
      $department_id=$this->session->userdata('department_id');
      $department_name=$this->Department_model->find($department_id);
      $data['page_title']="Daily stock report";
      $data['display_title'] = $department_name['name']."- Daily stock report";
      $this->breadcrumbs->push("Daily stock report", "Manufacture_report/Daily_stock_summary_report");
       //print_r($from_date);die;
      $list=$this->input->post('list');
      //print_r($list);


    if($list !="")
    {
      echo json_encode($this->generate_data_table($from_date,$to_date,$department_id));
    }else{
       $this->view->render('report/manufacture/daily_stock_report',$data);
    }

  }


  private function generate_data_table($from_date,$to_date,$department_id){

      $today=$from_date;
      $previous_opening_stock = $this->daily_stock_summary_report_model->get_previous_stock($today,$department_id,'2');
// echo $this->db->last_query();die;
      //$previous_receipt_from_karigar = $this->daily_stock_summary_report_model->get_previous_stock($today,$department_id,'1');

      $previous_return_to_karigar = $this->daily_stock_summary_report_model->get_previous_stock_k_return($today,$department_id,'0');

      $previous_sending_qc_data = $this->daily_stock_summary_report_model->get_previous_qc_data($from_date,$to_date,$department_id,'7');

      $previous_qc_data = $this->daily_stock_summary_report_model->get_previous_qc_data($today,$department_id,'4');

      $previous_repair = $this->daily_stock_summary_report_model->get_previous_repair_data($today,$department_id); 

      $previous_hm = $this->daily_stock_summary_report_model->get_previous_hm_data($today,$department_id,'7');

      //$previous_pending_tag = $this->daily_stock_summary_report_model->get_previous_pending_tagging_data($today,$department_id); 

      $previous_tagging = $this->daily_stock_summary_report_model->get_previous_tagging_data($today,$department_id); 

      $previous_k_detailing_data = $this->daily_stock_summary_report_model->get_kundan_qc_previous_detailing_data($today,$department_id);   


      $previous_kundan = $this->daily_stock_summary_report_model->get_previous_kundan_data($today,$department_id,'1');

      $previous_send_kundan_qc_data=$this->daily_stock_summary_report_model->get_previous_send_kundan_qc_data($from_date,$to_date,$department_id);  

      $previous_k_qc_data = $this->daily_stock_summary_report_model->get_previous_kundan_qc_data($today,$department_id); 

      $previous_ready_product_data = $this->daily_stock_summary_report_model->get_previous_ready_product_data($today,$department_id,'0');

      $previous_issue_to_Sale = $this->daily_stock_summary_report_model->get_previous_issue_to_sale_data($today,$department_id,'1');

      $previous_reject_to_sale = $this->daily_stock_summary_report_model->get_previous_rejected_data($today,$department_id,'3');

    $previous_total_opening_stock_pcs = ($previous_opening_stock['total_pcs'] +  $previous_sending_qc_data['total_pcs']  + $previous_k_detailing_data['total_pcs']+ $previous_tagging['total_pcs'] +  $previous_k_qc_data['total_pcs'] +$previous_ready_product_data['total_pcs']);



     $previous_total_opening_stock_weight = ($previous_opening_stock['order_weight'] + $previous_sending_qc_data['order_weight'] + $previous_k_detailing_data['order_weight'] +$previous_tagging['order_weight'] + $previous_k_qc_data['order_weight']  + $previous_ready_product_data['order_weight']);



      
      $opening_stock = $this->daily_stock_summary_report_model->get($from_date,$to_date,$department_id);
      // $receipt_from_karigar = $this->daily_stock_summary_report_model->get($from_date,$to_date,$department_id,'1');

      $return_to_karigar = $this->daily_stock_summary_report_model->get($from_date,$to_date,$department_id,'0');
//echo $this->db->last_query();die;

       $sending_qc_data = $this->daily_stock_summary_report_model->get_qc_data($from_date,$to_date,$department_id,'7');

         //echo $this->db->last_query();die;
      $qc_data = $this->daily_stock_summary_report_model->get_qc_data($from_date,$to_date,$department_id,'4');

      $repair = $this->daily_stock_summary_report_model->get_repair_data($from_date,$to_date,$department_id,"0"); 

      $hm = $this->daily_stock_summary_report_model->get_hm_data($from_date,$to_date,$department_id,'7');
     
/*      $pending_tag = $this->daily_stock_summary_report_model->get_pending_tagging_data($from_date,$to_date,$department_id); */
    //print_r($pending_tag['total_pcs']);die;
     $tagging = $this->daily_stock_summary_report_model->get_tagging_data($from_date,$to_date,$department_id,'1');  

      $kundan = $this->daily_stock_summary_report_model->get_kundan_data($from_date,$to_date,$department_id,'1');

      $k_qc_data = $this->daily_stock_summary_report_model->get_kundan_qc_data($from_date,$to_date,$department_id,'2');   
      $k_detailing_data = $this->daily_stock_summary_report_model->get_kundan_qc_detailing_data($from_date,$to_date,$department_id);    
      // echo $this->db->last_query();die;

      $send_kundan_qc_data=$this->daily_stock_summary_report_model->get_send_kundan_qc_data($from_date,$to_date,$department_id);  
      $ready_product_data = $this->daily_stock_summary_report_model->get_ready_product_data($from_date,$to_date,$department_id,'0');
      $issue_to_Sale = $this->daily_stock_summary_report_model->get_issue_to_sale_data($from_date,$to_date,$department_id,'1');
       //echo $this->db->last_query();die;
      $reject_to_sale = $this->daily_stock_summary_report_model->get_rejected_data($from_date,$to_date,$department_id,'3');
     
      $total_opening_stock_pcs = ($previous_total_opening_stock_pcs +  $sending_qc_data['total_pcs'] + $k_detailing_data['total_pcs']+$tagging['total_pcs']  +$ready_product_data['total_pcs'] );

      $total_outward_stock_pcs = ($return_to_karigar['total_pcs'] + $kundan['total_pcs'] + $issue_to_Sale['total_pcs'] + $hm['total_pcs']); 

 //print_r($qc_data['total_pcs']);die;

      $total_opening_stock_weight = ($previous_total_opening_stock_weight  + $sending_qc_data['order_weight']  +$k_detailing_data['order_weight'] + $tagging['order_weight'] +$ready_product_data['order_weight'] );

      $total_outward_stock_weight = ($return_to_karigar['order_weight'] + $kundan['order_weight'] + $issue_to_Sale['order_weight'] + $hm['order_weight']);

     $closing_stock_pcs = ($total_opening_stock_pcs - $total_outward_stock_pcs);
     $closing_stock_weight = ($total_opening_stock_weight - $total_outward_stock_weight);
        $start=@$_REQUEST['start'];
        if (!empty($opening_stock)) {

                $data[0][0] ='Opening';
                $data[0][1] ='<span style="float:right">'.blank_value(round($previous_total_opening_stock_weight,3)).'</span>';
                $data[0][2] ='<span style="float:right">'.blank_value(round($previous_total_opening_stock_pcs,2)).'</span>';
                $data[0][3] ='<span style="float:right"></span>';
                $data[0][4] ='<span style="float:right"></span>';

 
                $data[1][0] ='Received From Karigar';
                $data[1][1] ='<span style="float:right">'.blank_value(round($sending_qc_data['order_weight'],2)).'</span>';
                $data[1][2] ='<span style="float:right">'.blank_value(round($sending_qc_data['total_pcs'],2)).'</span>';
                $data[1][3] ='<span style="float:right">-</span>';
                $data[1][4] ='<span style="float:right">-</span>';
               

                
                $data[2][0] ='Tagging';
                $data[2][1] ='<span style="float:right">'.blank_value(round($tagging['order_weight'] ,2)).'</span>';
                $data[2][2] ='<span style="float:right">'.blank_value(round($tagging['total_pcs'] ,2)).'</span>';
                $data[2][3] ='<span style="float:right">-</span>';
                $data[2][4] ='<span style="float:right">-</span>';

                

                $data[3][0] ='Send to  Kundan';
                $data[3][1] ='<span style="float:right">-</span>';
                $data[3][2] ='<span style="float:right">-</span>';
                $data[3][3] ='<span style="float:right">'.blank_value(round($kundan['order_weight'],2)).'</span>';
                $data[3][4] ='<span style="float:right">'.blank_value(round($kundan['total_pcs'],2)).'</span>';

                
                $data[4][0] ='Receipt from Kundan karigar';
                $data[4][1] ='<span style="float:right">'.blank_value(round($k_detailing_data['order_weight'],2)).'</span>';
                $data[4][2] ='<span style="float:right">'.blank_value(round($k_detailing_data['total_pcs'],2)).'</span>';
                $data[4][3] ='<span style="float:right">-</span>';
                $data[4][4] ='<span style="float:right">-</span>';

               
                $data[5][0] ='Ready Product';
                $data[5][1] ='<span style="float:right">'.blank_value(round($ready_product_data['order_weight'],2)).'</span>';
                $data[5][2] ='<span style="float:right">'.blank_value(round($ready_product_data['total_pcs'],2)).'</span>';
                $data[5][3] ='<span style="float:right">-</span>';
                $data[5][4] ='<span style="float:right">-</span>';

                $data[6][0] ='Repair';
                $data[6][1] ='<span style="float:right">-</span>';
                $data[6][2] ='<span style="float:right">-</span>';
                $data[6][3] ='<span style="float:right">'.blank_value(round($repair['order_weight'],2)).'</span>';
                $data[6][4] ='<span style="float:right">'.blank_value(round($repair['total_pcs'],2)).'</span>';



                $data[7][0] ='Issue to Sale';
                $data[7][1] ='<span style="float:right">-</span>';
                $data[7][2] ='<span style="float:right">-</span>';
                $data[7][3] ='<span style="float:right">'.blank_value(round($issue_to_Sale['order_weight'],2)).'</span>';
                $data[7][4] ='<span style="float:right">'.blank_value(round($issue_to_Sale['total_pcs'],2)).'</span>';


              

                $data[8][0] ='closing Stock';
                $data[8][1] ='<span style="float:right">'.blank_value(round($closing_stock_weight,3)).'</span>';
                $data[8][2] ='<span style="float:right">'.blank_value(round($closing_stock_pcs,2)).'</span>';
                $data[8][3] ='<span style="float:right"></span>';
                $data[8][4] ='<span style="float:right"></span>';
   
             
        }else{
          $msg='No data found';
        for ($i=0; $i <=8 ; $i++) { 
            $data[0][$i] = [$msg];
            $msg="";
        }
           
        }
         $json_data = array(
              "draw" => intval($_REQUEST['draw']),
              "recordsTotal" => intval(9),
              "recordsFiltered" => intval(9),
              "data" => $data
          );
        return $json_data; 
      }
    

     



}//class