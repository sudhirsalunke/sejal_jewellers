<?php 
defined('BASEPATH') OR exit('No direct script access allowed');

class Search_popup extends CI_Controller {
  public function __construct() {
    parent::__construct();
  }
  
  function index(){
    $this->load->helper(array('table_config'));
    $data['param'] = $this->input->post();
    if(isset($data['param']['ordered_columns']) && $data['param']['ordered_columns'] ==1)
    {
      foreach ($this->session->userdata('arranged_columns') as $sesskey => $sessvalue) {
        foreach ($data['param']['function_name']() as $key => $value) {
          if($sessvalue == $value[0]){
            $newheadings[]= $value;
          }
        }
      }
      $data['heading'] = $newheadings;
    }
    else if(isset($data['param']['select_column']) && $data['param']['select_column'] ==1){
          foreach($this->session->userdata('filtered_columns') as $seesskey =>$sessvalue){
            foreach ($data['param']['function_name']() as $key => $value) {
            if($key == $sessvalue){
              $newheadings[] =  $value;
            }
          }
      }
      $data['heading'] = $newheadings;
    }
    else{
      if($data['param']['search_url']=='shared_wr')
         $data['heading'] = $data['param']['function_name']('work');
      else
        $data['heading'] = $data['param']['function_name']();
    }
    $data['status_type'] = StatusType();
	  $data['profile_status'] = ProfileStatus();
	  //$data['enagement'] = get_engagement_type();
    echo $this->load->view('master/search_popup/index',@$data,true);
  }
}//class