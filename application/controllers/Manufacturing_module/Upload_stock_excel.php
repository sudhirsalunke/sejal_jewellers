<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class Upload_stock_excel extends CI_Controller {

  public function __construct() {
    parent::__construct();
    if(empty($this->session->userdata('user_id'))){
      redirect(ADMIN_PATH . 'auth/logout');
    }
    $this->breadcrumbs->push("Order Module", "Order_dashboard");
    $this->breadcrumbs->push("Order List", "Manufacturing_module");
    $this->breadcrumbs->push("Upload Order Excel", "/Upload_stock_excel");
    $this->load->config('admin_validationrules', TRUE);
    $this->load->library(array('Data_encryption','excel_lib'));
  }

  public function index(){  
    $data['page_title'] = "Upload Stock Excel";
       $this->view->render('Manufacturing_module/Upload_stock_excel/index',$data);
/*    $list=$this->input->post('list');
    if($list !="")
    {
      echo json_encode($this->generate_data_table());
    }else{
    }*/
  }
 


}
?>