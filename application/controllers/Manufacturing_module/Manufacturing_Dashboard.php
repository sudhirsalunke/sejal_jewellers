<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Manufacturing_Dashboard extends CI_Controller {

    public function __construct() {
        parent::__construct();
        if(empty($this->session->userdata('user_id'))){
        redirect(ADMIN_PATH . 'auth/logout');
        }
        $this->breadcrumbs->push("Manufacturing Module", "Manufacturing_Dashboard");
        $this->breadcrumbs->push("Dashboard", "Manufacturing_Dashboard");
        $this->load->model(array('Manufacturing_module/Department_model','Manufacturing_module/Manufacturing_department_order_model','Manufacturing_module/Karigar_order_model','Manufacturing_module/Received_orders_model','Manufacturing_module/Quality_control_model','Manufacturing_module/Hallmarking_model','Manufacturing_module/Make_set_model','Manufacturing_module/kundan_karigar_model','Manufacturing_module/karigar_receive_order_model','Manufacturing_module/Kundan_QC_model','Manufacturing_module/Tag_products_model','Manufacturing_module/Ready_product_model','Manufacturing_module/Manufacturing_dashboard_model','sales_module/Sales_stock_model','sales_module/Sales_recieved_product_model','Manufacturing_module/Mnfg_accounting_rejected_model','Manufacturing_module/Mnfg_accounting_received_model','User_access_model','Manufacturing_module/Repair_product_model','Manufacturing_module/Few_box_model','Manufacturing_module/Sales_return_product_model'));
    }

    public function index($id=""){
        $dep=@$_GET['department_id'];
        if(!empty($dep)){
        $_SESSION['department_id']=$dep;
        }
        $department_id=$this->session->userdata('department_id');
        //echo $department_id;die;  
        $data['page_title'] = "Manufacturing Dashboard";
        $user_id=$this->session->userdata('user_id');
        //print_r($department_id);die;
       /* $dep_id=$this->User_access_model->find($user_id);
        $department_id=$dep_id['department_id']; */
        if($department_id != 0){
        $dep_name=$this->Department_model->find($department_id);
        $data['display_title'] = $dep_name['name'];
        }else{
            $data['display_title'] = "DASHBOARD";
        }

        $data['departments'] = $this->Department_model->get('','','','',false);
        $data['orders'] = $this->Manufacturing_department_order_model->get('','','','',false,$department_id);
        $data['not_sent_to_karigar'] = $this->Karigar_order_model->get('','','','',false,'','',$department_id);
       // $data['not_sent_to_karigar'] =0;//$this->Manufacturing_dashboard_model->get_ammended_products();
        $data['sent_to_karigar'] =$this->Karigar_order_model->get('','','','',false,'1','',$department_id);
        // sizeof($this->Karigar_order_model->get('','','','','1',''));
        //$data['received_orders'] = $this->Manufacturing_dashboard_model->get_pending_receive_product_count();
        $data['received_orders'] = $this->Karigar_order_model->get_order_wise_list('','','','',false,'1',$department_id);


        // $data['manf_received_orders'] = sizeof($this->Received_orders_model->get_order_wise_list('','','','','','1')); 
        //print_r($data['received_orders']);die;
      /*  $data['mnfg_accounting_received_orders'] = $this->Manufacturing_dashboard_model->get_pending_receive_product_count('0');*/
        //$data['received_receipt_mfg'] = $this->Manufacturing_dashboard_model->get_pending_qc();
        $data['received_receipt_mfg'] = $this->Received_orders_model->get_receipt_order('','','','',false,'','',$department_id);
        $data['sending_to_qc_mfg'] = $this->Received_orders_model->get_sending_to_qc('','','','',false,'','',$department_id);

        $data['engaged_karigar']= $this->Karigar_order_model->get_engaged_karigar($department_id);
        $_GET['status'] = 'complete';
        //$data['approved_qc'] = $this->Quality_control_model->get('','','','','', false,$department_id);
        $data['approved_qc'] = $this->Manufacturing_dashboard_model->get_accepted_qc($department_id);
        $_GET['status'] = 'rejected';
        $data['rejected_qc'] = $this->Manufacturing_dashboard_model->get_rejected_qc($department_id);
        //$data['rejected_qc'] = $this->Quality_control_model->get('','','','','', false,$department_id);
        $hallmarking=TRUE;
        $_GET['status'] = 'send';
        $data['sent_to_hallmarking'] = $this->Manufacturing_dashboard_model->get_sent_hm($department_id);
        $_GET['status'] = 'receive';
        $data['approved_hallmarking'] = $this->Hallmarking_model->get('','','','',false,$department_id);
        $_GET['status'] = 'rejected';
        $data['rejected_hallmarking'] = $this->Hallmarking_model->get('','','','',false,$department_id);
        $data['make_set'] = $this->Make_set_model->get('','','','',false,'',$department_id);

        $data['tag_products'] = $this->Tag_products_model->get('1','','','','',false,$department_id);
        
        $_GET['status'] = 'pending';
        $data['kundan_pending'] = $this->kundan_karigar_model->get('','','','',false,$department_id);
        $_GET['status'] = 'complete';
        $data['kundan_complete'] = $this->kundan_karigar_model->get('','','','',false,$department_id);
        $data['receive_order'] = $this->karigar_receive_order_model->get('','','','',false,$department_id);
        $_GET['status'] = 'send_qc_pending';
        $data['sending_to_kundan_qc_mfg'] = $this->Kundan_QC_model->get('','','','',false,$department_id);    
        
        $_GET['status'] = 'pending';
        $data['received_receipt'] = $this->Kundan_QC_model->get('','','','',false,$department_id);
        // $_GET['status'] = 'complete';
        // $data['qc_complete'] = sizeof($this->Kundan_QC_model->get('','','','',FALSE));
        $_GET['status'] = 'rejected';
        $data['kun_qc_rejected'] = $this->Kundan_QC_model->get('','','','',false,$department_id);
        $data['ready_products'] = $this->Ready_product_model->get('','','','',false,$department_id);
        $data['sales_return_product'] = $this->Sales_return_product_model->get('','','','',false,$department_id);
        // $data['rejected_products'] = sizeof($this->Mnfg_accounting_rejected_model->get_order_wise_list('','','','',$limit=FALSE,''));

        $_GET['status'] = 'pending';
        $data['ready_products_pending_approved'] = $this->Ready_product_model->get('','','','',false,$department_id);
        //$data['resend_to_karagir'] = $this->Manufacturing_dashboard_model->get_ammended_products();
        $data['resend_to_karagir'] =$this->Ready_product_model->get('','','','',false,$department_id);
        $data['sales_approved'] = $this->Sales_stock_model->get('','','',false,$department_id);
        

        $_GET['status']="sended_to_manufacture";
        $data['sales_rejected'] = $this->Sales_recieved_product_model->get_voucher_no_wise('','','','','',false,$department_id);
        //print_r($data['sales_rejected']);die;
        $data['repair_product'] = $this->Repair_product_model->get('','','','',false,$department_id,'0');
        $data['sent_to_repair_product'] = $this->Repair_product_model->get_sent_repair_product('','','','',false,$department_id,'1');
        $data['difference_product'] = $this->Repair_product_model->get_differnt('','','','',false,$department_id);
        $data['few_weight'] = $this->Few_box_model->get('','','','',false,$department_id); 
           // print_r($data['few_weight']);die;

        // echo '<pre>'; print_r($data); echo '</pre>'; exit();
        $this->view->render('Manufacturing_Dashboard/index',$data);
    }
}
