<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class Received_receipt extends CI_Controller {
  public function __construct() {
    parent::__construct();
    if(empty($this->session->userdata('user_id'))){
      redirect(ADMIN_PATH . 'auth/logout');
    }
    $this->load->library('excel_lib');
    $this->breadcrumbs->push("Manufacturing Module", "Manufacturing_Dashboard");
    $this->breadcrumbs->push("Dashboard", "Manufacturing_Dashboard");
    $this->load->model(array('Manufacturing_module/Received_orders_model','User_access_model','Manufacturing_module/Department_model'));
    $this->load->config('admin_validationrules', TRUE);
  }
  public function index(){
    $department_id=$this->session->userdata('department_id');
    $department_name=$this->Department_model->find($department_id);;
    $data['dep_id']=$department_id;
    if(isset($_GET['status']) && $_GET['status'] == 'pending')
    {
      $data['display_name'] = $department_name['name']." - All Pending Receipt";
      $this->breadcrumbs->push($department_name['name']." - All Pending Receipt", "Received_receipt");
    }
    else{
      $this->breadcrumbs->push($department_name['name']." - All Received Receipt", "Received_receipt");
      $data['display_name'] = $department_name['name']." - Pending QC";
     }
    $data['page_title'] = 'Recevied Receipt';
    $list=$this->input->post('list');
    if($list !="")
    {
      echo json_encode($this->generate_data_table($department_id));
    }else{
      $this->view->render('Received_receipt/index',$data);
    }
  }
  private function generate_data_table($department_id){
    $filter_status =@$_REQUEST['order'][0];
    $status = array();
    $search=@$_REQUEST['search']['value'];
    //print_r($department_id);die;
    $result = $this->Received_orders_model->get_receipt_order($filter_status,$status,$_REQUEST,$search,$limit=true,$hallmarking ='',$id='',$department_id);

    $totalRecords = $this->Received_orders_model->get_receipt_order($filter_status,$status,$_REQUEST,$search,$limit=false,$hallmarking ='',$id='',$department_id);
    if (!empty($result)) {
      $data = $this->get_datatable_rows($result);
    }else{
      $data = $this->get_null_row();
    }
    return $this->return_json($totalRecords,$data);
  }
  private function get_datatable_rows($result){
    //print_r($result);die;
    foreach ($result as $key => $value) {
      // $data[$key][] ="<input type='checkbox' name='all_check_qc[]' value='".$value["receipt_code"]."'/>";
      $sr_no=$key+1;
      $data[$key][] = '<span style="float:right">'.$sr_no.'</span>';
      $data[$key][] ='<span style="float:right">'.$value['order_id'];
      $data[$key][] =$value['receipt_code'];
      $data[$key][] ='<span style="float:right">'.date('d-m-Y',strtotime($value["order_date"])).'</span>';
      $data[$key][] ='<span style="float:right">'.$value["receipt_date"];
      //$data[$key][] ='<span style="float:right">'.$value['km_code'].'</span>';
      //$data[$key][] =$value["name"];
      if(!empty($value["weight"])){
        $data[$key][] ='<span style="float:right">'.$value["weight"].'</span>';
      }else{
        $data[$key][] ='<span style="float:right">'.$value["gross_wt"].'</span>';
      }
      $data[$key][] ='<span style="float:right">'.$value["net_wt"].'</span>';
      if($value['department_id'] =='2' || $value['department_id']=='3' || $value['department_id'] =='6' || $value['department_id']=='10'):
      $data[$key][] ='<span style="float:right">'.($value["weight"] - $value['qc_weight']).'</span>';
      else:
      $data[$key][] ='<span style="float:right">'.($value["quantity"] - $value['qc_quantity']).'</span>';
      endif;  

      $data[$key][] = '<a href="'.ADMIN_PATH.'Manufacturing_quality_control/check/'.$value["receipt_code"].'">
          <button class="btn btn-link view_link small loader-hide  btn-sm" name="commit" type="button">Qc Check</button>
          </a>&nbsp;&nbsp;
          <a href="'.ADMIN_PATH.'Manufacturing_quality_control/re_print_receipt/'.$value["receipt_code"].'" target="_blank">
          <button class="btn btn-link edit_link small loader-hide  btn-sm" name="commit" type="button">Print</button>
          </a>&nbsp;&nbsp;
          <button  onclick="accept_all_mfg_receipt_qc(this,\''.$value['receipt_code'].'\')" class="btn btn-link accept_link small loader-hide  btn-sm" name="commit"  id="a_'.$value['receipt_code'].'" type="button" >Accept</button>';  
    }
    return $data;
  }
  private function get_null_row(){
    $data[0][] = ['No data found'];
    $data[0][] = [];
    $data[0][] = [];
    $data[0][] = [];
    $data[0][] = [];
    $data[0][] = [];
    $data[0][] = [];
    $data[0][] = [];
    $data[0][] = [];
  /*  $data[0][] = [];
    $data[0][] = [];*/
    return $data;
  }
  private function return_json($totalRecords,$data){
    $json_data = array(
          "draw" => intval($_REQUEST['draw']),
          "recordsTotal" => intval($totalRecords),
          "recordsFiltered" => intval($totalRecords),
          "data" => $data
      );
    return $json_data;
  }
  
}