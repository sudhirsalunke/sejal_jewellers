<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Accounting_department extends CI_Controller {
	public function __construct() {
        parent::__construct();
        if(empty($this->session->userdata('user_id'))){
        redirect(ADMIN_PATH . 'auth/logout');
        }
        $this->breadcrumbs->push("Manufacturing Module", "Manufacturing_Dashboard");
        $this->breadcrumbs->push("Dashboard", "Manufacturing_Dashboard");
        $this->load->model('Manufacturing_module/accounting_department_module');
    }
    public function index(){
	   	$data['display_name'] = 'Orders Pending in Accounting';
	   	$data['page_title'] = 'Products Pending in Accounting';
	   	$list=$this->input->post('list');
	    if($list !="")
	    {
	      echo json_encode($this->generate_data_table());
	    }else{
	      $this->view->render('Manufacturing_module/Accounting_department/index',$data);
	    }
	}
    public function view($order_id){
	   	$data['display_name'] = 'Products Pending in Order '.$order_id ;
	   	$data['page_title'] = 'Products Pending in Order';
	   	$data['order_id'] = $order_id;
	   	$list=$this->input->post('list');
	    if($list !="")
	    {
	      echo json_encode($this->generate_data_table_view($order_id));
	    }else{
	      $this->view->render('Manufacturing_module/Accounting_department/view',$data);
	    }
	}
	private function generate_data_table(){
		$filter_status =@$_REQUEST['order'][0];
	    $status = array();
	    $search=@$_REQUEST['search']['value'];
	    $result = $this->accounting_department_module->get($filter_status,$status,$_REQUEST,$search,$limit=true,$status='1');
	    $totalRecords = sizeof($this->accounting_department_module->get($filter_status,$status,$_REQUEST,$search,$limit=false,$status='1'));
	    if (!empty($result)) {
	      $data = $this->get_order_wise_datatable_rows($result);
	    }else{
	    	$data = $this->get_null_rows(4);
	    }
	    return $this->return_json($totalRecords,$data);
	}
	private function get_order_wise_datatable_rows($result){
		foreach ($result as $key => $value) {
	      $data[$key][] ='<span style="float:right">'.$value['order_id'].'</span>';
	      $data[$key][] =$value['name'];
	      $data[$key][] ='<span style="float:right">'.date('d-m-Y || H:i:s',strtotime($value["order_date"])).'</span>';
	      $data[$key][] ='<a href="'.ADMIN_PATH.'accounting_department/view/'.$value['order_id'].'"><button class="btn btn-link view_link small loader-hide  btn-sm" name="commit" type="button">VIEW</button></a>';
	    }
	    return $data;
	}
	public function get_null_rows($param){
		for ($i=0; $i < $param; $i++) {
			if($i == 0)
				$data[$key][] = 'No record found';
			else
				$data[$key][] = '';
		}
		return $data;
	}
	private function return_json($totalRecords,$data){
	    $json_data = array(
	          "draw" => intval($_REQUEST['draw']),
	          "recordsTotal" => intval($totalRecords),
	          "recordsFiltered" => intval($totalRecords),
	          "data" => $data
	      );
	    return $json_data;
  	}
  	private function generate_data_table_view($order_id){
		$filter_status =@$_REQUEST['order'][0];
	    $status = array();
	    $search=@$_REQUEST['search']['value'];
	    $result = $this->accounting_department_module->get($filter_status,$status,$_REQUEST,$search,$limit=true,$status='1',$order_id,$group_by='kmm.id');
	    $totalRecords = sizeof($this->accounting_department_module->get($filter_status,$status,$_REQUEST,$search,$limit=false,$status='1',$order_id,$group_by='kmm.id'));
	    if (!empty($result)) {
	      $data = $this->get_datatable_rows($result);
	    }else{
	    	$data = $this->get_null_rows(9);
	    }
	    return $this->return_json($totalRecords,$data);
	}

	private function get_datatable_rows($result){
		foreach ($result as $key => $value) {
	      $data[$key][] ='<div class="checkbox checkbox-purpal"><input type="checkbox" class="from-control checkbox_ctn" name="product[ids]['.$value["id"].']" value="'.$value['id'].'"><label></label></div>';
	      $data[$key][] =$value['karigar_name'];
	      $data[$key][] =$value['sub_cat_name'];
	      $data[$key][] ='<input type="text" class="form-control" value="'.$value['approx_weight'].'">';
	      $data[$key][] = '<input type="text" class="form-control"  style="float:right" name="product[qunatity]['.$value["id"].']" value="'.$value['quantity'].'">';
	      $data[$key][] =$value['from_weight'].'-'.$value['to_weight'];
	      $data[$key][] ='<span style="float:right">'.date('d-m-Y H:i:s',strtotime($value["order_date"])).'</span>';
	      $data[$key][] ='<span style="float:right">'.date('d-m-Y H:i:s',strtotime($value["delivery_date"])).'</span>';
	    }
	    return $data;
	}
	public function store(){
		echo '<pre>';
		print_r($_POST);
		echo '</pre>';die;
	}
}