<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class Manufacturing_quality_control extends CI_Controller {
  public function __construct() {
    parent::__construct();
    if(empty($this->session->userdata('user_id'))){
      redirect(ADMIN_PATH . 'auth/logout');
    }
    $this->load->library('excel_lib');
    $this->breadcrumbs->push("Manufacturing Module", "Manufacturing_Dashboard");
    $this->breadcrumbs->push("Dashboard", "Manufacturing_Dashboard");
    $this->load->model(array('Manufacturing_module/Karigar_order_model','Manufacturing_module/Manufacturing_department_order_model','Manufacturing_module/Received_orders_model','Manufacturing_module/Quality_control_model','corporate_module/Receive_products_model','Karigar_model','Manufacturing_module/Hallmarking_model','Manufacturing_module/Prepare_karigar_model','Manufacturing_module/Tag_products_model','sale_master/Parent_category_model','Hallmarking_center_model','User_access_model','Manufacturing_module/Department_model','Manufacturing_module/Ready_product_model'));
    $this->load->config('admin_validationrules', TRUE);
    $this->load->library('m_pdf');
  }
  public function index(){
    $department_id=$this->session->userdata('department_id');
    $department_name=$this->Department_model->find($department_id);
    $data['display_name'] = $department_name['name'].' - QC Checked';
    if($_GET['status'] == 'complete')
      //$data['display_name'] = 'QC Accepted Orders';
      $data['display_name'] =   $department_name['name'].' - Pending For Tagging';
    elseif($_GET['status'] == 'rejected')
      $data['display_name'] = $department_name['name'].' - QC Rejected Orders';
    elseif($_GET['status'] == 'pending')
      $data['display_name'] = $department_name['name'].' - Pending Orders For QC';
    $this->breadcrumbs->push($data['display_name'], "Manufacturing_quality_control");
    $data['page_title'] = 'QC CHECKED ORDERS';
    $list=$this->input->post('list');

    $data['dep_id']=$department_id;
    if($list !="")
    {
      echo json_encode($this->generate_data_table($department_id));
    }else{
      $data['all_karigars'] = $this->Karigar_model->get('','','','',true);
      $data['hallmarking_centers']=$this->Hallmarking_center_model->get('','','','',true);
      $this->view->render('Manufacturing_quality_control/index',$data);
    }
  }
  public function create($receive_product_id=''){
    if(empty($receive_product_id))
      die('Please provide design code');
    $this->breadcrumbs->push("Quality Control Check", "Dashboard/Quality_control");
    $data['page_title'] = "Quality Control Check";
    $data['receive_from_hallmarking'] = false;
    $data['receive_order'] = $this->Received_orders_model->get_receipt_order('','','','','','',$receive_product_id);
    $data['result'] = $this->Quality_control_model->find($receive_product_id);
    $this->view->render('Manufacturing_quality_control/create',$data);
  }

  	public function check_all()
  	{
	    if(empty($_POST['all_check_qc']))
	        redirect('Received_orders');
	    $this->breadcrumbs->push("Quality Control Check", "Dashboard/Quality_control");
	    $data['page_title'] = "Quality Control Check";
      $data['hallmarking_centers']=$this->Hallmarking_center_model->get('','','','',true);
      if (isset($_POST['type']) && $_POST['type']=="qc_hallmarking") {
        $insert_hallmarking=array();
        foreach (explode(',', $_POST['all_check_qc']) as $key => $value) {
           $qc_data = $this->Quality_control_model->find_by_qc_id($value);
           $qc_data['qc_id']=$qc_data['id'];
           unset($qc_data['id']);
           $qc_data['created_at']=date('Y-m-d H:i:s');
           $qc_data['status']='7';
           $qc_data['hallmarking']=1;
           $qc_data['user_id']=$this->session->userdata('user_id');
           $insert_hallmarking[]=$qc_data;

        }
        $hallmarking_qc = $this->Hallmarking_model->hallmarking_qc($insert_hallmarking);
        echo json_encode(array('status' =>$hallmarking_qc,'msg'=>"Hallmarking QC"));exit();

        // $data['total_qty']= $this->Quality_control_model->get_total_qty($_POST['all_check_qc']);
        //$data['receive_from_hallmarking']= true;
        //$data['product_ids'] = $_POST['all_check_qc'];/*Ids*/
        
      }else{
	    foreach ($_POST['all_check_qc'] as $key => $value) {
	    	$data['receive_order'][] = $this->Received_orders_model->get_receipt_order('','','','','','',$value);
        $data['product_ids'] = implode("#", $_POST['all_check_qc']);
	  	}
	    
    }
	    $this->view->render('Manufacturing_quality_control/check',$data);
  	}
  public function check($receipt_code){
    $department_id=$this->session->userdata('department_id');
    $department_name=$this->Department_model->find($department_id);
    $data['page_title'] = $department_name['name']." -  Quality Control Check";
    $data['hallmarking_centers']=$this->Hallmarking_center_model->get('','','','',true);
    $data['receive_order'] = $this->Received_orders_model->get_by_receipt_code($receipt_code);
    $data['department_id']=$department_id;
    // echo $this->db->last_query();
    // print_r($data);exit;
    //$data['rp_quantity'] = $this->Quality_control_model->get_qc_quantity($id);
    //$data['product_ids'] = $id;
    $this->view->render('Manufacturing_quality_control/check',$data);
  }  

  public function store(){
     if($_POST['department_id']=='2'|| $_POST['department_id']=='3' || $_POST['department_id'] =='6' || $_POST['department_id']=='10'){
      $data = $this->Quality_control_model->validateBomPostdata($postdata);
    }else{
      $data = $this->Quality_control_model->validatepostdata($postdata);
    }
    if($data['status'] == 'success'){
      $postdata = $_POST;
      $data = $this->Quality_control_model->store($postdata);
    }
    echo json_encode($data);
  }
  public function store_all($receipt_code =''){
    if(!empty($receipt_code)){
    	$_POST = $this->create_postdata($receipt_code);
  	    if(!empty($_POST)){
  		    $postdata = $_POST;
  		    $data['status'] = 'success';
  	    }
      }else{
  	      $postdata = $_POST;
  	      if($_POST['department_id']=='2' || $_POST['department_id']=='3' || $_POST['department_id'] =='6' || $_POST['department_id']=='10'){
  		        $data = $this->Quality_control_model->validateBomPostdata($postdata);
  	      }else{
  		        $data = $this->Quality_control_model->validatepostdata($postdata);
  	      }
      }
      if($data['status'] == 'success'){
            $data = $this->Quality_control_model->store_all($postdata);
        
      }
	    if(!empty($receipt_code)){
	       redirect(ADMIN_PATH.'Received_receipt');
	      
	    }else{

	  	echo json_encode($data);
	   }
  }

  public function create_postdata($receipt_code){
    $return_array = array();
    $products = $this->Quality_control_model->get_product_qc_by_receipt_code($receipt_code);
    //print_r($products );die;
    if(!empty($products)){
      $return_array['karigar_id'] =$products[0]['karigar_id'];
      $return_array['department_id'] =$products[0]['department_id'];
      $return_array['ghat_wt'] =true;
      $return_array['design_checking'] =true;
      $return_array['pair_matching'] =true;
      $return_array['karatage/purity'] =true;
      $return_array['size'] =true;
      $return_array['stamping'] =true;
      $return_array['sharp_edge'] =true;
      $return_array['solder/linking'] =true;
      $return_array['shape_out'] =true;
      $return_array['finishing'] =true;
      $return_array['gr_wt/nt_wt'] =true;
      $return_array['tag_detail'] =true;
      $return_array['wearing_test'] =true;
      $return_array['alignment'] =true;
      $return_array['packing'] =true;
      $return_array['Meena'] =true;
      foreach ($products as $key => $value) {
        $return_array['qc']['receive_product_id'][] = $value['id'];
        $return_array['qc']['product_code'][$value['id']] = $value['Product_name'];
        if($products[0]['department_id']=='1' || $products[0]['department_id']=='11'){
        $return_array['qc']['quantity'][$value['id']] = $value['quantity'];
        $return_array['qc']['weight'][$value['id']] = $value['net_wt'];
        $return_array['qc']['net_wt'][$value['id']] = $value['net_wt'];
        }else{
        $return_array['qc']['bom_weight'][$value['id']] = $value['weight'];
        $return_array['qc']['bom_nt_wt'][$value['id']] = $value['net_wt'];
        }
        $return_array['qc']['hallmarking_quantity'][$value['id']] = '';
      }
    }
    //print_r($return_array);die;
    return $return_array;
  }
  public function reject(){
    $data = $this->Quality_control_model->validatepostdata();
    if($data['status'] == 'success'){
      $postdata = $_POST;
      $postdata['rejected'] = true;
      $data = $this->Quality_control_model->store_all($postdata);
    }
    echo json_encode($data);
  }
  public function reject_all(){
    $postdata = $_POST;
    if($_POST['department_id']=='2' || $_POST['department_id']=='3' || $_POST['department_id'] =='6' || $_POST['department_id']=='10'){
      $data = $this->Quality_control_model->validateBomPostdata($postdata);
    }else{
      $data = $this->Quality_control_model->validatepostdata($postdata);
    }
    if($data['status'] == 'success'){
      $postdata['rejected'] = true;
      $data = $this->Quality_control_model->store_all($postdata);
    }
    echo json_encode($data);
  }

  public function karigar_receipt($param='')
  {
    $department_id=$this->session->userdata('department_id');
    $department_data=$this->Department_model->find($department_id); 
    if($param == '1'){
      $data['is_kundan'] = true;
      $_POST['is_kundan'] = true;
      $_POST['is_group_by'] = true;
    }
    $data['display_name'] =$department_data['name']. ' - Receive products from karigar';
    $this->breadcrumbs->push('Karigar Receipt', "Manufacturing_quality_control");
    $data['page_title'] = 'Karigar Receipt';
    $data['department_id']=$department_id;
    $data['tagging_status']=$department_data['tagging_status'];
   //print_r($data['tagging_status']);die;
    $data['all_karigars'] = $this->Karigar_model->get('','','','',true);
    $data['receive_from_kariger'] = $this->Quality_control_model->get_engage_karigars($department_id);

    $this->view->render('Manufacturing_quality_control/kariger_receipt',$data);
  }

  public function receive_product($id)
  { $department_id=$this->session->userdata('department_id');
    echo json_encode($this->Quality_control_model->receive_product($id,$department_id));
  }
  public function print_receipt()
  {   
    $data['result']          =  $_POST;   
    $data['receipt_code'] = $this->Quality_control_model->create_receipt_code();
    $user_id=$this->session->userdata('user_id');
    $dep_id=$this->User_access_model->find($user_id); 
    $department_id=$this->session->userdata('department_id');
    $data['department_id']=$department_id;
/*     echo '<pre>';
     print_r($data);
     echo '</pre>';die;*/
     $this->load->view('Manufacturing_quality_control/kariger_receipt_print', $data);
     // $html=$this->load->view('Manufacturing_quality_control/kariger_receipt_print', $data,true);
     // $pdfFilePath = "Print_order_".$_POST['kariger_id'].".pdf";
     // $this->m_pdf->pdf->WriteHTML($html);
     // $this->m_pdf->pdf->Output($pdfFilePath, "D");
  }
  public function re_print_receipt($code)
  {
     $data['receipt_code'] = $this->Quality_control_model->get_product_by_receipt_code($code);
    $user_id=$this->session->userdata('user_id');
    $dep_id=$this->User_access_model->find($user_id); 
    $department_id=$dep_id['department_id'];
    $data['department_id']=$dep_id['department_id'];
     $this->load->view('Manufacturing_quality_control/kariger_receipt_print', $data);
     // $pdfFilePath = "Print_order_".$_POST['kariger_id'].".pdf";
     // $this->m_pdf->pdf->WriteHTML($html);
     // $this->m_pdf->pdf->Output($pdfFilePath, "D");
  }
  private function generate_data_table($department_id){
    $filter_status =@$_REQUEST['order'][0];
    $status = array('p.created_at','p.product_code');
    $search=@$_REQUEST['search']['value'];
   // print_r($department_id);die;
    $result = $this->Quality_control_model->get($filter_status,$status,$_REQUEST,$search,$limit=true,'',$department_id);
    $totalRecords = $this->Quality_control_model->get($filter_status,$status,$_REQUEST,$search,$limit=false,'',$department_id);
    /*print_r($result);
    echo $this->db->last_query();die;*/
    if (!empty($result)) {
      $data = $this->get_datatable_rows($result);
    }else{
      $data = $this->get_null_row();
    }
    return $this->return_json($totalRecords,$data);
  }
  private function get_datatable_rows($result){
    //print_r($result);
    foreach ($result as $key => $value) {
      // $data[$key][0] ='<input type="checkbox" value="'.$value['id'].'" name="send_all_to_hlmar[]"/>';
       $sr_no=$key+1;
      $data[$key][] = '<span style="float:right">'.$sr_no.'</span>';
     /* if($_GET['status'] == 'rejected')
         $data[$key][] = '<a href="'.ADMIN_PATH.'Manufacturing_quality_control/view_remark/'.$value['id'].'">'.$value['receipt_code'].'</a>';
       $data[$key][] = $value['receipt_code'];
      else*/
      $data[$key][] = $value['receipt_code'];
      $data[$key][] =  '<span style="float:right">'. $value['order_id'].'</span>';
      $data[$key][] =$value['sub_cat_name'];
      $data[$key][] ='<input type="hidden" id="department_id_'.$value['id'].'" name="department_id" value="'.$value['department_id'].'">'.$value['karigar_name'];
      if($_GET['status'] == 'rejected'){
        $rejected_res = $this->Manufacturing_department_order_model->check_rejected_quantity($value['id']);
        $ammended_res = $this->Quality_control_model->get_ammended_products($value['id']);
        $rejected_wt = $this->Manufacturing_department_order_model->check_rejected_weight($value['id']);  
          if($value['department_id'] == '2' || $value['department_id']=='3' || $value['department_id'] =='6' || $value['department_id']=='10'):
                      $wt= $value['rejected_weight']-$value['weight'];
                  $data[$key][] ='<input type="text" id="weight_'.$value['id'].'" class="form-control" value="'.($value['qc_weight']- $rejected_wt['rejected_weight']).'" name="weight"><p class="text-danger" id="rejected_weight_'.$value['id'].'_error"></p><input type="hidden" id="original_wt_'.$value['id'].'" value="'.$value['qc_weight'].'">';
           else: 
                  $data[$key][] ='<input type="text" id="quantity_'.$value['id'].'" class="form-control" value="'.($value["qc_quantity"] - $rejected_res['rejected_quantity'] - $ammended_res['rejected_quantity']).'" name="quantity"><p class="text-danger" id="quantity_'.$value['id'].'_error"></p><input type="hidden" id="original_qty_'.$value['id'].'" value="'.($value["qc_quantity"] - $rejected_res['rejected_quantity'] - $ammended_res['rejected_quantity']).'">';
          endif;
      }
      else{
            $tp_quantity = $this->Tag_products_model->get_quantity_by_id($value['id']);
            $hm_quantity = $this->Quality_control_model->get_quantity_by_id($value['id']);
            $QC_QUANTITY=$value["qc_quantity"] - $tp_quantity -$hm_quantity;

            $data[$key][] ='<span style="float:right">'. $QC_QUANTITY.'</span>';
      }
       if($_GET['status'] == 'rejected'){
        //$data[$key][] ='<input type="text" id="weight_'.$value['id'].'" class="form-control" value="'.(($value["from_weight"] + $value["to_weight"])/2 * $value["qc_quantity"]).'" name="quantity"><p class="text-danger" id="weight_'.$value['id'].'_error"></p>';
          
      if($value['department_id'] != '2' && $value['department_id']!='3' && $value['department_id'] !='6' && $value['department_id']!='10'){
                $data[$key][] ='<input type="text" id="weight_'.$value['id'].'" class="form-control" name="quantity"><p class="text-danger" id="weight_'.$value['id'].'_error"></p>';
              }
      }else{
  /*      $data[$key][] ='<span style="float:right">'. (($value["from_weight"] + $value["to_weight"])/2 * $value["qc_quantity"]).'</span>'; */ 
         $data[$key][] ='<span style="float:right">'. $value["qc_weight"].'</span>';
         $data[$key][] ='<span style="float:right">'. $value["net_wt"].'</span>';
           
      }
      if($value['status']=='8'){
        //$button_html = '<a href="'.ADMIN_PATH.'"><button id="b_'.$value['id'].'" class="btn btn-primary small loader-hide  btn-sm" name="commit" type="button">Tag Products</button></a>';
       $button_html ='ACCEPTED';
      }
      else{
        //$button_html = '<button id="b_'.$value['id'].'" onclick="resend_to_karigar('.$value["id"].')" class="btn btn-primary small loader-hide  btn-sm" name="commit" type="button">Send To Orders</button>&nbsp;<button id="b_'.$value['id'].'" onclick="ammend_mnfc_qc_products('.$value["id"].')" class="btn btn-primary small loader-hide  btn-sm" name="commit" type="button">Resend To karigar</button>';
          if($value['department_id'] == '2'|| $value['department_id']=='3' || $value['department_id'] =='6' || $value['department_id']=='10'){
            $button_html = '<input type="hidden" id="karigar_id_'.$value['id'].'" value="'.$value['karigar_id'].'"><button id="b_'.$value['id'].'" onclick="bom_mnfg_send_to_karigar('.$value["id"].')" class="btn btn-link view_link small loader-hide  btn-sm" name="commit" type="button">Send To Karigar</button>';
          }else{

             $button_html = '<input type="hidden" id="karigar_id_'.$value['id'].'" value="'.$value['karigar_id'].'"><button id="b_'.$value['id'].'" onclick="mnfg_send_to_karigar('.$value["id"].')" class="btn btn-link view_link small loader-hide  btn-sm" name="commit" type="button">Send To Karigar</button>';
          }
        // $data[$key][] ='REJECTED';
        $data[$key][] =$button_html;
      }
      // $data[$key][] = $button_html;  
       $data[$key][] = '';
    }
    return $data;
  }
  private function get_null_row(){
    $data[0][] = ['No data found'];
    $data[0][] = [];
    $data[0][] = [];
    $data[0][] = [];
    $data[0][] = [];
    $data[0][] = [];
    $data[0][] = [];
    $data[0][] = [];
    $data[0][] = [];
    $data[0][] = [];
    //$data[0][6] = [];
    return $data;
  }
  private function return_json($totalRecords,$data){
    $json_data = array(
          "draw" => intval($_REQUEST['draw']),
          "recordsTotal" => intval($totalRecords),
          "recordsFiltered" => intval($totalRecords),
          "data" => $data
      );
    return $json_data;
  }
  public function receive_from_hallmarking($receive_product_id){
    if(empty($receive_product_id))
      die('Please provide design code');
    $data['receive_from_hallmarking'] = true;
    $this->breadcrumbs->push("Quality Control Check(After Hallmarking)", "Dashboard/Quality_control");
    $data['page_title'] = "Quality Control Check (After Hallmarking)";
    $data['receive_product'] = $this->Receive_products_model->find($receive_product_id);
    $data['result'] = $this->Quality_control_model->find($receive_product_id);
    $this->view->render('Quality_control/create',$data);
  }
  public function send_to_hallmarking($qc_id){
    if(empty($qc_id))
      die('Please provide design code');
    $result = $this->Quality_control_model->send_to_hallmarking($qc_id);
    echo json_encode($result);
  }
  public function send_all_to_hallmarking(){
    if(empty($_POST['send_all_to_hlmar']))
      die('Please provide design code');
    foreach ($_POST['send_all_to_hlmar'] as $key => $value) {
       $result = $this->Quality_control_model->send_to_hallmarking($value);      
    }
    $this->print_voucher($_POST['send_all_to_hlmar']);
    redirect(ADMIN_PATH."Manufacturing_Dashboard");
  }
  public function send_to_prepare_order($corporate_product_id)
  {
    if(empty($corporate_product_id))
      die('Please provide design code');
    $result = $this->Quality_control_model->send_to_prepare_order($corporate_product_id);
    echo json_encode($result);
  }
  public function export($search='')
  {
    $data = $this->Quality_control_model->export_qc_products('','','',$search,'','');
    /*foreach ($_POST['product_code'] as $key => $value) {
      $corporate = $this->Corporate_Products_model->get_corporates_by_corporate_id($value);
      if($corporate['corporate'] == '1')
        $this->Stock_model->store($value,$status=2,$corporate['corporate']);
      else
        $this->Stock_model->store($value,$status=5,$corporate['corporate']);
    }*/
    $header_array=array("WO_Id","WO_Srl","Item_Id","PCS","Ext_Item_Id","Article_Desc","g_wt");
    $file_name="QC.xlsx";
    $excel_data = create_excel_array($data,$header_array);
    $this->excel_lib->export($excel_data,$header_array,$file_name,'','',$save=true);
  }
  
  public function view_remark($id)
  {
    $data['page_title'] = 'QC CHECKED PRODUCTS';
    $res = $this->Quality_control_model->find_by_qc_id($id);
    if($res['status']=='0'){
      $this->breadcrumbs->push("QC Rejected Products","Manufacturing_quality_control?status=rejected");
    }
    elseif($res['status']=='6'){
      $this->breadcrumbs->push("Amended Products", "Amend_products");
    }
    //$this->breadcrumbs->push("QC Rejected Products", "Quality_control?status=rejected");
    $this->breadcrumbs->push("View Remark", "Manufacturing_quality_control");
    $data['display_name'] = 'View Remark';
    $data['result'] =$res;// $this->Quality_control_model->get_remark($id);
    $this->view->render('Manufacturing_quality_control/view_remark',$data);
  }
  public function Send_rejected_products($qc_id){
    $result = $this->Quality_control_model->find_by_qc_id($qc_id);
    $karigar_id = $this->Quality_control_model->get_karigar_id($result['corporate_product_id']);
    $status = $this->Quality_control_model->insert_in_prepared_order($result,$karigar_id);
    if($status == true){
      $array = array('status'=>'5');
      $where = array('id'=>$qc_id);
      $this->Quality_control_model->update($array,$where);
    }
    $data['status'] = 'success';
    $data['karigar_id'] = $karigar_id;
    echo json_encode($data);die;
  }

 public function print_voucher($checked_ids){
      $data=array();
      $data['data'] = $this->Quality_control_model->get_printdata($checked_ids);
      $data['department_name'] = $data['data'][0]['department_name'];
     // print_r($data);
      $html =$this->load->view('Manufacturing_quality_control/print', $data,true); 
      // $data['receipt_code'] = $this->Karigar_receive_order_model->create_kundan_receipt_code();
      // $html=$this->load->view('Manufacturing_quality_control/print', $data,true); 
      // makeDirectory('Manufacturing_reciepts');
      // $pdfFilePath = dirname(dirname(dirname(dirname(__FILE__)))) . "/uploads/kundan_receive_pdf/kundan_pdf.pdf";
      $pdfFilePath="Hallmarking_send.pdf";
       $this->m_pdf->pdf->WriteHTML($html);
       ob_clean();
       $this->m_pdf->pdf->Output($pdfFilePath, "D");
      // $final_result['status'] = 'success';
      // $final_result['data']['url'] = ADMIN_PATH.'karigar_receive_order/download_pdf';
      // echo json_encode($final_result);
  }
  public function resend_to_karigar($qc_id){
    // print_r($_POST);die;
      if($_POST['department_id']=='2' || $_POST['department_id']=='3' || $_POST['department_id'] =='6' || $_POST['department_id']=='10'){
          $status = $this->Quality_control_model->validate_rejected_bom_data($qc_id);
      }else{
          $status = $this->Quality_control_model->validate_rejected_data($qc_id);
      }
      if($status['status'] == 'failure'){
        echo json_encode($status);die;
      }

 
   // $qc_data = $this->Quality_control_model->find_by_qc_id($qc_id);
    $insert_array = $this->Quality_control_model->get_printdata($qc_id);
    // $variant_data = $this->Manufacturing_department_order_model->find_by_manufacturing_order_id($qc_id);

 //
       
   $p_variant= $insert_array[0]['p_variant'];
   $variant=explode(',', $p_variant);  
    $insert_order[] = array(
      'sub_category'=>$insert_array[0]['sub_category_id'],
      'department_id'=>$insert_array[0]['department_id'],
      'weight_range_id'=>$insert_array[0]['weight_range_id'],
      'weight'=>@$_POST['weight'],
      'rejected_weight'=>@$_POST['weight'],
      'rejected_qc_id'=>$qc_id,
      'quantity'=>@$_POST['quantity'],
      'order_date'=>date('Y-m-d'),
      'tr_count'=>'',
      'sub_cat_img_id'=>'',
      'department_id'=>$_POST['department_id'],
      'order_name'=>$insert_array[0]['order_name'],
      'variant'=>$variant,
      );  
    //print_r($insert_array[0]['sub_category_id']);die;
    $mapping_data = $this->Manufacturing_department_order_model->store($insert_order,'',$qc_rejected=true);
      if($_POST['department_id'] =='2'|| $_POST['department_id']=='3' || $_POST['department_id'] =='6' || $_POST['department_id']=='10'){
             $kmm_array = array(
              'total_weight'=>$_POST['weight'],
              'mop_id'=>$mapping_data['order_id'],
              'karigar_id'=>$_POST['karigar_id'],
              'department_id'=>$_POST['department_id'],
            );
      }else{
              $kmm_array = array(
                  'quantity'=>$_POST['quantity'],
                  'approx_weight'=>$_POST['weight'],
                  //'approx_weight'=>($insert_array[0]['from_weight'] + $insert_array[0]['to_weight'])/2,
                  'mop_id'=>$mapping_data['order_id'],
                  'karigar_id'=>$_POST['karigar_id'],
                  'department_id'=>$_POST['department_id'],
              );
      }   

    $id = $this->Prepare_karigar_model->store($kmm_array);
    if($_POST['department_id'] =='2' || $_POST['department_id']=='3' || $_POST['department_id'] =='6' || $_POST['department_id']=='10'){
            if(!empty($id)){
              $rejected_res = $this->Manufacturing_department_order_model->check_rejected_weight($qc_id);
              if($_POST['original_wt'] <= $rejected_res['rejected_weight']){
                $array = array('status'=>'5');
                //$update = array('id'=>$qc_data['id']);
                 $update = array('id'=>$qc_id);
                $this->Quality_control_model->update($array,$update);
              }
            }
    }else{
            if(!empty($id)){
            $rejected_res = $this->Manufacturing_department_order_model->check_rejected_quantity($qc_id);
            //print_r($_POST['original_qty']. "<=". $rejected_res['rejected_quantity']);die;
            if($_POST['original_qty'] <= $rejected_res['rejected_quantity']){
              $array = array('status'=>'5');
              //$update = array('id'=>$qc_data['id']);
              $update = array('id'=>$qc_id);
              $this->Quality_control_model->update($array,$update);
            }
          }
    }
    echo json_encode(array('status'=>'success','url'=>ADMIN_PATH.'Karigar_order_list/show/'.$id));
    //echo json_encode(array('status'=>'success','url'=>ADMIN_PATH.'Manufacturing_department_order/view/'.$mapping_data['order_id']));
  }
  public function check_quantity(){
  
    $postdata = $_POST['kr'];
    $data['status'] = 'success'; 

    $net_wt_v = array_column($postdata, 'net_wt');
        if(!array_filter($net_wt_v)){
         $data['status'] = 'failure1';
         echo json_encode($data);die;
        }      
 // print_r($net_wt_v);die;
    foreach ($postdata as $key => $value) {
      $net_wt[$key]=$value['net_wt'];
      $few_box_status[$key]=$value['few_box_status'];
      $department_id[$key]=$value['department_id'];

    if(!empty($net_wt[$key])){
          $_POST = $value;  
       /* if($few_box_status[$key]=='1' && $department_id[$key]!='2' && $department_id[$key] !='3' && $department_id[$key] !='6' && $department_id[$key] !='10' ){
          $this->form_validation->set_rules('few_wt', 'Few wt', 'required|numeric|is_natural_no_zero');
        }*/
       
          $this->form_validation->set_rules('net_wt', 'Net Weight', 'required|numeric|greater_than_equal_to[0.1]');
          $this->form_validation->set_rules('gross_wt', 'Gross Weight', 'numeric|greater_than_equal_to[0.1]');
          $this->form_validation->set_rules('wastage', 'Wastage', 'numeric|greater_than_equal_to[0.1]');
          $this->form_validation->set_rules('amount', 'Amount', 'numeric|greater_than_equal_to[0.1]');
          $this->form_validation->set_rules('melting', 'Melting', 'required|numeric|greater_than_equal_to[0.1]');
          // $expr = '/^[1-9][0-9]*$/';
          // if (preg_match($expr, $value['quantity']) && filter_var($value['quantity'], FILTER_VALIDATE_INT)) {
          //   $kmm_res = $this->Quality_control_model->check_quantity($key,$value['quantity']);
          //   if($kmm_res['status'] == 'error'){
          //     echo json_encode($kmm_res);die;
          //   }
          // } else {
          //     $data['status'] = 'error';
          //     $data['error']['quantity'][$key] = 'Quantity field should be numeric';
          //     echo json_encode($data);die;
          // }
          if($department_id[$key] != '2' && $department_id[$key]!='2' && $department_id[$key] !='3' && $department_id[$key] !='6' && $department_id[$key] !='10'){
           
              $this->form_validation->set_rules('quantity', 'Quantity', 'required|numeric|is_natural_no_zero');
              $kmm_res = $this->Quality_control_model->check_quantity($key,$value['quantity']);
              if($kmm_res['status'] == 'error'){
                echo json_encode($kmm_res);die;
              }
          }else{
         
            $this->form_validation->set_rules('weight', 'Weight', 'required|numeric|greater_than_equal_to[0.1]');
          }
          if ($this->form_validation->run() == FALSE)
          {
            $data['status'] = 'failure';
            $data['error'][$key]= $this->error_msg($key);
          }
          $this->form_validation->reset_validation();
      }
  }
    echo json_encode($data);die;
  }
/*  public function error_msg(){
    return array(
        'quantity' => strip_tags(form_error('quantity')),
        'net_wt' => strip_tags(form_error('net_wt')),
        'wastage' => strip_tags(form_error('wastage')),
        'gross_wt' => strip_tags(form_error('gross_wt')),
        'amount' => strip_tags(form_error('amount')),
        'melting' => strip_tags(form_error('melting')),
      );
  }
*/
   private function error_msg($key){
    return array(
      'weight_error_'.$key=>strip_tags(form_error('weight')),
      'quantity_error_'.$key=>strip_tags(form_error('quantity')),
      'net_wt_error_'.$key=>strip_tags(form_error('net_wt')),
      'gross_wt_error_'.$key=>strip_tags(form_error('gross_wt')),    
      'wastage_error_'.$key=>strip_tags(form_error('wastage')),
      'amount_error_'.$key=>strip_tags(form_error('amount')),
      'melting_error_'.$key=>strip_tags(form_error('melting')),
      'few_wt_error_'.$key=>strip_tags(form_error('few_wt')),
      // 'stone_wt_error_'.$key=>strip_tags(form_error('stone_wt')),
    );
  }

  public function hallmarking_receipt(){
    if(!empty($_SESSION['hm_id'])){
      foreach ($_SESSION['hm_id'] as $key => $value) {
      $data['data'][] = $this->Quality_control_model->get_hallmarking_data($value);
    }
    //  $data['data'][] = $this->Quality_control_model->get_hallmarking_data($_SESSION['hm_id']);
      // echo '<pre>';
      // print_r($data);
      // echo '</pre>';die;
    }else{
      redirect(ADMIN_PATH . 'Received_receipt');
    }
    unset($_SESSION['hm_id']);
    $this->load->view('Manufacturing_quality_control/print', $data);

  }
  public function qc_ammend($qc_id){
    $status = $this->Quality_control_model->validate_rejected_data($qc_id);
    if($status['status'] == 'failure'){
      echo json_encode($status);die;
    }

    $qc_data = $this->Quality_control_model->find_by_qc_id($qc_id);
    //print_r($_POST);die;
    $voucher_data = $this->Quality_control_model->get_voucher_details($qc_id);
    $voucher_data['weight'] = $_POST['weight'];
    $voucher_data['quantity'] = $_POST['quantity'];
    $_SESSION['voucher'] = $voucher_data;
    $insert_order = array(
      'qc_id'=>$qc_id,
      'status'=>3,
      'quantity'=>$_POST['quantity'],
      'module'=>2,
      'created_at'=>date('Y-m-d H:i:s'),
      'rejected_weight'=>$_POST['weight']
      );
    $this->Quality_control_model->insert_ammended_products($insert_order);
    $rejected_res = $this->Quality_control_model->get_ammended_products($qc_id);
      if($_POST['original_qty'] <= $rejected_res['rejected_quantity']){
        $array = array('status'=>'5');
        $update = array('id'=>$qc_data['id']);
        $this->Quality_control_model->update($array,$update);
      }
    echo json_encode(array('status'=>'success','url'=>ADMIN_PATH.'Manufacturing_quality_control/generate_karigar_voucher'));  
  }
  public function resend_to_karagar(){
    $data['result'] = $this->Quality_control_model->get_ammended_list();
    $data['page_title'] = 'Sent Products To Karigar';
    $this->breadcrumbs->push('QC Sended Products To Karigar', "Manufacturing_quality_control");
    $this->view->render('Manufacturing_quality_control/resend',$data);
  }
  public function generate_karigar_voucher(){
    $data['result'] = $_SESSION['voucher'];
    $this->load->view('Manufacturing_quality_control/karigar_resend_receipt', $data);
  }
}
