<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class Karigar_receive_order extends CI_Controller {
	public function __construct(){
		parent::__construct();
    if(empty($this->session->userdata('user_id'))){
      redirect(ADMIN_PATH . 'auth/logout');
    }
    $this->breadcrumbs->push("Manufacturing Module", "Manufacturing_Dashboard");
  	$this->breadcrumbs->push("Dashboard", "Manufacturing_Dashboard");
  	$this->load->model(array('Manufacturing_module/Karigar_receive_order_model','Manufacturing_module/Quality_control_model','Karigar_model','Manufacturing_module/Ready_product_model','User_access_model','Manufacturing_module/Department_model','Manufacturing_module/Tag_products_model','order_module/Customer_order_model','Category_model'));
  	$this->load->config('admin_validationrules', TRUE);
    $this->load->library('m_pdf');
             $this->load->library('zend');
        $this->zend->load('Zend/Barcode');
	}
  public function index(){
    $department_id=$this->session->userdata('department_id');
    $department_name=$this->Department_model->find($department_id);
    $data['display_name'] = "Create Kundan Receipt";
    $this->breadcrumbs->push($department_name['name']." - All Receive Products", "Create_Kundan_receipt");
    $data['page_title'] = 'Create Kundan Receipt';
    $data['page_heading'] = " Received products from karigar";

    $list=$this->input->post('list');
    if($list !="")
    {
      echo json_encode($this->generate_data_table($department_id));
    }else{
      $this->view->render('Manufacturing_module/Create_Kundan_receipt/index',$data);
    }
  }
  public function generate_data_table($department_id){
    $filter_status =@$_REQUEST['order'][0];
    $search=@$_REQUEST['search']['value'];
    $status = array();
    $result = $this->Karigar_receive_order_model->get($filter_status,$status,$_REQUEST,$search,$limit=true,$department_id);
    //echo $this->db->last_query();print_r($result);exit;
    $totalRecords = $this->Karigar_receive_order_model->get($filter_status,$status,$_REQUEST,$search,$limit=false,$department_id);
    if (!empty($result)) {
      $data = $this->get_datatable_rows($result);
    }else{
      $data = $this->get_null_row();
    }
    return $this->return_json($totalRecords,$data);
  }
  private function get_datatable_rows($result){
    foreach ($result as $key => $value) {
      $data[$key][0] =  $value['km_name'];
      $data[$key][1] =$value['product_code'];
      $data[$key][2] = $value['name'];
      $data[$key][3] ='<span style="float:right">'. $value['quantity'].'</span>';
      $data[$key][4] ='<span style="float:right">'. round($value['gross_wt'],2).'</span>';
      $data[$key][5] ='<span style="float:right">'. round($value['net_wt'],2).'</span>';
      // $data[$key][] = $value['from_weight'].'-'.$value['to_weight'];
      // $data[$key][] = (($value['from_weight'] + $value['to_weight'])/2 * $value['quantity']);
      
    }
    return $data;
  }
  private function get_null_row(){
    $msg="No data found";
    for ($i=0; $i <=5 ; $i++) { 
      $data[0][$i] = [$msg];
      $msg="";
    }
    return $data;
  }
  private function return_json($totalRecords,$data){
    $json_data = array(
          "draw" => intval($_REQUEST['draw']),
          "recordsTotal" => intval($totalRecords),
          "recordsFiltered" => intval($totalRecords),
          "data" => $data
      );
    return $json_data;
  }
  public function validate(){
    $result = $this->Karigar_receive_order_model->validate();
    echo json_encode($result);
  }
  public function print_pdf(){
    $result = $this->Karigar_receive_order_model->validate_excel();
    //print_r($_POST);die;
    if($result['status'] == 'failure'){
      echo json_encode($result);die;
    } 

      if($_POST['action_type']=='edit'){
        $data['receipt_code'] = $this->Ready_product_model->update_from_new_stock();
      }else if($_POST['type']!='update'){      
        $data['receipt_code'] = $this->Karigar_receive_order_model->create_kundan_receipt_code();
      }else if($_POST['type'] == 'update'){
           $update = $this->Ready_product_model->update_from_kundan_receipt_code();
      }
      if($_POST['action_type']== 'stock' || $_POST['action_type']== 'edit' || $_POST['action_type']== 'repair_stock'){
        $final_result['data']['url'] = ADMIN_PATH.'ready_product';
        $final_result['url_stock'] = '';
        $final_result['status'] = 'success';
      }else if($_POST['type']=='update'){
         $final_result['status'] = 'success';
         $final_result['karigar_id'] = $_POST['kariger_id'];
      } else{
           $html=$this->load->view('Manufacturing_module/Create_Kundan_receipt/print', $data,true); 
          
          makeDirectory('kundan_receive_pdf');
          $pdfFilePath = dirname(dirname(dirname(dirname(__FILE__)))) . "/uploads/kundan_receive_pdf/kundan_pdf.pdf";
          $this->m_pdf->pdf->WriteHTML($html);
          ob_clean();
          $this->m_pdf->pdf->Output($pdfFilePath, "F");
          $final_result['status'] = 'success';
          $final_result['karigar_id'] = '';
          $final_result['data']['url'] = ADMIN_PATH.'karigar_receive_order/download_pdf';
          $final_result['data']['url'] = ADMIN_PATH.'karigar_receive_order/karigar_receipt';
      
      }
      //print_r($final_result);die;
       echo json_encode($final_result);
  }
  public function karigar_receipt($karigar_id=""){
    $data['is_kundan'] = true;
    $_POST['is_kundan'] = true;
    $_POST['is_group_by'] = true;
    $department_id=$this->session->userdata('department_id');
    $department_name=$this->Department_model->find($department_id);
    $data['display_name'] = 'Karigar Receipt';
    $this->breadcrumbs->push('Karigar Receipt ', "Manufacturing_quality_control");
    $data['page_title'] = 'Karigar Receipt';
    $data['karigar_id'] = $karigar_id;
    //print_r($karigar_id);die;

    $data['receive_from_kariger'] = $this->Karigar_receive_order_model->get_engaged_kundan_karigar();
    $this->view->render('Manufacturing_module/Create_Kundan_receipt/create',$data);
  }
  public function get_details($karigar_id)
  {
    $result = $this->Karigar_receive_order_model->get_details($karigar_id);
    echo json_encode($result);
  }
  public function download_pdf(){
    if(!empty($_SESSION['kundan_receive_products'])){
      foreach ($_SESSION['kundan_receive_products'] as $key => $value) {
        $data['receipt_code'][] = $this->Karigar_receive_order_model->find($value);
        $data['karigar_name']=@$data['receipt_code'][0]['km_name'];
      }
    }else{
      redirect(ADMIN_PATH.'karigar_receive_order');
    }
    $this->load->view('Manufacturing_module/ready_product/print_barcode',$data);
    //$this->load->view('Manufacturing_module/Create_Kundan_receipt/print', $data);
    //unset($_SESSION['kundan_receive_products']);
    // $pdfFilePath = dirname(dirname(dirname(dirname(__FILE__)))) . "/uploads/kundan_receive_pdf/kundan_pdf.pdf";
    // $name = "kundan_pdf.pdf";
    // if (file_exists($pdfFilePath)) { 
    //   header('Content-type: application/force-download'); 
    //   header('Content-Disposition: attachment; filename='.$name); 
    //   readfile($pdfFilePath);
    // }
  }
}