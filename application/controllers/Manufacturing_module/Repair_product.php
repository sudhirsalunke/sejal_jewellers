<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class Repair_product extends CI_Controller {
  public function __construct(){
    parent::__construct();
    if(empty($this->session->userdata('user_id'))){
      redirect(ADMIN_PATH . 'auth/logout');
    }
    $this->breadcrumbs->push("Manufacturing Module", "Manufacturing_Dashboard");
    $this->breadcrumbs->push("Dashboard", "Manufacturing_Dashboard");
    $this->load->model(array('Manufacturing_module/Repair_product_model','User_access_model','Manufacturing_module/Department_model', 'sale_master/Parent_category_model', 'Category_model','Manufacturing_module/Ready_product_model','sales_module/Sales_stock_model','Weight_range_model','Karigar_model',));
    $this->load->config('admin_validationrules', TRUE);
          $this->load->library('zend');
        $this->zend->load('Zend/Barcode');

  }

  public function index($department_id=''){
    $user_id=$this->session->userdata('user_id');
    $department_id=$this->session->userdata('department_id');
    
    $dep_id=$this->User_access_model->find($user_id); 
    $department_name=$this->Department_model->find($department_id);
    
    $data['department_id'] = $department_id;  
    $data['page_title'] = 'View Repair Product';
    $data['display_name'] = $department_name['name'].' - Repair Product';
    $this->breadcrumbs->push("Dashboard", "Manufacturing_Dashboard");
      // $this->view->render('Manufacturing_module/repair_product/index',$data);
    $list=$this->input->post('list');
    if($list !=""){
      echo json_encode($this->generate_data_table($department_id));
    }else{
       
      $this->view->render('Manufacturing_module/repair_product/index',$data);
    }
  }

  public function repair_product_list(){
    $user_id=$this->session->userdata('user_id');
    $department_id=$this->session->userdata('department_id');
    
    $dep_id=$this->User_access_model->find($user_id); 
    $department_name=$this->Department_model->find($department_id);
    
    $data['department_id'] = $department_id;  
    $data['page_title'] = 'View Repair Product';
    $data['display_name'] = $department_name['name'].' - Repair Product';
    $this->breadcrumbs->push("Dashboard", "Manufacturing_Dashboard");
      // $this->view->render('Manufacturing_module/repair_product/index',$data);
    $list=$this->input->post('list');
    if($list !=""){
        echo json_encode($this->generate_data_table_list($department_id));
    }else{
       
      $this->view->render('Manufacturing_module/repair_product/repair_product_list',$data);
    }
  }

  private function generate_data_table_list($department_id){
  //print_r($department_id);die;
    $start=0;
    $filter_status =@$_REQUEST['order'][0];
    $search=@$_REQUEST['search']['value'];
    $status = array();
    $result = $this->Repair_product_model->get($filter_status,$status,$_REQUEST,$search,$limit=true,$department_id,'0');
    $totalRecords = $this->Repair_product_model->get($filter_status,$status,$_REQUEST,$search,$limit=false,$department_id,'0');
  // echo "<pre>";print_r($result);die;
    if (!empty($result)) {
        foreach ($result as $key => $value) {
           $start++;    
      if($department_id=='11'){    
        $enc_id= $value["id"];     
        $data[$key][] = '<span style="float:right" id="'.$value['id'].'">'.$start.'</span>';
        $data[$key][] = $value['product_code'];     
        $data[$key][] = $value['quantity'];     
        $data[$key][] = $value['net_wt_o'];
        $data[$key][] = $value['gr_wt_o']; 
      }else{
        $enc_id= $value["id"];     
        $data[$key][] = '<span style="float:right" id="'.$value['id'].'">'.$start.'</span>';
        $data[$key][] = $value['product_code'];     
        $data[$key][] = $value['quantity'];     
        $data[$key][] = $value['net_wt_o'];
        $data[$key][] = $value['few_wt_o'];     
        $data[$key][] = $value['kundan_pure_o'];     
        $data[$key][] = $value['mina_wt_o'];     
        $data[$key][] = $value['wax_wt_o'];     
        $data[$key][] = $value['color_stone_o'];     
        $data[$key][] = $value['moti_o'];     
        $data[$key][] = $value['checker_wt_o'];     
        $data[$key][] = $value['gr_wt_o'];     
        $data[$key][] = $value['checker_pcs_o'];     
        $data[$key][] = $value['checker_rate_o'];     
        $data[$key][] = $value['checker_amt_o'];     
        $data[$key][] = $value['kundan_pc_o'];     
        $data[$key][] = $value['kundan_rate_o'];     
        $data[$key][] = $value['kundan_amt_o'];     
        $data[$key][] = $value['stone_wt_o'];     
        $data[$key][] = $value['color_stone_rate_o'];     
        $data[$key][] = $value['stone_amt_o'];     
        $data[$key][] = $value['other_wt_o'];     
        $data[$key][] = $value['total_wt_o'];   
      } 
        $data[$key][] = "<button class='btn btn-link view_link btn-sm' onclick='repiar_product_issue(".$value['id'].")' id='bid_".$value['id']."'>Issue Karigar</button>";  
        }
    } else {
        $msg='No data found';
        if($department_id=='11'){
          $row="25";
        }else{
          $row="24";
        }

        for ($i=0; $i <=$row ; $i++) { 
            $data[0][$i] = [$msg];
            $msg="";
        }
    }
    $json_data = array(
        "draw" => intval($_REQUEST['draw']),
        "recordsTotal" => intval($totalRecords),
        "recordsFiltered" => intval($totalRecords),
        "data" => $data
    );
    return $json_data;
  
  }    
       
  public function get_repair_details()
    {
        $data=array();
        $id=$this->input->post('id');
       // print_r($id);exit;
        $user_id=$this->session->userdata('user_id');
        $dep_id=$this->User_access_model->find($user_id); 
        $department_id=$dep_id['department_id'];
        $data['dep_id'] = $department_id; 
        $data['repair_details']=$this->Repair_product_model->get_repair_details($id);
        $response['html'] = $this->load->view('Manufacturing_module/repair_product/repair_details',$data, true);
        echo json_encode($response);
    }

  private function generate_data_table($department_id){
    $filter_status =@$_REQUEST['order'][0];
    $search=@$_REQUEST['search']['value'];
    $status = array();
    $result = $this->Repair_product_model->get_sent_repair_product($filter_status,$status,$_REQUEST,$search,$limit=true,$department_id);
    $totalRecords = $this->Repair_product_model->get_sent_repair_product($filter_status,$status,$_REQUEST,$search,$limit=false,$department_id);
    //print_r($result);die;
    if (!empty($result)) {
      $data = $this->get_datatable_rows($result,$department_id);
    }else{
      $data = $this->get_null_row($department_id);
    }
    return $this->return_json($totalRecords,$data);
  }
  private function get_datatable_rows($result,$department_id){

    $start=$_REQUEST['start'];
    foreach ($result as $key => $value) {
        $start++;    
        $enc_id= $value["id"];     
        $data[$key][] = '<input type="hidden" name="rp[repair_product_id]['.$value["id"].']"   class="form-control" value="'.$enc_id.'" id="repair_product_id_'.$enc_id.'"> 
        <input type="hidden" name="rp[ready_product_id]['.$value["id"].']"   class="form-control" value="'.$value['ready_product_id'].'" id="ready_product_id_'.$enc_id.'" > 
        <input type="hidden" name="rp[department_id]['.$value["id"].']" id="department_id_'.$enc_id.'"    class="form-control" value="'.$value['department_id'].'">
        <input type="hidden" name="rp[karigar_id]['.$value["id"].']" id="karigar_id_'.$enc_id.'"    class="form-control" value="'.$value['karigar_id'].'">
        <span style="float:right" id="'.$value['id'].'" class="'.$value['ready_product_id'].'">'.$start.'</span>';   
    if($department_id == '0' || $department_id == '1' ||  $department_id == '5' || 
          $department_id == '7'  ||  $department_id == '8' || $department_id =='11'){

        $data[$key][] ='<input type="hidden"   name="rp[product_code]['.$value["id"].']" id="product_code_'.$enc_id.'" class="form-control" value="'.$value['product_code'].'">'.$value['product_code'];
         }else{
          
        $data[$key][] =$value['chitti_no'];

         }


      if($value['department_id'] !='2' && $value['department_id']!='3' && $value['department_id'] !='6' && $value['department_id']!='10'){

             if($value['department_id']=='11'){  

              $data[$key][] ='<span style="float:right"><input type="hidden" name="rp[quantity_o]['.$value["id"].']"   id="quantity_o_'.$enc_id.'" class="form-control" value="'.$value['quantity'].'""> 
                  <input type="hidden" name="rp[quantity]['.$value["id"].']"   id="quantity_'.$enc_id.'" class="form-control" value="'.$value['quantity_o'].'">'.$value['quantity'].'
                  </span><p class="text-danger" id="quantity_'.$value["id"].'_error"></p>';  


              $data[$key][] ='<input type="hidden" name="rp[net_wt_o]['.$value["id"].']" id="net_wt_o_'.$enc_id.'" value="'.$value['net_wt_o'].'"><input type="text" name="rp[net_wt]['.$value["id"].']" id="net_wt_'.$enc_id.'" onkeyup="calculate_grs_wt('.$enc_id.',this)" class="form-control" >
              <p class="text-danger" id="net_wt_'.$value["id"].'_error"></p>';

              $data[$key][] ='<input type="hidden"   name="rp[chitti_no]['.$value["id"].']" id="chitti_no_'.$enc_id.'" class="form-control" value="'.$value['chitti_no'].'">
              <input type="hidden" name="rp[gr_wt_o]['.$value["id"].']" id="gr_wt_o_'.$enc_id.'" value="'.$value['gr_wt_o'].'" ><input type="text" name="rp[gr_wt]['.$value["id"].']" id="ttl_gross_wt_'.$enc_id.'"  class="form-control">
              <p class="text-danger" id="gr_wt_'.$value["id"].'_error"></p>';

               }else{
            
                  $data[$key][] ='<span style="float:right"><input type="hidden" name="rp[quantity_o]['.$value["id"].']"   id="quantity_o_'.$enc_id.'" class="form-control" value="'.$value['quantity'].'""> 
                  <input type="hidden" name="rp[quantity]['.$value["id"].']"   id="quantity_'.$enc_id.'" class="form-control" value="'.$value['quantity_o'].'">'.$value['quantity'].'
                  </span><p class="text-danger" id="quantity_'.$value["id"].'_error"></p>';              
        

              $data[$key][] ='<input type="hidden" name="rp[net_wt_o]['.$value["id"].']" id="net_wt_o_'.$enc_id.'" value="'.$value['net_wt_o'].'"><input type="text" name="rp[net_wt]['.$value["id"].']" id="net_wt_'.$enc_id.'" onkeyup="calculate_grs_wt('.$enc_id.',this)" class="form-control" >
              <p class="text-danger" id="net_wt_'.$value["id"].'_error"></p>';

              $data[$key][] ='<input type="hidden" name="rp[few_wt_o]['.$value["id"].']" id="few_wt_o_'.$enc_id.'" value="'.$value['few_wt_o'].'">
              <input type="text" name="rp[few_wt]['.$value["id"].']" id="few_wt_'.$enc_id.'" onkeyup="calculate_grs_wt('.$enc_id.',this)" class="form-control">
              <p class="text-danger" id="few_wt_'.$value["id"].'_error"></p>';

              $data[$key][] ='<input type="hidden" name="rp[kundan_pure_o]['.$value["id"].']" id="kundan_pure_o_'.$enc_id.'" value="'.$value['kundan_pure_o'].'">
                <input type="text" name="rp[kundan_pure]['.$value["id"].']" id="kundan_pure_'.$enc_id.'"  onkeyup="calculate_grs_wt('.$enc_id.',this)" class="form-control">
                <p class="text-danger" id="kundan_pure_'.$value["id"].'_error"></p>';

              $data[$key][] ='<input type="hidden" name="rp[mina_wt_o]['.$value["id"].']" id="mina_wt_o_'.$enc_id.'" value="'.$value['mina_wt_o'].'">
                <input type="text" name="rp[mina_wt]['.$value["id"].']" id="mina_wt_'.$enc_id.'" onkeyup="calculate_grs_wt('.$enc_id.',this)" class="form-control">
                <p class="text-danger" id="mina_wt_'.$value["id"].'_error"></p>';

              $data[$key][] ='<input type="hidden" name="rp[wax_wt_o]['.$value["id"].']" id="wax_wt_o_'.$enc_id.'" value="'.$value['wax_wt_o'].'">
              <input type="text" name="rp[wax_wt]['.$value["id"].']" id="wax_wt_'.$enc_id.'" onkeyup="calculate_grs_wt('.$enc_id.',this)" class="form-control">
              <p class="text-danger" id="wax_wt_'.$value["id"].'_error"></p>';


              $data[$key][] ='<input type="hidden" name="rp[color_stone_o]['.$value["id"].']" id="color_stone_o_'.$enc_id.'" value="'.$value['color_stone_o'].'">
              <input type="text" name="rp[color_stone]['.$value["id"].']" id="color_stone_'.$enc_id.'"  onkeyup="calculate_grs_wt('.$enc_id.',this)" class="form-control">
              <p class="text-danger" id="color_stone_'.$value["id"].'_error"></p>';

               $data[$key][] ='<input type="hidden" name="rp[moti_o]['.$value["id"].']" id="moti_o_'.$enc_id.'" value="'.$value['moti_o'].'">
               <input type="text" name="rp[moti]['.$value["id"].']" id="moti_'.$enc_id.'" onkeyup="calculate_grs_wt('.$enc_id.',this)" class="form-control">
               <p class="text-danger" id="moti_'.$value["id"].'_error"></p>';


              $data[$key][] ='<input type="hidden" name="rp[checker_wt_o]['.$value["id"].']" id="checker_wt_o_'.$enc_id.'" value="'.$value['checker_wt_o'].'">
              <input type="text" name="rp[checker_wt]['.$value["id"].']" id="checker_wt_'.$enc_id.'" class="form-control" >
              <p class="text-danger" id="checker_wt_'.$value["id"].'_error"></p>';

              if($department_id == '8'){
                   $data[$key][] ='<input type="hidden" name="rp[black_beads_wt_o]['.$value["id"].']" id="black_beads_wt_o_'.$enc_id.'" value="'.$value['black_beads_wt_o'].'">
              <input type="text" name="rp[black_beads_wt]['.$value["id"].']" id="black_beads_wt_'.$enc_id.'" class="form-control" >
              <p class="text-danger" id="black_beads_wt_'.$value["id"].'_error"></p>';  
              }

              $data[$key][] ='<input type="hidden"   name="rp[chitti_no]['.$value["id"].']" id="chitti_no_'.$enc_id.'" class="form-control" value="'.$value['chitti_no'].'">
              <input type="hidden" name="rp[gr_wt_o]['.$value["id"].']" id="gr_wt_o_'.$enc_id.'" value="'.$value['gr_wt_o'].'" >
              <input type="text" name="rp[gr_wt]['.$value["id"].']" id="ttl_gross_wt_'.$enc_id.'"  class="form-control" readonly>
              <p class="text-danger" id="gr_wt_'.$value["id"].'_error"></p>';

              $data[$key][] ='<input type="hidden" name="rp[checker_o]['.$value["id"].']" id="checker_o_'.$enc_id.'" value="'.$value['checker_o'].'">
              <input type="text" name="rp[checker]['.$value["id"].']" id="checker_'.$enc_id.'" class="form-control" onkeyup="calculate_checker_amt(' .$enc_id. ',this)" >
              <p class="text-danger" id="checker_'.$value["id"].'_error"></p>';

               $data[$key][] ='<input type="hidden" name="rp[checker_pcs_o]['.$value["id"].']" id="checker_pcs_o_'.$enc_id.'" value="'.$value['checker_pcs_o'].'">
               <input type="text" name="rp[checker_pcs]['.$value["id"].']" id="checker_pcs_'.$enc_id.'" 
                onkeyup="calculate_checker_amt(' .$enc_id. ',this)"  class="form-control" >
               <p class="text-danger" id="checker_pcs_'.$value["id"].'_error"></p>';

              $data[$key][] ='<input type="hidden" name="rp[checker_amt_o]['.$value["id"].']" id="checker_amt_o_'.$enc_id.'" value="'.$value['checker_amt_o'].'">
              <input type="text" name="rp[checker_amt]['.$value["id"].']" id="checker_amt_'.$enc_id.'" class="form-control" >
              <p class="text-danger" id="checker_amt_'.$value["id"].'_error"></p>';

              $data[$key][] ='<input type="hidden" name="rp[kundan_pc_o]['.$value["id"].']" id="kundan_pc_o_'.$enc_id.'" value="'.$value['kundan_pc_o'].'">
              <input type="text" name="rp[kundan_pc]['.$value["id"].']" id="kundan_pc_'.$enc_id.'"  onkeyup="calculate_kun_amt('.$enc_id. ',this)" class="form-control" >
              <p class="text-danger" id="kundan_pc_'.$value["id"].'_error"></p>';

               $data[$key][] ='<input type="hidden" name="rp[kundan_rate_o]['.$value["id"].']" id="kundan_rate_o_'.$enc_id.'" value="'.$value['kundan_rate_o'].'">
               <input type="text" name="rp[kundan_rate]['.$value["id"].']" id="kundan_rate_'.$enc_id.'" 
               onkeyup="calculate_kun_amt('.$enc_id. ',this)" class="form-control" >
               <p class="text-danger" id="kundan_rate_'.$value["id"].'_error"></p>';

              $data[$key][] ='<input type="hidden" name="rp[kundan_amt_o]['.$value["id"].']" id="kundan_amt_o_'.$enc_id.'" value="'.$value['kundan_amt_o'].'">
              <input type="text" name="rp[kundan_amt]['.$value["id"].']" id="kundan_amt_'.$enc_id.'" 
              onkeyup="calculate_total_amt('.$enc_id.',this)" class="form-control" >
              <p class="text-danger" id="kundan_amt_'.$value["id"].'_error"></p>';

              $data[$key][] ='<input type="hidden" name="rp[stone_wt_o]['.$value["id"].']" id="stone_wt_o_'.$enc_id.'" value="'.$value['stone_wt_o'].'">
              <input type="text" name="rp[stone_wt]['.$value["id"].']" id="stone_wt_'.$enc_id.'" class="form-control"  readonly>
              <p class="text-danger" id="stone_wt_'.$value["id"].'_error"></p>';

               $data[$key][] ='<input type="hidden" name="rp[color_stone_rate_o]['.$value["id"].']" id="color_stone_rate_o_'.$enc_id.'" value="'.$value['color_stone_rate_o'].'">
               <input type="text" name="rp[color_stone_rate]['.$value["id"].']" id="color_stone_rate_'.$enc_id.'" 
               onkeyup="calculate_grs_wt('.$enc_id.',this)" class="form-control" >
               <p class="text-danger" id="color_stone_rate_'.$value["id"].'_error"></p>';

              $data[$key][] ='<input type="hidden" name="rp[stone_amt_o]['.$value["id"].']" id="stone_amt_o_'.$enc_id.'" value="'.$value['stone_amt_o'].'" >
              <input type="text" name="rp[stone_amt]['.$value["id"].']" id="stone_amt_'.$enc_id.'"  
              onkeyup="calculate_total_amt('.$enc_id.',this)" class="form-control"  readonly>
              <p class="text-danger" id="stone_amt_'.$value["id"].'_error"></p>';

                $data[$key][] ='<input type="hidden" name="rp[other_wt_o]['.$value["id"].']" id="other_wt_o_'.$enc_id.'" value="'.$value['other_wt_o'].'">
              <input type="text" name="rp[other_wt]['.$value["id"].']" id="other_wt_'.$enc_id.'"  onkeyup="calculate_total_amt('.$enc_id.',this)"  class="form-control" >
              <p class="text-danger" id="other_wt_'.$value["id"].'_error"></p>';
                
              $data[$key][] ='<input type="hidden" name="rp[total_wt_o]['.$value["id"].']" id="total_wt_o_'.$enc_id.'" value="'.$value['total_wt_o'].'">
              <input type="text" name="rp[total_wt]['.$value["id"].']" id="ttl_wt_'.$enc_id.'"  class="form-control"  readonly>
              <p class="text-danger" id="total_wt_'.$value["id"].'_error"></p>';
               }
          }else{
                $data[$key][] ='<input type="hidden"   name="rp[chitti_no]['.$value["id"].']" id="chitti_no_'.$enc_id.'" class="form-control" value="'.$value['chitti_no'].'">
              <input type="hidden" name="rp[gr_wt_o]['.$value["id"].']" id="gr_wt_o_'.$enc_id.'" value="'.$value['gr_wt_o'].'" >
              <input type="text" name="rp[gr_wt]['.$value["id"].']" id="ttl_gross_wt_'.$enc_id.'"  class="form-control">
              <p class="text-danger" id="gr_wt_'.$value["id"].'_error"></p>';

                $data[$key][] ='<input type="hidden" name="rp[net_wt_o]['.$value["id"].']" id="net_wt_o_'.$enc_id.'" value="'.$value['net_wt_o'].'"><input type="text" name="rp[net_wt]['.$value["id"].']" id="net_wt_'.$enc_id.'"  class="form-control" >
                  <p class="text-danger" id="net_wt_'.$value["id"].'_error"></p>';


          }

        $data[$key][] = '<a Onclick="repair_to_ready_product('.$value['id'].')"><button type="button" name="commit" class="btn btn-link view_link small loader-hide  btn-sm " id="b_'.$value['id'].'">Ready</button></a>';
            

    }
    return $data;
  }
  private function get_null_row($department_id){    
        $msg='No data found';
             if($department_id=='11'){
          $row="25";
        }else{
          $row="24";
        }

        for ($i=0; $i <=$row ; $i++) { 
            $data[0][$i] = [$msg];
            $msg="";
        
        }
    return $data;
  }
  private function return_json($totalRecords,$data){
    $json_data = array(
          "draw" => intval($_REQUEST['draw']),
          "recordsTotal" => intval($totalRecords),
          "recordsFiltered" => intval($totalRecords),
          "data" => $data
      );
    return $json_data;
  }

  public function store($id){   
  //print_r($_POST);die;      
        $postdata=$_POST;
         $validate_postdata = $this->Repair_product_model->validate_postdata();    
            //print_r($error['error']);die;
            if($validate_postdata['status'] == 'failure'){
                echo json_encode($validate_postdata);die;                              
            }else{
                 // $_POST['status'] = '2';
                  $_POST['ready_product_date']=date('Y-m-d');
                  $_POST['created_at'] = date('Y-m-d H:i:s');
                  $_POST['updated_at'] = date('Y-m-d H:i:s');
                  unset($_POST['id']);
                  $data= $_POST;
                  $result = $this->Repair_product_model->update($data,$id);  
                 // $this->Repair_product_model->update_repair($id,'2');  
                  $rp_id[]=$_POST['ready_product_id'];
                  $_SESSION['rp_id']=$rp_id; 
                  echo json_encode($result);die;  

            }

    
    
  }


  public function store_all(){
 //print_r($_POST);die;
     unset($_POST['Chitti_No']);
      if(empty($_POST)){
     $error['error']='Please enter weight and net weight'; 
     $error['status'] = 'failure';
     unset($_POST['rp']['gr_wt']);
     unset($_POST['rp']['net_wt']);
     echo json_encode($error);die;    
    }else if(!array_filter($_POST['rp']['gr_wt'])){
     $error['status'] = 'failure1';
     echo json_encode($error);die;    
    }else if(empty(array_filter($_POST['rp']['net_wt']))){
     $error['status'] = 'failure1';
     echo json_encode($error);die;    
    }else{
             foreach ($_POST['rp']['ready_product_id'] as $key => $value) {
              //print_r($_POST['rp']['department_id'][$key]);
                    $gr_wt=$_POST['rp']['gr_wt'];
                        $net_wt=$_POST['rp']['net_wt'];
               if( !empty($gr_wt[$key]) && !empty($net_wt[$key])){
                      $_POST['id']=$value;
                      $_POST['ready_product_id']=$value;
                      $_POST['department_id']=$_POST['rp']['department_id'][$key]; 
                      $id=$_POST['rp']['repair_product_id'][$key]; 
                       /*new value*/
                      $_POST['gr_wt']=$_POST['rp']['gr_wt'][$key];
                      $_POST['net_wt']=$_POST['rp']['net_wt'][$key];
                      /*old value*/
                      $_POST['gr_wt_o']=$_POST['rp']['gr_wt_o'][$key];
                      $_POST['net_wt_o']=$_POST['rp']['net_wt_o'][$key];
                      /*end*/
                      $_POST['chitti_no']=$_POST['rp']['chitti_no'][$key];
                      $_POST['karigar_id']=$_POST['rp']['karigar_id'][$key];
                      if($_POST['department_id'] =='8'){
                              $_POST['black_beads_wt']=$_POST['rp']['black_beads_wt'][$key];
                      } 
                      if($_POST['department_id'] !='2' && $_POST['department_id']!='3' && $_POST['department_id'] !='6' && $_POST['department_id']!='10'){
                            if($_POST['department_id'] =='11'){
                               $_POST['quantity']=$_POST['rp']['quantity'][$key];
                            }else{
                              /*new value*/
                              $_POST['quantity']=$_POST['rp']['quantity'][$key];
                              $_POST['few_wt']=$_POST['rp']['few_wt'][$key];
                              $_POST['kundan_pure']=$_POST['rp']['kundan_pure'][$key];
                              $_POST['mina_wt']=$_POST['rp']['mina_wt'][$key];
                              $_POST['wax_wt']=$_POST['rp']['wax_wt'][$key];
                              $_POST['color_stone']=$_POST['rp']['color_stone'][$key];
                              $_POST['moti']=$_POST['rp']['moti'][$key];
                              $_POST['checker_wt']=$_POST['rp']['checker_wt'][$key];
                              $_POST['checker']=$_POST['rp']['checker'][$key];
                              $_POST['checker_pcs']=$_POST['rp']['checker_pcs'][$key];
                              $_POST['checker_amt']=$_POST['rp']['checker_amt'][$key];
                              $_POST['checker_wt']=$_POST['rp']['checker_wt'][$key];
                              $_POST['kundan_pc']=$_POST['rp']['kundan_pc'][$key];
                              $_POST['kundan_rate']=$_POST['rp']['kundan_rate'][$key];
                              $_POST['kundan_amt']=$_POST['rp']['kundan_amt'][$key];                              
                              $_POST['stone_wt']=$_POST['rp']['stone_wt'][$key];  
                              $_POST['color_stone_rate']=$_POST['rp']['color_stone_rate'][$key];  
                              $_POST['stone_amt']=$_POST['rp']['stone_amt'][$key];  
                              $_POST['other_wt']=$_POST['rp']['other_wt'][$key];  
                              $_POST['total_wt']=$_POST['rp']['total_wt'][$key];  
                              /*end*/
                              /*old value*/
                              $_POST['few_wt_o']=$_POST['rp']['few_wt_o'][$key];
                              $_POST['kundan_pure_o']=$_POST['rp']['kundan_pure_o'][$key];
                              $_POST['mina_wt_o']=$_POST['rp']['mina_wt_o'][$key];
                              $_POST['wax_wt_o']=$_POST['rp']['wax_wt_o'][$key];
                              $_POST['color_stone_o']=$_POST['rp']['color_stone_o'][$key];
                              $_POST['moti_o']=$_POST['rp']['moti_o'][$key];
                              $_POST['checker_wt_o']=$_POST['rp']['checker_wt_o'][$key];
                              $_POST['checker_o']=$_POST['rp']['checker_o'][$key];
                              $_POST['checker_pcs_o']=$_POST['rp']['checker_pcs_o'][$key];
                              $_POST['checker_amt_o']=$_POST['rp']['checker_amt_o'][$key];
                              $_POST['checker_wt_o']=$_POST['rp']['checker_wt_o'][$key];   
                              $_POST['kundan_pc_o']=$_POST['rp']['kundan_pc_o'][$key];
                              $_POST['kundan_rate_o']=$_POST['rp']['kundan_rate_o'][$key];
                              $_POST['kundan_amt_o']=$_POST['rp']['kundan_amt_o'][$key];                              
                              $_POST['stone_wt_o']=$_POST['rp']['stone_wt_o'][$key]; 
                              $_POST['color_stone_rate_o']=$_POST['rp']['color_stone_rate_o'][$key];  
                              $_POST['stone_amt_o']=$_POST['rp']['stone_amt_o'][$key];  
                              $_POST['other_wt_o']=$_POST['rp']['other_wt_o'][$key];  
                              $_POST['total_wt_o']=$_POST['rp']['total_wt_o'][$key];  
                              /*end*/
                            }
                         }         
                  
         
                    $validate_postdata = $this->Repair_product_model->validate_postdata();    
                    //print_r($error['error']);die;
                    if($validate_postdata['status'] == 'failure'){
                        echo json_encode($validate_postdata);die;                              
                    }else{
                         // $_POST['status'] = '1';
                          $_POST['ready_product_date']=date('Y-m-d');
                          $_POST['created_at'] = date('Y-m-d H:i:s');
                          $_POST['updated_at'] = date('Y-m-d H:i:s');
                          $data= $_POST;
                        //print_r($data);
                        //$result = $this->Repair_product_model->store($data); 
                          $result = $this->Repair_product_model->update($data,$id); 
                        $rp_id[]=$_POST['ready_product_id'];
                        $_SESSION['rp_id']=$rp_id;
                        //print_r($result);
                  
                    }

                    
                        
                }   
            }//print_r( $_SESSION);
            echo json_encode($result);die;   
          }
    
  }

  

  public function difference_product(){
    $user_id=$this->session->userdata('user_id');
    $department_id=$this->session->userdata('department_id');
    
    $dep_id=$this->User_access_model->find($user_id); 
    $department_name=$this->Department_model->find($department_id);    
    $data['department_id'] = $department_id;  
    $data['page_title'] = 'All Difference Products';
    $data['display_title'] = $department_name['name'].' - Difference In Product';
    $this->breadcrumbs->push("Dashboard", "Manufacturing_Dashboard");
    $list=$this->input->post('list');
    if($list !=""){
      echo json_encode($this->generate_data_table_detaile($department_id));
    }else{
       
      $this->view->render('Manufacturing_module/repair_product/difference_product',$data);
    }
  }
  private function generate_data_table_detaile($department_id){
      $filter_status =@$_REQUEST['order'][0];
      $search=@$_REQUEST['search']['value'];
      $status = array();
      $result_dif = $this->Repair_product_model->get_differnt($filter_status,$status,$_REQUEST,$search,$limit=true,$department_id);
      $totalRecords_dif = $this->Repair_product_model->get_differnt($filter_status,$status,$_REQUEST,$search,$limit=false,$department_id);
      //print_r($result_dif);die;
      //echo $this->db->last_query();print_r($result_dif);exit;
      if (!empty($result_dif)) {
        $data_dif = $this->get_datatable_rows_diff($result_dif,$department_id);
      }else{
        $data_dif = $this->get_null_row_dif($department_id);
      }
      return $this->return_json_dif($totalRecords_dif,$data_dif);
    

  }
    private function get_datatable_rows_diff($result_dif,$department_id){
    //print_r($result_dif);die;
      // round($value["gr_wt"],2)
    foreach ($result_dif as $key => $value) {
        if($department_id == '0' || $department_id == '1' ||   $department_id == '5' ||  
          $department_id == '7'  ||   $department_id == '8' ||  $department_id =='11'){
          $data[$key][] = $value['karigar_name'];
          $data[$key][] = round($value['gr_wt_o'],2);
          $data[$key][] = round($value['net_wt_o'],2);
          $data[$key][] = round($value['few_wt_o'],2);
          $data[$key][] = round($value['kundan_pure_o'],2);
          $data[$key][] = round($value['moti_o'],2);
          $data[$key][] = round($value['wax_wt_o'],2);
          // if($department_id == '8'){
          //   $data[$key][] = round($value["black_beads_wt_o"],2);
          // }
          $data[$key][] = round($value["gr_wt"],2);
          $data[$key][] = round($value['net_wt'],2);
          $data[$key][] = round($value['few_wt'],2);
          $data[$key][] = round($value['kundan_pure'],2);
          $data[$key][] = round($value['moti'],2);
          $data[$key][] = round($value['wax_wt'],2);
          // if($department_id == '8'){
          //   $data[$key][] = round($value["black_beads_wt"],2);
          // }
          $data[$key][] = round($value['gr_wt_o'] - $value['gr_wt'],2);
          $data[$key][] = round($value['net_wt_o'] - $value['net_wt'],2);
          $data[$key][] = round($value['few_wt_o']-$value['few_wt'],2);
          $data[$key][] = round($value['kundan_pure']-$value['kundan_pure_o'],2);
          $data[$key][] = round($value['moti_o']-$value['moti'],2);
          $data[$key][] = round($value['wax_wt_o']-$value['wax_wt'],2);
          //     if($department_id == '8'){
          //   $data[$key][] = round($value['black_beads_wt_o']-$value['black_beads_wt'],2);
          // }
        }else{
          $data[$key][] = $value['karigar_name'];
          $data[$key][] = round($value['gr_wt_o'],2);
          $data[$key][] = round($value['net_wt_o'],2);
          $data[$key][] = round($value["gr_wt"],2);
          $data[$key][] = round($value['net_wt'],2);
          $data[$key][] = round($value['gr_wt_o'] - $value['gr_wt'],2);
          $data[$key][] = round($value['net_wt_o'] - $value['net_wt'],2);

        }

    }
  return $data;
  }

  private function get_null_row_dif($department_id){    
        $msg='No data found';
        if($department_id == '0' || $department_id == '1' ||  $department_id == '5' || 
          $department_id == '7'  ||  $department_id =='11'){
                $t_count=19;}else if($department_id == '8'){$t_count=21;}else{$t_count=7;}
        for ($i=0; $i <=$t_count; $i++) { 
            $data[0][$i] = [$msg];
            $msg="";
        }
    return $data;
  }
  private function return_json_dif($totalRecords_dif,$data_dif){
    $json_data = array(
          "draw" => intval($_REQUEST['draw']),
          "recordsTotal" => intval($totalRecords_dif),
          "recordsFiltered" => intval($totalRecords_dif),
          "data" => $data_dif
      );
    return $json_data;
  }

  public function issue_to_karigar(){
       $repair_id = $this->input->post('id');
      //$result=$this->Repair_product_model->get_repair_product_by_id($repair_id);
      $result=$this->Repair_product_model->update_repair($repair_id,'1');
      if($result == true){
         $response_data['status'] = "success";
         $response_data['msg']    = "successfully issued to karigar";
      }
      echo json_encode($response_data);
       
    }

  public function repair_stock() {
      $department_id=$this->session->userdata('department_id');    
      $data['category'] = $this->Category_model->get('','','','',true,$department_id);
      $data['parent_category'] = $this->Sales_stock_model->get_product_category();
      $data['karigar']= $this->Karigar_model->get('','','','',true);
      if($department_id !='0'){
          $dep_name[]=$this->Department_model->find($department_id);   
          $data['department'] = $dep_name;
      }else{            
            $data['department'] = $this->Department_model->get('','','','',true);          
      }
      $data['weight_range'] = $this->Weight_range_model->get('','','','',true);
      $data['display_title'] = "Add Repair Stock";
      $data['department_id'] =   $department_id;
      $data['page_title']="Create Repair Stock";
      $this->view->render('Manufacturing_module/repair_product/repair_stock', $data);
    } 
   
  public function get_repair_max_code() {
      $result = $this->Repair_product_model->get_repair_max_code($_POST);
      echo json_encode($result);
  }

}
?>