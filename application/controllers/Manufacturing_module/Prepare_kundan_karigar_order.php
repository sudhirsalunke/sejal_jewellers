<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Prepare_kundan_karigar_order extends CI_Controller {
  public function __construct() {
    parent::__construct();
    if(empty($this->session->userdata('user_id'))){
      redirect(ADMIN_PATH . 'auth/logout');
    }
     $this->load->model(array('Manufacturing_module/Prepare_kundan_karigar_order_model'));
     $this->load->library('m_pdf');
  }
  public function store($karigar_id){
  /*  print_r($karigar_id);
     print_r($_POST);
    die;*/
    $data=$this->input->post();
    if (!empty($data['tag_product'])) {      
      $this->db->insert('make_set',array('created_at' => date('Y-m-d H:i:s'),'updated_at' => date('Y-m-d H:i:s') ));
      $make_set_id =$this->db->insert_id();
      $quantity = $data['quantity'];
      $department_id= $data['department_id'];
      $product_code= $data['product_code'];
      $pcs =$data['pcs'];
      $all_msqr_ids=array();
      $insert_array = array();

      $i=0;
      foreach ($data['tag_product'] as $key => $value) {

        $msqr_array = array('make_set_id' => $make_set_id,
                            'quantity' =>$quantity[$value],
                            'department_id'=>$department_id[$value],
                            'product_code'=>$product_code[$value],
                            'tag_id' => $value,
                            'qty_remaining'=>0,
                            'status'=>'1',
                            'pcs' =>@$pcs[$value],
                            );
       /* echo"<pre> msqr_array ";
        print_r($msqr_array);
        echo "</pre>";*/
        $this->db->insert('make_set_quantity_relation',$msqr_array);

        $msqr_id =$this->db->insert_id();
        $all_msqr_ids[]=$msqr_id;
        $result = $this->Prepare_kundan_karigar_order_model->get_department_id($msqr_id);

        $insert_array['department_id'] =$department_id[$value];
        $insert_array['product_code'] =$product_code[$value];
        $insert_array['manufacturing_order_id'] = $result['order_id'];
        $insert_array['msqr_id']=$msqr_id;
        $insert_array['karigar_id']=$karigar_id;
        $insert_array['issue_voucher'] = strtotime(date('Y-m-d H:i:s'));;
        $insert_array['quantity']=$quantity[$value];
        $insert_array['created_at'] = date('Y-m-d H:i:s');
        $insert_array['updated_at'] = date('Y-m-d H:i:s');
        $insert_array['status'] = '1';
   /*   echo"<pre> insert_array ";
        print_r($insert_array);
        echo "</pre>";*/
       $result = $this->Prepare_kundan_karigar_order_model->store($insert_array);
      }//die;
      $this->db->set('status','2');
      $this->db->where_in('id',$data['tag_product']);
      $this->db->update('tag_products');

      $this->session->set_userdata('print_tag_product_session',$all_msqr_ids);

      echo json_encode(get_successMsg());
    }
    else{
      $data['status'] = 'failure';
      $data['error']['quantity'] = 'Please select product for Kundan Karigar';
      echo json_encode($data);
    }

  }
  private function validate_quantity($qnt,$msqr_id){
    $data['status'] = 'success';
    if(empty($qnt) || !is_numeric($qnt)){
        $data['status'] = 'failure';
        $data['error']['quantity'] = 'Quantity should not be empty and must be number';
    }elseif($this->Prepare_kundan_karigar_order_model->check_quantity($qnt,$msqr_id,true) == false){
        $data['status'] = 'failure';
        $data['error']['quantity'] = 'Quantity exceeded';
    }
    return $data;
  }
  public function download_pdf($k=""){
     $msqr_ids = @$this->session->userdata('print_tag_product_session');

    if (empty($msqr_ids)) {
     redirect(ADMIN_PATH.'tag_products/create');
    }else{
      $data['page_title'] = 'print_pdf_manuf';
      $data['result'] = $this->Prepare_kundan_karigar_order_model->get_pdf_details($msqr_ids);
         // echo $this->db->last_query();die;
      $data['chitti_no'] =@$data['result'][0]['make_set_id'];
      $data['karigar_name'] =@$data['result'][0]['karigar_name'];
     /* echo "<pre>";
      print_r($data);
      echo "</pre>";die;*/
      $this->load->view('Manufacturing_module/Prepare_kundan_karigar_order/print',$data);
      //$this->session->unset_userdata('print_tag_product_session');

    }
  }
}
