<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Manufacturing_hallmarking extends CI_Controller {
	public function __construct() {
	    parent::__construct();
	    if(empty($this->session->userdata('user_id'))){
	      redirect(ADMIN_PATH . 'auth/logout');
	    }
      $this->load->library('excel_lib');
      $this->breadcrumbs->push("Manufacturing Module", "Manufacturing_Dashboard");
      $this->breadcrumbs->push("Dashboard", "Manufacturing_Dashboard");
	    $this->load->config('admin_validationrules', TRUE);
	    $this->load->model(array('Manufacturing_module/Quality_control_model','Manufacturing_module/Hallmarking_model','Manufacturing_module/Prepare_karigar_model','Manufacturing_module/Tag_products_model','sale_master/Parent_category_model','User_access_model','Manufacturing_module/Department_model'));
	}
	public function index(){
    $department_id=$this->session->userdata('department_id');
    $department_name=$this->Department_model->find($department_id);
		$data['page_title'] = 'Sent Orders For Hallmarking';
    $data['display_name'] = "";
    if($_GET['status'] == 'send')
		  $data['display_name'] = $department_name['name'].' - Pending To Be Receive From HM';
    elseif($_GET['status'] == 'receive')
      $data['display_name'] = $department_name['name'].' - QC Accepted Hallmarking Orders';
    elseif($_GET['status'] == 'pending')
      $data['display_name'] = $department_name['name'].' - Pending Orders For Hallmarking';
    elseif($_GET['status'] == 'rejected')
      $data['display_name'] = $department_name['name'].' - Rejected Hallmarking Orders';
    $this->breadcrumbs->push($data['display_name'], "Hallmarking");
	    $list=$this->input->post('list');
	    if($list !="")
	    {
	      	echo json_encode($this->generate_data_table($department_id));
	    }else{
			$this->view->render('Manufacturing_hallmarking/index',$data);
	    }
	}
	private function generate_data_table($department_id){
    $filter_status =@$_REQUEST['order'][0];
    $status = array('p.created_at','p.product_code');
    $search=@$_REQUEST['search']['value'];
    //if($_GET['status'] == 'receive' || $_GET['status']=='rejected'){
      $result = $this->Hallmarking_model->get($filter_status,$status,$_REQUEST,$search,$limit=true,$department_id);
      $totalRecords = $this->Hallmarking_model->get($filter_status,$status,$_REQUEST,$search,$limit=false,$department_id);
    // }else{
    //   $result = $this->Quality_control_model->get($filter_status,$status,$_REQUEST,$search,$limit=true,$hallmarking=true);
    //   $totalRecords = sizeof($this->Quality_control_model->get($filter_status,$status,$_REQUEST,$search,$limit=false,$hallmarking=true));
    // }
    if (!empty($result)) {
      $data = $this->get_datatable_rows($result);
    }else{
      $data = $this->get_null_row();
    }
    return $this->return_json($totalRecords,$data);
  }
  private function get_datatable_rows($result){
    foreach ($result as $key => $value) {
      $rec_qnt = $this->Hallmarking_model->get_all_qc_quantity($value['id']);
      if($_GET['status']=='rejected'){
        $data[$key][] = '<a href="'.ADMIN_PATH.'Manufacturing_hallmarking/view_remark/'.$value['id'].'">'.$value['order_id'].'</a>';

      }else if($_GET['status']=='send'){
        //print_r($_POST);exit;
         $checked="";
         $class_all_shortlist="shortlist_all";
        if (isset($_POST['selected_hallmark']) && in_array($value['id'], explode(',', $_POST['selected_hallmark']))) {
          $checked="checked";
          $class_all_shortlist="";
        }
/*        $data[$key][]='<div class="checkbox checkbox-purpal"><input type="checkbox" class="from-control '.$class_all_shortlist.' checkbox_ctn" name="hm[id]['.$value["id"].']" onclick="add_selected_hallmark(this)" value="'.$value['id'].'" '.$checked.'><label></label></div>';*/
      
        $data[$key][] =$value['order_name'].'<input type="hidden" class="from-control checkbox_ctn" name="hm[id]['.$value["id"].']" value="'.$value['id'].'">';   
      }else{
        $data[$key][] =$value['order_name'];
      }
      $data[$key][] =$value["km_name"];
      $data[$key][] =$value["sub_cat_name"];
      $data[$key][] =$value["receipt_code"];
      $data[$key][] ='<input type="text" class="form-control" name="hm[quantity]['.$value["id"].']" value="'.($value["quantity"] - $value["remaining_qty"]).'"><input type="hidden" name="hm[original_quantity]['.$value["id"].']" value="'.($value["quantity"] - $value["remaining_qty"]).'"><p class="text-danger" id="quantity_'.$value["id"].'_error"></p>';
      $data[$key][] ='<span style="float:right">'.$value["from_weight"].'-'.$value['to_weight'].'</span>';
   /*   $data[$key][] ='<span style="float:right">'.(($value["from_weight"]+$value['to_weight'])/2 * $value["quantity"]).'</span>';*/
   $data[$key][] ='<span style="float:right">'.round($value["hm_gr_wt"],2).'</span>';
      $data[$key][] ='<span style="float:right">'.round($value["hm_net_wt"],2).'</span>';
      //$data[$key][] ='<input type="text" class="form-control" name="hm[weight]['.$value["id"].']" value="'.(($value["from_weight"]+$value['to_weight'])/2 * $value["quantity"]).'">';
      $data[$key][] ='<input type="text" class="form-control" name="hm[weight]['.$value["id"].']"><p class="text-danger" id="weight_'.$value["id"].'_error"></p>';
      $data[$key][] ='<span style="float:right">'.date('d-m-Y '.'||'.' H:i:s',strtotime($value["HM_send_date"])).'</span>';

      if($_GET['status']=='rejected')
        $button_html ='';
      else if($_GET['status'] == 'receive'){
          $button_html ='';
       // $button_html = '<a href="'.ADMIN_PATH.'Manufacturing_tag_products/create/'.$value["id"].'" ><button id="b_'.$value['id'].'" class="btn btn-primary small loader-hide  btn-sm" name="commit" type="button">Tag Products</button></a>';
      
      }
       else if ($_GET['status']=='send') {
        $button_html="";
      }
      else
        $button_html = '<a href="'.ADMIN_PATH.'Manufacturing_hallmarking/create/'.$value["id"].'" ><button id="b_'.$value['id'].'" class="btn btn-link edit_link small loader-hide  btn-sm" name="commit" type="button">HALLMARKING QC Check</button></a>';  
     
      $data[$key][] = $button_html;
   
    }
    return $data;
  }
  private function get_null_row(){
    $msg='No data found';
    for ($i=0; $i <=10; $i++) {       
       $data[0][$i] = [$msg];
       $msg="";
   
    }
    return $data;
  }
  private function return_json($totalRecords,$data){
    $json_data = array(
          "draw" => intval($_REQUEST['draw']),
          "recordsTotal" => intval($totalRecords),
          "recordsFiltered" => intval($totalRecords),
          "data" => $data
      );
    return $json_data;
  }
  public function export($search='')
  {
    $data = $this->Hallmarking_model->export_hallmarking_products('','','',$search,'','');
    //print_r($data); die();
    
    $header_array=array("Vendor_Code","Order_No","Link_No","PCS","Gross_Wt","HM_Send_date","HM_Receive_Date","Delivery_Date","Assay_No","XRF_No","Item_Id","Config_Id","Unit_Id","HM_Code");
    $file_name="Hallmarking.xlsx";
    $excel_data = create_excel_array($data,$header_array);
    $this->excel_lib->export($excel_data,$header_array,$file_name,'','',$save=true);
  }
  public function create($qc_id){

    if(empty($qc_id))
      die('Please provide Quality Control refference ID');
    $this->breadcrumbs->push("Quality Control Check", "Dashboard/Quality_control");
    $data['order_data'] = $this->Hallmarking_model->get_order_data($qc_id);
    $data['page_title'] = "Hallmarking Check";
    $data['receive_from_hallmarking'] = TRUE;
    $data['result'] = $this->Quality_control_model->find_by_qc_id($qc_id);
    $this->view->render('Manufacturing_hallmarking/create',$data);
  }
  public function store(){
 
    //print_r($_POST);die;
    if(empty($_POST)){
     unset($_POST['hm']['weight']);
     $error['status'] = 'failure';
     echo json_encode($error);die;    
    }else  if(empty(array_filter($_POST['hm']['weight']))){
     $error['status'] = 'failure1';
     echo json_encode($error);die;    
    }
    if(!empty($_POST['hm']['weight'])){
          // $qch_id = explode(',',$_POST['all_check_qc']);
       $all_wt= $_POST['hm']['weight'];
      
      foreach ($_POST['hm']['id'] as $key => $value) {
          $weight[$key]=$_POST['hm']['weight'];     
         
         //print_r($weight[$key]);

        if(!empty($all_wt[$key])){
         /* $this->form_validation->set_rules($this->config->item('pending_to_be_rec_hm', 'admin_validationrules'));*/        
            $_POST['check_data']['quantity'] =$_POST['hm']['quantity'][$value];
            $_POST['check_data']['weight'] =$_POST['hm']['weight'][$value];
           //print_r($_POST['check_data']);
            $this->form_validation->set_rules('check_data[weight]', 'Gross Wt', 'trim|required|greater_than_equal_to[0.1]');
            $this->form_validation->set_rules('check_data[quantity]', 'Quantity', 'trim|required|numeric|is_natural_no_zero');  

               if ($this->form_validation->run() == FALSE) {        
                      $error['status'] = 'failure';
                      $error['error']['weight_'.$value] = strip_tags(form_error('check_data[weight]'));
                      $error['error']['quantity_'.$value] = strip_tags(form_error('check_data[quantity]'));
                      echo json_encode($error);die;
                }

              // if(empty($_POST['hm']['weight'][$key])){
              //   $error['status'] = 'failure';
              //   $error['error']['weight_'.$value] = 'This field is required';
              //   echo json_encode($error);die;
              // }
              $data = $this->Hallmarking_model->find($value);
              $data['manufacturing_qch_id'] = $data['id'];
              $data['status'] = '8';
              $data['quantity'] = $_POST['hm']['quantity'][$value];
              $data['original_quantity'] = $_POST['hm']['original_quantity'][$value];
              $data['weight'] = $_POST['hm']['weight'][$value];
              unset($data['id']);
              if($data['original_quantity'] <  $data['quantity']){
                $error['status'] = 'failure';
                $error['error']['quantity_'.$value] = 'Quantity Exceeded';
                echo json_encode($error);die;
              }else{
               
                $result=$this->Hallmarking_model->store($data);
                //echo json_encode($result);die;
              }
        }

      }echo json_encode($result);die;
       

    //echo json_encode(get_successMsg());
    }
  }
  public function reject(){
    $data = $this->Quality_control_model->validatepostdata();
    if($data['status'] == 'success'){
      $postdata = $_POST;
      $postdata['rejected'] = true;
      $data = $this->Hallmarking_model->store($postdata);

    }
    echo json_encode($data);
  }
  public function Send_rejected_products($hm_id){
    $result = $this->Hallmarking_model->find($hm_id);
    $karigar_id = $this->Quality_control_model->get_karigar_id($result['corporate_product_id']);
    $status = $this->Hallmarking_model->insert_in_prepared_order($result,$karigar_id);
    if($status == true){
      $array = array('status'=>'5');
      $where = array('id'=>$hm_id);
      $this->Hallmarking_model->update($array,$where);
    }
    $data['status'] = 'success';
    $data['karigar_id'] = $karigar_id;
    echo json_encode($data);die;
  }
  public function view_remark($id)
  {
    $data['page_title'] = 'HALLMARKING QC CHECKED PRODUCTS';
    $this->breadcrumbs->push("Hallmarking Rejected Products", "Manufacturing_hallmarking?status=rejected");
    $this->breadcrumbs->push("View Remark", "Manufacturing_hallmarking");
    $data['display_name'] = 'View Remark';
    $data['result'] = $this->Hallmarking_model->find($id);
    //print_r($data); die();
    $this->view->render('Hallmarking/view_remark',$data);
  }
}	