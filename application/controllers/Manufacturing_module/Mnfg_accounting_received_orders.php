<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class Mnfg_accounting_received_orders extends CI_Controller {
  public function __construct() {
    parent::__construct();
    if(empty($this->session->userdata('user_id'))){
      redirect(ADMIN_PATH . 'auth/logout');
    }
    $this->load->library('excel_lib');
    $this->breadcrumbs->push("Manufacturing Module", "Manufacturing_Dashboard");
    $this->breadcrumbs->push("Dashboard", "Manufacturing_Dashboard");
    $this->load->model(array('Manufacturing_module/Received_orders_model','Manufacturing_module/Karigar_order_model','Manufacturing_module/Mnfg_accounting_received_model'));
    $this->load->config('admin_validationrules', TRUE);
  }
  public function index(){    
  $data['page_title'] = 'Manufacturing Accounting Received Orders';
  $this->breadcrumbs->push("All Received Orders", "Mnfg_accounting_received_orders");
  $data['display_name'] = "Manufacturing Accounting Received Orders";
    $list=$this->input->post('list');
    if($list !="")
    {
      echo json_encode($this->generate_data_table());
    }else{
      $this->view->render('Manufacturing_module/Manufacturing_accounting/order_index',$data);
    }
  }
  public function view($order_id){
    $data['display_name'] = "Orders pending to be received from karigar";
    $this->breadcrumbs->push("All Received Orders", "Mnfg_accounting_received_orders");
    $this->breadcrumbs->push("View", "Manufacturing_module/Manufacturing_accounting/View");
    $data['page_title'] = 'Manufacturing Accounting Received Orders';
    $data['order_id'] = $order_id;
   
    $list=$this->input->post('list');
    if($list !="")
    {

    $filter_status =@$_REQUEST['order'][0];
    $status = array();
    $search=@$_REQUEST['search']['value'];
    $result = $this->Mnfg_accounting_received_model->get($filter_status,$_REQUEST,$search,$limit=true,$status='1',$order_id);
    $totalRecords = sizeof($this->Mnfg_accounting_received_model->get($filter_status,$_REQUEST,$search,$limit=false,$status='1',$order_id));
    if (!empty($result)) {
      $data = $this->get_datatable_rows($result);
    }else{
      $data = $this->get_null_row();
    }
      echo json_encode($this->return_json($totalRecords,$data));
    }else{
      $this->view->render('Manufacturing_module/Manufacturing_accounting/index',$data);
    }
  }
  public function store(){
    echo '<pre>';
    print_r($_POST);
    echo '</pre>';die;
  }
  private function generate_data_table(){
    $filter_status =@$_REQUEST['order'][0];
    $status = array();
    $search=@$_REQUEST['search']['value'];
    $result = $this->Mnfg_accounting_received_model->get_order_wise_list($filter_status,$status,$_REQUEST,$search,$limit=true,$status='1');
    $totalRecords = sizeof($this->Mnfg_accounting_received_model->get_order_wise_list($filter_status,$status,$_REQUEST,$search,$limit=false,$status='1'));
    if (!empty($result)) {
      $data = $this->get_order_wise_datatable_rows($result);
    }else{
      $data[0][] = ['No data found'];
      $data[0][] = [];
      $data[0][] = [];
      $data[0][] = [];
      $data[0][] = [];
      $data[0][] = [];
    }
    return $this->return_json($totalRecords,$data);
  }

  private function get_order_wise_datatable_rows($result){    
    foreach ($result as $key => $value) {
      $sr_no=$key+1;
      $data[$key][] = '<span style="float:right">'.$sr_no.'</span>';
      $data[$key][] ='<span style="float:right">'.$value['receipt_code'].'</span>';
      $data[$key][] ='<span style="float:right">'.$value['order_id'].'</span>';
      $data[$key][] =$value['name'];
      $data[$key][] ='<span style="float:right">'.$date('d-m-Y',strtotime($value["order_date"])).'</span>';
      $data[$key][] ='<a href="'.ADMIN_PATH.'Mnfg_accounting_received_orders/view/'.$value['order_id'].'"><button class="btn btn-link view_link small loader-hide  btn-sm" name="commit" type="button">VIEW</button></a>';
      }
    return $data;
  }
  private function get_datatable_rows($result){
    foreach ($result as $key => $value) {
      $rp_quantity = $this->Mnfg_accounting_received_model->get_rec_qnt_by_kmm_id($value['order_id']);
      //print_r($value['id']);
      $data[$key][] = $key+1;
      $data[$key][] ='<span style="float:right">'. $value['receipt_code'].'</span>';
      $data[$key][] =$value['order_id'];
      $data[$key][] =date('d-m-Y',strtotime($value["order_date"]));
      $data[$key][] =$value['karigar_name'];
      $data[$key][] =$value["sub_cat_name"];
      $data[$key][] =$value["from_weight"]."-".$value["to_weight"];
      $data[$key][] ='<span style="float:right">'. $value["quantity"].'</span>';
      $data[$key][] =$value["quantity"] - $rp_quantity['qnt'];
      }
    return $data;
  }
  private function get_null_row(){
    $data[0][] = ['No data found'];
    $data[0][] = [];
    $data[0][] = [];
    $data[0][] = [];
    $data[0][] = [];
    $data[0][] = [];
    $data[0][] = [];
    $data[0][] = [];
    $data[0][] = [];
    return $data;
  }
  private function return_json($totalRecords,$data){
    $json_data = array(
          "draw" => intval($_REQUEST['draw']),
          "recordsTotal" => intval($totalRecords),
          "recordsFiltered" => intval($totalRecords),
          "data" => $data
      );
    return $json_data;
  }
  
}