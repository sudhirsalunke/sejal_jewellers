<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Manufacturing_tag_products extends CI_Controller {
	public function __construct(){
		parent::__construct();
	    if(empty($this->session->userdata('user_id'))){
	      redirect(ADMIN_PATH . 'auth/logout');
	    }
	    $this->breadcrumbs->push("Manufacturing Module", "Manufacturing_Dashboard");
    	$this->breadcrumbs->push("Dashboard", "Manufacturing_Dashboard");
    	$this->load->model(array('Manufacturing_module/Hallmarking_model'));
    	$this->load->library('m_pdf');
    	// $this->load->config('admin_validationrules', TRUE);
	}

	public function create($id){
		$data['page_title'] = "Tag Products";
		$data['display_title'] = "Tag Products";
		$data['result'] = $this->Hallmarking_model->get('','','','','',$id);
		//echo '<pre>'; print_r($data); echo '</pre>'; exit();
		$this->view->render('Manufacturing_module/tag_products/create',$data);
	}

	public function print_tag_products($id){
		$data['display_title'] = 'Tag Products';
		$data['result'] = $this->Hallmarking_model->get('','','','','',$id);
		$html=$this->load->view('Manufacturing_module/tag_products/print', $data,true);
		$pdfFilePath = "Tag_Products_".$id.".pdf";
	    $this->m_pdf->pdf->WriteHTML($html);
	    $this->m_pdf->pdf->Output($pdfFilePath, "D");	    
	}
}
?>