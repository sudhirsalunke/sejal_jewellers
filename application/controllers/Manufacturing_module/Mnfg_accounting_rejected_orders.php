<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Mnfg_accounting_rejected_orders extends CI_Controller {
  public function __construct() {
    parent::__construct();
    if(empty($this->session->userdata('user_id'))){
      redirect(ADMIN_PATH . 'auth/logout');
    }
    $this->load->library('excel_lib');
    $this->breadcrumbs->push("Manufacturing Module", "Manufacturing_Dashboard");
    $this->breadcrumbs->push("Dashboard", "Manufacturing_Dashboard");
    $this->load->model(array('Manufacturing_module/Received_orders_model','Manufacturing_module/Karigar_order_model','Manufacturing_module/Mnfg_accounting_received_model','Manufacturing_module/Mnfg_accounting_rejected_model','Karigar_model'));
    $this->load->config('admin_validationrules', TRUE);
    $this->load->library('m_pdf');
  }
  public function index(){    
  $data['page_title'] = 'Manufacturing Accounting Rejected Orders';
  $this->breadcrumbs->push("All Rejected Orders", "Mnfg_accounting_rejected_orders");
  $data['display_name'] = "Manufacturing  Rejected Products";
    $list=$this->input->post('list');
    if($list !="")
    {
      echo json_encode($this->generate_data_table());
    }else{
      $this->view->render('Manufacturing_module/Rejected_orders/index',$data);
    }
  }

  public function view($order_id){
    $data['display_name'] = "Manufacturing  Rejected Products";
    $this->breadcrumbs->push("All Rejected Orders", "Mnfg_accounting_rejected_orders");
    $data['page_title'] = 'Manufacturing Accounting Rejected Orders';
    $data['order_id'] = $order_id;
    $list=$this->input->post('list');
    if($list !="")
    {
    $search=@$_REQUEST['search']['value'];
    
    $result = $this->Mnfg_accounting_rejected_model->get($filter_status,$_REQUEST,$search,$limit=true,$order_id);
    $totalRecords = sizeof($this->Mnfg_accounting_rejected_model->get($filter_status,$_REQUEST,$search,$limit=false,$order_id));
    if (!empty($result)) {
      $data = $this->get_datatable_rows($result);
    }else{
      $data = $this->get_null_row();
    }
      echo json_encode($this->return_json($totalRecords,$data));
    }else{
      $this->view->render('Manufacturing_module/Rejected_orders/rejected_index',$data);
    }
  }

 
  private function generate_data_table(){
    $filter_status =@$_REQUEST['order'][0];
    $status = array();
    $search=@$_REQUEST['search']['value'];
    $result = $this->Mnfg_accounting_rejected_model->get_order_wise_list($filter_status,$status,$_REQUEST,$search,$limit=true,$status='1');
    $totalRecords = sizeof($this->Mnfg_accounting_rejected_model->get_order_wise_list($filter_status,$status,$_REQUEST,$search,$limit=false,$status='1'));
    if (!empty($result)) {
      $data = $this->get_order_wise_datatable_rows($result);
    }else{
      $data[0][] = ['No data found'];
      $data[0][] = [];
      $data[0][] = [];
      $data[0][] = [];
      $data[0][] = [];
      $data[0][] = [];
    }
    return $this->return_json($totalRecords,$data);
  }

  private function get_order_wise_datatable_rows($result){
    foreach ($result as $key => $value) {
       $sr_no=$key+1;
      $data[$key][] = '<span style="float:right">'.$sr_no.'</span>';
      $data[$key][] ='<span style="float:right">'.$value['order_id'].'</span>';
      $data[$key][] =$value['name'];
      $data[$key][] ='<span style="float:right">'.date('d-m-Y',strtotime($value["order_date"])).'</span>';
      $data[$key][] ='<span style="float:right">'.date('d-m-Y H:i:s',strtotime($value["rejected_date"])).'</span>';
      $data[$key][] ='<a href="'.ADMIN_PATH.'Mnfg_accounting_rejected_orders/view/'.$value['order_id'].'"><button class="btn btn-link view_link small loader-hide  btn-sm" name="commit" type="button">VIEW</button></a>';
      }
    return $data;
  }
  private function get_datatable_rows($result){

    foreach ($result as $key => $value) {
      $data[$key][] = $key+1;
      $data[$key][] =$value['order_id'];
      $data[$key][] =date('d-m-Y',strtotime($value["order_date"]));
      $data[$key][] =date('d-m-Y H:i:s',strtotime($value["rejected_date"]));
      $data[$key][] =$value['karigar_name'];
      $data[$key][] =$value["sub_cat_name"];
      $data[$key][] =$value["from_weight"]."-".$value["to_weight"];
      $data[$key][] =$value["rejected_qty"];
    /*  $data[$key][] =$value["quantity"];*/
      }
    return $data;
  }
  private function get_null_row(){
    $data[0][] = ['No data found'];
    $data[0][] = [];
    $data[0][] = [];
    $data[0][] = [];
    $data[0][] = [];
    $data[0][] = [];
    $data[0][] = [];
    $data[0][] = [];
   /* $data[0][] = [];*/
    return $data;
  }
  private function return_json($totalRecords,$data){
    $json_data = array(
          "draw" => intval($_REQUEST['draw']),
          "recordsTotal" => intval($totalRecords),
          "recordsFiltered" => intval($totalRecords),
          "data" => $data
      );
    return $json_data;
  }

  public function Manufacturing_rejected_receipt()
  {
    $data['display_name'] = 'Rejected products from Manufacturing';
    $this->breadcrumbs->push('Manufacturing Rejected products', "Manufacturing_accounting_control");
    $data['page_title'] = 'Karigar Receipt';
    $data['all_karigars'] = $this->Karigar_model->get('','','','',true);
    $data['manufacturing_accounting_kariger'] = $this->Mnfg_accounting_rejected_model->get_engage_karigars();

    $this->view->render('Manufacturing_module/Rejected_orders/rejected_orders_receipt',$data);
  }
  public function rejected_products($id)
  {
      echo json_encode($this->Mnfg_accounting_rejected_model->rejected_products($id));
  }
  public function print_change_net_wt()
  {
    $data['result']       =  $_POST;   
    $data['receipt_code'] = $this->Mnfg_accounting_rejected_model->update();
    $this->load->view('Manufacturing_module/Rejected_orders/mnfg_reject_receipt_print',$data);
  }

  public function change_net_wt(){ 
  $data = $this->Mnfg_accounting_rejected_model->validate_tag_products();

  if($data['status'] == 'success' ){
     foreach ($_POST['kr'] as $key => $value) {
     $ma_res = $this->Mnfg_accounting_rejected_model->check_net_wt($key,$value['net_wt']);
      
    }//die;
  }  
    
    echo json_encode($data);

   }
}