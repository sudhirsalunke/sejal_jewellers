<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class Received_orders extends CI_Controller {
  public function __construct() {
    parent::__construct();
    if(empty($this->session->userdata('user_id'))){
      redirect(ADMIN_PATH . 'auth/logout');
    }
    $this->load->library('excel_lib');
    $this->breadcrumbs->push("Manufacturing Module", "Manufacturing_Dashboard");
    $this->breadcrumbs->push("Dashboard", "Manufacturing_Dashboard");
    $this->load->model(array('Manufacturing_module/Received_orders_model','Manufacturing_module/Karigar_order_model','User_access_model','Manufacturing_module/Department_model'));
    $this->load->config('admin_validationrules', TRUE);
  }
  public function index(){
    $department_id=$this->session->userdata('department_id');
    $department_name=$this->Department_model->find($department_id);
    if(isset($_GET['status']) && $_GET['status'] == 'pending')
    {
      $data['display_name'] =$department_name['name']." - All Pending Orders";
      $this->breadcrumbs->push($department_name['name']." - All Pending Orders", "Received_orders");
    }
    else{
      $this->breadcrumbs->push($department_name['name']." - All Received Orders", "Received_orders");
      $data['display_name'] = $department_name['name']." - Pending to be received from karigar";
     }
    $data['page_title'] = 'Recevied Orders';
    $list=$this->input->post('list');
    if($list !="")
    {
      echo json_encode($this->generate_data_table($department_id));
    }else{
      $this->view->render('Received_orders/order_index',$data);
    }
  }
  public function view($order_id){
    $department_id=$this->session->userdata('department_id');
    $department_name=$this->Department_model->find($department_id);
    $data['display_name'] = $department_name['name']." - Orders pending to be received from karigar";
    $this->breadcrumbs->push($department_name['name']." - All Received Orders", "Received_orders");
    $this->breadcrumbs->push("View", "Received_orders/View");
    $data['page_title'] = 'Recevied Orders';
    $data['order_id'] = $order_id;
    $data['dep_id']=$department_id;
    $list=$this->input->post('list');
    if($list !="")
    {
      $filter_status =@$_REQUEST['order'][0];
      $status = array();
      $search=@$_REQUEST['search']['value'];

      $result = $this->Karigar_order_model->get($filter_status,$status,$_REQUEST,$search,$limit=true,$status='1',$order_id,$department_id);
      $totalRecords = $this->Karigar_order_model->get($filter_status,$status,$_REQUEST,$search,$limit=false,$status='1',$order_id,$department_id);
    if(!empty($result)) {
      $data = $this->get_datatable_rows($result);
    }else{
      $data = $this->get_null_row();
    }
      echo json_encode($this->return_json($totalRecords,$data));
    }else{
      $this->view->render('Received_orders/index',$data);
    }
  }
  private function generate_data_table($department_id){
    $filter_status =@$_REQUEST['order'][0];
    $status = array();
    $search=@$_REQUEST['search']['value'];

    $result = $this->Karigar_order_model->get_order_wise_list($filter_status,$status,$_REQUEST,$search,$limit=true,$status='1',$department_id);
    $totalRecords = $this->Karigar_order_model->get_order_wise_list($filter_status,$status,$_REQUEST,$search,$limit=false,$status='1',$department_id);
    if (!empty($result)) {
      $data = $this->get_order_wise_datatable_rows($result);
    }else{
      $data[0][] = ['No data found'];
      $data[0][] = [];
      $data[0][] = [];
      $data[0][] = [];
      $data[0][] = [];
    }
    return $this->return_json($totalRecords,$data);
  }

  private function get_order_wise_datatable_rows($result){
    foreach ($result as $key => $value) {
      $sr_no=$key+1;
      $data[$key][] = '<span style="float:right">'.$sr_no.'</span>';
      $data[$key][] ='<span style="float:right">'.$value['order_id'].'</span>';
      $data[$key][] =$value['name'];
      $data[$key][] ='<span style="float:right">'.date('d-m-Y',strtotime($value["order_date"])).'</span>';
      $data[$key][] ='<a href="'.ADMIN_PATH.'Received_orders/view/'.$value['order_id'].'"><button class="btn btn-link view_link small loader-hide  btn-sm" name="commit" type="button">VIEW</button></a>';
      }
    return $data;
  }
  private function get_datatable_rows($result){
    foreach ($result as $key => $value) {
      $sr_no=$key+1;
      $data[$key][] = '<span style="float:right">'.$sr_no.'</span>';
      $data[$key][] ='<span style="float:right">'.$value['order_id'].'</span>';
      $data[$key][] ='<span style="float:right">'.$value["order_date"].'</span>';
      $data[$key][] =$value['karigar_name'];
      $data[$key][] =$value["sub_cat_name"];
      $data[$key][] ='<span style="float:right">'. $value["from_weight"]."-".$value["to_weight"].'</span>';

      if($value['department_id'] != '2' && $value['department_id'] !='3' && $value['department_id'] !='6' && $value['department_id'] !='10'):
        $rp_quantity = $this->Received_orders_model->get_rec_qnt_by_kmm_id($value['id']);
        $data[$key][] ='<span style="float:right">'. $value["quantity"].'</span>';
        $Pending_Quantity=$value["quantity"] - $rp_quantity['qnt'];
        $data[$key][] ='<span style="float:right">'. $Pending_Quantity.'</span>';
      else:
        $rp_weight = $this->Received_orders_model->get_rec_wt_by_kmm_id($value['id']);
        $data[$key][] ='<span style="float:right">'. $value["total_weight"].'</span>';
        $Pending_weight=$value["total_weight"] - $value['receive_weight'];
        $data[$key][] ='<span style="float:right">'. $Pending_weight.'</span>';
      endif;  
      }
    return $data;
  }
  private function get_null_row(){
    $data[0][] = ['No data found'];
    $data[0][] = [];
    $data[0][] = [];
    $data[0][] = [];
    $data[0][] = [];
    $data[0][] = [];
    $data[0][] = [];
    $data[0][] = [];
    return $data;
  }
  private function return_json($totalRecords,$data){
    $json_data = array(
          "draw" => intval($_REQUEST['draw']),
          "recordsTotal" => intval($totalRecords),
          "recordsFiltered" => intval($totalRecords),
          "data" => $data
      );
    return $json_data;
  }
  
}