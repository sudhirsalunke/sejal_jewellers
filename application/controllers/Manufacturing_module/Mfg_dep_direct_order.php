<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Mfg_dep_direct_order extends CI_Controller {

  public function __construct() {
    parent::__construct();
    if(empty($this->session->userdata('user_id'))){
      redirect(ADMIN_PATH . 'auth/logout');
    }
    $this->breadcrumbs->push("Manufacturing Module", "Manufacturing_Dashboard");
    $this->breadcrumbs->push("Dashboard", "Manufacturing_Dashboard");
    $this->load->model(array('sales_module/Sales_stock_model',
                            'sale_master/Customer_model',
                            'sale_master/Kundan_category_model',
                            'sale_master/Type_master_model',
                            'sale_master/Color_stone_model',
                            'User_access_model',
                            'Weight_range_model',
                            'Karigar_model',
                            'Party_master_model',
                            'sale_master/Parent_category_model',
                            'Category_model',
                            'Manufacturing_module/Department_model','Manufacturing_module/Mfg_dep_direct_order_model','Manufacturing_module/Prepare_karigar_model','Manufacturing_module/Quality_control_model'));
    $this->load->config('admin_validationrules', TRUE);
    $this->load->library('Data_encryption');
  }

  public function create() {
    $department_id=$this->session->userdata('department_id');
    $department_name=$this->Department_model->find($department_id);

    $data['parent_category'] = $this->Parent_category_model->get('','','','',true);
    $data['karigar']= $this->Karigar_model->get('','','','',true);
    
    if($department_id !='0'){
        $dep_name[]=$this->Department_model->find($department_id);   
        $data['department'] = $dep_name;
    }else{            
          $data['department'] = $this->Department_model->get('','','','',true);          
    }
    $data['weight_range'] = $this->Weight_range_model->get('','','','',true);
    $data['category']=$this->Category_model->get('','','','',true,$department_id);
    $data['dep_id']=$department_id;
    $data['tagging_status']=$department_name['tagging_status'];
    $data['display_title'] = $department_name['name']." - Create New Receive Products from karigar";
    $data['page_title']="Create New Deparment Order";
    $this->view->render('Manufacturing_module/Mfg_dep_direct_order/create', $data);
  }
 

  public function store(){
     $data=$this->validation_result();
     if($data['status']!='failure') {
      $data = $this->Mfg_dep_direct_order_model->store();
    }
   
    echo json_encode($data);
  }   

  public function print_receipt($receipt_code)
  {        
    $data['receipt_code'] = $this->Mfg_dep_direct_order_model->receive_product_details_by_receipt_code($receipt_code);
    $user_id=$this->session->userdata('user_id');
    $dep_id=$this->User_access_model->find($user_id); 
    $department_id=$this->session->userdata('department_id');
    $data['department_id']=$department_id;
    $this->load->view('Manufacturing_quality_control/kariger_receipt_print', $data);

  }

  private function validation_result(){
    $data = array();
  if(!empty($_POST['kariger_id']) && !empty($_POST['order_name'])){
      $postdata = $_POST;
      $data['status'] = 'success';
     //print_r($postdata);die;
      foreach ($postdata['product_name'] as $key => $value) {    
    
        if(empty($postdata['product_name'][$key])){
          $data['status'] = 'failure';
          $data['error'][] = 'Row '.($key+1).' Product name can not be empty';
        }
        if(empty($postdata['weight_range_id'][$key])){
          $data['status'] = 'failure';
          $data['error'][] = 'Row '.($key+1).' Weight Range can not be empty';
        }

        if(isset($postdata['quantity'][$key]) && empty($postdata['quantity'][$key])){
          $data['status'] = 'failure';
          $data['error'][] = 'Row '.($key+1).' Quantity can not be empty';
        }   
        
        if(isset($postdata['weight'][$key]) && empty($postdata['weight'][$key])){
          $data['status'] = 'failure';
          $data['error'][] = 'Row '.($key+1).' Weight can not be empty';
        }    
       
                
        if($postdata['dep_id'] !='2' && $postdata['dep_id']!='3' && $postdata['dep_id'] !='6' && $postdata['dep_id']!='10' && empty($postdata['gross_wt'][$key])){
          $data['status'] = 'failure';
          $data['error'][] = 'Row '.($key+1).' Gross wt can not be empty';
        }      
       
       
        if(empty($postdata['net_wt'][$key])){
          $data['status'] = 'failure';
          $data['error'][] = 'Row '.($key+1).'  Net wt can not be empty';
        }
        
        
      }
    }
    return $data;
  }

}//class