<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class Kundan_QC extends CI_Controller {
	public function __construct(){
		parent::__construct();
    if(empty($this->session->userdata('user_id'))){
      redirect(ADMIN_PATH . 'auth/logout');
    }
    $this->breadcrumbs->push("Manufacturing Module", "Manufacturing_Dashboard");
  	$this->breadcrumbs->push("Dashboard", "Manufacturing_Dashboard");
  	$this->load->model(array('Manufacturing_module/Kundan_QC_model','Manufacturing_module/Prepare_kundan_karigar_order_model','Manufacturing_module/Received_orders_model','Manufacturing_module/Karigar_receive_order_model','User_access_model','Manufacturing_module/Department_model'));
  	$this->load->config('admin_validationrules', TRUE);
    $this->load->library('m_pdf');
	}
	// public function store(){
	// 	$result = $this->Prepare_karigar_model->validation();
	// 	if($result['status'] == 'success'){
	// 		$result = $this->Prepare_karigar_model->store($_POST);
	// 	}
	// 	echo json_encode($result);
	// }
	public function index(){
    $department_id=$this->session->userdata('department_id');
    $department_name=$this->Department_model->find($department_id);
		if(isset($_GET['status']) && $_GET['status'] == 'send_qc_pending')
    {
      $data['display_name'] =$department_name['name']." - Sending QC";
      $this->breadcrumbs->push($department_name['name']." - All Sending QC Orders", "Kundan_QC");
    }elseif(isset($_GET['status']) && $_GET['status'] == 'pending')
    {
      $data['display_name'] =$department_name['name']." - Pending QC";
      $this->breadcrumbs->push($department_name['name']." - All Pending Orders", "Kundan_QC");
    }elseif(isset($_GET['status']) && $_GET['status'] == 'complete'){
      $this->breadcrumbs->push($department_name['name']." - Quality Control", "Kundan_QC");
      $data['display_name'] = $department_name['name']." - QC Accepted Products";
    }elseif(isset($_GET['status']) && $_GET['status'] == 'rejected'){
      $this->breadcrumbs->push($department_name['name']." - Quality Control", "Kundan_QC");
      $data['display_name'] = $department_name['name']." - QC Rejected Products";
    }
    $data['page_title'] = 'Quality Control';
    $list=$this->input->post('list');
    if($list !="")
    {
      echo json_encode($this->generate_data_table($department_id));
    }else{
      $this->view->render('Manufacturing_module/Kundan_karigar/Qc/index',$data);
    }
	}
	public function generate_data_table($department_id){
		$filter_status =@$_REQUEST['order'][0];
    $search=@$_REQUEST['search']['value'];
    $status = array();
    $result = $this->Kundan_QC_model->get($filter_status,$status,$_REQUEST,$search,$limit=true,$department_id);
    $totalRecords = $this->Kundan_QC_model->get($filter_status,$status,$_REQUEST,$search,$limit=false,$department_id);
    if (!empty($result)) {
      $data = $this->get_datatable_rows($result);
    }else{
      $data = $this->get_null_row();
    }
    return $this->return_json($totalRecords,$data);
	}
	private function get_datatable_rows($result){
    foreach ($result as $key => $value) {
      $sr_no=$key+1;
      if(isset($_GET['status']) && $_GET['status'] == 'pending')
      {
        // $data[$key][] ='<input type="checkbox" value="'.$value['rp_id'].'" name="Qc[]"/>';
    /*  $data[$key][] = '<span style="float:right">'.$sr_no.'</span>';*/
        $data[$key][] = '<div class="checkbox checkbox-purpal"><input type="checkbox" name="rp_id[]" id="checkbox_'.$value['rp_id'].'" class="form-control checkbox_ctn" value="'.$value['rp_id'].'"><label for="checkbox_'.$value['rp_id'].'"></label></div>';
      }elseif(isset($_GET['status']) && $_GET['status'] == 'complete'){
        $data[$key][] ='<input type="checkbox" value="'.$value['qc_id'].'" name="Qc[]"/>';
      }elseif(isset($_GET['status']) && $_GET['status'] == 'rejected' || $_GET['status'] == 'send_qc_pending'){       
        $data[$key][] = '<span style="float:right">'.$sr_no.'</span>';
      }
      $data[$key][] =$value['km_name'];
      $data[$key][] =$value['product_code'];
      $data[$key][] =$value['name'];
      $data[$key][] ='<span style="float:right">'. $value['quantity'].'</span>';
      $data[$key][] ='<span style="float:right">'. round($value['gr_wt'],2).'</span>';
      $data[$key][] ='<span style="float:right">'. round($value['net_wt'],2).'</span>';
      // $data[$key][] = $value['from_weight'].'-'.$value['to_weight'];
      // $data[$key][] = (($value['from_weight']+$value['to_weight']) /2 * $value['quantity']);
      if(isset($_GET['status']) && $_GET['status'] == 'rejected'){
        $data[$key][] ='<a href="'.ADMIN_PATH.'kundan_QC/reprint_order/'.$value['qc_id'].'"><button class="btn btn-link view_link small loader-hide  btn-sm" name="commit" type="button" >Send To Kundan Karigar</button></a>';
      }
      if(isset($_GET['status']) && $_GET['status'] == 'pending'){
        $data[$key][] ='<a href="'.ADMIN_PATH.'kundan_QC/create/'.$value['rp_id'].'"><button class="btn btn-link delete_link small loader-hide  btn-sm" name="commit" type="button" >QC Reject</button></a> <button class="btn btn-link view_link small loader-hide  btn-sm" id="s_'.$value['rp_id'].'" name="commit" type="button" onclick="kundan_qc_check_accept('.$value['rp_id'].'); ">QC Accept</button>';
      }    
       if(isset($_GET['status']) && $_GET['status'] == 'send_qc_pending'){
        $data[$key][] ='<button class="btn btn-link view_link small loader-hide  btn-sm" id="s_'.$value['rp_id'].'" name="commit" type="button" onclick="kundan_send_qc_mfg('.$value['rp_id'].'); ">Send QC</button>';
      }
      
    }
    return $data;
  }
  private function get_null_row(){
    $msg='No data found';
    if(isset($_GET['status']) && $_GET['status'] == 'rejected'){
        $t_count=10;
      }else{
        $t_count=9;
      }
    for ($i=0; $i <=$t_count ; $i++) { 
          $data[0][$i] = [$msg];
          $msg="";
    }
    return $data;
  }
  private function return_json($totalRecords,$data){
    $json_data = array(
          "draw" => intval($_REQUEST['draw']),
          "recordsTotal" => intval($totalRecords),
          "recordsFiltered" => intval($totalRecords),
          "data" => $data
      );
    return $json_data;
  }
  public function update(){
    die('Kundan QC model > status update');
  	$update_arr = array('status'=>1);
  	$result = $this->Kundan_QC_model->update($update_arr,$_POST);
  	echo json_encode($result);
  }
  public function send_to_receive_products(){
  	$result = $this->Kundan_QC_model->send_to_receive_products();
  	echo json_encode($result);
  }
  public function create($rp_id=''){
    if(!empty($rp_id)){
      $data['product_ids'] = $rp_id;
    }else{
      $data['product_ids'] = implode(',',$_POST['Qc']);
    }
    $data['receive_from_hallmarking'] = false;
    $department_id=$this->session->userdata('department_id');
    $department_name=$this->Department_model->find($department_id);
    $data['page_title'] = 'Quality Control';
    $data['display_name'] ="Quality Control";
    $this->breadcrumbs->push($department_name['name']." All Pending Orders", "Kundan_QC");
    $data['receive_order'] = $this->Karigar_receive_order_model->find_by_rp_id($rp_id);
    //print_r($data['receive_order']['department_id'] );die;
    $this->view->render('Manufacturing_module/Kundan_karigar/Qc/create',$data);
  }
  public function store(){
     $this->form_validation->set_rules('weight', 'Weight', 'required|numeric|greater_than_equal_to[0.1]',
                        array('required' => 'You must provide a %s.',
                              'numeric' => 'Please Enter Valid Weight',
                              'greater_than_equal_to'=>'Weight Should greter than 0'));
      if ($this->form_validation->run() == FALSE){
            
            $data['status']= 'failure';
            $data['data']= '';
            $data['error'] = array(
            'weight'=>strip_tags(validation_errors())
              );
          echo json_encode($data);


       }else{

          $rp_id = explode(',', $_POST['receive_product_id']);
            foreach ($rp_id as $key => $value) {
            //need receipt code here
            $postdata = $_POST;
            $postdata['receive_product_id'] = $value;
            $postdata['user_id'] = $this->session->userdata('user_id');
            $result = $this->Kundan_QC_model->store($postdata);
             }
          echo json_encode(get_successMSG());    
      }
   
  }

  public function accept_all(){
    //print_r($_POST['rp_id']);die;
      foreach ($_POST['rp_id'] as $key => $value){    
      $rp_id=$value;  
      $receive_order= $this->Karigar_receive_order_model->find_by_rp_id($rp_id);
       // print_r($receive_order);die;
            $postdata['receive_product_id'] = $rp_id;
            $postdata['quantity'] = $receive_order['quantity'];
            $postdata['weight'] = $receive_order['net_wt'];
            $postdata['net_wt'] = $receive_order['net_wt'];
            $postdata['gross_wt'] = $receive_order['gross_wt'];            
            $postdata['kundan_id'] = $receive_order['kundan_id'];
            $postdata['department_id'] = $receive_order['department_id'];
            $postdata['user_id'] = $this->session->userdata('user_id');
            $result = $this->Kundan_QC_model->store($postdata); 
            $data['status'] = 'success';         
    }
         echo json_encode($data); die;  
  }
  public function accept($rp_id){
    $receive_order= $this->Karigar_receive_order_model->find_by_rp_id($rp_id);
   // print_r($receive_order);die;
          $postdata['receive_product_id'] = $rp_id;
          $postdata['weight'] = $receive_order['net_wt'];
          $postdata['net_wt'] = $receive_order['net_wt'];
          $postdata['gross_wt'] = $receive_order['gross_wt']; 
          $postdata['quantity'] = $receive_order['quantity'];
          $postdata['kundan_id'] = $receive_order['kundan_id'];
          $postdata['department_id'] = $receive_order['department_id'];
          $postdata['user_id'] = $this->session->userdata('user_id');
          $result = $this->Kundan_QC_model->store($postdata);          
          echo json_encode(get_successMSG());   
  }

  public function send_kundan_qc($rp_id){
    $receive_order= $this->Kundan_QC_model->send_kundan_qc($rp_id);
        
          echo json_encode(get_successMSG());   
  }
  

  public function reject(){
    $rp_id = explode(',', $_POST['receive_product_id']);
    //print_r($_POST);die;
    foreach ($rp_id as $key => $value) {
      //need receipt code here
      $postdata = $_POST;
      $postdata['receive_product_id'] = $value;
      $postdata['user_id'] = $this->session->userdata('user_id');
      $postdata['rejected'] =true;
      $result = $this->Kundan_QC_model->store($postdata);
    }//die;
    echo json_encode(get_successMSG());
  }
  public function ready_products(){
    $data['pdf_details'] = $this->Kundan_QC_model->ready_products();
    $this->load->view('Manufacturing_module/Kundan_karigar/Qc/ready_products',$data);
    // $html=$this->load->view('Manufacturing_module/Kundan_karigar/Qc/ready_products',$data,true);
    // $pdfFilePath = "Ready_Products.pdf";
    // $this->m_pdf->pdf->WriteHTML($html);
    // $this->m_pdf->pdf->Output($pdfFilePath, "D");
  }
  public function reprint_order($qc_id){
    $hash = strtotime(date('Y-m-d H:i:s'));
    $result = $this->Kundan_QC_model->get_product_details_by_qc_id($qc_id);
    $department = $this->Prepare_kundan_karigar_order_model->get_department_id($result['msqr_id']);
    $postdata['department_id'] = $department['department_id'];
    $postdata['manufacturing_order_id'] = $department['order_id'];
    $postdata['quantity'] = $result['quantity'];
    $postdata['msqr_id'] = $result['msqr_id'];
    $postdata['status'] = 1;
    $postdata['issue_voucher'] =  $hash;
    $postdata['karigar_id'] = $result['k_id'];
    $postdata['created_at'] = date('Y-m-d H:i:s');
    $postdata['updated_at'] = date('Y-m-d H:i:s');
    $this->Prepare_kundan_karigar_order_model->store($postdata);
    $this->db->set('status','10');
    $this->db->where('id',$qc_id);
    $this->db->update('quality_control');
    $get_pdf_details = $this->Kundan_QC_model->get_product_details_by_qc_id($qc_id);
    //echo $this->db->last_query();exit;
    $pdf_data[0]['quantity'] = $result['quantity'];
    $pdf_data[0]['net_wt'] = $get_pdf_details['net_wt'];
    $pdf_data[0]['gross_wt'] = $get_pdf_details['gross_wt'];
    $pdf_data[0]['product_code'] = $get_pdf_details['product_code'];
    $pdf_data[0]['image'] = $get_pdf_details['image'];
    $pdf_data[0]['pcs'] = $get_pdf_details['pcs'];
    $data['karigar_name'] = $get_pdf_details['km_name'];
    $data['result'] = $pdf_data;
    $data['chitti_no'] = $hash;
    $this->load->view('Manufacturing_module/Prepare_kundan_karigar_order/print',$data);
    // $pdfFilePath = "Resend_order_".$qc_id.".pdf";
    // $this->m_pdf->pdf->WriteHTML($html);
    // $this->m_pdf->pdf->Output($pdfFilePath, "D");
  }
}