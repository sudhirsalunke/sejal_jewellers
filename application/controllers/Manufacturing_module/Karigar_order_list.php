<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class Karigar_order_list extends CI_Controller {
	public function __construct(){
		parent::__construct();
	    if(empty($this->session->userdata('user_id'))){
	      redirect(ADMIN_PATH . 'auth/logout');
	    }
        $this->breadcrumbs->push("Manufacturing Module", "Manufacturing_Dashboard");
        $this->breadcrumbs->push("Dashboard", "Manufacturing_Dashboard");
        $this->load->model(array('Manufacturing_module/Manufacturing_department_order_model','Manufacturing_module/Karigar_order_model','Manufacturing_module/Prepare_karigar_model','corporate_module/Prepare_product_list','Karigar_model','User_access_model','Manufacturing_module/Department_model'));
        $this->load->library('m_pdf');
        $this->load->config('admin_validationrules', TRUE);
    }
	public function index(){
        $department_id=$this->session->userdata('department_id');
        $department_name=$this->Department_model->find($department_id);
        $data['page_title'] = "Karigar Order List";
        $this->breadcrumbs->push($department_name['name']." - Order Not Sent To Karigar", "Karigar Order List");
        $data['display_title'] = $department_name['name']." - Order Not Sent To Karigar";
        $list=$this->input->post('list');
        $data['dep_id']=$department_id;

        if($list !="")
        {
          echo json_encode($this->generate_data_table($department_id));
        }else{
        $data['all_karigars'] = $this->Karigar_model->get('','','','',true);  
          $this->view->render('Manufacturing_module/Karigar_order/index',$data);
        }
	}
	private function generate_data_table($department_id){
          
	    $filter_status =@$_REQUEST['order'][0];
	    $status = array('name','code');
	    $search=@$_REQUEST['search']['value'];
	    $result = $this->Karigar_order_model->get($filter_status,$status,$_REQUEST,$search,$limit=true,'','',$department_id);
       
	    $totalRecords = $this->Karigar_order_model->get($filter_status,$status,$_REQUEST,$search,$limit=false,'','',$department_id);
	    if (!empty($result)) {
        foreach ($result as $key => $value) {
            $data[$key][] ='<span style="float:right">'.$value["order_id"].'</span>';
            // $data[$key][] =$value["product_code"];
            $data[$key][] ='<span style="float:right">'.$value["order_date"].'</span>';
            $data[$key][] =$value["karigar_name"];
            $data[$key][] =$value["sub_cat_name"];
            $data[$key][] ='<span style="float:right">'.$value["from_weight"].' - '.$value["to_weight"].'</span>';
            if($department_id == '2' || $department_id == '3' || $department_id =='6' || $department_id =='10'){
                  $data[$key][] ='<span style="float:right">'.$value["weight"].'</span>';
                }else{
                    $data[$key][] ='<span style="float:right">'.$value["quantity"].'</span>';
                    $data[$key][] ='<span style="float:right">'.$value["approx_weight"].'</span>';
                }
		        $button_html = '<a href="'.ADMIN_PATH.'Karigar_order_list/show/'.$value["id"].'"><button class="btn btn-link view_link small loader-hide  btn-sm" name="commit" type="button">SEND</button></a>&nbsp;&nbsp;<a><button class="btn btn-link edit_link small loader-hide  btn-sm" name="commit" type="button" onclick="change_manufactured_kariger('.$value['id'].')">Change Kariger</button></a>&nbsp;&nbsp;<a><button class="btn btn-link delete_link small loader-hide  btn-sm" name="commit" type="button" onclick="remove_kariger_from_kariger_list('.$value["id"].')">Remove</button></a>';
		     		$data[$key][] = $button_html;
        }
	    }else{
        $data[0][] = ['No data found'];
        $data[0][] = [];
        $data[0][] = [];
        $data[0][] = [];
        $data[0][] = [];
        $data[0][] = [];
        $data[0][] = [];
        $data[0][] = [];
	    }
     $json_data = array(
          "draw" => intval($_REQUEST['draw']),
          "recordsTotal" => intval($totalRecords),
          "recordsFiltered" => intval($totalRecords),
          "data" => $data
      );
	    return $json_data; 
	}
	public function show($id){
        $department_id=$this->session->userdata('department_id');
        $department_name=$this->Department_model->find($department_id);
		$data['page_title'] = "Send Order To Karigar";
		$data['display_title'] = "Send ".$department_name['name']." Order To Karigar";
        //print_r($id);
		$data['result'] = $this->Karigar_order_model->find($id);
        //print_r($data['result']);die;
        $data['sub_cat_img'] = $this->Manufacturing_department_order_model->get_parent_cat_img($data['result']['mom_id']);
		$this->view->render('Manufacturing_module/Karigar_order/show',$data);
	}

    public function validate_postdata(){
        $validation = $this->Karigar_order_model->validate_postdata();
        echo json_encode($validation);
    }
	public function print_karigar_order($id=''){    
    //print_r($_POST);die;   
        if(!empty($_POST)){
            foreach ($_POST['mop_id'] as $key => $value) {
                $insert_array = array(
                    'karigar_id'=>$_POST['karigar_id'],
                    'mop_id'=>$value,
                    'department_id'=>$_POST['department_id'][$key],
                    'quantity'=>@$_POST['quantity'][$key],
                    'approx_weight'=>@($_POST['quantity'][$key] * $_POST['approx_weight'][$key]),
                    'total_weight'=>@$_POST['weight'][$key],                    
                    'status'=>'1',
                    'created_at'=>date('Y-m-d H:i:s'),
                    'updated_at'=>date('Y-m-d H:i:s'),
                    'delivery_date'=>date('Y-m-d',strtotime("+12 day"))
                    );
            $id = $this->Prepare_karigar_model->store($insert_array);
            // $data['result'][] = $this->Karigar_order_model->find($id); 
            $data['result'][] = $this->Manufacturing_department_order_model->print_order($id);

            //print_r($data['result']);
            //echo $this->db->last_query();
            $data['sub_cat_img'][] = $this->Manufacturing_department_order_model->get_parent_cat_img($data['result']['mom_id']);
            }
        }else{
            $update = array('status'=>'1','delivery_date'=>date('Y-m-d',strtotime("+12 day")));
            $where = array('id'=>$id);
            $this->Karigar_order_model->update($where,$update);
            // $data['result'][] = $this->Karigar_order_model->find($id); 
    		$data['result'][] = $this->Manufacturing_department_order_model->print_order($id);   
           //print_r($data['result']);die;
            $data['sub_cat_img'] = $this->Manufacturing_department_order_model->get_parent_cat_img($data['result']['mom_id']);
        }

        $this->load->view('Manufacturing_module/Karigar_order/print', $data);
	}
    public function re_print_order($id){
        // $data['result'][] = $this->Karigar_order_model->find($id);  
        $data['result'][] = $this->Manufacturing_department_order_model->print_order($id);  
        $data['sub_cat_img'][] = $this->Manufacturing_department_order_model->get_parent_cat_img($data['result']['mom_id']);
        $this->load->view('Manufacturing_module/Karigar_order/print', $data);
        // $html=$this->load->view('Manufacturing_module/Karigar_order/print', $data,true);
        // $pdfFilePath = "Print_order_".$karigar_id.".pdf";
        // $this->m_pdf->pdf->WriteHTML($html);
        // $this->m_pdf->pdf->Output($pdfFilePath, "D"); 
    }
    public function kariger_details($kar_id)
    {
        $this->breadcrumbs->push("Engaged Karigar List", "Engaged_karigar_list");
        $this->breadcrumbs->push("Karigars Details", "Engaged_karigar_list/show/$karigar_id");
        $data['page_title'] = "KARIGAR LIST";
        $data['result']=$this->Karigar_order_model->view_karigar_details($kar_id);
        $this->view->render('Manufacturing_module/Karigar_order/kariger_details',$data);
    }

    /*
        Code to delete kariger from kariger order list 
    */
   public function delete($id)
   {
        $result = $this->Karigar_order_model->delete($id);
        echo json_encode($result);
   }     
}