<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class Tag_products extends CI_Controller {
    public function __construct() {
        parent::__construct();
        if (empty($this->session->userdata('user_id'))) {
            redirect(ADMIN_PATH . 'auth/logout');
        }
        $this->load->config('admin_validationrules', TRUE);
        $this->breadcrumbs->push("Corporate Module", "Manufacturing_Dashboard");
        $this->breadcrumbs->push("Dashboard", "Manufacturing_Dashboard");
        $this->load->library('m_pdf');
        $this->load->model(array('Manufacturing_module/Tag_products_model', 'Manufacturing_module/Quality_control_model', 'sale_master/parent_category_model', 'Karigar_model', 'Category_model', 'User_access_model', 'Manufacturing_module/Department_model'));
        $this->load->library('zend');
        $this->zend->load('Zend/Barcode');
    }

    public function index() {
        $department_id = $this->session->userdata('department_id');
        $department_name = $this->Department_model->find($department_id);
        $data['page_title'] = 'Tag Products';
        $data['display_name'] = $department_name['name'] . ' - Tag Products';

        if ($_GET['status'] == "hallmarking") { /* from hallmrking QC */
            $this->breadcrumbs->push("QC Accepted Hallmarking Orders", "Manufacturing_hallmarking?status=receive");
        }

        $data['karigar_ids'] = $this->Tag_products_model->get_engaged_karigar($department_id);
        $this->breadcrumbs->push("Tag Products", "Tag_products");
        $this->view->render('Manufacturing_module/tag_products/index', $data);
    }

    public function get_parent_category() {
        //print_r($_POST['karigar_id']);die;
        $department_id = $this->session->userdata('department_id');
        $data['status'] = 'failure';
        $data['karigar_ids'] = $this->Tag_products_model->get_parent_category($department_id);
          //echo $this->db->last_query();die;
        if (!empty($data['karigar_ids'])) {
            $data['status'] = 'success';
        }
        echo json_encode($data);
    }

    public function store() {

               // $this->create_product_code();
                $data = $this->Tag_products_model->validate_tag_products();
                //print_r($_POST['tag_products']);die;
                if ($data['status'] == 'success') {
                    $data['qc_id'] = $this->Tag_products_model->store($_POST);
                }
                echo json_encode($data);
  

    }

    public function get_qc_details() {
      // print_r($_POST);die;
        $department_id = $this->session->userdata('department_id');
        $where = array();
        $result['status'] = 'success';
        $expl_postdata = explode('-', $_POST['qc_id']);
        if (!empty($expl_postdata[0]))
            $where['tp.sub_code'] = $expl_postdata[0];
        if (!empty($expl_postdata[1]))
            $where['tp.weight_range_id'] = $expl_postdata[1];
        $where['tp.status'] = '0';
        if (!empty($_POST['karigar_id']))
            $where['tp.karigar_id'] = $_POST['karigar_id'];
        $result['data'] = $this->Tag_products_model->get_by_filter($where,$department_id);
        //print_r($result['data']);die;
        if (!empty($result['data'])) {
            foreach ($result['data'] as $key => $value) {
                //$result['data'][$key]['billing_cat'] = $this->Tag_products_model->get_billing_catgory($value['qc_id'],$department_id);
                $result['data'][$key]['billing_cat'] =get_max_code($department_id,$value['sub_code']);
            }
        }
        $result['qc']['remaining_qty_tag'] = count($this->Tag_products_model->get_by_filter($where,$department_id));
        //$result['tag']['net_wt_validtion'] = $this->Tag_products_model->get_by_filter_net_wt_validtion($where,$department_id);
    // echo $this->db->last_query();die;
        echo json_encode($result);
    }

    public function remove() {
        $data = $this->Tag_products_model->remove_products();
        echo json_encode($data);
    }

    public function max_count_category() {
        $data = $this->Tag_products_model->get_max_code($_POST);
        echo json_encode($data);
    }

    public function make_pdf($qc_id) {
   /*     $qc_id = y_decrypt($qc_id);
        $data['tag_products'] = $this->Tag_products_model->print_data($qc_id);
        if (empty($data['tag_products'])) {
            redirect(ADMIN_PATH . 'tag_products');
        }
        $this->load->view('Manufacturing_module/tag_products/print', $data);
*/
        $qc_id = y_decrypt($qc_id);
        $data['result'] = $this->Tag_products_model->print_data($qc_id);
        $data['barcode']='tag_products';

        if (empty($data['result'])) {
            redirect(ADMIN_PATH . 'tag_products');
        }

        $this->load->view('Manufacturing_module/ready_product/print_barcode',$data);


        //$this->load->view('Manufacturing_module/ready_product/print_barcode',$data);
        // $html=$this->load->view('Manufacturing_module/tag_products/print', $data, true);
        // if(!is_dir(FCPATH."uploads/pdf")){
        //     mkdir(FCPATH."uploads/pdf",0777);
        // }
        // $this->m_pdf->pdf->autoLangToFont = true;
        // $this->m_pdf->pdf->SetFont('AponaLohit');
        // $pdfFilePath = "Tag_order".date('YmdHis').".pdf";
        // $this->m_pdf->pdf->WriteHTML($html);
        // $this->m_pdf->pdf->Output($pdfFilePath, "D");
        // redirect(ADMIN_PATH.'Manufacturing_quality_control?status=complete');
    }

    public function create() {
        $department_id = $this->session->userdata('department_id');
        $department_name = $this->Department_model->find($department_id);
        $data['page_title'] = 'Tagged Products';
        $this->breadcrumbs->push($department_name['name'] . " - Tagged Product", "tag_products/create");
        $data['display_name'] = $department_name['name'] . ' - Tagged Products';
        $data['department_id']=$department_id;
        $_GET['status'] = 'receive';
        $list = $this->input->post('list');
        if ($list != "") {
            echo json_encode($this->generate_data_table($department_id));
        } else {
            $data['all_karigars'] = $this->Karigar_model->get('', '', '', '', true);
            $this->view->render('Manufacturing_module/tag_products/create_tag', $data);
        }
        // $data['result']=$this->Tag_products_model->get($status=1);
        // $data['get_filters']=$this->parent_category_model->get();
    }

    private function generate_data_table($department_id) {
        $filter_status = @$_REQUEST['order'][0];
        $status = array('p.created_at', 'p.product_code');
        $search = @$_REQUEST['search']['value'];
        $result = $this->Tag_products_model->get($status = 1, $filter_status, $status, $_REQUEST, $search, $limit = true, $department_id);
        $totalRecords = $this->Tag_products_model->get($status = 1, $filter_status, $status, $_REQUEST, $search, $limit = false, $department_id);
        //print_r($result);die;
        if (!empty($result)) {
            $data = $this->get_datatable_rows($result);
        } else {
            $data = $this->get_null_row();

        }
        return $this->return_json($totalRecords, $data);
    }

    private function get_datatable_rows($result) {
        foreach ($result as $key => $value) {
            if (empty($value['image'])) {
                $img = "";
            } else {
                $img = ADMIN_PATH . $value['image'];
            }
            $data[$key][] = '<input type="hidden" name="msr_id[' . $value['id'] . ']" value="' . $value['id'] . '"><div class="checkbox checkbox-purple">
                          <input type="checkbox" class="form-control pull-right checked checkbox_ctn" name="tag_product[]" value="' . $value['id'] . '" id="check_' . $key . '">
                          <label for="check_' . $key . '"></label>
                        </div>';
            $data[$key][] = '<input type="hidden" name="gr_wt['.$value['id'].']" value="'. $value['tg_gr_wt'] . '">
      <input type="hidden" name="net_wt[' . $value['id'] . ']" value="' . $value['net_wt'] .'">
      <input type="hidden" name="karigar_id[' . $value['id'] . ']" value="' . $value['karigar_id'] . '">
      <input type="hidden" name="department_id[' . $value['id'] . ']" value="' . $value['department_id'] . '"> 
      <input type="hidden" name="category_id[' . $value['id'] . ']" value="' . $value['billing_category_id'] . '"> 
      <input type="hidden" name="item_category_code[' . $value['id'] . ']" value="' . $value['item_category_code'] . '"> 
      <input type="hidden" name="product[' . $value['id'] . ']" value="' . $value['sub_category_name'] . '">
      <input type="hidden" name="product_code[' . $value['id'] . ']" value="' . $value['product_code'] . '">' . $value['product_code'] . '';
            $data[$key][] = $value['karigar_name'] . '';
            $data[$key][] = $value['sub_category_name'];
            $data[$key][] = '<input type="text" placeholder="Quantity" class="form-control" id="quantity_' . $value['id'] . '" name="quantity[' . $value['id'] . ']" value="' . $value['quantity'] . '"  readonly >';

            $data[$key][] = '<span style="float:right">' . round($value['tg_gr_wt'],2) . '</span>';
            $data[$key][] = '<span style="float:right">' . round($value['net_wt'],2) . '</span>';
            // $data[$key][] = '<input type="text" name="pcs[' . $value['id'] . ']" class="form-control">';
            $data[$key][] = '<input type="file" name="prepare[' . $value['id'] . ']" onchange="save_tag_image(' . $value['id'] . ')"><img src="' . $img . '" id="img_' . $value['id'] . '">';
        }
        return $data;
    }

    private function get_null_row() {
        $data[0][] = ['No data found'];
        $data[0][] = [];
        $data[0][] = [];
        $data[0][] = [];
        $data[0][] = [];
        $data[0][] = [];
        $data[0][] = [];
        $data[0][] = [];
        $data[0][] = [];
        return $data;
    }

    private function return_json($totalRecords, $data) {
        $json_data = array(
            "draw" => intval($_REQUEST['draw']),
            "recordsTotal" => intval($totalRecords),
            "recordsFiltered" => intval($totalRecords),
            "data" => $data
        );
        return $json_data;
    }

    public function get_max_code() {
              $department_id = $this->session->userdata('department_id');
        $result = $this->Tag_products_model->get_max_code($_POST,$department_id);
        echo json_encode($result);
    }

    /*function create_item_code() {
        $postData = $this->input->post();

        $previous_count = ($postData['count'] - 1);
        
        if (trim($postData['sub_code']) == '' && trim($postData['item_category_code']) == '') {

            echo json_encode(array('item_code' => ''));
            exit();
        }

        $res = $this->Tag_products_model->last_product_code($postData['sub_code'], $postData['item_category_code']);
        if (empty($res)) {
            $last_product_code = '0';
        } else {
            $last_product_code = $res['product_code'];
            $last_product_code = explode('-', $last_product_code);
            $last_product_code = end($last_product_code);
        }
        //print_r($last_product_code);die;
        
        $last_product_code = ($last_product_code + $previous_count);

        $item_code = $postData['tag_products'][$key]['product_code'] = $postData['item_category_code'] . '-' . ($last_product_code + 1);
        echo json_encode(array('item_code' => $item_code));
    }*/

    private function create_product_code() {
        $postData = $this->input->post();
       // print_r($postData);die;
        foreach ($postData['tag_products'] as $key => $value) {
            if(!empty($value['item_category_code'])){
             $res = $this->Tag_products_model->check_product_code($value['product_code'],$value['sub_code'], $value['item_category_code'], $postData['qc_id']);
            }
            if ($value['product_code'] == '' &&  empty($res) && $value['item_category_code']) {
                unset($postData['tag_products'][$key]);
            }
        }
        $product_code_array = array();
        foreach ($postData['tag_products'] as $key => $value) {
            //echo"SSSS";
            $item_category_code = explode('-', $value['product_code']);
            $item_category_code = reset($item_category_code);
            $res = $this->Tag_products_model->last_product_code($value['sub_code'], $value['item_category_code'], $postData['qc_id']);
            //print_r($res);die;
            if (empty($res)) {
                $product_code_array[$value['item_category_code']] = '0';
            } else {
                $last_product_code = $res['product_code'];
                $last_product_code = explode('-', $last_product_code);
                $last_product_code = end($last_product_code);
                $product_code_array[$value['item_category_code']] = $last_product_code;
            }
        }

        foreach ($postData['tag_products'] as $key => $value) {

            $item_category_code = explode('-', $value['product_code']);
            $item_category_code = reset($item_category_code);
            //print_r( $product_code_array[$value['item_category_code']] );
            if (isset($product_code_array[$value['item_category_code']])) {
                $postData['tag_products'][$key]['product_code'] = $item_category_code . '-' . ($product_code_array[$value['item_category_code']] + 1);
                $product_code_array[$value['item_category_code']] = ($product_code_array[$value['item_category_code']] + 1);
            }
            // echo"<pre>";
            // print_r($product_code_array[$value['item_category_code']]);
            // echo"</pre>";
        }
//print_r($postData);die;
        $_POST = $postData;
        return true;
    }
/*
    public function product_code_max_count_store(){
        $result=$this->Tag_products_model->product_code_max_count_store();
        print_r($result);die;
    }*/

}
