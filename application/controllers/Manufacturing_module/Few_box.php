<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class Few_box extends CI_Controller {
	public function __construct(){
		parent::__construct();
    if(empty($this->session->userdata('user_id'))){
      redirect(ADMIN_PATH . 'auth/logout');
    }
    $this->breadcrumbs->push("Manufacturing Module", "Manufacturing_Dashboard");
  	$this->breadcrumbs->push("Dashboard", "Manufacturing_Dashboard");
    $this->load->config('admin_validationrules', TRUE);
  	$this->load->model(array('Manufacturing_module/Few_box_model','User_access_model','Category_model','sale_master/parent_category_model'));

	}

  public function index($department_id=''){
    $data['page_title'] = 'View Few weightt';
    $data['display_name'] = 'Few weight Box';
    $user_id=$this->session->userdata('user_id');
    $dep_id=$this->User_access_model->find($user_id); 
    $department_id=$dep_id['department_id'];
    $data['department_id'] = $department_id;  
    $this->breadcrumbs->push("Dashboard", "Manufacturing_Dashboard");
      // $this->view->render('Manufacturing_module/repair_product/index',$data);
    $list=$this->input->post('list');
    if($list !=""){
      echo json_encode($this->generate_data_table($department_id));
    }else{
       
      $this->view->render('Manufacturing_module/few_box/index',$data);
    }
  }

  public function create(){
    $this->breadcrumbs->push("Add Few weight", "Few_box/create");
    $data['page_title'] = "ADD Few weight";
    $data['product_list'] = $this->Few_box_model->get_product();
    $this->view->render('Manufacturing_module/few_box/create',$data);
  }

  public function store(){
    $data = array();
    $validationResult = $this->Few_box_model->validatepostdata();    
    if($validationResult===FALSE){
      $data['status']= 'failure';
      $data['data']= '';
      $data['error'] = array(
        'product'=>strip_tags(form_error('few_box[product]')),
        'few_weight'=>strip_tags(form_error('few_box[few_weight]'))
      );
    }else{
      $data['status']= 'success';
      $data['data']= '';
      $data['user_id'] = $this->Few_box_model->store();
    }

    echo json_encode($data);
  }

  public function update(){
    $data = array();
    $validationResult = $this->Few_box_model->validatepostdata();
    //print_r($validationResult);die;
    if($validationResult===FALSE){
      $data['status']= 'failure';
      $data['data']= '';
      $data['error'] = array(
        'few_weight'=>strip_tags(form_error('few_box[few_weight]'))
        //'product'=>strip_tags(form_error('few_box[product]')),
      );
    }else{
      $data['status']= 'success';
      $data['data']= '';
      $data['user_id'] = $this->Few_box_model->update();
    }
    echo json_encode($data);
  }

  public function edit($id){
    $this->breadcrumbs->push("Edit Few weight", "Few_box/edit");
    $data['page_title'] = "EDIT Few weight";
    $data['product_list'] = $this->Few_box_model->get_product();
    $data['few_box'] = $this->Few_box_model->find($id);
    //print_r($data['few_box']);die;
    $this->view->render('Manufacturing_module/few_box/edit',$data);
  }
  public function delete(){

    $result = $this->Few_box_model->delete($_POST['id']);
    echo json_encode($result);
  }
       
  public function few_weight_details()
    {
        $data=array();
        $id=$this->input->post('id');     
        $user_id=$this->session->userdata('user_id');
        $dep_id=$this->User_access_model->find($user_id); 
        $department_id=$dep_id['department_id'];
        $data['dep_id'] = $department_id; 
        //print_r($id);die;
        $data['few_weight_details']=$this->Few_box_model->few_weight_details($id);
        $response['html'] = $this->load->view('Manufacturing_module/few_box/few_weight_details',$data, true);
        echo json_encode($response);
    }

  private function generate_data_table($department_id){
    $filter_status =@$_REQUEST['order'][0];
    $search=@$_REQUEST['search']['value'];
    $status = array();
    $result = $this->Few_box_model->get($filter_status,$status,$_REQUEST,$search,$limit=true,$department_id);
    $totalRecords = $this->Few_box_model->get($filter_status,$status,$_REQUEST,$search,$limit=false,$department_id);
    //print_r($result);
    if (!empty($result)) {
      $data = $this->get_datatable_rows($result,$department_id);
    }else{
      $data = $this->get_null_row($department_id);
    }
    return $this->return_json($totalRecords,$data);
  }
  private function get_datatable_rows($result,$department_id){

    $start=$_REQUEST['start'];
    foreach ($result as $key => $value) {
        $start++;      
        $few_weight_use=$this->Few_box_model->few_weight_details($value["product_name"]);    
      
        if(empty($few_weight_use[0]['few_wt'])):
          $onclick ='onclick=Delete_record("'.$value['id'].'",this,"Few_box")';
          //echo"not empty";
        else:
          //echo"empty";
          $onclick ="disabled";        
        endif;
        // die;
   
        $button_html  = '<span style="float:right"> <a href="'.ADMIN_PATH.'Few_box/edit/'.$value["id"].'"  class="btn btn-link edit_link small loader-hide btn-sm" id="confim_'.$value['id'].'">EDIT</a>&nbsp;&nbsp;<a  '.$onclick.'  class="btn btn-link delete_link small loader-hide btn-sm" id="reject_'.$value['id'].'" >DELETE</a></span>';
        $data[$key][] = '<input type="hidden" name="rp[ready_product_id]['.$value["id"].']"   class="form-control" value="'.$value['id'].'"><span style="float:right" id="'.$value['product_name'].'">'.$start.'</span>';   
        $data[$key][] ='<input type="hidden"   name="rp[product_code]['.$value["id"].']"class="form-control" value="'.$value['product_name'].'">'.$value['product_name'];
        $data[$key][] ='<input type="hidden"   name="rp[product_code]['.$value["id"].']"class="form-control" value="'.$value['weight'].'">'.$value['weight'];
         $data[$key][]   = $button_html; 
            

    }
    return $data;
  }

  private function get_null_row($department_id){    
    $data[0][] = ['No data found'];
    $data[0][] = [];
    $data[0][] = [];
    $data[0][] = [];
    $data[0][] = [];
    $data[0][] = [];
    $data[0][] = [];
    
    return $data;
  }
  private function return_json($totalRecords,$data){
    $json_data = array(
          "draw" => intval($_REQUEST['draw']),
          "recordsTotal" => intval($totalRecords),
          "recordsFiltered" => intval($totalRecords),
          "data" => $data
      );
    return $json_data;
  }


}
?>