<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class Engaged_karigar_list extends CI_Controller {
  public function __construct() {
    parent::__construct();
    if(empty($this->session->userdata('user_id'))){
      redirect(ADMIN_PATH . 'auth/logout');
    }
    $this->breadcrumbs->push("Manufacturing Module", "Manufacturing_Dashboard");
    $this->breadcrumbs->push("Dashboard", "Manufacturing_Dashboard");
    $this->load->model(array('Manufacturing_module/Engaged_karigar_list_model','Karigar_model','User_access_model','Manufacturing_module/Department_model'));
   }

  public function index($type=''){
    $department_id=$this->session->userdata('department_id');
    $department_name=$this->Department_model->find($department_id); 
    $this->breadcrumbs->push($department_name['name']." - Karigar Engaged List ", "Manufacturing_Engaged_karigar_list");
    $data['page_title'] = "MANUFACTURING ENGAGED KARIGAR LIST";
    $data['display_title'] = $department_name['name']." - Karigar Engaged List ";
    $list=$this->input->post('list');
    $data['dep_id']=$department_id;
    if($list !="")
    {
      echo json_encode($this->generate_data_table($department_id));
    }else{
      $this->view->render('Manufacturing_engaged_kariger_list/index',$data);
    }
  }
  public function delete(){
      $result = $this->Engaged_karigar_list_model->delete($_POST['id']);
      echo json_encode($result);
  }
  private function generate_data_table($department_id){

    $_POST['page_title']="PREPARE PRODUCT LIST";
    $filter_status =@$_REQUEST['order'][0];
    $status = array('p.created_at','p.product_code');
    $search=@$_REQUEST['search']['value'];
    //print_r($department_id);die;
    $result = $this->Engaged_karigar_list_model->get($filter_status,$status,$_REQUEST,$search,$limit=true,$karigar_id='',$department_id);    
    $totalRecords = $this->Engaged_karigar_list_model->get($filter_status,$status,$_REQUEST,$search,$limit=false,$karigar_id='',$department_id);
    if (!empty($result)) {
        /*  if($department_id != '2'){
          }else{
            $data = $this->get_bombay_datatable_rows($result,);
          }*/
          $data = $this->get_datatable_rows($result,$department_id);
    }else{
      $data = $this->get_null_row($department_id);
    }
    return $this->return_json($totalRecords,$data);
  }
  private function get_datatable_rows($result,$department_id){
    foreach ($result as $key => $value) {
      if($department_id !='2' && $department_id != '3' && $department_id != '6'&& $department_id != '10'){
          $weioght_per_qty =($value['to_weight']+$value['from_weight']/2) * $value['quantity'];
          if(!empty($value['kmm_ids']))
            $rp_quantity = $this->Engaged_karigar_list_model->receive_quantity($value['kmm_ids']);
          else
            $rp_quantity = 0;
          $rec_qty =$rp_quantity['weight'];
          $data[$key][] =$value['karigar_name'];
          $data[$key][] ='<span style="float:right">'. $value['quantity'].'</span>'; //count($pending_product);
          $data[$key][] ='<span style="float:right">'. $rp_quantity['pcs'].'</span>';
          $data[$key][] ='<span style="float:right">'. $value['approx_weight'].'</span>';
          $data[$key][] ='<span style="float:right">'. $rec_qty.'</span>'; 
          $Balance_Wt=$value['approx_weight']-$rec_qty;
          $data[$key][] ='<span style="float:right">'. $Balance_Wt.'</span>'; 

          $button_html = '<a href="'.ADMIN_PATH.'Manufacturing_engaged_karigar/reprint_order/'.$value['karigar_id'].'"><button class="btn btn-link view_link loader-hide  btn-sm" name="commit" type="button" >View</button></a>&nbsp;<a onclick=Delete_record("'.$value['karigar_id'].'",this,"Engaged_karigar_list")>';
          $data[$key][] =$button_html;
      }else{
              $weioght_per_qty =($value['to_weight']+$value['from_weight']/2) * $value['quantity'];
          if(!empty($value['kmm_ids']))
            $rp_weight = $this->Engaged_karigar_list_model->receive_weight($value['kmm_ids']);
          else
            $rp_weight = 0;
          $rec_wt =$rp_weight['receive_wt'];
          $data[$key][] =$value['karigar_name'];
          $data[$key][] ='<span style="float:right">'. $value['total_weight'].'</span>'; //count($pending_product);
          $data[$key][] ='<span style="float:right">'. $rp_weight['receive_wt'].'</span>';
          $Balance_Wt=$value['total_weight']-$rec_wt;
          $data[$key][] ='<span style="float:right">'. $Balance_Wt.'</span>'; 

          $button_html = '<a href="'.ADMIN_PATH.'Manufacturing_engaged_karigar/reprint_order/'.$value['karigar_id'].'"><button class="btn btn-link view_link loader-hide  btn-sm" name="commit" type="button" >View</button></a>&nbsp;<a onclick=Delete_record("'.$value['karigar_id'].'",this,"Engaged_karigar_list")>';
          $data[$key][] =$button_html;

      }
     
    }
    return $data;
  }

  private function get_null_row($department_id){
      if($department_id !='2'):
        $data[0][] = ['No data found'];
        $data[0][] = [];
        $data[0][] = [];
        $data[0][] = [];
        $data[0][] = [];
        $data[0][] = [];
        $data[0][] = [];
      else:
        $data[0][] = ['No data found'];
        $data[0][] = [];
        $data[0][] = [];
        $data[0][] = [];
        $data[0][] = [];
        
      endif;   


        return $data;
  }
  private function return_json($totalRecords,$data){
    $json_data = array(
          "draw" => intval($_REQUEST['draw']),
          "recordsTotal" => intval($totalRecords),
          "recordsFiltered" => intval($totalRecords),
          "data" => $data
      );
    return $json_data;
  }
/*  public function reprint_order1($karigar_id){
    $this->breadcrumbs->push("Manufacturing Engaged Karigar Details", "Manufacturing_Engaged_karigar_list/reprint_order");
    $data['page_title'] = "MANUFACTURING ENGAGED KARIGAR DETAILS";
    $karigar_details = $this->Karigar_model->find($karigar_id);
    $data['display_title'] = "Product Details of karigar ".$karigar_details['name'];
    $data['result'] = $this->Engaged_karigar_list_model->get('','','','',$limit=false,$karigar_id);
    $this->view->render('Manufacturing_engaged_kariger_list/show',$data);
  }
*/

  public function reprint_order($karigar_id){
    $department_id=$this->session->userdata('department_id');
    $department_name=$this->Department_model->find($department_id); 
    $this->breadcrumbs->push($department_name['name']." - Karigar Engaged List ", "Manufacturing_engaged_karigar");
    $this->breadcrumbs->push("Manufacturing Engaged Karigar Details", "Manufacturing_Engaged_karigar_list/reprint_order/".$karigar_id);

    $data['page_title'] = "MANUFACTURING ENGAGED KARIGAR DETAILS";
    $karigar_details = $this->Karigar_model->find($karigar_id);
    $data['display_title'] = $department_name['name']." - Product Details of karigar - ".$karigar_details['name'];
    $data['karigar_id'] = $karigar_id; 
    $user_id=$this->session->userdata('user_id');
    $dep_id=$this->User_access_model->find($user_id); 

    $data['dep_id']=$department_id;
    $list=$this->input->post('list');
    if($list !="")
    {  
        echo json_encode($this->generate_data_reprint_order_table($karigar_id,$department_id));
    }else{
       $this->view->render('Manufacturing_engaged_kariger_list/show',$data);
    }
  }

  private function generate_data_reprint_order_table($karigar_id,$department_id){
    $filter_status =@$_REQUEST['order'][0];
    $status = array();
    $search=@$_REQUEST['search']['value'];

    $result = $this->Engaged_karigar_list_model->get($filter_status,$status,$_REQUEST,$search,$limit=true,$karigar_id,$department_id);
    $totalRecords = $this->Engaged_karigar_list_model->get($filter_status,$status,$_REQUEST,$search,$limit=false,$karigar_id,$department_id);

    if (!empty($result)) {

     
        foreach ($result as $key => $value) {           
            $data[$key][0] =$value["k_name"];
            $data[$key][1] =$value["name"];
            $data[$key][2] ='<span style="float:right">'. $value["order_id"].'</span>';
            $data[$key][3] ='<span style="float:right">'. $value['karigar_engaged_date'].'</span>';
            $data[$key][4] ='<span style="float:right">'. $value['delivery_date'].'</span>';
            if($value["department_id"] != '2' && $value["department_id"] != '3' && $value["department_id"] != '6'&& $value["department_id"] != '10'):
            $data[$key][5] ='<span style="float:right">'. $value["quantity"].'</span>';
            $data[$key][6] ='<span style="float:right">'. $value["approx_weight"].'</span>';  
            else:
            $data[$key][5] ='<span style="float:right">'. $value["total_weight"].'</span>';
            $data[$key][6] ='<span style="float:right">'. $value["weight"].'</span>';
            endif;  
            $data[$key][7] ='<a target="_blank" href="'.ADMIN_PATH.'Karigar_order_list/re_print_order/'.$value['kmm_id'].'"><button class="btn btn-link edit_link small loader-hide  btn-sm" name="commit" type="button">Print</button></a>';
       
        }
    }else{
        $data[0][0] = ['No data found'];
        $data[0][1] = [];
        $data[0][2] = [];
        $data[0][3] = [];
        $data[0][4] = [];
        $data[0][5] = [];
        $data[0][6] = [];
        $data[0][7] = [];
    }

     $json_data = array(
          "draw" => intval($_REQUEST['draw']),
          "recordsTotal" => intval($totalRecords),
          "recordsFiltered" => intval($totalRecords),
          "data" => $data
      );
    return $json_data; 
  }
}
  ?>
