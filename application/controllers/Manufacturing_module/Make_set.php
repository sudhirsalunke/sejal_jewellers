<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Make_set extends CI_Controller {
	public function __construct(){
		parent::__construct();
    if(empty($this->session->userdata('user_id'))){
      redirect(ADMIN_PATH . 'auth/logout');
    }
    $this->breadcrumbs->push("Manufacturing Module", "Manufacturing_Dashboard");
  	$this->breadcrumbs->push("Dashboard", "Manufacturing_Dashboard");
  	$this->load->model(array('Manufacturing_module/Make_set_model','sale_master/Parent_category_model','Manufacturing_module/Hallmarking_model','Manufacturing_module/Tag_products_model','Karigar_model','User_access_model'));
	}

	public function index(){
    $user_id=$this->session->userdata('user_id');
    $dep_id=$this->User_access_model->find($user_id); 
    $department_id=$dep_id['department_id'];
		if($this->input->post('type')=='html'){
			$_GET['status'] = 'receive';
			if($this->input->post('id')!='')
				$html['data'] = $this->Make_set_model->find($this->input->post('id'));
			  $html['result'] = $this->Hallmarking_model->get('','',array('length' => '100','start' => $_POST['page_id']*100),'',TRUE,'',$this->input->post('id'),$department_id);
      echo $this->load->view('Manufacturing_module/Make_set/html',$html,TRUE);
		}
		else{
			$data['page_title'] = 'View Make Set';
			$data['display_name'] = 'All Make Set';
			$this->breadcrumbs->push("Dashboard", "Manufacturing_Dashboard");
			$this->breadcrumbs->push("All Make Set", "Make_set");
			$list=$this->input->post('list');
			if($list !=""){
	      echo json_encode($this->generate_data_table());
	    }else{
			$this->view->render('Manufacturing_module/Make_set/index',$data);
	    }
		}
	}

	private function generate_data_table(){
    $filter_status =@$_REQUEST['order'][0];
    $search=@$_REQUEST['search']['value'];
    $status = array();
    $result = $this->Make_set_model->get($filter_status,$status,$_REQUEST,$search,$limit=true);
    $totalRecords = sizeof($this->Make_set_model->get($filter_status,$status,$_REQUEST,$search,$limit=false));
    if (!empty($result)) {
      $data = $this->get_datatable_rows($result);
    }else{
      $data = $this->get_null_row();
    }
    return $this->return_json($totalRecords,$data);
  }
  private function get_datatable_rows($result){
    //print_r($result); die();
    foreach ($result as $key => $value) {
      $data[$key][0] = $key+1;
      $data[$key][1] =$value['id'];
      $data[$key][2] =date('d-m-Y H:i:s',strtotime($value["created_at"]));
      $data[$key][3] = '<a href="'.ADMIN_PATH.'Make_set/edit/'.$value["id"].'"  class="btn btn-link edit_link small loader-hide  btn-sm">EDIT</a>&nbsp  &nbsp <a onclick=Delete_record("'.$value['id'].'",this,"Make_set") class="btn btn-link delete_link small loader-hide  btn-sm" id="reject_'.$value['id'].'">DELETE</a>&nbsp  &nbsp '
//              . '<a onclick="open_product_ready_modal('.$value['id'].')"><button class="btn btn-primary small loader-hide  btn-sm" name="commit" type="button">Is Product ready?</button></a>'
              . '<a href="'.ADMIN_PATH.'Make_set/show/'.$value["id"].'"><button class="btn btn-link view_link small loader-hide  btn-sm" name="commit" type="button">View</button></a>';
    }
    return $data;
  }
  private function get_null_row(){
    $data[0][0] = ['No data found'];
    $data[0][1] = [];
    $data[0][2] = [];
    $data[0][3] = [];
    return $data;
  }
  private function return_json($totalRecords,$data){
    $json_data = array(
          "draw" => intval($_REQUEST['draw']),
          "recordsTotal" => intval($totalRecords),
          "recordsFiltered" => intval($totalRecords),
          "data" => $data
      );
    return $json_data;
  }

	public function create(){
		$data['page_title'] = 'Make Set';
    $this->breadcrumbs->push("Dashboard", "Manufacturing_Dashboard");
    $this->breadcrumbs->push("View Make Set", "Make_set");
    $user_id=$this->session->userdata('user_id');
    $dep_id=$this->User_access_model->find($user_id); 
    $department_id=$dep_id['department_id'];
    $data['display_name'] = 'Make Set';
    $_GET['status'] = 'receive';
    $data['result']=$this->Tag_products_model->get('','','','','',true,$department_id);
    $data['get_filters']=$this->Parent_category_model->get();
		$this->view->render('Manufacturing_module/Make_set/create',$data);
	}

	public function store(){
    //$data = $this->Make_set_model->validatepostdata();
    $data['status'] = 'success';
    if($data['status'] == 'success'){
      $postdata = $_POST;
      $result = $this->Make_set_model->store($postdata);
    }
    echo json_encode($data);
  }

  public function update(){
    $data = $this->Make_set_model->validatepostdata();
    if($data['status'] == 'success'){
      $postdata = $_POST;
      $result = $this->Make_set_model->update($postdata);
    }
    echo json_encode($data);
  }

  public function edit($id){
  	$data['page_title'] = "EDIT Make Set";
    $this->breadcrumbs->push("Dashboard", "Manufacturing_Dashboard");
    $this->breadcrumbs->push("View Make Set", "Make_set");
    $data['display_name'] = 'Make Set';
    $_GET['status'] = 'receive';
    $data['id'] = $id;
    //$data['result'] = $this->Hallmarking_model->get('','',array('length' => '100','start' => '0'),'',TRUE,'',$id);
    $data['result']=$this->Tag_products_model->get();
    $data['data'] = $this->Make_set_model->find($id);
    $this->view->render('Manufacturing_module/Make_set/edit',$data);
  }

  public function delete(){
    $result = $this->Make_set_model->delete($_POST['id']);
    echo json_encode($result);
  }

  public function store_image() {
    // $msqr_id=$_POST['msqr_id'];
    $tag_id=$_POST['tag_id'];
    $files=array();
    $files['file_0']['name'] = $_FILES['prepare']['name'][$tag_id];
    $files['file_0']['type'] = $_FILES['prepare']['type'][$tag_id];
    $files['file_0']['tmp_name']= $_FILES['prepare']['tmp_name'][$tag_id];
    $files['file_0']['size']= $_FILES['prepare']['size'][$tag_id];
    $files['file_0']['error']= $_FILES['prepare']['error'][$tag_id];

    $data = uploadFiles(array('tag_images'),$files);

    $update_data=array();
    $update_data['image'] ="uploads/tag_images/".$data[0]['file_name'];
    $result = $this->Make_set_model->update_tag($update_data,$tag_id);
   // $result = $this->Make_set_model->update_msqr($update_data,$msqr_id);
    $result['image']= ADMIN_PATH.$update_data['image'];
    
    echo json_encode($result);
    
  }
  
  public function show($id){
    $list=$this->input->post('list');
    if($list !=""){
        $filter_status =@$_REQUEST['order'][0];
        $search=@$_REQUEST['search']['value'];
        $status = array();
        //$result = $this->Hallmarking_model->get('','',array('length' => '100','start' => '0'),'',TRUE,'',$id);
        // print_r($result);exit;
        // $totalRecords = sizeof($this->Hallmarking_model->get('','',array('length' => '100','start' => '0'),'',TRUE,'',$id));
        $result = $this->Make_set_model->get_list($id);
        $totalRecords = sizeof($this->Make_set_model->get_list($id));
       // print_r($result);die;
        if (!empty($result)) {
          foreach ($result as $key => $value) {
            if (empty($value['image'])) {
            $img = '';
            }else{
             $img = "<img src='".ADMIN_PATH.$value['image']."' id='img_".$value['id']."' style='height:90px;width:150px'>";
            }
            //$read_product = $this->Make_set_model->check_ready_products($value['id']);
            $data[$key][0] = '<input type="checkbox" value="'.$value['id'].'" name="prepare[set_id]['.$value['id'].']"/> <input type="hidden" name="prepare[ms_id]['.$value['id'].']" value="'.$value['make_set_id'].'">';
           // $data[$key][1] =$value['order_id'];
            $data[$key][1] =$value['name'];
            $data[$key][2] ="<label id='pr_name_".$value['id']."'>".$value['product_code'].'-'.$value['tp_id']."</label>";
            $data[$key][3] ='<input type="textbox" value="'.$value['qty_remaining'].'" id="newqty_'.$value['id'].'" name="prepare[quantity]['.$value['id'].']"/><input type="hidden" value="'.$value['qty_remaining'].'" id="qty_'.$value['id'].'">';
            $data[$key][4] = '<input type="file" name="prepare['.$value['id'].']" onchange="save_msqr_image('.$value['id'].')">'.$img;
            
            
            
          }
        } else{
           $data[0][0] = ['No data found'];
    $data[0][1] = [];
    $data[0][2] = [];
    $data[0][3] = [];
    $data[0][4] = [];
        }
        $json = $this->return_json($totalRecords,$data);
        echo json_encode($json);
    }else{
  	    $data['page_title'] = 'Make Set Products';
        $data['display_name'] = 'Make Set Products';
        $data['id'] = $id;
        $this->breadcrumbs->push("Dashboard", "Manufacturing_Dashboard");
        $this->breadcrumbs->push("Make Set Products", "Make_set");
        $data['all_karigars'] = $this->Karigar_model->get('','','','',true);
        $this->view->render('Manufacturing_module/Make_set/show',$data);
    }
//    $data['data'] = $this->Hallmarking_model->get('','',array('length' => '100','start' => '0'),'',TRUE,'',$id);
//    $this->view->render('Manufacturing_module/Make_set/show',$data);
  } 

}
?>