<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Prepare_karigar extends CI_Controller {
	public function __construct(){
		parent::__construct();
	    if(empty($this->session->userdata('user_id'))){
	      redirect(ADMIN_PATH . 'auth/logout');
	    }
	    $this->breadcrumbs->push("Manufacturing Module", "Manufacturing_Dashboard");
    	$this->breadcrumbs->push("Dashboard", "Manufacturing_Dashboard");
    	$this->load->model(array('Manufacturing_module/Prepare_karigar_model','Manufacturing_module/Karigar_order_model','Manufacturing_module/Manufacturing_department_order_model'));
    	$this->load->config('admin_validationrules', TRUE);
    	$this->load->library('m_pdf');
	}
	public function store(){
		$result = $this->Prepare_karigar_model->validation();
		if($result['status'] == 'success'){
			$result = $this->Prepare_karigar_model->store($_POST);
			// makeDirectory('prepare_karigar');
	  //       $pdfFilePath = dirname(dirname(dirname(dirname(__FILE__)))) . "/uploads/prepare_karigar/order_".$_POST['karigar_id'].".pdf";
	  //       $html=$this->load->view('Manufacturing_module/Karigar_order/print', $result,true);
	  //       $this->m_pdf->pdf->WriteHTML($html);
	  //       ob_clean();
	  //       $this->m_pdf->pdf->Output($pdfFilePath, "F");
	        $final_result['status'] = 'success';
	        $final_result['data']['url'] = ADMIN_PATH.'Karigar_order_list/show/'.$result;
      		echo json_encode($final_result);die;
		}
		echo json_encode($result);
	}
	public function change()//code to change kariger
	{
		
		$result = $this->Prepare_karigar_model->chng_kar_validation();
		if($result['status'] == 'success'){
			$result = $this->Prepare_karigar_model->change($_POST);
		}	
		echo json_encode($result);
	}
	public function download_pdf($karigar_id){
	    $pdfFilePath = dirname(dirname(dirname(dirname(__FILE__)))) . "/uploads/prepare_karigar/order_".$karigar_id.".pdf";
	    $name = "order_".$karigar_id.".pdf";
	    if (file_exists($pdfFilePath)) { 
	      header('Content-type: application/force-download'); 
	      header('Content-Disposition: attachment; filename='.$name); 
	      readfile($pdfFilePath);
	    }
  }
}