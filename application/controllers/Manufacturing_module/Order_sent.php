<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class Order_sent extends CI_Controller {
	public function __construct(){
		parent::__construct();
	    if(empty($this->session->userdata('user_id'))){
	      redirect(ADMIN_PATH . 'auth/logout');
	    }
	    $this->breadcrumbs->push("Manufacturing Module", "Manufacturing_Dashboard");
    	$this->breadcrumbs->push("Dashboard", "Manufacturing_Dashboard");
    	$this->load->model(array('Manufacturing_module/Karigar_order_model','User_access_model','Manufacturing_module/Department_model'));
    	$this->load->config('admin_validationrules', TRUE);
	}
	public function index(){
		$data['page_title'] = "Order Sent";
        $department_id=$this->session->userdata('department_id');
    	$department_name=$this->Department_model->find($department_id); 
	 	$this->breadcrumbs->push($department_name['name']." - order sent to karigar", "Order Sent");
	 	$data['display_title'] = $department_name['name']." - order sent to karigar";
	      

        $data['dep_id']=$department_id;
	    $list=$this->input->post('list');
	    if($list !="")
	    {
	      echo json_encode($this->generate_data_table($department_id));
	    }else{
	      $this->view->render('Manufacturing_module/order_sent/index',$data);
	    }
	}
	private function generate_data_table($department_id){
		$filter_status =@$_REQUEST['order'][0];
	    $status = array('name','code');
	    $search=@$_REQUEST['search']['value'];

	    $result = $this->Karigar_order_model->get($filter_status,$status,$_REQUEST,$search,$limit=true,$status='1','',$department_id);
	
	    $totalRecords = $this->Karigar_order_model->get($filter_status,$status,$_REQUEST,$search,$limit=false,$status='1','',$department_id);
	   
	    if (!empty($result)) {
        foreach ($result as $key => $value) {
            $data[$key][] ='<span style="float:right">'.$value["order_id"].'</span>';
            // $data[$key][] =$value["product_code"];
            $data[$key][] ='<span style="float:right">'.$value["order_date"].'</span>';
            $data[$key][] ='<span style="float:right">'.$value["delivery_date"].'</span>';
            $data[$key][] =$value["karigar_name"];
            $data[$key][] =$value["sub_cat_name"];
            $data[$key][] ='<span style="float:right">'. $value["from_weight"].' - '.$value["to_weight"].'</span>';
            if( $value["department_id"] !='2' && $value["department_id"] != '3' && $value["department_id"] !='6' && $value["department_id"] !='10'):
           		$data[$key][] ='<span style="float:right">'. $value["quantity"].'</span>';
            	$data[$key][] ='<span style="float:right">'. $value["approx_weight"].'</span>';
            else:
            	$data[$key][] ='<span style="float:right">'. $value["total_weight"].'</span>';
            	$data[$key][] ='<span style="float:right">'. $value["weight"].'</span>';
            endif;	
            // $data[$key][] =$value["rp_quantity"];
		        //$button_html = '<a onclick="open_mfg_order_modal('.$value["id"].')"><button class="btn btn-primary small loader-hide  btn-sm" name="commit" type="button">Receive Product</button></a>';
		        $button_html = '';
		     		$data[$key][] = $button_html;
        }
	    }else{
	        $data[0][] = ['No data found'];
	        $data[0][] = [];
	        $data[0][] = [];
	        $data[0][] = [];
	        $data[0][] = [];
	        $data[0][] = [];
	        $data[0][] = [];
	        $data[0][] = [];
	        $data[0][] = [];
	        // $data[0][] = [];
	        // $data[0][] = [];
	    }
     $json_data = array(
          "draw" => intval($_REQUEST['draw']),
          "recordsTotal" => intval($totalRecords),
          "recordsFiltered" => intval($totalRecords),
          "data" => $data
      );
	    return $json_data; 
	}
	public function store(){
		$result = $this->Karigar_order_model->validate_quantity();
		if($result['status'] == 'success'){
			$postdata = $_POST;
			$postdata['date'] = date('Y-m-d');
			$postdata['module'] = '2';
			$postdata['status'] = '4';
			$postdata['quantity'] = $_POST['kmm_quantity'];
			unset($postdata['kmm_quantity']);
			$data = $this->Karigar_order_model->get_department_id($_POST['kmm_id']);
			$postdata['order_id'] = $data['id'];
			$postdata['department_id'] = $data['department_id'];
			$result = $this->Karigar_order_model->store($postdata);
		}
		echo json_encode($result);
	}
}