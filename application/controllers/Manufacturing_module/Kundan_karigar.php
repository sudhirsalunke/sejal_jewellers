<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class Kundan_karigar extends CI_Controller {
	public function __construct(){
		parent::__construct();
    if(empty($this->session->userdata('user_id'))){
      redirect(ADMIN_PATH . 'auth/logout');
    }
    $this->breadcrumbs->push("Manufacturing Module", "Manufacturing_Dashboard");
    $this->breadcrumbs->push("Dashboard", "Manufacturing_Dashboard");
    $this->load->model(array('Manufacturing_module/Kundan_karigar_model','User_access_model','Karigar_model','Manufacturing_module/Department_model','Manufacturing_module/Kundan_QC_model'));
    $this->load->config('admin_validationrules', TRUE);
  }
  // public function store(){
  //  $result = $this->Prepare_karigar_model->validation();
  //  if($result['status'] == 'success'){
  //    $result = $this->Prepare_karigar_model->store($_POST);
  //  }
  //  echo json_encode($result);
  // }
  public function index(){
    $department_id=$this->session->userdata('department_id');
    $department_name=$this->Department_model->find($department_id);
		if(isset($_GET['status']) && $_GET['status'] == 'pending')
    {
      $data['display_name'] = "Not Sent To Karigar";
      $this->breadcrumbs->push($department_name['name']." - All Pending Orders", "Received_orders");
    }
    elseif(isset($_GET['status']) && $_GET['status'] == 'complete'){
      $this->breadcrumbs->push($department_name['name']." - All Received Orders", "Received_orders");
      $data['display_name'] = $department_name['name']." - Sent To Karigar";
     }
    $data['page_title'] = 'Not Sent To Karigar';
    $list=$this->input->post('list');
    if($list !="")
    {
      echo json_encode($this->generate_data_table($department_id));
    }else{
      $this->view->render('Manufacturing_module/Kundan_karigar/index',$data);
    }
	}
	public function generate_data_table($department_id){
		$filter_status =@$_REQUEST['order'][0];
    $search=@$_REQUEST['search']['value'];
    $status = array();
    $result = $this->Kundan_karigar_model->get($filter_status,$status,$_REQUEST,$search,$limit=true,$department_id);
    $totalRecords = $this->Kundan_karigar_model->get($filter_status,$status,$_REQUEST,$search,$limit=false,$department_id);
    if (!empty($result)) {
      $data = $this->get_datatable_rows($result);
    }else{
      $data = $this->get_null_row();
    }
    return $this->return_json($totalRecords,$data);
	}
	private function get_datatable_rows($result){
    foreach ($result as $key => $value) {
         if($_GET['status'] != 'complete'){
       $data[$key][] ='<div class="checkbox checkbox-purpal"><input type="checkbox" class="from-control checkbox_ctn" value="'.$value["id"].'" name="id[]"><label></label></div>';
     }

      $data[$key][] =  $value['km_name'];
      if($_GET['status'] == 'pending'){
        $data[$key][] =$value['issue_voucher'];
      }else{
        $data[$key][] = $value['product_code'];
        // $data[$key][] = $value['from_weight'].'-'.$value['to_weight'];
        // $data[$key][] = (($value['from_weight'] + $value['to_weight'])/2 * $value['quantity']);
        // $data[$key][] =$value['issue_voucher'];
      }
      if($_GET['status'] == 'complete'){
         $enc_id=$value['id'];
        // $data[$key][] =$value['pcs'].'<input type="hidden" id="quantity_'.$value['id'].'" name="qnty" value="'.$value['quantity'].'">';

         // $data[$key][] = '<input type="hidden"  name="product[ids]['.$value["id"].']" value="'.$value['id'].'">'.'<span style="float:right">'. $value["pcs"].'</span>';
        //$data[$key][] ='<span style="float:right">'.$value['quantity'].'</span>';
       /* $data[$key][] = '<input type="text" name="k_pcs_'.$value['id'].'" id="k_pcs_'.$value['id'].'"  class="form-control" ><span class="text-danger col-sm-12" id="k_pcs'.$value['id'].'"></span>';*/
        /*$data[$key][] = '<input type="text" name="k_gold_'.$value['id'].'" id="k_gold_'.$value['id'].'" class="form-control"><span class="text-danger col-sm-12" id="k_gold'.$value['id'].'"></span>';*/

        $data[$key][] ='<input type="hidden" class="form-control gr_wt_cal"  value="'.$value['tg_gr_wt'].'"><input type="hidden" class="form-control net_wt_cal"  value="'.$value['net_wt'].'"><input type="hidden"  name="product[ids]['.$value["id"].']" value="'.$value['id'].'"><input type="text" name="product[k_gold]['.$value['id'].']" class="form-control k-gold k_gold" placeholder="Enter K Gold" id="k_gold_'.$enc_id.'" onkeyup="kundan_karigar_cal_grNetAmt(this)"  ><span class="text-danger col-sm-12" id="k_gold_'.$value['id'].'_error"></span>';


        $data[$key][] = '<input type="text" name="product[kundan_pc]['.$value['id'].']" class="form-control k-gold" placeholder="Enter K Pcs" id="kundan_pc_'.$enc_id.'"  onkeyup="calculate_amt(this)" ><span class="text-danger col-sm-12" id="kundan_pc_'.$value['id'].'_error"></span>';

        $data[$key][] = ' <input type="text" onkeyup="calculate_amt(this)" name="product[kundan_rate]['.$value['id'].']" class="form-control k-gold" placeholder="Enter K @" id="kundan_rate_'.$enc_id.'" value="'.$value['kund_rate'].'" readonly>
        <span class="text-danger col-sm-12" id="kundan_rate_'.$value['id'].'_error"></span>';

        $data[$key][] = '<input type="text" name="product[kundan_amt]['.$value['id'].']" class="form-control k-gold" placeholder="Enter K Amt" id="kundan_amt_'.$enc_id.'" readonly><span class="text-danger col-sm-12" id="kundan_amt_'.$value['id'].'_error"></span>';

      $data[$key][] ='<input type="text" name="product[gross_wt]['.$value['id'].']" class="form-control k-gross_wt" id="gross_wt_'.$enc_id.'" value="'.$value['tg_gr_wt'].'" readonly>';

        $data[$key][] = ' <input type="text" name="product[net_wt]['.$value['id'].']" class="form-control k-net_wt" value="'.$value['net_wt'].'" placeholder="Enter Net Wt" id="net_wt_'.$enc_id.'"><span class="text-danger col-sm-12" id="net_wt_'.$value['id'].'_error"></span>';

      }else{
        $data[$key][] ='<span style="float:right">'.$value['quantity'].'</span>';
      }
      
	    if($_GET['status'] == 'pending'){
	    	$data[$key][] = '<a Onclick="assign_kundan_karigar('.$value['issue_voucher'].')"><button type="button" name="commit" class="btn btn-success small loader-hide  btn-sm">SEND</button></a>';
	    }
	    if($_GET['status'] == 'complete'){
        $data[$key][] = '<a Onclick="receive_kundan_karigar('.$value['id'].')"><button type="button" name="commit" class="btn btn-receive small loader-hide  btn-sm">Receive</button></a>';
	    	//$data[$key][] = '<a target="_blank" href="'.ADMIN_PATH.'/kundan_karigar/re_print/'.$value['issue_voucher'].'"><button type="button" name="commit" class="btn btn-primary small loader-hide  btn-sm">Print</button></a>';
	    }
    }
    return $data;
  }
  private function get_null_row(){
    $data[0][0] = ['No data found'];
    $data[0][1] = [];
    $data[0][2] = [];
    $data[0][3] = [];
    if($_GET['status'] == 'complete'){
      $data[0][4] = [];
      $data[0][5] = [];
      $data[0][6] = [];
      $data[0][7] = [];
      $data[0][8] = [];
      $data[0][9] = [];
      $data[0][10] = [];
    }
    // $data[0][4] = [];
    return $data;
  }
  private function return_json($totalRecords,$data){
    $json_data = array(
          "draw" => intval($_REQUEST['draw']),
          "recordsTotal" => intval($totalRecords),
          "recordsFiltered" => intval($totalRecords),
          "data" => $data
      );
    return $json_data;
  }
  public function update(){
  	$update_arr = array('status'=>1);
  	$result = $this->Kundan_karigar_model->update($update_arr,$_POST);
  	echo json_encode($result);
  }
  

  public function send_to_receive_products($id){
     if(empty($_POST['k_gold'])){
        echo json_encode(array('status'=>'failure1','error'=>'Please enter at least one k gold'));die;
      }else if(empty($_POST['kundan_pc'])){  
          echo json_encode(array('status'=>'failure1','error'=>'Please enter at least one k pcs'));die;
      }else if(empty($_POST['kundan_rate'])){  
          echo json_encode(array('status'=>'failure1','error'=>'Please enter at least one k @'));die;
      }else if(empty($_POST['kundan_amt'])){  
          echo json_encode(array('status'=>'failure1','error'=>'Please enter at least one k amount'));die;
      }else if(empty($_POST['net_wt'])){  
          echo json_encode(array('status'=>'failure1','error'=>'Please enter at least one net wt'));die;
      }else if($_POST['net_wt'] > $_POST['gross_wt']){  
          echo json_encode(array('status'=>'failure1','error'=>'Net Weight should not be greater then Gross wt'));die;
      }else{

          $data =$this->Kundan_karigar_model->validate();
          //print_r($data);
        if($data['status'] == 'failure'){
              echo json_encode($data);die;
            }else{
               $result = $this->Kundan_karigar_model->send_to_receive_products();       
                    echo json_encode($result);
            }

    }
      
  }
  public function send_to_receive_all_products(){
     if(empty(array_filter($_POST['product']['k_gold']))){
        echo json_encode(array('status'=>'failure1','error'=>'Please enter at least one k gold'));die;
      }else if(empty(array_filter($_POST['product']['kundan_pc']))){  
          echo json_encode(array('status'=>'failure1','error'=>'Please enter at least one k pcs'));die;
      }else if(empty(array_filter($_POST['product']['kundan_rate']))){  
          echo json_encode(array('status'=>'failure1','error'=>'Please enter at least one k @'));die;
      }else if(empty(array_filter($_POST['product']['kundan_amt']))){  
          echo json_encode(array('status'=>'failure1','error'=>'Please enter at least one k amount'));die;
      }else if(empty(array_filter($_POST['product']['net_wt']))){  
          echo json_encode(array('status'=>'failure1','error'=>'Please enter at least one net wt'));die;
      }else if(array_filter($_POST['product']['net_wt']) > array_filter($_POST['product']['gross_wt'])){  
          echo json_encode(array('status'=>'failure1','error'=>'Net Weight should not be greater then Gross wt'));die;
      }else{
            $data =$this->Kundan_karigar_model->receive_order();
            //print_r( $data);die;
            if($data['status'] == 'failure'){
              echo json_encode($data);die;
            }else{
            foreach ($_POST['product']['ids'] as $key => $value) {
              if(!empty($_POST['product']['kundan_pc'][$key]) && !empty($_POST['product']['k_gold'][$key])){
                  $_POST['id'] = $value;
                  $_POST['quantity']=1;
                  $_POST['kundan_pc'] = $_POST['product']['kundan_pc'][$key];
                  $_POST['k_gold'] = $_POST['product']['k_gold'][$key];
                  $_POST['kundan_rate'] = $_POST['product']['kundan_rate'][$key];
                  $_POST['kundan_amt'] = $_POST['product']['kundan_amt'][$key];
                  $_POST['net_wt'] = $_POST['product']['net_wt'][$key];
                  $_POST['gross_wt'] = $_POST['product']['gross_wt'][$key];
                  $result = $this->Kundan_karigar_model->send_to_receive_products();
                }
              }  
                echo json_encode($result);die;
            }              
    }
      
  }
  public function re_print($issue_voucher){
      $data['page_title'] = 'print_pdf_manuf';
      $data['result'] = $this->Kundan_karigar_model->reprint($issue_voucher);
      $data['chitti_no'] =$issue_voucher;
      $this->load->view('Manufacturing_module/Prepare_kundan_karigar_order/print',$data);
  }
}
