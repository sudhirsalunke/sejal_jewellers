<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Manufacturing_department_order extends CI_Controller {

  public function __construct() {
    parent::__construct();
    if (empty($this->session->userdata('user_id'))) {
        redirect(ADMIN_PATH . 'auth/logout');
    }
    $this->breadcrumbs->push("Manufacturing Module", "Manufacturing_Dashboard");
    $this->breadcrumbs->push("Dashboard", "Manufacturing_Dashboard");
    $this->load->model(array('Manufacturing_module/Manufacturing_department_order_model', 'Manufacturing_module/Department_model', 'sale_master/Parent_category_model', 'sale_master/Parent_category_model', 'Weight_range_model', 'Karigar_model','Manufacturing_module/Karigar_order_model','User_access_model'));
    $this->load->config('admin_validationrules', TRUE);
  }

  public function index() {

    $department_id=$this->session->userdata('department_id');
    $department_name=$this->Department_model->find($department_id);
    $data['page_title'] = "Department Order List";
    $this->breadcrumbs->push($department_name['name']." - Department Order List", "Department Order List");
    $data['display_title'] = $department_name['name']." - Orders List";
    $list = $this->input->post('list');
    if ($list != "") {
        echo json_encode($this->generate_data_table($department_id));
    } else {
        $this->view->render('Manufacturing_module/department_order/index', $data);
    }
  }

  public function create($id = '') {
        //$data['department'] = $this->Department_model->find($id);
        /*department wise*/
        $department_id=$this->session->userdata('department_id');
        $department_name=$this->Department_model->find($department_id);
    $this->breadcrumbs->push($department_name['name']." - Department Order List", "Manufacturing_department_order");
    $this->breadcrumbs->push($department_name['name']." - Add Order", "Manufacturing_department_order/create");
         
        if($department_id !='0'){
        $dep_name[]=$this->Department_model->find($department_id);   
        $data['departments'] = $dep_name;
        }else{            
          $data['departments'] = $this->Department_model->get('','','','',true);          
        }
        $data['parent_category'] = $this->Parent_category_model->get('','','','',true);
        $data['weight_range'] = $this->Weight_range_model->get('','','','',true);
        $data['department_id'] = $id;
        $data['dep_id']=$department_id;
        //print_r($data['dep_id']);die;
        $data['mapping_data'] = '';
        $data['max_id'] = $this->Manufacturing_department_order_model->get_max_id();
        $data['page_title'] = $department_name['name']. " - Create Order Order No " . $data['max_id'];
        $this->view->render('Manufacturing_module/department_order/create', $data);
  }

  public function validate_order_sheet() {
      $result = $this->Manufacturing_department_order_model->validate_order_sheet();
      echo json_encode($result);
  }

  public function store() {   
    $department_id=$this->session->userdata('department_id');
    $department_name=$this->Department_model->find($department_id); 
    //$_POST['dep_id']=$dep_id['department_id'];
     if($department_id=='2' || $department_id == '3' || $department_id =='6' || $department_id =='10'){
         unset($_POST['department_order[quantity]']);
     }else if($department_id =='1' || $department_id =='11'){
        unset($_POST['department_order[weight]']);
     }
   // print_r($_POST);die;
    $result['status'] = 'failure';
    $result['error']['common'] = 'Please Provide at least one sub category';
    if ((!empty($_POST['order_data']))) {
        if (empty($_POST['id']))
            $result = $this->Manufacturing_department_order_model->store($_POST['order_data']);
        else
            $result = $this->Manufacturing_department_order_model->store($_POST['order_data'], $_POST['id']);
    }
    echo json_encode($result);
  }

  private function generate_data_table($department_id) {
    $filter_status = @$_REQUEST['order'][0];
    $status = array('name', 'code');
    $search = @$_REQUEST['search']['value'];   
    $result = $this->Manufacturing_department_order_model->get($filter_status, $status, $_REQUEST, $search, $limit = true,$department_id);
    $totalRecords = $this->Manufacturing_department_order_model->get($filter_status, $status, $_REQUEST, $search, $limit = false,$department_id);
    if (!empty($result)) {
     
   
        foreach ($result as $key => $value) {

            $button_html   = '';
            /*product form sale department*/
            if ($value['is_sale']==1) {
               $text_recived_from ='Received From Sale Department';
            }else{
               $text_recived_from ='Manufacture Order'; 
               $button_html .= '<a href="' . ADMIN_PATH . 'Manufacturing_department_order/edit/' . $value["id"] . '"><button class="btn btn-link edit_link small loader-hide btn-sm" name="commit" type="button">EDIT</button></a>&nbsp;&nbsp;';

            }

            $data[$key][0] = '<span style="float:right">'.$value["id"].'</span>';
            $data[$key][1] = $value["order_name"];
            $data[$key][2] = '<span style="float:right">'.date('d-m-Y', strtotime($value["order_date"])).'</span>';
            $data[$key][3] = $value["name"];
            $data[$key][4]=$text_recived_from;
            
            $btn_disabled  = "";   
            if( $value['kariger_engaged_qty'] != "")
                $btn_disabled = "disabled"; 
            else
                $btn_disabled = ""; 
            $button_html .= '<a href="' . ADMIN_PATH . 'Manufacturing_department_order/view/' . $value["id"] . '"><button class="btn btn-link view_link small loader-hide btn-sm" name="commit" type="button">VIEW</button></a>&nbsp;&nbsp;';
            $button_html .= '<a><button '.$btn_disabled.' class="btn btn-link delete_link small loader-hide btn-sm" name="commit" type="button" onclick="remove_mnfctrng_order('.$value["id"].')">Remove</button></a>';
            $data[$key][5] = $button_html;
        }
    } else {
        $data[0][0] = ['No data found'];
        $data[0][1] = [];
        $data[0][2] = [];
        $data[0][3] = [];
        $data[0][4] = [];
        $data[0][5] = [];
    }
    $json_data = array(
        "draw" => intval($_REQUEST['draw']),
        "recordsTotal" => intval($totalRecords),
        "recordsFiltered" => intval($totalRecords),
        "data" => $data
    );
    return $json_data;
  }

  public function edit($order_id) {
    /*department wise*/
   
    $department_id=$this->session->userdata('department_id');
    $department_name=$this->Department_model->find($department_id); 
    $this->breadcrumbs->push($department_name['name']." - Department Order List", "Manufacturing_department_order");
    $this->breadcrumbs->push($department_name['name']." - Edit Order ", "Manufacturing_department_order/create");
    $data['departments'] = $this->Department_model->find($order_id);
    $data['page_title'] = $department_name['name']." - Edit Order";
    /*$data['page_title'] = "Edit Order for  department " . $data['departments']['name'];*/
    if($department_id !='0'){
    $dep_name[]=$this->Department_model->find($department_id);   
    $data['departments'] = $dep_name;
    }else{            
      $data['departments'] = $this->Department_model->get('','','','',true);          
    }    
    $data['parent_category'] = $this->Parent_category_model->get('','','','',true);
    $data['weight_range'] = $this->Weight_range_model->get('','','','',true);
    $data['manufacturing_order'] = $this->Manufacturing_department_order_model->find($order_id);
    $data['department_id'] = $data['manufacturing_order']['department_id'];
    $data['mapping_data'] = $this->Manufacturing_department_order_model->get_all_subcategories_by_order_id($order_id, $data['manufacturing_order']['order_name'], $data['department_id'],'','','','',true,$variant=true);
    $data['dep_id']=$department_id;
    $data['order_id'] = $order_id;
    $this->view->render('Manufacturing_module/department_order/create', $data);
  }

  public function view($order_id){
    $department_id=$this->session->userdata('department_id');
    $department_name=$this->Department_model->find($department_id); 
    $this->breadcrumbs->push($department_name['name']." - Department Order List", "Manufacturing_department_order");
    $this->breadcrumbs->push($department_name['name']." - Prepare Karigar Order", "View");
    $data['page_title'] = "View Orders";
    $data['display_title'] = $department_name['name']. " - Prepare Karigar Order";
    $data['dep_id']=$department_id;
    //print_r( $department_id);die;
    $list = $this->input->post('list');
    if ($list != "") {      
        if($department_id =='2' || $department_id == '3' || $department_id =='6' || $department_id =='10' ){
           
            echo json_encode($this->generate_view_data_dept_wise_table($order_id));
        }else{
           
             echo json_encode($this->generate_view_all_dept_data_table($order_id));
        }
    } else {
        $data['order_id'] = $order_id;
        $data['all_karigars'] = $this->Karigar_model->get('','','','',true);

        $this->view->render('Manufacturing_module/department_order/view', $data);
    }
  }

  private function generate_view_all_dept_data_table($order_id) {
    $order = $this->Manufacturing_department_order_model->find($order_id);
    $filter_status = @$_REQUEST['order'][0];
    $status = array('name', 'code');
    $search = @$_REQUEST['search']['value'];
  //  print_r($_REQUEST['columns'][3]['search']['value']);
    $result = $this->Manufacturing_department_order_model->get_all_subcategories_by_order_id($order_id, $order['order_date'], $order['department_id'], $filter_status, $status, $_REQUEST, $search, $limit = true,$variant=false);
    //print_r($result);
    $totalRecords = $this->Manufacturing_department_order_model->get_all_subcategories_by_order_id($order_id, $order['order_date'], $order['department_id'], $filter_status, $status, $_REQUEST, $search, $limit = false,$variant=false);
 // print_r($result);die;
    if (!empty($result)) {
        $count = 0;
        $total_gr_wt = 0;
        $total_qnt = 0;
        
            foreach ($result as $key => $value) {
            $count++; 
            if ($value["assign_quantity"] >= $value["quanity"]) {
                $data[$key][] = '';
            } else {
            $data[$key][] = '<div class="PREPARE_check checkbox checkbox-purpal"><input type="checkbox" name="assign[]" class="order_id checkbox_ctn" value="'.$value["id"].'" data-parsley-id="'.$value["id"].'"><label for="PREPARE_check"></label></div>';
            }

            $sr_no = $key + 1;
            $data[$key][] ='<span style="float:right">'.$sr_no.'</span>';
            $data[$key][] ='<span style="float:right">'.$value["order_id"].'</span>';
            $data[$key][] = '<span style="float:right">'.date('d-m-Y', strtotime($value["order_date"])).'</span>';
            $data[$key][] = $value["name"];
            // $data[$key][3] = $value["product_code"];
            $data[$key][] = '<span style="float:right">'.$value["from_weight"] . ' - ' . $value["to_weight"].'</span>';
            if($order['department_id']=='8' || $order['department_id']=='2' || $order['department_id']=='3' || $order['department_id'] =='6' || $order['department_id']=='10'){
                $total_gr_wt += $value["weight"];
                $data[$key][] = '<span style="float:right">'.$value["weight"].'</span>';
            }else{
                $midweight   = (($value["from_weight"] + $value["to_weight"]) * $value["quanity"])/2;
                $total_gr_wt += $midweight;
                $data[$key][] = '<span style="float:right">'.$midweight.'</span>';
            }
            $total_qnt += $value["quanity"];
            $data[$key][] = '<span style="float:right">'.$value["quanity"].'</span>';
            $data[$key][] = '<span style="float:right">'.$value["assign_quantity"].'</span>';
            $data[$key][] = '<span style="float:right">'.$value["approx_weight"].'</span>';
            $button_html = '';
            if ($value["assign_quantity"] >= $value["quanity"]) {
                $data[$key][] = 'Ordered';
            } else {
                $data[$key][] = 'Open';
                if($value["assign_quantity"]!="")//disabled remvoe button if quantity assign to kariger
                    $remove_btn_html = '<a disabled href="' . ADMIN_PATH . 'Manufacturing_department_order/delete/' . $value["id"] . '"><button disabled class="btn btn-link delete_link small loader-hide btn-sm" name="commit" type="button">Remove</button></a>';
                else
                    $remove_btn_html = '<a href="javascript:void(0)"><button class="btn btn-link delete_link small loader-hide btn-sm" name="commit" type="button" onclick="remove_sub_category_from_dept_order('.$value["id"].','.$order_id.')">Remove</button></a>';

                $button_html = '&nbsp;&nbsp;'.$remove_btn_html.'<input type="hidden" value="'.$value["product_code"].'" id="product_code'.$count.'"/>';
            }
           $cat_img_html = '';
           if(!empty($value['path'])){

            $cat_img_html   =   '<img src="'.UPLOAD_IMAGES.'parent_category/'.$value["name"].'/small/'.$value['path'].'" style="width: 100%;"><br/><br/>';
           }else{
            $cat_img_html   = '';
           }
            $data[$key][] = $cat_img_html;
            $data[$key][] = $button_html;
        }
        $key = $key+1;
        $data[$key][] = '<b>Total</b>';
        $data[$key][] = '';
        $data[$key][] = '';
        $data[$key][] = '';
        $data[$key][] = '';
        $data[$key][] = '';
        $data[$key][] = '<span style="float:right"><b>'.$total_gr_wt.'</b></span>';
        $data[$key][] = '<span style="float:right"><b>'.$total_qnt.'</b></span>';   
        $data[$key][] = '';
        $data[$key][] = '';
        $data[$key][] = '';
        $data[$key][] = '';
        $data[$key][] = '';
    } else {
        $data[0][] = ['No data found'];
        $data[0][] = [];
        $data[0][] = [];
        $data[0][] = [];
        $data[0][] = [];
        $data[0][] = [];
        $data[0][] = [];
        $data[0][] = [];
        $data[0][] = [];
        $data[0][] = [];
        $data[0][] = [];
        $data[0][] = [];
        $data[0][] = [];
        $data[0][] = [];
    }
    $json_data = array(
        "draw" => intval($_REQUEST['draw']),
        "recordsTotal" => intval($totalRecords),
        "recordsFiltered" => intval($totalRecords),
        "data" => $data
    );

    return $json_data;
  }

  private function generate_view_data_dept_wise_table($order_id) {
    //print_r($order_id);die;
    $order = $this->Manufacturing_department_order_model->find($order_id);
    $filter_status = @$_REQUEST['order'][0];
    $status = array('name', 'code');
    $search = @$_REQUEST['search']['value'];
    $result = $this->Manufacturing_department_order_model->get_dept_by_order_id_data($order_id, $order['order_date'], $order['department_id'], $filter_status, $status, $_REQUEST, $search, $limit = true,$variant=false);
   // print_r($result);
    $totalRecords = $this->Manufacturing_department_order_model->get_dept_by_order_id_data($order_id, $order['order_date'], $order['department_id'], $filter_status, $status, $_REQUEST, $search, $limit = false,$variant=false);
  //print_r($result);die;
    if (!empty($result)) {
        $count = 0;
        $total_gr_wt = 0;
        $total_assign = 0;
        
            foreach ($result as $key => $value) {
            $count++; 
            if ($value["assign_quantity"] >= $value["weight"]) {
                $data[$key][] = '';
            } else {
            $data[$key][] = '<div class="PREPARE_check checkbox checkbox-purpal"><input type="checkbox" name="assign[]" class="order_id checkbox_ctn" value="'.$value["id"].'" data-parsley-id="'.$value["id"].'"><label for="PREPARE_check"></label></div>';
            }
            
            $total_gr_wt +=$value["weight"];
            $total_assign+=$value["total_weight"];
     
            $sr_no = $key + 1;
            $data[$key][] ='<span style="float:right">'.$sr_no.'</span>';
            $data[$key][] ='<span style="float:right">'.$value["order_id"].'</span>';
            $data[$key][] = '<span style="float:right">'.date('d-m-Y', strtotime($value["order_date"])).'</span>';
            $data[$key][] = $value["name"];
            $data[$key][] = '<span style="float:right">'.$value["from_weight"] . ' - ' . $value["to_weight"].'</span>';
      
            $data[$key][] = '<span style="float:right">'.$value["weight"].'</span>';
            $data[$key][] = '<span style="float:right">'.$value["total_weight"].'</span>';
            $button_html = '';
            if ($value["total_weight"] >= $value["weight"]) {
                $data[$key][] = 'Ordered';
            } else {
                $data[$key][] = 'Open';
                if($value["total_weight"]!="")//disabled remvoe button if quantity assign to kariger
                    $remove_btn_html = '<a disabled href="' . ADMIN_PATH . 'Manufacturing_department_order/delete/' . $value["id"] . '"><button disabled class="btn btn-link delete_link small loader-hide btn-sm" name="commit" type="button">Remove</button></a>';
                else
                    $remove_btn_html = '<a href="javascript:void(0)"><button class="btn btn-link delete_link small loader-hide btn-sm" name="commit" type="button" onclick="remove_sub_category_from_dept_order('.$value["id"].','.$order_id.')">Remove</button></a>';

                $button_html = '&nbsp;&nbsp;'.$remove_btn_html.'<input type="hidden" value="'.$value["product_code"].'" id="product_code'.$count.'"/>';
            }
           $cat_img_html = '';
           if(!empty($value['path'])){

            $cat_img_html   =   '<img src="'.UPLOAD_IMAGES.'parent_category/'.$value["name"].'/small/'.$value['path'].'" style="width: 100%;"><br/><br/>';
           }else{
            $cat_img_html   = '';
           }
            $data[$key][] = $cat_img_html;
            $data[$key][] = $button_html;
        }
        $key = $key+1;
        $data[$key][] = '<b>Total</b>';
        $data[$key][] = '';
        $data[$key][] = '';
        $data[$key][] = '';
        $data[$key][] = '';
        $data[$key][] = '';
        $data[$key][] = '<span style="float:right"><b>'.$total_gr_wt.'</b></span>';
        $data[$key][] = '<span style="float:right"><b>'.$total_assign.'</b></span>';
        $data[$key][] = '';
        $data[$key][] = '';
        $data[$key][] = '';
        
    } else {
        $data[0][] = ['No data found'];
        $data[0][] = [];
        $data[0][] = [];
        $data[0][] = [];
        $data[0][] = [];
        $data[0][] = [];
        $data[0][] = [];
        $data[0][] = [];
        $data[0][] = [];
        $data[0][] = [];
        $data[0][] = [];
        $data[0][] = [];
    }
    $json_data = array(
        "draw" => intval($_REQUEST['draw']),
        "recordsTotal" => intval($totalRecords),
        "recordsFiltered" => intval($totalRecords),
        "data" => $data
    );

    return $json_data;
  }



  public function delete($id) {
    $result = $this->Manufacturing_department_order_model->delete($id);
    echo json_encode($result);
  }
  public function delete_order($id) {
    $result = $this->Manufacturing_department_order_model->delete_order($id);
    echo json_encode($result);
  }
  private function get_mapping_array($data) {
    $sub_categories = $data['sub_category'];
    $array = array();
    $i = 0;
    foreach ($sub_categories as $sck => $sub_category) {
        $weight_band = $this->Sub_category_model->find_stock_with_weight_band($sub_category['id']);
        foreach ($weight_band as $wb) {
            $array[$i]['sub_category'] = $sub_category['id'];
            $array[$i]['name'] = $sub_category['name'];
            $array[$i]['order_date'] = date('Y-m-d');
            $array[$i]['department'] = $data['department']['id'];
            $array[$i]['tr_count'] = $i;
            $array[$i]['quantity'] = $wb['stock'];
            $array[$i]['weight_range_id'] = $wb['weight_range_id'];
            $array[$i]['from_weight'] = $wb['from_weight'];
            $array[$i]['to_weight'] = $wb['to_weight'];
            $i++;
        }
    }
    return $array;
  }

  //Code to get suncategory images
  public function get_sub_cat_images()
  {
    $id             =   $_POST['sub_cat_id'];
    $sub_cat_name   =   $this->Sub_category_model->sub_category_name_by_id($id);
    $data           =   array(
                'sub_cat_name'     =>  $sub_cat_name->name,
                'sub_cat_images'   =>  $this->Sub_category_model->sub_category_images($id),
                );
    echo json_encode($data);
  }

  public function show()
  {
    $department_id=$this->session->userdata('department_id');
    $department_name=$this->Department_model->find($department_id); 
    $sel_order = @$_POST['assign']; 
    foreach($sel_order as $key => $order_id)
    {
      $data['order_details'][] = $this->Manufacturing_department_order_model->get_mom_details($order_id,$department_id);
    }
    $data['page_title'] = "Send Order To Karigar";
    $data['display_title'] = "Send Order To Karigar";
    $data['karigar'] = $this->Karigar_model->get('','','','',true);
    $data['dep_id']=$department_id;
    $this->view->render('Manufacturing_module/department_order/assign_karigar',$data);
  }

  public function order_placed()
  {

    $karigar_id=$_POST['karigar_id'];
    $order_no = $_POST['order_no'];
    $quantity=$_POST['quantity'];
    $manufacturing_order_id = $_POST['manufacturing_order_id'];
    $approx_weight = $_POST['avg_weight'];

    $insert = array('mop_id'=>$manufacturing_order_id,'karigar_id'=>$karigar_id,'approx_weight'=>$approx_weight,'status'=>2,'quantity'=>$quantity,'created_at'=>date('Y-m-d'),'updated_at'=>date('Y-m-d'),'delivery_date'=>date('Y-m-d',strtotime("+12 day")));

    $data['order_details']= $this->Karigar_order_model->update_order_quantity($insert);
      

  }

    
}
?>