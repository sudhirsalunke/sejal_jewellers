<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class Sending_to_qc_mfg extends CI_Controller {
  public function __construct() {
    parent::__construct();
    if(empty($this->session->userdata('user_id'))){
      redirect(ADMIN_PATH . 'auth/logout');
    }
    $this->load->library('excel_lib');
    $this->breadcrumbs->push("Manufacturing Module", "Manufacturing_Dashboard");
    $this->breadcrumbs->push("Dashboard", "Manufacturing_Dashboard");
    $this->load->model(array('Manufacturing_module/Received_orders_model','User_access_model','Manufacturing_module/Department_model'));
    $this->load->config('admin_validationrules', TRUE);
  }
  public function index(){
    $department_id=$this->session->userdata('department_id');
    $department_name=$this->Department_model->find($department_id);;
    $data['dep_id']=$department_id;
    $data['display_name'] = $department_name['name']." - Send to QC";
    $data['page_title'] = 'Sending To Qc';
    $list=$this->input->post('list');
    if($list !="")
    {
      echo json_encode($this->generate_data_table($department_id));
    }else{
      $this->view->render('Manufacturing_module/Sending_to_qc_mfg/index',$data);
    }
  }
  private function generate_data_table($department_id){
    $filter_status =@$_REQUEST['order'][0];
    $status = array();
    $search=@$_REQUEST['search']['value'];
    //print_r($department_id);die;
    $result = $this->Received_orders_model->get_sending_to_qc($filter_status,$status,$_REQUEST,$search,$limit=true,$hallmarking ='',$id='',$department_id);
    //$this->db->last_query();die;
    $totalRecords = $this->Received_orders_model->get_sending_to_qc($filter_status,$status,$_REQUEST,$search,$limit=false,$hallmarking ='',$id='',$department_id);
    if (!empty($result)) {
      $data = $this->get_datatable_rows($result);
    }else{
      $data = $this->get_null_row();
    }
    return $this->return_json($totalRecords,$data);
  }
  private function get_datatable_rows($result){
    //print_r($result);die;
    foreach ($result as $key => $value) {
      $sr_no=$key+1;
      $data[$key][] = '<span style="float:right">'.$sr_no.'</span>';
      $data[$key][] ='<span style="float:right">'.$value['order_id'];
      $data[$key][] =$value['receipt_code'];
      $data[$key][] ='<span style="float:right">'.date('d-m-Y',strtotime($value["order_date"])).'</span>';
      $data[$key][] ='<span style="float:right">'.$value["receipt_date"];
      if(!empty($value["weight"])){
        $data[$key][] ='<span style="float:right">'.$value["weight"].'</span>';
      }else{
        $data[$key][] ='<span style="float:right">'.$value["gross_wt"].'</span>';
      }
      $data[$key][] ='<span style="float:right">'.$value["net_wt"].'</span>';
      if($value['department_id'] =='2' || $value['department_id']=='3' || $value['department_id'] =='6' || $value['department_id']=='10'):
      $data[$key][] ='<span style="float:right">'.($value["weight"] - $value['qc_weight']).'</span>';
      else:
      $data[$key][] ='<span style="float:right">'.($value["quantity"] - $value['qc_quantity']).'</span>';
      endif;  

      $data[$key][] = '<button  onclick="send_mfg_qc(this,\''.$value['receipt_code'].'\')" class="btn btn-link accept_link small loader-hide  btn-sm" name="commit"  id="a_'.$value['receipt_code'].'" type="button" >Send to qc</button>';  
    }
    return $data;
  }
  private function get_null_row(){
    $msg='No data found';
    for ($i=0; $i <=9 ; $i++) { 
      $data[0][$i] = [$msg];
      $msg="";
    }

    return $data;
  }
  private function return_json($totalRecords,$data){
    $json_data = array(
          "draw" => intval($_REQUEST['draw']),
          "recordsTotal" => intval($totalRecords),
          "recordsFiltered" => intval($totalRecords),
          "data" => $data
      );
    return $json_data;
  }

   public function send_mfg_qc($receipt_code='')
  {
     // print_r($receipt_code);die;
      if(!empty($receipt_code)){
        $data = $this->Received_orders_model->send_mfg_qc($receipt_code);
         redirect(ADMIN_PATH.'Sending_to_qc_mfg');
        
      }
  }
  
}