<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class Ready_product extends CI_Controller {
  public function __construct(){
    parent::__construct();
    if(empty($this->session->userdata('user_id'))){
      redirect(ADMIN_PATH . 'auth/logout');
    }
    $this->breadcrumbs->push("Manufacturing Module", "Manufacturing_Dashboard");
    $this->load->library('zend'); 
    $this->zend->load('Zend/Barcode');
    $this->breadcrumbs->push("Dashboard", "Manufacturing_Dashboard");
    $this->load->model(array('Manufacturing_module/Ready_product_model','Manufacturing_module/Prepare_kundan_karigar_order_model','sales_module/Sales_stock_model','sales_module/Sales_recieved_product_model','User_access_model','Manufacturing_module/Department_model','Karigar_model','Manufacturing_module/Karigar_receive_order_model','sale_master/Parent_category_model','Category_model','Manufacturing_module/Tag_products_model'));
          $this->load->library('zend');
        $this->zend->load('Zend/Barcode');
        $this->load->library(array('Data_encryption','excel_lib','m_pdf'));
  }

  public function index($department_id=''){
    $data['page_title'] = 'View Ready Product';
    $department_id=$this->session->userdata('department_id');
    $department_name=$this->Department_model->find($department_id);
    $data['location'] = $this->User_access_model->get_location();
    $data['department_id'] = $department_id;  
    $this->breadcrumbs->push("Dashboard", "Manufacturing_Dashboard");
    
    $list=$this->input->post('list');
    if($list !=""){
      echo json_encode($this->generate_data_table($department_id));
    }else{
        if (@$_GET['status']=="pending") {
           $data['display_name'] = $department_name['name'].' - Pending to be approved by sales';
          $this->breadcrumbs->push($department_name['name']." - Pending to be approved by sales", "Ready_product?status=pending");
        }else{
           $data['display_name'] =$department_name['name']. ' - Ready Product';
          $this->breadcrumbs->push($department_name['name']." - Ready Product", "Ready_product");
        }
      $this->view->render('Manufacturing_module/ready_product/index',$data);
    }
  }

  public function view($product_code=''){
    $department_id=$this->session->userdata('department_id');
    $department_name=$this->Department_model->find($department_id);
    $data['department_id'] = $department_id;  
    $data['display_name'] =$department_name['name']. ' -  Product Details';
    $this->breadcrumbs->push($department_name['name']." - Product Details", "Ready_product");
    $data['page_title'] = 'View Ready Product';
    $data['product_details']=$this->Ready_product_model->get_ready_product_details('','','','',$limit=true,$department_id, $product_code);
    $data['product_code'] = $product_code;
    //print_r($product_code);die;
    $list=$this->input->post('list');
    if($list !="")
    {
      //print_r($department_id);die;
      echo json_encode($this->generate_data_table_details($product_code,$department_id));
    }else{
      $this->view->render('Manufacturing_module/ready_product/product_details',$data);
    }
  }  
  public function delete(){
    $result = $this->Ready_product_model->delete($_POST['product_code']);
    echo json_encode($result);
  }
 

  private function generate_data_table($department_id){
    $filter_status =@$_REQUEST['order'][0];
    $search=@$_REQUEST['search']['value'];
    $status = array();
    $result = $this->Ready_product_model->get($filter_status,$status,$_REQUEST,$search,$limit=true,$department_id);
  //echo $this->db->last_query(); echo"<pre>"; print_r($result);echo"</pre>"; die;
    $totalRecords = $this->Ready_product_model->get($filter_status,$status,$_REQUEST,$search,$limit=false,$department_id);
    if (!empty($result)) {
      $data = $this->get_datatable_rows($result,$department_id);
    }else{
      $data = $this->get_null_row($department_id);
    }
    return $this->return_json($totalRecords,$data);
  }
  private function get_datatable_rows($result,$department_id){

    $start=$_REQUEST['start'];
    //print_r($department_id);
    foreach ($result as $key => $value) {
      $img="";
      if (!empty($value['image'])) {
        $img = '<img style="height:60px" src="'.ADMIN_PATH.$value['image'].'">';
      }
      //$btn_action ='<a class="btn btn-md btn-primary" onclick="transfer_to_sales('.$value['id'].')">TRANSFER TO SALE</a>';
      $button_html ="";
      $delete_btn ="";
      $veiw_btn ="";
      $edit_btn ="";

      $start++;
      if (!empty($value['product_code'])) {    

        $veiw_btn ='<a href="'.ADMIN_PATH.'Ready_product/view/'.$value['product_code'].'"><button class="btn btn-link view_link small loader-hide btn-sm" name="commit" type="button" >View</button></a>&nbsp';
        $edit_btn ='<a href="'.ADMIN_PATH.'sales_stock/edit/'.$value['product_code'].'"><button class="btn btn-link edit_link  btn-sm" name="commit" type="button" >Edit</button></a>&nbsp';
        $delete_btn ='<a href="javascript:void(0)" onclick=Delete_ready_products("'.$value['product_code'].'",this,"ready_product")><button class="btn btn-link delete_link  btn-sm" name="commit" id="d_'.$value['product_code'].'" type="button" >Delete</button></a>';
         $barcode_btn ='<button onclick=singal_ready_products_barcode("'.$value['id'].'") type="button" class="btn btn-link primery_link btn-sm">Print Barcode</button>';
        $button_html.=$veiw_btn;
        $button_html.=$edit_btn;
        if($department_id == '0' || $department_id == '1' ||  $department_id == '5' || 
          $department_id == '7'  ||  $department_id == '8' || $department_id =='11'){
          $button_html.=$delete_btn;
          $button_html.=$barcode_btn;
         }
       /* $button_html = '<span class="pull-right"><a href="'.ADMIN_PATH.'Ready_product/view/'.$value['product_code'].'"><button class="btn btn-warning small loader-hide btn-sm" name="commit" type="button" >View</button></a>&nbsp;<a href="'.ADMIN_PATH.'sales_stock/edit/'.$value['product_code'].'"><button class="btn btn-info  btn-sm" name="commit" type="button" >Edit</button></a>&nbsp;<a href="'.ADMIN_PATH.'sales_stock/edit/'.$value['product_code'].'"><button class="btn btn-danger  btn-sm" name="commit" type="button" >Delete</button></a></span>';*/

     }
       if (@$_GET['status'] != "pending") {
          $data[$key][] = '<div class="checkbox checkbox-purpal"><input type="checkbox" name="rp[]" id="checkbox_'.$value['id'].'" class="form-control checkbox_ctn" value="'.$value['id'].'"><label for="checkbox_'.$value['id'].'"></label></div>';
                if($department_id == '0' || $department_id == '1' ||  $department_id == '5' || 
                   $department_id == '7'  ||  $department_id == '8' || $department_id =='11'){  
                  $data[$key][] ='<span style="float:right">'.$value['department_name'].'</span>';
                  $data[$key][] ='<span style="float:right">'.$value['chitti_no'].'</span>';
                  $data[$key][] =$value['karigar_name'];
                  $data[$key][] ='<a href="'.ADMIN_PATH.'Ready_product/view/'.$value['product_code'].'">'.$value['product_code'].'</a>';
                  $data[$key][] =$value['product'];
                  if(!empty($value["custom_net_wt"])){
                      $data[$key][] ='<span style="float:right">'.round($value["custom_gr_wt"],2) .'</span>';
                      $data[$key][] ='<span style="float:right">'.round($value["custom_net_wt"],2) .'</span>';
                  }else{
                     $data[$key][] ='<span style="float:right">'.round($value["gr_wt"],2) .'</span>';
                     $data[$key][] ='<span style="float:right">'.round($value["net_wt"],2).'</span>';
                  }
                  $data[$key][] ='<span style="float:right">'. $value['quantity'].'</span>';
                  $data[$key][] ='<span style="float:right">'. $value['created_at'].'</span>';
                  $data[$key][] =$button_html;
                  //$data[$key][7] =$btn_action;
                }else{
                  $data[$key][] ='<span style="float:right">'.$value['department_name'].'</span>';
                  $data[$key][] ='<span style="float:right">'.$value['chitti_no'].'</span>';
                  $data[$key][] =$value['karigar_name'];
        /*          $data[$key][] =$value['product_code'];*/
                  $data[$key][] =$value['product'];
                if(!empty($value["custom_net_wt"])){
                      $data[$key][] ='<span style="float:right">'.round($value["custom_gr_wt"],2) .'</span>';
                      $data[$key][] ='<span style="float:right">'.round($value["custom_net_wt"],2) .'</span>';
                  }else{
                     $data[$key][] ='<span style="float:right">'.round($value["gr_wt"],2) .'</span>';
                     $data[$key][] ='<span style="float:right">'.round($value["net_wt"],2).'</span>';
                  }            
                 
         
                }
               
      }else{        
         

           
                $data[$key][] = '<span style="float:right">'.$start.'</span>';
                $data[$key][] ='<span style="float:right">'.$value['chitti_no'].'</span>';
                $data[$key][] =$value['karigar_name'];
                if($department_id!='2' && $department_id !='3' && $department_id !='6' && $department_id !='10'){
                $data[$key][] ='<a href="'.ADMIN_PATH.'Ready_product/view/'.$value['product_code'].'">'.$value['product_code'].'</a>';
                }
                $data[$key][] =$value['product'];
                  if(!empty($value["custom_net_wt"])){
                      $data[$key][] ='<span style="float:right">'.round($value["custom_gr_wt"],2) .'</span>';
                      $data[$key][] ='<span style="float:right">'.round($value["custom_net_wt"],2) .'</span>';
                  }else{
                     $data[$key][] ='<span style="float:right">'.round($value["gr_wt"],2) .'</span>';
                     $data[$key][] ='<span style="float:right">'.round($value["net_wt"],2).'</span>';
                  }
                $data[$key][] ='<span style="float:right">'. $value['quantity'].'</span>';
                // $data[$key][] =$img;
                $data[$key][] =$button_html;
      }
       

    }
    return $data;
  }
  private function get_null_row($department_id){
      $msg='No data found';
    for ($i=0; $i <=11 ; $i++) { 
      $data[0][$i] = [$msg];
      $msg="";
    }

    
    return $data;
  }
  private function return_json($totalRecords,$data){
    $json_data = array(
          "draw" => intval($_REQUEST['draw']),
          "recordsTotal" => intval($totalRecords),
          "recordsFiltered" => intval($totalRecords),
          "data" => $data
      );
    return $json_data;
  }

  private function generate_data_table_details($product_code,$department_id) {
    //print_r($product_code);die;
    $filter_status =@$_REQUEST['order'][0];
    $status = @$_GET['status'];
    $search=@$_REQUEST['search']['value'];
    
    $result = $this->Ready_product_model->get_ready_product_details($filter_status,$status,$_REQUEST, $status, $limit = true,$department_id,$product_code);

   $totalRecords = $this->Ready_product_model->get_ready_product_details($filter_status,$status,$_REQUEST, $status, $limit = false,$department_id,$product_code);
//echo $this->db->last_query(); print_r($result); die;
   //$j=0;
    if (!empty($result)) {
        foreach ($result as $key => $value) {
           //print_r($value);
          if($department_id=='8'){
            $data[$key][0] ='<span style="float:right">'. $value["quantity"].'</span>';
            if(!empty($value["custom_net_wt"])){
                $data[$key][1] ='<span style="float:right">'.round($value["custom_net_wt"],2) .'</span>';
            }else{
               $data[$key][1] ='<span style="float:right">'.round($value["net_wt"],2).'</span>';
            }         
            $data[$key][2] = '<span style="float:right">'.round($value["few_wt"],2).'</span>';
            $data[$key][3] = '<span style="float:right">'.round($value["kundan_pure"],2).'</span>';
            $data[$key][4] = '<span style="float:right">'.round($value["mina_wt"],2).'</span>';
            $data[$key][5] = '<span style="float:right">'.round($value["wax_wt"],2).'</span>';
            $data[$key][6] = '<span style="float:right">'.round($value["color_stone"],2).'</span>';
            $data[$key][7] = '<span style="float:right">'.round($value["moti"],2).'</span>';
            $data[$key][8] = '<span style="float:right">'.round($value["checker_wt"],2).'</span>';
            $data[$key][9] = '<span style="float:right">'.round($value["black_beads_wt"],2).'</span>';
            if(!empty($value["custom_gr_wt"])){
              $data[$key][10] ='<span style="float:right">'.round($value["custom_gr_wt"],2) .'</span>';
            }else{                                   
                $data[$key][10] = '<span style="float:right">'.round($value["gr_wt"] , 2).'</span>';
            } 
            $data[$key][11] = '<span style="float:right">'.round($value["checker_pcs"] , 2).'</span>';    
            $data[$key][12] = '<span style="float:right">'.round($value["checker_rate"] , 2).'</span>';  
            $data[$key][13] = '<span style="float:right">'.round($value["checker_amt"] , 2).'</span>';   
            $data[$key][14] = '<span style="float:right">'.round($value["kundan_pc"] , 2).'</span>';  
            $data[$key][15] = '<span style="float:right">'.round($value["kundan_rate"] , 2).'</span>';   
            $data[$key][16] = '<span style="float:right">'.round($value["kundan_amt"] , 2).'</span>';  
            $data[$key][17] = '<span style="float:right">'.round($value["stone_wt"] , 2).'</span>';
  
            $data[$key][18] = '<span style="float:right">'.round($value["color_stone_rate"] , 2).'</span>';
            $data[$key][19] = '<span style="float:right">'.round($value["stone_amt"] , 2).'</span>';
            $data[$key][20] = '<span style="float:right">'.round($value["other_wt"] , 2).'</span>';
            $data[$key][21] = '<span style="float:right">'.round($value["total_wt"] , 2).'</span>';

          }else if($department_id=='11'){
            $data[$key][0] ='<span style="float:right">'. $value["quantity"].'</span>';
                     if(!empty($value["custom_net_wt"])){
                $data[$key][1] ='<span style="float:right">'.round($value["custom_net_wt"],2) .'</span>';
            }else{
               $data[$key][1] ='<span style="float:right">'.round($value["net_wt"],2).'</span>';
            } 
            $data[$key][2] = '<span style="float:right">'.round($value["gr_wt"] , 2).'</span>';
          }else{
            $data[$key][0] ='<span style="float:right">'. $value["quantity"].'</span>';
                     if(!empty($value["custom_net_wt"])){
                $data[$key][1] ='<span style="float:right">'.round($value["custom_net_wt"],2) .'</span>';
            }else{
               $data[$key][1] ='<span style="float:right">'.round($value["net_wt"],2).'</span>';
            } 
            $data[$key][2] = '<span style="float:right">'.round($value["few_wt"],2).'</span>';
            $data[$key][3] = '<span style="float:right">'.round($value["kundan_pure"],2).'</span>';
            $data[$key][4] = '<span style="float:right">'.round($value["mina_wt"],2).'</span>';
            $data[$key][5] = '<span style="float:right">'.round($value["wax_wt"],2).'</span>';
            $data[$key][6] = '<span style="float:right">'.round($value["color_stone"],2).'</span>';
            $data[$key][7] = '<span style="float:right">'.round($value["moti"],2).'</span>';
            $data[$key][8] = '<span style="float:right">'.round($value["checker_wt"],2).'</span>';
             if(!empty($value["custom_gr_wt"])){
              $data[$key][9] ='<span style="float:right">'.round($value["custom_gr_wt"],2) .'</span>';
            }else{                                   
                $data[$key][9] = '<span style="float:right">'.round($value["gr_wt"] , 2).'</span>';
            } 
            $data[$key][10] = '<span style="float:right">'.round($value["checker_pcs"] , 2).'</span>';    
            $data[$key][11] = '<span style="float:right">'.round($value["checker_rate"] , 2).'</span>';  
            $data[$key][12] = '<span style="float:right">'.round($value["checker_amt"] , 2).'</span>';   
            $data[$key][13] = '<span style="float:right">'.round($value["kundan_pc"] , 2).'</span>';  
            $data[$key][14] = '<span style="float:right">'.round($value["kundan_rate"] , 2).'</span>';   
            $data[$key][15] = '<span style="float:right">'.round($value["kundan_amt"] , 2).'</span>';  
            $data[$key][16] = '<span style="float:right">'.round($value["stone_wt"] , 2).'</span>';
  
            $data[$key][17] = '<span style="float:right">'.round($value["color_stone_rate"] , 2).'</span>';
            $data[$key][18] = '<span style="float:right">'.round($value["stone_amt"] , 2).'</span>';
            $data[$key][19] = '<span style="float:right">'.round($value["black_beads_wt"],2).'</span>';
            $data[$key][20] = '<span style="float:right">'.round($value["other_wt"] , 2).'</span>';
            $data[$key][21] = '<span style="float:right">'.round($value["total_wt"] , 2).'</span>';
          }
            
        }
    } else {
       $msg='No data found';
         if($department_id=='8'){$row='21';}else if($department_id=='11'){$row="3";}else{$row="21";}
       for ($i=0; $i <=$row; $i++) { 
           $data[0][$i] = [$msg];
           $msg="";
       }
    
    }

      $json_data = array(
          "draw" => intval($_REQUEST['draw']),
          "recordsTotal" => intval($totalRecords),
          "recordsFiltered" => intval($totalRecords),
          "data" => $data
      );
    return $json_data;
  }

  public function store(){
    //print_r($_POST);die;
    $postdata = $_POST;
    $data = $this->Ready_product_model->store($postdata);
    $data['url'] = ADMIN_PATH.'Ready_product/print_tag_products';
    echo json_encode($data);
  }
  public function print_tag_products(){
    $st = @$this->session->userdata('print_tag_product_session');
    if (empty($st)) {
     redirect(ADMIN_PATH.'tag_products/create');
    }else{
      $data['page_title'] = 'print_pdf_manuf';
      $data['result'] = $this->Prepare_kundan_karigar_order_model->get_pdf_details($st);
      $data['chitti_no'] =@$data['result'][0]['make_set_id'];
      $this->load->view('Manufacturing_module/Prepare_kundan_karigar_order/print',$data);
      $this->session->unset_userdata('print_tag_product_session');

    }
  }

  // public function sales_to_manufacture(){ /*moved to sale_stock*/
  //   $data= $this->Ready_product_model->sales_to_manufacture();
  //   echo json_encode($data);
  // }
  public function transfer_to_sales(){
    $data= $this->Ready_product_model->transfer_to_sales();
    echo json_encode($data);
  }

  public function repair_products(){
    //print_r($_POST);die;
    $data= $this->Ready_product_model->repair_products();
    echo json_encode($data);
  }
  public function generate_voucher(){
    if(!empty($_SESSION['rp_id'])){
      foreach ($_SESSION['rp_id'] as $key => $value) {
        $data['result'][] = $this->Ready_product_model->find($value);
      }
    }
    $data['no_heading']=true;
    $this->load->view('Manufacturing_module/Prepare_kundan_karigar_order/print',$data);
  }
  public function generate_issue_receipt_repair(){
    if(!empty($_SESSION['rp_id'])){
      foreach ($_SESSION['rp_id'] as $key => $value) {
        $data['result'][] = $this->Ready_product_model->find($value);
      }
    }
    $data['no_heading']=true;
     $this->session->unset_userdata('rp_id');
    $this->load->view('Manufacturing_module/ready_product/receipt_print',$data);
  }
  public function approved_by_sale(){
    $department_id=$this->session->userdata('department_id');
    $department_name=$this->Department_model->find($department_id);
    $data['department_id'] = $department_id;  
    $display_title =$department_name['name']." - Approved By Sale";
    if (@$_GET['status']=="manufacture") {
      $display_title ="Products Approved By Sales" ;
    }
    $this->breadcrumbs->push($department_name['name']." - Approved By Sale", "Ready_product?status=pending");
    $data['page_title'] = "All Stock Products";
    $data['display_title'] = $display_title;
    $list=$this->input->post('list');
    if($list !="")
    {
      echo json_encode($this->generate_stock_approved_data_table($department_id));
    }else{
      $this->view->render('sales_module/stock/stock_approved',$data);
    }
  }

   private function generate_stock_approved_data_table($department_id){
    //print_r($department_id);die;
    $filter_status =@$_REQUEST['order'][0];
    $search=@$_REQUEST['search']['value'];
    $result = $this->Sales_stock_model->get($filter_status,$_REQUEST,$search,$limit=true,$department_id);
    $totalRecords = $this->Sales_stock_model->get($filter_status,$_REQUEST,$search,$limit=false,$department_id);
    //print_r($result);
    if (!empty($result)) {
        foreach ($result as $key => $value) {
          $btn_html="";
        if (!empty($value['product_code'])) {        
         $button_html = '<span class="pull-right"><a href="'.ADMIN_PATH.'Ready_product/view/'.$value['product_code'].'"><button class="btn btn-link view_link small loader-hide btn-sm" name="commit" type="button" >View</button></a>&nbsp;</span>';
         }
      
          $sr_no=$_REQUEST['start'] + $key+1;
          //$data[$key][] ='<span style="float:right">'.$sr_no.'</span>';
        
            $ci =& get_instance();
            $url=$ci->uri->segment(1); 
           // print_r($url);die;
           $data[$key][] = '<span style="float:right">'.$sr_no.'</span>';
                   
          $data[$key][] = '<span style="float:right">'.$value["voucher_no"].'</span>';
    /*      $data[$key][] = $value["karigar_name"];
          $data[$key][] = '<span style="float:right">'.$value["karigar_code"].'</span>';*/
        if($department_id!='2' && $department_id !='3' && $department_id !='6' && $department_id !='10'){
          $data[$key][] ='<a href="'.ADMIN_PATH.'Ready_product/view/'.$value['product_code'].'">'.$value['product_code'].'</a>';
        }
        /*  $data[$key][] = $value["sub_category"];*/
          $data[$key][] = $value["product"];
          if($department_id!='2'&& $department_id !='3' && $department_id !='6' && $department_id !='10'){
          $data[$key][] = '<span style="float:right">'.$value["quantity"].'</span>';
          }
          $data[$key][] = '<span style="float:right">'.round($value["gr_wt"],2).'</span>';
          $data[$key][] = '<span style="float:right">'.round($value["net_wt"],2).'</span>';
          $data[$key][] = '<span style="float:right">'.$value["approve_date"].'</span>';
          if($department_id!='2' && $department_id !='3' && $department_id !='6' && $department_id !='10'){
          $data[$key][] =$button_html;
         }

         
        }
    }else{
      $txt='No data found';
      for ($i=0; $i <=8; $i++) { 
         $data[0][$i] = [$txt];
         $txt="";
      }
    }

     $json_data = array(
          "draw" => intval($_REQUEST['draw']),
          "recordsTotal" => intval($totalRecords),
          "recordsFiltered" => intval($totalRecords),
          "data" => $data
      );

    return $json_data; 
  }
  public function rejected_by_sale(){
    $department_id=$this->session->userdata('department_id');
    $department_name=$this->Department_model->find($department_id);
    $data['department_id'] = $department_id; 
    $data['page_title'] = "All Recieved Products Show";
    $data['display_title'] = "Rejected By Sale";
    $this->breadcrumbs->push("Rejected By Sale", "Ready_product?status=pending");
    $list=$this->input->post('list');
    if($list !="")
    {
      echo json_encode($this->generate_show_data_table());
    }else{
    $_GET['status']="sended_to_manufacture";
      $data['th']=array('#','Product','Product Code','Gross Weight','Net Weight','Quantity');
        if (empty($_GET['status'])) {
          array_unshift($data['th'],$select_all_products);

        }
      $this->view->render('sales_module/recieved_product/show',$data);
    }
  }

  public function import(){
    $data['page_title'] = "Upload Stock Excel";
    //print_r($_FILES['file_0']);die;
    if(empty($_FILES['file_0'])){
      $data['status']= 'failure1';
      $data['data']= '';
      $data['error'] = 'Please upload Stock Excel';
      echo json_encode($data);   
    }
    if(!empty($_FILES['file_0']))
    {      
      $this->store_stock_excel();
    }else{
      $this->view->render('Manufacturing_module/Upload_stock_excel/index',$data);
    }
  }

  private function store_stock_excel(){
    $is_error = false;
    $product_code = array();
       
    $result = $this->excel_lib->import('department_ready_product',$_FILES['file_0']);
    //print_r($result['result']);die;

    //$data['receipt_code'] = $this->Karigar_receive_order_model->create_kundan_receipt_code();
    if($result['status'] == 'success'){
      $insert_array=array();
       $store_array=array();
     if(!empty($result['result'])){
       foreach ($result['result']  as $key => $value) {
        $department_id=$this->Department_model->find_name(trim($value['department_id']));
        $karigar_id=$this->Karigar_model->find_name(trim($value['karigar_id']));
        $item_category_code=$this->Category_model->find_name(trim($value['item_category_code']),trim($department_id['department_id']));
        $product=$this->Parent_category_model->find_name(trim($value['product']));
       
      //$code=$this->Ready_product_model->last_product_code($product[0]['product'],$item_category_code[0]['item_category_code']);
    // $product_code=$product[0]['product'].'-'.$item_category_code[0]['item_category_code'].'-'.$code['max_count'];
  
                   
                    $_POST['chitti_no'] = "MFGCEXST".mt_rand();
                    $_POST['action_type']='excel';
                    $_POST['rp_id'][]=$key;
                    $_POST['product'][]=$value['product'];
                    $_POST['parent_category_id'][]=$item_category_code[0]['item_category_code'];
                    $_POST['sub_code'][]=$product[0]['product'];

                    //$_POST['product_code'][]=$product_code;
                    $_POST['product_code'][]=$value ['product_code'];
                    $_POST['net_wt'][]=$value ['net_wt'];
                    $_POST['gross_wt'][]=$value ['gr_wt'];
                    $_POST['quantity'][]=@$value ['quantity'];
                    $_POST['few_wt'][]=@$value ['few_wt'];
                    $_POST['kundan_pure'][]=@$value ['kundan_pure'];
                    $_POST['mina_wt'][]=@$value ['mina_wt'];
                    $_POST['wax_wt'][]=@$value ['wax_wt'];
                    $_POST['color_stone'][]=@$value ['color_stone'];
                    $_POST['moti'][]=@$value ['moti'];
                    $_POST['checker_wt'][]=@$value ['checker_wt'];
                    $_POST['checker'][]=@$value ['checker'];
                    $_POST['checker_pcs'][]=@$value ['checker_pcs'];
                    $_POST['checker_amt'][]=@$value ['checker_amt'];
                    $_POST['kundan_pc'][]=@$value ['kundan_pc'];
                    $_POST['kundan_rate'][]=@$value ['kundan_rate'];
                    $_POST['kundan_amt'][]=@$value ['kundan_amt'];
                    $_POST['stone_wt'][]=@$value ['stone_wt'];
                    $_POST['color_stone_rate'][]=@$value ['color_stone_rate'];
                    $_POST['stone_amt'][]=@$value ['stone_amt'];
                    $_POST['other_wt'][]=@$value ['other_wt'];
                    $_POST['total_wt'][]=@$value ['total_wt'];
                    $_POST['karigar_id'][]=@$karigar_id['karigar_id'];
                    $_POST['department_id'][]=@$department_id['department_id'];
                                      
                   $data = $this->Karigar_receive_order_model->validate_excel();
                    $insert_array=$_POST;
                if($data['status'] == 'failure'){
                   echo json_encode($data);die;
                }else{
                  // $success=$this->Ready_product_model->store_excel_data($insert_array,$key);
                  $is_error == false;
                     $store_array=$insert_array;
                 // print_r($_POST);                  
                    /*$success['status'] = 'success';
                    echo json_encode($success);die;*/
                } 
       
        }
        if($is_error==false) {
          $success=$this->Ready_product_model->store_excel_data($store_array);
          $success['status'] = 'success';
          echo json_encode($success);
        }     
      }else{
        $error['status'] = 'failure';
        $error['error'][]= 'YOUR EXCEL IS EMPTY';
        echo json_encode($error);die;
      }
     }elseif($result['status'] == 'failure'){
       $error = $this->get_differnceMSG($result['error']);
       echo json_encode($error);die;
      
    }
 
  }
    private function get_differnceMSG($diff){
    $error['status']= 'failure';
    $error['data']= '';
    foreach ($diff as $key => $value) {
      $error['error'][$key] =$value.' Column not found in excel' ;
    }
    return array_filter($error);
  }

  public function barcode_products()
  { 
    $result=$this->Ready_product_model->update_product_barcode();
    echo json_encode($result);    
  } 
  public function singal_barcode_products($rp_id)
  { 
    $_POST['rp'][]=$rp_id;
    $result=$this->Ready_product_model->update_product_barcode();
    echo json_encode($result);    
  }
  public function print_product_barcode(){
    if(!empty($_SESSION['drp_id'])){
      foreach ($_SESSION['drp_id'] as $key => $value) {
        $data['receipt_code'][] = $this->Ready_product_model->find($value);
      }
    }
    $this->session->unset_userdata('drp_id');
    $this->load->view('Manufacturing_module/ready_product/print_barcode',$data);
  }

}
?>