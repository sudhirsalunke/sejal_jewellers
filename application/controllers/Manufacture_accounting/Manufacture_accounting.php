<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Manufacture_accounting extends CI_Controller {

	public function __construct() {
    parent::__construct();
     $this->breadcrumbs->push("Manufacturing Module", "Manufacturing_Dashboard");
     $this->load->model(array('manufacture_accounting/Issue_voucher_model','manufacture_accounting/Cash_bank_issue_model','manufacture_accounting/Metal_issue_model'));
    if(empty($this->session->userdata('user_id'))){
      redirect(ADMIN_PATH . 'auth/logout');
    }

  }

public function index(){
    $data['page_title']= 'Manufacture Accounting';
    $data['display_name']='Manufacture Accounting';
    $this->breadcrumbs->push("Accounting", "Manufacture_accounting");

    $data['vouchers']=sizeof($this->Issue_voucher_model->get());
    $data['cash_bank']=sizeof($this->Cash_bank_issue_model->get());
    $data['metal']=sizeof($this->Metal_issue_model->get());
    $this->view->render('Manufacturing_module/accounting/index',$data);
    
  }

}//class