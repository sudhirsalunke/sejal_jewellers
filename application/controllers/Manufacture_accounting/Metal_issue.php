<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Metal_issue extends CI_Controller {

  public function __construct() {
    parent::__construct();
    if (empty($this->session->userdata('user_id'))) {
        redirect(ADMIN_PATH . 'auth/logout');
    }
    $this->breadcrumbs->push("Manufacturing Module", "Manufacturing_Dashboard");
    $this->breadcrumbs->push("Accounting", "Manufacture_accounting");
     $this->breadcrumbs->push("Metal - Issue Voucher", "Manufacture_accounting/Metal_issue");
    $this->load->model(array('manufacture_accounting/Metal_issue_model','Material_master_model','sale_master/Customer_type_model'));
    $this->load->config('admin_validationrules', TRUE);
  }

  public function index() {
    $data['page_title'] = "Metal Issue Voucher";
    $data['display_title'] = "Metal - Issue Voucher";
    $list = $this->input->post('list');
    if ($list != "") {
        echo json_encode($this->generate_data_table());
    } else {
         $this->view->render('Manufacturing_module/accounting/Metal_issue/index', $data);
    }
  }

  public function create(){
    $this->breadcrumbs->push("Create", "Manufacture_accounting/Metal_issue/create");
        $data['voucher_no'] = $this->Metal_issue_model->get_voucher_no();
        $data['page_title'] = "Create Metal Issue Voucher";
        $data['display_title'] = "Create - Metal Issue Voucher";
        $data['material_master']=$this->Material_master_model->get();
        $data['party_type']=$this->Customer_type_model->get();
        $this->view->render('Manufacturing_module/accounting/Metal_issue/create', $data);
  }

  private function validation_results(){ 
    $data = array();
    $validationResult = $this->Metal_issue_model->validatepostdata();
    if(count($validationResult) != 0){
      $data['status']= 'failure';
      $data['data']= '';
      $data['error'] = $validationResult;
    }
    return $data;

  }

 

  public function store() {
   	$data = $this->validation_results();
   	if (count($data)==0) {
   		$data =$this->Metal_issue_model->store();
   	}
   	echo json_encode($data);
  }

  private function generate_data_table() {
    $filter_status = @$_REQUEST['order'][0];
    $status = array('name', 'code');
    $search = @$_REQUEST['search']['value'];
    $result = $this->Metal_issue_model->get($filter_status, $_REQUEST, $search, $limit = true);
    $totalRecords = sizeof($this->Metal_issue_model->get($filter_status, $_REQUEST, $search, $limit = false));
    if (!empty($result)) {
    	$start = $_REQUEST['start'];
        foreach ($result as $key => $value) {
            $start++;

            $data[$key][] = '<span style="float:right">'.$start.'</span>';
            $data[$key][] = '<span style="float:right">'.$value["id"].'</span>';
            $data[$key][] = '<span style="float:right">'.date('d-m-Y', strtotime($value["voucher_date"])).'</span>';
            $data[$key][] = $value["type_name"]." - ".$value["party_name"];
            $data[$key][]="<a href='".ADMIN_PATH."Manufacture_accounting/Metal_issue/show/".$value['id']."' class='btn btn-sm btn-link view_link'>View</a>";
            
        }
    } else {
        $data[0][0] = ['No data found'];
        $data[0][1] = [];
        $data[0][2] = [];
        $data[0][3] = [];
        $data[0][4] = [];
      
    }
    $json_data = array(
        "draw" => intval($_REQUEST['draw']),
        "recordsTotal" => intval($totalRecords),
        "recordsFiltered" => intval($totalRecords),
        "data" => $data
    );
    return $json_data;
  }

  // public function edit($order_id) {
  //   $this->breadcrumbs->push("Department Order List", "Manufacturing_department_order");
  //   $this->breadcrumbs->push("Edit Order", "Manufacturing_department_order/create");
  //   $data['departments'] = $this->Department_model->find($order_id);
  //   $data['page_title'] = "Edit Order for department " . $data['departments']['name'];
  //   $data['departments'] = $this->Department_model->get();
  //   $data['parent_category'] = $this->Parent_category_model->get();
  //   $data['weight_range'] = $this->Weight_range_model->get();
  //   $data['manufacturing_order'] = $this->Manufacturing_department_order_model->find($order_id);
  //   $data['department_id'] = $data['manufacturing_order']['department_id'];
  //   $data['mapping_data'] = $this->Manufacturing_department_order_model->get_all_subcategories_by_order_id($order_id, $data['manufacturing_order']['order_name'], $data['department_id']);
  //   $data['order_id'] = $order_id;
  //   $this->view->render('Manufacturing_module/department_order/create', $data);
  // }

  public function show($voucher_no) {
    $this->breadcrumbs->push("View", "Manufacture_accounting/Metal_issue/view");
    $data['page_title'] = "View Metal Issue voucher";
    $data['display_title'] = "View Metal Issue Voucher : ".$voucher_no;

    
    $data['voucher'] = $this->Metal_issue_model->find($voucher_no);
    $data['voucher_details'] = $this->Metal_issue_model->voucher_details($voucher_no);
    $this->view->render('Manufacturing_module/accounting/Metal_issue/view', $data);
    
  }

    
} //class
