<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Cash_bank_issue extends CI_Controller {

  public function __construct() {
    parent::__construct();
    if (empty($this->session->userdata('user_id'))) {
        redirect(ADMIN_PATH . 'auth/logout');
    }
    $this->breadcrumbs->push("Manufacturing Module", "Manufacturing_Dashboard");
    $this->breadcrumbs->push("Accounting", "Manufacture_accounting");
     $this->breadcrumbs->push("Cash-Bank Issue Receipt", "Manufacture_accounting/Cash_bank_issue");
    $this->load->model(array('manufacture_accounting/Cash_bank_issue_model','sale_master/Customer_type_model'));
    $this->load->config('admin_validationrules', TRUE);
  }

  public function index() {
    $data['page_title'] = "Cash Bank Issue";
    $data['display_title'] = "Cash Bank Issue Receipt";
    $list = $this->input->post('list');
    if ($list != "") {
        echo json_encode($this->generate_data_table());
    } else {
         $this->view->render('Manufacturing_module/accounting/Cash_bank_issue/index', $data);
    }
  }

  public function create(){
    $this->breadcrumbs->push("Create", "Manufacture_accounting/Cash_bank_issue/create");
        $data['page_title'] = "Create Issue Receipt";
        $data['display_title'] = "Create - Cash Bank Issue Receipt";
        $data['party_type']=$this->Customer_type_model->get();
        $this->view->render('Manufacturing_module/accounting/Cash_bank_issue/create', $data);
  }

  private function validation_results(){ 
    $data = array();
    $validationResult = $this->Cash_bank_issue_model->validatepostdata();
    if($validationResult == false){
      $data['status']= 'failure';
      $data['data']= '';
      $data['error'] =  array(
        'party_name'=>strip_tags(form_error('cash_bank[party_name]')),
        'party_type'=>strip_tags(form_error('cash_bank[party_type]')),
        'bank_name'=>strip_tags(form_error('cash_bank[bank_name]')),
        'amount'=>strip_tags(form_error('cash_bank[amount]')),
      );
    }
    return $data;

  }

 

  public function store() {
   	$data = $this->validation_results();
   	if (count($data)==0) {
   		$data =$this->Cash_bank_issue_model->store();
   	}
   	echo json_encode($data);
  }

  private function generate_data_table() {
    $filter_status = @$_REQUEST['order'][0];
    $status = array('name', 'code');
    $search = @$_REQUEST['search']['value'];
    $result = $this->Cash_bank_issue_model->get($filter_status, $_REQUEST, $search, $limit = true);
    $totalRecords = sizeof($this->Cash_bank_issue_model->get($filter_status, $_REQUEST, $search, $limit = false));
    if (!empty($result)) {
    	$start = $_REQUEST['start'];
        foreach ($result as $key => $value) {
            $start++;

            $data[$key][] = '<span style="float:right">'.$start.'</span>';
            $data[$key][] =$value["type_name"].' - '.$value["party_name"];
            $data[$key][] = '<span style="float:right">'.date('d-m-Y H:i:s', strtotime($value["created_at"])).'</span>';
            $data[$key][] = $value["bank_name"];
            $data[$key][] = '<span style="float:right">'.$value["amount"].'</span>';
            $data[$key][]="<a href='".ADMIN_PATH."Manufacture_accounting/Cash_bank_issue/show/".$value['id']."' class='btn btn-sm btn-link view_link'>View</a>";
            
        }
    } else {
        $data[0][0] = ['No data found'];
        $data[0][1] = [];
        $data[0][2] = [];
        $data[0][3] = [];
        $data[0][4] = [];
        $data[0][5] = [];
      
    }
    $json_data = array(
        "draw" => intval($_REQUEST['draw']),
        "recordsTotal" => intval($totalRecords),
        "recordsFiltered" => intval($totalRecords),
        "data" => $data
    );
    return $json_data;
  }

  // public function edit($order_id) {
  //   $this->breadcrumbs->push("Department Order List", "Manufacturing_department_order");
  //   $this->breadcrumbs->push("Edit Order", "Manufacturing_department_order/create");
  //   $data['departments'] = $this->Department_model->find($order_id);
  //   $data['page_title'] = "Edit Order for department " . $data['departments']['name'];
  //   $data['departments'] = $this->Department_model->get();
  //   $data['parent_category'] = $this->Parent_category_model->get();
  //   $data['weight_range'] = $this->Weight_range_model->get();
  //   $data['manufacturing_order'] = $this->Manufacturing_department_order_model->find($order_id);
  //   $data['department_id'] = $data['manufacturing_order']['department_id'];
  //   $data['mapping_data'] = $this->Manufacturing_department_order_model->get_all_subcategories_by_order_id($order_id, $data['manufacturing_order']['order_name'], $data['department_id']);
  //   $data['order_id'] = $order_id;
  //   $this->view->render('Manufacturing_module/department_order/create', $data);
  // }

  public function show($Receipt_no) {
    $this->breadcrumbs->push("View", "Manufacture_accounting/Cash_bank_issue/view");
    $data['page_title'] = "View Issue Receipt";
    $data['display_title'] = "View Cash Bank Issue Receipt : ".$Receipt_no;

    $data['Receipt'] = $this->Cash_bank_issue_model->find($Receipt_no);
    $this->view->render('Manufacturing_module/accounting/Cash_bank_issue/view', $data);
    
  }
    
} //class
