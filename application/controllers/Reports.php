<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Reports extends CI_Controller {
  public function __construct() {
	  parent::__construct();
	  if(empty($this->session->userdata('user_id'))){
	    redirect(ADMIN_PATH . 'auth/logout');
	    $this->load->model(array('Reports_model'));
    }
  }
  // public function accounting_report(){
  // }
}