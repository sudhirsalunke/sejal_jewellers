<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class Order_tracking_report extends CI_Controller {

  public function __construct() {
      parent::__construct();
      if(empty($this->session->userdata('user_id'))){
      redirect(ADMIN_PATH . 'auth/logout');
      }
    
      $this->load->model(array('report/Order_tracking_report_model'));
  }

  public function index($type)
  {       
    $data['page_title']='Orders Report';      
    $data['type']=$type; 
    $list = $this->input->post('list');

      if($type =='1'){
      $data['display_title']="Karigar order tracking report";        
          if ($list != "") {
          echo json_encode($this->generate_data_table($type));
          } else {  
            $this->view->render('report/order_module/Karigar_report',$data);   
          } 

      }elseif($type=='2'){
          $data['display_title']="Customer order tracking report";    
          if ($list != "") {
          echo json_encode($this->generate_data_table($type));
          } else {  
            $this->view->render('report/order_module/customer_report',$data);   
          } 
      }elseif($type=='3'){
          $data['display_title']="In stock reports";    
          if ($list != "") {
          echo json_encode($this->generate_data_table($type));
          } else {  
            $this->view->render('report/order_module/in_stock_report',$data);   
          } 
      }
      
  } 


  public function daily_report_summary(){
    
    $date = $_GET['today_date'];
    $todaty_date=date('Y-m-d', strtotime($date));
     $list = $this->input->post('list');
    $data['display_title']="Daily report summary";   
    $data['page_title']='Orders Report';   
    $data['today_date']=$todaty_date;   
     if ($list != "") {      
       echo json_encode($this->generate_summary_report_data_table($todaty_date));
     } else {  
       $this->view->render('report/order_module/daily_report_summary',$data);   
     }
  }

    public function delivery_report_in_quantity(){
    
    $frm_date = $_GET['from'];
    $to_date = $_GET['to'];
    $type = $_GET['type'];
     $list = $this->input->post('list');
    $data['display_title']="Delivery report in quantity";   
    $data['page_title']='Orders Report';
    $data['type']=$type;
    if ($list != "") {      
       echo json_encode($this->generate_delivery_report_in_quantity_data_table($frm_date,$to_date,$type));
     } else {  
       $this->view->render('report/order_module/delivery_report_in_quantity',$data);   
     }
  }


public function order_ledger($type){

    $frm_date = $_GET['from'];
    $to_date = $_GET['to'];
     $list = $this->input->post('list');
   if($type == '1'){
      $data['display_title']="Client Order Ledger";    
   }else{
     $data['display_title']="Karigar Order Ledger"; 
   }    
    $data['page_title']='Orders Report';
    $data['type']=$type;
    if ($list != "") {      
       echo json_encode($this->generate_order_ledger_data_table($type,$frm_date,$to_date));
     } else {  
       if($type == '1'){
        $this->view->render('report/order_module/client_order_ledger',$data);    
       }else{
        $this->view->render('report/order_module/karigar_order_ledger',$data);   
       }

     }
  }

public function order_ledger_by_customer($id){

    $frm_date = $_GET['from'];
    $to_date = $_GET['to'];
     $list = $this->input->post('list');
      $data['display_title']="Client Order Ledger (Department Wise)";        
    $data['page_title']='Orders Report';
    $data['type']=$id;
    if ($list != "") {      
       echo json_encode($this->generate_client_order_ledger_data_table($id,$frm_date,$to_date));
     } else {  
        $this->view->render('report/order_module/order_ledger_by_customer',$data);    

     }
  }


public function order_ledger_by_karigar($id){

    $frm_date = $_GET['from'];
    $to_date = $_GET['to'];
     $list = $this->input->post('list');
      $data['display_title']="Karigar Order Ledger (Department Wise)";        
    $data['page_title']='Orders Report';
    $data['type']=$id;
    if ($list != "") {      
       echo json_encode($this->generate_karigar_order_ledger_data_table($id,$frm_date,$to_date));
     } else {  
        $this->view->render('report/order_module/order_ledger_by_karigar',$data);    

     }
  }


  public function daily_order_received(){
    
    $date = $_GET['today_date'];

    $todaty_date=date('Y-m-d', strtotime($date));
     $list = $this->input->post('list');
    $data['display_title']="Daily order received report";   
    $data['page_title']='Orders Report';   
    $data['today_date']=$todaty_date;   
     if ($list != "") {
         
       echo json_encode($this->generate_daily_order_data_table($todaty_date,'custmer_order'));
     } else {  
       $this->view->render('report/order_module/daily_received_order_report',$data);   
     }
  }


  public function daily_order_received_from_karigar(){
  
    $date = $_GET['today_date'];
    $todaty_date=date('Y-m-d', strtotime($date));
     $list = $this->input->post('list');
    $data['display_title']="Daily product received from Karigar report";   
    $data['page_title']='Orders Report';   
    $data['today_date']=$todaty_date;   
     if ($list != "") {
       echo json_encode($this->generate_daily_order_data_table($todaty_date,'karagir_order'));
     } else {  
       $this->view->render('report/order_module/daily_order_received_from_karigar_report',$data);   
     }
  }

  public function delivery_performance($type){
    $list = $this->input->post('list');
    $data['type']=$type; 
    $data['page_title']='Orders Report';     
    $data['display_title']='Delivery Performance';   
     if ($list != "") {
       echo json_encode($this->generate_delivery_performance_data_table($type));
     } else { 
        if($type == 1){ 
             $data['display_title']=' Karigar Delivery Performance';  
          $this->view->render('report/order_module/delivery_performance',$data); 
        } else{
             $data['display_title']=' Party Delivery Performance';  
          $this->view->render('report/order_module/delivery_performance_party',$data);   
       }
     }
  }

  // private function generate_data_table($type) {
  // $search = @$_REQUEST['search']['value'];
  // $filter_status =@$_REQUEST['order'][0];
  // $start=@$_REQUEST['start'];
  // $result = $this->Order_tracking_report_model->get($filter_status,$_REQUEST,$search,true,$type);
  // $totalRecords = $this->Order_tracking_report_model->get($filter_status,$_REQUEST,$search,false,$type);
  //   //print_r($result);die;
  // if (!empty($result)) {
  //     foreach ($result as $key => $value) {
  //     $start++; 
    
  //     $data[$key][] = '<span style="float:right">'. $start.'</span>';
                        
  //       if($type == '1'){
  //         $data[$key][] = $value["karigar_name"] ;
  //         $data[$key][] = '<span style="float:right">'. $value["id"].'</span>';
  //         $data[$key][] = '<span style="float:right">'. date('d-m-Y',strtotime($value["order_date"])).'</span>';
  //         $data[$key][] = '<span style="float:right">'.date('d-m-Y',strtotime($value["expected_date"])).'</span>';
  //       }elseif($type == '2'){
  //         $data[$key][] = $value["party_name"];
  //         $data[$key][] = '<span style="float:right">'. $value["id"].'</span>';
  //         $data[$key][] = '<span style="float:right">'. date('d-m-Y',strtotime($value["order_date"])).'</span>';
  //         $data[$key][] = '<span style="float:right">'.date('d-m-Y',strtotime($value["delievery_date"])).'</span>';

       
  //       $data[$key][] = '<span style="float:right">'. $value["quantity"].'</span>' ;          
  //         if($value['order_status'] == 0){
  //           $data[$key][] = 'Pending to send to karigar';
  //         }
  //         if($value['order_status'] == 2){
  //           $data[$key][] = 'Pending to receive from karigar';
  //         }
  //         if($value['order_status'] == 3){
  //           $data[$key][] = 'pending to send to customer';
  //         }
  //         if($value['order_status'] == 4){
  //           $data[$key][] = 'Sent to customer_report';
  //         }
  //       }
  //     }
  //   } else {
  //       $msg='No data found';
  //       for ($i=0; $i <=5 ; $i++) { 
  //           $data[0][$i] = [$msg];
  //           $msg="";
  //       }
  //   }

  //   $json_data = array(
  //       "draw" => intval($_REQUEST['draw']),
  //       "recordsTotal" => intval($totalRecords),
  //       "recordsFiltered" => intval($totalRecords),
  //       "data" => $data
  //   );
  //   return $json_data;
  // }


 private function generate_data_table($type) {
  $search = @$_REQUEST['search']['value'];
  $filter_status =@$_REQUEST['order'][0];
  $start=@$_REQUEST['start'];
  $result = $this->Order_tracking_report_model->get($filter_status,$_REQUEST,$search,true,$type);
  $totalRecords = $this->Order_tracking_report_model->get($filter_status,$_REQUEST,$search,false,$type);
       $total_weight = $this->Order_tracking_report_model->get('','','',true,'4');
   // print_r($total_weight[0]['total_weight']);die;

  if (!empty($result)) {
      foreach ($result as $key => $value) {
      $start++; 
    
      $data[$key][] = '<span style="float:right">'. $start.'</span>';
                        
        if($type == '1'){
          $data[$key][] = $value["karigar_name"] ;
          $data[$key][] = '<span style="float:right">'. $value["weight_range"].'</span>';
            $data[$key][] = '<a href="'.ADMIN_PATH.'Order_report/karigar_order_report/'.@$value["karigar_id"].'"><button type="button" class="btn btn-link view_link btn-sm">View</button></a>';
         
        }elseif($type == '2'){
          $data[$key][] = $value["party_name"];
          $data[$key][] = '<span style="float:right">'. $value["weight_range"].'</span>';
          $data[$key][] = '<a href="'.ADMIN_PATH.'Order_report/customer_order_report/'.@$value["customer_id"].'"><button type="button" class="btn btn-link view_link btn-sm">View</button></a>';       
        }elseif($type == '3'){
          $data[$key][] = $value["party_name"];
          $data[$key][] = $value["karigar_name"];
          $data[$key][] = $value["parent_category_id"];
          $data[$key][] = '<span style="float:right">'. $value["weight_range_id"].'</span>';      
            }
      }

      if($type == 1 || $type==2){
        $key=$key+1;
            $data[$key][]="";
            $data[$key][]='<span style="float:right;"><b>Total</b></span>';
            $data[$key][]='<span style="float:right;"><b>'.$total_weight[0]['total_weight'].'</b></span>';
            $data[$key][]="";

      }
    } else {
        $msg='No data found';
        for ($i=0; $i <=5 ; $i++) { 
            $data[0][$i] = [$msg];
            $msg="";
        }
    }

    $json_data = array(
        "draw" => intval($_REQUEST['draw']),
        "recordsTotal" => intval($totalRecords),
        "recordsFiltered" => intval($totalRecords),
        "data" => $data
    );
    return $json_data;
  }


 private function generate_daily_order_data_table($date,$type) {
  $search = @$_REQUEST['search']['value'];
  $filter_status =@$_REQUEST['order'][0];
  $start=@$_REQUEST['start'];
  $result = $this->Order_tracking_report_model->get_daily_order_report($filter_status,$_REQUEST,$search,true,$date,$type);
  $totalRecords = $this->Order_tracking_report_model->get_daily_order_report($filter_status,$_REQUEST,$search,false,$date,$type);
  if (!empty($result)) {
      foreach ($result as $key => $value) {
      $start++;     
      $data[$key][] = '<span style="float:right">'. $start.'</span>';  
        if($type == "karagir_order"){
            $data[$key][] ='<a href="'.ADMIN_PATH.'Customer_order/view/'.$value["id"].'">'.$value["id"].'</a>' ;
          $data[$key][] = $value["karigar_name"] ;
          $data[$key][] = $value["department_name"];
          $data[$key][] = $value["parent_category_id"];
          $data[$key][] = $value["party_name"];
          $data[$key][] = '<span style="float:right">'. $value["weight_range_id"].'</span>';
          $data[$key][] = '<span style="float:right">'. $value["quantity"].'</span>';
          if($value['created_at'] > $value['delievery_date']){
                $ontime = "NO";
             }else{
               $ontime = "YES";
              
             }
      $data[$key][] = '<span style="float:right">'. $ontime.'</span>';
        }else{
          $data[$key][] ='<a href="'.ADMIN_PATH.'Customer_order/view/'.$value["id"].'">'.$value["id"].'</a>' ;
          $data[$key][] = $value["party_name"] ;
          $data[$key][] = $value["department_name"];
          $data[$key][] = $value["karigar_name"];
          $data[$key][] = $value["parent_category_id"];
          $data[$key][] = '<span style="float:right">'. $value["weight_range_id"].'</span>';
          if($value['order_status'] == 1){
            $data[$key][] = 'Pending';
          }
          if($value['order_status'] == 2){
            $data[$key][] = 'In Production';
          }
          if($value['order_status'] == 3){
            $data[$key][] = 'Ready For Delivery';
          }
          if($value['order_status'] == 4){
            $data[$key][] = 'Delivered';
          }
           if($value['order_status'] == 5){
            $data[$key][] = 'Pending';
          }
          if($value['order_status'] == 6){
            $data[$key][] = 'Cancelled';
          }
        }
      }
    } else {
        $msg='No data found';
        for ($i=0; $i <=8 ; $i++) { 
            $data[0][$i] = [$msg];
            $msg="";
        }
    }

    $json_data = array(
        "draw" => intval($_REQUEST['draw']),
        "recordsTotal" => intval($totalRecords),
        "recordsFiltered" => intval($totalRecords),
        "data" => $data
    );
    return $json_data;
  }




public function orders_by_customer($customer_id=""){

    $data['page_title']='Orders Report';      
    $data['customer_id']=$customer_id; 
    $data['customer_name'] = get_customer_name_by_id($customer_id);
    $list = $this->input->post('list');
     
      $data['display_title']="Customer order tracking report";        
          if ($list != "") {
          echo json_encode($this->generate_customer_order_data_table($customer_id));
          } else {  
            $this->view->render('report/order_module/individual_customer_report',$data);   
          } 

}

public function orders_by_karigar($karigar_id=""){
    
    $data['page_title']='Orders Report';      
    $data['karigar_id']=$karigar_id; 
    $data['karigar_name'] = get_karigar_name_by_id($karigar_id);
    $list = $this->input->post('list');
     
      $data['display_title']="Karigar order tracking report";        
          if ($list != "") {
          echo json_encode($this->generate_karigar_order_data_table($karigar_id));
          } else {  
            $this->view->render('report/order_module/individual_karigar_report',$data);   
          } 

}


 private function generate_summary_report_data_table($date) {
  $search = @$_REQUEST['search']['value'];
  $filter_status =@$_REQUEST['order'][0];
  $start=@$_REQUEST['start'];
  $result = $this->Order_tracking_report_model->report_summary_departmentWise($filter_status,$_REQUEST,$search,true,$date);
  $totalRecords = $this->Order_tracking_report_model->report_summary_departmentWise($filter_status,$_REQUEST,$search,false,$date);

  if (!empty($result)) {
      foreach ($result as $key => $value) {
      //  print_r($value);exit;
       $opening_stock = get_opening_stock($value['department_id'],$date);  
       $prev_opening_stock = get_today_send_opening_stock($value['department_id'],$date);  
       $received_stock = get_received_stock($value['department_id'],$date);  
       $prev_received_stock = get_today_send_received_stock($value['department_id'],$date);  
       $issued_stock = get_issued_stock($value['department_id'],$date);  
       $received_order = get_order_received($value['department_id'],$date);  
       $issued_order = get_order_issued($value['department_id'],$date);    
       $total_opening_stock =($opening_stock + $prev_opening_stock);
       $total_received_stock =($received_stock + $prev_received_stock);
       $closing_stock = (($total_opening_stock + $total_received_stock)-$issued_stock);
      $start++; 
    
      $data[$key][] = '<span style="float:right">'. $start.'</span>';
      
           $data[$key][] = $value['name'];
           $data[$key][] = $total_opening_stock;
           $data[$key][] = $total_received_stock;
           $data[$key][] = $issued_stock;
           $data[$key][] = $closing_stock;
           $data[$key][] = $received_order;
           $data[$key][] = $issued_order;
          // $data[$key][] = '<span style="float:right">'. $value["order_date"].'</span>';
          // $data[$key][] = '<span style="float:right">'. $value["expected_date"].'</span>';
          // $data[$key][] = '<span style="float:right">'. $value["quantity"].'</span>';
          //  if($value['status'] == 1){
          //   $data[$key][] = 'Pending';
          // }
          // if($value['status'] == 2){
          //   $data[$key][] = 'In Production';
          // }
          // if($value['status'] == 3){
          //   $data[$key][] = 'Ready For Delivery';
          // }
          // if($value['status'] == 4){
          //   $data[$key][] = 'Delivered';
          // }
          
     }
    } else {
        $msg='No data found';
        for ($i=0; $i <=7 ; $i++) { 
            $data[0][$i] = [$msg];
            $msg="";
        }
    }

    $json_data = array(
        "draw" => intval($_REQUEST['draw']),
        "recordsTotal" => intval($totalRecords),
        "recordsFiltered" => intval($totalRecords),
        "data" => $data
    );
    return $json_data;
  }



 private function generate_customer_order_data_table($customer_id) {
  $search = @$_REQUEST['search']['value'];
  $filter_status =@$_REQUEST['order'][0];
  $start=@$_REQUEST['start'];
  $result = $this->Order_tracking_report_model->customer_orders_by_id($filter_status,$_REQUEST,$search,true,$customer_id);
  $totalRecords = $this->Order_tracking_report_model->customer_orders_by_id($filter_status,$_REQUEST,$search,false,$customer_id);

  if (!empty($result)) {
      foreach ($result as $key => $value) {
      //  print_r($value);exit;
      $start++; 
    
      $data[$key][] = '<span style="float:right">'. $start.'</span>';
        
          $data[$key][] ='<a href="'.ADMIN_PATH.'Customer_order/view/'.$value["id"].'">'.$value["id"].'</a>';
          $data[$key][] = $value["name"];
          $data[$key][] = '<span style="float:right">'. $value["order_date"].'</span>';
          $data[$key][] = '<span style="float:right">'. $value["expected_date"].'</span>';
          $data[$key][] = '<span style="float:right">'. $value["quantity"].'</span>';
           if($value['status'] == 1){
            $data[$key][] = 'Pending';
          }
          if($value['status'] == 2){
            $data[$key][] = 'In Production';
          }
          if($value['status'] == 3){
            $data[$key][] = 'Ready For Delivery';
          }
          if($value['status'] == 4){
            $data[$key][] = 'Delivered';
          }
           if($value['status'] == 5){
            $data[$key][] = 'Pending';
          }
           if($value['status'] == 6){
            $data[$key][] = 'Cancelled';
          }
          
     }
    } else {
        $msg='No data found';
        for ($i=0; $i <=6 ; $i++) { 
            $data[0][$i] = [$msg];
            $msg="";
        }
    }

    $json_data = array(
        "draw" => intval($_REQUEST['draw']),
        "recordsTotal" => intval($totalRecords),
        "recordsFiltered" => intval($totalRecords),
        "data" => $data
    );
    return $json_data;
  }




 private function generate_karigar_order_data_table($karigar_id) {
  $search = @$_REQUEST['search']['value'];
  $filter_status =@$_REQUEST['order'][0];
  $start=@$_REQUEST['start'];
  $result = $this->Order_tracking_report_model->karigar_orders_by_id($filter_status,$_REQUEST,$search,true,$karigar_id);
  $totalRecords = $this->Order_tracking_report_model->karigar_orders_by_id($filter_status,$_REQUEST,$search,false,$karigar_id);
    //print_r($result);die;
  if (!empty($result)) {
      foreach ($result as $key => $value) {
      //  print_r($value);exit;
      $start++; 
    
      $data[$key][] = '<span style="float:right">'. $start.'</span>';
        
          $data[$key][] = '<a href="'.ADMIN_PATH.'Customer_order/view/'.$value["id"].'">'.$value["id"].'</a>';
          $data[$key][] = $value["party_name"];
          $data[$key][] = '<span style="float:right">'. $value["order_date"].'</span>';
          $data[$key][] = '<span style="float:right">'. $value["expected_date"].'</span>';
          $data[$key][] = '<span style="float:right">'. $value["quantity"].'</span>';
           if($value['status'] == 1){
            $data[$key][] = 'Pending';
          }
          if($value['status'] == 2){
            $data[$key][] = 'In Production';
          }
          if($value['status'] == 3){
            $data[$key][] = 'Ready For Delivery';
          }
          if($value['status'] == 4){
            $data[$key][] = 'Delivered';
          }
          if($value['status'] == 5){
            $data[$key][] = 'Pending';
          }
          if($value['status'] == 6){
            $data[$key][] = 'Cancelled';
          }
          
     }
    } else {
        $msg='No data found';
        for ($i=0; $i <=6 ; $i++) { 
            $data[0][$i] = [$msg];
            $msg="";
        }
    }

    $json_data = array(
        "draw" => intval($_REQUEST['draw']),
        "recordsTotal" => intval($totalRecords),
        "recordsFiltered" => intval($totalRecords),
        "data" => $data
    );
    return $json_data;
  }

 private function generate_delivery_report_in_quantity_data_table($from_date,$to_date,$type) {
  $search = @$_REQUEST['search']['value'];
  $filter_status =@$_REQUEST['order'][0];
  $start=@$_REQUEST['start'];
  $result = $this->Order_tracking_report_model->delivery_report_in_quantity($filter_status,$_REQUEST,$search,true,$from_date,$to_date,$type);
  $totalRecords = $this->Order_tracking_report_model->delivery_report_in_quantity($filter_status,$_REQUEST,$search,false,$from_date,$to_date,$type);
  if (!empty($result)) {
      foreach ($result as $key => $value) {
      $start++; 
       $frst_column = get_delivered_quantity_with_range(0,15,$type,$from_date,$to_date,$value['id']);
       $secound_column = get_delivered_quantity_with_range(16,30,$type,$from_date,$to_date,$value['id']);
       $third_column = get_delivered_quantity_with_range(31,45,$type,$from_date,$to_date,$value['id']);
       $fourth_column = get_delivered_quantity_with_range(46,'',$type,$from_date,$to_date,$value['id']);
      $data[$key][] = '<span style="float:right">'. $start.'</span>';
        
          $data[$key][] = $value["name"];
          $data[$key][] = '<span style="float:right">'. $frst_column.'</span>';
          $data[$key][] = '<span style="float:right">'. $secound_column.'</span>';
          $data[$key][] = '<span style="float:right">'. $third_column.'</span>';
          $data[$key][] = '<span style="float:right">'. $fourth_column.'</span>';
          
          
     }
    } else {
        $msg='No data found';
        for ($i=0; $i <=5 ; $i++) { 
            $data[0][$i] = [$msg];
            $msg="";
        }
    }

    $json_data = array(
        "draw" => intval($_REQUEST['draw']),
        "recordsTotal" => intval($totalRecords),
        "recordsFiltered" => intval($totalRecords),
        "data" => $data
    );
    return $json_data;
  }


 private function generate_order_ledger_data_table($type,$frm_date,$to_date) {
  $search = @$_REQUEST['search']['value'];
  $filter_status =@$_REQUEST['order'][0];
  $start=@$_REQUEST['start'];
  $result = $this->Order_tracking_report_model->order_ledger($filter_status,$_REQUEST,$search,true,$type,$frm_date,$to_date);
  $totalRecords = $this->Order_tracking_report_model->order_ledger($filter_status,$_REQUEST,$search,false,$type,$frm_date,$to_date);
  if (!empty($result)) {
      foreach ($result as $key => $value) {
     // $data[$key][] = '<span style="float:right">'. $start.'</span>';
          if($type == '1'){
            $data[$key][] = $value["party_name"];
          }else{
            $data[$key][] = $value["karigar_name"];
          }
          
          $data[$key][] = '<span style="float:right">'. $value['weight'].'</span>';
      if($type == '1'){
          $data[$key][] = '<a href="'.ADMIN_PATH.'Order_report/order_ledger_by_customer/'.@$value["customer_id"].'?from='.$frm_date.'&to='.$to_date.'"><button type="button" class="btn btn-link view_link btn-sm">View</button></a>';
        }else{
          $data[$key][] = '<a href="'.ADMIN_PATH.'Order_report/order_ledger_by_karigar/'.@$value["karigar_id"].'?from='.$frm_date.'&to='.$to_date.'"><button type="button" class="btn btn-link view_link btn-sm">View</button></a>';  
        }  
     }
    } else {
        $msg='No data found';
        for ($i=0; $i <=5 ; $i++) { 
            $data[0][$i] = [$msg];
            $msg="";
        }
    }

    $json_data = array(
        "draw" => intval($_REQUEST['draw']),
        "recordsTotal" => intval($totalRecords),
        "recordsFiltered" => intval($totalRecords),
        "data" => $data
    );
    return $json_data;
  }


 private function generate_client_order_ledger_data_table($id,$frm_date,$to_date) {
  $search = @$_REQUEST['search']['value'];
  $filter_status =@$_REQUEST['order'][0];
  $start=@$_REQUEST['start'];
  $result = $this->Order_tracking_report_model->order_ledger_by_customer($filter_status,$_REQUEST,$search,true,$id,$frm_date,$to_date);
  $totalRecords = $this->Order_tracking_report_model->order_ledger_by_customer($filter_status,$_REQUEST,$search,false,$id,$frm_date,$to_date);

  if (!empty($result)) {
      foreach ($result as $key => $value) {
          $ontime_rate = ($value['ontime_orders'] / $value['total_order'])*100;
     // $data[$key][] = '<span style="float:right">'. $start.'</span>';
        
          $data[$key][] = $value["name"];
          $data[$key][] = '<span style="float:right">'. $value['weight'].'</span>';
          $data[$key][] = $ontime_rate ."%";
     }
    } else {
        $msg='No data found';
        for ($i=0; $i <=5 ; $i++) { 
            $data[0][$i] = [$msg];
            $msg="";
        }
    }

    $json_data = array(
        "draw" => intval($_REQUEST['draw']),
        "recordsTotal" => intval($totalRecords),
        "recordsFiltered" => intval($totalRecords),
        "data" => $data
    );
    return $json_data;
  }

 private function generate_karigar_order_ledger_data_table($id,$frm_date,$to_date) {
  $search = @$_REQUEST['search']['value'];
  $filter_status =@$_REQUEST['order'][0];
  $start=@$_REQUEST['start'];
  $result = $this->Order_tracking_report_model->order_ledger_by_karigar($filter_status,$_REQUEST,$search,true,$id,$frm_date,$to_date);
  $totalRecords = $this->Order_tracking_report_model->order_ledger_by_karigar($filter_status,$_REQUEST,$search,false,$id,$frm_date,$to_date);

  if (!empty($result)) {
      foreach ($result as $key => $value) {
          $ontime_rate = ($value['ontime_orders'] / $value['total_order'])*100;
     // $data[$key][] = '<span style="float:right">'. $start.'</span>';
        
          $data[$key][] = $value["name"];
          $data[$key][] = '<span style="float:right">'. $value['weight'].'</span>';
          $data[$key][] = $ontime_rate ."%";
     }
    } else {
        $msg='No data found';
        for ($i=0; $i <=5 ; $i++) { 
            $data[0][$i] = [$msg];
            $msg="";
        }
    }

    $json_data = array(
        "draw" => intval($_REQUEST['draw']),
        "recordsTotal" => intval($totalRecords),
        "recordsFiltered" => intval($totalRecords),
        "data" => $data
    );
    return $json_data;
  }

 private function generate_delivery_performance_data_table($type) {
  $search = @$_REQUEST['search']['value'];
  $filter_status =@$_REQUEST['order'][0];
  $start=@$_REQUEST['start'];
  $result = $this->Order_tracking_report_model->delivery_performance($filter_status,$_REQUEST,$search,true,$type);
  $totalRecords = $this->Order_tracking_report_model->delivery_performance($filter_status,$_REQUEST,$search,false,$type);
  if (!empty($result)) {
      foreach ($result as $key => $value) {
      $start++; 
      
      $data[$key][] = '<span style="float:right">'. $start.'</span>';
         if($type  == 1){
             if($value['created_at'] > $value['delivery_date']){
                $ontime = "NO";
                $delayed = "YES";
             }else{
               $ontime = "YES";
                $delayed = "NO";
             }
             $data[$key][] = $value["karigar_name"];
              $data[$key][] = $value['parent_category_id'];
              $data[$key][] = $ontime;
              $data[$key][] = $delayed;
              $data[$key][] = $value['quantity'];
         }else{
              if($value['send_at'] > $value['delivery_date']){
                $ontime = "NO";
                $delayed = "YES";
             }else{
               $ontime = "YES";
                $delayed = "NO";
             }
             $data[$key][] = $value["party_name"];
             $data[$key][] =   $value['parent_category_id'];
             $data[$key][] = $ontime;
             $data[$key][] = $delayed;
             $data[$key][] = $value['quantity'];
         }  
         
          
          
     }
    } else {
        $msg='No data found';
        for ($i=0; $i <=5 ; $i++) { 
            $data[0][$i] = [$msg];
            $msg="";
        }
    }

    $json_data = array(
        "draw" => intval($_REQUEST['draw']),
        "recordsTotal" => intval($totalRecords),
        "recordsFiltered" => intval($totalRecords),
        "data" => $data
    );
    return $json_data;
  }


  

  
}
