<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Minimum_subcat_order_report_manufacture extends CI_Controller {

  public function __construct() {
    parent::__construct();
     if(empty($this->session->userdata('user_id'))){
        redirect(ADMIN_PATH . 'auth/logout');
     	}
      	$this->breadcrumbs->push("Manufacture Report", "Manufacture_report");
      	$this->load->model(array('report/Minimum_subcat_report_model'));
    	$this->load->config('admin_validationrules', TRUE);
    	$this->load->library(array('Data_encryption','excel_lib'));
    }

    public function index(){
        $data['page_title']="Minimum Subcat Order Report";
        $data['display_title'] = "Minimum Sub category  Report";
        $this->breadcrumbs->push("Minimum Sub category  Report", "Manufacture_report/Minimum_subcat_order_report_manufacture");
        $data['result']=$this->Minimum_subcat_report_model->get();
        $data['parent_categories']=$this->Minimum_subcat_report_model->get_parent_category();
        $data['weight_range']=$this->Minimum_subcat_report_model->get_weights();
        //print_r($data);exit;
        $this->view->render('report/manufacture/minimum_subcat_report',$data);
            
    }

      public function download(){
        $filename="Order_status_report".date('YmdHis').".xlsx";
        $headings = array('Order Name','Order Weigth','Total Pcs');

         $data =  $this->Minimum_subcat_report_model->get();
       
         $this->excel_lib->export($data,$headings,$filename,$main_heading=array(),$image=array(),$save=false);
      }
}//class