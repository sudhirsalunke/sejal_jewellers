<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Order_status_report_manufacture extends CI_Controller {

  public function __construct() {
    parent::__construct();
     if(empty($this->session->userdata('user_id'))){
        redirect(ADMIN_PATH . 'auth/logout');
     	}
      	$this->breadcrumbs->push("Manufacture Report", "Manufacture_report");
      	$this->load->model(array('report/order_status_report_model'));
    	$this->load->config('admin_validationrules', TRUE);
    	$this->load->library(array('Data_encryption','excel_lib'));
    }

    public function index(){
        $data['page_title']="Order Status Report";
        $data['display_title'] = "Order Status Report";
        $this->breadcrumbs->push("Order Status Report", "Manufacture_report/order_status");
          
        $list=$this->input->post('list');
       //echo $list;die;
            if($list !=""){
              echo json_encode($this->generate_data_table());
            }else{
              $this->view->render('report/manufacture/order_status_report',$data);
            }
    }

    private function generate_data_table(){
        $filter_status =@$_REQUEST['order'][0];
        $search=@$_REQUEST['search']['value'];
        $result = $this->order_status_report_model->get($filter_status,$_REQUEST,$search,$limit=true);
        $totalRecords =$this->order_status_report_model->get($filter_status,$_REQUEST,$search,$limit=false);
        $start=@$_REQUEST['start'];
        if (!empty($result)) {
            foreach ($result as $key => $value) {
              $start++;

                $data[$key][] ='<span style="float:right">'.$start.'</span>';
                $data[$key][] =$value["order_name"];
                $data[$key][] ='<span style="float:right">'. $value["id"].'</span>';
                $data[$key][] ='<span style="float:right">'. $value['order_weight'].'</span>';
                $data[$key][] ='<span style="float:right">'. $value['total_pcs'].'</span>';
                $data[$key][] ='<a href="'.ADMIN_PATH.'Manufacture_report/order_status/view/'.$value["id"].'"><button class="btn btn-link view_link small loader-hide  btn-sm" name="commit" type="button">VIEW</button></a>';
              }
        }else{
            $data[0][] = ['No data found'];
            $data[0][] = [];
            $data[0][] = [];
            $data[0][] = [];
            $data[0][] = [];
            $data[0][] = [];
        }
         $json_data = array(
              "draw" => intval($_REQUEST['draw']),
              "recordsTotal" => intval($totalRecords),
              "recordsFiltered" => intval($totalRecords),
              "data" => $data
          );
        return $json_data; 
      }
    private function generate_data_table_view($id){
        $filter_status =@$_REQUEST['order'][0];
        $search=@$_REQUEST['search']['value'];
        $result = $this->order_status_report_model->get($filter_status,$_REQUEST,$search,$limit=true,$id);
        $totalRecords =sizeof($this->order_status_report_model->get($filter_status,$_REQUEST,$search,$limit=false,$id));
        $start=@$_REQUEST['start'];
        if (!empty($result)) {
            foreach ($result as $key => $value) {
                $start++;
                $balance_pcs = ($value['total_pcs']-$value['rp_qnt']);
                $mid_wt = (($value["from_weight"] + $value['to_weight'])/2);
                $receive_date = '<span style="float:right">'.date('d-m-Y',strtotime($value['created_at']. " + 20 days")).'</span>';
                $data[$key][] = '<span style="float:right">'.$start.'</span>';
                $data[$key][] = '<span style="float:right">'. $value["id"].'</span>';
                $data[$key][] = $value["order_name"];
                $data[$key][] = $value["item_name"];
                $data[$key][] = '<span style="float:right">'. $value["from_weight"].'-'.$value['to_weight'].'</span>';
                $data[$key][] = '<span style="float:right">'. $value['order_weight'].'</span>';
                $data[$key][] = '<span style="float:right">'. $balance_pcs * $mid_wt.'</span>';
                $data[$key][] = '<span style="float:right">'. $value['total_pcs'].'</span>';
                $data[$key][] = '<span style="float:right">'. $balance_pcs.'</span>';
                $data[$key][] = '<span style="float:right">'.$receive_date.'</span>';
                if($value['km_status'] == '2'){
                  $data[$key][] = 'Received';
                }else{
                   $Dueindays= 20 - ceil(abs(strtotime(date('Y-m-d')) - strtotime($value['created_at'])) / 86400);
                  $data[$key][] ='<span style="float:right">'. $Dueindays.'</span>';
                }
              }
        }else{
            $data[0][] = ['No data found'];
            $data[0][] = [];
            $data[0][] = [];
            $data[0][] = [];
            $data[0][] = [];
            $data[0][] = [];
            $data[0][] = [];
            $data[0][] = [];
            $data[0][] = [];
            $data[0][] = [];
            $data[0][] = [];
        }
         $json_data = array(
              "draw" => intval($_REQUEST['draw']),
              "recordsTotal" => intval($totalRecords),
              "recordsFiltered" => intval($totalRecords),
              "data" => $data
          );
        return $json_data; 
      }

      public function download(){
        $filename="Order_status_report".date('YmdHis').".xlsx";
        $headings = array('Order Id','Order Name','Order Weigth','Total Pcs');
        $search=$_POST['search'];
        $data =  $this->order_status_report_model->get('','',$search,'');
       
         $this->excel_lib->export($data,$headings,$filename,$main_heading=array(),$image=array(),$save=false);
      }
      public function view($id){

        $data['page_title']="View Order Status Report";
        $data['order_id']=$id;
        $data['display_title'] = "View Order Status Report";
        $this->breadcrumbs->push("Order Status Report", "Manufacture_report/order_status/view");
        $list=$this->input->post('list');
        if($list !=""){
          echo json_encode($this->generate_data_table_view($id));
        }else{
          $this->view->render('report/manufacture/order_status_report/view',$data);
        }
    }

    public function download_detailed_report($id){
      $filename="Order_id_".$id.'_'.date('YmdHis').".xlsx";
      $headings = array('Order Id','Order Name','Item Name','Weight Range','Issued Wt','Balance Wt','Total Pcs','Balance Pcs','Delivery Date','Due In Days');
      $search=$_POST['search']; 
      $result = $this->order_status_report_model->get($filter_status,$_REQUEST,$search,$limit=false,$id);
      
       foreach ($result as $key => $value) {

            $balance_pcs = ($value['total_pcs']-$value['rp_qnt']);
            $mid_wt = (($value["from_weight"] + $value['to_weight'])/2);
            $receive_date = date('d-m-Y',strtotime($value['created_at']. " + 20 days"));

            $data[$key]['order_id'] = $value["id"];
            $data[$key]['order_name'] = $value["order_name"];
            $data[$key]['item_name'] = $value["item_name"];
            $data[$key]['weight_range'] = $value["from_weight"].'-'.$value['to_weight'];
            $data[$key]['issued_wt'] = $value['order_weight'];
            $data[$key]['balance_wt'] = $balance_pcs * $mid_wt;
            $data[$key]['total_pcs'] = $value['total_pcs'];
            $data[$key]['balance_pcs'] = $balance_pcs;
            $data[$key]['delivery_date'] = $receive_date;
            if($value['km_status'] == '2'){
              $data[$key]['due_in_days'] = 'Received';
            }else{
              $data[$key]['due_in_days'] = 20 - ceil(abs(strtotime(date('Y-m-d')) - strtotime($value['created_at'])) / 86400);;
            }
          }
          $this->excel_lib->export($data,$headings,$filename,$main_heading=array(),$image=array(),$save=false);
    }



}//class