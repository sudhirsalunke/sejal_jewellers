<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Karigar_report_manufacture extends CI_Controller {

    public function __construct() {
        parent::__construct();
        if(empty($this->session->userdata('user_id'))){
        redirect(ADMIN_PATH . 'auth/logout');
        }
        $this->breadcrumbs->push("Manufacture Report", "Manufacture_report");
        $this->load->model(array('report/karigar_report_model'));
    $this->load->config('admin_validationrules', TRUE);
    $this->load->library(array('Data_encryption','excel_lib'));
    }

    public function index(){
        $data['page_title']="Karigar Report";
        $data['display_title'] = "Karigar Summary Report";
        $this->breadcrumbs->push("Karigar Report", "Report/Karigar_report_manufacture");
          
        $list=$this->input->post('list');
            if($list !=""){
              echo json_encode($this->generate_data_table());
            }else{
              $this->view->render('report/manufacture/karigar_report',$data);
            }

    }

    // public function download(){

    //     $headings=array('')
    //     $this->excel_lib->export($data,$headings,$filename,$main_heading=array(),$image=array(),$save=false){
    // }
    private function generate_data_table(){
        $filter_status =@$_REQUEST['order'][0];
        $search=@$_REQUEST['search']['value'];
        $result = $this->karigar_report_model->get($filter_status,$_REQUEST,$search,$limit=true);
        $totalRecords =$this->karigar_report_model->get($filter_status,$_REQUEST,$search,$limit=false);
        $start=@$_REQUEST['start'];
        if (!empty($result)) {
            foreach ($result as $key => $value) {
              $start++;

                $data[$key][0] ='<span style="float:right">'.$start.'</span>';
                $data[$key][1] =$value["karigar_name"];
                $data[$key][2] ='<span style="float:right">'.$value['order_weight'].'</span>';
                $data[$key][3] ='<span style="float:right">'.$value['total_pcs'].'</span>';
              }
        }else{
            $data[0][0] = ['No data found'];
            $data[0][1] = [];
            $data[0][2] = [];
            $data[0][3] = [];
        }
         $json_data = array(
              "draw" => intval($_REQUEST['draw']),
              "recordsTotal" => intval($totalRecords),
              "recordsFiltered" => intval($totalRecords),
              "data" => $data
          );
        return $json_data; 
      }

      public function download(){
        $filename="Karigar_report".date('YmdHis').".xlsx";
        $headings = array('Karigar Name','Order Weigth','Total Pcs');
         $search=$_POST['search'];
         $data =  $this->karigar_report_model->get('','',$search,'');
       
         $this->excel_lib->export($data,$headings,$filename,$main_heading=array(),$image=array(),$save=false);
      }
} //class
