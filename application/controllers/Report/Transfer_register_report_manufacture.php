<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Transfer_register_report_manufacture extends CI_Controller {

  public function __construct() {
    parent::__construct();
     if(empty($this->session->userdata('user_id'))){
        redirect(ADMIN_PATH . 'auth/logout');
     	}
      	$this->breadcrumbs->push("Manufacture Report", "Manufacture_report");
      	$this->load->model(array('report/Transfer_register_report_model'));
    	$this->load->config('admin_validationrules', TRUE);
    	$this->load->library(array('Data_encryption','excel_lib'));
    }

    public function index(){
        $this->breadcrumbs->push("Transfer Register Report", "Manufacture_report/Transfer_register_report_manufacture");
        $data['page_title']="Transfer Register Report";
        $data['display_title'] = "Transfer Register Report";
        $data['result']=$this->Transfer_register_report_model->get();
        $this->view->render('report/manufacture/transfer_register_report',$data);
            
    }

      public function download(){
        $filename="Order_status_report".date('YmdHis').".xlsx";
        $headings = array('Order Name','Order Weigth','Total Pcs');

         $data =  $this->Transfer_register_report_model->get();
       
         $this->excel_lib->export($data,$headings,$filename,$main_heading=array(),$image=array(),$save=false);
      }
}//class