<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Kundan_Karigarwise_detail_report_manufacture extends CI_Controller {

  public function __construct() {
    parent::__construct();
     if(empty($this->session->userdata('user_id'))){
        redirect(ADMIN_PATH . 'auth/logout');
     	}
      	$this->breadcrumbs->push("Manufacture Report", "Manufacture_report");
      	$this->load->model(array('report/Kundan_karigar_detail_report_model'));
    	$this->load->config('admin_validationrules', TRUE);
    	$this->load->library(array('Data_encryption','excel_lib'));
    }

    public function index(){
        $this->breadcrumbs->push("Kundan Karigar-wise Detail Report", "Manufacture_report/Kundan_karigar_report_manufacture");
        $data['page_title']="Kundan Karigar-wise Detail Report";
        $data['display_title'] = "Kundan Karigar-wise Detail Report";
        $data['result']=$this->Kundan_karigar_detail_report_model->get();
        $this->view->render('report/manufacture/kundan_karigar_detail_report',$data);
            
    }

      public function download(){
        $filename="Order_status_report".date('YmdHis').".xlsx";
        $headings = array('Order Name','Order Weigth','Total Pcs');

         $data =  $this->Kundan_karigar_detail_report_model->get();
       
         $this->excel_lib->export($data,$headings,$filename,$main_heading=array(),$image=array(),$save=false);
      }
}//class