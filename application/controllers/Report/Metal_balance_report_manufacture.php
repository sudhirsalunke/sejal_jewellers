<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Metal_balance_report_manufacture extends CI_Controller {

  public function __construct() {
    parent::__construct();
     if(empty($this->session->userdata('user_id'))){
        redirect(ADMIN_PATH . 'auth/logout');
     	}
      	$this->breadcrumbs->push("Manufacture Report", "Manufacture_report");
      	$this->load->model(array('report/Metal_balance_report_model'));
    	$this->load->config('admin_validationrules', TRUE);
    	$this->load->library(array('Data_encryption','excel_lib'));
    }

    public function index(){
        $this->breadcrumbs->push("Metal Balance Ledger Report", "Manufacture_report/Metal_balance_report_manufacture");
        $data['page_title']="Metal Balance Ledger";
        $data['display_title'] = "Metal Balance Ledger Report";
        $data['result']=$this->Metal_balance_report_model->get();
        $this->view->render('report/manufacture/metal_balance_report',$data);
            
    }

      public function download(){
        $filename="Order_status_report".date('YmdHis').".xlsx";
        $headings = array('Order Name','Order Weigth','Total Pcs');

         $data =  $this->Metal_balance_report_model->get();
       
         $this->excel_lib->export($data,$headings,$filename,$main_heading=array(),$image=array(),$save=false);
      }
}//class