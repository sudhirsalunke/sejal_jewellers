<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Order_report extends CI_Controller {

  public function __construct() {
    parent::__construct();
    if(empty($this->session->userdata('user_id'))){
    redirect(ADMIN_PATH . 'auth/logout');
    }
    $this->breadcrumbs->push("Manufacture Report", "Report/Order_report");
      
  }

  public function index(){
    $data['page_title']="Order Report";
    $data['display_title'] = "Order Report";
    $this->breadcrumbs->push("Order Report", "Report/Order_report");
    $today = date('Y-m-d');
    $data['all_reports'] = array(
                              array('name' => 'Karigar Order tracking',
                                    'path' => 'Order_report/order_tracking_report/1'),
                              array('name' => 'Customer order tracking',
                                    'path' =>'Order_report/order_tracking_report/2'),
                              array('name' => 'Daily order received',
                                    'path' =>'Order_report/daily_order_received?today_date='.$today.''),
                             array('name' => 'Daily product received from karigar',
                                    'path' =>'Order_report/daily_order_received_from_karigar?today_date='.$today.''), 
                             array('name' => 'Daily Report Summary',
                                    'path' =>'Order_report/daily_report_summary?today_date='.$today.''),
                             array('name' => 'In stock',
                                    'path' =>'Order_report/order_tracking_report/3'),
                             array('name' => 'Delivery in Quantity',
                                    'path' =>'Order_report/delivery_report_in_quantity?from='.$today.'&to='.$today.'&type=department'),
                            array('name' => 'Karigar Delivery Performance',
                                    'path' =>'Order_report/delivery_performance/1'),
                            array('name' => 'Party Delivery Performance',
                                    'path' =>'Order_report/delivery_performance/2'),
                            array('name' => 'Client Order Ledger',
                                   'path' =>'Order_report/order_ledger/1?from='.$today.'&to='.$today.''),
                            array('name' => 'Karigar Order Ledger',
                                   'path' =>'Order_report/order_ledger/2?from='.$today.'&to='.$today.''),
                            array('name' => 'Sales and Purchase Report',
                                   'path' =>'Order_report/sales_purchase_report?from='.$today.'&to='.$today.''),
                                                        
                             
                            );
    //print_r($data);die;
    $this->view->render('report/order_module/index',$data);
  }


}//class