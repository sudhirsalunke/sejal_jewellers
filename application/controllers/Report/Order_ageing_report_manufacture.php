<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Order_ageing_report_manufacture extends CI_Controller {

  public function __construct() {
    parent::__construct();
     if(empty($this->session->userdata('user_id'))){
        redirect(ADMIN_PATH . 'auth/logout');
     	}
      	$this->breadcrumbs->push("Manufacture Report", "Manufacture_report");
      	$this->load->model(array('report/Order_ageing_report_model'));
    	$this->load->config('admin_validationrules', TRUE);
    	$this->load->library(array('Data_encryption','excel_lib'));
    }

    public function index(){
        
        $this->breadcrumbs->push("Order Ageing Report", "Manufacture_report/Order_ageing_report_manufacture");
        $data=$this->Order_ageing_report_model->get();
        $data['page_title']="Order Ageing Report";
        $data['display_title'] = "Order Ageing Report";
        $this->view->render('report/manufacture/order_ageing_report',$data);
            
    }

      public function download(){
        $filename="Order_status_report".date('YmdHis').".xlsx";
        $headings = array('Order Name','Order Weigth','Total Pcs');

         $data =  $this->Order_ageing_report_model->get();
       
         $this->excel_lib->export($data,$headings,$filename,$main_heading=array(),$image=array(),$save=false);
      }
}//class