<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Itemwise_stock_report_manufacture extends CI_Controller {

  public function __construct() {
    parent::__construct();
     if(empty($this->session->userdata('user_id'))){
        redirect(ADMIN_PATH . 'auth/logout');
     	}
      	$this->breadcrumbs->push("Manufacture Report", "Manufacture_report");
      	$this->load->model(array('report/Itemwise_stock_report_model'));
    	$this->load->config('admin_validationrules', TRUE);
    	$this->load->library(array('Data_encryption','excel_lib'));
    }

    public function index(){
        $this->breadcrumbs->push("Item-Wise Stock Report", "Manufacture_report/Itemwise_stock_report_manufacture");
        $data['page_title']="Item-Wise Stock Report";
        $data['display_title'] = "Item-Wise Stock Report";
        $data['result']=$this->Itemwise_stock_report_model->get();
        $this->view->render('report/manufacture/itemwise_stock_report',$data);
            
    }

      public function download(){
        $filename="Order_status_report".date('YmdHis').".xlsx";
        $headings = array('Order Name','Order Weigth','Total Pcs');

         $data =  $this->Itemwise_stock_report_model->get();
       
         $this->excel_lib->export($data,$headings,$filename,$main_heading=array(),$image=array(),$save=false);
      }
}//class