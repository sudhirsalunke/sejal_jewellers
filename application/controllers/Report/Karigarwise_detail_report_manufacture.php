<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Karigarwise_detail_report_manufacture extends CI_Controller {

  public function __construct() {
    parent::__construct();
     if(empty($this->session->userdata('user_id'))){
        redirect(ADMIN_PATH . 'auth/logout');
     	}
      	$this->breadcrumbs->push("Manufacture Report", "Manufacture_report");
      	$this->load->model(array('report/karigarwise_detail_report_model'));
    	$this->load->config('admin_validationrules', TRUE);
    	$this->load->library(array('Data_encryption','excel_lib'));
    }

    public function index(){
        $this->breadcrumbs->push("Karigar-Wise Detail Report", "Manufacture_report/karigarwise_detail_report_manufacture");
        $data=$this->karigarwise_detail_report_model->get();
        $data['page_title']="Karigar-Wise DetailReport";
        $data['display_title'] = "Karigar-Wise Detail Report";
       	//print_r($data);exit;
        $this->view->render('report/manufacture/karigarwise_detail_report',$data);
            
    }

      public function download(){
        $filename="Order_status_report".date('YmdHis').".xlsx";
        $headings = array('Order Name','Order Weigth','Total Pcs');

         $data =  $this->karigarwise_detail_report_model->get();
       
         $this->excel_lib->export($data,$headings,$filename,$main_heading=array(),$image=array(),$save=false);
      }
}//class