<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Report_manufacture extends CI_Controller {

  public function __construct() {
    parent::__construct();
    if(empty($this->session->userdata('user_id'))){
    redirect(ADMIN_PATH . 'auth/logout');
    }
    $this->breadcrumbs->push("Manufacture Report", "Report/Report_manufacture");
      
  }

  public function index(){
    $data['page_title']="Manufacture Report";
    $data['display_title'] = "Manufacturing Report";
    $this->breadcrumbs->push("Karigar Report", "Report/Report_manufacture");
    $data['all_reports'] = array(
                              array('name' => 'Daily stock report',
                                    'path' => 'Manufacture_report/Daily_stock_summary_report'),
                                    /* 
                             array('name' => 'Order Status Report',
                                    'path' => 'Manufacture_report/order_status'),
                             array('name' => 'Stock Summary Report',
                                    'path' =>'Manufacture_report/stock_summary'),
                              array('name' => 'Order Summary Report',
                                    'path' =>'Manufacture_report/order_summary'),
                              array('name' => 'Stock Detail Summary Report',
                                    'path' =>'Manufacture_report/Stock_detail_summary'),
                              array('name' => 'Minimum sub category Report',
                                    'path' =>'Manufacture_report/Minimum_subcategory'),
                              array('name' => 'Order Ageing Report',
                                    'path' =>'Manufacture_report/Order_ageing'),
                              array('name' => 'Category Order Ageing Report',
                                    'path' =>'Manufacture_report/Category_order_ageing'),
                              array('name' => 'Stock Ageing Report',
                                    'path' =>'Manufacture_report/Stock_ageing'),
                              array('name' => 'Category Stock Ageing Report',
                                    'path' =>'Manufacture_report/Stock_ageing'),
                              array('name' => 'Karigar Summary Report',
                                    'path' => 'Manufacture_report/karigar'),
                              array('name' => 'Karigar-wise Detail Report',
                                    'path' =>'Manufacture_report/karigarwise_detail'),
                              array('name' => 'QC Report',
                                    'path' =>'Manufacture_report/qc_report'),
                              array('name' => 'Kundan QC Report',
                                    'path' =>'Manufacture_report/kundan_qc_report'),
                              array('name' => 'Item-wise Stock Report',
                                    'path' =>'Manufacture_report/itemwise_stock'),
                              array('name' => 'Transfer Register Report',
                                    'path' =>'Manufacture_report/transfer_register'),
                              array('name' => 'Kundan Karigar Summary Report',
                                    'path' => 'Manufacture_report/kundan_karigar_summary'),
                              array('name' => 'Kundan Karigar-wise Detail Report',
                                    'path' =>'Manufacture_report/kundan_karigarwise_detail'),
                              array('name' => 'Kundan Report',
                                    'path' =>'Manufacture_report/kundan_karigar'),
                              array('name' => 'Metal Balance Ledger Report',
                                    'path' =>'Manufacture_report/metal_balance'),*/
                             
                            );
    $this->view->render('report/manufacture/index',$data);
  }

  public function order_summary(){
    $this->db->select('gross_wt gross_wt,rp.quantity,po.manufacturing_order_id,tp.weight_range_id');
    $this->db->from('department_ready_product drp');
    $this->db->join('quality_control qc','drp.qc_id = qc.id');
    $this->db->join('Receive_products rp','rp.id = qc.receive_product_id');
    $this->db->join('prepare_kundan_karigar_order po','po.id = rp.kmm_id');
    $this->db->join('make_set_quantity_relation msqr','msqr.id = po.msqr_id');
    $this->db->join('tag_products tp','tp.id = msqr.tag_id');
    $this->db->where('drp.status',1);
    // $this->db->group_by('po.manufacturing_order_id');
    // $this->db->group_by('tp.weight_range_id');
    $result = $this->db->get()->result_array();
    echo '<pre>';
    print_r($result);
    echo '</pre>';die;
    // foreach ($result as $key => $value) {
    //     # code...
    // }
  }
  public function qc_report(){
    $this->db->select('SUM(IF(qc.status ="3",qc.status,0)) as reject,SUM(IF(qc.status <="8",qc.status,0)) as accepted');
    $this->db->from('quality_control qc');
    // $this->db->join('Receive_products rp','rp.id = qc.receive_product_id');
    // $this->db->join('Karigar_manufacturing_mapping kmm','kmm.id = rp.kmm_id');
    $result = $this->db->get()->result_array();
    echo '<pre>';
    print_r($result);
    echo '</pre>';die;
  }

 

}//class