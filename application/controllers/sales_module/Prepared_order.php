<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Prepared_order extends CI_Controller {

  public function __construct() {
    parent::__construct();
    if(empty($this->session->userdata('user_id'))){
      redirect(ADMIN_PATH . 'auth/logout');
    }
    $this->breadcrumbs->push("Sales Module", "sales_dashboard");
    
    $this->load->model(array('sales_module/Prepared_order_model','sale_master/Parent_category_model','Weight_range_model','Manufacturing_module/Department_model'));
    $this->load->config('admin_validationrules', TRUE);
    $this->load->library('Data_encryption');
  }

  public function index(){

    $data['page_title'] = "All Prepared Orders";
    
    $list=$this->input->post('list');
    if($list !="")
    {
      echo json_encode($this->generate_data_table());
    }else{
      if (@$_GET['status'] != "sent") {
       $data['display_title'] = "All Prepared Orders";
       $this->breadcrumbs->push("Prepared Order", "prepared_order");
      }else{
       $data['display_title'] = "Sent Orders";
       $this->breadcrumbs->push("Sent Order", "prepared_order?status=sent");
      }
      $this->view->render('sales_module/prepared_order/index',$data);
    }
  }

  public function create(){
    $this->breadcrumbs->push("Prepared Orders", "Prepared_order");
    $this->breadcrumbs->push("Create", "prepared_order/create");
    $data['page_title'] = "Prepared Order";
    $data['order_id']=$this->Prepared_order_model->get_order_id();
    $data['parent_category']=$this->Parent_category_model->get('','','','',true);
    $data['weight_range']=$this->Weight_range_model->get('','','','',true);
    $data['department'] =$this->Department_model->get('','','','',true);
    $this->view->render('sales_module/prepared_order/create',$data);
  }

  private function validation_result(){
    $data = array();
    $validationResult = $this->Prepared_order_model->validatepostdata();

    if(count($validationResult) != 0){
      $data['status']= 'failure';
      $data['data']= '';
      $data['error'] = $validationResult;

    }
    return $data;
  }

  public function store(){
    $data = array();
    $validationResult = $this->validation_result();
   if(count($validationResult) == 0){
      $data['status']= 'success';
      $data['data']= '';
      $data['user_id'] = $this->Prepared_order_model->store();
      $validationResult=$data;
    }
    echo json_encode($validationResult);
  }

  public function send_to_manufacture(){
    if (empty($_POST['id']) || $_POST['status'] =="") {
      $data['status']="error";
    }else{
      $data =$this->Prepared_order_model->send_to_manufacture($_POST['id'],$_POST['status']);
    }

    echo json_encode($data);
  }
  public function edit($id){
    $this->breadcrumbs->push("Prepared Orders", "Prepared_order");
    $this->breadcrumbs->push("Edit Order", "prepared_order/edit");
    $data['page_title'] = "Prepared Order";
    $data['order']=$this->Prepared_order_model->find($id);
    $data['order_details']=$this->Prepared_order_model->order_details($id);
    $data['parent_category']=$this->Parent_category_model->get();
    $data['weight_range']=$this->Weight_range_model->get();
    $data['department'] =$this->Department_model->get();
    $this->view->render('sales_module/prepared_order/edit',$data);
  }
  
  public function show($id){
    $this->breadcrumbs->push("Prepared Orders", "Prepared_order");
    $this->breadcrumbs->push("View Order", "prepared_order/view");
    $data['page_title'] = "Prepared Order";
    $data['order_details']=$this->Prepared_order_model->show_page_data($id);
    $data['order_id'] =$id;
    //print_r($data);exit;
    $this->view->render('sales_module/prepared_order/view',$data);
  }

  public function update(){
    $data = $this->validation_result();
   if(count($data) == 0){
      $data = $this->Prepared_order_model->update();
    }
    echo json_encode($data);
   
  }
 
  private function generate_data_table(){
    $status='';
    $filter_status =@$_REQUEST['order'][0];
    $search=@$_REQUEST['search']['value'];
    $result = $this->Prepared_order_model->get($filter_status,$status,$_REQUEST,$search,$limit=true);
    $totalRecords =sizeof($this->Prepared_order_model->get($filter_status,$status,$_REQUEST,$search,$limit=false));
    $start=@$_REQUEST['start'];
    if (!empty($result)) {
        foreach ($result as $key => $value) {
          $start++;
            $button_html="";
            if (@$_GET['status'] != "sent"){

               $button_html .= '<a class="btn btn-link edit_link small loader-hide  btn-sm" href="'.ADMIN_PATH.'Prepared_order/edit/'.$value['id'].'">Edit</a>  <a class="btn btn-link view_link small loader-hide  btn-sm" href="'.ADMIN_PATH.'Prepared_order/show/'.$value['id'].'">View</a> <button onclick="sale_tp_manufacture('.$value['id'].',1)" id="btn_'.$value['id'].'" class="btn btn-link small loader-hide  btn-sm" name="commit" type="button">SEND TO MANUFACTURING</button></a>';

            }else{
                $button_html .=' <a class="btn btn-link view_link small loader-hide btn-sm" href="'.ADMIN_PATH.'Prepared_order/show/'.$value['id'].'">View</a> ';
              if ($value['status']=='0'|| is_null($value['status'])) {
                 $button_html .= '<button onclick="sale_tp_manufacture('.$value['id'].',0)" id="btn_'.$value['id'].'" class="btn btn-link delete_link small loader-hide  btn-sm" name="commit" type="button">CANCEL</button></a>';
                 
              }
            }

            $data[$key][0] ='<span style="float:right">'.$start.'</span>';
            $data[$key][1] ='<span style="float:right">'.$value["id"].'</span>';
            $data[$key][2] =$value["order_name"];
            $data[$key][3] =$value['d_name'];
            $data[$key][4] ='<span style="float:right">'.$value['categories'].'</span>';
            $data[$key][5] ='<span style="float:right">'.$value['total_qty'].'</span>';
            $data[$key][6] =$button_html;
            
     
           
        }
    }else{
        $data[0][0] = ['No data found'];
        $data[0][1] = [];
        $data[0][2] = [];
        $data[0][3] = [];
        $data[0][4] = [];
        $data[0][5] = [];
        $data[0][6] = [];
       
        
    }
     $json_data = array(
          "draw" => intval($_REQUEST['draw']),
          "recordsTotal" => intval($totalRecords),
          "recordsFiltered" => intval($totalRecords),
          "data" => $data
      );
    return $json_data; 
  }


}