<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Sales_invoice extends CI_Controller {

  public function __construct() {
    parent::__construct();
    if(empty($this->session->userdata('user_id'))){
      redirect(ADMIN_PATH . 'auth/logout');
    }
    $this->breadcrumbs->push("Sales Module", "sales_dashboard");
    $this->load->model(array('sales_module/Sales_stock_model',
                            'sale_master/Customer_model',
                            // 'sale_master/Kundan_category_model',
                            // 'sale_master/Color_stone_model',
                            'sale_master/Type_master_model',
                            'sales_module/Sales_invoice_model',
                            'sale_master/Variations_category_model'
                            ));
    $this->load->config('admin_validationrules', TRUE);
    $this->load->library(array('Data_encryption','m_pdf'));
  }

  public function index(){

    $data['page_title'] = "Sales Invoice";
    $list=$this->input->post('list');
    if($list !="")
    {
      echo json_encode($this->generate_data_table());
    }else{
       if (@$_GET['status'] !="sent") {
          $this->breadcrumbs->push("Invoice", "sales_invoice");
           $data['display_title'] = "All Invoices";
        }else{
          $this->breadcrumbs->push("Sent To Dispatch", "sales_invoice?status=sent");
           $data['display_title'] = "Dispatch Invoices";
        }
      $this->view->render('sales_module/invoice/index',$data);
    }
  }

  public function create(){
    $this->breadcrumbs->push("Invoice", "sales_invoice");
    $this->breadcrumbs->push("Generate Invoice", "sales_stock/invoice");
    $data['page_title'] = "Invoice";
    $data['display_title'] = "Generate Invoice";
    
    $data['chitti_no_temp']=$this->Sales_invoice_model->temp_chitti_no();
    $data['all_customers']=$this->Customer_model->get('','','','',true);
    $data['all_types']=$this->Type_master_model->get();
    // $data['all_kundan_category']=$this->Kundan_category_model->get();
    // $data['all_cs']=$this->Color_stone_model->get();
    $data['variations']=$this->Variations_category_model->get();

    $this->view->render('sales_module/invoice/create',$data);
  }

  public function edit($invoice_id){
    $this->breadcrumbs->push("Invoice", "sales_invoice");
    $this->breadcrumbs->push("Edit Invoice", "sales_stock/edit/$invoice_id");
    $data['page_title'] = "Edit Invoice";
    $data['display_title'] = "Edit Invoice";

    $data['invoice']= $this->Sales_invoice_model->find($invoice_id);
    $data['invoice_details']= $this->Sales_invoice_model->invoice_details($invoice_id);
    $data['all_customers']=$this->Customer_model->get();
    $data['all_types']=$this->Type_master_model->get();
    // $data['all_kundan_category']=$this->Kundan_category_model->get();
    // $data['all_cs']=$this->Color_stone_model->get();
    $data['variations']=$this->Variations_category_model->get();
    //print_r($data);exit;
    $this->view->render('sales_module/invoice/edit',$data);
  }

  public function show($invoice_id){
    $this->breadcrumbs->push("Invoice", "sales_invoice");
    $this->breadcrumbs->push("Chitti No: ".$invoice_id."", "sales_invoice/show/$invoice_id");
    $data['page_title'] = "View Invoice";
    $data['display_title'] = "View Invoice : $invoice_id";
    $data['invoice']= $this->Sales_invoice_model->find_view_data($invoice_id);
    $data['invoice_details']= $this->Sales_invoice_model->invoice_details($invoice_id);
    //print_r($data);
    $this->view->render('sales_module/invoice/show',$data);

  }

  public function print_invoice($invoice_id){
    $data['invoice']= $this->Sales_invoice_model->find_view_data($invoice_id);
    $data['invoice_details']= $this->Sales_invoice_model->invoice_details($invoice_id);
    $this->load->view('sales_module/invoice/print', $data);
    //$html=$this->load->view('sales_module/invoice/print', $data, true);
    
    // $pdfFilePath = "Print_order_".$invoice_id.".pdf";
    // $this->m_pdf->pdf->WriteHTML($html);
    // $this->m_pdf->pdf->Output($pdfFilePath, "D");

  }

  private function validation_result(){
    $data = array();
    $validationResult = $this->Sales_invoice_model->validatepostdata();
    $invoice_errors=$this->Sales_invoice_model->validate_invoice_details();
    if($validationResult===FALSE || !empty($invoice_errors)){
      $data['status']= 'failure';
      $data['data']= '';
      $data['error'] = array(
        /*basic details*/
        'transaction_date'=>strip_tags(form_error('invoice_details[transaction_date]')),
        'customer_id'=>strip_tags(form_error('invoice_details[customer_id]')),
        'color_stone_id'=>strip_tags(form_error('invoice_details[color_stone_id]')),
        'kundan_category_id'=>strip_tags(form_error('invoice_details[kundan_category_id]')),
        /*invoice details*/
        'type'=>strip_tags(form_error('invoice[type][]')),
        'item_no'=>strip_tags(form_error('invoice[item_no][]')),
        'cs_wt'=>strip_tags(form_error('invoice[cs_wt][]')),
        'kundan_wt'=>strip_tags(form_error('invoice[kundan_wt][]')),
        'cs_amt'=>strip_tags(form_error('invoice[cs_amt][]')),
        'kundan_amt'=>strip_tags(form_error('invoice[kundan_amt][]')),
        'rodo'=>strip_tags(form_error('invoice[rodo][]')),
        'radi'=>strip_tags(form_error('invoice[radi][]')),
        'other'=>strip_tags(form_error('invoice[other][]')),
        'all_invoice'=>@$invoice_errors,
      );
    }
    return $data;
  }

  public function store(){
    //$data = $this->validation_result();
    $data ='';
    if (empty($data)) {
      $data=$this->Sales_invoice_model->store();
    }
    echo json_encode($data);
   
  }

  public function update(){
    $data = $this->validation_result();
    if (empty($data)) {
      $data=$this->Sales_invoice_model->update();
    }
    echo json_encode($data);
   
  }

  public function send_to_dispatch(){
    $data=$this->Sales_invoice_model->send_to_dispatch();
    echo json_encode($data);
  }


  private function generate_data_table(){
    $filter_status =@$_REQUEST['order'][0];
    $search=@$_REQUEST['search']['value'];
    $result = $this->Sales_invoice_model->get($filter_status,$_REQUEST,$search,$limit=true);
    $totalRecords = sizeof($this->Sales_invoice_model->get($filter_status,$_REQUEST,$search,$limit=false));
    if (!empty($result)) {
        foreach ($result as $key => $value) {
        $btn_html=""; 
        if (@$_GET['status'] !="sent") {
         $btn_html .="<a class='btn btn-info small loader-hide btn-sm' href='sales_invoice/edit/".$value['chitti_no']."'>Edit</a> <a class='btn btn-link view_link small loader-hide btn-sm' target='_blank' href='sales_invoice/show/".$value['chitti_no']."'>View</a> <button class='btn btn-success small loader-hide btn-sm' onclick='send_to_dispatch(this,".$value['chitti_no'].")'>Send To Dispatch Department</button>";
        }else{
           $btn_html .=" <a class='btn btn-link view_link small loader-hide btn-sm' target='_blank' href='sales_invoice/show/".$value['chitti_no']."'>View</a> ";
        }

          $sr_no=$_REQUEST['start'] + $key+1;
          $data[$key][0] ='<span style="float:right">'.$sr_no.'</span>';
          $data[$key][1] = '<span style="float:right">'. $value["chitti_no"].'</span>';
          $data[$key][2] = '<span style="float:right">'.$value["products"].'</span>';
          $data[$key][3] = '<span style="float:right">'. $value["total_pcs"].'</span>';
          $data[$key][4] = $value["customer_name"];
          $data[$key][5] = $value["color_stone_name"];
          $data[$key][6] = $value["kundan_category_name"];
          $data[$key][7] = '<span style="float:right">'. $value["gross_wt"].'</span>';
          $data[$key][8] = '<span style="float:right">'. $value["total_amt"].'</span>';
          $data[$key][9] = $value["created_at"];
          $data[$key][10] = $btn_html;
        }
    }else{
      $txt='No data found';
      for ($i=0; $i <=10 ; $i++) { 
        $data[0][$i] = [$txt];
        $txt="";
      }
    }

     $json_data = array(
          "draw" => intval($_REQUEST['draw']),
          "recordsTotal" => intval($totalRecords),
          "recordsFiltered" => intval($totalRecords),
          "data" => $data
      );

    return $json_data; 
  }

}//class