<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Sales_recieved_product extends CI_Controller {

  public function __construct() {
    parent::__construct();
    if(empty($this->session->userdata('user_id'))){
      redirect(ADMIN_PATH . 'auth/logout');
    }
    $this->breadcrumbs->push("Sales", "sales_dashboard");
    
    $this->load->model(array('sales_module/Sales_recieved_product_model','Sub_category_model','Weight_range_model','Manufacturing_module/Department_model','Manufacturing_module/Ready_product_model'));
    $this->load->config('admin_validationrules', TRUE);
    $this->load->library('Data_encryption');
  }

  public function index(){
    $department_id=$this->session->userdata('department_id');
    $data['page_title'] = "All Recieved Products";
    $data['display_title'] = "Approve / Reject Products";
    $data['department_id'] =$department_id;
    $department=@$_GET['department'];
    $list=$this->input->post('list');
    if($list !="")
    {
      echo json_encode($this->generate_data_table($department_id,$department));
    }else{

       if (@$_GET['status']=="sended_to_manufacture") {
          $data['display_title'] = "Returned To Manufacture";
          $this->breadcrumbs->push("Returned To Manufacture", "Sales_recieved_product?status=sended_to_manufacture");
          $data['th']=array('#','Chitti No','Rejected From Chitti No','Total Qty','Gross Weight','Date','');
        }else{
          $this->breadcrumbs->push("Received Products", "Sales_recieved_product");
          $data['th']=array('#','Chitti No','Total Qty','Gross Weight','Date','');
        }
      $this->view->render('sales_module/recieved_product/index',$data);
    }
  }

  public function show($voucher_no=''){
    $data['page_title'] = "All Recieved Products Show";
    $data['display_title'] = "All Received Products";
    $data['chitti_no']=$voucher_no;
    $data['voucher_no']=$voucher_no;
    $department_id=$this->session->userdata('department_id');
    $location_id=$this->session->userdata('location_id');
    $data['department_id']=$department_id;
    $data['location_id']=$location_id;
    //print_r($voucher_no);die;
    $list=$this->input->post('list');
    if($list !="")
    {
      echo json_encode($this->generate_show_approve_reject_data_table($voucher_no));
    }else{

       if (@$_GET['status']=="sended_to_manufacture") {
          $data['display_title'] = "Returned To Manufacture";
          $this->breadcrumbs->push("Returned To Manufacture", "Sales_recieved_product?status=sended_to_manufacture");
        }else{
          $this->breadcrumbs->push("Received Products", "Sales_recieved_product");
        }
        $this->breadcrumbs->push("Products", "#products");
        $select_all_products='<div class="checkbox checkbox-purple"><input  type="checkbox" id="select_check_box_sales"><label for="select_check_box_sales"></label></div>';
      $data['th']=array('','Parent Category','Product Code','Gross Weight','Net Weight','Quantity');
      
        if (empty($_GET['status'])) {
          array_unshift($data['th'],$select_all_products);

        }
      $this->view->render('sales_module/recieved_product/show',$data);
    }
  }

 

  public function create(){
    $this->breadcrumbs->push("Prepared Order", "prepared_order/create");
    $data['page_title'] = "Prepared Order";
    $data['sub_category']=$this->Sub_category_model->get();
    $data['weight_range']=$this->Weight_range_model->get();
    $data['department'] =$this->Department_model->get();
    $this->view->render('sales_module/prepared_order/create',$data);
  }

  private function validation_result(){
    $data = array();
    $validationResult = $this->Sales_recieved_product_model->validatepostdata();
    if($validationResult===FALSE){
      $data['status']= 'failure';
      $data['data']= '';
      $data['error'] = array(
        'department_id'=>strip_tags(form_error('product[department_id]')),
        'weight'=>strip_tags(form_error('product[weight][]')),
        'quantity'=>strip_tags(form_error('product[quantity][]')),
        'sub_category'=>strip_tags(form_error('product[sub_category][]')),
        
      );


    }
    return $data;
  }

  public function store(){
    $data = array();
    $validationResult = $this->validation_result();
   if(count($validationResult) == 0){
      $data['status']= 'success';
      $data['data']= '';
      $data['user_id'] = $this->Sales_recieved_product_model->store();
      $validationResult=$data;
    }
    echo json_encode($validationResult);
  }

  public function send_to_manufacture(){
    if (empty($_POST['id'])) {
      $data['status']="error";
    }else{
      $data =$this->Sales_recieved_product_model->send_to_manufacture($_POST['id']);
    }

    echo json_encode($data);
  }
 
  private function generate_data_table($department_id,$department){
    $status='';
    $filter_status =@$_REQUEST['order'][0];
    $search=@$_REQUEST['search']['value'];
    $result = $this->Sales_recieved_product_model->get($filter_status,$status,$_REQUEST,$search,$limit=true,$department_id,$department);
    $totalRecords =$this->Sales_recieved_product_model->get($filter_status,$status,$_REQUEST,$search,$limit=false,$department_id,$department);
//print_r($result);die;
       if (!empty($result)) {
          foreach ($result as $key => $value) {
          $btn_html="";
          
          /*  $btn_html.="<a href='".ADMIN_PATH."sales_recieved_product/show/".$value["chitti_no"]."?status=".@$_GET['status']."' class='btn btn-sm btn-warning'>View</a>";*/
            $btn_html.="<a href='".ADMIN_PATH."sales_recieved_product/show/".$value["voucher_no"]."?status=".@$_GET['status']."' class='btn btn-sm btn-link view_link'>View</a>";
      
            $sr_no=$key+1;
            if(@$_GET['status']=="sended_to_manufacture"){
                 $data[$key][] ='<span style="float:right">'.$sr_no.'</span>';
            }else{
                $data[$key][] = '<div class="checkbox checkbox-purple"><input id="checkbox_'.$sr_no.'" type="checkbox" name="voucher_no[]" value="'.$value["voucher_no"].'"><label for="checkbox'.$sr_no.'"></label></div>';
            }
          /*  $data[$key][] ='<span style="float:right">'.$value["chitti_no"].'</span>';*/
            $data[$key][] ='<span style="float:right">'.$value["voucher_no"].'</span>';
            if(@$_GET['status']=="sended_to_manufacture"){
              $data[$key][] ='<span style="float:right">'.$value["rejected_chitti_no"].'</span>';
            }
            if($department_id!="2" && $department_id!='3' && $department_id !='6' && $department_id!='10'){
            $data[$key][] ='<span style="float:right">'.$value["total_qty"].'</span>';
            }
            $data[$key][] ='<span style="float:right">'.round($value["total_gross"],2).'</span>';
             if(@$_GET['status']=="sended_to_manufacture"){
              $data[$key][] ='<span style="float:right">'.$value["rejected_date"].'</span>';
            }else{
            $data[$key][] ='<span style="float:right">'.$value["sales_date"].'</span>';
            }
            $data[$key][] =$btn_html;
          }
    }else{
        for ($i=0; $i <=6 ; $i++) { 
          if ($i==0) {
            $data[0][$i]="No Data Found";
          }else{
            $data[0][$i]="";
          }
          
        }
       
        
    }
    return $this->json_data($data,$totalRecords);
  }


  private function generate_show_data_table($voucher_no){
    $status=@$_GET['status'];
    $filter_status =@$_REQUEST['order'][0];
    $search=@$_REQUEST['search']['value'];
    $department_id=$this->session->userdata('department_id');
    // $result = $this->Sales_recieved_product_model->get_chitti_wise($chitti_no,$filter_status,$status,$_REQUEST,$search,$limit=true,$department_id);
    // $totalRecords =sizeof($this->Sales_recieved_product_model->get_chitti_wise($chitti_no,$filter_status,$status,$_REQUEST,$search,$limit=false,$department_id));
    $result = $this->Sales_recieved_product_model->get_voucher_no_wise($voucher_no,$filter_status,$status,$_REQUEST,$search,$limit=true,$department_id);
    
    //echo $this->db->last_query(); print_r($result); die;
    $totalRecords =$this->Sales_recieved_product_model->get_voucher_no_wise($voucher_no,$filter_status,$status,$_REQUEST,$search,$limit=false,$department_id);

       if (!empty($result)) {
          foreach ($result as $key => $value) {
          $btn_html="";
           if (@$_GET['status'] != "sended_to_manufacture"){
            $btn_html.="<button id='btn_".$value['id']."' onclick='send_to_salesstock(".$value['id'].")' type='button' class='btn btn-md btn-link view_link'>Approve</button>";
            $btn_html.=" <button onclick='send_to_manufacture(".$value['id'].")' type='button' class='btn btn-md btn-link delete_link'>Reject</button>";
              }
          if (empty($_GET['status'])) {
            $data[$key][] ='<div class="checkbox checkbox-purple"><input id="checkbox'.$key.'" type="checkbox" name="drp_id[]" value="'.$value['id'].'"><label for="checkbox'.$key.'"></label></div>';
          }else{
            $sr_no=$key+1;
            //$data[$key][] = '<span style="float:right">'.$sr_no.'</span>';
            $data[$key][] = '<div class="checkbox checkbox-purpal"><input type="checkbox" name="rp[]" id="checkbox_'.$value['id'].'" class="form-control checkbox_ctn" value="'.$value['id'].'"><label for="checkbox_'.$value['id'].'"></label></div>';
          }
             
            if(empty($_GET['status'])) {
              $button_html = '<span class="pull-right"><a href="'.ADMIN_PATH.'Sales_recieved_product/view/'.$value['product_code'].'"><button class="btn btn-link view_link small loader-hide btn-sm" name="commit" type="button" >View</button></a>&nbsp;</span>';  
              if($department_id!='2' && $department_id !='3' && $department_id !='6' && $department_id !='10'){
                $data[$key][] ='<a href="'.ADMIN_PATH.'Ready_product/view/'.$value['product_code'].'">'.$value['product_code'].'</a>';
              }            
            }else {

              $button_html = '<span class="pull-right"><a href="'.ADMIN_PATH.'Sales_recieved_product/view/'.$value['product_code'].'?status=rejected_chitti"><button class="btn btn-link view_link small loader-hide btn-sm" name="commit" type="button" >View</button></a>&nbsp;</span>';
              if($department_id!="2" && $department_id !='3' && $department_id !='6' && $department_id !='10'){
                $data[$key][] ='<a href="'.ADMIN_PATH.'Sales_recieved_product/view/'.$value['product_code'].'?status=rejected_chitti">'.$value['product_code'].'</a>';
              }
            }
         

            $data[$key][] =$value["product"];
            $data[$key][] ='<input type="hidden" name="product_code[]" value="'.$value["product_code"].'" > <span style="float:right">'.round($value["gr_wt"],2).'</span>';
            $data[$key][] ='<span style="float:right">'.round($value["net_wt"],2).'</span>';

            $data[$key][] ='<span style="float:right">'.$value["quantity"].'</span>';
            $data[$key][] ='<span style="float:right">'.$value["location"].'</span>';
            $data[$key][] =$button_html;
          }
          
    }else{
        for ($i=0; $i <=7 ; $i++) { 
          if ($i==0) {
            $data[0][$i]="No Data Found";
          }else{
            $data[0][$i]="";
          }
          
        }
       
        
    }

    return $this->json_data($data,$totalRecords);
  
  }
  private function generate_show_approve_reject_data_table($voucher_no){
    $status=@$_GET['status'];
    $filter_status =@$_REQUEST['order'][0];
    $search=@$_REQUEST['search']['value'];
    $department_id=$this->session->userdata('department_id');
    // $result = $this->Sales_recieved_product_model->get_chitti_wise($chitti_no,$filter_status,$status,$_REQUEST,$search,$limit=true,$department_id);
    // $totalRecords =sizeof($this->Sales_recieved_product_model->get_chitti_wise($chitti_no,$filter_status,$status,$_REQUEST,$search,$limit=false,$department_id));
    $result = $this->Sales_recieved_product_model->get_voucher_no_wise($voucher_no,$filter_status,$status,$_REQUEST,$search,$limit=true,$department_id);
    
    //echo $this->db->last_query(); print_r($result); die;
    $totalRecords =$this->Sales_recieved_product_model->get_voucher_no_wise($voucher_no,$filter_status,$status,$_REQUEST,$search,$limit=false,$department_id);

       if (!empty($result)) {
          foreach ($result as $key => $value) {
          $btn_html="";
           if (@$_GET['status'] != "sended_to_manufacture"){
            $btn_html.="<button id='btn_".$value['id']."' onclick='send_to_salesstock(".$value['id'].")' type='button' class='btn btn-md btn-link view_link'>Approve</button>";
            $btn_html.=" <button onclick='send_to_manufacture(".$value['id'].")' type='button' class='btn btn-md btn-link delete_link'>Reject</button>";
              }
          if (empty($_GET['status'])) {
            $data[$key][] ='<div class="checkbox checkbox-purple"><input id="checkbox'.$key.'" type="checkbox" name="drp_id[]" value="'.$value['id'].'"><label for="checkbox'.$key.'"></label></div>';
          }else{
            $sr_no=$key+1;
            //$data[$key][] = '<span style="float:right">'.$sr_no.'</span>';
            $data[$key][] = '<div class="checkbox checkbox-purpal"><input type="checkbox" name="rp[]" id="checkbox_'.$value['id'].'" class="form-control checkbox_ctn" value="'.$value['id'].'"><label for="checkbox_'.$value['id'].'"></label></div>';
          }
             
            if(empty($_GET['status'])) {
              $button_html = '<span class="pull-right"><a href="'.ADMIN_PATH.'Sales_recieved_product/change_location/'.$value['product_code'].'"><button class="btn btn-link edit_link small loader-hide btn-sm" name="commit" type="button" >change location</button></a>&nbsp;<a href="'.ADMIN_PATH.'Sales_recieved_product/view/'.$value['product_code'].'"><button class="btn btn-link view_link small loader-hide btn-sm" name="commit" type="button" >View</button></a>&nbsp;</span>';  

              if($department_id!='2' && $department_id !='3' && $department_id !='6' && $department_id !='10'){
                $data[$key][] ='<a href="'.ADMIN_PATH.'Sales_recieved_product/view/'.$value['product_code'].'">'.$value['product_code'].'</a>';
              }            
            }else {

              $button_html = '<span class="pull-right"><a href="'.ADMIN_PATH.'Sales_recieved_product/view/'.$value['product_code'].'?status=rejected_chitti"><button class="btn btn-link view_link small loader-hide btn-sm" name="commit" type="button" >View</button></a>&nbsp;</span>';
              if($department_id!="2" && $department_id !='3' && $department_id !='6' && $department_id !='10'){
                $data[$key][] ='<a href="'.ADMIN_PATH.'Sales_recieved_product/view/'.$value['product_code'].'?status=rejected_chitti">'.$value['product_code'].'</a>';
              }
            }
         

            $data[$key][] =$value["product"];
            $data[$key][] ='<input type="hidden" name="product_code[]" value="'.$value["product_code"].'" > <span style="float:right">'.round($value["gr_wt"],2).'</span>';
            $data[$key][] ='<span style="float:right">'.round($value["net_wt"],2).'</span>';

            $data[$key][] ='<span style="float:right">'.$value["quantity"].'</span>';
            $data[$key][] ='<span style="float:right">'.$value["location"].'</span>';
            $data[$key][] =$button_html;
          }
          
    }else{
        for ($i=0; $i <=7 ; $i++) { 
          if ($i==0) {
            $data[0][$i]="No Data Found";
          }else{
            $data[0][$i]="";
          }
          
        }
       
        
    }

    return $this->json_data($data,$totalRecords);
  
  }

  private function json_data($data,$totalRecords){
       $json_data = array(
          "draw" => intval($_REQUEST['draw']),
          "recordsTotal" => intval($totalRecords),
          "recordsFiltered" => intval($totalRecords),
          "data" => $data
      );
    return $json_data; 
  }
  public function change_location($product_code=''){
    $data['product_code']= $product_code;
    $department_id=$this->session->userdata('department_id');
    $data['page_title'] = "Change Location";
    $data['display_title'] = "Change Location";
    $data['location'] = $this->Sales_recieved_product_model->get_location_master();
    $data['ready_product_data'] = $this->Sales_recieved_product_model->get_ready_product_data($product_code);
    $this->view->render('sales_module/stock/loction_change_vw',$data);
    
  }
    public function view($product_code=''){
    $department_id=$this->session->userdata('department_id');
    $department_name=$this->Department_model->find($department_id);
    $data['department_id'] = $department_id;  
    $data['display_name'] =$department_name['name']. ' -  Product Details';
    $this->breadcrumbs->push($department_name['name']." - Product Details", "Ready_product");
    $data['page_title'] = 'View Ready Product';
    $data['product_details']=$this->Ready_product_model->get_ready_product_details('','','','',$limit=true,$department_id, $product_code);
    $data['product_code'] = $product_code;
    //print_r($product_code);die;
    $list=$this->input->post('list');
    if($list !="")
    {
      //print_r($department_id);die;
      echo json_encode($this->generate_data_table_details($product_code,$department_id));
    }else{
      $this->view->render('Manufacturing_module/ready_product/product_details',$data);
    }
  }

   private function generate_data_table_details($product_code,$department_id) {
    //print_r($product_code);die;
    $filter_status =@$_REQUEST['order'][0];
    $status = @$_GET['status'];
    $search=@$_REQUEST['search']['value'];
    
    $result = $this->Ready_product_model->get_ready_product_details($filter_status,$status,$_REQUEST, $status, $limit = true,$department_id,$product_code);

   $totalRecords = $this->Ready_product_model->get_ready_product_details($filter_status,$status,$_REQUEST, $status, $limit = false,$department_id,$product_code);
//echo $this->db->last_query(); print_r($result); die;
   //$j=0;
    if (!empty($result)) {
        foreach ($result as $key => $value) {
           //print_r($value);
          if($department_id=='8'){
            $data[$key][0] ='<span style="float:right">'. $value["quantity"].'</span>';
            if(!empty($value["custom_net_wt"])){
                $data[$key][1] ='<span style="float:right">'.round($value["custom_net_wt"],2) .'</span>';
            }else{
               $data[$key][1] ='<span style="float:right">'.round($value["net_wt"],2).'</span>';
            }         
            $data[$key][2] = '<span style="float:right">'.round($value["few_wt"],2).'</span>';
            $data[$key][3] = '<span style="float:right">'.round($value["kundan_pure"],2).'</span>';
            $data[$key][4] = '<span style="float:right">'.round($value["mina_wt"],2).'</span>';
            $data[$key][5] = '<span style="float:right">'.round($value["wax_wt"],2).'</span>';
            $data[$key][6] = '<span style="float:right">'.round($value["color_stone"],2).'</span>';
            $data[$key][7] = '<span style="float:right">'.round($value["moti"],2).'</span>';
            $data[$key][8] = '<span style="float:right">'.round($value["checker_wt"],2).'</span>';
            $data[$key][9] = '<span style="float:right">'.round($value["black_beads_wt"],2).'</span>';
            if(!empty($value["custom_gr_wt"])){
              $data[$key][10] ='<span style="float:right">'.round($value["custom_gr_wt"],2) .'</span>';
            }else{                                   
                $data[$key][10] = '<span style="float:right">'.round($value["gr_wt"] , 2).'</span>';
            } 
            $data[$key][11] = '<span style="float:right">'.round($value["checker_pcs"] , 2).'</span>';    
            $data[$key][12] = '<span style="float:right">'.round($value["checker_rate"] , 2).'</span>';  
            $data[$key][13] = '<span style="float:right">'.round($value["checker_amt"] , 2).'</span>';   
            $data[$key][14] = '<span style="float:right">'.round($value["kundan_pc"] , 2).'</span>';  
            $data[$key][15] = '<span style="float:right">'.round($value["kundan_rate"] , 2).'</span>';   
            $data[$key][16] = '<span style="float:right">'.round($value["kundan_amt"] , 2).'</span>';  
            $data[$key][17] = '<span style="float:right">'.round($value["stone_wt"] , 2).'</span>';
  
            $data[$key][18] = '<span style="float:right">'.round($value["color_stone_rate"] , 2).'</span>';
            $data[$key][19] = '<span style="float:right">'.round($value["stone_amt"] , 2).'</span>';
            $data[$key][20] = '<span style="float:right">'.round($value["other_wt"] , 2).'</span>';
            $data[$key][21] = '<span style="float:right">'.round($value["total_wt"] , 2).'</span>';

          }else if($department_id=='11'){
            $data[$key][0] ='<span style="float:right">'. $value["quantity"].'</span>';
                     if(!empty($value["custom_net_wt"])){
                $data[$key][1] ='<span style="float:right">'.round($value["custom_net_wt"],2) .'</span>';
            }else{
               $data[$key][1] ='<span style="float:right">'.round($value["net_wt"],2).'</span>';
            } 
            $data[$key][2] = '<span style="float:right">'.round($value["gr_wt"] , 2).'</span>';
          }else{
            $data[$key][0] ='<span style="float:right">'. $value["quantity"].'</span>';
                     if(!empty($value["custom_net_wt"])){
                $data[$key][1] ='<span style="float:right">'.round($value["custom_net_wt"],2) .'</span>';
            }else{
               $data[$key][1] ='<span style="float:right">'.round($value["net_wt"],2).'</span>';
            } 
            $data[$key][2] = '<span style="float:right">'.round($value["few_wt"],2).'</span>';
            $data[$key][3] = '<span style="float:right">'.round($value["kundan_pure"],2).'</span>';
            $data[$key][4] = '<span style="float:right">'.round($value["mina_wt"],2).'</span>';
            $data[$key][5] = '<span style="float:right">'.round($value["wax_wt"],2).'</span>';
            $data[$key][6] = '<span style="float:right">'.round($value["color_stone"],2).'</span>';
            $data[$key][7] = '<span style="float:right">'.round($value["moti"],2).'</span>';
            $data[$key][8] = '<span style="float:right">'.round($value["checker_wt"],2).'</span>';
             if(!empty($value["custom_gr_wt"])){
              $data[$key][9] ='<span style="float:right">'.round($value["custom_gr_wt"],2) .'</span>';
            }else{                                   
                $data[$key][9] = '<span style="float:right">'.round($value["gr_wt"] , 2).'</span>';
            } 
            $data[$key][10] = '<span style="float:right">'.round($value["checker_pcs"] , 2).'</span>';    
            $data[$key][11] = '<span style="float:right">'.round($value["checker_rate"] , 2).'</span>';  
            $data[$key][12] = '<span style="float:right">'.round($value["checker_amt"] , 2).'</span>';   
            $data[$key][13] = '<span style="float:right">'.round($value["kundan_pc"] , 2).'</span>';  
            $data[$key][14] = '<span style="float:right">'.round($value["kundan_rate"] , 2).'</span>';   
            $data[$key][15] = '<span style="float:right">'.round($value["kundan_amt"] , 2).'</span>';  
            $data[$key][16] = '<span style="float:right">'.round($value["stone_wt"] , 2).'</span>';
  
            $data[$key][17] = '<span style="float:right">'.round($value["color_stone_rate"] , 2).'</span>';
            $data[$key][18] = '<span style="float:right">'.round($value["stone_amt"] , 2).'</span>';
            $data[$key][19] = '<span style="float:right">'.round($value["black_beads_wt"] , 2).'</span>';
            $data[$key][20] = '<span style="float:right">'.round($value["other_wt"] , 2).'</span>';
            $data[$key][21] = '<span style="float:right">'.round($value["total_wt"] , 2).'</span>';
          }
            
        }
    } else {
       $msg='No data found';
         if($department_id=='8'){$row='21';}else if($department_id=='11'){$row="3";}else{$row="21";}
       for ($i=0; $i <=$row; $i++) { 
           $data[0][$i] = [$msg];
           $msg="";
       }
    
    }

      $json_data = array(
          "draw" => intval($_REQUEST['draw']),
          "recordsTotal" => intval($totalRecords),
          "recordsFiltered" => intval($totalRecords),
          "data" => $data
      );
    return $json_data;
  }
}//class