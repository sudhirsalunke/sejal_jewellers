<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Sales_Dashboard extends CI_Controller {

    public function __construct() {
        parent::__construct();
        if(empty($this->session->userdata('user_id'))){
        redirect(ADMIN_PATH . 'auth/logout');
        }
        $this->breadcrumbs->push("Sales Module", "sales_dashboard");
        $this->breadcrumbs->push("Dashboard", "Sales_Dashboard");
        $this->load->model(array('sales_module/Prepared_order_model','sales_module/Sales_recieved_product_model','sales_module/Sales_stock_model','sales_module/Sales_invoice_model'));
    }

    public function index(){
        $department_id=$this->session->userdata('department_id');
        $location_id=$this->session->userdata('location_id');
        $data['page_title'] = "Sales Dashboard";
        $data['order_prepare'] = $this->Prepared_order_model->get();
        $_GET['status']="sent";
        $data['order_sent'] = $this->Prepared_order_model->get();
        $data['bom_recieved_products'] = $this->Sales_recieved_product_model->get('','','','',false,$department_id,'1');
        $data['delhi_recieved_products'] = $this->Sales_recieved_product_model->get('','','','',false,$department_id,'2');
        $data['bom_stock'] = $this->Sales_stock_model->get('','','',false,$department_id,'1');
        $data['delhi_stock'] = $this->Sales_stock_model->get('','','',false,$department_id,'2');
       // print_r($data['stock'] );die;
        $_GET['status']="sended_to_manufacture";
        $data['delhi_manufacture_send'] = $this->Sales_recieved_product_model->get('','','','',false,$department_id,'1');
        $data['bom_manufacture_send'] = $this->Sales_recieved_product_model->get('','','','',false,$department_id,'2');
        $data['invoices'] = sizeof($this->Sales_invoice_model->get());
        // print_r($data['invoices']);die;
        $_GET['status']='sent';
        $data['dispatch_invoices'] = sizeof($this->Sales_invoice_model->get());
        // $data['received_orders'] = sizeof($this->Received_orders_model->get());
        // $data['received_receipt'] = sizeof($this->Received_orders_model->get_receipt_order());
        
        // $data['engaged_karigar']= $this->Karigar_order_model->get_engaged_karigar();
        // $_GET['status'] = 'complete';
        // $data['approved_qc'] = sizeof($this->Quality_control_model->get());
        // $_GET['status'] = 'rejected';
        // $data['rejected_qc'] = sizeof($this->Quality_control_model->get());
        // $hallmarking=TRUE;
        // $_GET['status'] = 'send';
        // $data['sent_to_hallmarking'] = sizeof($this->Quality_control_model->get('','','','','',$hallmarking));
        // $_GET['status'] = 'receive';
        // $data['approved_hallmarking'] = sizeof($this->Hallmarking_model->get('','','','',FALSE));
        // $_GET['status'] = 'rejected';
        // $data['rejected_hallmarking'] = sizeof($this->Hallmarking_model->get('','','','',FALSE));
        // $data['make_set'] = sizeof($this->Make_set_model->get('','','','',FALSE));
        //echo '<pre>'; print_r($data); echo '</pre>'; exit();
        $this->view->render('sales_module/dashboard/index',$data);
    }
}
