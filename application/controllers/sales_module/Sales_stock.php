<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class Sales_stock extends CI_Controller {

  public function __construct() {
    parent::__construct();
    if(empty($this->session->userdata('user_id'))){
      redirect(ADMIN_PATH . 'auth/logout');
    }
    $this->breadcrumbs->push("Sales", "sales_dashboard");
    $this->breadcrumbs->push("Stock", "Sales_stock");
    $this->load->model(array('sales_module/Sales_stock_model',
                            'sale_master/Customer_model',
                            'sale_master/Kundan_category_model',
                            'sale_master/Type_master_model',
                            'sale_master/Color_stone_model',
                            'User_access_model',
                            'Weight_range_model',
                            'Karigar_model',
                            'Party_master_model',
                            'sale_master/Parent_category_model',
                            'Category_model',
                            'Manufacturing_module/Department_model',
                            'Manufacturing_module/Ready_product_model'));
    $this->load->config('admin_validationrules', TRUE);
    $this->load->library('Data_encryption');
  }

  public function index(){

    $display_title ="All Stock Products";    
    $department_id=$this->session->userdata('department_id');
    $location_id=$this->session->userdata('location_id');
    $dep_name=$this->Department_model->find($department_id);   
    $department=@$_GET['department'];
    //print_r($department);die;
    $data['department_id']= $department_id;
    if (@$_GET['status']=="manufacture") {
      $display_title ="Products Approved By Sales" ;
    }

    $data['page_title'] = "All Stock Products";
    $data['display_title'] = $display_title;

    $list=$this->input->post('list');
    if($list !="")
    {
      echo json_encode($this->generate_data_table($department_id,$location_id,$department));
    }else{
      $this->view->render('sales_module/stock/index',$data);
    }
  }

  // public function create(){
  //   $this->breadcrumbs->push("Prepared Order", "prepared_order/create");
  //   $data['page_title'] = "Prepared Order";
  //   $data['sub_category']=$this->Sub_category_model->get();
  //   $data['weight_range']=$this->Weight_range_model->get();
  //   $data['department'] =$this->Department_model->get();
  //   $this->view->render('sales_module/prepared_order/create',$data);
  // }

  // private function validation_result(){
  //   $data = array();
  //   $validationResult = $this->Sales_recieved_product_model->validatepostdata();
  //   if($validationResult===FALSE){
  //     $data['status']= 'failure';
  //     $data['data']= '';
  //     $data['error'] = array(
  //       'department_id'=>strip_tags(form_error('product[department_id]')),
  //       'weight'=>strip_tags(form_error('product[weight][]')),
  //       'quantity'=>strip_tags(form_error('product[quantity][]')),
  //       'sub_category'=>strip_tags(form_error('product[sub_category][]')),
        
  //     );


  //   }
  //   return $data;
  // }

  public function create() {
    $user_id=$this->session->userdata('user_id');    
    $dep_id=$this->User_access_model->find($user_id); 
    $department_id=$this->session->userdata('department_id');    

    $data['category'] = $this->Category_model->get('','','','',true,$department_id);
    // $data['parent_category'] = $this->Parent_category_model->get('','','','',true);
    $data['parent_category'] = $this->Sales_stock_model->get_product_category();
    $data['karigar']= $this->Karigar_model->get('','','','',true);
    if($department_id !='0'){
        $dep_name[]=$this->Department_model->find($department_id);   
        $data['department'] = $dep_name;
    }else{            
          $data['department'] = $this->Department_model->get('','','','',true);          
    }
    $data['weight_range'] = $this->Weight_range_model->get('','','','',true);
    
   // $data['category']=$this->Sales_stock_model->get_product_name($department_id);
    //print_r($data['category']);die;
/*  $data['max_id'] = $this->Customer_order_model->get_max_id();*/
    $data['display_title'] = "Add New Stock";
    $data['department_id'] =   $department_id;
    $data['page_title']="Create New Stock";
    $this->view->render('sales_module/stock/create', $data);
  }

    public function edit($product_code=''){
    $department_id=$this->session->userdata('department_id');
    $department_name=$this->Department_model->find($department_id);
    $data['department_id'] = $department_id;  
    $data['display_name'] =$department_name['name']. ' - Ready Product Details';
    $this->breadcrumbs->push($department_name['name']." - Ready Product Details", "Ready_product");

    $data['product_details']=$this->Ready_product_model->get_ready_product_details('','','','',$limit=true,$department_id, $product_code);
    $data['product_code'] = $product_code;
    //print_r($data['product_details']);die;
    if($department_id !='0'){
        $dep_name[]=$this->Department_model->find($department_id);   
        $data['department'] = $dep_name;
    }else{            
          $data['department'] = $this->Department_model->get('','','','',true);          
    }
    $data['category'] = $this->Category_model->get('','','','',true,$department_id);
    // $data['parent_category'] = $this->Parent_category_model->get('','','','',true);
    $data['parent_category'] = $this->Sales_stock_model->get_product_category();
    $data['ac_product_category'] = $this->Sales_stock_model->get_ac_product_category();
   // print_r( $data['ac_product_category'] );die;
    $data['karigar']= $this->Karigar_model->get('','','','',true);
  
    $data['display_title'] = "Edit New Stock";
    $data['department_id'] =   $department_id;
    $data['page_title']="Create New Stock";
    $this->view->render('sales_module/stock/edit', $data);
  }   

  public function store(){

    $data= $this->Sales_stock_model->store();
    
    echo json_encode($data);
  }

  public function store_all(){

    $data= $this->Sales_stock_model->store_all();
    
    echo json_encode($data);
  }

  public function get_category_name(){    
    $department_id=$_POST['department_id'];
    $category= $this->Category_model->get_category_name_dep($department_id);  
    
    //print_r($data);die;  
    echo json_encode($category);
  }
   

  public function invoice(){
    $this->breadcrumbs->push("Generate Invoice", "Sales_stock/invoice");
    $data['page_title'] = "Invoice";
    $data['display_title'] = "Generate Invoice";
    $data['all_customers']=$this->Customer_model->get();
    $data['all_types']=$this->Type_master_model->get();
    $data['all_kundan_category']=$this->Kundan_category_model->get();
    $data['all_cs']=$this->Color_stone_model->get();

    $this->view->render('sales_module/stock/invoice',$data);
  }

  public function get_item_details(){
   $data = $this->Sales_stock_model->get_item_details();
   if (empty($data)) {
     $data['result']="error";
   }
   echo json_encode($data);
  }

  public function get_stock_product(){
   $data=$this->Sales_stock_model->get_stock_product();
   echo json_encode($data);
  }

  private function generate_data_table($department_id,$location_id,$department){
    //print_r($department);die;
  //print_r($location_id);die;
    $filter_status =@$_REQUEST['order'][0];
    $search=@$_REQUEST['search']['value'];
    $result = $this->Sales_stock_model->get($filter_status,$_REQUEST,$search,$limit=true,$department_id,$department);
    $totalRecords = $this->Sales_stock_model->get($filter_status,$_REQUEST,$search,$limit=false,$department_id,$department);
    // echo $this->db->last_query();
    // print_r($result);die;
    if (!empty($result)) {
        foreach ($result as $key => $value) {
          $btn_html="";
        if (!empty($value['product_code'])) {        
         $button_html = '<span class="pull-right"><a href="'.ADMIN_PATH.'Sales_recieved_product/view/'.$value['product_code'].'"><button class="btn btn-link view_link small loader-hide btn-sm" name="commit" type="button" >View</button></a>&nbsp;</span>';
         }
      
          $sr_no=$_REQUEST['start'] + $key+1;
          //$data[$key][] ='<span style="float:right">'.$sr_no.'</span>';
        

         $data[$key][] = '<input type="hidden" name="type" id="type" value="stock"><div class="checkbox checkbox-purple"><input id="checkbox_'.$sr_no.'" type="checkbox" name="product_code[]" value="'.$value["product_code"].'"><label for="checkbox'.$sr_no.'"></label></div>';
            
          $data[$key][] = '<span style="float:right">'.$value["voucher_no"].'</span>';
    /*      $data[$key][] = $value["karigar_name"];
          $data[$key][] = '<span style="float:right">'.$value["karigar_code"].'</span>';*/
          if($department_id!='2' && $department_id !='3' && $department_id !='6' && $department_id !='10'){
          $data[$key][] ='<a href="'.ADMIN_PATH.'Sales_recieved_product/view/'.$value['product_code'].'">'.$value['product_code'].'</a>';
        }
        /*  $data[$key][] = $value["sub_category"];*/
          $data[$key][] = $value["product"];
          if($department_id!='2'&& $department_id !='3' && $department_id !='6' && $department_id !='10'){
          $data[$key][] = '<span style="float:right">'.$value["quantity"].'</span>';
          }
          $data[$key][] = '<span style="float:right">'.round($value["gr_wt"],2).'</span>';
          $data[$key][] = '<span style="float:right">'.round($value["net_wt"],2).'</span>';
          $data[$key][] = '<input type="hidden" name="location_id[]" id="location_id" value="'.$value["location_id"].'">'.$value["location_name"];
          $data[$key][] = '<span style="float:right">'.$value["approve_date"].'</span>';
          if($department_id!='2' && $department_id !='3' && $department_id !='6' && $department_id !='10'){
          $data[$key][] =$button_html;
         }

         
        }
    }else{
      $txt='No data found';
      for ($i=0; $i <=9; $i++) { 
         $data[0][$i] = [$txt];
         $txt="";
      }
    }

     $json_data = array(
          "draw" => intval($_REQUEST['draw']),
          "recordsTotal" => intval($totalRecords),
          "recordsFiltered" => intval($totalRecords),
          "data" => $data
      );

    return $json_data; 
  }

  public function sales_to_manufacture(){
    $data= $this->Sales_stock_model->sales_to_manufacture();
    echo json_encode($data);
  }

  public function sales_to_manufacture_all(){
    //print_r($_POST);die;
    $data= $this->Sales_stock_model->sales_to_manufacture_all();
    if(@$_POST['type']=="stock"){
      $data['redirect_url']="sales_stock";
    }else if(@$_POST['type']=="ac_sales_return_voucher"){
      $data['redirect_url']="sales_return_product";
    }else{
      $data['redirect_url']="Sales_recieved_product";
    }
    echo json_encode($data);
  }

  public function stock_to_manufacture_all(){
    //print_r($_POST);die;
    $data= $this->Sales_stock_model->stock_to_manufacture_all();
    if(@$_POST['type']=="stock"){
      $data['redirect_url']="sales_stock";
    }else if(@$_POST['type']=="ac_sales_return_voucher"){
      $data['redirect_url']="sales_return_product";
    }else{
      $data['redirect_url']="Sales_recieved_product";
    }
    echo json_encode($data);
  }

    public function stock_to_transfer_all(){

      $location_id=true;
      // foreach ($_POST['product_code'] as $key => $value) { 
      //   if($_POST['location_id'][$key] =='2'){
      //       $location_id=false;        
      //   }
      // }
      if( $location_id==false){
        $data['status']="failure";
        $data['error']['location']="select only mumbai stock";
      
      }else{
          $data= $this->Sales_stock_model->stock_to_transfer_all();
        if(@$_POST['type']=="stock"){
          $data['redirect_url']="sales_stock";
        }else if(@$_POST['type']=="ac_sales_return_voucher"){
          $data['redirect_url']="sales_return_product";
        }else{
          $data['redirect_url']="Sales_recieved_product";
        }
      }
        echo json_encode($data);
      //die;
   // echo $location_id; die;
  }

  public function stock(){
    $data=$this->validation_result();
    if (count($data)==0) {
    
      $data = $this->Sales_stock_model->add_stock();
    }
    echo json_encode($data);
  }   

  private function validation_result(){
    $data = array();
    $validationResult = $this->Sales_stock_model->validatepostdata();
    if($validationResult===FALSE){
        $data['status']= 'failure';
        $data['data']= '';
        $data['error'] = array(
          'department_id'=>strip_tags(form_error('sales_stock[department_id]')),
          //'weight_range_id'=>strip_tags(form_error('sales_stock[weight_range_id]')),
          'category_id'=>strip_tags(form_error('sales_stock[category_id]')),
          'parent_category_id'=>strip_tags(form_error('sales_stock[parent_category_id]')),
          'product_code'=>strip_tags(form_error('sales_stock[product_code]')),
          //'weight'=>strip_tags(form_error('sales_stock[weight]')),
          'net_wt'=>strip_tags(form_error('sales_stock[net_wt]')),
          'quantity'=>strip_tags(form_error('sales_stock[quantity]')),
          'few_wt'=>strip_tags(form_error('sales_stock[few_wt]')),
          'kundan'=>strip_tags(form_error('sales_stock[kundan]')),
          //'meena_wt'=>strip_tags(form_error('sales_stock[meena_wt]')),
          'wax_wt'=>strip_tags(form_error('sales_stock[wax_wt]')),
          'kundan_pcs'=>strip_tags(form_error('sales_stock[kundan_pcs]')),
          //'moti_wt'=>strip_tags(form_error('sales_stock[moti_wt]')),
          'gr_wt'=>strip_tags(form_error('sales_stock[gr_wt]')),
          'color_stone' => strip_tags(form_error('sales_stock[color_stone]')),
                  
        );
    }
    //print_r(count($data));die;
    return $data;
  }

  

}//class