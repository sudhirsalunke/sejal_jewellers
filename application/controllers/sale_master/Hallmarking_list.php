<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Hallmarking_list extends CI_Controller {

  public function __construct() {
    parent::__construct();
    if(empty($this->session->userdata('user_id'))){
      redirect(ADMIN_PATH . 'auth/logout');
    }
    $this->breadcrumbs->push("Master", "#");
    $this->breadcrumbs->push("Hallmarking List", "hallmarking_list");
    $this->load->model(array('sale_master/hallmarking_list_model',
                      'hallmarking_center_model',
                       'common_model/state_model',
                       'common_model/city_model',));
    $this->load->config('admin_validationrules', TRUE);
    $this->load->library(array('excel_lib','Data_encryption'));
    
  }

  public function index(){
    $data['page_title'] = "All Hallmarking Center";
    $list=$this->input->post('list');
    if($list !="")
    {
      echo json_encode($this->generate_data_table());
    }else{
      $this->view->render('sale_master/hallmarking_list/index',$data);
    }
  }

  public function create(){
    $this->breadcrumbs->push("All Hallmarking Center", "hallmarking_list/create");
  	$data['page_title'] = "All Hallmarking Center";
    $data['hallmarking_center']=$this->hallmarking_center_model->get();
    $data['city']=array();
    $data['state']=$this->state_model->get();
    $this->view->render('sale_master/hallmarking_list/create',$data);
  }
    public function get_city(){
      $result=get_successMsg();
      $result['data']=$this->city_model->get();
    echo json_encode($result);
  }
  private function validation_result(){
    $data = array();
    $validationResult = $this->hallmarking_list_model->validatepostdata();
    if($validationResult===FALSE){
      $data['status']= 'failure';
      $data['data']= '';
      $data['error'] = array(
        'c_name'=>strip_tags(form_error('hallmarking_list[c_name]')),
        'owner'=>strip_tags(form_error('hallmarking_list[owner]')),
        'ap_name'=>strip_tags(form_error('hallmarking_list[ap_name]')),
        'hc_id'=>strip_tags(form_error('hallmarking_list[hc_id]')),
        'code'=>strip_tags(form_error('hallmarking_list[code]')),
        'address'=>strip_tags(form_error('hallmarking_list[address]')),
        'city'=>strip_tags(form_error('hallmarking_list[city]')),
        'state'=>strip_tags(form_error('hallmarking_list[state]')),
        'pincode'=>strip_tags(form_error('hallmarking_list[pincode]')),
        'email'=>strip_tags(form_error('hallmarking_list[email]')),
        'mobile_no'=>strip_tags(form_error('hallmarking_list[mobile_no]')),
        'phone_no'=>strip_tags(form_error('hallmarking_list[phone_no]')),
        'icom_no'=>strip_tags(form_error('hallmarking_list[icom_no]')),
        'limit'=>strip_tags(form_error('hallmarking_list[limit]')),
        
      );
    }
    return $data;
  }

  public function store(){
    $data = array();
    $validationResult = $this->validation_result();
   // print_r($validationResult);
    if(count($validationResult) == 0){
      $data['status']= 'success';
      $data['data']= '';
      $data['user_id'] = $this->hallmarking_list_model->store();
      $validationResult=$data;
    }
    echo json_encode($validationResult);
  }
  public function edit($id){
    $this->breadcrumbs->push("Edit hallmarking List", "hallmarking_list/edit");
  	$data['page_title'] = "EDIT hallmarking List";
    $data['parent_category']= $this->hallmarking_list_model->get();
    $data['hallmarking_center']=$this->hallmarking_center_model->get();
    $data['hl'] = $this->hallmarking_list_model->find_by_encrypted_id($id);
    $data['city']=array();
    $data['state']=$this->state_model->get();
    //print_r($data['hallmarking_center']);exit;
    $this->view->render('sale_master/hallmarking_list/edit',$data);
  }
  public function update(){

    $data = array();
    $validationResult = $this->validation_result();
   // print_r($validationResult);
    if(count($validationResult) == 0){
      $data['status']= 'success';
      $data['data']= '';
      $data['user_id'] = $this->hallmarking_list_model->update();
      $validationResult=$data;
    }
    echo json_encode($validationResult);
   
  }
  public function delete(){
    $result = $this->hallmarking_list_model->delete($_POST['id']);
    echo json_encode($result);
  }
  private function generate_data_table(){
    $status='';
    $filter_status =@$_REQUEST['order'][0];
    $search=@$_REQUEST['search']['value'];
    $result = $this->hallmarking_list_model->get($filter_status,$status,$_REQUEST,$search,$limit=true);
    $totalRecords = sizeof($this->hallmarking_list_model->get($filter_status,$status,$_REQUEST,$search,$limit=false));
    if (!empty($result)) {
        foreach ($result as $key => $value) {
            $data[$key][0] =$value["hc_name"];
            $data[$key][1] =$value["code"];            
            $data[$key][2] =blank_value($value["c_name"]);
            $data[$key][3] =blank_value($value["owner"]);
            $data[$key][4] =blank_value($value["ap_name"]);           
            $data[$key][5] =blank_value('<span style="float:right">'. $value["mobile_no"].'</span>');
            $data[$key][6] =blank_value('<span style="float:right">'. $value["phone_no"].'</span>');
            $data[$key][7] =blank_value($value["address"]);
            $pc_used =$this->hallmarking_center_model->check_hc_used($value['hc_id']);
            $button_html = '';
            if($pc_used != 0){
                $onclick ="disabled";
            }else{
                $onclick ='onclick=Delete_record("'.$value['encrypted_id'].'",this,"hallmarking_list")';
            }
            
            $button_html  = '<span class="pull-right"><a href="'.ADMIN_PATH.'hallmarking_list/print_hallmarking_center/'.$value["encrypted_id"].'" target="_blank" class="btn btn-link view_link small loader-hide btn-sm" id="confim_'.$value['encrypted_id'].'">PRINT</a>&nbsp&nbsp<a href="'.ADMIN_PATH.'hallmarking_list/edit/'.$value["encrypted_id"].'"  class="btn btn-link edit_link small loader-hide  btn-sm" id="confim_'.$value['encrypted_id'].'">EDIT</a>&nbsp&nbsp<a '.$onclick.' class="btn btn-link delete_link small loader-hide btn-sm" id="reject_'.$value['encrypted_id'].'">DELETE</a></span>';
            
            $data[$key][8]   = $button_html; 
        }
    }else{
        $data[0][0] = ['No data found'];
        $data[0][1] = [];
        $data[0][2] = [];
        $data[0][3] = [];
        $data[0][4] = [];
        $data[0][5] = [];
        $data[0][6] = [];
        $data[0][7] = [];
        $data[0][8] = [];
        
        
    }
     $json_data = array(
          "draw" => intval($_REQUEST['draw']),
          "recordsTotal" => intval($totalRecords),
          "recordsFiltered" => intval($totalRecords),
          "data" => $data
      );
    return $json_data; 
  }

   public function get(){
    $data = array();
    $result = $this->hallmarking_center_model->get('','','',$_GET['term'],$limit=true);
    if(!empty($result)){
      foreach ($result as $key => $value) {
        $data[$key]['label'] = $value['name'];
        $data[$key]['value'] = $value['name'];
        $data[$key]['hc_id'] = $value['id'];
        $data[$key]['id'] = $value['id'];
        $data[$key]['code'] = $value['code'];
      }
    }
    echo json_encode($data);
  }

  public function export($search='')
  {
    
    $data = $this->hallmarking_list_model->export_hallmarking_list('','','',$search,'');
    $header_array=array("HM Center","HM Code","Company Name","Owner","Authorized Person","Mobile No","Phone No","Address");  
    $file_name="hallmarking_list.xlsx";
    $excel_data = create_excel_array($data,$header_array);
    $this->excel_lib->export($excel_data,$header_array,$file_name,'','',$save=true);
  }

  public function  print_hallmarking_center($hm_id){
    $data['page_title'] = "Hallmarking Center";
    $data['Hallmarking_details'] = $this->hallmarking_list_model->getPrintData($hm_id);
    //echo $this->db->last_query(); print_r($result); die;
    $this->load->view('sale_master/hallmarking_list/print_hallmarking_center',$data);

  }
}