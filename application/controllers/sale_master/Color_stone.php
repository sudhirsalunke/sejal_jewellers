<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Color_stone extends CI_Controller {

  public function __construct() {
    parent::__construct();
    if(empty($this->session->userdata('user_id'))){
      redirect(ADMIN_PATH . 'auth/logout');
    }
    $this->breadcrumbs->push("Master", "#");
    $this->breadcrumbs->push("Color Stone", "color_stone");
    $this->load->model(array('sale_master/color_stone_model'));
    $this->load->config('admin_validationrules', TRUE);
    $this->load->library('Data_encryption');
  }

  public function index(){
    $data['page_title'] = "All Color Stone";
    $list=$this->input->post('list');
    if($list !="")
    {
      echo json_encode($this->generate_data_table());
    }else{
      $this->view->render('sale_master/color_stone/index',$data);
    }
  }

  public function create(){
    $this->breadcrumbs->push("Add Color Stone", "color_stone/create");
    $data['page_title'] = "Add Color Stone";
    $this->view->render('sale_master/color_stone/create',$data);
  }

  private function validation_result(){
    $data = array();
    $validationResult = $this->color_stone_model->validatepostdata();
    if($validationResult===FALSE){
      $data['status']= 'failure';
      $data['data']= '';
      $data['error'] = array(
        'name'=>strip_tags(form_error('color_stone[name]')),
        
      );
    }
    return $data;
  }

  public function store(){
    $data = array();
    $validationResult = $this->validation_result();
   // print_r($validationResult);
    if(count($validationResult) == 0){
      $data['status']= 'success';
      $data['data']= '';
      $data['user_id'] = $this->color_stone_model->store();
      $validationResult=$data;
    }
    echo json_encode($validationResult);
  }
  public function edit($id){
    $this->breadcrumbs->push("Edit Color Stone", "color_stone/edit");
    $data['page_title'] = "EDIT Color Stone";
    $data['color_stone'] = $this->color_stone_model->find_by_encrypted_id($id);
    //print_r($data);exit;
    $this->view->render('sale_master/color_stone/edit',$data);
  }
  public function update(){

    $data = array();
    $validationResult = $this->validation_result();
   // print_r($validationResult);
    if(count($validationResult) == 0){
      $data['status']= 'success';
      $data['data']= '';
      $data['user_id'] = $this->color_stone_model->update();
      $validationResult=$data;
    }
    echo json_encode($validationResult);
   
  }
  public function delete(){
    $result = $this->color_stone_model->delete($_POST['id']);
    echo json_encode($result);
  }
  private function generate_data_table(){
    $status='';
    $filter_status =@$_REQUEST['order'][0];
    $search=@$_REQUEST['search']['value'];
    $result = $this->color_stone_model->get($filter_status,$status,$_REQUEST,$search,$limit=true);
    $totalRecords = $this->color_stone_model->get($filter_status,$status,$_REQUEST,$search,$limit=false);
    if (!empty($result)) {
        foreach ($result as $key => $value) {
            $data[$key][0] =$value["name"];
            $pc_used =$this->color_stone_model->check_cs_exits($value['id']);
            $button_html = '';
            if($pc_used != 0){
                $onclick ="disabled";
            }else{
                $onclick ='onclick=Delete_record("'.$value['encrypted_id'].'",this,"color_stone")';
            }
            
            $button_html  = '<span style="float:right"> <a href="'.ADMIN_PATH.'color_stone/edit/'.$value["encrypted_id"].'"  class="btn btn-link edit_link small loader-hide  btn-sm" id="confim_'.$value['encrypted_id'].'">EDIT</a>&nbsp  &nbsp <a '.$onclick.' class="btn btn-link delete_link small loader-hide  btn-sm" id="reject_'.$value['encrypted_id'].'">DELETE</a></span>  &nbsp';
            
            $data[$key][1]   = $button_html; 
        }
    }else{
        $data[0][0] = ['No data found'];
        $data[0][1] = [];
        
    }
     $json_data = array(
          "draw" => intval($_REQUEST['draw']),
          "recordsTotal" => intval($totalRecords),
          "recordsFiltered" => intval($totalRecords),
          "data" => $data
      );
    return $json_data; 
  }


}