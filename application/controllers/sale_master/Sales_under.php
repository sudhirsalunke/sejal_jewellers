<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Sales_under extends CI_Controller {

  public function __construct() {
    parent::__construct();
    // if(empty($this->session->userdata('user_id'))){
    //   redirect(ADMIN_PATH . 'auth/logout');
    // }
    $this->breadcrumbs->push("Master", "#");
    $this->breadcrumbs->push("Sales Under", "Sales_under");
    $this->load->model('sale_master/Sales_under_model');
    $this->load->config('admin_validationrules', TRUE);
    $this->load->library('Data_encryption');
  }

  public function index(){
    
    $data['page_title'] = "All Sales Under";
    $list=$this->input->post('list');
    if($list !="")
    {
      echo json_encode($this->generate_data_table());
    }else{
      $this->view->render('sale_master/sale_under/index',$data);
    }
  }

  public function create(){
    $this->breadcrumbs->push("Add Sales Under", "Sales_under/create");
  	$data['page_title'] = "Add Sales Under";
    $this->view->render('sale_master/sale_under/create',$data);
  }

  public function store(){
    $data = array();
    $data = $this->Sales_under_model->validatepostdata();
    if($data['status'] == 'success'){
      $postdata = $_POST['Sales_under'];
      $data = $this->Sales_under_model->store($postdata);
    }
    echo json_encode($data);
  }
  public function edit($encrypted_id){
    $this->breadcrumbs->push("Edit Sales Under", "Sales_under/edit");
  	$data['page_title'] = "Edit Sales Under";
    $id = $this->Sales_under_model->get_primary_key($encrypted_id);
    $data['Sales_under'] = $this->Sales_under_model->find($id);
    $this->view->render('sale_master/sale_under/edit',$data);
  }
  public function update(){
  	$data = array();
    $data = $this->Sales_under_model->validatepostdata();
    if($data['status'] == 'success'){
      $postdata = $_POST['Sales_under'];
      $data = $this->Sales_under_model->update($postdata);
    }
    echo json_encode($data);
  }
  public function delete(){
    $result = $this->Sales_under_model->delete($_POST['id']);
    echo json_encode($result);
  }
  private function generate_data_table(){
    $filter_status =@$_REQUEST['order'][0];
    $status = array('name');
    $search=@$_REQUEST['search']['value'];
    $result = $this->Sales_under_model->get($filter_status,$status,$_REQUEST,$search,$limit=true);
    $totalRecords = sizeof($this->Sales_under_model->get($filter_status,$status,$_REQUEST,$search,$limit=false));
    if (!empty($result)) {
        foreach ($result as $key => $value) {
           $pcs_used = $this->Sales_under_model->check_sales_used($value['id']);
            $data[$key][0] =$value["name"];
            $button_html = '';
            if($pcs_used>0){
               $button_html  = '<span style="float:right"> <a href="'.ADMIN_PATH.'Sales_under/edit/'.$value["encrypted_id"].'"  class="btn btn-link edit_link small loader-hide  btn-sm" id="confim_'.$value['encrypted_id'].'">EDIT</a>&nbsp  &nbsp <a disabled class="btn btn-link delete_link small loader-hide  btn-sm" id="reject_'.$value['encrypted_id'].'">DELETE</a></span>  &nbsp';
            }
            else{
               $button_html  = '<span style="float:right"> <a href="'.ADMIN_PATH.'Sales_under/edit/'.$value["encrypted_id"].'"  class="btn btn-link edit_link small loader-hide  btn-sm" id="confim_'.$value['encrypted_id'].'">EDIT</a>&nbsp  &nbsp <a onclick=Delete_record("'.$value['encrypted_id'].'",this,"Sales_under") class="btn btn-link delete_link small loader-hide  btn-sm" id="reject_'.$value['encrypted_id'].'">DELETE</a></span>  &nbsp';
            }
            $data[$key][1]   = $button_html;
        }
    }else{
        $data[0][0] = ['No data found'];
        $data[0][1] = [];
    }
     $json_data = array(
          "draw" => intval($_REQUEST['draw']),
          "recordsTotal" => intval($totalRecords),
          "recordsFiltered" => intval($totalRecords),
          "data" => $data
      );
    return $json_data; 
  }
}