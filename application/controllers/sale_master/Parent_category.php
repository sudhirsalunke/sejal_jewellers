<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Parent_category extends CI_Controller {

  public function __construct() {
    parent::__construct();
    if(empty($this->session->userdata('user_id'))){
      redirect(ADMIN_PATH . 'auth/logout');
    }
    $this->breadcrumbs->push("Master", "#");
    $this->breadcrumbs->push("Parent Category", "parent_category");
    $this->load->model(array('sale_master/parent_category_model','sale_master/product_category_model','Weight_range_model','Category_model','product_category_rate_model'));
    $this->load->config('admin_validationrules', TRUE);
    $this->load->library(array('Data_encryption','Dropbox'));
    $this->load->config('Dropbox', TRUE);
  }

  public function index(){

    $data['page_title'] = "All Parent Category";
    $list=$this->input->post('list');
    if($list !="")
    {
      echo json_encode($this->generate_data_table());
    }else{
      $this->view->render('sale_master/parent_category/index',$data);
    }
  }

  public function create(){
    $this->breadcrumbs->push("Add Parent Category", "Hallmarking_center/create");
  	$data['page_title'] = "ADD Parent Category";
    $data['categories'] = $this->product_category_model->get('','','','',true);
    $data['product_category_rate'] = $this->product_category_rate_model->get('','','','',true);
    // print_r( $data['product_category_rate']);die;
    $this->view->render('sale_master/parent_category/create',$data);
  }

  private function validationResult(){
    $data = array();
    $validationResult = $this->parent_category_model->validatepostdata();
    if(count($validationResult) != 0){
      $data['status']= 'failure';
      $data['data']= '';
      $data['error'] = $validationResult;
    }
    return $data;
  }

  public function store(){
    $data = $this->validationResult();
    if(count($data)==0){
      $data['status']= 'success';
      $data['data']= '';
      $data['user_id'] = $this->parent_category_model->store();
    }
    echo json_encode($data);
  }
  public function edit($id){
    $this->breadcrumbs->push("Edit Parent Category", "Hallmarking_center/edit");
  	$data['page_title'] = "EDIT Parent Category";
    $data['parent_category'] = $this->parent_category_model->find_by_encrypted_id($id);
    $data['categories'] = $this->product_category_model->get();
    $data['images'] = $this->parent_category_model->get_images($data['parent_category']['id']);
    $data['parent_category_codes'] = $this->parent_category_model->get_category_codes($data['parent_category']['id']);
    $data['parent_category_variants'] = $this->parent_category_model->get_variants($data['parent_category']['id']);
    $data['product_category_rate'] = $this->product_category_rate_model->get('','','','',true);
    $this->view->render('sale_master/parent_category/edit',$data);
  }
  public function get_images(){
    $data['images'] = $this->parent_category_model->get_images($_POST['id']);
    if($_POST['id'])
      $data['variants'] = $this->parent_category_model->get_variants($_POST['id']);
    echo json_encode($data);
  }
  public function update(){
  	$data = $this->validationResult();
    if(count($data)==0){
      $data['status']= 'success';
      $data['data']= '';
      $data['user_id'] = $this->parent_category_model->update();
    }
    echo json_encode($data);
  }
  public function delete(){
    $result = $this->parent_category_model->delete($_POST['id']);
    echo json_encode($result);
  }
  private function generate_data_table(){
    $filter_status =@$_REQUEST['order'][0];
    $status = array('name','description');
    $search=@$_REQUEST['search']['value'];
    $result = $this->parent_category_model->get($filter_status,$status,$_REQUEST,$search,$limit=true);
    $totalRecords = $this->parent_category_model->get($filter_status,$status,$_REQUEST,$search,$limit=false);
    if (!empty($result)) {
        foreach ($result as $key => $value) {
            $data[$key][] =$value["name"];
            $data[$key][] =(!empty($value["code"])) ? implode(',',json_decode($value["code"])) : '';
            $data[$key][] =$value["category_name"];
            //$data[$key][1] =(empty($value["code"]))? '-' :$value["code"];
            $pc_used =$this->parent_category_model->check_pc_used($value['id']);
            $button_html = '';
            if($pc_used != 0){
                $onclick ="disabled";
            }else{
                $onclick ='onclick=Delete_record("'.$value['encrypted_id'].'",this,"parent_category")';
            }
            // <span style="float:right"><a href="'.ADMIN_PATH.'parent_category/create_minimum_stock/'.$value["encrypted_id"].'"  class="btn btn-success btn-sm" id="confim_'.$value['encrypted_id'].'">ADD MINIMUM STOCK</a>&nbsp&nbsp
            $button_html  = '<a href="'.ADMIN_PATH.'parent_category/edit/'.$value["encrypted_id"].'"  class="btn btn-link edit_link btn-sm" id="confim_'.$value['encrypted_id'].'">EDIT</a>&nbsp&nbsp<a '.$onclick.' class="btn btn-link delete_link btn-sm" id="reject_'.$value['encrypted_id'].'">DELETE</a></span>';
            
            $data[$key][]   = $button_html; 
        }
    }else{
        $data[0][0] = ['No data found'];
        $data[0][1] = [];
        $data[0][2] = [];
        $data[0][3] = [];
    }
     $json_data = array(
          "draw" => intval($_REQUEST['draw']),
          "recordsTotal" => intval($totalRecords),
          "recordsFiltered" => intval($totalRecords),
          "data" => $data
      );
    return $json_data; 
  }
  public function fetch_from_dropbox(){
    $this->parent_category_model->fetch_from_dropbox();
    echo json_encode(get_successMsg());
  }
   function create_minimum_stock($encrypted_id){
    $this->breadcrumbs->push("Create Minimum Stock", "parent_category/create_minimum_stock");
    $data['page_title'] = "Parent Category Create Minimum Stock";
    $data['parent_category']=$this->parent_category_model->find_by_encrypted_id($encrypted_id);
    $data['codes']=$this->parent_category_model->get_pc_codes($data['parent_category']['id']);
    //$data['sub_category'] = $this->Sub_category_model->find($id);
    // $data['category'] = $this->Category_model->find($data['sub_category']['category_id']);
    // $data['stock'] = $this->parent_category_model->find_stock($data['codes']);
    $data['weight_ranges'] = $this->Weight_range_model->get('','','','',true);;
  //print_r($data);exit;
    $this->view->render('sale_master/parent_category/create_minimum_stock',$data);
  }

  public function store_minimum_stock(){
    $data =$this->minimum_stock_validation();
   //print_r($data);die;
    if (count($data) == 0) {
      $data =$this->parent_category_model->store_minimum_stock();
    }else{
      $data['status']="failure";
      $data['error']=$data;
    }
   echo json_encode($data);
  }

  private function minimum_stock_validation(){
    $postdata =$this->input->post('minimum_stock');
    $data=array();
    foreach ($postdata as $key => $value) {
        $pc_code = $value['pc_code_id'];
        foreach ($value['weight_range_id'] as $wr_key => $wr_value) {
            if (!empty($wr_value) && !is_numeric($wr_value)){
                $data[$pc_code.'_'.$wr_key]="Please Enter Valid Numeric Value";
              }
        }
    }
     return $data;
  }

}//class