<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Variations_category extends CI_Controller {

  public function __construct() {
    parent::__construct();
    if(empty($this->session->userdata('user_id'))){
      redirect(ADMIN_PATH . 'auth/logout');
    }
    $this->breadcrumbs->push("Master", "#");
    $this->breadcrumbs->push("Variations Category", "Variations_category");
    $this->load->model(array('sale_master/Variations_category_model'));
    $this->load->config('admin_validationrules', TRUE);
    $this->load->library('Data_encryption');
  }

  public function index(){
    $data['page_title'] = "All Variations Categories";
    $list=$this->input->post('list');
    if($list !="")
    {
      echo json_encode($this->generate_data_table());
    }else{
      $this->view->render('sale_master/Variations_category/index',$data);
    }
  }

  public function create(){
    $this->breadcrumbs->push("Add Variation Category", "Variations_category/create");
    $data['page_title'] = "Add Variation Category";
    $this->view->render('sale_master/Variations_category/create',$data);
  }

  private function validation_result(){
    $data = array();
    $validationResult = $this->Variations_category_model->validatepostdata();
    if($validationResult===FALSE){
      $data['status']= 'failure';
      $data['data']= '';
      $data['error'] = array(
        'type'=>strip_tags(form_error('Variations_category[type]')),
        'name'=>strip_tags(form_error('Variations_category[name]')),
        
      );
    }
    return $data;
  }

  public function store(){
    $data = array();
    $validationResult = $this->validation_result();
   // print_r($validationResult);
    if(count($validationResult) == 0){
      $data['status']= 'success';
      $data['data']= '';
      $data['user_id'] = $this->Variations_category_model->store();
      $validationResult=$data;
    }
    echo json_encode($validationResult);
  }
  public function edit($id){
    $this->breadcrumbs->push("Edit Color Stone", "Variations_category/edit");
    $data['page_title'] = "EDIT Color Stone";
    $data['Variations_category'] = $this->Variations_category_model->find_by_encrypted_id($id);
    //print_r($data);exit;
    $this->view->render('sale_master/Variations_category/edit',$data);
  }
  public function update(){

    $data = array();
    $validationResult = $this->validation_result();
   // print_r($validationResult);
    if(count($validationResult) == 0){
      $data['status']= 'success';
      $data['data']= '';
      $data['user_id'] = $this->Variations_category_model->update();
      $validationResult=$data;
    }
    echo json_encode($validationResult);
   
  }
  public function delete(){
    $result = $this->Variations_category_model->delete($_POST['id']);
    echo json_encode($result);
  }
  private function generate_data_table(){
    $status='';
    $filter_status =@$_REQUEST['order'][0];
    $search=@$_REQUEST['search']['value'];
    $result = $this->Variations_category_model->get($filter_status,$status,$_REQUEST,$search,$limit=true);
    $totalRecords = sizeof($this->Variations_category_model->get($filter_status,$status,$_REQUEST,$search,$limit=false));
    if (!empty($result)) {
        foreach ($result as $key => $value) {
            $data[$key][0] ='<span style="float:right">'. $value["type"].'</span>';
            $data[$key][1] =$value["name"];
            $pc_used =0;//$this->Variations_category_model->check_cs_exits($value['id']);
            $button_html = '';
            if($pc_used != 0){
                $onclick ="disabled";
            }else{
                $onclick ='onclick=Delete_record("'.$value['encrypted_id'].'",this,"Variations_category")';
            }
            
            $button_html  = '<span style="float:right"> <a href="'.ADMIN_PATH.'Variations_category/edit/'.$value["encrypted_id"].'"  class="btn btn-link edit_link loader-hide btn-sm" id="confim_'.$value['encrypted_id'].'">EDIT</a>&nbsp&nbsp<a '.$onclick.' class="btn btn-link delete_link loader-hide btn-sm" id="reject_'.$value['encrypted_id'].'">DELETE</a></span>';
            
            $data[$key][2]   = $button_html; 
        }
    }else{
        $data[0][0] = ['No data found'];
        $data[0][1] = [];
        $data[0][2] = [];
        
    }
     $json_data = array(
          "draw" => intval($_REQUEST['draw']),
          "recordsTotal" => intval($totalRecords),
          "recordsFiltered" => intval($totalRecords),
          "data" => $data
      );
    return $json_data; 
  }


}