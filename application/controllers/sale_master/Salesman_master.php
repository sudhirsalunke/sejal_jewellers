<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Salesman_master extends CI_Controller {

  public function __construct() {
    parent::__construct();
    if(empty($this->session->userdata('user_id'))){
      redirect(ADMIN_PATH . 'auth/logout');
    }
    $this->breadcrumbs->push("Master", "#");
    $this->breadcrumbs->push("Salesman Type", "salesman_master");
    $this->load->model(array('sale_master/salesman_master_model',
                            'common_model/State_model',
                            'common_model/City_model'));
    $this->load->config('admin_validationrules', TRUE);
    $this->load->library('Data_encryption');
  }

  public function index(){
    $data['page_title'] = "All Salesmans";
    $list=$this->input->post('list');
    if($list !="")
    {
      echo json_encode($this->generate_data_table());
    }else{
      $this->view->render('sale_master/salesman/index',$data);
    }
  }

  public function create(){
    $this->breadcrumbs->push("Add Salesmans Type", "salesmans_master/create");
    $data['page_title'] = "Add customer Master";
    $data['state']=$this->State_model->get();
    $data['city']=array();
    $this->view->render('sale_master/salesman/create',$data);
  }

  private function validation_result(){
    $data = array();
    $validationResult = $this->salesman_master_model->validatepostdata();
    if($validationResult===FALSE){
      $data['status']= 'failure';
      $data['data']= '';
      $data['error'] = array(
        'name'=>strip_tags(form_error('salesmans[name]')),
        'mobile_no'=>strip_tags(form_error('salesmans[mobile_no]')),
        'address'=>strip_tags(form_error('salesmans[address]')),
        'state_id'=>strip_tags(form_error('salesmans[state_id]')),
        'city_id'=>strip_tags(form_error('salesmans[city_id]')),
      );
    }
    return $data;
  }

  public function store(){
    $data = array();
    $validationResult = $this->validation_result();
   // print_r($validationResult);
    if(count($validationResult) == 0){
      $data['status']= 'success';
      $data['data']= '';
      $data['user_id'] = $this->salesman_master_model->store();
      $validationResult=$data;
    }
    echo json_encode($validationResult);
  }
  public function edit($id){
    $this->breadcrumbs->push("Edit Salesmans Type", "salesmans_master/edit");
    $data['page_title'] = "Edit Customer type";
    $data['state']=$this->State_model->get();
    $data['city']=array();
    $data['salesmans'] = $this->salesman_master_model->find_by_encrypted_id($id);
    //print_r($data);exit;
    $this->view->render('sale_master/salesman/edit',$data);
  }
  public function update(){

    $data = array();
    $validationResult = $this->validation_result();
   // print_r($validationResult);
    if(count($validationResult) == 0){
      $data['status']= 'success';
      $data['data']= '';
      $data['user_id'] = $this->salesman_master_model->update();
      $validationResult=$data;
    }
    echo json_encode($validationResult);
   
  }
  public function delete(){
    $result = $this->salesman_master_model->delete($_POST['id']);
    echo json_encode($result);
  }
  private function generate_data_table(){
    $status='';
    $filter_status =@$_REQUEST['order'][0];
    $search=@$_REQUEST['search']['value'];
    $result = $this->salesman_master_model->get($filter_status,$status,$_REQUEST,$search,$limit=true);
    $totalRecords = sizeof($this->salesman_master_model->get($filter_status,$status,$_REQUEST,$search,$limit=false));
    if (!empty($result)) {
        foreach ($result as $key => $value) {
          $pc_used =0;//$this->salesman_master_model->check_cust_exits($value['id']);
            $button_html = '';
            if($pc_used != 0){
                $onclick ="disabled";
            }else{
                $onclick ='onclick=Delete_record("'.$value['encrypted_id'].'",this,"salesman_master")';
            }
            
            $button_html  = '<span style="float:right"> <a href="'.ADMIN_PATH.'salesman_master/edit/'.$value["encrypted_id"].'"  class="btn btn-link edit_link small loader-hide btn-sm" id="confim_'.$value['encrypted_id'].'">EDIT</a>&nbsp&nbsp<a '.$onclick.' class="btn btn-link delete_link small loader-hide btn-sm" id="reject_'.$value['encrypted_id'].'">DELETE</a></span>';
            
            $data[$key][0] =$value["name"];
            $data[$key][1] ='<span style="float:right">'. $value["mobile_no"].'</span>';
            $data[$key][2] =$value["address"];
            $data[$key][3] =$value["city"];
            $data[$key][4] =$value["state"];
            $data[$key][5] = $button_html; 
        }
    }else{
        $data[0][0] = ['No data found'];
        $data[0][1] = []; 
        $data[0][2] = []; 
        $data[0][3] = []; 
        $data[0][4] = []; 
        $data[0][5] = []; 
    }
     $json_data = array(
          "draw" => intval($_REQUEST['draw']),
          "recordsTotal" => intval($totalRecords),
          "recordsFiltered" => intval($totalRecords),
          "data" => $data
      );
    return $json_data; 
  }

  public function get_city(){
      $result=get_successMsg();
      $result['data']=$this->City_model->get();
    echo json_encode($result);
    }

}//class