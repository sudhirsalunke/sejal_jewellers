<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Customer extends CI_Controller {

  public function __construct() {
    parent::__construct();
    if(empty($this->session->userdata('user_id'))){
      redirect(ADMIN_PATH . 'auth/logout');
    }
    $this->breadcrumbs->push("Master", "#");
    $this->breadcrumbs->push("Customer", "customer");
    $this->load->model(array('sale_master/customer_model',
                            'sale_master/sales_under_model',
                            'sale_master/customer_type_model',
                            'common_model/city_model',
                            'common_model/state_model',
                            'hallmarking_center_model', 
                            ));
    $this->load->config('admin_validationrules', TRUE);
    $this->load->library('Data_encryption');
  }

  public function index(){
    $data['page_title'] = "All Customers";
    $list=$this->input->post('list');
    if($list !="")
    {
      echo json_encode($this->generate_data_table());
    }else{
      $this->view->render('sale_master/customer/index',$data);
    }
  }

  public function create(){
    $this->breadcrumbs->push("Add Customer", "customer/create");
    $data['page_title'] = "Add Customer Master";
    $data['corporate']=$this->customer_model->get_corporate();
    $data['customer_type']=$this->customer_type_model->get();
    $data['hallmarking_center']=$this->hallmarking_center_model->get();
    $data['city']=array();
    $data['state']=$this->state_model->get();
    $data['sales_under']=$this->sales_under_model->get();
    $this->view->render('sale_master/customer/create',$data);
  }

  private function validation_result(){
    $data = array();
    $validationResult = $this->customer_model->validatepostdata();
    if($validationResult===FALSE){
      $data['status']= 'failure';
      $data['data']= '';
      $data['error'] = array(
        'name'=>strip_tags(form_error('customer[name]')),
        'contact_person'=>strip_tags(form_error('customer[contact_person]')),
        'mobile_no'=>strip_tags(form_error('customer[mobile_no]')),
        'address'=>strip_tags(form_error('customer[address]')),
        'corporate_id'=>strip_tags(form_error('customer[corporate_id]')),

        'customer_type_id'=>strip_tags(form_error('customer[customer_type_id]')),
        'area'=>strip_tags(form_error('customer[area]')),
        'state_id'=>strip_tags(form_error('customer[state_id]')),
        'city_id'=>strip_tags(form_error('customer[city_id]')),
        'limit_amt'=>strip_tags(form_error('customer[limit_amt]')),
        'limit_metal'=>strip_tags(form_error('customer[limit_metal]')),
        'days'=>strip_tags(form_error('customer[days]')),
        'sales_under'=>strip_tags(form_error('customer[sales_under]')),
        'terms'=>strip_tags(form_error('customer[terms]')),
        'hc_id'=>strip_tags(form_error('customer[hc_id]')),
        'refer_by'=>strip_tags(form_error('customer[refer_by]')),

        'bank_ac'=>strip_tags(form_error('customer[bank_ac]')),
        'bank_ifsc'=>strip_tags(form_error('customer[bank_ifsc]')), 
        'bank_name'=>strip_tags(form_error('customer[bank_name]')), 
        'bank_branch'=>strip_tags(form_error('customer[bank_branch]')), 
      );
    }
    return $data;
  }

  public function store(){
    $data = array();
    $validationResult = $this->validation_result();
   // print_r($validationResult);
    if(count($validationResult) == 0){
      $data['status']= 'success';
      $data['data']= '';
      $data['user_id'] = $this->customer_model->store();
      $validationResult=$data;
    }
    echo json_encode($validationResult);
  }
  public function edit($id){
    $this->breadcrumbs->push("Edit Customer", "customer/edit");
    $data['page_title'] = "EDIT Customer";
    $data['customer'] = $this->customer_model->find_by_encrypted_id($id);
    $data['corporate']=$this->customer_model->get_corporate();
    $data['customer_type']=$this->customer_type_model->get();
    $data['hallmarking_center']=$this->hallmarking_center_model->get();
    $data['city']=array();
    $data['state']=$this->state_model->get();
    $data['sales_under']=$this->sales_under_model->get();
    //print_r($data);exit;
    $this->view->render('sale_master/customer/edit',$data);
  }
  public function update(){

    $data = array();
    $validationResult = $this->validation_result();
   // print_r($validationResult);
    if(count($validationResult) == 0){
      $data['status']= 'success';
      $data['data']= '';
      $data['user_id'] = $this->customer_model->update();
      $validationResult=$data;
    }
    echo json_encode($validationResult);
   
  }
  public function delete(){
    $result = $this->customer_model->delete($_POST['id']);
    echo json_encode($result);
  }
  private function generate_data_table(){
    $status='';
    $filter_status =@$_REQUEST['order'][0];
    $search=@$_REQUEST['search']['value'];
    $result = $this->customer_model->get($filter_status,$status,$_REQUEST,$search,$limit=true);
    $totalRecords = $this->customer_model->get($filter_status,$status,$_REQUEST,$search,$limit=false);
    if (!empty($result)) {
        foreach ($result as $key => $value) {
            $data[$key][0] =$value["name"];
            $data[$key][1] =$value["corporate"];
            $data[$key][2] =$value["contact_person"];
            $data[$key][3] =$value["address"];
            $data[$key][4] =$value["mobile_no"];
            $pc_used =$this->customer_model->check_cust_exits($value['id']);
            $button_html = '';
            if($pc_used != 0){
                $onclick ="disabled";
            }else{
                $onclick ='onclick=Delete_record("'.$value['encrypted_id'].'",this,"customer")';
            }
            
            $button_html  = '<span style="float:right"> <a href="'.ADMIN_PATH.'customer/edit/'.$value["encrypted_id"].'"  class="btn btn-link edit_link small loader-hide  btn-sm" id="confim_'.$value['encrypted_id'].'">EDIT</a>&nbsp;&nbsp;<a '.$onclick.' class="btn btn-link delete_link small loader-hide  btn-sm" id="reject_'.$value['encrypted_id'].'">DELETE</a></span>  &nbsp';
            
            $data[$key][5]   = $button_html; 
        }
    }else{
        $data[0][0] = ['No data found'];
        $data[0][1] = [];
        $data[0][2] = [];
        $data[0][3] = [];
        $data[0][4] = [];
        $data[0][5] = [];
        
    }
     $json_data = array(
          "draw" => intval($_REQUEST['draw']),
          "recordsTotal" => intval($totalRecords),
          "recordsFiltered" => intval($totalRecords),
          "data" => $data
      );
    return $json_data; 
  }

public function get_city(){
       $result=get_successMsg();
      $result['data']=$this->city_model->get();
    echo json_encode($result);
  }

}