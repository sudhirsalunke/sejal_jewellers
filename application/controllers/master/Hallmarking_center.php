<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Hallmarking_center extends CI_Controller {

  public function __construct() {
    parent::__construct();
    if(empty($this->session->userdata('user_id'))){
      redirect(ADMIN_PATH . 'auth/logout');
    }
    $this->breadcrumbs->push("Master", "#");
    $this->breadcrumbs->push("Hallmarking Center", "Hallmarking_center");
    $this->load->model('hallmarking_center_model');
    $this->load->config('admin_validationrules', TRUE);
    $this->load->library('Data_encryption');
  }

  public function index(){
    $data['page_title'] = "All Hallmarking Center";
    $list=$this->input->post('list');
    if($list !="")
    {
      echo json_encode($this->generate_data_table());
    }else{
      $this->view->render('Hallmarking_center/index',$data);
    }
  }

  public function create(){
    $this->breadcrumbs->push("Add Hallmarking Center", "Hallmarking_center/create");
  	$data['page_title'] = "ADD HALLMARKING CENTER";
    $this->view->render('Hallmarking_center/create',$data);
  }

  public function store(){
    $data = array();
    $validationResult = $this->hallmarking_center_model->validatepostdata();
    if($validationResult===FALSE){
      $data['status']= 'failure';
      $data['data']= '';
      $data['error'] = array(
        'name'=>strip_tags(form_error('hallmarking_center[name]')),
        'code'=>strip_tags(form_error('hallmarking_center[code]'))
      );
    }else{
      $data['status']= 'success';
      $data['data']= '';
      $data['user_id'] = $this->hallmarking_center_model->store();
    }
    echo json_encode($data);
  }
  public function edit($id){
    $this->breadcrumbs->push("Edit Hallmarking Center", "Hallmarking_center/edit");
  	$data['page_title'] = "EDIT Hallmarking Center";
    $data['hallmarking_center'] = $this->hallmarking_center_model->find_by_encrypted_id($id);
    $this->view->render('Hallmarking_center/edit',$data);
  }
  public function update(){
  	$data = array();
    $validationResult = $this->hallmarking_center_model->validatepostdata();
    if($validationResult===FALSE){
      $data['status']= 'failure';
      $data['data']= '';
      $data['error'] = array(
            'name'=>strip_tags(form_error('hallmarking_center[name]')),
            'code'=>strip_tags(form_error('hallmarking_center[code]'))
      );
    }else{
      $data['status']= 'success';
      $data['data']= '';
      $data['user_id'] = $this->hallmarking_center_model->update();
    }
    echo json_encode($data);
  }
  public function delete(){
    $result = $this->hallmarking_center_model->delete($_POST['id']);
    echo json_encode($result);
  }
  private function generate_data_table(){
    $filter_status =@$_REQUEST['order'][0];
    $status = array('name','description');
    $search=@$_REQUEST['search']['value'];
    $result = $this->hallmarking_center_model->get($filter_status,$status,$_REQUEST,$search,$limit=true);
    $totalRecords = $this->hallmarking_center_model->get($filter_status,$status,$_REQUEST,$search,$limit=false);
    if (!empty($result)) {
        foreach ($result as $key => $value) {
            $data[$key][0] =$value["name"];
            $data[$key][1] =(empty($value["code"]))? '-' :$value["code"];
            $hc_used =$this->hallmarking_center_model->check_hc_used($value['id']);
            $button_html = '';
            if($hc_used != 0){
                $onclick ="disabled";
            }else{
                $onclick ='onclick=Delete_record("'.$value['encrypted_id'].'",this,"hallmarking_center")';
            }
            
            $button_html  = '<div class="pull-right"><a href="'.ADMIN_PATH.'hallmarking_center/edit/'.$value["encrypted_id"].'"  class="btn btn-link edit_link small loader-hide btn-sm" id="confim_'.$value['encrypted_id'].'">EDIT</a>&nbsp&nbsp<a '.$onclick.' class="btn btn-linkdelete_link small loader-hide btn-sm" id="reject_'.$value['encrypted_id'].'">DELETE</a></div>';
            
            $data[$key][2]   = $button_html; 
        }
    }else{
        $data[0][0] = ['No data found'];
        $data[0][1] = [];
        $data[0][2] = [];
    }
     $json_data = array(
          "draw" => intval($_REQUEST['draw']),
          "recordsTotal" => intval($totalRecords),
          "recordsFiltered" => intval($totalRecords),
          "data" => $data
      );
    return $json_data; 
  }
}