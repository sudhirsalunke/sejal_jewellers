<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class dispatch_mode extends CI_Controller {

  public function __construct() {
    parent::__construct();
    if(empty($this->session->userdata('user_id'))){
      redirect(ADMIN_PATH . 'auth/logout');
    }
    $this->breadcrumbs->push("Master", "#");
    $this->load->model('dispatch_mode_model');
    $this->load->config('admin_validationrules', TRUE);
    $this->load->library('Data_encryption');
  }

  public function index(){
    $data['page_title'] = "All Dispatch Modes";
    $data['display_title'] = "All Dispatch Modes";
    $list=$this->input->post('list');
    if($list !="")
    {
      echo json_encode($this->generate_data_table());
    }else{
      $this->view->render('dispatch_mode/index',$data);
    }
  }

  public function create(){
    $this->breadcrumbs->push("Add Dispatch Modes", "dispatch_mode/create");
  	$data['page_title'] = "ADD Dispatch Modes";
     $data['display_title'] = "Add Dispatch Mode";
    $this->view->render('dispatch_mode/create',$data);
  }

  private function validationResult(){
    $data =array();
     $validationResult = $this->dispatch_mode_model->validatepostdata();
    if($validationResult===FALSE){
      $data['status']= 'failure';
      $data['data']= '';
      $data['error'] = array(
        'mode_name'=>strip_tags(form_error('dispatch_mode[mode_name]')),
        'mode_type'=>strip_tags(form_error('dispatch_mode[mode_type]')),
      );
    }
    return $data;
  }

  public function dispatch_mode_data_store(){
    $input=$this->input->post('dispatch_mode_view');
    if(empty($input['name'])){
      $data=array('status'=>'failure','data'=>'','error'=>array('name'=>'This Field is required'));
    }else{
       $data =$this->dispatch_mode_model->store_mode_data($input);
    }
    echo json_encode($data);
  }

  public function store(){
    $data = $this->validationResult();
   if (empty($data)) {
     $data =$this->dispatch_mode_model->store();
   }
    echo json_encode($data);
  }

  public function view($id){
     $this->breadcrumbs->push("Add Dispatch Modes", "dispatch_mode/create");
     $data['page_title'] = "ADD Dispatch Modes";
     $data['display_title'] = "View Dispatch Mode";
     $data['dispatch_mode']=$this->dispatch_mode_model->find_by_encrypted_id($id);
     $data['dispatch_mode_data']=$this->dispatch_mode_model->dispatch_mode_data($id);
    $this->view->render('dispatch_mode/view',$data);
  }
  // public function edit($id){
  //   $this->breadcrumbs->push("Edit Dispatch Modes", "dispatch_mode/edit");
  // 	$data['page_title'] = "EDIT Dispatch Modes";
  //   $data['dispatch_mode'] = $this->dispatch_mode_model->find_by_encrypted_id($id);
  //   $this->view->render('dispatch_mode/edit',$data);
  // }
  // public function update(){
  // 	$data = array();
  //   $validationResult = $this->dispatch_mode_model->validatepostdata();
  //   if($validationResult===FALSE){
  //     $data['status']= 'failure';
  //     $data['data']= '';
  //     $data['error'] = array(
  //           'name'=>strip_tags(form_error('dispatch_mode[name]')),
  //           'code'=>strip_tags(form_error('dispatch_mode[code]'))
  //     );
  //   }else{
  //     $data['status']= 'success';
  //     $data['data']= '';
  //     $data['user_id'] = $this->dispatch_mode_model->update();
  //   }
  //   echo json_encode($data);
  // }
  // public function delete(){
  //   $result = $this->dispatch_mode_model->delete($_POST['id']);
  //   echo json_encode($result);
  // }
  private function generate_data_table(){
    $filter_status =@$_REQUEST['order'][0];
    $status = array('name','description');
    $search=@$_REQUEST['search']['value'];
    $result = $this->dispatch_mode_model->get($filter_status,$status,$_REQUEST,$search,$limit=true);
    $totalRecords = $this->dispatch_mode_model->get($filter_status,$status,$_REQUEST,$search,$limit=false);
    if (!empty($result)) {
        foreach ($result as $key => $value) {
          $button_html = '';
          // if($hc_used != 0){
            //     $onclick ="disabled";
            // }else{
            //     $onclick ='onclick=Delete_record("'.$value['encrypted_id'].'",this,"dispatch_mode")';
            // }
            
            //$button_html  = '<span style="float:right"> <a href="'.ADMIN_PATH.'dispatch_mode/edit/'.$value["encrypted_id"].'"  class="btn btn-info small loader-hide  btn-sm" id="confim_'.$value['encrypted_id'].'">EDIT</a>&nbsp  &nbsp <a '.$onclick.' class="btn btn-danger small loader-hide  btn-sm" id="reject_'.$value['encrypted_id'].'">DELETE</a></span>  &nbsp';


          $button_html  = '<span style="float:right"> <a href="'.ADMIN_PATH.'dispatch_mode/view/'.$value["encrypted_id"].'"  class="btn btn-sm btn-link view_link small loader-hide btn-sm " id="confim_'.$value['encrypted_id'].'">VIEW</a></span>  &nbsp ';
            $sr_no=$key+1;
            $data[$key][0] ='<span style="float:right">'.$sr_no.'</span>';


            $data[$key][1] =$value["mode_name"];
            $data[$key][2] =input_types($value['mode_type']);
            $data[$key][3] =($value['is_img']==1) ?'Yes' : 'No';
            $hc_used =0;//$this->dispatch_mode_model->check_hc_used($value['id']);
            
            $data[$key][4]   = $button_html; 
        }
    }else{
        $data[0][0] = ['No data found'];
        $data[0][1] = [];
        $data[0][2] = [];
        $data[0][3] = [];
        $data[0][4] = [];
    }
     $json_data = array(
          "draw" => intval($_REQUEST['draw']),
          "recordsTotal" => intval($totalRecords),
          "recordsFiltered" => intval($totalRecords),
          "data" => $data
      );
    return $json_data; 
  }
}