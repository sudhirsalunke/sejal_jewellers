<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Karigar extends CI_Controller {

  public function __construct() {
    parent::__construct();
    if(empty($this->session->userdata('user_id'))){
      redirect(ADMIN_PATH . 'auth/logout');
    }
    $this->breadcrumbs->push("Master", "#");
    $this->breadcrumbs->push("Karigar", "Karigar");
    $this->load->model(array('Karigar_model',
                      'hallmarking_center_model',
                      'common_model/state_model',
                      'common_model/city_model',
                      'sale_master/customer_type_model',
                      'sale_master/sales_under_model'
                        ));
    $this->load->config('admin_validationrules', TRUE);
    $this->load->library('Data_encryption');
  }

  public function index(){
    $data['page_title'] = "All Karigars";
    $list=$this->input->post('list');
    if($list !="")
    {
      echo json_encode($this->generate_data_table());
    }else{
      $this->view->render('Karigar/index',$data);
    }
  }

  public function create(){
     $this->breadcrumbs->push("Add Karigar", "Karigar/create");
  	$data['page_title'] = "ADD Karigar";
    $data['customer_type']=$this->customer_type_model->get();
    $data['hallmarking_center']=$this->hallmarking_center_model->get('','','','',true);
    $data['city']=array();
    $data['state']=$this->state_model->get();
    $data['sales_under']=$this->sales_under_model->get();
    $this->view->render('Karigar/create',$data);
  }

  private function validationResult(){
    $data = array();
    $validationResult = $this->Karigar_model->validatepostdata();
    if($validationResult===FALSE){
      $data['status']= 'failure';
      $data['data']= '';
      $data['error'] = array(
        'name'=>strip_tags(form_error('Karigar[name]')),       
        'wastage'=>strip_tags(form_error('Karigar[wastage]')),     
        'customer_type_id'=>strip_tags(form_error('Karigar[customer_type_id]')),

      );
    }
    return $data;
  }

  public function store(){
    $data = $this->validationResult();
    if(count($data) == 0){
      $data['status']= 'success';
      $data['data']= '';
      $data['user_id'] = $this->Karigar_model->store();
    }
    echo json_encode($data);
  }
  public function edit($id){
     $this->breadcrumbs->push("Edit Karigar", "Karigar/edit");
  	$data['page_title'] = "EDIT Karigar";
    $data['id'] = $this->Karigar_model->get_primary_key($id);
    $data['karigar'] = $this->Karigar_model->find($data['id']);
    $data['customer_type']=$this->customer_type_model->get();
    $data['hallmarking_center']=$this->hallmarking_center_model->get('','','','',true);
    $data['city']=array();
    $data['state']=$this->state_model->get();
    $data['sales_under']=$this->sales_under_model->get();
    $this->view->render('Karigar/edit',$data);
  }
  public function update(){
     $data = $this->validationResult();
    if(count($data) == 0){
      $data['status']= 'success';
      $data['data']= '';
      $data['user_id'] = $this->Karigar_model->update();
    }
    echo json_encode($data);
  	
  }

  public function delete(){
  	$data['id'] = $this->Karigar_model->get_primary_key($_POST['id']);
    $result = $this->Karigar_model->delete($data['id']);
    echo json_encode($result);
  }
  private function generate_data_table(){
    $filter_status =@$_REQUEST['order'][0];
    $status = array('name','code');
    $search=@$_REQUEST['search']['value'];
    $result = $this->Karigar_model->get($filter_status,$status,$_REQUEST,$search,$limit=true);
    $totalRecords = $this->Karigar_model->get($filter_status,$status,$_REQUEST,$search,$limit=false);
    if (!empty($result)) {
        foreach ($result as $key => $value) {
         $karigar_used = check_master_in_used('karigar_id',$value['id'],'product');
         $karigar_used_corp = check_master_in_used('karigar_id',$value['id'],'Prepare_product_list');
         $karigar_used_order = check_master_in_used('karigar_id',$value['id'],'customer_orders');
         $karigar_used_mfg = check_master_in_used('karigar_id',$value['id'],'Karigar_manufacturing_mapping');
        /*  echo $this->db->last_query(); 
          print_r($value['id']);
          print_r( $karigar_used_corp);die;*/
            $data[$key][0] =$value["name"];
            $data[$key][1] =$value["code"];
            $data[$key][2] =$value["wastage"];
            $button_html = '';
            if($karigar_used >0 || $karigar_used_corp > 0 || $karigar_used_order > 0 || $karigar_used_mfg >0)
            {
               $button_html  = '<span style="float:right"><a href="'.ADMIN_PATH.'Karigar/merge_replace_karigar_list/'.$value["id"].'"  class="btn btn-link prime_link small loader-hide  btn-sm" id="confim_'.$value['encrypted_id'].'">Replace & Merge</a>&nbsp;&nbsp;<a href="'.ADMIN_PATH.'Karigar/edit/'.$value["encrypted_id"].'"  class="btn btn-link edit_link small loader-hide btn-sm" id="confim_'.$value['encrypted_id'].'">EDIT</a>&nbsp;&nbsp;<a disabled class="btn btn-link delete_link small loader-hide btn-sm" id="reject_'.$value['encrypted_id'].'">DELETE</a></span>  &nbsp';
            }
            else
            {
               $button_html  = '<span style="float:right"><a href="'.ADMIN_PATH.'Karigar/merge_replace_karigar_list/'.$value["id"].'"  class="btn btn-link prime_link small loader-hide  btn-sm" id="confim_'.$value['encrypted_id'].'">Replace & Merge</a>&nbsp;&nbsp;<a href="'.ADMIN_PATH.'Karigar/edit/'.$value["encrypted_id"].'"  class="btn btn-link edit_link small loader-hide  btn-sm" id="confim_'.$value['encrypted_id'].'">EDIT</a>&nbsp;&nbsp;<a onclick=Delete_record("'.$value['encrypted_id'].'",this,"Karigar") class="btn btn-link delete_link small loader-hide btn-sm" id="reject_'.$value['encrypted_id'].'">DELETE</a></span>  &nbsp';
            }
            $data[$key][3]   = $button_html;
            
        }
    }else{
        $data[0][0] = ['No data found'];
        $data[0][1] = [];
        $data[0][2] = [];
        $data[0][3] = [];
    }
     $json_data = array(
          "draw" => intval($_REQUEST['draw']),
          "recordsTotal" => intval($totalRecords),
          "recordsFiltered" => intval($totalRecords),
          "data" => $data
      );
    return $json_data; 
  }
  public function get_city(){
       $result=get_successMsg();
      $result['data']=$this->city_model->get();
    echo json_encode($result);
  }
  public function get_wastage(){
       $result=get_successMsg();
      $result['data']=$this->Karigar_model->find($_POST['karigar_id']);
      //print_r($result['data']);die;
    echo json_encode($result);
  }

  public function merge_replace_karigar_list($karigar_id){
    //print_r($id);die;
     $this->breadcrumbs->push("Merge Replace Karigar", "Karigar/merge_replace");
     $data['page_title'] = "All Karigars Merge Replace";
     $data['karigar_id']=$karigar_id;
     $list=$this->input->post('list');
    if($list !="")
    {
      echo json_encode($this->generate_merge_data_table($karigar_id));
    }else{
      $this->view->render('Karigar/replace',$data);
    };
  }

  public function merge_replace()
  {
     $data = $this->Karigar_model->merge_replace($_POST['id'],$_POST['karigar_id']);
     echo json_encode($data);
  }

  private function generate_merge_data_table($karigar_id){
    //echo"sss";die;
    $filter_status =@$_REQUEST['order'][0];
    $status = array('name','code');
    $search=@$_REQUEST['search']['value'];
    $result = $this->Karigar_model->get($filter_status,$status,$_REQUEST,$search,$limit=true);
    //echo $this->db->last_query();die; 
    $totalRecords = $this->Karigar_model->get($filter_status,$status,$_REQUEST,$search,$limit=false);
    if (!empty($result)) {
        foreach ($result as $key => $value) {
         $karigar_used = check_master_in_used('karigar_id',$value['id'],'product');
         $karigar_used_corp = check_master_in_used('karigar_id',$value['id'],'Prepare_product_list');
         $karigar_used_order = check_master_in_used('karigar_id',$value['id'],'customer_orders');
         $karigar_used_mfg = check_master_in_used('karigar_id',$value['id'],'Karigar_manufacturing_mapping');
        /*  echo $this->db->last_query(); 
          print_r($value['id']);
          print_r( $karigar_used_corp);die;*/
            $data[$key][0] =$value["name"];
            $data[$key][1] =$value["code"];
            $data[$key][2] =$value["limit_wt"];
            $button_html = '';
            if($karigar_id==$value['id']){
              $button_html  = '<span style="float:right"><a disabled class="btn btn-link delete_link small loader-hide btn-sm" id="reject_'.$value['encrypted_id'].'">Cancel</a></span>  &nbsp';
            }else{
            $button_html  = '<span style="float:right"><a onclick=karigar_replace_record("'.$value['id'].'",this,"'.$karigar_id.'","Karigar") class="btn btn-link prime_link small loader-hide  btn-sm" id="confim_'.$value['encrypted_id'].'">select</a>&nbsp;&nbsp;<a disabled class="btn btn-link delete_link small loader-hide btn-sm" id="reject_'.$value['encrypted_id'].'">Cancel</a></span>  &nbsp';
            }
            

            $data[$key][3]   = $button_html;
            
        }
    }else{
        $data[0][0] = ['No data found'];
        $data[0][1] = [];
        $data[0][2] = [];
        $data[0][3] = [];
    }
     $json_data = array(
          "draw" => intval($_REQUEST['draw']),
          "recordsTotal" => intval($totalRecords),
          "recordsFiltered" => intval($totalRecords),
          "data" => $data
      );
    return $json_data; 
  }

}