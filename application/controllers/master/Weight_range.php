<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Weight_range extends CI_Controller {

  public function __construct() {
    parent::__construct();
    if(empty($this->session->userdata('user_id'))){
      redirect(ADMIN_PATH . 'auth/logout');
    }
    $this->breadcrumbs->push("Master", "#");
    $this->breadcrumbs->push("Weight Range", "Weight_range");
    $this->load->model('Weight_range_model');
    $this->load->config('admin_validationrules', TRUE);
    $this->load->library('Data_encryption');
  }

  public function index(){
    $data['page_title'] = "All Weight Ranges";
    $list=$this->input->post('list');
    if($list !="")
    {
      echo json_encode($this->generate_data_table());
    }else{
      $this->view->render('Weight_range/index',$data);
    }
  }

  public function create(){
    $this->breadcrumbs->push("Add Weight Range", "Weight_range/create");
  	$data['page_title'] = "ADD WEIGHT RANGE";
    $this->view->render('Weight_range/create',$data);
  }

  public function store(){
    $data = array();
    $validationResult = $this->Weight_range_model->validatepostdata();
    if($validationResult===FALSE){
      $data['status']= 'failure';
      $data['data']= '';
      $data['error'] = array(
        'from'=>strip_tags(form_error('Weight_range[from]')),
        'to'=>strip_tags(form_error('Weight_range[to]'))
      );
    }else{
      $data['status']= 'success';
      $data['data']= '';
      $data['user_id'] = $this->Weight_range_model->store();
    }
    echo json_encode($data);
  }
  public function edit($id){
    $this->breadcrumbs->push("Edit Weight Range", "Weight_range/edit");
  	$data['page_title'] = "EDIT WEIGHT RANGE";
    $data['id'] = $this->Weight_range_model->get_primary_key($id);
    $data['weight_range'] = $this->Weight_range_model->find($data['id']);
    $this->view->render('Weight_range/edit',$data);
  }
  public function update(){
  	$data = array();
    $validationResult = $this->Weight_range_model->validatepostdata();
    if($validationResult===FALSE){
      $data['status']= 'failure';
      $data['data']= '';
      $data['error'] = array(
        'from'=>strip_tags(form_error('Weight_range[from]')),
        'to'=>strip_tags(form_error('Weight_range[to]'))
      );
    }else{
      $data['status']= 'success';
      $data['data']= '';
      $data['user_id'] = $this->Weight_range_model->update();
    }
    echo json_encode($data);
  }
  public function delete(){
  	$data['id'] = $this->Weight_range_model->get_primary_key($_POST['id']);
    $result = $this->Weight_range_model->delete($data['id']);
    echo json_encode($result);
  }
  private function generate_data_table(){
    $filter_status =@$_REQUEST['order'][0];
    $status = array('from_weight','to_weight');
    $search=@$_REQUEST['search']['value'];
    $result = $this->Weight_range_model->get($filter_status,$status,$_REQUEST,$search,$limit=true);
    $totalRecords = $this->Weight_range_model->get($filter_status,$status,$_REQUEST,$search,$limit=false);
    if (!empty($result)) {
        foreach ($result as $key => $value) {
            $data[$key][0] ='<span style="float:right">'. $value["from_weight"].'</span>';
            $data[$key][1] ='<span style="float:right">'. $value["to_weight"].'</span>';
      
            $weight_range_used = check_master_in_used('weight_band_id',$value['id'],'product');
            $button_html = '';
            if($weight_range_used>0)
            {
               $button_html  = '<span style="float:right"><a href="'.ADMIN_PATH.'Weight_range/edit/'.$value["encrypted_id"].'"  class="btn btn-link edit_link small loader-hide btn-sm" id="confim_'.$value['encrypted_id'].'">EDIT</a>&nbsp;&nbsp;<a disabled class="btn btn-link delete_link small loader-hide btn-sm" id="reject_'.$value['encrypted_id'].'">DELETE</a></span>';
            }
            else
            {
               $button_html  = '<span style="float:right"> <a href="'.ADMIN_PATH.'Weight_range/edit/'.$value["encrypted_id"].'"  class="btn btn-link edit_link small loader-hide btn-sm" id="confim_'.$value['encrypted_id'].'">EDIT</a>&nbsp;&nbsp;<a onclick=Delete_record("'.$value['encrypted_id'].'",this,"Weight_range") class="btn btn-link delete_link small loader-hide btn-sm" id="reject_'.$value['encrypted_id'].'">DELETE</a></span>';
            }
            $data[$key][2]   = $button_html; 
        }
    }else{
        $data[0][0] = ['No data found'];
        $data[0][1] = [];
        $data[0][2] = [];
    }
     $json_data = array(
          "draw" => intval($_REQUEST['draw']),
          "recordsTotal" => intval($totalRecords),
          "recordsFiltered" => intval($totalRecords),
          "data" => $data
      );
    return $json_data; 
  }
}