<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Pcs extends CI_Controller {

  public function __construct() {
    parent::__construct();
    if(empty($this->session->userdata('user_id'))){
      redirect(ADMIN_PATH . 'auth/logout');
    }
    $this->breadcrumbs->push("Master", "#");
    $this->breadcrumbs->push("Pcs", "Pcs");
    $this->load->model('Pcs_model');
    $this->load->config('admin_validationrules', TRUE);
    $this->load->library('Data_encryption');
  }

  public function index(){
    $data['page_title'] = "All PCS";
    $list=$this->input->post('list');
    if($list !="")
    {
      echo json_encode($this->generate_data_table());
    }else{
      $this->view->render('pcs/index',$data);
    }
  }
  public function create(){
    $this->breadcrumbs->push("Add Pcs", "Pcs/create");
  	$data['page_title'] = "ADD PCS";
    $this->view->render('pcs/create',$data);
  }

  public function store(){
    $data = array();
    $validationResult = $this->Pcs_model->validatepostdata();
    if($validationResult===FALSE){
      $data['status']= 'failure';
      $data['data']= '';
      $data['error'] = array(
        'name'=>strip_tags(form_error('pcs[name]')),
      );
    }else{
      $data['status']= 'success';
      $data['data']= '';
      $data['user_id'] = $this->Pcs_model->store();
    }
    echo json_encode($data);
  }
  public function edit($id){
    $this->breadcrumbs->push("Edit Pcs", "Pcs/edit");
  	$data['page_title'] = "EDIT PCS";
    $data['id'] = $this->Pcs_model->get_primary_key($id);
    $data['pcs'] = $this->Pcs_model->find($data['id']);
    $this->view->render('pcs/edit',$data);
  }
  public function update(){
  	$data = array();
    $validationResult = $this->Pcs_model->validatepostdata();
    if($validationResult===FALSE){
      $data['status']= 'failure';
      $data['data']= '';
      $data['error'] = array(
        'name'=>strip_tags(form_error('pcs[name]')),
      );
    }else{
      $data['status']= 'success';
      $data['data']= '';
      $data['user_id'] = $this->Pcs_model->update();
    }
    echo json_encode($data);
  }
  public function delete(){
  	$data['id'] = $this->Pcs_model->get_primary_key($_POST['id']);
    $result = $this->Pcs_model->delete($data['id']);
    echo json_encode($result);
  }
  private function generate_data_table(){
    $filter_status =@$_REQUEST['order'][0];
    $status = array('name');
    $search=@$_REQUEST['search']['value'];
    $result = $this->Pcs_model->get($filter_status,$status,$_REQUEST,$search,$limit=true);
    $totalRecords = $this->Pcs_model->get($filter_status,$status,$_REQUEST,$search,$limit=false);
    if (!empty($result)) {
        foreach ($result as $key => $value) {
          $pcs_used = check_master_in_used('pcs_id',$value['id'],'product');
            $data[$key][0] ='<span style="float:right">'. $value["name"].'</span>';
            $button_html = '';
            if($pcs_used>0)
            {
               $button_html  = '<span style="float:right"> <a href="'.ADMIN_PATH.'Pcs/edit/'.$value["encrypted_id"].'"  class="btn btn-link edit_link  small loader-hide btn-sm" id="confim_'.$value['encrypted_id'].'">EDIT</a>&nbsp&nbsp<a disabled class="btn btn-link delete_link small loader-hide btn-sm" id="reject_'.$value['encrypted_id'].'">DELETE</a></span>';
            }
            else
            {
               $button_html  = '<span style="float:right"> <a href="'.ADMIN_PATH.'Pcs/edit/'.$value["encrypted_id"].'"  class="btn btn-link edit_link small loader-hide btn-sm" id="confim_'.$value['encrypted_id'].'">EDIT</a>&nbsp&nbsp<a onclick=Delete_record("'.$value['encrypted_id'].'",this,"Pcs") class="btn btn-link delete_link small loader-hide btn-sm" id="reject_'.$value['encrypted_id'].'">DELETE</a></span>';
            }
            $data[$key][1]   = $button_html;
        }
    }else{
        $data[0][0] = ['No data found'];
        $data[0][1] = [];
    }
     $json_data = array(
          "draw" => intval($_REQUEST['draw']),
          "recordsTotal" => intval($totalRecords),
          "recordsFiltered" => intval($totalRecords),
          "data" => $data
      );
    return $json_data; 
  }
}