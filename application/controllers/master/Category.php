<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Category extends CI_Controller {

  public function __construct() {
    parent::__construct();
    if(empty($this->session->userdata('user_id'))){
      redirect(ADMIN_PATH . 'auth/logout');
    }
    $this->breadcrumbs->push("Master", "#");
    $this->breadcrumbs->push("Category", "Category");
    $this->load->model('Category_model');
    $this->load->model('Manufacturing_module/Department_model');
    $this->load->config('admin_validationrules', TRUE);
    $this->load->library('Data_encryption');
    $this->load->library('Breadcrumbs');
  }

  public function index(){
    $data['page_title'] = "All Catagories";
    $list=$this->input->post('list');
    if($list !="")
    {
      echo json_encode($this->generate_data_table());
    }else{
      $this->view->render('Category/index',$data);
    }
  }

  public function create(){
  	$data['page_title'] = "ADD CATEGORY";
    $this->breadcrumbs->push("Add Category", "Category/create");
    $data['department'] = $this->Department_model->get('','','','',true);
    $this->view->render('Category/create',$data);
  }

  public function store(){
    $data = array();
    $validationResult = $this->Category_model->validatepostdata();

    
    if($validationResult===FALSE){
      $data['status']= 'failure';
      $data['data']= '';
      $data['error'] = array(
        'name'=>strip_tags(form_error('Category[name]')),
        'code'=>strip_tags(form_error('Category[code]'))
      );
    }else{
      $data['status']= 'success';
      $data['data']= '';
      $data['user_id'] = $this->Category_model->store();
    }
    echo json_encode($data);
  }
  public function edit($id){
  	$data['page_title'] = "EDIT CATEGORY";
    $this->breadcrumbs->push("Edit Category", "Category/edit");
    $data['id'] = $this->Category_model->get_primary_key($id);
    $data['department'] = $this->Department_model->get('','','','',true);
    $data['category'] = $this->Category_model->find($data['id']);
    $this->view->render('Category/edit',$data);
  }
  public function update(){
  	$data = array();
    $validationResult = $this->Category_model->validatepostdata();
    if($validationResult===FALSE){
      $data['status']= 'failure';
      $data['data']= '';
      $data['error'] = array(
          'name'=>strip_tags(form_error('Category[name]')),
          'code'=>strip_tags(form_error('Category[code]'))
      );
    }else{
      $data['status']= 'success';
      $data['data']= '';
      $data['user_id'] = $this->Category_model->update();
    }
    echo json_encode($data);
  }
  public function delete(){
  	$data['id'] = $this->Category_model->get_primary_key($_POST['id']);
    $result = $this->Category_model->delete($data['id']);
    echo json_encode($result);
  }
  private function generate_data_table(){
    $filter_status =@$_REQUEST['order'][0];
    $status = array('name','code');
    $search=@$_REQUEST['search']['value'];
    $result = $this->Category_model->get($filter_status,$status,$_REQUEST,$search,$limit=true);
    $totalRecords = $this->Category_model->get($filter_status,$status,$_REQUEST,$search,$limit=false);
    if (!empty($result)) {
        foreach ($result as $key => $value) {
          $category_used = check_master_in_used('category_id',$value['id'],'product');
            $data[$key][] =$value["name"];
            $data[$key][] =$value["code"];
     /*       $data[$key][2] =$value["department"];*/
            $data[$key][] =blank_value($value["purity"]);
            $data[$key][] =blank_value($value["wastage"]);
            $data[$key][] =blank_value($value["labore"]);
            $button_html = '';
            if($category_used>0)
            {
               $button_html  = '<div class="pull-right"> <a href="'.ADMIN_PATH.'Category/edit/'.$value["encrypted_id"].'"  class="btn btn-link edit_link loader-hide btn-sm" id="confim_'.$value['encrypted_id'].'">EDIT</a>&nbsp;&nbsp;<a  disabled class="btn btn-link  delete_link loader-hide btn-sm" id="reject_'.$value['encrypted_id'].'">DELETE</a></div>';
            }
            else
            {
               $button_html  = '<div class="pull-right"> <a href="'.ADMIN_PATH.'Category/edit/'.$value["encrypted_id"].'"  class="btn btn-link edit_link loader-hide btn-sm" id="confim_'.$value['encrypted_id'].'">EDIT</a>&nbsp;&nbsp;<a onclick=Delete_record("'.$value['encrypted_id'].'",this,"Category") class="btn btn-link delete_link loader-hide btn-sm" id="reject_'.$value['encrypted_id'].'">DELETE</a></div>';
            }
            $data[$key][]   = $button_html;
        }
    }else{
        $data[0][0] = ['No data found'];
        $data[0][1] = [];
        $data[0][2] = [];
        $data[0][3] = [];
        $data[0][4] = [];
        $data[0][5] = [];
        $data[0][6] = [];
    }
     $json_data = array(
          "draw" => intval($_REQUEST['draw']),
          "recordsTotal" => intval($totalRecords),
          "recordsFiltered" => intval($totalRecords),
          "data" => $data
      );
    return $json_data; 
  }
}