<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Sub_category extends CI_Controller {

  public function __construct() {
    parent::__construct();
    if(empty($this->session->userdata('user_id'))){
      redirect(ADMIN_PATH . 'auth/logout');
    }
    $this->breadcrumbs->push("Master", "#");
    $this->breadcrumbs->push("Sub Category", "Sub_category");
    $this->load->model(array('Sub_category_model','Category_model','Weight_range_model'));
    $this->load->config('admin_validationrules', TRUE);
    $this->load->config('Dropbox', TRUE);
    $this->load->library(array('Data_encryption','Dropbox'));
  }
  public function index($type=''){
    if($type=="getCategoryWiseData"){
      $res = $this->Sub_category_model->get('','','','',true,$this->input->post('category_id'));
      echo json_encode($res);
      exit();
    }
    $data['page_title'] = "All Sub Catagories";
    $list=$this->input->post('list');
    if($list !="")
    {
      echo json_encode($this->generate_data_table());
    }else{
      $this->view->render('Sub_category/index',$data);
    }
  }

  public function create(){
    $this->breadcrumbs->push("Add Sub Category", "Sub_category/create");
  	$data['page_title'] = "ADD SUB CATEGORY";
    $data['category'] = $this->Category_model->get('','','','',true);
    $this->view->render('Sub_category/create',$data);
  }

  public function store(){
    $data = array();
    $validationResult = $this->Sub_category_model->validatepostdata();
    if($validationResult===FALSE){
      $data['status']= 'failure';
      $data['data']= '';
      $data['error'] = array(
        'category_id'=>strip_tags(form_error('Sub_category[category_id]')),
        'name'=>strip_tags(form_error('Sub_category[name]')),
        'code'=>strip_tags(form_error('Sub_category[code]'))
      );
    }else{
      $data['status']= 'success';
      $data['data']= '';
      $data['user_id'] = $this->Sub_category_model->store();
    }
    echo json_encode($data);
  }
  public function edit($id){
     $this->breadcrumbs->push("Edit Sub Category", "Sub_category/edit");
  	$data['page_title'] = "EDIT SUB CATEGORY";
    $id= $this->Sub_category_model->get_primary_key($id);
    $data['category'] = $this->Category_model->get('','','','',true);
    $data['sub_category'] = $this->Sub_category_model->find($id);
    $this->view->render('Sub_category/edit',$data);
  }

  public function update(){
  	$data = array();
    $validationResult = $this->Sub_category_model->validatepostdata();
    if($validationResult===FALSE){
      $data['status']= 'failure';
      $data['data']= '';
      $data['error'] = array(
          'name'=>strip_tags(form_error('Sub_category[name]')),
          'code'=>strip_tags(form_error('Sub_category[code]'))
      );
    }else{
      $data['status']= 'success';
      $data['data']= '';
      $data['user_id'] = $this->Sub_category_model->update();
    }
    echo json_encode($data);
  }

 
  public function delete(){
  	$data['id'] = $this->Sub_category_model->get_primary_key($_POST['id']);
    $result = $this->Sub_category_model->delete($data['id']);
    echo json_encode($result);
  }
  private function generate_data_table(){
    $filter_status =@$_REQUEST['order'][0];
    $status = array('scm.name','cm.name','scm.code');
    $search=@$_REQUEST['search']['value'];
    $result = $this->Sub_category_model->get($filter_status,$status,$_REQUEST,$search,$limit=true);
    $totalRecords = $this->Sub_category_model->get($filter_status,$status,$_REQUEST,$search,$limit=false);
    if (!empty($result)) {
        foreach ($result as $key => $value) {
          $sub_category_used = check_master_in_used('sub_category_id',$value['id'],'product');
            $data[$key][0] =$value["name"];
            $data[$key][1] =$value["category_name"];
            $data[$key][2] =$value["code"];
            $button_html = '';
            if($sub_category_used>0)
            {
               /*$button_html  = '<span style="float:right"> <a href="'.ADMIN_PATH.'Sub_category/create_minimum_stock/'.$value["encrypted_id"].'"  class="btn btn-info small loader-hide  btn-sm" id="confim_'.$value['encrypted_id'].'">ENTER MINIMUM STOCK</a>&nbsp  &nbsp<a href="'.ADMIN_PATH.'Sub_category/edit/'.$value["encrypted_id"].'"  class="btn btn-info small loader-hide  btn-sm" id="confim_'.$value['encrypted_id'].'">EDIT</a>&nbsp  <a href="'.ADMIN_PATH.'Sub_category/view/'.$value["encrypted_id"].'" class="btn btn-primary small loader-hide  btn-sm" id="reject_'.$value['encrypted_id'].'" >View</a>&nbsp <a href="javascript:void(0);" class="btn btn-primary small loader-hide  btn-sm" id="reject_'.$value['encrypted_id'].'" Onclick="fetch_sub_category_img_from_dropbox('.$value['id'].')">Upload Images</a>&nbsp <a disabled class="btn btn-danger small loader-hide  btn-sm" id="reject_'.$value['encrypted_id'].'">DELETE</a></span>  &nbsp';*/
               $button_html  = '<div class="pull-right"><a href="'.ADMIN_PATH.'Sub_category/edit/'.$value["encrypted_id"].'"  class="btn btn-link btn-sm edit_link" id="confim_'.$value['encrypted_id'].'">EDIT</a>&nbsp;&nbsp;<a href="'.ADMIN_PATH.'Sub_category/view/'.$value["encrypted_id"].'" class="btn btn-link view_link btn-sm" id="reject_'.$value['encrypted_id'].'" >View</a>&nbsp;&nbsp;<a disabled class="btn btn-link delete_link btn-sm" id="reject_'.$value['encrypted_id'].'">DELETE</a></div>';
            }
            else
            {
               /*$button_html  = '<span style="float:right"> <a href="'.ADMIN_PATH.'Sub_category/create_minimum_stock/'.$value["encrypted_id"].'"  class="btn btn-info small loader-hide  btn-sm" id="confim_'.$value['encrypted_id'].'">ENTER MINIMUM STOCK</a>&nbsp  &nbsp<a href="'.ADMIN_PATH.'Sub_category/edit/'.$value["encrypted_id"].'"  class="btn btn-info small loader-hide  btn-sm" id="confim_'.$value['encrypted_id'].'">EDIT</a>&nbsp <a href="'.ADMIN_PATH.'Sub_category/view/'.$value["encrypted_id"].'" class="btn btn-primary small loader-hide  btn-sm" id="reject_'.$value['encrypted_id'].'" >View</a>&nbsp <a href="javascript:void(0);" class="btn btn-primary small loader-hide  btn-sm" id="reject_'.$value['encrypted_id'].'" Onclick="fetch_sub_category_img_from_dropbox('.$value['id'].')">Upload Images</a>&nbsp <a onclick=Delete_record("'.$value['encrypted_id'].'",this,"Sub_category") class="btn btn-danger small loader-hide  btn-sm" id="reject_'.$value['encrypted_id'].'">DELETE</a></span>  &nbsp';*/
               $button_html  = '<div class="pull-right"><a href="'.ADMIN_PATH.'Sub_category/edit/'.$value["encrypted_id"].'"  class="btn btn-link edit_link btn-sm pull-left" id="confim_'.$value['encrypted_id'].'">EDIT</a>&nbsp; <a href="'.ADMIN_PATH.'Sub_category/view/'.$value["encrypted_id"].'" class="btn btn-link view_link btn-sm" id="reject_'.$value['encrypted_id'].'" >View</a>&nbsp;&nbsp; <a disabled class="btn btn-link delete_link btn-sm pull-right" id="reject_'.$value['encrypted_id'].'">DELETE</a></div>';
            }
            $data[$key][3]   = $button_html;
        }
    }else{
        $data[0][0] = ['No data found'];
        $data[0][1] = [];
        $data[0][2] = [];
        $data[0][3] = [];
    }
     $json_data = array(
          "draw" => intval($_REQUEST['draw']),
          "recordsTotal" => intval($totalRecords),
          "recordsFiltered" => intval($totalRecords),
          "data" => $data
      );
    return $json_data; 
  }
  function create_minimum_stock($encrypted_id){
    $this->breadcrumbs->push("Create Minimum Stock", "Sub_category/create_minimum_stock");
    $data['page_title'] = "Create Minimum Stock";
    $id= $this->Sub_category_model->get_primary_key($encrypted_id);
    $data['sub_category'] = $this->Sub_category_model->find($id);
    $data['category'] = $this->Category_model->find($data['sub_category']['category_id']);
    $data['stock'] = $this->Sub_category_model->find_stock($id);
    $data['weight_ranges'] = $this->Weight_range_model->get();
    $this->view->render('Sub_category/create_minimum_stock',$data);
  }
  function save_minimum_stock(){
    $result = $this->Sub_category_model->validate_minimum_stock();
    if($result['status'] == 'success'){
      $result = $this->Sub_category_model->save_minimum_stock();
    }
    echo json_encode($result);die;
  }

  public function view($id){
    $this->breadcrumbs->push("Sub Category Details", "Sub_category/view");
    $data['page_title'] = "Sub Category Details";
    $id= $this->Sub_category_model->get_primary_key($id);
    $data['category'] = $this->Category_model->get('','','','',true);
    $data['sub_category'] = $this->Sub_category_model->find($id);
    $data['sub_category_images'] = $this->Sub_category_model->sub_category_images($id);
    $this->view->render('Sub_category/view',$data);
  }  


  public function view_dropbox_image($id)
  {
      $this->breadcrumbs->push("Sub Category Image Details", "Sub_category/view_dropbox_image");
      $data['page_title'] = "Sub Category Image Details";
      $id= $this->Sub_category_model->get_primary_key($id);
      $data['category'] = $this->Category_model->get();
      $data['sub_category'] = $this->Sub_category_model->find($id);
      $data['sub_category_images'] = $this->Sub_category_model->sub_category_images($id);
      $this->view->render('Sub_category/view_dropbox_image',$data);
  }

  public function fetch_images_from_dropbox(){
    $path  = "sub_category";
    $this->db->where('id',$_POST['sub_cat_id']);
    $data_arr =  $this->db->get('sub_category_master')->row();
    $sub_cat_name = $data_arr->name;
    $result = $this->dropbox->get($sub_cat_name."",$token='sub_access_token');
    if(!empty($result['contents'])){

      foreach($result['contents'] as $value){

          $image_path = explode('/',$value['path']);
          $upload_path = FCPATH.'uploads/sub_category/'.$sub_cat_name.'/';
          $image_name = $this->dropbox->download_files($value['path'],$upload_path,true);
          upload_images($path,$sub_cat_name,$image_name,$upload_path.$image_name);
      } 
      $data  = get_successMsg(); 
      $data['encrypted_id'] = $data_arr->encrypted_id;
      echo json_encode($data);         
    }
    else
    {
       echo json_encode(get_errorMsg());
    }
  }


  public function update_sub_cat_images(){
    $data = array();
  
    $data['status']= 'success';
    $data['data']= '';
    $this->Sub_category_model->update_sub_cat_images();
    echo json_encode($data);
  }

}