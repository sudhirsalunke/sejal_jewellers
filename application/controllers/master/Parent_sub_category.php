<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Parent_sub_category extends CI_Controller {

  public function __construct() {
    parent::__construct();
    if(empty($this->session->userdata('user_id'))){
      redirect(ADMIN_PATH . 'auth/logout');
    }
    $this->breadcrumbs->push("Master", "#");
    $this->breadcrumbs->push("Product Category", "product_category");
    $this->load->model(array('Parent_sub_category_model','sale_master/parent_category_model'));
    $this->load->config('admin_validationrules', TRUE);
    $this->load->library('Data_encryption');
  }

  public function index(){
    $data['page_title'] = "All Sub Category";
    $list=$this->input->post('list');
    if($list !="")
    {
      echo json_encode($this->generate_data_table());
    }else{
      $this->view->render('master/parent_sub_category/index',$data);
    }
  }

  public function create(){
    $this->breadcrumbs->push("Add Product Category", "product_category/create");
  	$data['page_title'] = "Add Product Category";
    $data['parent_category']= $this->parent_category_model->get();
    $this->view->render('master/parent_sub_category/create',$data);
  }

  public function store(){
    $data = array();
    $validationResult = $this->Parent_sub_category_model->validatepostdata();
    if($validationResult===FALSE){
      $data['status']= 'failure';
      $data['data']= '';
      $data['error'] = array(
        'name'=>strip_tags(form_error('product_category[name]')),
        'parent_category'=>strip_tags(form_error('product_category[parent_category]')),
        
      );
    }else{
      $data['status']= 'success';
      $data['data']= '';
      $data['user_id'] = $this->Parent_sub_category_model->store();
    }
    echo json_encode($data);
  }
  public function edit($id){
    $this->breadcrumbs->push("Edit Product Category", "product_category/edit");
  	$data['page_title'] = "EDIT Product Category";
    $data['parent_category']= $this->parent_category_model->get();
    $data['product_category'] = $this->Parent_sub_category_model->find_by_encrypted_id($id);
    //print_r($data);exit;
    $this->view->render('master/parent_sub_category/edit',$data);
  }
  public function update(){
  	$data = array();
    $validationResult = $this->Parent_sub_category_model->validatepostdata();
    if($validationResult===FALSE){
      $data['status']= 'failure';
      $data['data']= '';
      $data['error'] = array(
            'name'=>strip_tags(form_error('product_category[name]')),
            'parent_category'=>strip_tags(form_error('product_category[parent_category]')),
      );
    }else{
      $data['status']= 'success';
      $data['data']= '';
      $data['user_id'] = $this->Parent_sub_category_model->update();
    }
    echo json_encode($data);
  }
  public function delete(){
    $result = $this->Parent_sub_category_model->delete($_POST['id']);
    echo json_encode($result);
  }
  private function generate_data_table(){
    $filter_status =@$_REQUEST['order'][0];
    $status = array('name','description');
    $search=@$_REQUEST['search']['value'];
    $result = $this->Parent_sub_category_model->get($filter_status,$status,$_REQUEST,$search,$limit=true);
    $totalRecords = $this->Parent_sub_category_model->get($filter_status,$status,$_REQUEST,$search,$limit=false);
    if (!empty($result)) {
        foreach ($result as $key => $value) {
            $data[$key][0] =$value["name"];
            $data[$key][1] =$value["parent_category"];
            $pc_used =0;//$this->Parent_sub_category_model->check_pc_used($value['id']);
            $button_html = '';
            if($pc_used != 0){
                $onclick ="disabled";
            }else{
                $onclick ='onclick=Delete_record("'.$value['encrypted_id'].'",this,"parent_sub_category")';
            }
            
            $button_html  = '<span style="float:right"> <a href="'.ADMIN_PATH.'product_category/edit/'.$value["encrypted_id"].'"  class="btn btn-link edit_link small loader-hide btn-sm" id="confim_'.$value['encrypted_id'].'">EDIT</a> <a '.$onclick.' class="btn btn-link delete_link small loader-hide btn-sm" id="reject_'.$value['encrypted_id'].'">DELETE</a></span> ';
            
            $data[$key][2]   = $button_html; 
        }
    }else{
        $data[0][0] = ['No data found'];
        $data[0][1] = [];
        $data[0][2] = [];
    }
     $json_data = array(
          "draw" => intval($_REQUEST['draw']),
          "recordsTotal" => intval($totalRecords),
          "recordsFiltered" => intval($totalRecords),
          "data" => $data
      );
    return $json_data; 
  }


}