<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Carat extends CI_Controller {

  public function __construct() {
    parent::__construct();
    if(empty($this->session->userdata('user_id'))){
      redirect(ADMIN_PATH . 'auth/logout');
    }
    $this->breadcrumbs->push("Master", "#");
    $this->breadcrumbs->push("Carat", "Carat");
    $this->load->model('Carat_model');
    $this->load->config('admin_validationrules', TRUE);
    $this->load->library('Data_encryption');
  }
  public function index(){
    $data['page_title'] = "All Carats";
    $list=$this->input->post('list');
    if($list !="")
    {
      echo json_encode($this->generate_data_table());
    }else{
      $this->view->render('Carat/index',$data);
    }
  }

  public function create(){
    $this->breadcrumbs->push("Add Carat", "Carat/create");
  	$data['page_title'] = "ADD CARAT";
    $this->view->render('Carat/create',$data);
  }

  public function store(){
    $data = array();
    $validationResult = $this->Carat_model->validatepostdata();
    if($validationResult===FALSE){
      $data['status']= 'failure';
      $data['data']= '';
      $data['error'] = array(
        'name'=>strip_tags(form_error('Carat[name]'))
      );
    }else{
      $data['status']= 'success';
      $data['data']= '';
      $data['user_id'] = $this->Carat_model->store();
    }
    echo json_encode($data);
  }
  public function edit($id){
     $this->breadcrumbs->push("Edit Carat", "Carat/edit");
  	$data['page_title'] = "EDIT CARAT";
    $data['id'] = $this->Carat_model->get_primary_key($id);
    $data['carat'] = $this->Carat_model->find($data['id']);
    $this->view->render('Carat/edit',$data);
  }
  public function update(){
  	$data = array();
    $validationResult = $this->Carat_model->validatepostdata();
    if($validationResult===FALSE){
      $data['status']= 'failure';
      $data['data']= '';
      $data['error'] = array(
          'name'=>strip_tags(form_error('Carat[name]'))
      );
    }else{
      $data['status']= 'success';
      $data['data']= '';
      $data['user_id'] = $this->Carat_model->update();
    }
    echo json_encode($data);
  }
  public function delete(){
  	$data['id'] = $this->Carat_model->get_primary_key($_POST['id']);
    $result = $this->Carat_model->delete($data['id']);
    echo json_encode($result);
  }
  private function generate_data_table(){
    $filter_status =@$_REQUEST['order'][0];
    $status = array('name');
    $search=@$_REQUEST['search']['value'];
    $result = $this->Carat_model->get($filter_status,$status,$_REQUEST,$search,$limit=true);
    $totalRecords = $this->Carat_model->get($filter_status,$status,$_REQUEST,$search,$limit=false);
    if (!empty($result)) {
        foreach ($result as $key => $value) {
            $data[$key][0] =$value["name"];

            $carat_used = check_master_in_used('carat_id',$value['id'] ,'product');

            $button_html = '';
            if($carat_used>0)
            {
               $button_html  = '<span style="float:right"> <a href="'.ADMIN_PATH.'Carat/edit/'.$value["encrypted_id"].'"  class="btn btn-link edit_link small loader-hide  btn-sm" id="confim_'.$value['encrypted_id'].'">EDIT</a>&nbsp;&nbsp;<a disabled class="btn btn-link delete_link small loader-hide btn-sm" id="reject_'.$value['encrypted_id'].'">DELETE</a></span>';
            }
            else
            {
               $button_html  = '<span style="float:right"> <a href="'.ADMIN_PATH.'Carat/edit/'.$value["encrypted_id"].'"  class="btn btn-link edit_link small loader-hide btn-sm" id="confim_'.$value['encrypted_id'].'">EDIT</a>&nbsp;&nbsp;<a onclick=Delete_record("'.$value['encrypted_id'].'",this,"Carat") class="btn btn-link delete_link small loader-hide btn-sm" id="reject_'.$value['encrypted_id'].'">DELETE</a></span>';
            }
            $data[$key][1]   = $button_html; 

        }
    }else{
        $data[0][0] = ['No data found'];
        $data[0][1] = [];
    }
     $json_data = array(
          "draw" => intval($_REQUEST['draw']),
          "recordsTotal" => intval($totalRecords),
          "recordsFiltered" => intval($totalRecords),
          "data" => $data
      );
    return $json_data; 
  }
}