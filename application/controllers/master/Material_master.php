<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Material_master extends CI_Controller {

  public function __construct() {
    parent::__construct();
    if(empty($this->session->userdata('user_id'))){
      redirect(ADMIN_PATH . 'auth/logout');
    }
    $this->breadcrumbs->push("Master", "#");
    $this->breadcrumbs->push("Material", "Material_master");
    $this->load->model('Material_master_model');
    $this->load->config('admin_validationrules', TRUE);
    $this->load->library('Data_encryption');
  }

  private function validationResult(){
    $data=array();
    $validationResult = $this->Material_master_model->validatepostdata();
    if($validationResult===FALSE){
      $data['status']= 'failure';
      $data['data']= '';
      $data['error'] = array(
        'description'=>strip_tags(form_error('material[description]')),
        'short_code'=>strip_tags(form_error('material[short_code]'))
      );
    }
    return $data;
  }

  public function index(){
    $data['page_title'] = "All Material";
    $list=$this->input->post('list');
    if($list !="")
    {
      echo json_encode($this->generate_data_table());
    }else{
      $this->view->render('Material_master/index',$data);
    }
  }

  public function create(){
    $this->breadcrumbs->push("Add Material", "Material_master/create");
  	$data['page_title'] = "ADD Material";
    $this->view->render('Material_master/create',$data);
  }

  public function store(){
    $data = array();
    $data = $this->validationResult();
    if(count($data) == 0){
      $data = $this->Material_master_model->store();
    }
    echo json_encode($data);
  }
  public function edit($id){
    $this->breadcrumbs->push("Edit Material", "Material_master/edit");
  	$data['page_title'] = "EDIT Material";
    $data['Material_master'] = $this->Material_master_model->find_by_encrypted_id($id);
    $this->view->render('Material_master/edit',$data);
  }
  public function update(){
  	$data = array();
    $data = $this->validationResult();
    if(count($data) == 0){
      $data = $this->Material_master_model->update();
    }
    echo json_encode($data);
  }
  public function delete(){
    $result = $this->Material_master_model->delete($_POST['id']);
    echo json_encode($result);
  }
  private function generate_data_table(){
    $filter_status =@$_REQUEST['order'][0]; 
    $search=@$_REQUEST['search']['value'];
    $result = $this->Material_master_model->get($filter_status,$_REQUEST,$search,$limit=true);
    $totalRecords = $this->Material_master_model->get($filter_status,$_REQUEST,$search,$limit=false);
    if (!empty($result)) {
        foreach ($result as $key => $value) {
            $data[$key][0] =$value["description"];
            $data[$key][1] =$value["short_code"];
            $hc_used =$this->Material_master_model->check_material_used($value['id']);
            $button_html = '';
            if($hc_used != 0){
                $onclick ="disabled";
            }else{
                $onclick ='onclick=Delete_record("'.$value['encrypted_id'].'",this,"Material")';
            }
            
            $button_html  = '<span style="float:right"> <a href="'.ADMIN_PATH.'Material/edit/'.$value["encrypted_id"].'"  class="btn btn-link small loader-hide btn-sm" id="confim_'.$value['encrypted_id'].'">EDIT</a>&nbsp&nbsp<a '.$onclick.' class="btn btn-link delete_link small loader-hide btn-sm" id="reject_'.$value['encrypted_id'].'">DELETE</a></span>';
            
            $data[$key][2]   = $button_html; 
        }
    }else{
        $data[0][0] = ['No data found'];
        $data[0][1] = [];
        $data[0][2] = [];
    }
     $json_data = array(
          "draw" => intval($_REQUEST['draw']),
          "recordsTotal" => intval($totalRecords),
          "recordsFiltered" => intval($totalRecords),
          "data" => $data
      );
    return $json_data; 
  }
}