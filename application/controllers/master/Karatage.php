<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Karatage extends CI_Controller {

  public function __construct() {
    parent::__construct();
    if(empty($this->session->userdata('user_id'))){
      redirect(ADMIN_PATH . 'auth/logout');
    }
    $this->breadcrumbs->push("Master", "#");
    $this->breadcrumbs->push("Karatage", "Karatage");
    $this->load->model('karatage_model');
    $this->load->config('admin_validationrules', TRUE);
    $this->load->library('Data_encryption');
    $this->load->library('Breadcrumbs');
  }

  public function index(){
    $data['page_title'] = "ALL KARATAGES";
    $list=$this->input->post('list');
    if($list !="")
    {
      echo json_encode($this->generate_data_table());
    }else{
      $this->view->render('karatage/index',$data);
    }
  }

  public function create(){
  	$data['page_title'] = "ADD KARATAGE";
    $this->breadcrumbs->push("Add Category", "karatage/create");
    $this->view->render('karatage/create',$data);
  }

  public function store(){
    $data = array();
    $validationResult = $this->Category_model->validatepostdata();
    if($validationResult===FALSE){
      $data['status']= 'failure';
      $data['data']= '';
      $data['error'] = array(
        'name'=>strip_tags(form_error('karatage[name]')),
        'code'=>strip_tags(form_error('karatage[code]'))
      );
    }else{
      $data['status']= 'success';
      $data['data']= '';
      $data['user_id'] = $this->Category_model->store();
    }
    echo json_encode($data);
  }
  public function edit($id){
  	$data['page_title'] = "EDIT KARATAGE";
    $this->breadcrumbs->push("Edit Karatage", "karatage/edit");
    $data['id'] = $this->Category_model->get_primary_key($id);
    $data['category'] = $this->Category_model->find($data['id']);
    $this->view->render('karatage/edit',$data);
  }
  public function update(){
  	$data = array();
    $validationResult = $this->Category_model->validatepostdata();
    if($validationResult===FALSE){
      $data['status']= 'failure';
      $data['data']= '';
      $data['error'] = array(
          'name'=>strip_tags(form_error('karatage[name]')),
          'code'=>strip_tags(form_error('karatage[code]'))
      );
    }else{
      $data['status']= 'success';
      $data['data']= '';
      $data['user_id'] = $this->Category_model->update();
    }
    echo json_encode($data);
  }
  public function delete(){
  	$data['id'] = $this->Category_model->get_primary_key($_POST['id']);
    $result = $this->Category_model->delete($data['id']);
    echo json_encode($result);
  }
  private function generate_data_table(){
    $filter_status =@$_REQUEST['order'][0];
    $status = array('name','code');
    $search=@$_REQUEST['search']['value'];
    $result = $this->Category_model->get($filter_status,$status,$_REQUEST,$search,$limit=true);
    $totalRecords = $this->Category_model->get($filter_status,$status,$_REQUEST,$search,$limit=false);
    if (!empty($result)) {
        foreach ($result as $key => $value) {
          $category_used = check_master_in_used('category_id',$value['id'],'product');
            $data[$key][0] =$value["name"];
            $data[$key][1] =$value["code"];
            $button_html = '';
            if($category_used>0)
            {
               $button_html  = '<span style="float:right"> <a href="'.ADMIN_PATH.'karatage/edit/'.$value["encrypted_id"].'"  class="btn btn-link edit_link small loader-hide btn-sm" id="confim_'.$value['encrypted_id'].'">EDIT</a>&nbsp  &nbsp <a  disabled class="btn btn-link delete_link small loader-hide btn-sm" id="reject_'.$value['encrypted_id'].'">DELETE</a></span>  &nbsp';
            }
            else
            {
               $button_html  = '<span style="float:right"> <a href="'.ADMIN_PATH.'karatage/edit/'.$value["encrypted_id"].'"  class="btn btn-link edit_link small loader-hide  btn-sm" id="confim_'.$value['encrypted_id'].'">EDIT</a>&nbsp  &nbsp <a onclick=Delete_record("'.$value['encrypted_id'].'",this,"karatage") class="btn btn-link delete_link small loader-hidebtn-sm" id="reject_'.$value['encrypted_id'].'">DELETE</a></span>  &nbsp';
            }
            $data[$key][2]   = $button_html;
        }
    }else{
        $data[0][0] = ['No data found'];
        $data[0][1] = [];
        $data[0][2] = [];
    }
     $json_data = array(
          "draw" => intval($_REQUEST['draw']),
          "recordsTotal" => intval($totalRecords),
          "recordsFiltered" => intval($totalRecords),
          "data" => $data
      );
    return $json_data; 
  }
}