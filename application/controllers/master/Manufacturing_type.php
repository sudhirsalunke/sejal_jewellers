<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Manufacturing_type extends CI_Controller {

  public function __construct() {
    parent::__construct();
    if(empty($this->session->userdata('user_id'))){
      redirect(ADMIN_PATH . 'auth/logout');
    }
    $this->breadcrumbs->push("Master", "#");
    $this->breadcrumbs->push("Manufacturing Type", "Manufacturing_type");
    $this->load->model('manufacturing_type_model');
    $this->load->config('admin_validationrules', TRUE);
    $this->load->library('Data_encryption');
  }

  public function index(){
    $data['page_title'] = "All Manufacturing Type";
    $list=$this->input->post('list');
    if($list !="")
    {
      echo json_encode($this->generate_data_table());
    }else{
      $this->view->render('manufacturing_type/index',$data);
    }
  }

  public function create(){
    $this->breadcrumbs->push("Add Manufacturing Type", "manufacturing_type/create");
  	$data['page_title'] = "ADD Manufacturing Type";
    $this->view->render('manufacturing_type/create',$data);
  }
  public function store(){
    $data = array();
    $validationResult = $this->manufacturing_type_model->validatepostdata();
    if($validationResult===FALSE){
      $data['status']= 'failure';
      $data['data']= '';
      $data['error'] = array(
        'name'=>strip_tags(form_error('manufacturing_type[name]')),
      );
    }else{
      $data['status']= 'success';
      $data['data']= '';
      $data['user_id'] = $this->manufacturing_type_model->store();
    }
    echo json_encode($data);
  }
  public function edit($id){
    $this->breadcrumbs->push("Edit Manufacturing Type", "manufacturing_type/edit");
  	$data['page_title'] = "EDIT Manufacturing Type";
    $data['id'] = $this->manufacturing_type_model->get_primary_key($id);
    $data['manufacturing_type'] = $this->manufacturing_type_model->find($data['id']);
    $this->view->render('manufacturing_type/edit',$data);
  }
  public function update(){
  	$data = array();
    $validationResult = $this->manufacturing_type_model->validatepostdata();
    if($validationResult===FALSE){
      $data['status']= 'failure';
      $data['data']= '';
      $data['error'] = array(
        'name'=>strip_tags(form_error('manufacturing_type[name]')),
      );
    }else{
      $data['status']= 'success';
      $data['data']= '';
      $data['user_id'] = $this->manufacturing_type_model->update();
    }
    echo json_encode($data);
  }
  public function delete(){
  	$data['id'] = $this->manufacturing_type_model->get_primary_key($_POST['id']);
    $result = $this->manufacturing_type_model->delete($data['id']);
    echo json_encode($result);
  }
  private function generate_data_table(){
    $filter_status =@$_REQUEST['order'][0];
    $status = array('name');
    $search=@$_REQUEST['search']['value'];
    $result = $this->manufacturing_type_model->get($filter_status,$status,$_REQUEST,$search,$limit=true);
    $totalRecords = $this->manufacturing_type_model->get($filter_status,$status,$_REQUEST,$search,$limit=false);
    if (!empty($result)) {
        foreach ($result as $key => $value) {
          $mf_used = check_master_in_used('manufacturing_type_id',$value['id'],'product');
            $data[$key][0] =$value["name"];
            $button_html = '';
            if($mf_used>0)
            {
               $button_html  = '<span style="float:right"> <a href="'.ADMIN_PATH.'Manufacturing_type/edit/'.$value["encrypted_id"].'"  class="btn btn-link edit_link small loader-hide btn-sm" id="confim_'.$value['encrypted_id'].'">EDIT</a>&nbsp;&nbsp;<a disabled class="btn btn-link delete_link small loader-hide  btn-sm" id="reject_'.$value['encrypted_id'].'">DELETE</a></span>';
            }
            else
            {
               $button_html  = '<span style="float:right"> <a href="'.ADMIN_PATH.'Manufacturing_type/edit/'.$value["encrypted_id"].'"  class="btn btn-link edit_link small loader-hide btn-sm" id="confim_'.$value['encrypted_id'].'">EDIT</a>&nbsp;&nbsp;<a onclick=Delete_record("'.$value['encrypted_id'].'",this,"Manufacturing_type") class="btn btn-link delete_link small loader-hide  btn-sm" id="reject_'.$value['encrypted_id'].'">DELETE</a></span>';
            }
            $data[$key][1]   = $button_html;
            
        }
    }else{
        $data[0][0] = ['No data found'];
        $data[0][1] = [];
    }
     $json_data = array(
          "draw" => intval($_REQUEST['draw']),
          "recordsTotal" => intval($totalRecords),
          "recordsFiltered" => intval($totalRecords),
          "data" => $data
      );
    return $json_data; 
  }
}