<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Article extends CI_Controller {

  public function __construct() {
    parent::__construct();
    if(empty($this->session->userdata('user_id'))){
      redirect(ADMIN_PATH . 'auth/logout');
    }
    $this->breadcrumbs->push("Master", "#");
    $this->breadcrumbs->push("Article", "Article");
    $this->load->model('Article_model');
    $this->load->config('admin_validationrules', TRUE);
    $this->load->library('Data_encryption');
  }

  public function index(){
    $data['page_title'] = "All Articles";
    $list=$this->input->post('list');
    if($list !="")
    {
      echo json_encode($this->generate_data_table());
    }else{
      $this->view->render('Article/index',$data);
    }
  }

  public function create(){
    $this->breadcrumbs->push("Add Article", "Article/create");
  	$data['page_title'] = "ADD ARTICLE";
    $this->view->render('Article/create',$data);
  }

  public function store(){
    $data = array();
    $validationResult = $this->Article_model->validatepostdata();
    if($validationResult===FALSE){
      $data['status']= 'failure';
      $data['data']= '';
      $data['error'] = array(
        'name'=>strip_tags(form_error('Article[name]')),
        'description'=>strip_tags(form_error('Article[description]'))
      );
    }else{
      $data['status']= 'success';
      $data['data']= '';
      $data['user_id'] = $this->Article_model->store();
    }
    echo json_encode($data);
  }
  public function edit($id){
    $this->breadcrumbs->push("Edit Article", "Article/edit");
  	$data['page_title'] = "EDIT ARTICLE";
    $data['id'] = $this->Article_model->get_primary_key($id);
    $data['article'] = $this->Article_model->find($data['id']);
    $this->view->render('Article/edit',$data);
  }
  public function update(){
  	$data = array();
    $validationResult = $this->Article_model->validatepostdata();
    if($validationResult===FALSE){
      $data['status']= 'failure';
      $data['data']= '';
      $data['error'] = array(
          'name'=>strip_tags(form_error('Article[name]')),
          'description'=>strip_tags(form_error('Article[code]'))
      );
    }else{
      $data['status']= 'success';
      $data['data']= '';
      $data['user_id'] = $this->Article_model->update();
    }
    echo json_encode($data);
  }
  public function delete(){
  	$data['id'] = $this->Article_model->get_primary_key($_POST['id']);
    $result = $this->Article_model->delete($data['id']);
    echo json_encode($result);
  }
  private function generate_data_table(){
    $filter_status =@$_REQUEST['order'][0];
    $status = array('name','description');
    $search=@$_REQUEST['search']['value'];
    $result = $this->Article_model->get($filter_status,$status,$_REQUEST,$search,$limit=true);
    $totalRecords = $this->Article_model->get($filter_status,$status,$_REQUEST,$search,$limit=false);
    if (!empty($result)) {
        foreach ($result as $key => $value) {
            $data[$key][0] =$value["name"];
            $data[$key][1] =$value["description"];
            $article_used = check_master_in_used('article_id',$value['id'],'product');
            $button_html = '';
            if($article_used>0)
            {
               $button_html  = '<span style="float:right"> <a href="'.ADMIN_PATH.'Article/edit/'.$value["encrypted_id"].'"  class="btn btn-link edit_link small loader-hide btn-sm" id="confim_'.$value['encrypted_id'].'">EDIT</a>&nbsp;&nbsp;<a disabled class="btn btn-link delete_link small loader-hide btn-sm" id="reject_'.$value['encrypted_id'].'">DELETE</a></span>';
            }
            else
            {
               $button_html  = '<span style="float:right"> <a href="'.ADMIN_PATH.'Article/edit/'.$value["encrypted_id"].'"  class="btn btn-link edit_link  small loader-hide  btn-sm" id="confim_'.$value['encrypted_id'].'">EDIT</a>&nbsp;&nbsp;<a onclick=Delete_record("'.$value['encrypted_id'].'",this,"Article") class="btn btn-link delete_link small loader-hide  btn-sm" id="reject_'.$value['encrypted_id'].'">DELETE</a></span>';
            }
            $data[$key][2]   = $button_html; 
        }
    }else{
        $data[0][0] = ['No data found'];
        $data[0][1] = [];
        $data[0][2] = [];
    }
     $json_data = array(
          "draw" => intval($_REQUEST['draw']),
          "recordsTotal" => intval($totalRecords),
          "recordsFiltered" => intval($totalRecords),
          "data" => $data
      );
    return $json_data; 
  }
}