<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Department extends CI_Controller {

  public function __construct() {
    parent::__construct();
    if(empty($this->session->userdata('user_id'))){
      redirect(ADMIN_PATH . 'auth/logout');
    }
    $this->breadcrumbs->push("Master", "#");
    $this->breadcrumbs->push("Department", "Department");
    $this->load->model('Manufacturing_module/Department_model');
    $this->load->config('admin_validationrules', TRUE);
    $this->load->library('Data_encryption');
  }

  public function index(){
    $data['page_title'] = "All Department";
    $list=$this->input->post('list');
    if($list !="")
    {
      echo json_encode($this->generate_data_table());
    }else{
      $this->view->render('Department/index',$data);
    }
  }

  public function create(){
    $this->breadcrumbs->push("Add Department", "Department/create");
  	$data['page_title'] = "ADD Department";
    $this->view->render('Department/create',$data);
  }

  public function store(){
    $data = array();
    $validationResult = $this->Department_model->validatepostdata();
    if($validationResult===FALSE){
      $data['status']= 'failure';
      $data['data']= '';
      $data['error'] = array(
        'name'=>strip_tags(form_error('Department[name]'))
      );
    }else{
      $data['status']= 'success';
      $data['data']= '';
      $data['user_id'] = $this->Department_model->store();
    }
    echo json_encode($data);
  }
  public function edit($id){
    $this->breadcrumbs->push("Edit Department", "Department/edit");
  	$data['page_title'] = "Edit Department";
    $data['id'] = $this->Department_model->get_primary_key($id);
    $data['Department'] = $this->Department_model->find($data['id']);
    $this->view->render('Department/edit',$data);
  }
  public function update(){
  	$data = array();
    $validationResult = $this->Department_model->validatepostdata();
    if($validationResult===FALSE){
      $data['status']= 'failure';
      $data['data']= '';
      $data['error'] = array(
          'name'=>strip_tags(form_error('Department[name]'))
      );
    }else{
      $data['status']= 'success';
      $data['data']= '';
      $data['user_id'] = $this->Department_model->update();
    }
    echo json_encode($data);
  }
  public function delete(){
  	$data['id'] = $this->Department_model->get_primary_key($_POST['id']);
    $result = $this->Department_model->delete($data['id']);
    echo json_encode($result);
  }
  private function generate_data_table(){
    $filter_status =@$_REQUEST['order'][0];
    $status = array('name','description');
    $search=@$_REQUEST['search']['value'];
    $result = $this->Department_model->get($filter_status,$status,$_REQUEST,$search,$limit=true);
    $totalRecords = $this->Department_model->get($filter_status,$status,$_REQUEST,$search,$limit=false);
    if (!empty($result)) {
        foreach ($result as $key => $value) {
          $department_used = check_master_in_used('department_id',$value['id'],'category_master');
            $data[$key][0] =$value["name"];
            $button_html  = '<span style="float:right">';
            
              // $button_html  .='<a href="'.ADMIN_PATH.'Manufacturing_department_order/create/'.$value["id"].'"  class="btn btn-purp small loader-hide  btn-sm" id="confim_'.$value['encrypted_id'].'">Create New Order</a>&nbsp  &nbsp';
             /* $button_html  .='<a href="'.ADMIN_PATH.'Department/edit/'.$value["encrypted_id"].'"  class="btn btn-info small loader-hide  btn-sm" id="confim_'.$value['encrypted_id'].'">EDIT</a>&nbsp  &nbsp ';

              $button_html  .='<a disabled class="btn btn-danger small loader-hide  btn-sm" id="reject_'.$value['encrypted_id'].'">DELETE</a>  &nbsp &nbsp ';*/

              // $button_html  .='<a href="'.ADMIN_PATH.'Ready_product/index/'.$value["id"].'"  class="btn btn-info small loader-hide  btn-sm">Ready Product</a>'
            $button_html  .='</span>';

              if($department_used>0)
            {
               
                $button_html  .='<span class="pull-right"><a href="'.ADMIN_PATH.'Department/edit/'.$value["encrypted_id"].'"  class="btn btn-link edit_link  loader-hide btn-sm" id="confim_'.$value['encrypted_id'].'">EDIT</a>&nbsp;&nbsp;<a disabled class="btn btn-link delete_link loader-hide btn-sm" id="reject_'.$value['encrypted_id'].'">DELETE</a></span>';

            }
            else
            {

                  $button_html  .='<span class="pull-right"> <a href="'.ADMIN_PATH.'Department/edit/'.$value["encrypted_id"].'"  class="btn btn-link edit_link  loader-hide btn-sm" id="confim_'.$value['encrypted_id'].'">EDIT</a>&nbsp;&nbsp;<a onclick=Delete_record("'.$value['encrypted_id'].'",this,"Department")  class="btn btn-link delete_link  loader-hide btn-sm" id="reject_'.$value['encrypted_id'].'">DELETE</a></span> ';

            }
          
            $data[$key][1]   = $button_html; 
        }
    }else{
        $data[0][0] = ['No data found'];
        $data[0][1] = [];
    }
     $json_data = array(
          "draw" => intval($_REQUEST['draw']),
          "recordsTotal" => intval($totalRecords),
          "recordsFiltered" => intval($totalRecords),
          "data" => $data
      );
    return $json_data; 
  }
}