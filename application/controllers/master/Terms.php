<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Terms extends CI_Controller {

  public function __construct() {
    parent::__construct();
    if(empty($this->session->userdata('user_id'))){
      redirect(ADMIN_PATH . 'auth/logout');
    }
    $this->breadcrumbs->push("Master", "#");
    $this->breadcrumbs->push("Terms", "Terms");
    $this->load->model(array('Terms_model','sale_master/Parent_category_model'));
    $this->load->config('admin_validationrules', TRUE);
    $this->load->library('Data_encryption');
  }

  public function index(){
    $data['page_title'] = "All Terms";
    $list=$this->input->post('list');
    if($list !="")
    {
      echo json_encode($this->generate_data_table());
    }else{
      $this->view->render('terms/index',$data);
    }
  }

  private function validationResult(){
    $data = array();
    $validationResult = $this->Terms_model->validatepostdata();
    if($validationResult===FALSE){
      $data['status']= 'failure';
      $data['data']= '';
      $data['error'] = array(
        'parent_category_id'=>strip_tags(form_error('terms[parent_category_id]')),
        'code'=>strip_tags(form_error('terms[code]')),
        'hard_cash'=>strip_tags(form_error('terms[hard_cash]')),
        'normal'=>strip_tags(form_error('terms[normal]')),
        '7days'=>strip_tags(form_error('terms[7days]')),
        '15days'=>strip_tags(form_error('terms[15days]')),
        '1month'=>strip_tags(form_error('terms[1month]')),
        '2month'=>strip_tags(form_error('terms[2month]')),
      );
    }

    return $data;
  }

  public function create(){
    $this->breadcrumbs->push("Add Term", "Term/create");
  	$data['page_title'] = "Add Term";
    $data['parent_Category']=$this->Parent_category_model->get('','','','',true);
    $this->view->render('terms/create',$data);
  }

  public function store(){
    $data = $this->validationResult();
    if(count($data)== 0){
      $data = $this->Terms_model->store();
    }
    echo json_encode($data);
  }
  public function edit($id){
    $this->breadcrumbs->push("Edit Term", "Term/edit");
  	$data['page_title'] = "EDIT Term";
    $data['terms'] = $this->Terms_model->find_by_encrypted_id($id);
    $data['parent_Category']=$this->Parent_category_model->get('','','','',true);
    $this->view->render('terms/edit',$data);
  }
  public function update(){
  	  $data = $this->validationResult();
    if(count($data)== 0){
      $data = $this->Terms_model->update();
    }
    echo json_encode($data);
  }
  public function delete(){
    $result = $this->Terms_model->delete($_POST['id']);
    echo json_encode($result);
  }
  private function generate_data_table(){
    $filter_status =@$_REQUEST['order'][0];
    $status = array('name');
    $search=@$_REQUEST['search']['value'];
    $result = $this->Terms_model->get($filter_status,$status,$_REQUEST,$search,$limit=true);
    $totalRecords = $this->Terms_model->get($filter_status,$status,$_REQUEST,$search,$limit=false);
    $start =$_REQUEST['start'];
    if (!empty($result)) {
        foreach ($result as $key => $value) {
          $start++;
          $terms_used=0;
            if($terms_used != 0){
                $onclick ="disabled";
            }else{
                $onclick ='onclick=Delete_record("'.$value['encrypted_id'].'",this,"Term")';
            }
            
            $button_html  = '<span style="float:right"> <a href="'.ADMIN_PATH.'Term/edit/'.$value["encrypted_id"].'"  class="btn btn-link edit_link small loader-hide btn-sm" id="confim_'.$value['encrypted_id'].'">EDIT</a>&nbsp&nbsp<a '.$onclick.' class="btn btn-link delete_link small loader-hide btn-sm" id="reject_'.$value['encrypted_id'].'">DELETE</a></span>';

            $data[$key][0] ='<span style="float:right">'.$start.'</span>';
            $data[$key][1] =$value['parent_category']; 
            $data[$key][2] =$value["code"];
            $data[$key][3] ='<span style="float:right">'. $value["hard_cash"].'</span>';
            $data[$key][4] ='<span style="float:right">'. $value["normal"].'</span>';
            $data[$key][5] ='<span style="float:right">'. $value["7days"].'</span>';
            $data[$key][6] ='<span style="float:right">'. $value["15days"].'</span>';
            $data[$key][7] ='<span style="float:right">'. $value["1month"].'</span>';
            $data[$key][8] ='<span style="float:right">'. $value["2month"].'</span>';
            $data[$key][9] =$button_html;
        }
    }else{
      for ($i=0; $i <= 9 ; $i++) { 
      $data[0][$i]="";
         if ($i==1) {
          $data[0][$i] = ['No data found'];
         }
      }
    }
     $json_data = array(
          "draw" => intval($_REQUEST['draw']),
          "recordsTotal" => intval($totalRecords),
          "recordsFiltered" => intval($totalRecords),
          "data" => $data
      );
    return $json_data; 
  }
}