<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Party_master extends CI_Controller {

  public function __construct() {
    parent::__construct();
    if(empty($this->session->userdata('user_id'))){
      redirect(ADMIN_PATH . 'auth/logout');
    }
    $this->breadcrumbs->push("Master", "#");
    $this->breadcrumbs->push("Party", "Party_master");
    $this->load->model(array('Karigar_model',
                      'Party_master_model',
                      'hallmarking_center_model',
                      'common_model/state_model',
                      'common_model/city_model',
                      'sale_master/customer_type_model',
                      'sale_master/Salesman_master_model'
                        ));
    $this->load->config('admin_validationrules', TRUE);
    $this->load->library('Data_encryption');
  }

  public function index(){
    $data['page_title'] = "All Parties";
    $list=$this->input->post('list');
    if($list !="")
    {
      echo json_encode($this->generate_data_table());
    }else{
      $this->view->render('party_master/index',$data);
    }
  }

  public function create(){
     $this->breadcrumbs->push("Add Party", "Party_master/create");
  	$data['page_title'] = "ADD Party_master";
    $data['customer_type']=$this->customer_type_model->get();
    $data['hallmarking_center']=$this->hallmarking_center_model->get();
    $data['city']=array();
    $data['state']=$this->state_model->get();
    $data['sales_under']=$this->Salesman_master_model->get();
    $this->view->render('party_master/create',$data);
  }

  private function validationResult(){
    $data = array();
    $validationResult = $this->Party_master_model->validatepostdata();
    if($validationResult===FALSE){
      $data['status']= 'failure';
      $data['data']= '';
      $data['error'] = array(
        'name'=>strip_tags(form_error('Karigar[name]')),
        //'code'=>strip_tags(form_error('Karigar[code]')),
        // 'address'=>strip_tags(form_error('Karigar[address]')),
        // 'contact_person'=>strip_tags(form_error('Karigar[contact_person]')),
        'company_name'=>strip_tags(form_error('Karigar[company_name]')),
        'email'=>strip_tags(form_error('Karigar[email]')),

        'phone_no'=>strip_tags(form_error('Karigar[phone_no]')),
        'mobile_no'=>strip_tags(form_error('Karigar[mobile_no]')),
        'customer_type_id'=>strip_tags(form_error('Karigar[customer_type_id]')),
        // 'area'=>strip_tags(form_error('Karigar[area]')),
        // 'state_id'=>strip_tags(form_error('Karigar[state_id]')),
        // 'city_id'=>strip_tags(form_error('Karigar[city_id]')),
        // 'limit_amt'=>strip_tags(form_error('Karigar[limit_amt]')),
        // 'limit_metal'=>strip_tags(form_error('Karigar[limit_metal]')),
        // 'days'=>strip_tags(form_error('Karigar[days]')),
        // 'sales_under'=>strip_tags(form_error('Karigar[sales_under]')),
        // 'terms'=>strip_tags(form_error('Karigar[terms]')),
        // 'hc_id'=>strip_tags(form_error('Karigar[hc_id]')),
        // 'refer_by'=>strip_tags(form_error('Karigar[refer_by]')),
        
        // 'bank_ac'=>strip_tags(form_error('Karigar[bank_ac]')),
        // 'bank_ifsc'=>strip_tags(form_error('Karigar[bank_ifsc]')), 
        // 'bank_name'=>strip_tags(form_error('Karigar[bank_name]')), 
        // 'bank_branch'=>strip_tags(form_error('Karigar[bank_branch]')), 
      );
    }
    return $data;
  }

  public function store(){
    $data = $this->validationResult();
    if(count($data) == 0){
      $data['status']= 'success';
      $data['data']= '';
      $data['user_id'] = $this->Party_master_model->store();
    }
    echo json_encode($data);
  }
  public function edit($party_type,$id){
     $this->breadcrumbs->push("Edit Party Master", "Party_master/edit");
  	$data['page_title'] = "Edit Party Master";
    $data['karigar'] = $this->Party_master_model->find($party_type,$id);
    $data['customer_type']=$this->customer_type_model->get();
    $data['hallmarking_center']=$this->hallmarking_center_model->get();
    $data['city']=array();
    $data['state']=$this->state_model->get();
    $data['sales_under']=$this->Salesman_master_model->get();
    $this->view->render('party_master/edit',$data);
  }
  public function update(){
     $data = $this->validationResult();
    if(count($data) == 0){
      $data['status']= 'success';
      $data['data']= '';
      $data['user_id'] = $this->Party_master_model->update();
    }
    echo json_encode($data);
  	
  }
  public function delete(){
    
  	$data['id'] = $this->Party_master_model->get_primary_key($_POST['id'],$_POST['type']);
    $result = $this->Party_master_model->delete($data['id'],$_POST['type']);
    echo json_encode($result);
  }
  private function generate_data_table(){
    $filter_status =@$_REQUEST['order'][0];
    $search=@$_REQUEST['search']['value'];
   // print_r($_REQUEST);
    $result = $this->Party_master_model->get($filter_status,$_REQUEST,$search,$limit=true);
    $totalRecords = $this->Party_master_model->get($filter_status,$_REQUEST,$search,$limit=false);
   // print_r($result);die;
    if (!empty($result)) {
        foreach ($result as $key => $value) {
          if ($value['type']=="Karigar") {
            $karigar_used = check_master_in_used('karigar_id',$value['id'],'product');
            $href_type="partyk";
          }else{
            $karigar_used=0;
            $href_type="party";
          }
     
           
            if($karigar_used>0){
               $onclick  ='disabled';
            }else{
              //$onclick='disabled';
              $onclick='onclick=Delete_party_record("'.$value['encrypted_id'].'",this,"Party_master","'.$href_type.'")';
              
            }
             $button_html  = '<div class="pull-right"> <a href="'.ADMIN_PATH.'Party_master/edit/'.$href_type.'/'.$value["encrypted_id"].'"  class="btn btn-link small loader-hide btn-sm edit_link" id="confim_'.$value['encrypted_id'].'">EDIT</a>&nbsp;&nbsp;<a '.$onclick.' class="btn btn-link  delete_link small loader-hide btn-sm" id="reject_'.$value['encrypted_id'].'">DELETE</a></div> ';


            $data[$key][] =$value["type"]; /*type*/
            $data[$key][] =blank_value($value["name"]);/*name*/
            $data[$key][] =blank_value($value["code"]);/*name*/
            $data[$key][] ='<span style="float:right">'.blank_value($value["mobile_no"]).'</span>';/*mob_no*/
        
            $data[$key][]   = $button_html;
            
        }
    }else{
        $data[0][0] = ['No data found'];
        $data[0][1] = [];
        $data[0][2] = [];
        $data[0][3] = [];
        $data[0][4] = [];
      
    }
     $json_data = array(
          "draw" => intval($_REQUEST['draw']),
          "recordsTotal" => intval($totalRecords),
          "recordsFiltered" => intval($totalRecords),
          "data" => $data
      );
    return $json_data; 
  }
  public function get_city(){
       $result=get_successMsg();
      $result['data']=$this->city_model->get();
    echo json_encode($result);
  }
  public function get(){
    $data = array();
    $result = $this->Party_master_model->get('',$_GET['term'],'',true,$_GET['type'],$auto_complete=$_GET['term']);
   // print_r($result);die;
  //  ($filter_status='',$params='',$search='',$limit='',$type='')
    if(!empty($result)){
      // echo '<pre>';
      // print_r($result);
      // echo '</pre>';
      foreach ($result as $key => $value) {
        $data[$key]['label'] = $value['name'];
        $data[$key]['value'] = $value['name'];
        $data[$key]['id'] = $value['id'];
        $data[$key]['company_name'] = $value['company_name'];
        $data[$key]['mobile_no'] = $value['mobile_no'];
        $data[$key]['email'] = $value['email'];
        $data[$key]['code'] = $value['code'];
      }
    }
    echo json_encode($data);
  }
    public function get_party_type(){
      $data = $this->Party_master_model->get_party_list();
     echo json_encode($data);
  }
}