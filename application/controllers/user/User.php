<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class User extends CI_Controller {

  public function __construct() {
      parent::__construct();
      if(empty($this->session->userdata('user_id'))){
        redirect(ADMIN_PATH . 'auth/logout');
      }
      $this->breadcrumbs->push("Users", "User");
      $this->load->model(array('User_access_model','Manufacturing_module/Department_model'));
      $this->load->library('data_encryption');
      $this->load->config('admin_validationrules', TRUE);

  }
  public function index(){
  	$data['page_title'] = "All Users";
    $list=$this->input->post('list');
    if($list !="")
    {
      echo json_encode($this->generate_data_table());
    }else{
      $this->view->render('User_access/index',$data);
    }
  }
  public function create(){
  $this->breadcrumbs->push("Add User", "User/create");
	$data['page_title'] = "ADD Users";
  $data['master'] = $this->User_access_model->get_master();
	$data['location'] = $this->User_access_model->get_location();
  $data['departments'] = $this->Department_model->get('','','','',true);
  $this->view->render('User_access/create',$data);
  }

  public function store(){
    $data = $this->User_access_model->validatepostdata();
    if($data['status'] == 'success'){
      $data = $this->User_access_model->store($_POST['User_access']);
    }
    echo json_encode($data);
  }
  public function edit($id){
    $this->breadcrumbs->push("Edit User", "User/edit");
  	$data['page_title'] = "EDIT User";
    $data['id'] = $this->User_access_model->get_primary_key($id);
    $department_map = $this->User_access_model->get_department_map($data['id']);
    $selwt_ids = '';
    if(!empty($department_map)){
      foreach ($department_map as $key => $value) {       
        $selwt_ids[] = $value;
        
      }
    $data['department_map'] = $selwt_ids;
    }
    //print_r($data['department_map']);die;
    $data['user'] = $this->User_access_model->get($data['id']);
    $data['location'] = $this->User_access_model->get_location();
    $data['master'] = $this->User_access_model->get_master();
    $data['departments'] = $this->Department_model->get('','','','',true);
    $this->view->render('User_access/edit',$data);
  }
  public function update(){
  	$data = $this->User_access_model->validatepostdata(true);
     if($data['status'] == 'success'){
      $data = $this->User_access_model->update($_POST['User_access']);
    }
    echo json_encode($data);
  }
  public function delete(){
  	$data['id'] = $this->User_access_model->get_primary_key($_POST['id']);
    $result = $this->User_access_model->delete($data['id']);
    echo json_encode($result);
  }
  private function generate_data_table(){
    $filter_status =@$_REQUEST['order'][0];
    $status = array('name');
    $search=@$_REQUEST['search']['value'];
    $result = $this->User_access_model->get('',$filter_status,$status,$_REQUEST,$search,$limit=true);
    $totalRecords = sizeof($this->User_access_model->get('',$filter_status,$status,$_REQUEST,$search,$limit=true));
    if (!empty($result)) {
      $data = $this->get_datatable_rows($result);
    }else{
      $data = $this->get_null_row();
    }
    return $this->return_json($totalRecords,$data);
  }
  private function get_datatable_rows($result){
    foreach ($result as $key => $value) {
      $data[$key][0] =$value['name'];
      $data[$key][1] =$value['email'];
      $data[$key][2] =$value['master_name'];
/*      $button_html = '<div class="pull-right"> <a href="'.ADMIN_PATH.'User/edit/'.$value["encrypted_id"].'"  class="btn btn-info small loader-hide btn-sm" id="confim_'.$value['encrypted_id'].'">EDIT</a>&nbsp;<a onclick=Delete_record("'.$value['encrypted_id'].'",this,"User") class="btn btn-danger small loader-hide btn-sm" id="reject_'.$value['encrypted_id'].'">DELETE</a></div>';
*/
      $button_html = '<div class="pull-right"> <a href="'.ADMIN_PATH.'User/edit/'.$value["encrypted_id"].'"  class="btn btn-link small loader-hide btn-sm edit_link" id="confim_'.$value['encrypted_id'].'">EDIT</a></div>';   



      $data[$key][3] =$button_html;
     
    }
    return $data;
  }
  private function get_null_row(){
    $data[0][0] = ['No data found'];
        $data[0][1] = [];
        //$data[0][2] = [];
        $data[0][2] = [];
        $data[0][3] = [];

        return $data;
  }
  private function return_json($totalRecords,$data){
    $json_data = array(
          "draw" => intval($_REQUEST['draw']),
          "recordsTotal" => intval($totalRecords),
          "recordsFiltered" => intval($totalRecords),
          "data" => $data
      );
    return $json_data;
  }
}