<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Cron_jobs extends CI_Controller {
  public function __construct() {
      parent::__construct();
      $this->load->config('Dropbox', TRUE);
      $this->load->model(array('Product_model','Cron_model','Cron_model','sale_master/Parent_category_model','order_module/Customer_order_model','corporate_module/Prepare_order_model'));
      //ini_set('max_execution_time', -1);
      $this->load->library(array('Email_lib'));
      $this->load->library(array('excel_lib'));
    }

    public function get_images_from_dropbox(){
      $path = 'Products';
      $status = $this->Cron_model->get_dropbox_status();
      if($status == false){
        return false;
      }
      // ECHO 'true';die;
      $_POST['page_title'] = 'DESIGNS WITHOUT IMAGES';
      $data = $this->Product_model->get('','',array('length'=>'600','start'=>'0'),'',$limit=true);
      $this->load->library(array('Dropbox'));
      if(!empty($data)){
        foreach ($data as $key => $value) {
          $result = $this->dropbox->getMetadataExists(trim($value['product_code']).'.jpg');
          if($result == true){
            $product_code = $value['product_code'];
            $upload_path = FCPATH.'uploads/product/'.$value['product_code'].'/';
            if(!is_dir($upload_path))
              mkdir($upload_path);
            $image_name = $this->dropbox->download_files('/'.$path.'/'.trim($value['product_code']).'.jpg',$upload_path,true);
            upload_images("product",$value['product_code'],$image_name,$upload_path.$image_name);
            $image_name = RenameUploadFile($image_name);
            $update_image =  $this->Product_model->update_images($image_name,$value['product_code']);
            
          }
        }
      }
      if(count($data)<60){
        $this->Product_model->fetch_images_from_dropbox('0');
      }
    }
    public function get_parent_category_images(){
      $path = 'parent category';
      $status = $this->Cron_model->get_dropbox_status($id=2,$type='2');
      if($status == false){
        return false;
      }
      // ECHO 'true';die;
      $_POST['page_title'] = 'DESIGNS WITHOUT IMAGES';
      $data = $this->Parent_category_model->get('','',array('length'=>'60','start'=>'0'),'',$limit=true);
      $this->load->library(array('Dropbox'));
      if(!empty($data)){
        foreach ($data as $key => $value) {
          $result = $this->dropbox->get($path.'/'.$value['name'],'parent_access_token');
          foreach ($result['contents'] as $i_key => $i_value) {
            $img_result = $this->dropbox->get($i_value['path'],'parent_access_token');
            if(!empty($img_result)){
              $image_path = explode('/',$img_result['path']);
              $product_code = strstr(end($image_path), '.', true);
              makeDirectory('parent_category');
              makeDirectory($image_path[2]);
              $upload_path = FCPATH.'uploads/parent_category/'.$image_path[2].'/';
              $image_name = $this->dropbox->download_files($img_result['path'],$upload_path,true);
              upload_images("parent_category",$image_path[2],$image_name,$upload_path.$image_name);
              $image_name = RenameUploadFile($image_name);
              $update_image =  $this->Parent_category_model->update_images($image_name,$value['id']);
              
            }
          }
        }
      }
      if(count($data)<60){
        $this->Product_model->fetch_images_from_dropbox('0');
      }
    }

  
  public function order_email(){
    $this->Customer_order_model->create_order_mail();
    $this->Customer_order_model->customer_delivery_mail();
    //$this->Customer_order_model->karigar_received_mail();
  }

  public function due_date_reminder(){
      $this->Prepare_order_model->due_date_reminder_mail();
   }
} 
