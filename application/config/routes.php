<?php
defined('BASEPATH') OR exit('No direct script access allowed');
$route['default_controller'] = 'auth';
$route['404_override'] = '';
$route['translate_uri_dashes'] = FALSE;
/* 
| -------------------------------------------------------------------------
| Product catalog module routes
| -------------------------------------------------------------------------

*/

$route['Dashboard'] = 'product_catalog/Dashboard';
$route['Approved_Design/index'] = 'product_catalog/Approved_products/index';
$route['Approved_Design/index/(:any)'] = 'product_catalog/Approved_products/index/$1';
$route['Approved_Design/show/(:any)'] = 'product_catalog/Approved_products/show/$1';
$route['Approved_Design/export_products/(:any)'] = 'product_catalog/Approved_products/export_products/$1';
$route['Approved_Design/export_images/(:any)'] = 'product_catalog/Approved_products/export_images/$1';
 
$route['Services/user_login'] = 'v1/Services/user_login';
$route['Services/register_user'] = 'v1/Services/register_user';
$route['Services/create_order'] = 'v1/Services/create_order';
$route['Services/get_sales_data'] = 'v1/Services/get_sales_data';
// $route['Services/get_sales_data/(:any)'] = 'v1/Services/get_sales_data/$1';
$route['Services/forgot_password'] = 'v1/Services/forgot_password';
$route['Services/change_password'] = 'v1/Services/change_password';
$route['Services/changePassword'] = 'v1/Services/changePassword';
$route['Services/update_user'] = 'v1/Services/update_user';
$route['Services/user_orders'] = 'v1/Services/user_orders';
$route['Services/get_order'] = 'v1/Services/get_order';
$route['Services/update_order'] = 'v1/Services/update_order';
$route['Services/change_status'] = 'v1/Services/change_status';
$route['Services/get_parent_category'] = 'v1/Services/get_parent_category';

$route['finalize_and_send'] = 'product_catalog/finalize_and_send';
$route['Finalize_and_send/update'] = 'product_catalog/finalize_and_send/update';
$route['Finalize_and_send/update'] = 'product_catalog/finalize_and_send/update';
$route['Finalize_and_send/export_shortlisted_products/(:any)'] = 'product_catalog/Finalize_and_send/export_shortlisted_products/$1';
$route['Finalize_and_send/export_shortlisted_images/(:any)'] = 'product_catalog/Finalize_and_send/export_shortlisted_images/$1';
$route['Approved_Design/(:any)'] = 'product_catalog/Approved_products/index/$1';
$route['Send_for_approval'] = 'product_catalog/Send_for_approval/index';
$route['Send_for_approval/(:any)'] = 'product_catalog/Send_for_approval/index/$1';
$route['send_for_approval/store'] = 'product_catalog/send_for_approval/store';
$route['send_for_approval/delete'] = 'product_catalog/send_for_approval/delete';
$route['send_for_approval/shortlisted_all_products'] = 'product_catalog/send_for_approval/shortlisted_all_products';
$route['Receive_pieces/pieces_to_approved'] = 'product_catalog/Receive_pieces/pieces_to_approved';
$route['Receive_pieces/pieces_to_rejected'] = 'product_catalog/Receive_pieces/pieces_to_rejected';
$route['Receive_pieces/approve'] = 'product_catalog/Receive_pieces/approve';
$route['Receive_pieces/reject'] = 'product_catalog/Receive_pieces/reject';
$route['Receive_pieces/Upload_excel/(:any)'] = 'product_catalog/Receive_pieces/Upload_excel/$1';
$route['Receive_pieces/import'] = 'product_catalog/Receive_pieces/import';
$route['Receive_pieces/index'] = 'product_catalog/Receive_pieces/index';
$route['Receive_pieces/export_products'] = 'product_catalog/Receive_pieces/export_products';
$route['Receive_pieces/(:any)'] = 'product_catalog/Receive_pieces/index/$1';
$route['Receive_pieces/index/(:any)'] = 'product_catalog/Receive_pieces/index/$1';
$route['Receive_pieces/show/(:any)'] = 'product_catalog/Receive_pieces/show/$1';
$route['Receive_pieces/export_products/(:any)'] = 'product_catalog/Receive_pieces/export_products/$1';


$route['Rejected_Design/index'] = 'product_catalog/Rejected_products/index';
$route['Rejected_Design/index/(:any)'] = 'product_catalog/Rejected_products/index/$1';
$route['Rejected_Design/(:any)'] = 'product_catalog/Rejected_products/index/$1';
$route['Rejected_Design/export_products/(:any)'] = 'product_catalog/Rejected_products/export_products/$1';
$route['Rejected_Design/show/(:any)'] = 'product_catalog/Rejected_products/show/$1';
$route['Rejected_Design/export_images/(:any)'] = 'product_catalog/Rejected_products/export_images/$1';
$route['Rejected_Design/all_export_products/(:any)'] = 'product_catalog/Rejected_products/all_export_products/$1';
$route['Rejected_Design/all_export_images/(:any)'] = 'product_catalog/Rejected_products/all_export_images/$1';

$route['sub_category/index/getCategoryWiseData'] = 'master/sub_category/index/getCategoryWiseData';
$route['Upload_design_excel'] = 'product_catalog/Upload_product_excel';
$route['Dashboard/Product_with_images'] = 'product_catalog/Product_with_images';
$route['Dashboard/Product_without_image'] = 'product_catalog/Product_without_image';
$route['Dashboard/Send_for_approval'] = 'product_catalog/Send_for_approval';
$route['Dashboard/finalize_and_send'] = 'product_catalog/finalize_and_send';
$route['Dashboard/Approved_products/(:any)'] = 'product_catalog/Approved_products/index/$1';
$route['Dashboard/Receive_pieces/(:any)'] = 'product_catalog/Receive_pieces/index/$1';
$route['Dashboard/Rejected_products/(:any)'] = 'product_catalog/Rejected_products/index/$1';
$route['Design/create'] = 'product_catalog/Product/create';
$route['Design/edit/(:any)'] = 'product_catalog/Product/edit/$1';
$route['Design/store'] = 'product_catalog/Product/store';
$route['Design/update'] = 'product_catalog/Product/update';
$route['Design/import'] = 'product_catalog/Product/import';
$route['Design/delete'] = 'product_catalog/Product/delete';
$route['Design/fetch_images_from_dropbox'] = 'product_catalog/Product/fetch_images_from_dropbox';

$route['Design'] = 'product_catalog/Product/index';
$route['Design/index'] = 'product_catalog/Product/index/Product_without_images';
$route['Design/Design_with_images'] = 'product_catalog/Product/index/Product_with_images';
$route['Design/Design_without_images'] = 'product_catalog/Product/index/Product_without_images';
$route['Design/export_products_with_image/(:any)'] = 'product_catalog/Product/export_products_with_image/$1';
$route['Design/export_products_with_image'] = 'product_catalog/Product/export_products_with_image';
$route['Design/export_products_without_image/(:any)'] = 'product_catalog/Product/export_products_without_image/$1';
$route['Design/export_products_without_image'] = 'product_catalog/Product/export_products_without_image';
$route['Design/export_product_template'] = 'product_catalog/Product/export_product_template';
$route['Design/get_wt_search'] = 'product_catalog/Product/get_wt_search';





/* 

| -------------------------------------------------------------------------
| End
| -------------------------------------------------------------------------

*/

/* 

| -------------------------------------------------------------------------
| Corporate module routes
| -------------------------------------------------------------------------

*/
$route['Corporate_Process'] = 'corporate_module/Corporate_Process';
$route['Corporate_Dashboard/reminder/(:any)'] = 'corporate_module/Corporate_Dashboard/reminder/$1';
$route['Corporate_Dashboard'] = 'corporate_module/Corporate_Dashboard';
$route['Prepare_order/index'] = 'corporate_module/Prepare_order/index';
$route['Prepare_order/search'] = 'corporate_module/Prepare_order/search';
$route['Prepare_order/store'] = 'corporate_module/Prepare_order/store';
$route['Prepare_order/check_quantity'] = 'corporate_module/Prepare_order/check_quantity';
$route['Prepare_order/(:any)'] = 'corporate_module/Prepare_order/index/$1';
$route['Print_Order/index/(:any)'] = 'corporate_module/Print_Order/index/$1';
$route['Print_Order/createPdf/(:any)'] = 'corporate_module/Print_Order/createPdf/$1';
$route['Print_Order/get_product_code'] = 'corporate_module/Print_Order/get_product_code';
$route['Print_Order/get_karigar_wt_limit'] = 'corporate_module/Print_Order/get_karigar_wt_limit';
$route['Reprint_Order/index/(:any)'] = 'corporate_module/Reprint_Order/index/$1';
$route['Reprint_Order/createPdf/(:any)'] = 'corporate_module/Reprint_Order/createPdf/$1';
$route['Reprint_Order/get_product_code'] = 'corporate_module/Reprint_Order/get_product_code';
$route['Karigar_product_list'] = 'corporate_module/Karigar_product_list';
$route['Karigar_product_list/(:any)'] = 'corporate_module/Karigar_product_list/index';
$route['Engaged_karigar_list/delete'] = 'corporate_module/Engaged_karigar_list/delete';
$route['Engaged_karigar_list'] = 'corporate_module/Engaged_karigar_list';
$route['Engaged_karigar_list/export_karigar_images'] = 'corporate_module/Engaged_karigar_list/export_karigar_images';
$route['Engaged_karigar_list/show/(:any)'] = 'corporate_module/Engaged_karigar_list/show/$1';
$route['Engaged_karigar_list/(:any)'] = 'corporate_module/Engaged_karigar_list/index';
$route['Engaged_karigar_list/export_karigar_engaged/(:any)'] = 'corporate_module/Engaged_karigar_list/export_karigar_engaged/$1';
$route['Search_order'] = 'corporate_module/Search_order';
$route['Search_order/search'] = 'corporate_module/Search_order/search';
$route['Search_order/store'] = 'corporate_module/Search_order/store';
$route['corporate_module_Dashboard'] = 'corporate_module/corporate_module_Dashboard';
$route['Prepare_order'] = 'corporate_module/Prepare_order';
$route['Prepare_order/search/(:any)'] = 'corporate_module/Prepare_order/search/$1';
$route['Prepare_order/search'] = 'corporate_module/Prepare_order/search';
$route['Prepare_order/store'] = 'corporate_module/Prepare_order/store';
$route['Products_sent'] = 'corporate_module/Products_sent';
$route['Products_sent/index'] = 'corporate_module/Products_sent/index';
$route['Products_sent/show/(:any)'] = 'corporate_module/Products_sent/show/$1';
$route['Products_sent/export'] = 'corporate_module/Products_sent/export';
$route['Products_sent/reprint'] = 'corporate_module/Products_sent/reprint';

$route['Receive_products'] = 'corporate_module/Receive_products/index';
$route['Receive_products/check_corporate'] = 'corporate_module/Receive_products/check_corporate';

$route['Receive_products/index'] = 'corporate_module/Receive_products/index';
$route['Receive_products/getNameByProductCode'] = 'corporate_module/Receive_products/getNameByProductCode';
$route['Receive_products/create'] = 'corporate_module/Receive_products/create';
$route['Receive_products/store'] = 'corporate_module/Receive_products/store';
$route['Receive_products/get_karigar_name'] = 'corporate_module/Receive_products/get_karigar_name';
$route['Receive_products/get_karigar_engaged_products'] = 'corporate_module/Receive_products/get_karigar_engaged_products';
$route['Receive_products/send_to_amended'] = 'corporate_module/Receive_products/send_to_amended';
$route['Receive_products/send_to_rejected'] = 'corporate_module/Receive_products/send_to_rejected';

$route['Quality_control/create/(:any)'] = 'corporate_module/Quality_control/create/$1';
$route['Quality_control/Send_rejected_products/(:any)'] = 'corporate_module/Quality_control/Send_rejected_products/$1';
$route['Quality_control/Send_rejected_products_to_stock/(:any)'] = 'corporate_module/Quality_control/Send_rejected_products_to_stock/$1';
$route['Quality_control/store'] = 'corporate_module/Quality_control/store';
$route['Quality_control/reject'] = 'corporate_module/Quality_control/reject';
$route['Quality_control/amend'] = 'corporate_module/Quality_control/amend';
$route['Quality_control'] = 'corporate_module/Quality_control/index';
$route['Quality_control/index'] = 'corporate_module/Quality_control/index';
$route['Quality_control/send_to_hallmarking'] = 'corporate_module/Quality_control/send_to_hallmarking';
$route['Quality_control/send_to_hallmarking/(:any)'] = 'corporate_module/Quality_control/send_to_hallmarking/$1';
$route['Quality_control/receive_from_hallmarking/(:any)'] = 'corporate_module/Quality_control/receive_from_hallmarking/$1';
$route['Quality_control/generate_packing_list'] = 'corporate_module/Quality_control/generate_packing_list';
$route['Quality_control/view_remark/(:any)'] = 'corporate_module/Quality_control/view_remark/$1';
$route['Hallmarking'] = 'corporate_module/Hallmarking/index';
$route['Hallmarking/index'] = 'corporate_module/Hallmarking/index';
$route['Hallmarking/export'] = 'corporate_module/Hallmarking/export';
$route['Hallmarking/store'] = 'corporate_module/Hallmarking/store';
$route['Hallmarking/reject'] = 'corporate_module/Hallmarking/reject';
$route['Hallmarking/send_to_qcexported'] = 'corporate_module/Hallmarking/send_to_qcexported';
$route['Hallmarking/send_to_relience/(:any)'] = 'corporate_module/Hallmarking/send_to_relience/$1';
$route['Hallmarking/export/(:any)'] = 'corporate_module/Hallmarking/export/$1';
$route['Hallmarking/Send_rejected_products/(:any)'] = 'corporate_module/Hallmarking/Send_rejected_products/$1';
$route['Hallmarking/barcode_products'] = 'corporate_module/Hallmarking/barcode_products';
$route['Hallmarking/barcode_products_print'] = 'corporate_module/Hallmarking/barcode_products_print';

$route['Hallmarking/create/(:any)'] = 'corporate_module/Hallmarking/create/$1';
$route['Hallmarking/view_remark/(:any)'] = 'corporate_module/Hallmarking/view_remark/$1';
$route['Hallmarking/export_accepted/(:any)'] = 'corporate_module/Hallmarking/export_accepted/$1';
$route['Hallmarking/export_accepted'] = 'corporate_module/Hallmarking/export_accepted';
$route['Quality_control/export'] = 'corporate_module/Quality_control/export';
$route['Quality_control/export/(:any)'] = 'corporate_module/Quality_control/export/$1';
$route['Print_Stickers/index/(:any)'] = 'corporate_module/Print_Stickers/index/$1';
$route['Print_Stickers/createPdf/(:any)'] = 'corporate_module/Print_Stickers/createPdf/$1';
$route['Print_Stickers/store'] = 'corporate_module/Print_Stickers/store';
$route['Corporate_product/reject'] = 'corporate_module/Corporate_product/reject';
$route['Quality_control/send_to_prepare_order/(:any)'] = 'corporate_module/Quality_control/send_to_prepare_order/$1';
$route['Stock'] = 'corporate_module/Stock/index';
$route['Stock/(:any)'] = 'corporate_module/Stock/index/$1';
// $route['Stock/approve'] = 'corporate_module/Stock/approve';
// $route['Stock/reject'] = 'corporate_module/Stock/reject';

$route['stock/Send_stock_products_party/(:any)']='corporate_module/Stock/Send_stock_products_party/$1';
$route['stock/Send_stock_products_karigar/(:any)']='corporate_module/Stock/Send_stock_products_karigar/$1';
$route['stock/rejected/send_to_karigar']='corporate_module/Stock/send_to_karigar';
$route['stock/rejected/send_to_stock']='corporate_module/Stock/send_to_stock';

$route['Generate_packing_list'] = 'corporate_module/Generate_packing_list/index';
$route['Generate_packing_list/index'] = 'corporate_module/Generate_packing_list/index';
$route['Generate_packing_list/create'] = 'corporate_module/Generate_packing_list/create';
$route['Generate_packing_list/send_to_karigar/(:any)'] = 'corporate_module/Generate_packing_list/send_to_karigar/$1';
$route['Generate_packing_list/send_to_stock/(:any)'] = 'corporate_module/Generate_packing_list/send_to_stock/$1';
$route['Generate_packing_list/store'] = 'corporate_module/Generate_packing_list/store';
$route['Generate_packing_list/search'] = 'corporate_module/Generate_packing_list/search';
$route['Generate_packing_list/export'] = 'corporate_module/Generate_packing_list/export';
$route['Generate_packing_list/show/(:any)'] = 'corporate_module/Generate_packing_list/show/$1';
$route['Generate_packing_list/change_status'] = 'corporate_module/Generate_packing_list/change_status';

$route['Amend_products'] = 'corporate_module/Amend_products/index';
$route['Amend_products/multiple_send_to_karigar'] = 'corporate_module/Amend_products/multiple_send_to_karigar';
$route['Amend_products/multiple_send_to_stock'] = 'corporate_module/Amend_products/multiple_send_to_stock';
$route['Amend_products/show/(:any)'] = 'corporate_module/Amend_products/show/$1';
$route['Amend_products/send_to_karigar/(:any)'] = 'corporate_module/Amend_products/send_to_karigar/$1';
$route['Amend_products/mark_as_amend/(:any)'] = 'corporate_module/Amend_products/mark_as_amend/$1';
$route['Amend_products/export'] = 'corporate_module/Amend_products/export';
$route['Amend_products/export/(:any)'] = 'corporate_module/Amend_products/export/$1';
$route['Import_bill_excel'] = 'corporate_module/Import_bill_excel/index';
$route['Import_bill_excel/store'] = 'corporate_module/Import_bill_excel/store';
$route['generate_chitti/(:any)'] = 'corporate_module/generate_chitti/index/$1';
$route['generate_chitti/show/(:any)'] = 'corporate_module/generate_chitti/show/$1';
$route['generate_chitti/accept_reject/1'] = 'corporate_module/generate_chitti/accept_reject';


$route['Amend_products/send_to_stock/(:any)'] = 'corporate_module/Amend_products/send_to_stock/$1';
$route['Amend_products/view/(:any)'] = 'corporate_module/Amend_products/view/$1';

$route['User_app/delete']='order_module/User_app/delete';
$route['User_app']='order_module/User_app';
$route['User_app/update']='order_module/User_app/update';
$route['User_app/update_status']='order_module/User_app/update_status';
$route['User_app/edit/(:any)']='order_module/User_app/edit/$1';
$route['User_app/create']='order_module/User_app/create';
$route['User_app/store']='order_module/User_app/store';

$route['customer_order/backend_entery_data']='order_module/customer_order/backend_entery_data';
/*order detailes*/
$route['Corporate_order_list'] = 'corporate_module/Corporate_order_list/index';
$route['Corporate_order_list/detail/(:any)'] = 'corporate_module/Corporate_order_list/detail/$1';
$route['Corporate_order_list/show/(:any)'] = 'corporate_module/Corporate_order_list/show/$1';
$route['Corporate_order_list/work_order_detail'] = 'corporate_module/Corporate_order_list/work_order_detail';

/* 

| -------------------------------------------------------------------------
| End
| -------------------------------------------------------------------------

*/

/* 

| -------------------------------------------------------------------------
| Corporate module routes
| -------------------------------------------------------------------------

*/
$route['Category'] = 'master/Category';
$route['Category/index'] = 'master/Category/index';
$route['Category/create'] = 'master/Category/create';
$route['Category/edit/(:any)'] = 'master/Category/edit/$1';
$route['Category/update'] = 'master/Category/update';
$route['Category/Store'] = 'master/Category/Store';
$route['Category/delete'] = 'master/Category/delete';


$route['Carat'] = 'master/Carat';
$route['Carat/index'] = 'master/Carat/index';
$route['Carat/create'] = 'master/Carat/create';
$route['Carat/edit/(:any)'] = 'master/Carat/edit/$1';
$route['Carat/edit/(:any)'] = 'master/Carat/edit/$1';
$route['Carat/update'] = 'master/Carat/update';
$route['Carat/store'] = 'master/Carat/store';
$route['Carat/delete'] = 'master/Carat/delete';

$route['Carat/edit/(:any)'] = 'master/Carat/edit/$1';
$route['Buying_complexity'] = 'master/Buying_complexity';
$route['Buying_complexity/index'] = 'master/Buying_complexity/index';
$route['Buying_complexity/create'] = 'master/Buying_complexity/create';
$route['Buying_complexity/edit/(:any)'] = 'master/Buying_complexity/edit/$1';
$route['Buying_complexity/update'] = 'master/Buying_complexity/update';
$route['Buying_complexity/store'] = 'master/Buying_complexity/store';
$route['Buying_complexity/delete'] = 'master/Buying_complexity/delete';

$route['Article'] = 'master/Article';
$route['Article/index'] = 'master/Article/index';
$route['Article/create'] = 'master/Article/create';
$route['Article/edit/(:any)'] = 'master/Article/edit/$1';
$route['Article/update'] = 'master/Article/update';
$route['Article/store'] = 'master/Article/store';
$route['Article/delete'] = 'master/Article/delete';

$route['Karigar'] = 'master/Karigar';
$route['Karigar/index'] = 'master/Karigar/index';
$route['Karigar/create'] = 'master/Karigar/create';
$route['Karigar/edit/(:any)'] = 'master/Karigar/edit/$1';
$route['Karigar/merge_replace_karigar_list/(:any)'] = 'master/Karigar/merge_replace_karigar_list/$1';
$route['Karigar/merge_replace'] = 'master/Karigar/merge_replace';

$route['Karigar/update'] = 'master/Karigar/update';
$route['Karigar/store'] = 'master/Karigar/store';
$route['Karigar/delete'] = 'master/Karigar/delete';
$route['Karigar/get_city'] = 'master/Karigar/get_city';

$route['Party_master'] = 'master/Party_master';
$route['Party_master/index'] = 'master/Party_master/index';
$route['Party_master/create'] = 'master/Party_master/create';
$route['Party_master/edit/(:any)/(:any)'] = 'master/Party_master/edit/$1/$2';
$route['Party_master/update'] = 'master/Party_master/update';
$route['Party_master/store'] = 'master/Party_master/store';
$route['Party_master/delete'] = 'master/Party_master/delete';
$route['Party_master/get_city'] = 'master/Party_master/get_city';
$route['Party_master/get'] = 'master/Party_master/get';
$route['Party_master/get_party_type'] = 'master/Party_master/get_party_type';


$route['Manufacturing_type'] = 'master/Manufacturing_type';
$route['Manufacturing_type/index'] = 'master/Manufacturing_type/index';
$route['Manufacturing_type/create'] = 'master/Manufacturing_type/create';
$route['Manufacturing_type/edit/(:any)'] = 'master/Manufacturing_type/edit/$1';
$route['Manufacturing_type/update'] = 'master/Manufacturing_type/update';
$route['Manufacturing_type/store'] = 'master/Manufacturing_type/store';
$route['Manufacturing_type/delete'] = 'master/Manufacturing_type/delete';

$route['Metal'] = 'master/Metal';
$route['Metal/index'] = 'master/Metal/index';
$route['Metal/create'] = 'master/Metal/create';
$route['Metal/edit/(:any)'] = 'master/Metal/edit/$1';
$route['Metal/update'] = 'master/Metal/update';
$route['Metal/store'] = 'master/Metal/store';
$route['Metal/delete'] = 'master/Metal/delete';

$route['Pcs'] = 'master/Pcs';
$route['Pcs/index'] = 'master/Pcs/index';
$route['Pcs/create'] = 'master/Pcs/create';
$route['Pcs/edit/(:any)'] = 'master/Pcs/edit/$1';
$route['Pcs/update'] = 'master/Pcs/update';
$route['Pcs/store'] = 'master/Pcs/store';
$route['Pcs/delete'] = 'master/Pcs/delete';

$route['Sub_category'] = 'master/Sub_category';

$route['Sub_category/index'] = 'master/Sub_category/index';
$route['Sub_category/create'] = 'master/Sub_category/create';
$route['Sub_category/edit/(:any)'] = 'master/Sub_category/edit/$1';
$route['Sub_category/view_dropbox_image/(:any)'] = 'master/Sub_category/view_dropbox_image/$1';
$route['Sub_category/view/(:any)'] = 'master/Sub_category/view/$1';
$route['Sub_category/fetch_images_from_dropbox'] = 'master/Sub_category/fetch_images_from_dropbox';
$route['Sub_category/create_minimum_stock/(:any)'] = 'master/Sub_category/create_minimum_stock/$1';
$route['Sub_category/save_minimum_stock'] = 'master/Sub_category/save_minimum_stock';
$route['Sub_category/update'] = 'master/Sub_category/update';
$route['Sub_category/store'] = 'master/Sub_category/store';
$route['Sub_category/delete'] = 'master/Sub_category/delete';
$route['Sub_category/update_sub_cat_images'] = 'master/Sub_category/update_sub_cat_images';


$route['Weight_range'] = 'master/Weight_range';
$route['Weight_range/index'] = 'master/Weight_range/index';
$route['Weight_range/create'] = 'master/Weight_range/create';
$route['Weight_range/edit/(:any)'] = 'master/Weight_range/edit/$1';
$route['Weight_range/update'] = 'master/Weight_range/update';
$route['Weight_range/store'] = 'master/Weight_range/store';
$route['Weight_range/delete'] = 'master/Weight_range/delete';

$route['Department'] = 'master/Department';
$route['Department/index'] = 'master/Department/index';
$route['Department/create'] = 'master/Department/create';
$route['Department/delete'] = 'master/Department/delete';
$route['Department/edit/(:any)'] = 'master/Department/edit/$1';
$route['Department/update'] = 'master/Department/update';
$route['Department/store'] = 'master/Department/store';

$route['hallmarking_center'] = 'master/Hallmarking_center';
$route['hallmarking_center/index'] = 'master/Hallmarking_center/index';
$route['hallmarking_center/create'] = 'master/Hallmarking_center/create';
$route['hallmarking_center/edit/(:any)'] = 'master/Hallmarking_center/edit/$1';
$route['hallmarking_center/update'] = 'master/Hallmarking_center/update';
$route['hallmarking_center/store'] = 'master/Hallmarking_center/store';
$route['hallmarking_center/delete'] = 'master/Hallmarking_center/delete';

$route['product_category'] = 'sale_master/Product_category';
$route['product_category/index'] = 'sale_master/Product_category/index';
$route['product_category/create'] = 'sale_master/Product_category/create';
$route['product_category/edit/(:any)'] = 'sale_master/Product_category/edit/$1';
$route['product_category/update'] = 'sale_master/Product_category/update';
$route['product_category/store'] = 'sale_master/Product_category/store';
$route['product_category/delete'] = 'sale_master/Product_category/delete';


$route['product_category_rate'] = 'master/Product_category_rate';
$route['product_category_rate/index'] = 'master/Product_category_rate/index';
$route['product_category_rate/create'] = 'master/Product_category_rate/create';
$route['product_category_rate/edit/(:any)'] = 'master/Product_category_rate/edit/$1';
$route['product_category_rate/update'] = 'master/Product_category_rate/update';
$route['product_category_rate/store'] = 'master/Product_category_rate/store';
$route['product_category_rate/delete'] = 'master/Product_category_rate/delete';

$route['hallmarking_list'] = 'sale_master/Hallmarking_list';
$route['hallmarking_list/index'] = 'sale_master/Hallmarking_list/index';
$route['hallmarking_list/create'] = 'sale_master/Hallmarking_list/create';
$route['hallmarking_list/edit/(:any)'] = 'sale_master/Hallmarking_list/edit/$1';
$route['hallmarking_list/print_hallmarking_center/(:any)'] = 'sale_master/Hallmarking_list/print_hallmarking_center/$1';
$route['hallmarking_list/update'] = 'sale_master/Hallmarking_list/update';
$route['hallmarking_list/store'] = 'sale_master/Hallmarking_list/store';
$route['hallmarking_list/delete'] = 'sale_master/Hallmarking_list/delete';
$route['hallmarking_list/get'] = 'sale_master/Hallmarking_list/get';
$route['hallmarking_list/get_city'] = 'sale_master/hallmarking_list/get_city';
$route['hallmarking_list/export'] = 'sale_master/hallmarking_list/export';


$route['parent_category'] = 'sale_master/Parent_category';
$route['parent_category/index'] = 'sale_master/Parent_category/index';
$route['parent_category/create'] = 'sale_master/Parent_category/create';
$route['parent_category/edit/(:any)'] = 'sale_master/Parent_category/edit/$1';
$route['parent_category/update'] = 'sale_master/Parent_category/update';
$route['parent_category/store'] = 'sale_master/Parent_category/store';
$route['parent_category/delete'] = 'sale_master/Parent_category/delete';
$route['parent_category/fetch_from_dropbox'] = 'sale_master/Parent_category/fetch_from_dropbox';
$route['parent_category/get_images'] = 'sale_master/Parent_category/get_images';
$route['parent_category/create_minimum_stock/(:any)'] = 'sale_master/Parent_category/create_minimum_stock/$1';
$route['parent_category/store_minimum_stock'] = 'sale_master/Parent_category/store_minimum_stock';



$route['Sales_under'] = 'sale_master/Sales_under';
$route['Sales_under/index'] = 'sale_master/Sales_under/index';
$route['Sales_under/create'] = 'sale_master/Sales_under/create';
$route['Sales_under/edit/(:any)'] = 'sale_master/Sales_under/edit/$1';
$route['Sales_under/update'] = 'sale_master/Sales_under/update';
$route['Sales_under/store'] = 'sale_master/Sales_under/store';
$route['Sales_under/delete'] = 'sale_master/Sales_under/delete';

$route['color_stone'] = 'sale_master/color_stone';
$route['color_stone/index'] = 'sale_master/color_stone/index';
$route['color_stone/create'] = 'sale_master/color_stone/create';
$route['color_stone/edit/(:any)'] = 'sale_master/color_stone/edit/$1';
$route['color_stone/update'] = 'sale_master/color_stone/update';
$route['color_stone/store'] = 'sale_master/color_stone/store';
$route['color_stone/delete'] = 'sale_master/color_stone/delete';

$route['kundan_category'] = 'sale_master/kundan_category';
$route['kundan_category/index'] = 'sale_master/kundan_category/index';
$route['kundan_category/create'] = 'sale_master/kundan_category/create';
$route['kundan_category/edit/(:any)'] = 'sale_master/kundan_category/edit/$1';
$route['kundan_category/update'] = 'sale_master/kundan_category/update';
$route['kundan_category/store'] = 'sale_master/kundan_category/store';
$route['kundan_category/delete'] = 'sale_master/kundan_category/delete';

$route['type_master'] = 'sale_master/type_master';
$route['type_master/index'] = 'sale_master/type_master/index';
$route['type_master/create'] = 'sale_master/type_master/create';
$route['type_master/edit/(:any)'] = 'sale_master/type_master/edit/$1';
$route['type_master/update'] = 'sale_master/type_master/update';
$route['type_master/store'] = 'sale_master/type_master/store';
$route['type_master/delete'] = 'sale_master/type_master/delete';

$route['customer'] = 'sale_master/customer';
$route['customer/index'] = 'sale_master/customer/index';
$route['customer/create'] = 'sale_master/customer/create';
$route['customer/edit/(:any)'] = 'sale_master/customer/edit/$1';
$route['customer/update'] = 'sale_master/customer/update';
$route['customer/store'] = 'sale_master/customer/store';
$route['customer/delete'] = 'sale_master/customer/delete';
$route['customer/get_city'] = 'sale_master/customer/get_city';


$route['dispatch_mode'] = 'master/dispatch_mode';
$route['dispatch_mode/index'] = 'master/dispatch_mode/index';
$route['dispatch_mode/create'] = 'master/dispatch_mode/create';
$route['dispatch_mode/edit/(:any)'] = 'master/dispatch_mode/edit/$1';
$route['dispatch_mode/update'] = 'master/dispatch_mode/update';
$route['dispatch_mode/store'] = 'master/dispatch_mode/store';
$route['dispatch_mode/delete'] = 'master/dispatch_mode/delete';
$route['dispatch_mode/view/(:any)'] = 'master/dispatch_mode/view/$1';
$route['dispatch_mode/dispatch_mode_data_store'] = 'master/dispatch_mode/dispatch_mode_data_store';
$route['dispatch_mode/dispatch_mode_data_update'] = 'master/dispatch_mode/dispatch_mode_data_update';

$route['customer_type'] = 'sale_master/customer_type';
$route['customer_type/index'] = 'sale_master/customer_type/index';
$route['customer_type/create'] = 'sale_master/customer_type/create';
$route['customer_type/edit/(:any)'] = 'sale_master/customer_type/edit/$1';
$route['customer_type/update'] = 'sale_master/customer_type/update';
$route['customer_type/store'] = 'sale_master/customer_type/store';
$route['customer_type/delete'] = 'sale_master/customer_type/delete';

$route['salesman_master'] = 'sale_master/salesman_master';
$route['salesman_master/index'] = 'sale_master/salesman_master/index';
$route['salesman_master/create'] = 'sale_master/salesman_master/create';
$route['salesman_master/edit/(:any)'] = 'sale_master/salesman_master/edit/$1';
$route['salesman_master/update'] = 'sale_master/salesman_master/update';
$route['salesman_master/store'] = 'sale_master/salesman_master/store';
$route['salesman_master/delete'] = 'sale_master/salesman_master/delete';
$route['salesman_master/get_city'] = 'sale_master/salesman_master/get_city';

$route['Material'] = 'master/Material_master';
$route['Material/index'] = 'master/Material_master/index';
$route['Material/create'] = 'master/Material_master/create';
$route['Material/edit/(:any)'] = 'master/Material_master/edit/$1';
$route['Material/update'] = 'master/Material_master/update';
$route['Material/store'] = 'master/Material_master/store';
$route['Material/delete'] = 'master/Material_master/delete';
$route['Material/view/(:any)'] = 'master/Material_master/view/$1';

$route['Variations_category'] = 'sale_master/Variations_category';
$route['Variations_category/index'] = 'sale_master/Variations_category/index';
$route['Variations_category/create'] = 'sale_master/Variations_category/create';
$route['Variations_category/edit/(:any)'] = 'sale_master/Variations_category/edit/$1';
$route['Variations_category/update'] = 'sale_master/Variations_category/update';
$route['Variations_category/store'] = 'sale_master/Variations_category/store';
$route['Variations_category/delete'] = 'sale_master/Variations_category/delete';

$route['Term'] = 'master/Terms';
$route['Term/index'] = 'master/Terms/index';
$route['Term/create'] = 'master/Terms/create';
$route['Term/edit/(:any)'] = 'master/Terms/edit/$1';
$route['Term/update'] = 'master/Terms/update';
$route['Term/store'] = 'master/Terms/store';
$route['Term/delete'] = 'master/Terms/delete';

$route['parent_sub_category'] = 'master/parent_sub_category';
$route['parent_sub_category/index'] = 'master/parent_sub_category/index';
$route['parent_sub_category/create'] = 'master/parent_sub_category/create';
$route['parent_sub_category/edit/(:any)'] = 'master/parent_sub_category/edit/$1';
$route['parent_sub_category/update'] = 'master/parent_sub_category/update';
$route['parent_sub_category/store'] = 'master/parent_sub_category/store';
$route['parent_sub_category/delete'] = 'master/parent_sub_category/delete';

/* 

| -------------------------------------------------------------------------
| End
| -------------------------------------------------------------------------

*/

/* 

| -------------------------------------------------------------------------
| User routes
| -------------------------------------------------------------------------

*/


$route['User'] = 'user/User';
$route['User/index'] = 'user/User/index';
$route['User/create'] = 'user/User/create';
$route['User/edit/(:any)'] = 'user/User/edit/$1';
$route['User/update'] = 'user/User/update';
$route['User/store'] = 'user/User/store';
$route['User/delete'] = 'user/User/delete';
/* 

| -------------------------------------------------------------------------
| End
| -------------------------------------------------------------------------

*/
/* 

| -------------------------------------------------------------------------
| Manufacturing_module
| -------------------------------------------------------------------------

*/
$route['Manufacturing_Dashboard'] = 'Manufacturing_module/Manufacturing_Dashboard';
$route['Manufacturing_Dashboard/(:any)'] = 'Manufacturing_module/Manufacturing_Dashboard/$1';
$route['Manufacturing_department_order'] = 'Manufacturing_module/Manufacturing_department_order/index';
$route['Manufacturing_department_order/create'] = 'Manufacturing_module/Manufacturing_department_order/create';
$route['Manufacturing_department_order/create/(:any)'] = 'Manufacturing_module/Manufacturing_department_order/create/$1';
$route['Manufacturing_department_order/edit/(:any)'] = 'Manufacturing_module/Manufacturing_department_order/edit/$1';
$route['Manufacturing_department_order/validate_order_sheet'] = 'Manufacturing_module/Manufacturing_department_order/validate_order_sheet';

$route['Manufacturing_department_order/show'] = 'Manufacturing_module/Manufacturing_department_order/show';

$route['Manufacturing_department_order/order_placed'] = 'Manufacturing_module/Manufacturing_department_order/order_placed';

$route['Manufacturing_department_order/store/(:any)'] = 'Manufacturing_module/Manufacturing_department_order/store/$1';
$route['Manufacturing_department_order/view/(:any)'] = 'Manufacturing_module/Manufacturing_department_order/view/$1';
$route['Manufacturing_department_order/store'] = 'Manufacturing_module/Manufacturing_department_order/store';
$route['Manufacturing_department_order/delete/(:any)'] = 'Manufacturing_module/Manufacturing_department_order/delete/$1';
$route['Manufacturing_department_order/delete_order/(:any)'] = 'Manufacturing_module/Manufacturing_department_order/delete_order/$1';
$route['Manufacturing_department_order/get_sub_cat_images'] = 'Manufacturing_module/Manufacturing_department_order/get_sub_cat_images';
$route['Manufacturing_department_order/test'] = 'Manufacturing_module/Manufacturing_department_order/test';
$route['Prepare_karigar/store'] = 'Manufacturing_module/Prepare_karigar/store';
$route['Prepare_karigar/change'] = 'Manufacturing_module/Prepare_karigar/change';
$route['Karigar_order_list'] = 'Manufacturing_module/Karigar_order_list';
$route['Karigar_order_list/index'] = 'Manufacturing_module/Karigar_order_list/index';
$route['Karigar_order_list/show/(:any)'] = 'Manufacturing_module/Karigar_order_list/show/$1';
$route['Karigar_order_list/delete/(:any)'] = 'Manufacturing_module/Karigar_order_list/delete/$1';
$route['Karigar_order_list/kariger_details/(:any)'] = 'Manufacturing_module/Karigar_order_list/kariger_details/$1';
$route['Karigar_order_list/print_karigar_order'] = 'Manufacturing_module/Karigar_order_list/print_karigar_order';
$route['Karigar_order_list/print_karigar_order/(:any)'] = 'Manufacturing_module/Karigar_order_list/print_karigar_order/$1';
$route['Karigar_order_list/re_print_order/(:any)'] = 'Manufacturing_module/Karigar_order_list/re_print_order/$1';
$route['Karigar_order_list/validate_postdata'] = 'Manufacturing_module/Karigar_order_list/validate_postdata';

$route['Manufacturing_engaged_karigar/reprint_order/(:any)'] = 'Manufacturing_module/Engaged_karigar_list/reprint_order/$1';
$route['Manufacturing_engaged_karigar/(:any)'] = 'Manufacturing_module/Engaged_karigar_list/index';
$route['Manufacturing_engaged_karigar/delete'] = 'Manufacturing_module/Engaged_karigar_list/delete';
$route['Manufacturing_engaged_karigar'] = 'Manufacturing_module/Engaged_karigar_list/index';
$route['Manufacturing_engaged_karigar/show/(:any)'] = 'Manufacturing_module/Engaged_karigar_list/show/$1';

$route['Order_sent'] = 'Manufacturing_module/Order_sent';
$route['Order_sent/index'] = 'Manufacturing_module/Order_sent/index';
$route['Order_sent/store'] = 'Manufacturing_module/Order_sent/store';
$route['Received_orders'] = 'Manufacturing_module/Received_orders/index';
$route['Received_orders/view/(:any)'] = 'Manufacturing_module/Received_orders/view/$1';
$route['Received_orders/index'] = 'Manufacturing_module/Received_orders/index';
$route['Received_orders/receive_product/(:any)'] = 'Manufacturing_module/Received_orders/receive_product/$1';
$route['Received_orders/check_all'] = 'Manufacturing_module/Received_orders/check_all';
$route['Received_orders/check/(:any)'] = 'Manufacturing_module/Received_orders/check/$1';
$route['Received_orders/receive_product/(:any)'] = 'Manufacturing_module/Received_orders/receive_product/$1';
$route['Received_orders/print_receipt'] = 'Manufacturing_module/Received_orders/print_receipt';
$route['Received_orders/print_voucher'] = 'Manufacturing_module/Received_orders/print_voucher';
$route['Received_orders/resend_to_karigar/(:any)'] = 'Manufacturing_module/Received_orders/resend_to_karigar/$1';
$route['Received_orders/karigar_receipt'] = 'Manufacturing_module/Received_orders/karigar_receipt';
$route['Received_orders/karigar_receipt/(:any)'] = 'Manufacturing_module/Received_orders/karigar_receipt/$1';
$route['Received_orders/check_net_wt'] = 'Manufacturing_module/Received_orders/check_net_wt';
$route['Received_orders/rejected_product'] = 'Manufacturing_module/Received_orders/rejected_product';

$route['Mnfg_accounting_received_orders'] = 'Manufacturing_module/Mnfg_accounting_received_orders/index';
$route['Mnfg_accounting_received_orders/view/(:any)'] = 'Manufacturing_module/Mnfg_accounting_received_orders/view/$1';
$route['Mnfg_accounting_received_orders/index'] = 'Manufacturing_module/Mnfg_accounting_received_orders/index';
$route['Mnfg_accounting_received_orders/karigar_receipt'] = 'Manufacturing_module/Mnfg_accounting_received_orders/karigar_receipt';
$route['Mnfg_accounting_received_orders/karigar_receipt/(:any)'] = 'Manufacturing_module/Mnfg_accounting_received_orders/karigar_receipt/$1';
$route['Mnfg_accounting_received_orders/store'] = 'Manufacturing_module/Mnfg_accounting_received_orders/store';

$route['Mnfg_accounting'] = 'Manufacturing_module/Mnfg_accounting/index';
$route['Mnfg_accounting/index'] = 'Manufacturing_module/Mnfg_accounting/index';
$route['Mnfg_accounting/store'] = 'Manufacturing_module/Mnfg_accounting/store';
$route['Mnfg_accounting/view/(:any)'] = 'Manufacturing_module/Mnfg_accounting/view/$1';
$route['Mnfg_accounting/Manufacturing_accounting_receipt'] = 'Manufacturing_module/Mnfg_accounting/Manufacturing_accounting_receipt';
$route['Mnfg_accounting/Manufacturing_accounting_receipt/(:any)'] = 'Manufacturing_module/Mnfg_accounting/Manufacturing_accounting_receipt/$1';
$route['Mnfg_accounting/check_quantity'] = 'Manufacturing_module/Mnfg_accounting/check_quantity';
$route['Mnfg_accounting/print_accounting_receipt'] = 'Manufacturing_module/Mnfg_accounting/print_accounting_receipt';
$route['Mnfg_accounting/receive_product/(:any)'] = 'Manufacturing_module/Mnfg_accounting/receive_product/$1';

$route['Mnfg_accounting_rejected_orders'] = 'Manufacturing_module/Mnfg_accounting_rejected_orders/index';
$route['Mnfg_accounting_rejected_orders/view/(:any)'] = 'Manufacturing_module/Mnfg_accounting_rejected_orders/view/$1';
$route['Mnfg_accounting_rejected_orders/index'] = 'Manufacturing_module/Mnfg_accounting_rejected_orders/index';
$route['Mnfg_accounting_rejected_orders/Manufacturing_rejected_receipt'] = 'Manufacturing_module/Mnfg_accounting_rejected_orders/Manufacturing_rejected_receipt';
$route['Mnfg_accounting_rejected_orders/Manufacturing_rejected_receipt/(:any)'] = 'Manufacturing_module/Mnfg_accounting_rejected_orders/Manufacturing_rejected_receipt/$1';
$route['Mnfg_accounting_rejected_orders/rejected_products/(:any)'] = 'Manufacturing_module/Mnfg_accounting_rejected_orders/rejected_products/$1';
$route['Mnfg_accounting_rejected_orders/change_net_wt'] = 'Manufacturing_module/Mnfg_accounting_rejected_orders/change_net_wt';
$route['Mnfg_accounting_rejected_orders/print_change_net_wt'] = 'Manufacturing_module/Mnfg_accounting_rejected_orders/print_change_net_wt';


$route['Received_receipt'] = 'Manufacturing_module/Received_receipt/index';
$route['Received_receipt/index'] = 'Manufacturing_module/Received_receipt/index';


$route['Sending_to_qc_mfg'] = 'Manufacturing_module/Sending_to_qc_mfg/index';
$route['Sending_to_qc_mfg/send_mfg_qc/(:any)'] = 'Manufacturing_module/Sending_to_qc_mfg/send_mfg_qc/$1';




$route['Mfg_dep_direct_order/store'] = 'Manufacturing_module/Mfg_dep_direct_order/store';
$route['Mfg_dep_direct_order/create'] = 'Manufacturing_module/Mfg_dep_direct_order/create';
$route['Mfg_dep_direct_order/print_receipt/(:any)'] = 'Manufacturing_module/Mfg_dep_direct_order/print_receipt/$1';

$route['Manufacturing_quality_control'] = 'Manufacturing_module/Manufacturing_quality_control/index';
$route['Manufacturing_quality_control/index'] = 'Manufacturing_module/Manufacturing_quality_control/index';
$route['Manufacturing_quality_control/create/(:any)'] = 'Manufacturing_module/Manufacturing_quality_control/create/$1';
$route['Manufacturing_quality_control/store'] = 'Manufacturing_module/Manufacturing_quality_control/store';
$route['Manufacturing_quality_control/store_all'] = 'Manufacturing_module/Manufacturing_quality_control/store_all';
$route['Manufacturing_quality_control/store_all/(:any)'] = 'Manufacturing_module/Manufacturing_quality_control/store_all/$1';
$route['Manufacturing_quality_control/reject'] = 'Manufacturing_module/Manufacturing_quality_control/reject';
$route['Manufacturing_quality_control/reject_all'] = 'Manufacturing_module/Manufacturing_quality_control/reject_all';
$route['Manufacturing_quality_control/karigar_receipt'] = 'Manufacturing_module/Manufacturing_quality_control/karigar_receipt';
$route['Manufacturing_quality_control/karigar_receipt/(:any)'] = 'Manufacturing_module/Manufacturing_quality_control/karigar_receipt/$1';
$route['Manufacturing_quality_control/send_to_hallmarking/(:any)'] = 'Manufacturing_module/Manufacturing_quality_control/send_to_hallmarking/$1';
$route['Manufacturing_quality_control/send_all_to_hallmarking'] = 'Manufacturing_module/Manufacturing_quality_control/send_all_to_hallmarking';

$route['Manufacturing_quality_control/view_remark/(:any)'] = 'Manufacturing_module/Manufacturing_quality_control/view_remark/$1';
$route['Manufacturing_quality_control/check_all'] = 'Manufacturing_module/Manufacturing_quality_control/check_all';
$route['Manufacturing_quality_control/check/(:any)'] = 'Manufacturing_module/Manufacturing_quality_control/check/$1';
$route['Manufacturing_quality_control/receive_product/(:any)'] = 'Manufacturing_module/Manufacturing_quality_control/receive_product/$1';
$route['Manufacturing_quality_control/print_receipt'] = 'Manufacturing_module/Manufacturing_quality_control/print_receipt';
$route['Manufacturing_quality_control/print_voucher'] = 'Manufacturing_module/Manufacturing_quality_control/print_voucher';
$route['Manufacturing_quality_control/resend_to_karigar/(:any)'] = 'Manufacturing_module/Manufacturing_quality_control/resend_to_karigar/$1';
$route['Manufacturing_quality_control/qc_ammend/(:any)'] = 'Manufacturing_module/Manufacturing_quality_control/qc_ammend/$1';
$route['Manufacturing_quality_control/check_quantity'] = 'Manufacturing_module/Manufacturing_quality_control/check_quantity';
$route['Manufacturing_quality_control/re_print_receipt/(:any)'] = 'Manufacturing_module/Manufacturing_quality_control/re_print_receipt/$1';
$route['Manufacturing_quality_control/hallmarking_receipt'] = 'Manufacturing_module/Manufacturing_quality_control/hallmarking_receipt';
$route['Manufacturing_quality_control/resend_to_karagar'] = 'Manufacturing_module/Manufacturing_quality_control/resend_to_karagar';

$route['Manufacturing_quality_control/generate_karigar_voucher'] = 'Manufacturing_module/Manufacturing_quality_control/generate_karigar_voucher';

$route['Manufacturing_hallmarking'] = 'Manufacturing_module/Manufacturing_hallmarking/index';
$route['Manufacturing_hallmarking/index'] = 'Manufacturing_module/Manufacturing_hallmarking/index';
$route['Manufacturing_hallmarking/create/(:any)'] = 'Manufacturing_module/Manufacturing_hallmarking/create/$1';
$route['Manufacturing_hallmarking/store'] = 'Manufacturing_module/Manufacturing_hallmarking/store';
$route['Manufacturing_hallmarking/reject'] = 'Manufacturing_module/Manufacturing_hallmarking/reject';
$route['Manufacturing_hallmarking/view_remark/(:any)'] = 'Manufacturing_module/Manufacturing_hallmarking/view_remark/$1';
$route['Manufacturing_tag_products/create/(:any)'] = 'Manufacturing_module/Manufacturing_tag_products/create/$1';
$route['Manufacturing_tag_products/print_tag_products/(:any)'] = 'Manufacturing_module/Manufacturing_tag_products/print_tag_products/$1';
$route['Tag_products/create_item_code'] = 'Manufacturing_module/Tag_products/create_item_code';
$route['Make_set'] = 'Manufacturing_module/Make_set/index';
$route['Make_set/index'] = 'Manufacturing_module/Make_set/index';
$route['Make_set/create'] = 'Manufacturing_module/Make_set/create';
$route['Make_set/store'] = 'Manufacturing_module/Make_set/store';
$route['Make_set/edit/(:any)'] = 'Manufacturing_module/Make_set/edit/$1';
$route['Make_set/update'] = 'Manufacturing_module/Make_set/update';
$route['Make_set/delete'] = 'Manufacturing_module/Make_set/delete';
$route['Make_set/show/(:any)'] = 'Manufacturing_module/Make_set/show/$1';
$route['Make_set/store_image'] = 'Manufacturing_module/Make_set/store_image';
$route['Ready_product/store'] = 'Manufacturing_module/Ready_product/store';
$route['ready_product/delete'] = 'Manufacturing_module/Ready_product/delete';
$route['Ready_product/print_tag_products'] = 'Manufacturing_module/Ready_product/print_tag_products';
$route['ready_product'] = 'Manufacturing_module/Ready_product/index';
$route['Ready_product/index/(:any)'] = 'Manufacturing_module/Ready_product/index/$1';
$route['Ready_product/sales_to_manufacture'] = 'Manufacturing_module/Ready_product/sales_to_manufacture';
$route['Ready_product/transfer_to_sales'] = 'Manufacturing_module/Ready_product/transfer_to_sales';
$route['Ready_product/repair_products'] = 'Manufacturing_module/Ready_product/repair_products';

$route['Ready_product/singal_barcode_products/(:any)'] = 'Manufacturing_module/Ready_product/singal_barcode_products/$1';
$route['Ready_product/barcode_products'] = 'Manufacturing_module/Ready_product/barcode_products';
$route['Ready_product/view/(:any)'] = 'Manufacturing_module/Ready_product/view/$1';
$route['Sales_recieved_product/view/(:any)'] = 'sales_module/Sales_recieved_product/view/$1';

$route['Upload_stock_excel']='Manufacturing_module/Upload_stock_excel';

$route['Ready_product/import'] = 'Manufacturing_module/Ready_product/import';



$route['Ready_product/generate_voucher'] = 'Manufacturing_module/Ready_product/generate_voucher';
$route['Ready_product/print_product_barcode'] = 'Manufacturing_module/Ready_product/print_product_barcode';
$route['Ready_product/generate_issue_receipt_repair'] = 'Manufacturing_module/Ready_product/generate_issue_receipt_repair';

$route['ready_product/change_location'] = 'Manufacturing_module/Ready_product/change_location';
/*Sales_return voucher _product */
$route['sales_return_product'] = 'Manufacturing_module/Sales_return_product/index';
$route['sales_return_product/index/(:any)'] = 'Manufacturing_module/Sales_return_product/index/$1';
$route['sales_return_product/transfer_to_sales_return_voucher'] = 'Manufacturing_module/Sales_return_product/transfer_to_sales_return_voucher';


$route['tag_products'] = 'Manufacturing_module/Tag_products/index';
$route['tag_products/create'] = 'Manufacturing_module/Tag_products/create';
$route['tag_products/get_sub_category'] = 'Manufacturing_module/Tag_products/get_sub_category';
$route['tag_products/get_parent_category'] = 'Manufacturing_module/Tag_products/get_parent_category';
$route['tag_products/store'] = 'Manufacturing_module/Tag_products/store';
$route['tag_products/get_qc_details'] = 'Manufacturing_module/Tag_products/get_qc_details';
$route['tag_products/max_count_category'] = 'Manufacturing_module/Tag_products/max_count_category';
$route['repair_product'] = 'Manufacturing_module/Repair_product/index';
$route['repair_product/issue'] = 'Manufacturing_module/Repair_product/issue_to_karigar';
$route['repair_product_list'] = 'Manufacturing_module/Repair_product/repair_product_list';
$route['repair_product/get_repair_details'] = 'Manufacturing_module/Repair_product/get_repair_details';
$route['repair_product/store/(:any)'] = 'Manufacturing_module/Repair_product/store/$1';
$route['repair_product/store_all'] = 'Manufacturing_module/Repair_product/store_all';
$route['difference_product'] = 'Manufacturing_module/Repair_product/difference_product';
$route['repair_product/stock'] = 'Manufacturing_module/Repair_product/repair_stock';
$route['Repair_product/get_repair_max_code'] = 'Manufacturing_module/Repair_product/get_repair_max_code';


$route['Few_box'] = 'Manufacturing_module/Few_box/index';
$route['Few_box/get_repair_details'] = 'Manufacturing_module/Few_box/few_weight_details';
$route['Few_box/create'] = 'Manufacturing_module/Few_box/create';
$route['Few_box/store'] = 'Manufacturing_module/Few_box/store';
$route['Few_box/update'] = 'Manufacturing_module/Few_box/update';
$route['Few_box/edit/(:any)'] = 'Manufacturing_module/Few_box/edit/$1';
$route['Few_box/delete'] = 'Manufacturing_module/Few_box/delete';


$route['tag_products/get_max_code'] = 'Manufacturing_module/Tag_products/get_max_code';
$route['tag_products/make_pdf/(:any)'] = 'Manufacturing_module/tag_products/make_pdf/$1';
$route['Prepare_kundan_karigar_order/store/(:any)'] = 'Manufacturing_module/Prepare_kundan_karigar_order/store/$1';
$route['Prepare_kundan_karigar_order/download_pdf/(:any)'] = 'Manufacturing_module/Prepare_kundan_karigar_order/download_pdf/$1';
$route['kundan_karigar'] = 'Manufacturing_module/Kundan_karigar';
$route['kundan_karigar/index'] = 'Manufacturing_module/Kundan_karigar/index';
$route['kundan_karigar/update'] = 'Manufacturing_module/Kundan_karigar/update';
$route['kundan_karigar/send_to_receive_products/(:any)'] = 'Manufacturing_module/Kundan_karigar/send_to_receive_products/$1';
$route['kundan_karigar/send_to_receive_all_products'] = 'Manufacturing_module/Kundan_karigar/send_to_receive_all_products';
$route['kundan_karigar/re_print/(:any)'] = 'Manufacturing_module/Kundan_karigar/re_print/$1';

$route['kundan_QC'] = 'Manufacturing_module/Kundan_QC';
$route['kundan_QC/index'] = 'Manufacturing_module/Kundan_QC/index';
$route['kundan_QC/accept_all'] ='Manufacturing_module/Kundan_QC/accept_all';
$route['kundan_QC/update'] = 'Manufacturing_module/Kundan_QC/update';
$route['kundan_QC/create'] = 'Manufacturing_module/Kundan_QC/create';
$route['kundan_QC/create/(:any)'] = 'Manufacturing_module/Kundan_QC/create/$1';
$route['kundan_QC/accept/(:any)'] = 'Manufacturing_module/Kundan_QC/accept/$1';
$route['kundan_QC/send_kundan_qc/(:any)'] = 'Manufacturing_module/Kundan_QC/send_kundan_qc/$1';
$route['kundan_QC/reject'] = 'Manufacturing_module/Kundan_QC/reject';
$route['kundan_QC/store'] = 'Manufacturing_module/Kundan_QC/store';
$route['kundan_QC/ready_products'] = 'Manufacturing_module/Kundan_QC/ready_products';
$route['kundan_QC/reprint_order/(:any)'] = 'Manufacturing_module/Kundan_QC/reprint_order/$1';

$route['karigar_receive_order'] = 'Manufacturing_module/Karigar_receive_order';
$route['karigar_receive_order/index'] = 'Manufacturing_module/Karigar_receive_order/index';
$route['karigar_receive_order/create'] = 'Manufacturing_module/Karigar_receive_order/create';
$route['karigar_receive_order/validate'] = 'Manufacturing_module/Karigar_receive_order/validate';
$route['karigar_receive_order/print_pdf'] = 'Manufacturing_module/Karigar_receive_order/print_pdf';
$route['karigar_receive_order/karigar_receipt'] = 'Manufacturing_module/Karigar_receive_order/karigar_receipt';
$route['karigar_receive_order/karigar_receipt/(:any)'] = 'Manufacturing_module/Karigar_receive_order/karigar_receipt/$1';
$route['karigar_receive_order/get_details/(:any)'] = 'Manufacturing_module/Karigar_receive_order/get_details/$1';
$route['karigar_receive_order/download_pdf'] = 'Manufacturing_module/Karigar_receive_order/download_pdf';
$route['tag_products/remove'] = 'Manufacturing_module/Tag_products/remove';
$route['accounting_department'] = 'Manufacturing_module/accounting_department/index';
$route['accounting_department/index'] = 'Manufacturing_module/accounting_department/index';
$route['accounting_department/view/(:any)'] = 'Manufacturing_module/accounting_department/view/$1';
$route['accounting_department/store'] = 'Manufacturing_module/accounting_department/store';
$route['ready_product/approved_by_sale'] = 'Manufacturing_module/ready_product/approved_by_sale';
$route['ready_product/rejected_by_sale'] = 'Manufacturing_module/ready_product/rejected_by_sale';


/*Sales Module*/
$route['sales_dashboard'] = 'sales_module/Sales_Dashboard';
/*prepare order*/
$route['Prepared_order']='sales_module/Prepared_order';
$route['prepared_order/index']='sales_module/Prepared_order';
$route['Prepared_order/create']='sales_module/Prepared_order/create';
$route['Prepared_order/edit/(:any)']='sales_module/Prepared_order/edit/$1';
$route['Prepared_order/show/(:any)']='sales_module/Prepared_order/show/$1';
$route['Prepared_order/store']='sales_module/Prepared_order/store';
$route['Prepared_order/update']='sales_module/Prepared_order/update';
$route['Prepared_order/send_to_manufacture']='sales_module/Prepared_order/send_to_manufacture';

/*recieved from manuf*/
$route['Sales_recieved_product']='sales_module/Sales_recieved_product';
$route['sales_recieved_product/index']='sales_module/Sales_recieved_product';
$route['sales_recieved_product/show/(:any)']='sales_module/Sales_recieved_product/show/$1';
$route['sales_recieved_product/show']='sales_module/Sales_recieved_product/show';
$route['Sales_recieved_product/change_location/(:any)'] = 'sales_module/Sales_recieved_product/change_location/$1';

// $route['Prepared_order/create']='sales_module/Prepared_order/create';
// $route['Prepared_order/store']='sales_module/Prepared_order/store';
// $route['Prepared_order/send_to_manufacture']='sales_module/Prepared_order/send_to_manufacture';

/*sales_stock*/

$route['sales_stock/delete']='sales_module/Sales_stock/delete';
$route['sales_stock/store']='sales_module/Sales_stock/store';
$route['sales_stock/store_all']='sales_module/Sales_stock/store_all';
$route['sales_stock']='sales_module/Sales_stock';
$route['sales_stock/index']='sales_module/Sales_stock';
$route['sales_stock/get_item_details']='sales_module/Sales_stock/get_item_details';
$route['sales_stock/get_stock_product']='sales_module/Sales_stock/get_stock_product';
$route['sales_stock/sales_to_manufacture'] = 'sales_module/Sales_stock/sales_to_manufacture';
$route['sales_stock/sales_to_manufacture_all'] = 'sales_module/Sales_stock/sales_to_manufacture_all';
$route['sales_stock/stock_to_manufacture_all'] = 'sales_module/Sales_stock/stock_to_manufacture_all';
$route['sales_stock/stock_to_transfer_all'] = 'sales_module/Sales_stock/stock_to_transfer_all';
$route['sales_stock/create']='sales_module/Sales_stock/create';
$route['sales_stock/stock']='sales_module/Sales_stock/stock';
$route['sales_stock/get_category_name']='sales_module/Sales_stock/get_category_name';
$route['sales_stock/edit/(:any)'] = 'sales_module/Sales_stock/edit/$1';






/*corporate_accounting*/
$route['accounting']='corporate_accounting/Dashboard';
/*karigar*/
$route['accounting/issue']='corporate_accounting/Issue';
$route['accounting/issue/index']='corporate_accounting/Issue';
$route['accounting/issue/create']='corporate_accounting/Issue/create';
$route['accounting/issue/store']='corporate_accounting/Issue/store';


$route['accounting/issue/show/(:any)']='corporate_accounting/Issue/show/$1';
$route['accounting/issue/export/(:any)']='corporate_accounting/Issue/export/$1';



/*received*/
$route['accounting/receipt']='corporate_accounting/Receipt';
$route['accounting/receipt/index']='corporate_accounting/Receipt';
$route['accounting/receipt/create']='corporate_accounting/Receipt/create';
$route['accounting/receipt/store']='corporate_accounting/Receipt/store';
$route['accounting/receipt/show/(:any)']='corporate_accounting/Receipt/show/$1';
$route['accounting/receipt/export/(:any)']='corporate_accounting/Receipt/export/$1';



$route['Manufacture_report'] = 'Report/Report_manufacture';

$route['Daily_stock_summary_report'] = 'Manufacture_report/Daily_stock_summary_report/index';

$route['Manufacture_report/karigar'] = 'Report/Karigar_report_manufacture';

$route['Manufacture_report/order_status'] = 'Report/Order_status_report_manufacture/index';
$route['Manufacture_report/order_status/view/(:any)'] = 'Report/Order_status_report_manufacture/view/$1';
$route['Manufacture_report/order_status/download'] = 'Report/Order_status_report_manufacture/download';
$route['Manufacture_report/order_status/download_detailed_report/(:any)'] = 'Report/Order_status_report_manufacture/download_detailed_report/$1';
$route['Manufacture_report/stock_summary'] = 'Report/Stock_summary_report_manufacture';
$route['Manufacture_report/stock_summary/download'] = 'Report/Stock_summary_report_manufacture/download';

$route['Manufacture_report/order_summary'] = 'Report/Order_summary_report_manufacture';
$route['Manufacture_report/order_summary/download'] = 'Report/Order_summary_report_manufacture/download';

$route['Manufacture_report/Stock_detail_summary'] = 'Report/Stock_detail_summary_report_manufacture';
$route['Manufacture_report/Stock_detail_summary/download'] = 'Report/Stock_detail_summary_report_manufacture/download';

$route['Manufacture_report/Minimum_subcategory'] = 'Report/Minimum_subcat_order_report_manufacture';
$route['Manufacture_report/Minimum_subcategory/download'] = 'Report/Stock_detail_summary_report_manufacture/download';

$route['Manufacture_report/Order_ageing'] = 'Report/Order_ageing_report_manufacture';
$route['Manufacture_report/Order_ageing/download'] = 'Report/Order_ageing_report_manufacture/download';

$route['Manufacture_report/Category_order_ageing'] = 'Report/Category_order_ageing_report_manufacture';
$route['Manufacture_report/Category_order_ageing/download'] = 'Report/Category_order_ageing_report_manufacture/download';

$route['Manufacture_report/Stock_ageing'] = 'Report/Stock_ageing_report_manufacture';
$route['Manufacture_report/Stock_ageing/download'] = 'Report/Stock_ageing_report_manufacture/download';

$route['Manufacture_report/karigarwise_detail'] = 'Report/karigarwise_detail_report_manufacture';
$route['Manufacture_report/karigarwise_detail/download'] = 'Report/karigarwise_detail_report_manufacture/download';

$route['Manufacture_report/qc_report'] = 'Report/Qc_report_manufacture';
$route['Manufacture_report/qc_report/download'] = 'Report/Qc_report_manufacture/download';

$route['Manufacture_report/kundan_qc_report'] = 'Report/Kundan_qc_report_manufacture';
$route['Manufacture_report/kundan_qc_report/download'] = 'Report/Kundan_qc_report_manufacture/download';

$route['Manufacture_report/itemwise_stock'] = 'Report/Itemwise_stock_report_manufacture';
$route['Manufacture_report/itemwise_stock/download'] = 'Report/Itemwise_stock_report_manufacture/download';

$route['Manufacture_report/transfer_register'] = 'Report/Transfer_register_report_manufacture';
$route['Manufacture_report/transfer_register/download'] = 'Report/Transfer_register_report_manufacture/download';

$route['Manufacture_report/kundan_karigar_summary'] = 'Report/Kundan_karigar_report_manufacture';
$route['Manufacture_report/kundan_karigar_summary/download'] = 'Report/Kundan_karigar_report_manufacture/download';

$route['Manufacture_report/kundan_karigarwise_detail'] = 'Report/Kundan_Karigarwise_detail_report_manufacture';
$route['Manufacture_report/kundan_karigarwise_detail/download'] = 'Report/Kundan_karigarwise_detail_report_manufacture/download';

$route['Manufacture_report/kundan_karigar'] = 'Report/Kundan_report_manufacture';
$route['Manufacture_report/kundan_karigar/download'] = 'Report/Kundan_report_manufacture/download';

$route['Manufacture_report/metal_balance'] = 'Report/Metal_balance_report_manufacture';
$route['Manufacture_report/metal_balance/download'] = 'Report/Metal_balance_report_manufacture/download';








$route['Pending_to_be_recieved']='order_module/Pending_to_be_recieved';
$route['Pending_to_be_recieved/index']='order_module/Pending_to_be_recieved/index';
$route['Pending_to_be_recieved/create']='order_module/Pending_to_be_recieved/create';
$route['Pending_to_be_recieved/get_order_by_karigar']='order_module/Pending_to_be_recieved/get_order_by_karigar';
$route['Pending_to_be_recieved/store']='order_module/Pending_to_be_recieved/store';

$route['Recieved_customer_products']='order_module/Recieved_products';
$route['Recieved_customer_products/index']='order_module/Recieved_products/index';
$route['Recieved_customer_products/sent_to_customer']='order_module/Recieved_products/sent_to_customer';


$route['karigar/get_wastage']='master/karigar/get_wastage';

/* 

| -------------------------------------------------------------------------
| End
| -------------------------------------------------------------------------

*/





$route['product_code_max_count_store'] = 'Manufacturing_module/Tag_products/product_code_max_count_store';