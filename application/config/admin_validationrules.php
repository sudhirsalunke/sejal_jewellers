<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

$config['login'] = array(
    array(
        'field' => 'username',
        'label' => 'Email',
        'rules' => 'trim|required|verifyCredentials'
    ),
    array(
        'field' => 'password',
        'label' => 'Password',
        'rules' => 'trim|required|md5'
    )
);
$config['Category'] = array(
    array(
        'field' => 'Category[code]',
        'label' => 'Category code',
        'rules' => 'trim|required|check_category_code'
    ),
    array(
        'field' => 'Category[name]',
        'label' => 'Category name',
        'rules' => 'trim|required|check_category_name'
    )
);
$config['departments'] = array(
    array(
        'field' => 'Department[name]',
        'label' => 'Department name',
        'rules' => 'trim|required|check_department_exist'
    )
);
$config['receive_pieces'] = array(
    array(
        'field' => 'corporate',
        'label' => 'Corporate',
        'rules' => 'trim|required'
    ),
    array(
        'field' => 'status',
        'label' => 'status',
        'rules' => 'trim|required'
    ),
    array(
        'field' => 'order_id',
        'label' => 'Order Id',
        'rules' => 'trim|required|check_order_exist'
    )
);
$config['buying_complexity'] = array(
    array(
        'field' => 'buying_complexity[name]',
        'label' => 'Buying complexity name',
        'rules' => 'trim|required|check_buying_complexity_name'
    ),
);
$config['manufacturing_type'] = array(
    array(
        'field' => 'manufacturing_type[name]',
        'label' => 'Manufacturing type name',
        'rules' => 'trim|required|check_manufacturing_type_name'
    ),
);
$config['pcs'] = array(
    array(
        'field' => 'pcs[name]',
        'label' => 'Pcs Number',
        'rules' => 'trim|required|numeric|check_pcs_name'
    ),
);
$config['product'] = array(
    array(
        'field' => 'product[product_code]',
        'label' => 'Product code',
        'rules' => 'trim|required|check_product_code'
    ),
    array(
        'field' => 'product[karigar_id]',
        'label' => 'Karigar',
        'rules' => 'trim|required|check_karigar'
    ),
   /* array(
        'field' => 'product[size]',
        'label' => 'Size',
        'rules' => 'trim|greater_than_equal_to[0.0]'
    ),*/
    array(
        'field' => 'product[category_id]',
        'label' => 'Category',
        'rules' => 'trim|required|check_category'
    ),
    array(
        'field' => 'product[weight_band_id]',
        'label' => 'Weight band',
        'rules' => 'trim|required'
    ),
    array(
        'field' => 'product[corporate]',
        'label' => 'Corporate',
        'rules' => 'trim|required'
    ),
    array(
        'field' => 'product[sub_category_id]',
        'label' => 'Sub category',
        'rules' => 'trim|required|check_sub_category'
    )
    /*array(
        'field' => 'product[article_id]',
        'label' => 'Article',
        'rules' => 'trim|required|check_article'
    ),
    array(
        'field' => 'product[carat_id]',
        'label' => 'Carat',
        'rules' => 'trim|required|check_carat'
    ),
    array(
        'field' => 'product[metal_id]',
        'label' => 'Metal',
        'rules' => 'trim|required|check_metal'
    ),
    
    array(
        'field' => 'product[buying_complexity_id]',
        'label' => 'Buying Complexity',
        'rules' => 'trim|required'
    ),
    array(
        'field' => 'product[manufacturing_type_id]',
        'label' => 'Manufacturing type',
        'rules' => 'trim|required'
    ),
    array(
        'field' => 'product[pcs_id]',
        'label' => 'Pcs Number',
        'rules' => 'trim|required|check_pcs'
    ), */
   
);
$config['Sub_category'] = array(
    array(
        'field' => 'Sub_category[code]',
        'label' => 'Category code',
        'rules' => 'trim|required|check_sub_category_code'
    ),
    array(
        'field' => 'Sub_category[name]',
        'label' => 'Category name',
        'rules' => 'trim|required|check_sub_category_name'
    ),
    array(
        'field' => 'Sub_category[category_id]',
        'label' => 'Category',
        'rules' => 'trim|required'
    )
);
$config['Karigar'] = array(
    // array(
    //     'field' => 'Karigar[code]',
    //     'label' => 'Karigar code',
    //     'rules' => 'check_karigar_code'
    // ),
    array(
        'field' => 'Karigar[name]',
        'label' => 'Party name',
        'rules' => 'trim|required'
    ),
    
    array(
        'field' => 'Karigar[wastage]',
        'label' => 'Wastage',
        'rules' => 'trim|required'
    ),
   
    array(
        'field' => 'Karigar[customer_type_id]',
        'label' => 'customer type',
        'rules' => 'trim|required',
        'errors' =>array('required'=>"Please Select Type"),
    ),


    
);

$config['Party'] = array(
    // array(
    //     'field' => 'Karigar[code]',
    //     'label' => 'Karigar code',
    //     'rules' => 'check_karigar_code'
    // ),
    array(
        'field' => 'Karigar[name]',
        'label' => 'Party name',
        'rules' => 'trim|required'
    ),

   
    array(
        'field' => 'Karigar[customer_type_id]',
        'label' => 'customer type',
        'rules' => 'trim|required',
        'errors' =>array('required'=>"Please Select Type"),
    ),


    
);

$config['app_user'] = array(
    // array(
    //     'field' => 'Karigar[code]',
    //     'label' => 'Karigar code',
    //     'rules' => 'trim|required|check_karigar_code'
    // ),
    array(
        'field' => 'user[first_name]',
        'label' => 'First name',
        'rules' => 'trim|required|regex_match[/^[a-zA-Z ]*$/]',
        'errors' => array("regex_match" => "enter valid details"),
    ),
    
    //  array(
    //     'field' => 'user[middle_name]',
    //     'label' => 'Middle name',
    //     'rules' => 'trim|required|regex_match[/^[a-zA-Z ]*$/]',
    //     'errors' => array("regex_match" => "enter valid details"),
    // ),

     array(
        'field' => 'user[last_name]',
        'label' => 'Last name',
        'rules' => 'trim|required|regex_match[/^[a-zA-Z ]*$/]',
        'errors' => array("regex_match" => "enter valid details"),
    ), 
     array(
        'field' => 'user[company_name]',
        'label' => 'Company name',
        'rules' => 'trim|required|regex_match[/^[a-zA-Z ]*$/]',
        'errors' => array("regex_match" => "enter valid details"),
    ),
    //   array(
    //     'field' => 'Karigar[address]',
    //     'label' => 'Karigar address',
    //     'rules' => 'trim|required'
    // ),
    // array(
    //     'field' => 'Karigar[contact_person]',
    //     'label' => 'Contact Person',
    //     'rules' => 'trim|required'
    // ),
    //  array(
    //     'field' => 'Karigar[phone_no]',
    //     'label' => 'Phone No',
    //     'rules' => 'trim|required|numeric',
    //     'errors' =>array(
    //         'numeric'=>"Please Enter Digits not characters",
    //         ),
    // ),
    array(
        'field' => 'user[mobile_no]',
        'label' => 'Mobile No',
        'rules' => 'trim|required|exact_length[10]|numeric',
        'errors' =>array(
            'exact_length'=>"Please Enter Valid Mobile Number",
            'numeric'=>"Please Enter Digits not characters",
            ),
    ),
    // array(
    //     'field' => 'Karigar[customer_type_id]',
    //     'label' => 'customer_type_id',
    //     'rules' => 'trim|required',
    //     'errors' =>array('required'=>"Please Select Type"),
    // ),
    //  array(
    //     'field' => 'Karigar[hc_id]',
    //     'label' => 'hc_id',
    //     'rules' => 'trim|required',
    //     'errors' =>array('required'=>"Please Select Hallmarking Center"),
    // ),
    //   array(
    //     'field' => 'Karigar[refer_by]',
    //     'label' => 'Reference Name',
    //     'rules' => 'trim|required',
        
    // ),
    /*array(
        'field' => 'Karigar[area]',
        'label' => 'area',
        'rules' => 'trim|required',
    ),
    array(
        'field' => 'Karigar[city_id]',
        'label' => 'city_id',
        'rules' => 'trim|required',
        'errors' =>array('required'=>"Please Select City"),
    ),
    array(
        'field' => 'Karigar[state_id]',
        'label' => 'state_id',
        'rules' => 'trim|required',
        'errors' =>array('required'=>"Please Select State"),
    ),*/array(
        'field' => 'user[email]',
        'label' => 'Email',
        'rules' => 'trim|required|valid_email',
        //'rules' => 'trim|valid_email',
        //'errors' =>array('required'=>"Please Select Email"),
    ),
    // array(
    //     'field' => 'Karigar[limit_amt]',
    //     'label' => 'limit amt',
    //     'rules' => 'trim|required|numeric',
    // ),
    // array(
    //     'field' => 'Karigar[limit_metal]',
    //     'label' => 'limit metal',
    //     'rules' => 'trim|required|numeric',
    // ),
    // array(
    //     'field' => 'Karigar[days]',
    //     'label' => 'days',
    //     'rules' => 'trim|required|is_natural_no_zero',
    //     'errors'=> array('is_natural_no_zero' =>"Please Enter Numbers Only"),
    // ),
    // array(
    //     'field' => 'Karigar[sales_under]',
    //     'label' => 'sales under',
    //     'rules' => 'trim|required',
    //     'errors' =>array('required'=>"Please Select Sales Under"),
    // ),
    // array(
    //     'field' => 'Karigar[terms]',
    //     'label' => 'terms',
    //     'rules' => 'trim|required',
    // ),
    // array(
    //     'field' => 'Karigar[bank_ac]',
    //     'label' => 'Bank Account Number',
    //     'rules' => 'trim|required|is_natural_no_zero',
    //     'errors' =>array('is_natural_no_zero'=>"Please Select Valid Bank Account Number"),
    // ),
    // array(
    //     'field' => 'Karigar[bank_ifsc]',
    //     'label' => 'Bank IFSC code',
    //     'rules' => 'trim|required',
    // ),
    // array(
    //     'field' => 'Karigar[bank_name]',
    //     'label' => 'Bank Name',
    //     'rules' => 'trim|required',
    // ),
    // array(
    //     'field' => 'Karigar[bank_branch]',
    //     'label' => 'Branch Name',
    //     'rules' => 'trim|required',
    // ),
);
$config['Weight_range'] = array(
    array(
        'field' => 'Weight_range[from]',
        'label' => 'From',
        'rules' => 'trim|required|numeric'
    ),
    array(
        'field' => 'Weight_range[to]',
        'label' => 'To',
        'rules' => 'trim|numeric'
    )
);
$config['Article'] = array(
    array(
        'field' => 'Article[name]',
        'label' => 'Article name',
        'rules' => 'trim|required|check_article_exist'
    ),
    array(
        'field' => 'Article[description]',
        'label' => 'Article description',
        'rules' => 'trim|required'
    )
);
$config['Metal'] = array(
    array(
        'field' => 'Metal[name]',
        'label' => 'Metal name',
        'rules' => 'trim|required|check_metal_name'
    )
);
$config['Carat'] = array(
    array(
        'field' => 'Carat[name]',
        'label' => 'Carat name',
        'rules' => 'trim|required|check_carat_name'
    )
);

$config['product_sub_category'] = array(
    array(
        'field' => 'product[sub_category_id]',
        'label' => 'Sub category',
        'rules' => 'trim|required|check_sub_category'
    )
);
$config['products'] = array(
    array(
        'field' => 'products',
        'label' => 'Products',
        'rules' => 'trim|required'
    )
);
$config['Prepare_Order'] = array(
    array(
        'field' => 'karigar_id',
        'label' => 'Karigar',
        'rules' => 'trim|required'
    )
);
$config['Quality_control'] = array(
    /*array(
        'field' => 'gt_no',
        'label' => 'GR No',
        'rules' => 'trim|required|numeric'
    ),*/array(
        'field' => 'quantity',
        'label' => 'Quantity',
        'rules' => 'trim|required|greater_than_equal_to[0.1]'
    ),array(
        'field' => 'total_gr_wt',
        'label' => 'Total GR WT',
        'rules' => 'trim|required|greater_than_equal_to[0.1]'
    ),array(
        'field' => 'total_net_wt',
        'label' => 'Total NET WT',
        'rules' => 'trim|required|greater_than_equal_to[0.0]'
    ),array(
        'field' => 'cp_size',
        'label' => 'Size',
        'rules' => 'trim|required'
    )/*,array(
        'field' => 'stone_wt',
        'label' => 'stone weight',
        'rules' => 'trim|required|numeric'
    )*/,array(
        'field' => 'less_wt',
        'label' => 'Less weight',
        'rules' => 'trim|required|greater_than_equal_to[0.0]'
    )
);
$config['Manufacturing_quality_control'] = array(
    array(
        'field' => 'net_wt',
        'label' => 'Net WT',
        'rules' => 'trim|required|numeric'
    ),array(
        'field' => 'quantity',
        'label' => 'Quantity',
        'rules' => 'trim|required|numeric|quantity_validation'
    ),array(
        'field' => 'few_wt',
        'label' => 'Few WT',
        'rules' => 'trim|required|numeric'
    ),array(
        'field' => 'mks_wt',
        'label' => 'MKS WT',
        'rules' => 'trim|required|numeric'
    ),array(
        'field' => 'stone_wt',
        'label' => 'Stone WT',
        'rules' => 'trim|required|numeric'
    ),array(
        'field' => 'kundan_wt',
        'label' => 'Kundan WT',
        'rules' => 'trim|required|numeric'
    ),array(
        'field' => 'kundan_pc',
        'label' => 'Kundan PC',
        'rules' => 'trim|required|numeric'
    ),array(
        'field' => 'stone_amt',
        'label' => 'Stone Amount',
        'rules' => 'trim|required|numeric'
    ),array(
        'field' => 'kundan_amt',
        'label' => 'Kundan Amount',
        'rules' => 'trim|required|numeric'
    ),array(
        'field' => 'other_amt',
        'label' => 'Other Amount',
        'rules' => 'trim|required|numeric'
    )
);
$config['Manufacturing_quality_control_all_qc'] = array(
    array(
        'field' => 'weight',
        'label' => 'Weight',
        'rules' => 'trim|required|greater_than_equal_to[0.1]'
    ),
    array(
        'field' => 'quantity',
        'label' => 'Quantity',
        'rules' => 'trim|required|greater_than_equal_to[0.1]|check_qc_qnt'
    ),
    array(
        'field' => 'receive_product_id',
        'label' => 'receive product id',
        'rules' => 'trim|required|numeric'
    )
);

$config['Mfg_qc_ctr_all_qc_bom'] = array(
    array(
        'field' => 'bom_weight',
        'label' => 'weight',
        'rules' => 'trim|required|greater_than_equal_to[0.1]|check_qc_wt'
    ),
    array(
        'field' => 'bom_nt_wt',
        'label' => 'Net weight',
        'rules' => 'trim|required|greater_than_equal_to[0.1]'
    ),
    array(
        'field' => 'receive_product_id',
        'label' => 'receive product id',
        'rules' => 'trim|required|numeric'
    )
);
$config['User_access'] = array(
    array(
        'field' => 'User_access[first_name]',
        'label' => 'First name',
        'rules' => 'trim|required'
    ),
    array(
        'field' => 'User_access[last_name]',
        'label' => 'First name',
        'rules' => 'trim|required'
    ),
    array(
        'field' => 'User_access[email]',
        'label' => 'Email',
        'rules' => 'trim|required|valid_email'
    ),
    array(
        'field' => 'User_access[password]',
        'label' => 'Password',
        'rules' => 'trim|required'
    ),
    array(
        'field' => 'User_access[controller_id][]',
        'label' => 'Master',
        'rules' => 'trim|required'
    )
);
$config['User_access_edit'] = array(
    array(
        'field' => 'User_access[first_name]',
        'label' => 'First name',
        'rules' => 'trim|required'
    ),
    array(
        'field' => 'User_access[last_name]',
        'label' => 'First name',
        'rules' => 'trim|required'
    ),
    array(
        'field' => 'User_access[email]',
        'label' => 'Email',
        'rules' => 'trim|required|valid_email'
    ),
    array(
        'field' => 'User_access[controller_id][]',
        'label' => 'Master',
        'rules' => 'trim|required'
    )
);
$config['search_order'] = array(
   array(
        'field' => 'Order[date]',
        'label' => 'Date',
        'rules' => 'trim|required'
    ),array(
        'field' => 'Order[gross_weight]',
        'label' => 'Gross weight',
        'rules' => 'trim|required|greater_than_equal_to[0.1]'
    ),array(
        'field' => 'Order[net_weight]',
        'label' => 'Net weight',
        'rules' => 'trim|required'
    ),array(
        'field' => 'Order[corporate]',
        'label' => 'Corporate',
        'rules' => 'trim|required'
    ),array(
        'field' => 'Order[less_weight]',
        'label' => 'Less weight',
        'rules' => 'trim|required|numeric|greater_than_equal_to[0.0]'
    )
);
$config['department_order'] = array(
    array(
        'field' => 'department_order[order_date]',
        'label' => 'Order Date',
        'rules' => 'trim|required'
    ),
    array(
        'field' => 'department_order[department_id]',
        'label' => 'Department',
        'rules' => 'trim|required'
    ),
    array(
        'field' => 'department_order[sub_category_id]',
        'label' => 'Product',
        'rules' => 'trim|required'
    ),
    array(
        'field' => 'department_order[weight_range_id]',
        'label' => 'Weight range',
        'rules' => 'trim|required'
    ),
    array(
        'field' => 'department_order[quantity]',
        'label' => 'Quantity',
        'rules' => 'trim|required|is_natural_no_zero',
        'errors'=>array('is_natural_no_zero'=>'Please Enter Valid Quantity'),
    ),
    array(
        'field' => 'department_order[department_id]',
        'label' => 'Department',
        'rules' => 'trim|required'
    ),
    array(
        'field' => 'department_order[order_name]',
        'label' => 'order name',
        'rules' => 'trim|required'
    )
);
$config['department_order_flow'] = array(
    array(
        'field' => 'department_order[order_date]',
        'label' => 'Order Date',
        'rules' => 'trim|required'
    ),
    array(
        'field' => 'department_order[department_id]',
        'label' => 'Department',
        'rules' => 'trim|required'
    ),
    array(
        'field' => 'department_order[sub_category_id]',
        'label' => 'Product',
        'rules' => 'trim|required'
    ),
    array(
        'field' => 'department_order[weight_range_id]',
        'label' => 'Weight range',
        'rules' => 'trim|required'
    ),
    array(
        'field' => 'department_order[weight]',
        'label' => 'Weight',
        'rules' => 'trim|required|is_natural_no_zero',
        'errors'=>array('is_natural_no_zero'=>'Please Enter Valid Weight'),
    ),
   
    array(
        'field' => 'department_order[department_id]',
        'label' => 'Department',
        'rules' => 'trim|required'
    ),
    array(
        'field' => 'department_order[order_name]',
        'label' => 'order name',
        'rules' => 'trim|required'
    )
);
$config['prepare_karigar'] = array(
        array(
            'field' => 'quantity',
            'label' => 'Quantity',
            'rules' => 'trim|required|check_manufacturing_quantity'
        ),array(
            'field' => 'mop_id',
            'label' => 'Order Id',
            'rules' => 'trim|required'
        ),array(
            'field' => 'karigar_id',
            'label' => 'karigar',
            'rules' => 'trim|required'
        )
    );
$config['prepare_karigar_to_change'] = array(
      array(
            'field' => 'karigar_id',
            'label' => 'karigar',
            'rules' => 'trim|required'
        )
    );
$config['receive_order'] = array(
    array(
        'field' => 'kmm_quantity',
        'label' => 'Quantity',
        'rules' => 'trim|required|check_kmm_quantity|numeric'
    )
);

$config['gt_excel'] = array(
    array(
        'field' => 'challan_no',
        'label' => 'challan_no',
        'rules' => 'trim|required|is_unique[gt_excel.challan_no]',
        'errors' =>array(
            'is_unique'=>"This challan no already exits",
            )
    )
    
);

$config['hallmarking_center'] = array(
    array(
        'field' => 'hallmarking_center[name]',
        'label' => 'hallmarking Center name',
        'rules' => 'trim|required|check_hallmarkcenter',
        'errors' =>array(
            'check_hallmarkcenter'=>"This Center Name already exits",
            ),
    )
);

$config['parent_category'] = array(
    array(
        'field' => 'parent_category[name]',
        'label' => 'parent Category name',
        'rules' => 'trim|required|check_parent_category',
        'errors' =>array(
            'check_parent_category'=>"This Category already exits",
            ),
        ),
    array(
        'field' => 'product_code',
        'label' => 'Product code',
        'rules' => 'trim|required|check_parent_category_code',
        'errors' =>array(
            'check_parent_category_code'=>"This Code already exits",
            ),
        )
);

$config['product_category_rate'] = array(
    array(
        'field' => 'product_category[name]',
        'label' => 'product Category name',
        'rules' => 'trim|required|is_unique[product_category.name]',/*check_product_category*/
        'errors' =>array(
            'check_product_category'=>"This Product category already exits",
            ),
    ),
     array(
        'field' => 'product_category[percentage]',
        'label' => 'product Category',
        'rules' => 'trim|required',
       
    )
);
$config['product_category'] = array(
    array(
        'field' => 'product_category[name]',
        'label' => 'product Category name',
        'rules' => 'trim|required|is_unique[product_category.name]',/*check_product_category*/
        'errors' =>array(
            'check_product_category'=>"This Product category already exits",
            ),
    ),
     array(
        'field' => 'product_category[parent_category]',
        'label' => 'product Category',
        'rules' => 'trim|required',
       
    )
);

$config['product_category_edit'] = array(

     array(
        'field' => 'product_category[parent_category]',
        'label' => 'product Category',
        'rules' => 'trim|required',
       
    )
);

$config['hallmarking_list'] = array(
    array(
        'field' => 'hallmarking_list[hc_id]',
        'label' => 'Hallmarking Center',
        'rules' => 'trim|required',
    ),
    array(
        'field' => 'hallmarking_list[code]',
        'label' => 'Hallmarking Center Code',
        'rules' => 'trim|required',
    ),
    /*array(
        'field' => 'hallmarking_list[c_name]',
        'label' => 'Company name',
        'rules' => 'trim|required',
    ),
    array(
        'field' => 'hallmarking_list[owner]',
        'label' => 'Owner name',
        'rules' => 'trim|required',
    ),
    array(
        'field' => 'hallmarking_list[ap_name]',
        'label' => 'Authorized Person name',
        'rules' => 'trim|required',
    ),  
    array(
        'field' => 'hallmarking_list[address]',
        'label' => 'Address',
        'rules' => 'trim|required',
    ),
    array(
        'field' => 'hallmarking_list[city]',
        'label' => 'City',
        'rules' => 'trim|required',
    ),
    array(
        'field' => 'hallmarking_list[state]',
        'label' => 'State',
        'rules' => 'trim|required',
    ),*/
    array(
        'field' => 'hallmarking_list[email]',
        'label' => 'Email Id',
        'rules' => 'trim|valid_email',
    ),
    array(
        'field' => 'hallmarking_list[mobile_no]',
        'label' => 'Mobile Number',
        'rules' => 'trim|numeric|exact_length[10]',
         'errors'=>array(
            'numeric' => "Please Enter Valid Mobile Number", 
            'exact_length' => "Mobile Number should contain 10 digits"),
    ),
    array(
        'field' => 'hallmarking_list[phone_no]',
        'label' => 'Phone Number',
        'rules' => 'trim|numeric',
        'errors'=>array(
            'numeric' => "Please Enter Valid Phone Number",),
    ),
    array(
        'field' => 'hallmarking_list[icom_no]',
        'label' => 'Icom Number',
        'rules' => 'trim|numeric',
        'errors'=>array(
            'numeric' => "Please Enter Valid Icom Number",),
    ),
     array(
        'field' => 'hallmarking_list[limit]',
        'label' => 'Limit',
        'rules' => 'trim|numeric',
        'errors'=>array(
            'numeric' => "Please Enter Numbers",),
    ),
);

$config['Sales_under'] = array(
    array(
        'field' => 'Sales_under[name]',
        'label' => 'Sales Under Name',
        'rules' => 'trim|required|check_Sale_under_exist'
    )
);
$config['tag_products'] = array(
    array(
        'field' => 'karigar_id',
        'label' => 'karigar id',
        'rules' => 'trim|required'
    ),
    array(
        'field' => 'sub_category_id',
        'label' => 'Product',
        'rules' => 'trim|required|check_product_available'
    ),
    array(
        'field' => 'gr_wt',
        'label' => 'Gross Weight',
        'rules' => 'trim|required|greater_than_equal_to[0.1]'
    ),
    array(
        'field' => 'net_wt',
        'label' => 'Net Weight',
        'rules' => 'trim|required|greater_than_equal_to[0.1]'
    ),
    array(
        'field' => 'Quantity',
        'label' => 'Quantity',
        'rules' => 'trim|required'
    ),
    array(
        'field' => 'is_sub_cat',
        'label' => 'subcategory',
        'rules' => 'trim|required'
    )
);
$config['karigar_receive_order'] = array(
    array(
        'field' => 'kariger_id',
        'label' => 'karigar id',
        'rules' => 'trim|required'
    ),
    array(
        'field' => 'gross_wt',
        'label' => 'Gross Weight',
        'rules' => 'trim|required'
    ),
    array(
        'field' => 'receive_wt',
        'label' => 'Recived Weight',
        'rules' => 'trim|required'
    ),
    array(
        'field' => 'net_wt',
        'label' => 'Net Weight',
        'rules' => 'trim|required'
    ),
    array(
        'field' => 'few_wt',
        'label' => 'Few Weight',
        'rules' => 'trim|required'
    ),
    array(
        'field' => 'mina_wt',
        'label' => 'Mina Weight',
        'rules' => 'trim|required'
    ),
    array(
        'field' => 'kundan_wt',
        'label' => 'Kundan Weight',
        'rules' => 'trim|required'
    ),
    array(
        'field' => 'other_wt',
        'label' => 'Other Weight',
        'rules' => 'trim|required'
    ),
    array(
        'field' => 'stone_wt',
        'label' => 'Stone Weight',
        'rules' => 'trim|required'
    ),
    array(
        'field' => 'stone_amt',
        'label' => 'Stone Amount',
        'rules' => 'trim|required'
    ),
    array(
        'field' => 'kundan_amt',
        'label' => 'Kundan Amount',
        'rules' => 'trim|required'
    )
);


$config['sales_prepare_order'] = array(
     array(
        'field' => 'product[order_name]',
        'label' => 'Order Name',
        'rules' => 'trim|required',
    ),array(
        'field' => 'product[department_id]',
        'label' => 'Department',
        'rules' => 'trim|required',
        'errors'=>array('required'=>"Please Select Department"),
    ),array(
        'field' => 'product[weight]',
        'label' => 'Weight Range',
        'rules' => 'trim|required|check_pc_wr',
        'errors'=>array('required'=>"Please Select Weight Range",
                        'check_pc_wr'=>"This Category And Weight Range already exits in above rows"),

    ),
    array(
        'field' => 'product[quantity]',
        'label' => 'Quantity',
        'rules' => 'trim|required|is_natural_no_zero',
        'errors'=> array('is_natural_no_zero' => "The Quantity field must contain only numbers."),
    ),
    array(
        'field' => 'product[parent_category]',
        'label' => 'Parent Category',
        'rules' => 'trim|required',
        'errors'=>array('required'=>"Please Select Parent Category"),
    )
);

$config['accounting'] = array(
    array(
        'field' => 'accounting[weight]',
        'label' => 'Weight',
        'rules' => 'trim|required|numeric|greater_than_equal_to[0.1]',
        'errors'=> array('numeric'=>"The Quantity field must contain only numbers",
                        'greater_than_equal_to' => "The Weight Should be greater than 0."),
    ),array(
        'field' => 'accounting[metal_id]',
        'label' => 'Metal',
        'rules' => 'trim|required'
    ),
    array(
        'field' => 'accounting[karigar_customer_id]',
        'label' => 'Karigar',
        'rules' => 'trim|required'
    ),
    array(
        'field' => 'accounting[type]',
        'label' => 'Karigar',
        'rules' => 'trim|required',
        'errors'=>array(
            'required'=>'Please Select Type')
    ),
    array(
        'field' => 'accounting[purity]',
        'label' => 'Purity',
        'rules' => 'trim|required|numeric|greater_than_equal_to[0.1]',
        'errors'=> array('numeric'=>"The Quantity field must contain only numbers",
                        'greater_than_equal_to' => "The Weight Should be greater than 0."),
    )
);


$config['color_stone'] = array(
    array(
        'field' => 'color_stone[name]',
        'label' => 'Name',
        'rules' => 'trim|required|check_color_stone',
        'errors' =>array(
            'check_color_stone'=>"This Color Stone already exits",
            ),
    )
);

$config['kundan_category'] = array(
    array(
        'field' => 'kundan_category[name]',
        'label' => 'Name',
        'rules' => 'trim|required|check_kundan_category',
        'errors' =>array(
            'check_kundan_category'=>"This Kundan Category already exits",
            ),
    )
);
$config['Kundan_receive_products'] = array(
/*    array(
        'field' => 'quantity',
        'label' => 'Quantity',
        'rules' => 'trim|required|greater_than_equal_to[0.1]',
        'errors' =>array(
            'greater_than'=>"Quantity should be greater than 0.1",
            ),
        ),*/
    array(
        'field' => 'kundan_pc',
        'label' => 'kundan_pc',
        'rules' => 'trim|required|greater_than_equal_to[0.1]',
        'errors' =>array(
            'greater_than'=>"K Pcs should be greater than 0.1",
            ),  
        ),      
    array(
            'field' => 'k_gold',
            'label' => 'k_gold',
            'rules' => 'trim|required|greater_than_equal_to[0.1]',
            'errors' =>array(
                'greater_than'=>"K Gold should be greater than 0.1",
            ),
        ),
    array(
            'field' => 'kundan_rate',
            'label' => 'kundan_rate',
            'rules' => 'trim|required|greater_than_equal_to[0.1]',
            'errors' =>array(
                'greater_than'=>"K @ should be greater than 0.1",
            ),
        ),    
    array(
            'field' => 'kundan_amt',
            'label' => 'kundan_amt',
            'rules' => 'trim|required|greater_than_equal_to[0.1]',
            'errors' =>array(
                'greater_than'=>"K amount should be greater than 0.1",
            ),
        ),    
    array(
        'field' => 'net_wt',
        'label' => 'net_wt',
        'rules' => 'trim|required|greater_than_equal_to[0.1]',
        'errors' =>array(
            'greater_than'=>"Net Wt should be greater than 0.1",
        ), 

    ),
);
$config['type_master'] = array(
    array(
        'field' => 'type_master[name]',
        'label' => 'Name',
        'rules' => 'trim|required|check_type_master',
        'errors' =>array(
            'check_type_master'=>"This Type already exits",
            ),
    )
);

$config['customer'] = array(
    array(
        'field' => 'customer[name]',
        'label' => 'Name',
        'rules' => 'trim|required',
    ),
    //  array(
    //     'field' => 'customer[contact_person]',
    //     'label' => 'Contact Person',
    //     'rules' => 'trim|required',
    // ),
    //   array(
    //     'field' => 'customer[mobile_no]',
    //     'label' => 'Mobile No',
    //     'rules' => 'trim|required|exact_length[10]|numeric',
    //     'errors' =>array(
    //         'exact_length'=>"Please Enter Valid Mobile Number",
    //         'numeric'=>"Please Enter Digits not characters",
    //         ),
    // ),
    array(
        'field' => 'customer[address]',
        'label' => 'Address',
        'rules' => 'trim|required',
    ),
    array(
        'field' => 'customer[corporate_id]',
        'label' => 'corporate_id',
        'rules' => 'trim|required',
        'errors' =>array('required'=>"Please Select Corporate"),
    ),
    array(
        'field' => 'customer[customer_type_id]',
        'label' => 'customer_type_id',
        'rules' => 'trim|required',
        'errors' =>array('required'=>"Please Select Customer Type"),
    ),
    //  array(
    //     'field' => 'customer[hc_id]',
    //     'label' => 'hc_id',
    //     'rules' => 'trim|required',
    //     'errors' =>array('required'=>"Please Select Hallmarking Center"),
    // ),
    //   array(
    //     'field' => 'customer[refer_by]',
    //     'label' => 'Reference Name',
    //     'rules' => 'trim|required',
        
    // ),
    array(
        'field' => 'customer[area]',
        'label' => 'area',
        'rules' => 'trim|required',
    ),
    array(
        'field' => 'customer[city_id]',
        'label' => 'city_id',
        'rules' => 'trim|required',
        'errors' =>array('required'=>"Please Select City"),
    ),
    array(
        'field' => 'customer[state_id]',
        'label' => 'state_id',
        'rules' => 'trim|required',
        'errors' =>array('required'=>"Please Select State"),
    ),
    // array(
    //     'field' => 'customer[limit_amt]',
    //     'label' => 'limit amt',
    //     'rules' => 'trim|required|numeric',
    // ),
    // array(
    //     'field' => 'customer[limit_metal]',
    //     'label' => 'limit metal',
    //     'rules' => 'trim|required|numeric',
    // ),
    // array(
    //     'field' => 'customer[days]',
    //     'label' => 'days',
    //     'rules' => 'trim|required|is_natural_no_zero',
    //     'errors'=> array('is_natural_no_zero' =>"Please Enter Numbers Only"),
    // ),
    // array(
    //     'field' => 'customer[sales_under]',
    //     'label' => 'sales under',
    //     'rules' => 'trim|required',
    //     'errors' =>array('required'=>"Please Select Sales Under"),
    // ),
    // array(
    //     'field' => 'customer[terms]',
    //     'label' => 'terms',
    //     'rules' => 'trim|required',
    // ),
    // array(
    //     'field' => 'customer[bank_ac]',
    //     'label' => 'Bank Account Number',
    //     'rules' => 'trim|required|is_natural_no_zero',
    //     'errors' =>array('is_natural_no_zero'=>"Please Select Valid Bank Account Number"),
    // ),
    // array(
    //     'field' => 'customer[bank_ifsc]',
    //     'label' => 'Bank IFSC code',
    //     'rules' => 'trim|required',
    // ),
    // array(
    //     'field' => 'customer[bank_name]',
    //     'label' => 'Bank Name',
    //     'rules' => 'trim|required',
    // ),
    // array(
    //     'field' => 'customer[bank_branch]',
    //     'label' => 'Branch Name',
    //     'rules' => 'trim|required',
    // ),
);

$config['sales_invoice'] = array(
    array(
        'field' => 'invoice_details[transaction_date]',
        'label' => 'Transaction date',
        'rules' => 'trim|required',
        'errors'=>array('required' => 'Please select Transaction date'),
    ),
     array(
        'field' => 'invoice_details[customer_id]',
        'label' => 'Customer',
        'rules' => 'trim|required',
        'errors'=>array('required' => 'Please select Customer'),
    ),
      array(
        'field' => 'invoice_details[color_stone_id]',
        'label' => 'Color Stone',
        'rules' => 'trim|required',
        'errors'=>array('required' => 'Please select Color Stone'),
    ),
    array(
        'field' => 'invoice_details[kundan_category_id]',
        'label' => 'kundan Category',
        'rules' => 'trim|required',
        'errors'=>array('required' => 'Please select Kundan Catergory'),
    ),
    /*details
    array(
        'field' => 'invoice[type][]',
        'label' => 'Type',
        'rules' => 'trim|required',
        'errors'=>array('required' => 'Please select Type'),
    ),
    array(
        'field' => 'invoice[item_no][]',
        'label' => 'Item no',
        'rules' => 'trim|required',
    ),
    array(
        'field' => 'invoice[cs_wt][]',
        'label' => 'Color stone weight',
        'rules' => 'trim|required',
    ),
    array(
        'field' => 'invoice[kundan_wt][]',
        'label' => 'kundan Weight',
        'rules' => 'trim|required',
    ),
    array(
        'field' => 'invoice[cs_amt][]',
        'label' => 'color stone amount',
        'rules' => 'trim|required',
    ),
    array(
        'field' => 'invoice[kundan_amt][]',
        'label' => 'Kundan Amount',
        'rules' => 'trim|required',
    ),

    array(
        'field' => 'invoice[rodo][]',
        'label' => 'rodo',
        'rules' => 'trim|required',
    ),
    array(
        'field' => 'invoice[radi][]',
        'label' => 'radi',
        'rules' => 'trim|required',
    ),
    array(
        'field' => 'invoice[other][]',
        'label' => 'other',
        'rules' => 'trim|required',
    ),*/
);


$config['dispatch_hallmarking'] = array(
    array(
        'field' => 'hallmarking[hc_id]',
        'label' => 'Name',
        'rules' => 'trim|required',
        'errors' =>array(
            'required'=>"Please Select Hallmarking Center",
            ),
    ),
    array(
        'field' => 'hallmarking[hc_logo]',
        'label' => 'Hallmarking Logo',
        'rules' => 'trim|required',
    ),
    array(
        'field' => 'hallmarking[total_gr_wt]',
        'label' => 'Gross Weight',
        'rules' => 'trim|required|numeric|greater_than_equal_to[0.1]',
        'errors' =>array(
            'numeric'=>"Gross Weight should be Numeric",
            'greater_than_equal_to'=>"Gross Weight Should greater than Zero",
            ),
    ),
    array(
        'field' => 'hallmarking[person_name]',
        'label' => 'Person Name',
        'rules' => 'trim|required',
    ),
    array(
        'field' => 'hallmarking[quantity]',
        'label' => 'Quantity',
        'rules' => 'trim|required|greater_than_equal_to[1]|check_hallmarking_dispatch_qty',
        'errors' =>array(
            'check_hallmarking_dispatch_qty'=>"Quantity Exceeds",
            'greater_than_equal_to'=>"Quantity Should greater than Zero",
            ),
    ),
);

$config['dispatch_hallmarking_recieve'] = array(
   
   
    array(
        'field' => 'hallmarking[total_gr_wt]',
        'label' => 'Gross Weight',
        'rules' => 'trim|required|numeric|greater_than_equal_to[0.1]',
        'errors' =>array(
            'numeric'=>"Gross Weight should be Numeric",
            'greater_than_equal_to'=>"Gross Weight Should greater than Zero",
            ),
    ),
    array(
        'field' => 'hallmarking[quantity]',
        'label' => 'Quantity',
        'rules' => 'trim|required|greater_than_equal_to[1]|check_hallmarking_received_qty',
        'errors' =>array(
            'check_hallmarking_received_qty'=>"Quantity Exceeds",
            'greater_than_equal_to'=>"Quantity Should greater than Zero",
            ),
    ),
    array(
        'field' => 'hallmarking[amount]',
        'label' => 'Amount',
        'rules' => 'trim|required|greater_than_equal_to[1]',
        'errors' =>array(
            'greater_than_equal_to'=>"Amount Should greater than Zero",
            ),
    ),
);

$config['dispatch_mode'] = array(
   
    array(
        'field' => 'dispatch_mode[mode_name]',
        'label' => 'name',
        'rules' => 'trim|required',
    ),
    array(
        'field' => 'dispatch_mode[mode_type]',
        'label' => 'mode type',
        'rules' => 'trim|required',
        'errors'=> array('required' =>'Please Select Mode Type' ),
    ),
    
);

$config['customer_type'] = array(
    array(
        'field' => 'customer_type[name]',
        'label' => 'Name',
        'rules' => 'trim|required|check_exits_customer_type',
        'errors'=>array('check_exits_customer_type' => "This Type Already exits"),
    ),
);

$config['Salesman_master'] = array(
    array(
        'field' => 'salesmans[name]',
        'label' => 'Name',
        'rules' => 'trim|required',
    ),
    array(
        'field' => 'salesmans[mobile_no]',
        'label' => 'Mobile No',
        'rules' => 'trim|required|numeric|exact_length[10]|check_mob_no_saleman',
        'errors'=> array('numeric' =>"Please Enter Digit from 0-9",
                        'exact_length' =>"Mobile Number should be in 10 digits",
                        "check_mob_no_saleman"=>"This Mobile No already Exits"),
    ),
    array(
        'field' => 'salesmans[address]',
        'label' => 'Address',
        'rules' => 'trim|required',
    ),
    array(
        'field' => 'salesmans[state_id]',
        'label' => 'state_id',
        'rules' => 'trim|required',
        'errors'=> array('required' =>"Please Select State"),
    ),
    array(
        'field' => 'salesmans[city_id]',
        'label' => 'city_id',
        'rules' => 'trim|required',
        'errors'=> array('required' =>"Please Select City"),
    ),
);
$config['validate_rejected_data'] = array(
    array(
        'field' => 'quantity',
        'label' => 'Quantity',
        'rules' => 'trim|required|numeric|greater_than_equal_to[0.1]|check_qc_quantity'
    ),array(
        'field' => 'weight',
        'label' => 'weight',
        'rules' => 'trim|required|numeric|greater_than_equal_to[0.1]'
    )
);
$config['validate_rejected_bom_data'] = array(
    array(
        'field' => 'weight',
        'label' => 'Weight',
        'rules' => 'trim|required|numeric|greater_than_equal_to[0.1]|check_qc_weight'
    ),
);

$config['issue_voucher'] = array(
     array(
        'field' => 'voucher[party_name]',
        'label' => 'Party Name',
        'rules' => 'trim|required',
        'errors'=>array('required'=>"Please Select Party Name"),
    ),
    array(
        'field' => 'voucher[party_type]',
        'label' => 'Party Type',
        'rules' => 'trim|required',
        'errors'=>array('required'=>"Please Select Party Type"),
    ),
    array(
        'field' => 'voucher[buying_complexity_id]',
        'label' => 'buying_complexity_id',
        'rules' => 'trim|required',
        'errors'=>array('required'=>"Please Select Item"),
    ),array(
        'field' => 'voucher[gr_wt]',
        'label' => 'Gross Weight',
        'rules' => 'trim|required|numeric|greater_than_equal_to[0.1]',
        'errors'=>array('numeric'=>"Please Enter Digits Only",
                        'greater_than_equal_to' => "Please Enter Value Greater Than 0"),
    ),
    array(
        'field' => 'voucher[net_wt]',
        'label' => 'Net Weight',
        'rules' => 'trim|required|numeric|greater_than_equal_to[0.1]',
        'errors'=>array('numeric'=>"Please Enter Digits Only",
                        'greater_than_equal_to' => "Please Enter Value Greater Than 0"),
    ),
    array(
        'field' => 'voucher[stone]',
        'label' => 'stone',
        'rules' => 'trim|required|numeric|greater_than_equal_to[0]',
        'errors'=>array('numeric'=>"Please Enter Digits Only",
                        'greater_than_equal_to' => "Please Enter Value Greater Than 0"),
    ),
    array(
        'field' => 'voucher[melting]',
        'label' => 'Melting',
        'rules' => 'trim|required|numeric|greater_than_equal_to[0.1]',
        'errors'=>array('numeric'=>"Please Enter Digits Only",
                        'greater_than_equal_to' => "Please Enter Value Greater Than 0"),
    ),
    array(
        'field' => 'voucher[touch]',
        'label' => 'Touch',
        'rules' => 'trim|required|numeric',
        'errors'=>array('numeric'=>"Please Enter Digits Only"),
    ),
    array(
        'field' => 'voucher[purity]',
        'label' => 'Purity',
        'rules' => 'trim|required|numeric',
        'errors'=>array('numeric'=>"Please Enter Digits Only"),
    ),
    array(
        'field' => 'voucher[amount]',
        'label' => 'Amount',
        'rules' => 'trim|required|numeric|greater_than_equal_to[0.1]',
        'errors'=>array('numeric'=>"Please Enter Digits Only",
                        'greater_than_equal_to' => "Please Enter Value Greater Than 0"),
    )
);


$config['cash_bank_voucher'] = array(
     array(
        'field' => 'cash_bank[party_name]',
        'label' => 'Party Name',
        'rules' => 'trim|required',
        'errors'=>array('required'=>"Please Select Party Name"),
    ),array(
        'field' => 'cash_bank[party_type]',
        'label' => 'Party type',
        'rules' => 'trim|required',
        'errors'=>array('required'=>"Please Select Party Type"),
    ),array(
        'field' => 'cash_bank[bank_name]',
        'label' => 'Bank Name',
        'rules' => 'trim|required',
        
    ),array(
        'field' => 'cash_bank[amount]',
        'label' => 'Amount',
        'rules' => 'trim|required|numeric|greater_than_equal_to[0.1]',
        'errors'=>array('numeric'=>"Please Enter Valid Amount",
                        'greater_than_equal_to'=>'Please Enter Amount Greater Than 0'),
    ),
);

$config['metal_issue_voucher'] = array(
     array(
        'field' => 'metal_issue[party_name]',
        'label' => 'Party Name',
        'rules' => 'trim|required',
        'errors'=>array('required'=>"Please Select Party Name"),

    ),array(
        'field' => 'metal_issue[party_type]',
        'label' => 'Party Name',
        'rules' => 'trim|required',
        'errors'=>array('required'=>"Please Select Party Type"),

    ),array(
        'field' => 'metal_issue[material_id]',
        'label' => 'Item',
        'rules' => 'trim|required',
        'errors'=>array('required'=>"Please Select Item"),
        
    ),array(
        'field' => 'metal_issue[weight]',
        'label' => 'Weight',
        'rules' => 'trim|required|numeric|greater_than_equal_to[0.1]',
        'errors'=>array('numeric'=>"Please Enter Valid Amount",
                        'greater_than_equal_to'=>'Please Enter Amount Greater Than 0'),
    ),array(
        'field' => 'metal_issue[melting]',
        'label' => 'Melting',
        'rules' => 'trim|required|numeric|greater_than_equal_to[0.1]',
        'errors'=>array('numeric'=>"Please Enter Valid Amount",
                        'greater_than_equal_to'=>'Please Enter Amount Greater Than 0'),
    ),array(
        'field' => 'metal_issue[purity]',
        'label' => 'Purity',
        'rules' => 'trim|required|numeric|greater_than_equal_to[0.1]',
        'errors'=>array('numeric'=>"Please Enter Valid Amount",
                        'greater_than_equal_to'=>'Please Enter Amount Greater Than 0'),
    )
);

$config['material_master'] = array(
     array(
        'field' => 'material[description]',
        'label' => 'Description',
        'rules' => 'trim|required',

    ),array(
        'field' => 'material[short_code]',
        'label' => 'Item',
        'rules' => 'trim|required|check_short_code',
        'errors'=>array('check_short_code'=>"This Short code Already Exits."),
        
    )
);

$config['Variations_category'] = array(
    array(
        'field' => 'Variations_category[type]',
        'label' => 'Variations category',
        'rules' => 'trim|required|numeric|check_Variations_category',
        'errors'=>array('numeric' =>"Please Enter Numeric value",
                        'check_Variations_category'=>"This Variations category Already Exits."),
        
    ),
     array(
        'field' => 'Variations_category[name]',
        'label' => 'Variations category',
        'rules' => 'trim|required|check_Variations_category',
        'errors'=>array('numeric' =>"Please Enter Numeric value",
                        'check_Variations_category'=>"This Variations category Already Exits."),
        
    )
);

$config['accounting_details'] = array(
    array(
        'field' => 'accounting[karigar_customer_id]',
        'label' => 'name',
        'rules' => 'trim|required',
        'errors'=>array('required' =>"Please Select Name"),
        
    ),
     array(
        'field' => 'accounting[type]',
        'label' => 'type',
        'rules' => 'trim|required',
        'errors'=>array('required' =>"Please Select Type"),
        
    ),
    array(
        'field' => 'accounting[rate_type]',
        'label' => 'Rate type',
        'rules' => 'trim|required',
        'errors'=>array('required' =>"Please Choose Rate Type"),
        
    ),
    array(
        'field' => 'accounting[payment_type]',
        'label' => 'Payment type',
        'rules' => 'trim|required',
        'errors'=>array('required' =>"Please Select Payment Type"),
        
    ),
    // array(
    //     'field' => 'accounting[rate]',
    //     'label' => 'Rate',
    //     'rules' => 'trim|required|numeric|greater_than_equal_to[0.001]',
    //     'errors'=>array('numeric' =>"Please Enter Numeric value",
    //                     'greater_than_equal_to'=>"Please Enter Value greater than 0"),   
    // ),
    array(
        'field' => 'ac_details[category_id]',
        'label' => 'Category Id',
        'rules' => 'trim|required|numeric|greater_than_equal_to[0.001]',
        'errors'=>array('numeric' =>"Please Enter Numeric value",
                        'greater_than_equal_to'=>"Please Enter Value greater than 0"),
        
    ),
    array(
        'field' => 'ac_details[gr_wt]',
        'label' => 'Gross Weight',
        'rules' => 'trim|required|numeric|greater_than_equal_to[0.001]',
        'errors'=>array('numeric' =>"Please Enter Numeric value",
                        'greater_than_equal_to'=>"Please Enter Value greater than 0"),
        
    ),
    // array(
    //     'field' => 'ac_details[stone]',
    //     'label' => 'Stone',
    //     'rules' => 'trim|required|numeric|greater_than_equal_to[0]',
    //     'errors'=>array('numeric' =>"Please Enter Numeric value",
    //                     'greater_than_equal_to'=>"Please Enter Value greater than 0"),
        
    // ),
    array(
        'field' => 'ac_details[net_wt]',
        'label' => 'Net Weight',
        'rules' => 'trim|required|numeric|greater_than_equal_to[0.001]',
        'errors'=>array('numeric' =>"Please Enter Numeric value",
                        'greater_than_equal_to'=>"Please Enter Value greater than 0"),
        
    ),
    array(
        'field' => 'ac_details[melting]',
        'label' => 'Melting',
        'rules' => 'trim|required|numeric|greater_than_equal_to[0.001]',
        'errors'=>array('numeric' =>"Please Enter Numeric value",
                        'greater_than_equal_to'=>"Please Enter Value greater than 0"),
        
    ),
    // array(
    //     'field' => 'ac_details[touch]',
    //     'label' => 'Touch',
    //     'rules' => 'trim|required|numeric|greater_than_equal_to[0.001]',
    //     'errors'=>array('numeric' =>"Please Enter Numeric value",
    //                     'greater_than_equal_to'=>"Please Enter Value greater than 0"),
        
    // ),
    array(
        'field' => 'ac_details[purity]',
        'label' => 'Pure',
        'rules' => 'trim|required|numeric|greater_than_equal_to[0.001]',
        'errors'=>array('numeric' =>"Please Enter Numeric value",
                        'greater_than_equal_to'=>"Please Enter Value greater than 0"),
        
    ),
    // array(
    //     'field' => 'ac_details[gold]',
    //     'label' => 'Gold',
    //     'rules' => 'trim|required|numeric|greater_than_equal_to[0.001]',
    //     'errors'=>array('numeric' =>"Please Enter Numeric value",
    //                     'greater_than_equal_to'=>"Please Enter Value greater than 0"),
        
    // ),
    // array(
    //     'field' => 'ac_details[gold_amt]',
    //     'label' => 'Gold Amount',
    //     'rules' => 'trim|required|numeric|greater_than_equal_to[0.001]',
    //     'errors'=>array('numeric' =>"Please Enter Numeric value",
    //                     'greater_than_equal_to'=>"Please Enter Value greater than 0"),
        
    // ),
    // array(
    //     'field' => 'ac_details[lab]',
    //     'label' => 'Lab',
    //     'rules' => 'trim|required|numeric|greater_than_equal_to[0.001]',
    //     'errors'=>array('numeric' =>"Please Enter Numeric value",
    //                     'greater_than_equal_to'=>"Please Enter Value greater than 0"),
        
    // ),
    // array(
    //     'field' => 'ac_details[lab_amt]',
    //     'label' => 'Lab Amount',
    //     'rules' => 'trim|required|numeric|greater_than_equal_to[0.001]',
    //     'errors'=>array('numeric' =>"Please Enter Numeric value",
    //                     'greater_than_equal_to'=>"Please Enter Value greater than 0"),
        
    // ),
    //  array(
    //     'field' => 'ac_details[total_amt]',
    //     'label' => 'Total Amount',
    //     'rules' => 'trim|required|numeric|greater_than_equal_to[0.001]',
    //     'errors'=>array('numeric' =>"Please Enter Numeric value",
    //                     'greater_than_equal_to'=>"Please Enter Value greater than 0"),
        
    // ),
    //  array(
    //     'field' => 'gst',
    //     'label' => 'GST',
    //     'rules' => 'trim|required|numeric|greater_than_equal_to[0]',
    //     'errors'=>array('numeric' =>"Please Enter Numeric value",
    //                     'greater_than_equal_to'=>"Please Enter Value greater than 0"),
        
    // ),
    
);

$config['terms_master'] = array(
     array(
        'field' => 'terms[parent_category_id]',
        'label' => 'parent Category ',
        'rules' => 'trim|required|check_term_exists',
         'errors'=>array('required'=>"Please Select Parent Category.",
                        'check_term_exists'=>"This Category Already Exists"),
    ),
    //  array(
    //     'field' => 'terms[code]',
    //     'label' => 'Code',
    //     'rules' => 'trim|required', 
    // ),
     array(
        'field' => 'terms[hard_cash]',
        'label' => 'Hard Cash',
        'rules' => 'trim|required|numeric|greater_than_equal_to[0.1]',
        'errors'=>array('numeric'=>"Please Enter Numeric Value.",
                        'greater_than_equal_to'=>"Please Enter Value Greater Than Zero(0)."),

    ),
    array(
        'field' => 'terms[normal]',
        'label' => 'Normal',
        'rules' => 'trim|required|numeric|greater_than_equal_to[0.1]',
        'errors'=>array('numeric'=>"Please Enter Numeric Value.",
                        'greater_than_equal_to'=>"Please Enter Value Greater Than Zero(0)."),
        
    ),
    array(
        'field' => 'terms[7days]',
        'label' => '7 Days',
        'rules' => 'trim|required|numeric|greater_than_equal_to[0.1]',
        'errors'=>array('numeric'=>"Please Enter Numeric Value.",
                        'greater_than_equal_to'=>"Please Enter Value Greater Than Zero(0)."),
        
    ),
    array(
        'field' => 'terms[15days]',
        'label' => '15 Days',
        'rules' => 'trim|required|numeric|greater_than_equal_to[0.1]',
        'errors'=>array('numeric'=>"Please Enter Numeric Value.",
                        'greater_than_equal_to'=>"Please Enter Value Greater Than Zero(0)."),
        
    ),
    array(
        'field' => 'terms[1month]',
        'label' => '1 Month',
        'rules' => 'trim|required|numeric|greater_than_equal_to[0.1]',
        'errors'=>array('numeric'=>"Please Enter Numeric Value.",
                        'greater_than_equal_to'=>"Please Enter Value Greater Than Zero(0)."),
        
    ),
    array(
        'field' => 'terms[2month]',
        'label' => '2 Month',
        'rules' => 'trim|required|numeric|greater_than_equal_to[0.1]',
        'errors'=>array('numeric'=>"Please Enter Numeric Value.",
                        'greater_than_equal_to'=>"Please Enter Value Greater Than Zero(0)."),
        
    )
);

$config['customer_order'] = array(
    //  array(
    //     'field' => 'customer_order[customer_id]',
    //     'label' => 'parent Category ',
    //     'rules' => 'trim|required',
    //      'errors'=>array('required'=>"Please Select Party."),
    // ),
      array(
        'field' => 'customer_order[department_id]',
        'label' => 'Department ',
        'rules' => 'trim|required',
         'errors'=>array('required'=>"Please Select Department."),
    ),
   array(
        'field' => 'customer_order[party_name]',
        'label' => 'Party Name',
        'rules' => 'trim|required',
        // 'errors'=>array('required'=>"Please Select Party Name."),
    ),
    array(
        'field' => 'customer_order[company_name]',
        'label' => 'Company Name ',
        'rules' => 'trim|required',
         //'errors'=>array('required'=>"Please Select Department."),
    ),
   array(
        'field' => 'customer_order[mobile_no]',
        'label' => 'Mobile No ',
        'rules' => 'trim|required|numeric|exact_length[10]|check_customer_mobile_exists',
        'errors'=>array('numeric'=>"Please Enter Digits characters Not allowed",
                        'exact_length'=>"Please Enter 10 Digits Mobile Number"),
    ),
  /*  array(
        'field' => 'customer_order[email_id]',
        'label' => 'Email Id ',
        'rules' => 'trim|required|valid_email|check_customer_email_exists',
         'errors'=>array('valid_email'=>"Please Enter Valid Email Id."),
    ),*/
    //  array(
    //     'field' => 'customer_order[karigar_id]',
    //     'label' => 'karigar ',
    //     'rules' => 'trim|required',
    //      'errors'=>array('required'=>"Please Select Karigar."),
    // ),
/*     array(
        'field' => 'customer_order[order_name]',
        'label' => 'Order Name',
        'rules' => 'trim|required',

    ),*/
    array(
        'field' => 'customer_order[order_date]',
        'label' => 'Order Date',
        'rules' => 'trim|required',
        'errors'=>array('required'=>"Please Select Order Date"),
        
    ),
    array(
        'field' => 'customer_order[parent_category_id]',
        'label' => 'Parent Category',
        'rules' => 'trim|required',
        //'errors'=>array('check_parent_category_id_code'=>"This Parent Category Not Exits"),
        
    ),
    array(
        'field' => 'customer_order[weight_range_id]',
        'label' => 'Weight Range',
        'rules' => 'trim|required',
        // 'rules' => 'trim|required|numeric|greater_than_equal_to[0.1]',
        // 'errors'=>array('greater_than_equal_to[0.1]'=>'Net Wt Should be Greater Than 0.1',
        //                 'numeric'=>'Please Enter Valid Weights.')

        
    ),
    array(
        'field' => 'customer_order[quantity]',
        'label' => 'Quantity',
        'rules' => 'trim|required|regex_match[/^[A-Za-z0-9_ ]*$/]',
        'errors'=>array('regex_match'=>"Please Enter Valid Quantity",
                        /*'is_natural_no_zero'=>"Please Enter Valid Quantity"*/),
        
    ),
    // array(
    //     'field' => 'customer_order[size]',
    //     'label' => 'Size',
    //     'rules' => 'trim|required|numeric|greater_than_equal_to[0.1]',
    //     'errors'=>array('numeric'=>"Please Enter Numeric Value.",
    //                     'greater_than_equal_to'=>"Please Enter Greater Than 0"),
    // ),
    // array(
    //     'field' => 'customer_order[length]',
    //     'label' => 'Length',
    //     'rules' => 'trim|required|numeric|greater_than_equal_to[0.1]',
    //     'errors'=>array('numeric'=>"Please Enter Numeric Value.",
    //                     'greater_than_equal_to'=>"Please Enter Greater Than 0"),
    // ),
    array(
        'field' => 'customer_order[delievery_date]',
        'label' => 'delievery_date',
        'rules' => 'trim|required',
        'errors'=>array('required'=>"Please Select Delivery Date."),
    ),
    // array(
    //     'field' => 'customer_order[purity]',
    //     'label' => 'Purity',
    //     'rules' => 'trim|required|numeric',
    //     'errors'=>array('numeric'=>"Please Enter Numeric Value."),
    // ),
    array(
        'field' => 'customer_order[expected_date]',
        'label' => 'expected_date',
        'rules' => 'trim|required',
        'errors'=>array('required'=>"Please Select Expected Date."),
    ),

    // array(
    //     'field' => 'customer_order[weight_tolerance]',
    //     'label' => 'Weight Tolerance',
    //     'rules' => 'trim|required|numeric',
    //     'errors'=>array('numeric'=>"Please Enter Numeric Value."),
    // ),
    // array(
    //     'field' => 'customer_order[remark]',
    //     'label' => 'Remark',
    //     'rules' => 'trim|required',
    // ),
    array(
        'field' => 'customer_order[k_name]',
        'label' => 'Karigar Name',
        'rules' => 'trim|required',
    ),
    /*array(
        'field' => 'customer_order[k_code]',
        'label' => 'Karigar Code',
        'rules' => 'trim|required',
    ),*/
/*    array(
        'field' => 'customer_order[k_mob_no]',
        'label' => 'Mobile No',
        'rules' => 'trim|required|numeric|exact_length[10]',
    ),*/
   /* array(
        'field' => 'customer_order[k_email]',
        'label' => 'Email',
        'rules' => 'trim|required|valid_email',
    )*/
    // array(
    //     'field' => 'customer_order[weight_tolerance]',
    //     'label' => 'Weight Tolerance',
    //     'rules' => 'trim|required|numeric',
    //     'errors'=>array('numeric'=>"Please Enter Numeric Value."),
    // ),
    // array(
    //     'field' => 'customer_order[remark]',
    //     'label' => 'Remark',
    //     'rules' => 'trim|required',
    // )
);
$config['excel_order'] = array(

      array(
        'field' => 'customer_order[department_id]',
        'label' => 'Department ',
        'rules' => 'trim|required|check_department_code',
         'errors'=>array('required'=>"Please Enter Department.",'check_department_code'=>"This Department Not Exits."),
    ),

    array(
        'field' => 'customer_order[company_name]',
        'label' => 'Company Name ',
        'rules' => 'trim|required',
         //'errors'=>array('required'=>"Please Select Department."),
    ),

    array(
        'field' => 'customer_order[order_date]',
        'label' => 'Order Date',
        'rules' => 'trim|required',
        'errors'=>array('required'=>"Please Enter Order Date"),
        
    ),
    array(
        'field' => 'customer_order[mobile_no]',
        'label' => 'Mobile Number',
        'rules' => 'trim|required',
        'errors'=>array('required'=>"Please Enter Mobile Number"),
        
    ), 
      array(
        'field' => 'customer_order[party_name]',
        'label' => 'Customer Name',
        'rules' => 'trim|required',
        'errors'=>array('required'=>"Please Enter Customer Name"),
        
    ),
    array(
        'field' => 'customer_order[parent_category_id]',
        'label' => 'Parent Category',
        'rules' => 'trim|required|check_parent_category_id_code',
        'errors'=>array('check_parent_category_id_code'=>"This Parent Category Not Exits"),
        
    ),
    array(
        'field' => 'customer_order[weight_range_id]',
        'label' => 'Weight Range',
        'rules' => 'trim|required',
        // 'rules' => 'trim|required|numeric|greater_than_equal_to[0.1]',
        // 'errors'=>array('greater_than_equal_to[0.1]'=>'Net Wt Should be Greater Than 0.1',
        //                 'numeric'=>'Please Enter Valid Weights.')
        
    ),
    array(
        'field' => 'customer_order[quantity]',
        'label' => 'Quantity',
        'rules' => 'trim|required|numeric|is_natural_no_zero',
        'errors'=>array('numeric'=>"Please Enter Numeric Value.",
                        'is_natural_no_zero'=>"Please Enter Valid Quantity"),
        
    ),
   
    array(
        'field' => 'customer_order[delievery_date]',
        'label' => 'delievery_date',
        'rules' => 'trim|required|check_delivery_date',
        'errors'=>array('required'=>"Please Select Delivery Date."),
    ),
  
    array(
        'field' => 'customer_order[expected_date]',
        'label' => 'expected_date',
        'rules' => 'trim|required',
        'errors'=>array('required'=>"Please Select Expected Date."),
    ),


    array(
        'field' => 'customer_order[k_name]',
        'label' => 'Karigar Name',
        'rules' => 'trim|required',
    ),
    array(
        'field' => 'customer_order[k_code]',
        'label' => 'Karigar Code',
        'rules' => 'trim|required',
    ),
  
);


$config['customer_order_assign'] = array(
     array(
        'field' => 'karigar_id',
        'label' => 'Karigar ',
        'rules' => 'trim|required',
         'errors'=>array('required'=>"Please Select Karigar."),
    ),
     array(
        'field' => 'quantity',
        'label' => 'Quantity',
        'rules' => 'trim|required|is_natural_no_zero|check_customer_assign_qty',
        'errors'=>array('is_natural_no_zero'=>'Quantity Should be Greater Than 0',
                        'check_customer_assign_qty'=>'Quantity Exceeds')

    ),
);

$config['pending_to_be_rec_hm'] = array(
     array(
        'field' => 'check_data[weight]',
        'label' => 'Weight ',
        'rules' => 'trim|required|greater_than_equal_to[0.1]',
         'errors'=>array('greater_than_equal_to[0.1]'=>'Please Enter Valid Weight'),
    ),
     array(
        'field' => 'check_data[quantity]',
        'label' => 'Quantity',
        'rules' => 'trim|required|numeric|is_natural_no_zero',
        'errors'=>array('is_natural_no_zero'=>'Quantity Should be Greater Than 0',
                        'numeric'=>'Please Enter Numeric Value')

    ),
);

$config['Pending_to_be_recieved_model'] = array(
     array(
        'field' => 'karigar_id',
        'label' => 'karigar  ',
        'rules' => 'trim|required',
         'errors'=>array('required'=>'Please Select Karigar'),
    ), array(
        'field' => 'test_data[net_wt]',
        'label' => 'Weight ',
        'rules' => 'trim|required|numeric',
         'errors'=>array('numeric'=>'Please Enter Valid Weight'),
    ),
     array(
        'field' => 'test_data[quantity]',
        'label' => 'Quantity',
        'rules' => 'trim|required|numeric|is_natural_no_zero|check_received_qty',
        'errors'=>array('is_natural_no_zero'=>'Quantity Should be Greater Than 0',
                        'numeric'=>'Please Enter Numeric Value',
                        'check_received_qty'=>'Quantity Exceeds')

    ),
);
$config['receive_order_validation'] = array(
     array(
        'field' => 'qnt',
        'label' => 'Quantity',
        'rules' => 'trim|required|numeric|is_natural_no_zero|order_qnt_vald',
         'errors'=>array('is_natural_no_zero'=>'Quantity Should be Greater Than 0',
                        'numeric'=>'Please Enter Numeric Value',
                        'check_received_qty'=>'Quantity Exceeds')
    ),array(
        'field' => 'net_wt',
        'label' => 'Net Wt',
        'rules' => 'trim|numeric|required|greater_than_equal_to[0.1]',
        'errors'=>array('greater_than_equal_to[0.1]'=>'Net Wt Should be Greater Than 0.1',
                        'numeric'=>'Please Enter Numeric Value')
    ),array(
        'field' => 'category',
        'label' => 'Category',
        'rules' => 'trim|required',
    )
);

$config['parent_sub_category'] = array(
    array(
        'field' => 'product_category[name]',
        'label' => 'product Category name',
        'rules' => 'trim|required|check_product_category',
        'errors' =>array(
            'check_product_category'=>"This Product category already exits",
            ),
    ),
     array(
        'field' => 'product_category[parent_category]',
        'label' => 'parent Category name',
        'rules' => 'trim|required',
       
    )
);


$config['few_box'] = array(
    array(
        'field' => 'few_box[product]',
        'label' => 'Product',
        'rules' => 'trim|required|check_product_few_wt_exists'
    ),
    array(
        'field' => 'few_box[few_weight]',
        'label' => 'few weight',
        'rules' => 'trim|numeric|required|greater_than_equal_to[0.1]',
        'errors'=>array('greater_than_equal_to[0.1]'=>'Weight Should be Greater Than 0.1',
                        'numeric'=>'Please Enter Numeric Value')

    ),
);

$config['few_box_update'] = array(
    array(
        'field' => 'few_box[few_weight]',
        'label' => 'few weight',
        'rules' => 'trim|numeric|required|greater_than_equal_to[0.1]',
        'errors'=>array('greater_than_equal_to[0.1]'=>'Weight Should be Greater Than 0.1',
                        'numeric'=>'Please Enter Numeric Value')

    ),
);

$config['sales_stock'] = array(


    array(
        'field' => 'sales_stock[department_id]',
        'label' => 'Department ',
        'rules' => 'trim|required',
         'errors'=>array('required'=>"Please Select Department."),
    ),
    
    array(
        'field' => 'sales_stock[category_id]',
        'label' => 'Category',
        'rules' => 'trim|required',
        'errors'=>array('required'=>"Please Select  Category."),
        
    ),

    array(
        'field' => 'sales_stock[parent_category_id]',
        'label' => 'Product',
        'rules' => 'trim|required',
        'errors'=>array('required'=>"Please Select Product."),
        
    ),
     array(
        'field' => 'sales_stock[product_code]',
        'label' => 'Product code',
        'rules' => 'trim|required',
    ),
 /*   array(
        'field' => 'sales_stock[weight_range_id]',
        'label' => 'Weight Range',
        'rules' => 'trim|required',
        'errors'=>array('required'=>'Please select Weights.')
        
    ),*/
/*    array(
        'field' => 'sales_stock[weight]',
        'label' => 'Weight',
        'rules' => 'trim|required|greater_than_equal_to[0.1]',
        'errors'=>array('greater_than_equal_to[0.1]'=>'weight Should be Greater Than 0.1'
                       )
        
    ),*/
    array(
        'field' => 'sales_stock[quantity]',
        'label' => 'Quantity',
        'rules' => 'trim|required|numeric|is_natural_no_zero',
        'errors'=>array('numeric'=>"Please Enter Numeric Value.",
                        'is_natural_no_zero'=>"Please Enter Valid Quantity"),
        
    ),
    array(
        'field' => 'sales_stock[net_wt]',
        'label' => 'Net Wt',
        'rules' => 'trim|required|numeric|greater_than_equal_to[0.1]',
        'errors'=>array('greater_than_equal_to[0.1]'=>'Net weight Should be Greater Than 0.1',
                        'numeric'=>'Please Enter Valid Net Weight.')

    ),

   array(
        'field' => 'sales_stock[few_wt]',
        'label' => 'Few Wt',
        'rules' => 'trim|required|numeric|greater_than_equal_to[0.1]',
        'errors'=>array('greater_than_equal_to[0.1]'=>'Net weight Should be Greater Than 0.1',
                        'numeric'=>'Please Enter Valid Net Weight.')

    ),
     array(
        'field' => 'sales_stock[kundan]',
        'label' => 'kundan',
        'rules' => 'trim|required|numeric|greater_than_equal_to[0.1]',
        'errors'=>array('greater_than_equal_to[0.1]'=>'kundan Should be Greater Than 0.1',
                        'numeric'=>'Please Enter Valid kundan.')

    ),
/*
   array(
        'field' => 'sales_stock[meena_wt]',
        'label' => 'meena wt',
        'rules' => 'trim|required|numeric|greater_than_equal_to[0.1]',
        'errors'=>array('greater_than_equal_to[0.1]'=>'meena wt Should be Greater Than 0.1',
                        'numeric'=>'Please Enter Valid Meena wt.')

    ),*/



   array(
        'field' => 'sales_stock[wax_wt]',
        'label' => 'Wax Wt',
        'rules' => 'trim|required|numeric|greater_than_equal_to[0.1]',
        'errors'=>array('greater_than_equal_to[0.1]'=>'wax weight Should be Greater Than 0.1',
                        'numeric'=>'Please Enter Valid wax Weight.')

    ),
     array(
        'field' => 'sales_stock[kundan_pcs]',
        'label' => 'kundan pcs',
        'rules' => 'trim|required|numeric|greater_than_equal_to[0.1]',
        'errors'=>array('greater_than_equal_to[0.1]'=>'kundan pcs Should be Greater Than 0.1',
                        'numeric'=>'Please Enter Valid kundan pcs.')

    ),

/*    array(
        'field' => 'sales_stock[moti_wt]',
        'label' => 'moti wt',
        'rules' => 'trim|required|numeric|greater_than_equal_to[0.1]',
        'errors'=>array('greater_than_equal_to[0.1]'=>'moti wt Should be Greater Than 0.1',
                        'numeric'=>'Please Enter Valid moti wt.')

    ), */

    array(
        'field' => 'sales_stock[gr_wt]',
        'label' => 'gr wt',
        'rules' => 'trim|required|numeric|greater_than_equal_to[0.1]',
        'errors'=>array('greater_than_equal_to[0.1]'=>'gross wt Should be Greater Than 0.1',
                        'numeric'=>'Please Enter Valid gross wt.')

    ),
    array(
        'field' => 'sales_stock[color_stone]',
        'label' => 'color stone',
        'rules' => 'trim|required|numeric|greater_than_equal_to[0.1]',
        'errors'=>array('greater_than_equal_to[0.1]'=>'color stone Should be Greater Than 0.1',
                        'numeric'=>'Please Enter Valid color stone.')

    ),
    );

$config['mfg_dep_direct_order_qty'] = array(

     array(
        'field' => 'product_name',
        'label' => 'Product ',
        'rules' => 'trim|required',
         'errors'=>array('required'=>"Please Select product."),
    ),
     array(
        'field' => 'weight_range_id',
        'label' => 'Weight range',
        'rules' => 'trim|required'
    ),
   
    
    
    array(
        'field' => 'parent_category_id',
        'label' => 'Product',
        'rules' => 'trim|required',
        'errors'=>array('required'=>"Please Select Product."),
        
    ),
    array(
        'field' => 'karigar_id',
        'label' => 'karigar',
        'rules' => 'trim|required',
        'errors'=>array('required'=>"Please Select  karigar."),
        
    ),


    array(
        'field' => 'quantity',
        'label' => 'Quantity',
        'rules' => 'trim|required|numeric|is_natural_no_zero',
        'errors'=>array('numeric'=>"Please Enter Numeric Value.",
                        'is_natural_no_zero'=>"Please Enter Valid Quantity"),
        
    ),
   
   
 
);
$config['mfg_dep_direct_order_wt'] = array(

    array(
        'field' => 'product_name',
        'label' => 'Product ',
        'rules' => 'trim|required',
         'errors'=>array('required'=>"Please Select product."),
    ),
     array(
        'field' => 'weight_range_id',
        'label' => 'Weight range',
        'rules' => 'trim|required'
    ),
     
    array(
        'field' => 'parent_category_id',
        'label' => 'Product',
        'rules' => 'trim|required',
        'errors'=>array('required'=>"Please Select Product."),
        
    ),
    array(
        'field' => 'karigar_id',
        'label' => 'karigar',
        'rules' => 'trim|required',
        'errors'=>array('required'=>"Please Select  karigar."),
        
    ),

    array(
        'field' => 'weight',
        'label' => 'weight',
        'rules' => 'trim|required|numeric|greater_than_equal_to[0.1]',
        'errors'=>array('greater_than_equal_to[0.1]'=>'weight Should be Greater Than 0.1',
                        'numeric'=>'Please Enter Valid weight.')
        
    ),
   
 
);


$config['repair_product'] = array(
    array(
        'field' => 'quantity',
        'label' => 'Quantity',
        'rules' => 'trim|required|greater_than_equal_to[1]',
        'errors' =>array(
            'greater_than_equal_to[1]'=>"Quantity should be greater than 1",
            ),
        ),
    array(
        'field' => 'gr_wt',
        'label' => 'Gross',
        'rules' => 'trim|required|greater_than_equal_to[0.1]',
        'errors' =>array(
            'greater_than_equal_to[1]'=>"Gross Wt should be greater than 0.1",
             ),
        ),
    array(
        'field' => 'net_wt',
        'label' => 'net wt',
        'rules' => 'trim|required|greater_than_equal_to[0.1]',
        'errors' =>array(
            'greater_than_equal_to[1]'=>"Net Wt should be greater than 0.1",
            ), 
        ),
    array(
        'field' => 'kundan_pc',
        'label' => 'kundan pc',
        'rules' => 'trim|required|greater_than_equal_to[0.1]',
        'errors' =>array(
            'greater_than'=>"K Pcs should be greater than 0.1",
            ),  
        ),      
    array(
            'field' => 'kundan_rate',
            'label' => 'kundan rate',
            'rules' => 'trim|required|greater_than_equal_to[0.1]',
            'errors' =>array(
                'greater_than'=>"kundan rate should be greater than 0.1",
            ),
        ),
    array(
            'field' => 'kundan_rate',
            'label' => 'kundan rate',
            'rules' => 'trim|required|greater_than_equal_to[0.1]',
            'errors' =>array(
                'greater_than'=>"K @ should be greater than 0.1",
            ),
        ),    
    array(
            'field' => 'kundan_amt',
            'label' => 'kundan amt',
            'rules' => 'trim|required|greater_than_equal_to[0.1]',
            'errors' =>array(
                'greater_than'=>"K amount should be greater than 0.1",
            ),
        ),    
    array(
            'field' => 'checker',
            'label' => 'checker',
            'rules' => 'trim|required|greater_than_equal_to[0.1]',
            'errors' =>array(
                'greater_than'=>"checker should be greater than 0.1",
            ),
    ),
    array(
            'field' => 'checker_pcs',
            'label' => 'checker pcs',
            'rules' => 'trim|required|greater_than_equal_to[0.1]',
            'errors' =>array(
                'greater_than'=>"checker pcs should be greater than 0.1",
            ),
    ),
    array(
            'field' => 'stone_amt',
            'label' => 'stone amt',
            'rules' => 'trim|required|greater_than_equal_to[0.1]',
            'errors' =>array(
                'greater_than'=>"stone amt should be greater than 0.1",
            ),
    ),

    
);
$config['repair_product_dep'] = array(

    array(
        'field' => 'gr_wt',
        'label' => 'Gross',
        'rules' => 'trim|required|greater_than_equal_to[0.1]',
        'errors' =>array(
            'greater_than_equal_to[1]'=>"Gross Wt should be greater than 0.1",
             ),
        ),
    array(
        'field' => 'net_wt',
        'label' => 'net wt',
        'rules' => 'trim|required|greater_than_equal_to[0.1]',
        'errors' =>array(
            'greater_than_equal_to[1]'=>"Net Wt should be greater than 0.1",
            ), 
        ),
   

    
);