<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

$config['forgot_password'] =  array(
    array(
        'field'=>'forgot[forgot_email]',
        'label'=>'Email',
        'rules'=>'trim|required|valid_email|checkFORGOTEMAIL',
        'errors'=>array(
                    'checkFORGOTEMAIL'=>'Invalid Email'
                  )
    ),
   
);
$config['customer_order'] = array(
  


    array(
        'field' => 'customer_order[parent_category_id]',
        'label' => 'Parent Category',
        'rules' => 'trim|required',
        //'errors'=>array('check_parent_category_id_code'=>"This Parent Category Not Exits"),
        
    ),
    array(
        'field' => 'customer_order[weight_range_id]',
        'label' => 'Weight Range',
        'rules' => 'trim|required|numeric|greater_than_equal_to[0.1]',
        'errors'=>array('greater_than_equal_to[0.1]'=>'Net Wt Should be Greater Than 0.1',
                        'numeric'=>'Please Enter Valid Weights.')
        
    ),
    array(
        'field' => 'customer_order[quantity]',
        'label' => 'Quantity',
        'rules' => 'trim|required|numeric|is_natural_no_zero',
        'errors'=>array('numeric'=>"Please Enter Numeric Value.",
                        'is_natural_no_zero'=>"Please Enter Valid Quantity"),
        
    ),
   
    array(
        'field' => 'customer_order[delievery_date]',
        'label' => 'delievery_date',
        'rules' => 'trim|required',
        'errors'=>array('required'=>"Please Select Delivery Date."),
    ),
 
);


$config['register'] = array(
      
      array(
          'field'=>'email',
          'label'=>'Email',
          'rules'=>'check_already_exist',
          'errors'=>array('check_already_exist' => "Email already exist")
        ), 
     
    );


$config['update_user'] = array(
      array(
          'field'=>'email',
          'label'=>'Email',
          'rules'=>'check_already_exist',
          'errors'=>array('check_already_exist' => "Email already exist")
        ), 
    );

$config['forgot_password'] =  array(
    array(
        'field'=>'forgot[forgot_email]',
        'label'=>'Email',
        'rules'=>'checkFORGOTEMAIL',
        'errors'=>array(
                    'checkFORGOTEMAIL'=>'Inavlid Email'
                  )
    ),
   
);

$config['change_password'] =  array(
      array(
          'field'=>'reset_password[password]',
          'label'=>'Password',
          'rules'=>'trim|required'
        ),
         array(
          'field'=>'confirm_password',
          'label'=>'Confirm password',
          'rules'=>'trim|required|matches[reset_password[password]]'
        )
   
);