<?php
class Services_model extends CI_Model
{
    function __construct()
    {
        parent::__construct();
        $this->sql="";
        $this->userid="";
        $this->limit="";
        $this->where="";
        $this->sql1="";
        $this->postData="";
         $this->load->library('Data_encryption');
         $this->load->library('Email_lib');
          $this->load->library('M_pdf');

    }
     function validateLoginData($type) 
    {
        $this->form_validation->set_rules($this->config->item($type, 'api_validationrules'));
        if ($this->form_validation->run() == FALSE) {
            return FALSE;
        } else {
            return TRUE;
        }
    }

     function validateForgotPassswordData() 
    {
        $this->form_validation->set_rules($this->config->item('forgot_password', 'api_validationrules'));
        if ($this->form_validation->run() == FALSE) {
            return FALSE;
        } else {
            return TRUE;
        }
    }

     function validateChangePasswordData() 
    {
        $this->form_validation->set_rules($this->config->item('change_password', 'api_validationrules'));
        if ($this->form_validation->run() == FALSE) {
            return FALSE;
        } else {
            return TRUE;
        }
    }
     function validateregisterData() 
    {
        $this->form_validation->set_message('check_already_exist', 'Email Already Exist');
        $this->form_validation->set_rules($this->config->item('register', 'api_validationrules'));
        if ($this->form_validation->run() == FALSE) {
            return FALSE;
        } else {
            return TRUE;
        }
    }
     public function validatepostdata(){
    $this->form_validation->set_rules($this->config->item('customer_order','api_validationrules'));
     if ($this->form_validation->run() == FALSE) {        
          return FALSE;
      } else {
          return TRUE;
      }
  }

    function login($data)
    {
       $result=$this->db->query("SELECT * FROM customer WHERE email='".$data['username']."' and encrypted_password='".$data['password']."'")->row_array();
       if(!empty($result))
       {    if($result['imagepath'] == "" || $result['imagepath'] == null){
               $imagepath = null;
            }else{
              $imagepath = UPLOAD_IMAGES.$result['imagepath'];
            }
            $response['user_profile'] = array(
                'id'=>$result['id'],
                'name'=>$result['name'],
                'email' => $result['email'],
                'mobile_no' =>$result['mobile_no'],
                'imagepath' =>$imagepath,
                'client_code' =>$result['client_code'],
                'company_name' =>$result['company_name'],
                
            ); 
            $session_data = array(
                'email' => $result['username'],
                'logged_in' => TRUE,
                'type' =>$result['role']
            );
            $this->session->set_userdata($session_data);
            $response['status'] = TRUE;
            //print_r($response);exit;
           return $response;
       }
       else
       {    $response['status'] = FALSE;
             $response['user_profile'] = array(
                'id'=>null,
                'name'=>null,
                'email' => null,
                'mobile_no' =>null,
                'imagepath' => null,
                'client_code' => null,
                'company_name' => null,
                
            ); 
            return $response;
       }
    }


      function save_user($data){
         $insert_array = $data;
        $encrption = $this->data_encryption->get_encrypted_id();      
        $num_str = sprintf("%06d", mt_rand(1, 999999));
        $this->db->select('max(id)+1 as last_id');
        $result = $this->db->get('customer')->row_array();
          $last_id =$result['last_id'];
        $insert_array['client_code']='C'.$num_str.$last_id;
        $insert_array['encrypted_password']=$data['set_password'];
        $insert_array['encrypted_id']=$encrption;
        $insert_array['customer_type_id']=2;
        $insert_array['added_by']='app';
        $insert_array['updated_at']=date('Y-m-d H:i:s');
        $insert_array['created_at']=date('Y-m-d H:i:s');
        unset($insert_array['set_password']);
        unset($insert_array['re_password']); 
        $this->db->insert('customer',$insert_array);
          $last_id =$this->db->insert_id();
          $response['status']="success";
          $response['msg']="Register Successfully!!!";
          $response['data'] =  array(
                'id'=>$last_id,
                'name'=>$data['name'],
                'email' => $data['email'],
                );  
     
        
        return $response;
    }
   
   public function store($post_array=''){  
      $postdata = $this->input->post();
//  print_r($postdata);
//echo "<pre>";print_r($_FILES);exit;
    $insert_data=$this->form_data($postdata['customer_order']);  
    if ($this->db->insert('customer_orders',$insert_data)) {                
      $response['status']="success";
      $response['data']=null; 
      $response['msg']="Successfully Inserted"; 
      $response['error'] = null;
      $this->upload_image($postdata);        
      return $response;
    }
  }

   public function find($order_id){
    //return $this->db->get_where($this->table_name,array('id'=>$order_id))->row_array();
   $this->db->select('co.*,d.name as department_name,a.username as prepared_by,co.weight_range_id as approx_weight,km.name as karigar_name,c.name category_name,cor.karigar_rcpt_voucher_no,km.email as k_email');
    $this->db->from('customer_orders co');
    $this->db->join('category_master c','co.category_id=c.id','left');
    $this->db->join('customer_order_received cor','cor.order_id=co.id','left');
    $this->db->join('departments d','co.department_id=d.id','left');
    $this->db->join('karigar_master km','co.karigar_id=km.id','left');
    //$this->db->join('weights w','co.weight_range_id=w.id');
    $this->db->join('admin a','co.prepared_by=a.id');
    $this->db->where('co.id',$order_id);
  
    return $this->db->get()->row_array();

  }


    public function create_pdf($data,$order_id,$type){
    $html=$this->load->view('order_module/customer_order/'.$type, $data,true);
    makeDirectory('Customer_order_pdf/'.$order_id);
    $pdfFilePath = dirname(dirname(dirname(dirname(__FILE__))))."/uploads/Customer_order_pdf/".$order_id."/".$type.".pdf";
    $mpdf=new mPDF();
    $mpdf->WriteHTML($html);
    $mpdf->Output($pdfFilePath, "F"); 
    $pdfPath="Customer_order_pdf/".$order_id."/".$type.".pdf";
    return $pdfPath;
  } 

 public function get_images($order_id){
    $this->db->select('id,img_file_path');
    $this->db->from('customer_orders_img');
    $this->db->where('customer_id',$order_id);
    $result = $this->db->get()->result_array();
    return $result;
  }

     public function update_pdf_path($order_id,$path_clm,$table,$column)
  { 
    $this->db->where($column,$order_id);
    $this->db->update($table,$path_clm);
  }

  private function customer_update($postdata)
  {
    
    if(empty($postdata['customer_id'])){
      $customer['encrypted_id']=$this->data_encryption->get_encrypted_id('customer');
      $customer['name'] = $postdata['party_name'];
      $customer['mobile_no'] = @$postdata['mobile_no'];
      $customer['email'] = @$postdata['email_id'];
      $customer['company_name'] = $postdata['company_name'];
      $customer['created_at']=date('Y-m-d H:i:s');
      $customer['customer_type_id'] = '2';
      $this->db->insert('customer',$customer);
      $postdata['customer_id'] = $this->db->insert_id();
    }else{
      $customer['name'] = $postdata['party_name'];
      $customer['mobile_no'] = @$postdata['mobile_no'];
      $customer['email'] = $postdata['email_id'];
      $customer['company_name'] = $postdata['company_name'];
      $customer['customer_type_id'] = '2';
      $customer['updated_at']=date('Y-m-d H:i:s');
      $this->db->where('id',$postdata['customer_id']);
      $this->db->update('customer',$customer);
    }
    return $postdata;
  }


 private function karigar_update($postdata)
  {
   
    if(empty($postdata['karigar_id'])){
      $karigar['encrypted_id']=$this->data_encryption->get_encrypted_id('karigar_master');
      $karigar['name'] = $postdata['k_name'];
      $karigar['mobile_no'] = @$postdata['k_mob_no'];
      $karigar['email'] = @$postdata['k_email'];
      $karigar['code'] = @$postdata['k_code'];
      $karigar['created_at']=date('Y-m-d H:i:s');
      $karigar['customer_type_id'] = '1';
      $this->db->insert('karigar_master',$karigar);
      $postdata['karigar_id'] = $this->db->insert_id();

    }else{
      $karigar['name'] = $postdata['k_name'];
      $karigar['mobile_no'] = @$postdata['k_mob_no'];
      $karigar['email'] = @$postdata['k_email'];
      $karigar['customer_type_id'] = '1';
      $karigar['code'] = $postdata['k_code'];
      $karigar['updated_at']=date('Y-m-d H:i:s');
      $this->db->where('id',$postdata['karigar_id']);
      $this->db->update('karigar_master',$karigar);
      $postdata['karigar_id'] = $postdata['karigar_id'];
    }
    if (!empty($postdata['karigar_id'])) {
      $postdata['status']=1;
    }else{
      $postdata['status']=0;
    }
 //die;
    return $postdata;
  }

private function form_data($postdata)
  { 

    $postdata['created_at']=date('Y-m-d H:i:s');
    $postdata['updated_at']=date('Y-m-d H:i:s');
    $delievery_date = str_replace('/', '-', $postdata['delievery_date']);
    $postdata['delievery_date']=date('Y-m-d',strtotime($delievery_date));
    //print_r($postdata['delievery_date']);exit;
    //$postdata['expected_date']=date('Y-m-d',strtotime($postdata['expected_date']));
    $postdata['expected_date']=date('Y-m-d', strtotime($delievery_date. ' - 3 days'));
    $postdata['order_date']=date('Y-m-d');
    $postdata['added_by']='app';
    $postdata['status']=0;
    $postdata['remaining_qty']=$postdata['quantity'];
    $postdata['customer_id']=$postdata['customer_id'];
    $postdata['party_name']=$postdata['customer_name'];
    $postdata['prepared_by']=$postdata['customer_id'];
    unset($postdata['customer_name']);
    return $postdata;
  } 

     private function upload_image($postdata)
  {   

      if($_FILES['image']['name']){   
      { //print_r($_FILES['image']);exit;     
        foreach ($_FILES['image']['name'] as $key => $val )
        
            if(!empty($val)){
            $val1 = true;
            $file_array['name'] = $val;
            $file_array['type'] = $_FILES['image']['type'][$key];
            $file_array['tmp_name'] = $_FILES['image']['tmp_name'][$key];
            $file_array['error'] = $_FILES['image']['error'][$key];
            $file_array['size'] = $_FILES['image']['size'][$key];
            $upload_img[$key] =uploadImage('order_module',$file_array);
            $uploadData[$key]['img_file_path'] ='order_module/'.$upload_img[$key]['file_name'];
            $uploadData[$key]['created_at'] = date("Y-m-d H:i:s");
            $uploadData[$key]['updated_at'] = date("Y-m-d H:i:s");         
            $uploadData[$key]['customer_id'] = $this->db->insert_id();
          }
      }
    }

    if(isset($val1) && $val1 == true){
      $this->db->insert_batch('customer_orders_img',$uploadData);
    }

}


// public function get_sales_data(){  
//     $this->db->select('drp.product_code as name,drp.gr_wt as gross_wt,drp.net_wt as net_wt,drp.color_stone,rp.stone_amt,qc.checker_amt,qc.checker@ as checker_rate ,qc.checker_pcs,qc.kundan_wt,qc.kundan_pc,qc.kundan_amt,qc.color_stone_rate');
//     $this->db->from('department_ready_product drp');
//     $this->db->join('tag_products tp','tp.product_code = drp.product_code','left');
//     $this->db->join('Receive_products rp','drp.receive_product_id = rp.id','left');
//     $this->db->join('category_master c','c.code=tp.item_category_code','left');    
//     $this->db->join('quality_control qc','qc.id = drp.qc_id','left');
//     $this->db->where('drp.status','2');
//     $this->db->where('drp.department_id','1');
//     $this->db->order_by('drp.product_code','asc');
//     $result=$this->db->get()->result_array();
//     //echo $this->db->last_query();die;
//     return $result;

//   }

  public function get_sales_data($department_name=''){  
     $this->db->select('drp.product_code as name,drp.gr_wt as gross_wt,drp.net_wt as net_wt,color_stone,stone_amt,checker_amt,checker_rate ,checker_pcs,wax_wt as kundan_wt,kundan_pc,kundan_amt,color_stone_rate,checker_wt,kundan_rate,other_wt as other_charges,black_beads_wt,stone_wt,cm.name as category_name,km.name as karigar_name');
     $this->db->from('department_ready_product drp');  
     $this->db->join('departments dpt','dpt.id=drp.department_id'); 
     $this->db->join('tag_products tp','drp.product_code=tp.product_code'); 
     $this->db->join('category_master cm','cm.code=tp.item_category_code'); 
     $this->db->join('karigar_master km','km.id = drp.karigar_id','left');

      
     $this->db->where('drp.status','2');
     //$this->db->where('department_id','1');
     if(!empty($department_name)){
      $this->db->where('dpt.name',$department_name);
     }
     $this->db->order_by('drp.product_code','asc');
     $result=$this->db->get()->result_array();
     return $result;
   }

  public function update_sales_data($data){
        $updated_data['status']='1';
        $updated_data['updated_at'] = date("Y-m-d H:i:s");
        $this->db->where('product_code',$data['product_code']);     
        if ($this->db->update('sale_stock',$updated_data)) {  
          $response=true;
        }else{
          $response=false;
        }
          
        return $response;
  }
/* public function get_sales_data(){  
   $this->db->select('product_code as name,gr_wt as gross_wt,net_wt as net_wt,color_stone,stone_amt,checker_amt,checker_rate ,checker_pcs,wax_wt as kundan_wt,kundan_pc,kundan_amt,color_stone_rate,checker_wt,kundan_rate,other_wt');
   $this->db->from('department_ready_product drp');     
   $this->db->where('status','2');
   $this->db->where('department_id','1');
   $this->db->order_by('product_code','asc');
   $result=$this->db->get()->result_array();
   return $result;
 }*/

public function get_parent_category(){
  $this->db->select('id,name');
  $this->db->from('parent_category');
  $this->db->where('app_order',1);
  $this->db->order_by('name','asce');
  $result = $this->db->get()->result_array();
  return $result;
}



 function forgot_password($email,$data){
     $result = $this->db->update('customer',$data,array('email'=>$email));
     $res=$this->password_link_send_email($email,$data);
      return TRUE;
  }

 private function password_link_send_email($email,$linkdata){
        $user = get_user_by_email($email);
       
        $data['name']= $user['name'];
        $data['encrypted_password']= $linkdata['encrypted_password'];
       // print_r($data);exit;
        $mail['to'] = $email;
        $from="akshay@ascratech.com";
        $from_name="Shilpi_jewlwers";
        $result =$this->email_lib->send($data,$email,'Forgot Password','auth/forgot_password_email.php',$from,$from_name);
        return $result;
    }


       public function update_password($data){
        $updated_data['encrypted_password']=$data['reset_password']['password'];
        $updated_data['updated_at'] = date("Y-m-d H:i:s");
        $updated_data['is_change_password'] = "Y";
         $this->db->where('reset_password_token',$data['forgot_password_code']);
        $this->db->update('customer',$updated_data);
        return TRUE;
    }

       public function update_password_by_id($data){
        $updated_data['encrypted_password']=$data['new_password'];
        $updated_data['updated_at'] = date("Y-m-d H:i:s");
         $this->db->where('id',$data['id']);
         $res = $this->db->update('customer',$updated_data);
        if($res){
            return TRUE; 
        }
       
    }

    public function update_user($data){

        $updated_data['email']=$data['email'];
        $updated_data['name'] = $data['name'];
        $updated_data['updated_at'] = date("Y-m-d H:i:s");
       $this->db->where('id',$data['id']);
       $this->db->update('customer',$updated_data); 
       $response['image_path'] =$this->upload_user_image($data,$data['id']);      
       $response['status']  = TRUE;
       return $response;
    }


     private function upload_user_image($postdata,$id)
  {  
      if($_FILES['image']['name']){        
              $val1 = true;
            $file_array['name'] = $_FILES['image']['name'];
            $file_array['type'] = $_FILES['image']['type'];
            $file_array['tmp_name'] = $_FILES['image']['tmp_name'];
            $file_array['error'] = $_FILES['image']['error'];
            $file_array['size'] = $_FILES['image']['size'];
            $upload_img =uploadImage('user_profile',$file_array);
            $uploadData['imagepath'] ='user_profile/'.$upload_img['file_name'];
           // $uploadData[$key]['updated_at'] = date("Y-m-d H:i:s");         
            
          }
      
   if(isset($val1) && $val1 == true){
      $this->db->where('id',$id);
      $this->db->update('customer',$uploadData);
    }
    return $uploadData['imagepath'];

}


    public function check_customer_mobile_exists($postdata){  
        $this->db->select('*');
        $this->db->from('customer');
        $this->db->where('mobile_no',$postdata['mobile_no']);
        $this->db->where('customer_type_id','2');
        if(!empty($postdata['customer_id']))
            $this->db->where('id !=',$postdata['customer_id']);
        $result = $this->db->get()->row_array();
        if(!empty($result)){
            return false;
        }else{
            return true;
        }
    }   

    public function find_old_order_data($order_id){

   $this->db->select('co.*,d.name as department_name,a.username as prepared_by,co.weight_range_id as approx_weight,km.name as karigar_name,c.name category_name,cor.karigar_rcpt_voucher_no,km.email as k_email,DATE_FORMAT(co.order_date,"%d-%m-%Y")  order_date , DATE_FORMAT(ifnull(co.change_delivery_date,co.delievery_date),"%d-%m-%Y") delievery_date, DATE_FORMAT(co.expected_date,"%d-%m-%Y")  expected_date ');
            $this->db->from('customer_orders co');
    $this->db->join('category_master c','co.category_id=c.id','left');
    $this->db->join('customer_order_received cor','cor.order_id=co.id','left');
    $this->db->join('departments d','co.department_id=d.id','left');
    $this->db->join('karigar_master km','co.karigar_id=km.id','left');
    //$this->db->join('weights w','co.weight_range_id=w.id');
    $this->db->join('admin a','co.prepared_by=a.id','left');
    $this->db->where('co.id',$order_id);
  
    return $this->db->get()->row_array();

  }


    //  public function get_order_by_user_id($postdata){  
    //     $this->db->select('co.*,DATE_FORMAT(co.order_date,"%d/%m/%Y") AS order_date,DATE_FORMAT(co.delievery_date,"%d/%m/%Y") AS delievery_date,DATE_FORMAT(co.expected_date,"%d/%m/%Y") AS expected_date,DATE_FORMAT(co.sent_to_karigar_date,"%d/%m/%Y") AS sent_to_karigar_date,group_concat(coi.img_file_path) as images');
    //     $this->db->from('customer_orders co');
    //     $this->db->join('customer_orders_img coi','coi.customer_id = co.id','left');
    //     $this->db->where('co.customer_id',$postdata['id']);
    //     $this->db->where('added_by','app');
    //     $this->db->order_by('co.id','DESC');
    //     $this->db->group_by('co.id');
    //     $result = $this->db->get()->result_array(); 
    //     return $result;
    // }

    private function get_images_by_order_id($order_id){
              $this->db->select('id,img_file_path');
       return $this->db->get_where('customer_orders_img',array('customer_id'=>$order_id))->result_array();
    }

    public function get_order_by_user_id($postdata){  
        $this->db->select('co.*,DATE_FORMAT(co.order_date,"%d/%m/%Y") AS order_date,ifnull(DATE_FORMAT(co.change_delivery_date,"%d/%m/%Y"),DATE_FORMAT(co.delievery_date,"%d/%m/%Y")) AS delievery_date,DATE_FORMAT(co.expected_date,"%d/%m/%Y") AS expected_date,DATE_FORMAT(co.sent_to_karigar_date,"%d/%m/%Y") AS sent_to_karigar_date,DATE_FORMAT(co.pending_date,"%d/%m/%Y") AS pending_date,DATE_FORMAT(co.inproduction_date,"%d/%m/%Y") AS inproduction_date,DATE_FORMAT(co.receive_date,"%d/%m/%Y") AS ready_for_delievery_date,DATE_FORMAT(co.actual_delivery_date,"%d/%m/%Y") AS dispatch_date');
        $this->db->from('customer_orders co');
        $this->db->where('co.customer_id',$postdata['id']);
        $this->db->where('added_by','app');
        $this->db->order_by('co.id','DESC');
        $result = $this->db->get()->result_array();
        foreach ($result as $key => $value) {
          $result[$key]['images']=$this->get_images_by_order_id($value['id']);
        }

        return $result;
    }

     public function get_order($postdata){  
        $this->db->select('co.*,coi.img_file_path as image,DATE_FORMAT(co.order_date,"%d/%m/%Y") AS order_date,ifnull(DATE_FORMAT(co.change_delivery_date,"%d/%m/%Y"),DATE_FORMAT(co.delievery_date,"%d/%m/%Y")) AS delievery_date,DATE_FORMAT(co.expected_date,"%d/%m/%Y") AS expected_date,DATE_FORMAT(co.sent_to_karigar_date,"%d/%m/%Y") AS sent_to_karigar_date,DATE_FORMAT(co.pending_date,"%d/%m/%Y") AS pending_date,DATE_FORMAT(co.inproduction_date,"%d/%m/%Y") AS inproduction_date,DATE_FORMAT(co.receive_date,"%d/%m/%Y") AS ready_for_delievery_date,DATE_FORMAT(co.actual_delivery_date,"%d/%m/%Y") AS dispatch_date');
        $this->db->from('customer_orders co');      
        $this->db->join('customer_orders_img coi','coi.customer_id = co.id','left');
        $this->db->where('co.id',$postdata['id']);
        $this->db->where('added_by','app');
        $result = $this->db->get()->row_array(); 
        return $result;
    }  

    public function get_order_history($order_id){  
        $this->db->select('REPLACE(filed_name, "_", " ") as filed_name,filed_value, DATE_FORMAT( order_change_date,  "%d-%M-%Y  %T" ) as order_change_date');
        $this->db->from('customer_orders_history');      
        $this->db->where('order_id',$order_id);       
        $result = $this->db->get()->result_array(); 
       //echo $this->db->last_query();
        return filter_reponse_array($result,'order_change_date');      
    }


     public function update_order($postdata){ 
         $updated_data= $postdata;
          if($postdata['status'] == 2){
           $updated_data['status'] = 5;
         }
         
         $delievery_date = str_replace('/', '-', $postdata['delievery_date']);
         $updated_data['delievery_date']=date('Y-m-d',strtotime($delievery_date));
        //print_r($updated_data);exit;
          $updated_data['expected_date']=date('Y-m-d', strtotime($delievery_date. ' - 3 days'));
          $updated_data['order_history'] = '1';
          $updated_data['updated_at'] = date('Y-m-d H:i:s'); 
          $updated_data['remaining_qty']=$updated_data['quantity'];
          $postdata_id = $this->find_old_order_data($postdata['id']);  
          $order_result=array_diff($updated_data,$postdata_id);
          $order_result['added_by']='app';
          $order_result['order_id']=$postdata['id'];
          $order_history=array();
          $order_history_value=order_history_update($order_result);
         $this->db->where('id',$postdata['id']);
         $res = $this->db->update('customer_orders',$updated_data);
         $this->update_upload_image($postdata,$postdata['id']); 
          if($res){
            return TRUE;
          }
    }



      private function update_upload_image($postdata,$id)
   {
        
      if($_FILES){
          foreach ($_FILES['image']['name'] as $key => $value) {
            $file_array['name'] = $_FILES['image']['name'][$key];
            $file_array['type'] = $_FILES['image']['type'][$key];
            $file_array['tmp_name'] = $_FILES['image']['tmp_name'][$key];
            $file_array['error'] = $_FILES['image']['error'][$key];
            $file_array['size'] = $_FILES['image']['size'][$key];
            $upload_img =uploadImage('order_module',$file_array);
            $uploadData['img_file_path'] ='order_module/'.$upload_img['file_name'];
            $uploadData['created_at'] = date("Y-m-d H:i:s");
            $uploadData['updated_at'] = date("Y-m-d H:i:s");         
            $uploadData['customer_id'] = $id;
            if($key == "0" & $value != ""){
              $this->db->insert('customer_orders_img',$uploadData);
            }elseif ($key != '0' && $value != "") {
                $this->db->where('customer_id',$id);
                $this->db->where('id',$key);
                $this->db->update('customer_orders_img',$uploadData);
            }
          }
        }  
   

}


public function change_status($data){
        $updated_data['status']=$data['status'];
        $updated_data['updated_at'] = date("Y-m-d H:i:s");
         $this->db->where('id',$data['id']);
         $res = $this->db->update('customer_orders',$updated_data);
        if($res){
            return TRUE; 
        }   
}

}

?>
