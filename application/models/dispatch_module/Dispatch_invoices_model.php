<?php
class Dispatch_invoices_model extends CI_Model {

  function __construct() {
      $this->table_name = "sales_invoice";
      $this->table_name_invoice_details = "sales_invoice_details";
      parent::__construct();
  }

 
  public function get($filter_status='',$params='',$search='',$limit=''){
    $this->db->select('si.id as chitti_no,count(sid.id) as products,si.status,c.name as customer_name,cs.name as color_stone_name,kc.name as kundan_category_name,date_format(si.created_at,"%D %b %Y") as created_at,SUM(sid.gross_wt) as gross_wt,SUM(sid.total_amt) as total_amt');
    $this->db->from($this->table_name.' si');
    $this->db->join($this->table_name_invoice_details.' sid','si.id=sid.sales_invoice_id','left');
    $this->db->join('customer c','si.customer_id=c.id');
    $this->db->join('color_stone cs','si.color_stone_id=cs.id');
    $this->db->join('kundan_category kc','si.kundan_category_id=kc.id');

    if (@$_GET['status']=="approved") { 
      $this->db->where('si.status',2);
    }else if (@$_GET['status']=="rejected") { 
      $this->db->where('si.status',3);
    }else{
      $this->db->where('si.status',1);
    }

  /*  if(!empty($search)){
        $this->db->like('si.id',$search);
    }*/
    if(isset($params['columns']) && !empty($params['columns'])){
      $filter_input=$params['columns'];
      $table_col_name="dispatch_invoice_table";
      $this->get_filter_value($filter_input,$table_col_name);
    }
    if($limit == true)
    	$this->db->limit($params['length'],$params['start']);

    $this->db->group_by('si.id');
    $result = $this->db->get()->result_array();
//echo $this->db->last_query(); print_r($result);exit;
    return $result;
  }
  private function get_filter_value($filter_input,$table_col_name){
    $column_name=array();  
    $filter_column_name=filter_column_name($table_col_name);
    $sql='';
    $i=0;
     
     //print_r($filter_input);die;
    foreach ($filter_input as $key => $search_value){
       $column_name=$filter_column_name;
       //print_r($search_value['search']['value']);
        if(!empty($search_value['search']['value'])){
          if($i != 0){
            $sql.=' AND  ';
          }
            $sql.=''.$column_name[$key].' like "%'.$search_value['search']['value'].'%" ';
            $i++;
        

         }   
    }

    if(!empty($sql)){  
      $this->db->where($sql);  
    } 
   
       
  }
   
  public function get_hallmarked($type){
    $this->db->select('qch.id');
    $this->db->from('quality_control_hallmarking qch');
    $this->db->where('qch.module','4');
    $result = $this->db->get()->result_array();
    return $result;
  } 
  public function find($id){
  	$this->db->where('id',$id);
  	$result = $this->db->get($this->table_name)->row_array();
  	return $result;
  }

  
  public function approve_reject(){
  
     $this->db->set('status',$_POST['status']);
     $this->db->where('id',$_POST['id']);
    if($this->db->update('sales_invoice')){
       return get_successMsg();
     }else{
       return get_errorMsg();
     }
  }
 
  public function get_remaing_qty($sid_id){
      $this->db->select('drp.quantity');
      $this->db->where('sid.id',$sid_id);
      $this->db->from('sales_invoice_details sid');
      $this->db->join('sale_stock ss','sid.stock_id=ss.id','left');
      $this->db->join('department_ready_product drp','ss.drp_id=drp.id','left');
      $query = $this->db->get()->row_array();

      $this->db->select('SUM(qch.quantity) as total_qty');
      $this->db->where('qch.sid_id',$sid_id);
      $this->db->from('quality_control_hallmarking qch');
      $result = $this->db->get()->row_array();

      $this->db->select('SUM(quantity) as dispatch_qty');
      $this->db->where('sid_id',$sid_id);
      $this->db->from('dispatch_products');
      $dispatch = $this->db->get()->row_array();
      // echo $sid_id."sid--".$query['quantity']."<<".@$result['total_qty'].">>".@$dispatch['dispatch_qty'];
      // print_r($query);exit;
      return $query['quantity']-(@$result['total_qty']+@$dispatch['dispatch_qty']);
  }
 
  public function drp_data($id){
    $this->db->select('drp.id');
    $this->db->from('department_ready_product'.' drp');
    $this->db->join('sale_stock st','st.drp_id=drp.id','left');
    $this->db->join('sales_invoice_details s_de_id','s_de_id.stock_id=st.id','left');
    $this->db->where('s_de_id.id',$id);
    $result = $this->db->get()->row_array();
    return $result;
  }
  // public function sent_to_hallamrking($hallmarking_data){
  //     $responce_array = array();
  //     $responce_array['status']   = "success";
  //     foreach ($hallmarking_data['invoice_detail_id'] as $key => $value) {
  //       $drp_result  =  $this->drp_data($value);
  //       $insert_arr  = array(
  //                       //'hallmarking_center_id'=>$hallmarking_data['hallmarrking_center'][$key],
  //                       'hc_id'=>$hallmarking_data['hallmarrking_center'][$key],
  //                       'drp_id'  =>$drp_result['id'],
  //                       'si_detail_id'=>$value,
  //                       'person_name'  =>$hallmarking_data['person_name'][$key],
  //                       'hc_logo' =>  $hallmarking_data['hallmarking_logo'][$key],
  //                       'gr_wt'   =>  $hallmarking_data['hallmarking_gr_wt'][$key],
  //                       'quantity'=>  $hallmarking_data['quantity'][$key],
  //                       'status'  =>  '10',
  //                       'module'  =>  '4',
  //                       'HM_send_date'=>date('Y-m-d H:i:s'),
  //                       'created_at'=>date('Y-m-d H:i:s')
  //                     );
  //        if(!$this->db->insert("quality_control_hallmarking",$insert_arr))
  //            $responce_array['status']   = "failure";
  //     }
  //     return $responce_array;  
  // }
}    