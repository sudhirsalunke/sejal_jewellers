<?php
class Hallmarking_model extends CI_Model {

  function __construct() {
      $this->table_name = "quality_control_hallmarking";
      $this->table_sid="sales_invoice_details";
      parent::__construct();
  }

  public function validatepostdata($type){

    if ($type=="store") {
      $rule="dispatch_hallmarking";
    }else{
      $rule="dispatch_hallmarking_recieve";
    }

  	$this->form_validation->set_rules($this->config->item($rule, 'admin_validationrules'));
     if ($this->form_validation->run() == FALSE) {       	
          return FALSE;
      } else {
          return TRUE;
      }
  }
  public function store(){
    $insert_array=$this->input->post('hallmarking');
    $insert_array['remaining_qty']=$insert_array['quantity'];
    $insert_array['status']='10';
    $insert_array['module']='4';
    $insert_array['HM_send_date']=date('Y-m-d H:i:s');
    $insert_array['created_at']=date('Y-m-d H:i:s');

  if($this->db->insert($this->table_name,$insert_array)){
  		return get_successMsg();
  	}else{
  		return get_errorMsg();
  	}
  }

  private function received_Qty($qch_id,$qty){
    $this->db->query("UPDATE quality_control_hallmarking SET remaining_qty = remaining_qty - $qty WHERE id = '$qch_id'");
  }

  public function recieve(){
     $all_data=$this->input->post('hallmarking');
    /*deduct recived amount from QCh*/
    $this->received_Qty($all_data['qch_id'],$all_data['quantity']);

    $insert_array= array('qch_id' => $all_data['qch_id'],
                         'sid_id'=> $all_data['sid_id'],
                         'recieved_weight'=> $all_data['total_gr_wt'],
                         'recieved_quantity'=> $all_data['quantity'],
                         'amount_per_qty'=> $all_data['amount'],
                         'total_amount'=> $all_data['total_amount'],
                        );

    if($this->db->insert('dispatch_hallmarking_recieved',$insert_array)){
      return get_successMsg();
    }else{
      return get_errorMsg();
    }
  }

  public function get($filter_status='',$status='',$params='',$search='',$limit=''){
    $this->db->select('qch.remaining_qty,qch.id,qch.sid_id,sid.product_code,qch.quantity,qch.total_gr_wt as gross_wt,qch.person_name,hc.name as hallmarking_center_name,sid.sales_invoice_id');
    $this->db->from($this->table_name.' qch');
    $this->db->join($this->table_sid.' sid','qch.sid_id=sid.id','left');
    $this->db->join('hallmarking_center hc','qch.hc_id=hc.id');
    $this->db->where('qch.module','4');
    $this->db->where('qch.remaining_qty >=','1');
 /*   if(!empty($search)){
        $this->db->like('sid.product_code',$search);
    }
*/  
    if(isset($params['columns']) && !empty($params['columns'])){
      $filter_input=$params['columns'];
      $table_col_name="dispatch_hallmarking_table";
      $this->get_filter_value($filter_input,$table_col_name);
    }
    if($limit == true)
    	$this->db->limit($params['length'],$params['start']);

    $this->db->group_by('qch.id');
    $result = $this->db->get()->result_array();
   // echo $this->db->last_query(); print_r($result);exit;
    return $result;
  }
  private function get_filter_value($filter_input,$table_col_name){
    $column_name=array();  
    $filter_column_name=filter_column_name($table_col_name);
    $sql='';
    $i=0;
     
     //print_r($filter_input);die;
    foreach ($filter_input as $key => $search_value){
       $column_name=$filter_column_name;
       //print_r($search_value['search']['value']);
        if(!empty($search_value['search']['value'])){
          if($i != 0){
            $sql.=' AND  ';
          }
            $sql.=''.$column_name[$key].' like "%'.$search_value['search']['value'].'%" ';
            $i++;
        

         }   
    }

    if(!empty($sql)){  
      $this->db->where($sql);  
    } 
   
       
  }

 //  public function find_by_encrypted_id($encrypted_id){
 //   $this->db->where('encrypted_id',$encrypted_id);
 //   $result = $this->db->get($this->table_name)->row_array();
 //   return $result;
 //  }
 //  public function find($id){
 //  	$this->db->where('id',$id);
 //  	$result = $this->db->get($this->table_name)->row_array();
 //  	return $result;
 //  }
 //  public function order_details($id){
 //    $this->db->where('manufacturing_order_id',$id);
 //    $result = $this->db->get($this->table_name_mapping)->result_array();
 //    return $result;
 //  }

 //  public function update(){
 //    $postdata = $this->input->post('product');
 //    /*create_order*/
 //    $this->db->where('id',$_POST['order_id']);
 //    $this->db->update($this->table_name,array('department_id' => $postdata['department_id'],
 //                                              'updated_at' =>date('Y-m-d H:i:s'),
 //                                              ));
 //    /**/
 //    $order_id = $_POST['order_id'];
 //    $insert_array=array();
 //    $added_updted_ids=array();
 //    foreach ($postdata['parent_category'] as $key => $value) {
        
 //        $insert_array['parent_category_id']=$value;
 //        $insert_array['weight_range_id']=$postdata['weight'][$key];
 //        $insert_array['quantity']=$postdata['quantity'][$key];
 //        $insert_array['updated_at']=date('Y-m-d H:i:s');

 //        if (isset($postdata['id'][$key]) && !empty($postdata['id'][$key])) {
 //           $added_updted_ids[]=$postdata['id'][$key];

 //           $this->db->where('id',$postdata['id'][$key]);
 //           $this->db->update($this->table_name_mapping,$insert_array);

 //        }else{
 //          $insert_array['manufacturing_order_id']=$order_id;
 //          $insert_array['is_sale']=1;
 //          $insert_array['created_at']=date('Y-m-d H:i:s');
 //          $this->db->insert($this->table_name_mapping,$insert_array);

 //          $added_updted_ids[]=$this->db->insert_id();
 //        }
         
      
 //    }
 //    $this->db->where('manufacturing_order_id',$order_id);
 //    $this->db->where_not_in('id',$added_updted_ids);
 //    $this->db->delete($this->table_name_mapping);

 //      return get_successMsg(1);
    
 //  }
 //  public function delete($id){
 //  	$this->db->where('encrypted_id',$id);
 //  	if($this->db->delete($this->table_name)){
 //  		return get_successMsg();
 //  	}else{
 //  		return get_errorMsg();
 //  	}
 //  }

 // //  public function check_pc_used($id){
 // //      $this->db->where('',$id);
 // //      $this->db->from('');
 // //     return $this->db->count_all_results();
 // // }

 // public function send_to_manufacture($id,$sent_status){
 //   $this->db->where('id',$id);
 //   if ($this->db->update($this->table_name,array('sent'=>$sent_status,'order_date'=>date('Y-m-d')))){
 //     return get_successMsg();
 //    }else{
 //      return get_errorMsg();
 //    }
 // }

  public function get_remaing_qty($sid_id){
      $ci->db->select('drp.quantity');
      $ci->db->where('sid.id',$sid_id);
      $ci->db->from('sales_invoice_details sid');
      $ci->db->join('sale_stock ss','sid.stock_id=ss.id','left');
      $ci->db->join('department_ready_product drp','ss.drp_id=drp.id','left');
      $query = $ci->db->get()->row_array();

      $ci->db->select('SUM(qch.quantity) as total_qty');
      $ci->db->where('qch.id',$sid_id);
      $ci->db->from('quality_control_hallmarking qch');
      $result = $ci->db->get()->row_array();

      return $query['quantity']-@$result['total_qty'];
  }
}    