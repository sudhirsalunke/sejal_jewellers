<?php
class Metal_issue_model extends CI_Model {

  function __construct() {
      $this->table_name = "manufacture_metal_issue";
      $this->mi_details = "manufacture_metal_issue_details";
      parent::__construct();
  }

   public function validatepostdata(){

    $data=array();
    $old_post=$_POST;
    $tr = $_POST['metal_issue_details']['cnt'];
 
    $material =$_POST['metal_issue_details']['material_id'];
    $weight =$_POST['metal_issue_details']['weight'];
    $melting =$_POST['metal_issue_details']['melting'];
    $purity =$_POST['metal_issue_details']['purity'];

   foreach ($material as $key => $value) {
        $_POST['metal_issue']['cnt']=@$tr[$key];
        $_POST['metal_issue']['material_id']=@$material[$key];
        $_POST['metal_issue']['weight']=@$weight[$key];
        $_POST['metal_issue']['melting']=@$melting[$key];
        $_POST['metal_issue']['purity']=@$purity[$key];
    $this->form_validation->set_rules($this->config->item('metal_issue_voucher', 'admin_validationrules'));

     if ($this->form_validation->run() == FALSE) {
          $data['party_name'] =strip_tags(form_error('metal_issue[party_name]'));   
          $data['party_type'] =strip_tags(form_error('metal_issue[party_type]'));
          $data['material_id_'.@$tr[$key]] =strip_tags(form_error('metal_issue[material_id]'));
          $data['weight_'.@$tr[$key]] =strip_tags(form_error('metal_issue[weight]'));
          $data['melting_'.@$tr[$key]] =strip_tags(form_error('metal_issue[melting]'));
          $data['purity_'.@$tr[$key]] =strip_tags(form_error('metal_issue[purity]'));
      } 
   }
   $_POST=$old_post;
   return $data;
    
  }

  public function get($filter_status='',$params='',$search='',$limit=''){
    $this->db->select('mei.*,ct.name as type_name,(case when mei.party_type=1 then km.name else c.name end) party_name');
    $this->db->from($this->table_name.' mei');
    $this->db->join('customer_type ct','mei.party_type=ct.id');
    $this->db->join('karigar_master km','mei.party_name=km.id','left');
    $this->db->join('customer c','mei.party_name=c.id','left');
  
    /*if(!empty($search)){
        $this->db->like('mei.party_name',$search);
    }
*/
    if(isset($params['columns']) && !empty($params['columns'])){
      $filter_input=$params['columns'];
      $table_col_name="metal_issue_voucher";
      $this->get_filter_value($filter_input,$table_col_name);
    }
    if($limit == true)
    	$this->db->limit($params['length'],$params['start']);
    $result = $this->db->get()->result_array();
 //echo $this->db->last_query(); print_r($result);exit;
    return $result;
  }


   public function get_voucher_no(){
    $this->db->select('MAX(id) as last_id');
    $result = $this->db->get($this->table_name)->row_array();
    return @$result['last_id']+1;
  }
  private function get_filter_value($filter_input,$table_col_name){
    $column_name=array();  
    $filter_column_name=filter_column_name($table_col_name);
    $sql='';
    $i=0;
     
     //print_r($filter_input);die;
    foreach ($filter_input as $key => $search_value){
       $column_name=$filter_column_name;
       //print_r($search_value['search']['value']);
        if(!empty($search_value['search']['value'])){
          if($i != 0){
            $sql.=' AND  ';
          }
            $sql.=''.$column_name[$key].' like "%'.$search_value['search']['value'].'%" ';
            $i++;
        

         }   
    }

    if(!empty($sql)){  
      $this->db->where($sql);  
    } 
   
       
  }


  public function store(){
    $post = $this->input->post();
    /*create voucher*/
    $voucher_array = array('voucher_date' =>date('Y-m-d'),
                           'party_name' =>$post['metal_issue']['party_name'],
                           'party_type' =>$post['metal_issue']['party_type'],
                           'created_at' =>date('Y-m-d H:i:s'),
                           'updated_at' =>date('Y-m-d H:i:s'),
                           'status'=>0,
                            );
    $this->db->insert($this->table_name,$voucher_array);
    $voucher_no = $this->db->insert_id();
    $metal_issue_details =$post['metal_issue_details'];
    $insert_array=array();
    foreach ($metal_issue_details['material_id'] as $key => $value) {
      $insert_array[$key]['metal_issue_id']=$voucher_no;
      $insert_array[$key]['material_id']=$value;
      $insert_array[$key]['weight']= $metal_issue_details['weight'][$key];
      $insert_array[$key]['melting']= $metal_issue_details['melting'][$key];
      $insert_array[$key]['purity']= $metal_issue_details['purity'][$key];
      $insert_array[$key]['created_at']= date('Y-m-d H:i:s');
      $insert_array[$key]['updated_at']= date('Y-m-d H:i:s');
      $insert_array[$key]['status']= 0;
    }
    if($this->db->insert_batch($this->mi_details,$insert_array)){
      return get_successMsg();
    }else{
      return get_errorMsg();
    }
  }

  public function find($id){
    $this->db->select('mi.*,ct.name as type_name,(case when mi.party_type=1 then km.name else c.name end) party_name');
    $this->db->where('mi.id',$id);
    $this->db->from($this->table_name.' mi');
    $this->db->join('customer_type ct','mi.party_type=ct.id');
    $this->db->join('karigar_master km','mi.party_name=km.id','left');
    $this->db->join('customer c','mi.party_name=c.id','left');
    return $this->db->get()->row_array();
  }

  public function voucher_details($voucher_no){
    $this->db->select('mi.*,mm.short_code as bc_name');
    $this->db->where('mi.metal_issue_id',$voucher_no);
    $this->db->from($this->mi_details.' mi');
    $this->db->join('material_master mm','mi.material_id=mm.id');
    return $this->db->get()->result_array();
  }

  
} //class 