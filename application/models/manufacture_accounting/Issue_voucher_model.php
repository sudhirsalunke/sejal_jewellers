<?php
class Issue_voucher_model extends CI_Model {

  function __construct() {
      $this->table_name = "manufacture_issue_voucher";
      $this->miv_details = "manufacture_issue_voucher_details";
      parent::__construct();
  }

   public function validatepostdata(){
    $data=array();
    $old_post=$_POST;
    $tr = $_POST['voucher_details']['cnt'];
    $buying_complexity =$_POST['voucher_details']['buying_complexity_id'];
    $net_wt =$_POST['voucher_details']['net_wt'];
    $gr_wt =$_POST['voucher_details']['gr_wt'];
    $stone =$_POST['voucher_details']['stone'];
    $melting =$_POST['voucher_details']['melting'];
    $purity =$_POST['voucher_details']['purity'];
    $touch =$_POST['voucher_details']['touch'];
    $amount =$_POST['voucher_details']['amount'];

   foreach ($buying_complexity as $key => $value) {
        $_POST['voucher']['cnt']=@$tr[$key];
        $_POST['voucher']['buying_complexity_id']=@$buying_complexity[$key];
        $_POST['voucher']['net_wt']=@$net_wt[$key];
        $_POST['voucher']['gr_wt']=@$gr_wt[$key];
        $_POST['voucher']['stone']=@$stone[$key];
        $_POST['voucher']['melting']=@$melting[$key];
        $_POST['voucher']['purity']=@$purity[$key];
        $_POST['voucher']['touch']=@$touch[$key];
        $_POST['voucher']['amount']=@$amount[$key];
    $this->form_validation->set_rules($this->config->item('issue_voucher', 'admin_validationrules'));

     if ($this->form_validation->run() == FALSE) {
          $data['party_name'] =strip_tags(form_error('voucher[party_name]'));
          $data['party_type'] =strip_tags(form_error('voucher[party_type]'));
          $data['buying_complexity_id_'.@$tr[$key]] =strip_tags(form_error('voucher[buying_complexity_id]'));
          $data['net_wt_'.@$tr[$key]] =strip_tags(form_error('voucher[net_wt]'));
          $data['gr_wt_'.@$tr[$key]] =strip_tags(form_error('voucher[gr_wt]'));
          $data['stone_'.@$tr[$key]] =strip_tags(form_error('voucher[stone]'));
          $data['melting_'.@$tr[$key]] =strip_tags(form_error('voucher[melting]'));
          $data['purity_'.@$tr[$key]] =strip_tags(form_error('voucher[purity]'));
          $data['touch_'.@$tr[$key]] =strip_tags(form_error('voucher[touch]'));
          $data['amount_'.@$tr[$key]] =strip_tags(form_error('voucher[amount]'));
      } 
   }

   $_POST=$old_post;
   return $data;
    
  }

  public function get($filter_status='',$params='',$search='',$limit=''){
    $this->db->select('miv.*,ct.name as type_name,(case when miv.party_type=1 then km.name else c.name end) party_name');
    $this->db->from($this->table_name.' miv');
    $this->db->join('customer_type ct','miv.party_type=ct.id');
    $this->db->join('karigar_master km','miv.party_name=km.id','left');
    $this->db->join('customer c','miv.party_name=c.id','left');
  
   /* if(!empty($search)){
        $this->db->like('miv.id',$search);
    }*/
    if(isset($params['columns']) && !empty($params['columns'])){
      $filter_input=$params['columns'];
      $table_col_name="issue_voucher";
      $this->get_filter_value($filter_input,$table_col_name);
    }

    if($limit == true)
    	$this->db->limit($params['length'],$params['start']);
    $result = $this->db->get()->result_array();
   // echo $this->db->last_query(); print_r($result);exit;
    return $result;
  }
  private function get_filter_value($filter_input,$table_col_name){
    $column_name=array();  
    $filter_column_name=filter_column_name($table_col_name);
    $sql='';
    $i=0;
     
     //print_r($filter_input);die;
    foreach ($filter_input as $key => $search_value){
       $column_name=$filter_column_name;
       //print_r($search_value['search']['value']);
        if(!empty($search_value['search']['value'])){
          if($i != 0){
            $sql.=' AND  ';
          }
            $sql.=''.$column_name[$key].' like "%'.$search_value['search']['value'].'%" ';
            $i++;
        

         }   
    }

    if(!empty($sql)){  
      $this->db->where($sql);  
    } 
   
       
  }

   public function get_voucher_no(){
    $this->db->select('MAX(id) as last_id');
    $result = $this->db->get($this->table_name)->row_array();
    return @$result['last_id']+1;
  }

  public function store(){
    $post = $this->input->post();
    /*create voucher*/
    $voucher_array = array('voucher_date' =>date('Y-m-d'),
                           'party_name' =>$post['voucher']['party_name'],
                           'party_type' =>$post['voucher']['party_type'],
                           'created_at' =>date('Y-m-d H:i:s'),
                           'updated_at' =>date('Y-m-d H:i:s'),
                           'status'=>0,
                            );
    $this->db->insert($this->table_name,$voucher_array);
    $voucher_no = $this->db->insert_id();
    $voucher_details =$post['voucher_details'];
    $insert_array=array();
    foreach ($voucher_details['buying_complexity_id'] as $key => $value) {
      $insert_array[$key]['miv_id']=$voucher_no;
      $insert_array[$key]['buying_complexity_id']=$value;
      $insert_array[$key]['gr_wt']= $voucher_details['gr_wt'][$key];
      $insert_array[$key]['net_wt']= $voucher_details['net_wt'][$key];
      $insert_array[$key]['stone']= $voucher_details['stone'][$key];
      $insert_array[$key]['melting']= $voucher_details['melting'][$key];
      $insert_array[$key]['touch']= $voucher_details['touch'][$key];
      $insert_array[$key]['purity']= $voucher_details['purity'][$key];
      $insert_array[$key]['amount']= $voucher_details['amount'][$key];
      $insert_array[$key]['created_at']= date('Y-m-d H:i:s');
      $insert_array[$key]['updated_at']= date('Y-m-d H:i:s');
      $insert_array[$key]['status']= 0;
    }
    if($this->db->insert_batch($this->miv_details,$insert_array)){
      return get_successMsg();
    }else{
      return get_errorMsg();
    }
  }

  public function find($id){
    $this->db->select('iv.*,ct.name as type_name,(case when iv.party_type=1 then km.name else c.name end) party_name');
    $this->db->where('iv.id',$id);
    $this->db->from($this->table_name.' iv');
    $this->db->join('customer_type ct','iv.party_type=ct.id');
    $this->db->join('karigar_master km','iv.party_name=km.id','left');
    $this->db->join('customer c','iv.party_name=c.id','left');
    return $this->db->get()->row_array();
  }

  public function voucher_details($voucher_no){
    $this->db->select('miv.*,bcm.name as bc_name');
    $this->db->where('miv_id',$voucher_no);
    $this->db->from($this->miv_details.' miv');
    $this->db->join('buying_complexity_master bcm','miv.buying_complexity_id=bcm.id');
    return $this->db->get()->result_array();
  }

  public function get_party_list(){
    if (!empty($_POST['customer_type_id'])) {
        if ($_POST['customer_type_id']==1) {
          return  $this->db->get_where('karigar_master',array('customer_type_id'=>1))->result_array();
        }else{
          return  $this->db->get_where('customer',array('customer_type_id'=>$_POST['customer_type_id']))->result_array();
        }
    }else{
      return get_errorMsg();
    }
  }

  
} //class 