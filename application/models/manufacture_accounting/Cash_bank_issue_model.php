<?php
class Cash_bank_issue_model extends CI_Model {

  function __construct() {
      $this->table_name = "manufacture_cashbannk_voucher";
      parent::__construct();
  }

   public function validatepostdata(){
   
    $this->form_validation->set_rules($this->config->item('cash_bank_voucher', 'admin_validationrules'));

  if ($this->form_validation->run() == FALSE) {
      return FALSE;
   }else{
      return true;
   }
    
  }

  public function get($filter_status='',$params='',$search='',$limit=''){
    $this->db->select('mcv.*,ct.name as type_name,(case when mcv.party_type=1 then km.name else c.name end) party_name');
    $this->db->from($this->table_name.' mcv');
    $this->db->join('customer_type ct','mcv.party_type=ct.id');
    $this->db->join('karigar_master km','mcv.party_name=km.id','left');
    $this->db->join('customer c','mcv.party_name=c.id','left');
  
  /*  if(!empty($search)){
        $this->db->like('mcv.party_name',$search);
    }*/
     if(isset($params['columns']) && !empty($params['columns'])){
      $filter_input=$params['columns'];
      $table_col_name="cash_bank_voucher";
      $this->get_filter_value($filter_input,$table_col_name);
    }

    if($limit == true)
    	$this->db->limit($params['length'],$params['start']);
    $result = $this->db->get()->result_array();
   // echo $this->db->last_query(); print_r($result);exit;
    return $result;
  }
  private function get_filter_value($filter_input,$table_col_name){
    $column_name=array();  
    $filter_column_name=filter_column_name($table_col_name);
    $sql='';
    $i=0;
     
     //print_r($filter_input);die;
    foreach ($filter_input as $key => $search_value){
       $column_name=$filter_column_name;
       //print_r($search_value['search']['value']);
        if(!empty($search_value['search']['value'])){
          if($i != 0){
            $sql.=' AND  ';
          }
            $sql.=''.$column_name[$key].' like "%'.$search_value['search']['value'].'%" ';
            $i++;
        

         }   
    }

    if(!empty($sql)){  
      $this->db->where($sql);  
    } 
   
       
  }

   

  public function store(){
    $post = $this->input->post('cash_bank');
      $insert_array=$post;
      $insert_array['created_at']= date('Y-m-d H:i:s');
      $insert_array['updated_at']=date('Y-m-d H:i:s');
      $insert_array['status']= 0;
      
      if($this->db->insert($this->table_name,$insert_array)){
        return get_successMsg();
      }else{
        return get_errorMsg();
      }
  }

  public function find($id){
   $this->db->select('mcv.*,ct.name as type_name,(case when mcv.party_type=1 then km.name else c.name end) party_name');
    $this->db->where('mcv.id',$id);
    $this->db->from($this->table_name.' mcv');
    $this->db->join('customer_type ct','mcv.party_type=ct.id');
    $this->db->join('karigar_master km','mcv.party_name=km.id','left');
    $this->db->join('customer c','mcv.party_name=c.id','left');
    return $this->db->get()->row_array();
  }

  

  
} //class 