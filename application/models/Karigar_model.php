<?php

class Karigar_model extends CI_Model {

  function __construct() {
      $this->table_name = "karigar_master";
      parent::__construct();
  }

  public function validatepostdata(){
  	$this->form_validation->set_message('check_karigar_code', 'Code is already exists');
  	$this->form_validation->set_rules($this->config->item('Karigar', 'admin_validationrules'));
     if ($this->form_validation->run() == FALSE) {       	
          return FALSE;
      } else {
          return TRUE;
      }
  }
  public function store($postdata=''){
  	$postdata = $this->input->post('Karigar');
  	$encrption = $this->data_encryption->get_encrypted_id($this->table_name);

     $insert_array = $postdata;
     $insert_array['encrypted_id']=$encrption;
     $insert_array['updated_at']=date('Y-m-d H:i:s');
     $insert_array['created_at']=date('Y-m-d H:i:s');
  	// $insert_array = array(
  	// 	'name' => $postdata['name'],
  	// 	'code' => $postdata['code'],
   //    'address' => $postdata['address'],
   //    'contact_person' => $postdata['contact_person'],
  	// 	'encrypted_id' => $encrption,
  	// 	'updated_at' => date('Y-m-d H:i:s'),
  	// 	'created_at' => date('Y-m-d H:i:s')
  	// 	);
  	if($this->db->insert($this->table_name,$insert_array)){
  		return get_successMsg();
  	}else{
  		return get_errorMsg();
  	}
  }
  public function get($filter_status='',$status='',$params='',$search='',$limit=''){
    $this->db->select('*');
    $this->db->from($this->table_name);
    /*if(!empty($filter_status)){
       $this->db->order_by($status[$filter_status['column']],$filter_status['dir']);
     }*/
    if(!empty($search)){
      $this->db->like('name',$search);
      $this->db->or_like('code',$search);
    }
    if($limit == true){
      $this->db->order_by('name','ASC');
      if(!empty($params)){
      $this->db->limit($params['length'],$params['start']);
      }
      $result = $this->db->get()->result_array();
      //print_r($result);die;
      
    }else{
      $row_array = $this->db->get()->result_array();
      $result = count($row_array);
    
    }
    return $result;
  }

  public function find($id){
  	$this->db->where('id',$id);
  	$result = $this->db->get($this->table_name)->row_array();
  	return $result;
  }

  public function get_primary_key($encrypted_key){
  	$this->db->where('encrypted_id',$encrypted_key);
  	$result = $this->db->get($this->table_name)->row_array();
  	return $result['id'];
  }
  public function update(){
  	$postdata = $this->input->post('Karigar');
  	$id = $this->get_primary_key($postdata['encrypted_id']);
    $update_array=$postdata;
    $update_array['updated_at']=date('Y-m-d H:i:s');
  	// $update_array = array(
  	// 	'name' => $postdata['name'],
  	// 	'code' => $postdata['code'],
   //    'address' => $postdata['address'],
   //    'contact_person' => $postdata['contact_person'],
  	// 	'updated_at' => date('Y-m-d H:i:s')
  	// 	);
  	$this->db->where('id',$id);
  	if($this->db->update($this->table_name,$update_array)){
  		return get_successMsg();
  	}else{
  		return get_errorMsg();
  	}
  }
  public function delete($id){
  	$this->db->where('id',$id);
  	if($this->db->delete($this->table_name)){
  		return get_successMsg();
  	}else{
  		return get_errorMsg();
  	}
  }

  public function get_karigar_name(){
        $this->db->select('name');
        $result = $this->db->get($this->table_name)->result_array();
        return array_column($result, 'name');
  }
  public function get_karigar_by_code($code){
    $this->db->select('id');
    $this->db->where('code',$code);
    $result = $this->db->get($this->table_name)->row_array();
    return $result;
  }
  public function get_all_karigar(){
    $this->db->select('km.*');
    $this->db->from($this->table_name.' km');
    $this->db->join('prepare_kundan_karigar_order po','po.karigar_id = km.id');
    $this->db->where('po.status','1');
    $this->db->group_by('km.id');
    $result = $this->db->get()->result_array();
    return $result;
  }

    public function find_name($name){
    $this->db->select('id as karigar_id');
    $this->db->where('name',$name);
    $result = $this->db->get($this->table_name)->row_array();
    return $result;
  }

  public function merge_replace($id,$karigar_id){
    $update_array = array('customer_orders' => 'karigar_id', 
                        'customer_order_assign' =>'karigar_id' ,
                        'department_ready_product' =>'karigar_id' ,
                        'Karigar_manufacturing_mapping' =>'karigar_id' ,
                        'prepare_kundan_karigar_order' =>'karigar_id' ,
                        'Prepare_product_list' =>'karigar_id' ,
                        'product' =>'karigar_id' ,
                        'quality_control' =>'karigar_id' ,
                        'quality_control_hallmarking' =>'karigar_id' ,
                        'Receive_products' =>'karigar_id' ,
                        'repair_product' =>'karigar_id' ,
                        'stock' =>'karigar_id' ,
                        'tag_products' =>'karigar_id' 
                        );
      //print_r($update_array);
      $this->db->select('name');
      $this->db->from($this->table_name);
      $this->db->where('id',$id);
      $new_name = $this->db->get()->row();
     // print_r($new_name);
      $this->db->select('name');
      $this->db->from($this->table_name);
      $this->db->where('id',$karigar_id);
      $old_name = $this->db->get()->row();
     /* print_r($old_name);die;
       merge_karigar_detail*/
       $insert_array=array('old_k_id'=>$karigar_id,
                          'old_k_name'=>$old_name->name,
                          'new_k_id'=>$id,
                          'new_k_name'=>$new_name->name,
                          'updated_at'=>date("Y-m-d H:i:s")
                          );
       //print_r( $insert_array);die;
      $this->db->insert('merge_karigar_detail',$insert_array);

      foreach ($update_array as $key => $value) {
       //print_r($value);
        $this->db->set($value,$id);
        $this->db->where($value,$karigar_id);
        $this->db->update($key);
       // echo $this->db->last_query();
      }//die;
      $this->db->where('id',$karigar_id);
      if($this->db->delete($this->table_name)){
        return get_successMsg();
      }else{
        return get_errorMsg();
      }

  }

  
}    