<?php

class Receipt_model extends CI_Model {

  function __construct() {
      parent::__construct();
      $this->table_name="accounting";
      $this->load->library('excel');
  }


  public function validatepostdata(){
    $data=array();
    $old_post=$_POST;
    $tr = $_POST['accounting_details']['cnt'];
    $category_id =$_POST['accounting_details']['category_id'];
    $gr_wt =$_POST['accounting_details']['gr_wt'];
    $stone =$_POST['accounting_details']['stone'];
    $net_wt =$_POST['accounting_details']['net_wt'];
    $melting =$_POST['accounting_details']['melting'];
    $touch =$_POST['accounting_details']['touch'];
    $purity =$_POST['accounting_details']['purity'];
    $gold =$_POST['accounting_details']['gold'];
    $gold_amt =$_POST['accounting_details']['gold_amt'];
    $lab =$_POST['accounting_details']['lab'];
    $lab_amt =$_POST['accounting_details']['lab_amt'];
    $total_amt =$_POST['accounting_details']['total_amt'];
     
    if (@$_POST['accounting']['payment_type']==1) {
   $this->form_validation->set_rules('gst', 
                                    'GST', 
                                    'trim|required|numeric|greater_than_equal_to[0]',
                                    array('numeric' =>"Please Enter Numeric value",
                                              'greater_than_equal_to'=>"Please Enter Value greater than 0")
                                       );
  }

  if(!empty(@$_POST['accounting']['rate'])) {
    $this->form_validation->set_rules('accounting[rate]', 
                                    'Rate', 
                                    'trim|required|numeric',
                                    array('numeric' =>"Please Enter Numeric value")
                                       );
  }  

   foreach ($category_id as $key => $value) {
        $_POST['ac_details']['cnt']=@$tr[$key];
        $_POST['ac_details']['category_id']=@$category_id[$key];
        $_POST['ac_details']['gr_wt']=@$gr_wt[$key];
        $_POST['ac_details']['stone']=@$stone[$key];
        $_POST['ac_details']['net_wt']=@$net_wt[$key];
        $_POST['ac_details']['melting']=@$melting[$key];
        $_POST['ac_details']['touch']=@$touch[$key];
        $_POST['ac_details']['purity']=@$purity[$key];
        $_POST['ac_details']['gold']=@$gold[$key];
        $_POST['ac_details']['gold_amt']=@$gold_amt[$key];
        $_POST['ac_details']['lab']=@$lab[$key];
        $_POST['ac_details']['lab_amt']=@$lab_amt[$key];
        $_POST['ac_details']['total_amt']=@$total_amt[$key];
    $this->form_validation->set_rules($this->config->item('accounting_details', 'admin_validationrules'));

     if ($this->form_validation->run() == FALSE) {
          $data['type'] =strip_tags(form_error('accounting[type]'));
          $data['karigar_customer_id'] =strip_tags(form_error('accounting[karigar_customer_id]'));
          $data['rate'] =strip_tags(form_error('accounting[rate]'));
          $data['gst'] =strip_tags(form_error('gst'));
          $data['payment_type'] =strip_tags(form_error('accounting[payment_type]'));
          $data['rate_type'] =strip_tags(form_error('accounting[rate_type]'));


          $data['category_id_'.@$tr[$key]] =strip_tags(form_error('ac_details[category_id]'));
          $data['gr_wt_'.@$tr[$key]] =strip_tags(form_error('ac_details[gr_wt]'));
          $data['stone_'.@$tr[$key]] =strip_tags(form_error('ac_details[stone]'));
          $data['net_wt_'.@$tr[$key]] =strip_tags(form_error('ac_details[net_wt]'));
          $data['melting_'.@$tr[$key]] =strip_tags(form_error('ac_details[melting]'));
          $data['touch_'.@$tr[$key]] =strip_tags(form_error('ac_details[touch]'));
          $data['purity_'.@$tr[$key]] =strip_tags(form_error('ac_details[purity]'));
          $data['gold_'.@$tr[$key]] =strip_tags(form_error('ac_details[gold]'));
          $data['gold_amt_'.@$tr[$key]] =strip_tags(form_error('ac_details[gold_amt]'));
          $data['lab_'.@$tr[$key]] =strip_tags(form_error('ac_details[lab]'));
          $data['lab_amt_'.@$tr[$key]] =strip_tags(form_error('ac_details[lab_amt]'));
          $data['total_amt_'.@$tr[$key]] =strip_tags(form_error('ac_details[total_amt]'));
      } 

   }

   $_POST=$old_post;
   return $data;
    
  }

    public function get($filter_status='',$params='',$search='',$limit=''){
	
      $this->db->select('ac.*,k.name as karigar_name,c.name as c_name');
	    $this->db->from($this->table_name.' ac');
	    $this->db->join('karigar_master k','ac.karigar_customer_id = k.id AND ac.type=1','left');
      $this->db->join('customer c','ac.karigar_customer_id = c.id AND ac.type=2','left');
	    $this->db->where('table_type',2);
	 /*   if(!empty($search)){
        $this->db->where("(c.name like '%".$search."%' OR k.name like '%".$search."%')");    
	    }*/
      if(isset($params['columns'][3]['search']['value']) && !empty($params['columns'][3]['search']['value'])){
        $this->db->where("(c.name like '%".$params['columns'][3]['search']['value']."%' OR k.name like '%".$params['columns'][3]['search']['value']."%')");    
      }else  if(isset($params['columns']) && !empty($params['columns']) ){
      $filter_input=$params['columns'];
      $table_col_name="accounting";
      $this->get_filter_value($filter_input,$table_col_name);
      }

	   
    if($limit == true){
      $this->db->limit($params['length'],$params['start']);
      $result = $this->db->get()->result_array();
    }else{
      $row_array = $this->db->get()->result_array();
      $result = count($row_array);
      //echo $this->db->last_query();print_r($result);exit;
    }

    return $result;

  	}
     private function get_filter_value($filter_input,$table_col_name){
    $column_name=array();  
    $filter_column_name=filter_column_name($table_col_name);
    $sql='';
    $i=0;
     
     //print_r($filter_input);die;
    foreach ($filter_input as $key => $search_value){
       $column_name=$filter_column_name;
       //print_r($search_value['search']['value']);
        if(!empty($search_value['search']['value'])){
          if($i != 0){
            $sql.=' AND  ';
          }
            $sql.=''.$column_name[$key].' like "%'.$search_value['search']['value'].'%" ';
            $i++;
        

         }   
    }

    if(!empty($sql)){  
      $this->db->where($sql);  
    } 
       
  }

  public function store(){
  	$postdata=$this->input->post('accounting');
  	$postdata['created_at']=$postdata['updated_at']=date('Y-m-d H:i:s');
    $postdata['voucher_date']=date('Y-m-d');
    $postdata['table_type']=2;
  	if($this->db->insert($this->table_name,$postdata)){
      $ac_id=$this->db->insert_id();
      $details = $this->input->post('accounting_details');

      foreach ($details['category_id'] as $key => $value) {
        $insert_array[$key]['category_id']=$details['category_id'][$key];
        $insert_array[$key]['gr_wt']=$details['gr_wt'][$key];
        $insert_array[$key]['stone']=$details['stone'][$key];
        $insert_array[$key]['net_wt']=$details['net_wt'][$key];
        $insert_array[$key]['melting']=$details['melting'][$key];
        $insert_array[$key]['touch']=$details['touch'][$key];
        $insert_array[$key]['purity']=$details['purity'][$key];
        $insert_array[$key]['gold']=$details['gold'][$key];
        $insert_array[$key]['gold_amt']=$details['gold_amt'][$key];
        $insert_array[$key]['lab']=$details['lab'][$key];
        $insert_array[$key]['lab_amt']=$details['lab_amt'][$key];
        $insert_array[$key]['total_amt']=$details['total_amt'][$key];
        $insert_array[$key]['gst_percentage']=$_POST['gst'];
        $insert_array[$key]['accounting_id']=$ac_id;
        $insert_array[$key]['created_at']=$insert_array[$key]['updated_at']=date('Y-m-d H:i:s');
        $insert_array[$key]['table_type']=2;
      }
     
      $this->db->insert_batch('accounting_details',$insert_array);
  		  $response =get_successMsg();
        $response['voucher_no']=$ac_id;
      return  $response;
	}else{
	  	return get_errorMsg();
	}
  }

public function get_total_by_voucher($voucher_no){
    $this->db->select('SUM(gr_wt) as gross_weight,SUM(purity) as pure');
    $this->db->where('accounting_id',$voucher_no);
    $result = $this->db->get('accounting_details')->row_array();
    return $result;
  }


  public function find($id){
  	$this->db->select('*');
    $this->db->from($this->table_name);
    $this->db->where('id',$id);
  	$result = $this->db->get()->row_array();
  	return $result;
  }

  public function get_voucher_no(){
    $this->db->select('MAX(id) as last_id');
    $result = $this->db->get($this->table_name)->row_array();
    return @$result['last_id']+1;
  }

  public function get_voucher_details($voucher_no){
     $this->db->select('aid.*,cm.name as category_name');
     $this->db->where('aid.accounting_id',$voucher_no);
     $this->db->from('accounting_details aid');
     $this->db->join('category_master cm','aid.category_id=cm.id');
     return $this->db->get()->result_array();     
  }

  public function get_accounting($voucher_id){
    $this->db->select('ac.*,(case when ac.type=1 then k.name else c.name end) as karigar_customer_name');
    $this->db->from($this->table_name.' ac');
    $this->db->join('karigar_master k','ac.karigar_customer_id = k.id AND ac.type=1','left');
    $this->db->join('customer c','ac.karigar_customer_id = c.id AND ac.type=2','left');
    $this->db->where('ac.id',$voucher_id);
    $result = $this->db->get()->row_array();
    return $result;
  }

  function genrate_excel($data){
    $accounting=$data['accounting'];
    $accounting_details=$data['accounting_details'];

    // Create new Spreadsheet object
    $spreadsheet = new PHPExcel();
    // Set document properties
   

    $spreadsheet->setActiveSheetIndex(0)->mergeCells('B2:N2')->setCellValue('B2', 'RECEIPT VOUCHER')
            ->setCellValue('B3', 'Type')
            ->setCellValue('C3',($accounting['type']==1) ? 'Karigar' : 'Customer')

            ->setCellValue('F3', 'Name')
            ->setCellValue('G3', $accounting['karigar_customer_name'])

            ->setCellValue('L3', 'Rate')
            ->setCellValue('M3', ($accounting['rate_type']==1) ? 'Standard' : 'Pure')
            ->setCellValue('N3', $accounting['rate'])

            ->setCellValue('B5', 'R. Voucher')
            ->setCellValue('C5', $accounting['id'])

            ->setCellValue('F5', 'DATE')
            ->setCellValue('G5', date('d-m-Y',strtotime($accounting['voucher_date'])))

            ->setCellValue('L5', 'Narration')
            ->setCellValue('N5', $accounting['narration']);
       $row=8;
       
       $spreadsheet->setActiveSheetIndex(0)
                                          ->setCellValue('B'.$row,'Sr No')
                                          ->setCellValue('C'.$row,'Product')
                                          ->setCellValue('D'.$row,'Gross Weight')
                                          ->setCellValue('E'.$row,'Stone')
                                          ->setCellValue('F'.$row,'Net Weight')
                                          ->setCellValue('G'.$row,'Melting')
                                          ->setCellValue('H'.$row,'Touch')
                                          ->setCellValue('I'.$row,'Purity')
                                          ->setCellValue('J'.$row,'Gold@')
                                          ->setCellValue('K'.$row,'Gold Amount')
                                          ->setCellValue('L'.$row,'Lab@')
                                          ->setCellValue('M'.$row,'Lab Amount')
                                          ->setCellValue('N'.$row,'Total Amount');
      
      $row++;

      $gross_amt=$net_wt=$lab_amt=$purity_amt=$stone_amt=$total_amt=0;
      $gst_per = @$accounting_details[0]['gst_percentage'];
      foreach ($accounting_details as $key => $value) {
        $total_amt += $value['total_amt'];
        $gross_amt +=$value['gr_wt'];
        $net_wt += $value['net_wt'];
        $lab_amt +=$value['lab_amt'];
        $purity_amt +=$value['purity'];
        $stone_amt += $value['stone'];
        $spreadsheet->setActiveSheetIndex(0)->setCellValue('B'.$row, $key+1)
                                          ->setCellValue('C'.$row,$value['category_name'])
                                          ->setCellValue('D'.$row,$value['gr_wt'])
                                          ->setCellValue('E'.$row,$value['stone'])
                                          ->setCellValue('F'.$row,$value['net_wt'])
                                          ->setCellValue('G'.$row,$value['melting'])
                                          ->setCellValue('H'.$row,$value['touch'])
                                          ->setCellValue('I'.$row,$value['purity'])
                                          ->setCellValue('J'.$row,$value['gold'])
                                          ->setCellValue('K'.$row,$value['gold_amt'])
                                          ->setCellValue('L'.$row,$value['lab'])
                                          ->setCellValue('M'.$row,$value['lab_amt'])
                                          ->setCellValue('N'.$row,$value['total_amt']);
      $row++;
      
        
      }
      $row++;
      $spreadsheet->setActiveSheetIndex(0)->setCellValue('C'.$row,'GST')
                                          ->setCellValue('D'.$row,$gst_per.' %')
                                          ->setCellValue('N'.$row,round(($total_amt*$gst_per)/100,2));
      $row+=2;
      $spreadsheet->setActiveSheetIndex(0)->setCellValue('C'.$row,'Total')
                                          ->setCellValue('D'.round($gross_amt,2))
                                          ->setCellValue('E'.$row,round($stone_amt,2))
                                          ->setCellValue('F'.$row,round($net_wt,2))
                                          ->setCellValue('I'.$row,round($purity_amt,2))
                                          ->setCellValue('M'.$row,round($lab_amt,2))
                                          ->setCellValue('N'.$row,round($total_amt+(($total_amt*$gst_per)/100),2));

      $spreadsheet->getActiveSheet()->getStyle('B2:N'.$row)->applyFromArray(array('font' => array('name'=>'Bookman Old Style','size' => 10,'color' => array('rgb' => 'FF000000')),'borders' => array(
            'allborders' => array(
              'style' => PHPExcel_Style_Border::BORDER_THIN
            ),
            'outline' => array(
              'style' => PHPExcel_Style_Border::BORDER_THICK
            )
        ),
        'alignment' => array(
                'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER,
              )
        ));

      $spreadsheet->getActiveSheet()->getStyle('B2:N8')->applyFromArray(array(
      'font'  => array(
          'bold'=>true,
              ),
      ));

    for ($i='A'; $i<="N" ; $i++) { 
      $spreadsheet->getActiveSheet()->getColumnDimension($i)->setAutoSize(true);

    }


    // Rename worksheet
    $spreadsheet->getActiveSheet()->setTitle('receipt_voucher');
    // Set active sheet index to the first sheet, so Excel opens this as the first sheet
    $spreadsheet->setActiveSheetIndex(0);
    // Redirect output to a client’s web browser (Xlsx)
    header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
    header('Content-Disposition: attachment;filename="receipt_voucher.xlsx"');
    header('Cache-Control: max-age=0');
    // If you're serving to IE 9, then the following may be needed
    header('Cache-Control: max-age=1');
    // If you're serving to IE over SSL, then the following may be needed
    header('Expires: Mon, 26 Jul 1997 05:00:00 GMT'); // Date in the past
    header('Last-Modified: ' . gmdate('D, d M Y H:i:s') . ' GMT'); // always modified
    header('Cache-Control: cache, must-revalidate'); // HTTP/1.1
    header('Pragma: public'); // HTTP/1.0

    $writer = PHPExcel_IOFactory::createWriter($spreadsheet, 'Excel2007');
    $writer->save('php://output');  
  
  }

} //class