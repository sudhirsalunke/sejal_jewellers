<?php
class Dispatch_mode_model extends CI_Model {

  function __construct() {
      $this->table_name = "dispatch_mode";
      $this->dispatch_mode_data="dispatch_mode_data";
      parent::__construct();
  }

  public function validatepostdata(){
   
  	$this->form_validation->set_rules($this->config->item('dispatch_mode', 'admin_validationrules'));
     if ($this->form_validation->run() == FALSE) {       	
          return FALSE;
      } else {
          return TRUE;
      }
  }
  public function store(){
    $insert_array=array();
  	$postdata = $this->input->post('dispatch_mode');
  	$insert_array = $postdata;
    $insert_array['encrypted_id']=$this->data_encryption->get_encrypted_id();
    $insert_array['updated_at']=date('Y-m-d H:i:s');
    $insert_array['created_at']=date('Y-m-d H:i:s');
  		
  	if($this->db->insert($this->table_name,$insert_array)){
  		return get_successMsg();
  	}else{
  		return get_errorMsg();
  	}
  }
  public function get($filter_status='',$status='',$params='',$search='',$limit=''){
    $this->db->select('*');
    $this->db->from($this->table_name);
    // if(!empty($filter_status))
    //    $this->db->order_by($status[$filter_status['column']],$filter_status['dir']);
   /* if(!empty($search)){
        $this->db->like('mode_name',$search);
    }*/
     if(isset($params['columns']) && !empty($params['columns'])){
      $filter_input=$params['columns'];
      $table_col_name="dispatch_mode_table";
      $this->get_filter_value($filter_input,$table_col_name);
    }
    if($limit == true){
       if(!empty($params)){
      $this->db->limit($params['length'],$params['start']);
      }
      $result = $this->db->get()->result_array();
    }else{
      $row_array = $this->db->get()->result_array();
      $result = count($row_array);
      //echo $this->db->last_query();print_r($result);exit;
    }
    return $result;
  }
  private function get_filter_value($filter_input,$table_col_name){
    $column_name=array();  
    $filter_column_name=filter_column_name($table_col_name);
    $sql='';
    $i=0;
     
     //print_r($filter_input);die;
    foreach ($filter_input as $key => $search_value){
       $column_name=$filter_column_name;
       //print_r($search_value['search']['value']);
        if(!empty($search_value['search']['value'])){
          if($i != 0){
            $sql.=' AND  ';
          }
            $sql.=''.$column_name[$key].' like "%'.$search_value['search']['value'].'%" ';
            $i++;
        

         }   
    }

    if(!empty($sql)){  
      $this->db->where($sql);  
    } 
       
  }

  public function find_by_encrypted_id($encrypted_id){
   $this->db->where('encrypted_id',$encrypted_id);
   $result = $this->db->get($this->table_name)->row_array();
   return $result;
  }
  public function find($id){
  	$this->db->where('id',$id);
  	$result = $this->db->get($this->table_name)->row_array();
  	return $result;
  }

  public function dispatch_mode_data($id){
    $this->db->where('dispatch_mode_id',$id);
   return $this->db->get($this->dispatch_mode_data)->result_array();
  }

  public function update(){
    $postdata =$this->input->post('dispatch_mode');
  	$update_array = array(
  		'name' => $postdata['name'],
  		'code' => $postdata['code'],
  		'updated_at' => date('Y-m-d H:i:s')
  		);
  	$this->db->where('encrypted_id',$postdata['encrypted_id']);
  	if($this->db->update($this->table_name,$update_array)){
  		return get_successMsg();
  	}else{
  		return get_errorMsg();
  	}
  }
  public function delete($id){
  	$this->db->where('encrypted_id',$id);
  	if($this->db->delete($this->table_name)){
  		return get_successMsg();
  	}else{
  		return get_errorMsg();
  	}
  }

  public function check_hc_used($id){
      $this->db->where('hc_id',$id);
      $this->db->from('quality_control');
     return $this->db->count_all_results();
 }

 public function store_mode_data($input){
   if ($this->db->insert($this->dispatch_mode_data,$input)) {
     return get_successMsg();
   }else{
    return get_errorMsg();
   }
 }

  public function get_dispatch_mode_data(){
   // $this->db->select('dm.mode_name,dm.mode_type,dmp.*');
    $response_array=array();
    $this->db->from($this->table_name.' dm');
    $this->db->join($this->dispatch_mode_data.' dmp','dm.encrypted_id=dmp.dispatch_mode_id','left');
    $result = $this->db->get()->result_array();
    foreach ($result as $key => $value) {
      $response_array[$value['encrypted_id']][]=$value;
    }
   // print_r($response_array);exit;
   return $response_array;
  }
}    