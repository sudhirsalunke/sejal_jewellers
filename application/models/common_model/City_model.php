<?php
class City_model extends CI_Model {

  function __construct() {
      $this->table_name = "city";
      parent::__construct();
  }

  function get(){
    $this->db->where('state_id',$_POST['state_id']);
   return $this->db->get($this->table_name)->result_array();
  }
  
} //class 