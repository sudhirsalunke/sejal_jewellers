<?php
class State_model extends CI_Model {

  function __construct() {
      $this->table_name = "state";
      parent::__construct();
  }

  function get(){
    $this->db->where('country_id',101);
    return $this->db->get($this->table_name)->result_array();
  }
  
}