<?php

class Product_model extends CI_Model {

  function __construct() {
    $this->table_name = "product";
    parent::__construct();
    $this->insert_array = array();
  }
  public function validatepostdata($insert_array = '',$from_excel=false){ 
    if($from_excel == true){
      $_POST['product'] = $insert_array;
    }
  	$this->form_validation->set_message('check_product_code', 'Code is already exists');
  	$this->form_validation->set_rules($this->config->item('product', 'admin_validationrules'));

      //print_r($_FILES['product']);
   // $allowed_ext=array('jpg','jpeg','png');
   // $image_validation=product_validate_images($_FILES['product'],$allowed_ext);
    //print_r($image_validation);
   /* if(!empty(trim($_POST['product']['sub_category_id'])) && !is_null($_POST['product']['sub_category_id'])){
      $this->form_validation->set_rules($this->config->item('product_sub_category', 'admin_validationrules'));
    }*/   

    if(!empty($_FILES['product'])){
      $allowed_ext=array('jpg','jpeg','png');
      $image_validation=product_validate_images($_FILES['product'],$allowed_ext);
    }else{
      $image_validation['status'] = true;
    }

    $data['status']= 'success';
    if($this->form_validation->run()===FALSE){
      $data['status']= 'failure';
      $data['data']= '';
      $data['error'] = $this->getErrorMsg();
  
    }
    return $data;
  }

  public function store($insert_array,$from_excel=false){
    if(!empty($insert_array[0])){
      foreach ($insert_array as $key => $value) {

      $_POST['product'] = $this->insert_array =  $value;
      if(!empty($_FILES['file'])){
        $ext = pathinfo($_FILES['file']['name'], PATHINFO_EXTENSION);
        $this->insert_array['image'] =upload_images("product",$_POST['product']['product_code'],$_POST['product']['product_code'].'.'.$ext,$_FILES['file']['tmp_name']);
        
      }
    // print_r($insert_array); die;
        $sub_category_id = $this->get_sub_category_id($value['sub_category_id']);
        if(!empty($sub_category_id))
        {
          $this->insert_array['sub_category_id'] = $sub_category_id['id'];
          $this->insert_array['category_id'] = $sub_category_id['category_id'];
        }else{
          $this->insert_array['sub_category_id'] =$value['weight_band_id'];  
          $this->insert_array['category_id'] = $value['category_id'];        
        }

        $weight_range_id = $this->get_weight_range_id($value['weight_band_id']);
        if(!empty(trim($weight_range_id)))
        {
          $this->insert_array['weight_band_id'] = $weight_range_id;
        }
        else
        {
          $this->insert_array['weight_band_id'] ='';
          $this->insert_array['actual_wt'] = $value['weight_band_id'];
        }
      $encrption = $this->data_encryption->get_encrypted_id($value['product_code']);
      $this->get_master_values($value);
      $this->insert_array['encrypted_id'] = $encrption;
      $this->insert_array['created_at'] = date('Y-m-d H:i:s');
      if($this->db->insert($this->table_name,$this->insert_array)){
        $this->insert_array = array();
      }else{
       die('not inserted successfully');
      }
    }
    return get_successMsg();
    }
  }
   public function get_sub_category_id($sc_id)
  {   
    $this->db->select('*');
    $this->db->from('sub_category_master');
    $this->db->where('name',$sc_id);
    $query = $this->db->get()->row_array();
    return $query;
  }
  public function get_weight_range_id($wt)
  {
    $weight_range_id = explode('-',$wt);
    $this->db->select('*');
    $this->db->from('weights');
    $whr = '';
    if(!empty($weight_range_id[0]) && !empty($weight_range_id[1])){
      $whr = '(from_weight = '.(float)trim($weight_range_id[0]).' AND to_weight = '.(float)trim($weight_range_id[1]).')';
    }elseif(isset($weight_range_id[0]) && isset($weight_range_id[1]) && $weight_range_id[0] !== '' && $weight_range_id[1] !== ''){
        $whr = '(from_weight = '.(float)trim($weight_range_id[0]).' AND to_weight = '.(float)trim($weight_range_id[1]).')';
    }
    else if(!empty($weight_range_id[0]) && empty($weight_range_id[1])){
      $whr = '(from_weight = '.(float)trim($weight_range_id[0]).')';
    }
    $this->db->where($whr);
    $query = $this->db->get()->row_array();

    //echo $this->db->last_query();
     // echo $query['id']; 
     // echo '<br/>'; 
    return $query['id'];
  }
  public function find($id,$product_code=false){
  	$this->db->where('id',$id);
    if($product_code == true)
      $this->db->or_where('product_code',$id);
  	$result = $this->db->get($this->table_name)->row_array();
  	return $result;
  }

  public function update(){
    if(!empty($_FILES['file'])){
        $ext = strtolower(pathinfo($_FILES['file']['name'], PATHINFO_EXTENSION));
        $_POST['product']['image'] = upload_images("product",$_POST['product']['product_code'],$_POST['product']['product_code'].'.'.$ext,$_FILES['file']['tmp_name']);
      }
    $update_array = $this->input->post('product');
  	$id = $this->get_primary_key($update_array['encrypted_id']);
  	$update_array['updated_at'] = date('Y-m-d H:i:s');
  	
    $weight_range_id = $this->get_weight_range_id($update_array['weight_band_id']);
    if($weight_range_id!='')
    {
      $update_array['weight_band_id'] = $weight_range_id;
    }
    else
    {
      $update_array['actual_wt'] = $update_array['weight_band_id'];
      $update_array['weight_band_id'] ='';
    }
    $this->db->where('id',$id);
  	if($this->db->update($this->table_name,$update_array)){
  		return get_successMsg();
  	}else{
  		return get_errorMsg();
  	}
  }
  public function get($filter_status='',$status='',$params='',$search='',$limit='',$limit1=''){
    $this->db->select('k.name karigar_name,k.code karigar,m.name metal,c.name category,c.code category_code,sc.name sub_category,sc.code sub_category_code,ca.name carat,pm.name as pcs,CONCAT(w.from_weight, "-", w.to_weight) as weight_band,w.from_weight,w.to_weight,a.name article,pt.name manufacturing_type,bc.name as buying_complexity,group_concat(sp.status separator ",") as shortlisted_products,group_concat(cp.status separator ",") as corporate_products,p.*,p.status');
    $this->db->from($this->table_name." p");
    $this->all_joins();
    $this->all_where_conditions();
    $ci = &get_instance();
    $get_url = $ci->uri->segment(2); 

   // print_r($get_url);
    if(@$get_url =='Product_with_images'){    

      $table_col_name='viewProduct';

    }else{
   
      $table_col_name='viewProductwithoutimg';
    }
   if (isset($params['columns']) && !empty($params['columns'])){
      $filter_input=$params['columns'];
      $this->get_filter_value($filter_input,$table_col_name);
    }

/*    if(!empty($filter_status))
       $this->db->order_by($status[$filter_status['column']],$filter_status['dir']);*/

    if(!empty($search)){
        $this->all_like_queries($search);
    }

  

    if($limit1!='')
    {
      $this->db->limit('1');
    }
    //$this->db->limit('10');
/*   
    if($status=='0'){
        $this->db->where('p.status',$status);
        $this->db->where('p.image is not null');
    }*/


//print_r($limit);
  if($limit == true){
      $this->db->group_by('p.id');
      $this->db->order_by('p.id','DESC');
      if(!empty($params)){
      $this->db->limit($params['length'],$params['start']);
      }
      $result = $this->db->get()->result_array();
// echo $this->db->last_query();   print_r($result);exit;
    }else{
      $row_array = $this->db->get()->result_array();
      $result = count($row_array);
      //echo $this->db->last_query(); print_r($row_array); exit;
      //
    } 

/*  echo $this->db->last_query(); 
  print_r($result);
  die();*/
    return $result;
  }
  private function get_filter_value($filter_input,$table_col_name){
    $column_name=array();  
    $filter_column_name=filter_column_name($table_col_name);
    $sql='';
    $i=0;
     
     //print_r($filter_input);die;
    foreach ($filter_input as $key => $search_value){
       $column_name=$filter_column_name;
       //print_r($search_value['search']['value']);
        if(!empty($search_value['search']['value'])){
          if($i != 0){
            $sql.=' AND  ';
          }
            $sql.=''.$column_name[$key].' like "%'.$search_value['search']['value'].'%" ';
            $i++;
        

         }   
    }

    if(!empty($sql)){  
      $this->db->where($sql);  
    }     
       
  }

  public function delete($id){
    $this->db->where('id',$id);
    if($this->db->delete('product')){
      return get_successMsg();
    }else{
      return get_errorMsg();
    }
  }
  private function all_like_queries($search){
    $weight = explode('-', $search);
    if(empty($weight[1])){
      $weight[1] = 'no';
    }
    $this->db->where("(p.product_code LIKE '%$search%' OR DATE_FORMAT( p.created_at,'%d-%m-%Y') LIKE '%$search%' OR k.name LIKE '%$search%' OR  k.code LIKE '%$search%' OR  c.name LIKE '%$search%' OR  c.code LIKE '%$search%' OR  p.size LIKE '%$search%' OR  pt.name LIKE '%$search%' OR  a.name LIKE '%$search%' OR  ca.name LIKE '%$search%' OR  m.name LIKE '%$search%' OR  w.from_weight LIKE '%$weight[0]%' OR  w.to_weight LIKE '%$weight[1]%' OR p.actual_wt LIKE '%$search%' )");

/*$this->db->where("(p.product_code LIKE '%$search%' AND  k.name LIKE '%$search%' AND  c.code LIKE '%$search%' AND  w.from_weight LIKE '%$weight[0]%' AND  w.to_weight LIKE '%$weight[1]%')");
$this->db->or_where("(k.code LIKE '%$search%' OR  c.code LIKE '%$search%' OR  p.size LIKE '%$search%' OR  pt.name LIKE '%$search%' OR  a.name LIKE '%$search%' OR  ca.name LIKE '%$search%' OR  m.name LIKE '%$search%'");*/

  }
   
  private function all_joins(){
    $this->db->join('karigar_master k', 'k.id = p.karigar_id', 'left');
    $this->db->join('category_master c', 'c.id = p.category_id', 'left');
    $this->db->join('sub_category_master sc', 'sc.id = p.sub_category_id', 'left');
    $this->db->join('manufacturing_type_master pt', 'pt.id = p.manufacturing_type_id', 'left');
    $this->db->join('buying_complexity_master bc', 'bc.id = p.buying_complexity_id', 'left');
    $this->db->join('article_master a', 'a.id = p.article_id', 'left');
    $this->db->join('carat_master ca', 'ca.id = p.carat_id', 'left');
    $this->db->join('metal_master m', 'm.id = p.metal_id', 'left');
    $this->db->join('weights w', 'w.id = p.weight_band_id', 'left');
    $this->db->join('shortlisted_products sp', 'sp.product_id = p.id', 'left');
    $this->db->join('pcs_master pm', 'pm.id = p.pcs_id', 'left');
    $this->db->join('corporate_products cp', 'cp.sort_Item_number = p.product_code', 'left');
  }

  public function get_primary_key($encrypted_key){
    $this->db->where('encrypted_id',$encrypted_key);
    $result = $this->db->get($this->table_name)->row_array();
    return $result['id'];
  }
  public function get_product_name(){
    $this->db->select('product_code');
    $result = $this->db->get($this->table_name)->result_array();
    return array_column($result, 'product_code');
  } 

  public function fetch_images_from_dropbox($status){
    $this->db->set('updated_at',date('Y-m-d H:i:s'));
    $this->db->set('status',$status);
    $this->db->where('id',1);
    if($this->db->update('fetch_from_dropbox')){
      return get_successMsg();
    }else{
      return get_errorMsg();
    }
  }

  private function search()
  {
    if(!empty($_POST['category_search']))
      $this->db->like('c.name',$_POST['category_search']);
    if(!empty($_POST['product_code']))
      $this->db->like('p.product_code',$_POST['product_code']);
    if(!empty($_POST['karigar']))
      $this->db->like('k.name',$_POST['karigar']);
    if(!empty($_POST['weight_range'])){
      $weight = explode('-',$_POST['weight_range']);
      $from_weight = $weight[0]; $to_weight = $weight[1];
      $this->db->where('from_weight >=', (float)$from_weight);
      $this->db->where('to_weight <=',(float)$to_weight ); 
     }
  }

  public function update_images($file_name,$code){
      $this->db->where('product_code',$code);
      if($this->db->update($this->table_name,array('image'=>$file_name)))
        return get_successMsg();
      else
        return get_errorMsg();
  }

  private function getErrorMsg(){
    return array(
      'karigar_id'=>strip_tags(form_error('product[karigar_id]')),
      'product_code'=>strip_tags(form_error('product[product_code]')),
      'size'=>strip_tags(form_error('product[size]')),
      'category_id'=>strip_tags(form_error('product[category_id]')),
      'sub_category_id'=>strip_tags(form_error('product[sub_category_id]')),
      //'manufacturing_type_id'=>strip_tags(form_error('product[manufacturing_type_id]')),
      'article_id'=>strip_tags(form_error('product[article_id]')),
      'carat_id'=>strip_tags(form_error('product[carat_id]')),
      'metal_id'=>strip_tags(form_error('product[metal_id]')),
      'weight_band_id'=>strip_tags(form_error('product[weight_band_id]')),
      'buying_complexity_id'=>strip_tags(form_error('product[buying_complexity_id]')),
      'manufacturing_type_id'=>strip_tags(form_error('product[manufacturing_type_id]')),
      'Pcs'=>strip_tags(form_error('product[pcs_id]')),
      'corporate'=>strip_tags(form_error('product[corporate]')),
    );
  }
  private function get_master_values($value){
    $this->insert_array['karigar_id'] = $this->form_validation->check_karigar(true);
    $this->insert_array['category_id'] = $this->form_validation->check_category(true);
    $this->insert_array['sub_category_id'] = $this->form_validation->check_sub_category(true);
    $this->insert_array['carat_id'] = $this->form_validation->check_carat(true);
    $this->insert_array['article_id'] = $this->form_validation->check_article(true);
    $this->insert_array['manufacturing_type_id'] = $this->form_validation->check_manufacturing_type(true);
    //$this->insert_array['weight_band_id'] = $this->form_validation->check_weight_range(true);
    $this->insert_array['metal_id'] = $this->form_validation->check_metal(true);
    $this->insert_array['pcs_id'] = $this->form_validation->check_pcs(true);
    $this->insert_array['buying_complexity_id'] = $this->form_validation->check_buying_complexity(true);
  }

  

  private function all_where_conditions(){
    //print_r($this->input->post('page_title'));
    //$this->db->where('p.status','0');
    if($this->input->post('page_title')=="DESIGNS WITHOUT IMAGES" || $this->input->post('page_title')=="ADD PRODUCTS IMAGES"){
      $this->db->where('p.image',null);
    }
    elseif($this->input->post('page_title')=='DESIGNS WITH IMAGES' || $this->input->post('page_title')=='SEND DESIGNS FOR APPROVAL'){
      $this->db->where('p.image is not null');
     
    }
/*    if(!empty($_REQUEST['columns'])){
      $c_search = array();
        foreach ($_REQUEST['columns'] as $key => $value) {
            if(!empty($value['search']['value'])){
                if($key == 0){
                    $this->db->like('c.name',$value['search']['value']);
                }
                if($key == 1){
                  $weight=explode('-', $value['search']['value']);
                  $this->db->where('w.to_weight',$weight[0]);
                  if(!empty($weight[1]))
                    $this->db->or_where('w.to_weight',$weight[1]);
                  $this->db->or_where('p.actual_wt BETWEEN "'.$weight[0].'" AND "'.$weight[1].'"');
                }
                if($key == 2){
                    $this->db->like('p.product_code',$value['search']['value']);
                }
                if($key == 3){
                    $this->db->like('k.code',$value['search']['value']);
                }
                if($key == 4){
                    $this->db->like('p.corporate',$value['search']['value']);
                }
            }
        }
    }*/
  }
}

?>