<?php
class Accounting_report_model extends CI_Model {

  function __construct() {
      $this->table_details ="accounting_details";
      $this->table_name = "accounting";
      parent::__construct();
  }

  public function get($filter_status='',$params='',$search='',$limit=''){
    $type_filter = @$params['columns'][1]['search']['value'];

    $this->db->select('k.name as k_name,c.name as c_name,ac.type,ac.table_type,GROUP_CONCAT(CASE WHEN table_type=1 THEN ac.id END ) as issue_vouchers,GROUP_CONCAT(CASE WHEN table_type=2 THEN ac.id END ) as receive_vouchers,');
    $this->db->from($this->table_name.' ac');
    $this->db->join('karigar_master k','ac.karigar_customer_id = k.id AND ac.type=1','left');
    $this->db->join('customer c','ac.karigar_customer_id = c.id AND ac.type=2','left');
    $this->db->group_by(array('ac.karigar_customer_id','ac.type'));
      
    if (isset($params['columns'][2]['search']['value']) && !empty($params['columns'][2]['search']['value'])) {

        $this->db->where("(c.name like '%".$params['columns'][2]['search']['value']."%' OR k.name like '%".$params['columns'][2]['search']['value']."%')");    
      }
    if (!empty($type_filter)) {
      $this->db->where('ac.type',$type_filter);
    }
    if($limit == true)
    	$this->db->limit($params['length'],$params['start']);
    $result = $this->db->get()->result_array();
   //echo $this->db->last_query(); print_r($result);exit;
    return $result;
  }

public function get_gross_weight($vouchers){
    $this->db->select('SUM(gr_wt) as gross_weight');
    $this->db->from($this->table_details);
    $this->db->where_IN('accounting_id',explode(',', $vouchers));
    $result = $this->db->get()->row_array();
    return @$result['gross_weight'];

}

public function get_received_weight($vouchers){
    $this->db->select('SUM(gr_wt) as gross_weight');
    $this->db->from($this->table_details);
    $this->db->where_IN('accounting_id',explode(',', $vouchers));
    $result = $this->db->get()->row_array();
    return @$result['gross_weight'];

}

public function issue_weight_indetail($accounting_ids){
  $this->db->select('ad.*,c.name as category_name');
  $this->db->from($this->table_details.' ad');
  $this->db->join('category_master c','ad.category_id=c.id');
  $this->db->where_IN('ad.accounting_id',explode(',', $accounting_ids));
  $result = $this->db->get()->result_array();
  return $result;
}

  
} //class 