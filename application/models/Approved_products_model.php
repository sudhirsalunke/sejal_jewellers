<?php

class Approved_products_model extends CI_Model {

  function __construct() {
      parent::__construct();
  }
  public function get($filter_status='',$status='',$params='',$search='',$limit='',$vendor=''){
    $this->db->select('p.sub_Sr_no,p.UID_No,"" IntendedWH,p.Order_Reff,p.Vendor_Code,p.Vendor_Name,p.product_code Vendor_Design_Code,ca.name karat,a.name article_code,a.description,p.Sub_Product,bc.name buying_complexity,pt.name manufacturing_type,p.size,p.Size_UOM,pc.name Pcs,p.grs_wt,"" total_stn_qty,p.Total_Stn_Cts,p.Net_Wt,p.Net_Order_Wt,CONCAT(w.from_weight, "-", w.to_weight) as wt_band,p.Stone_Name,p.Stone_Qty,p.Stn_Cts,p.Stone_Rate as stone_rate/Cts,p.Labour_Chg as Labour Chg / Gram,p.Wastage as Wastage %, p.remarks,ap.order_id,k.name karigar_name,k.code karigar_code,,m.name metal_name,c.name category_name,c.code category_code,sc.name sub_category_name,sc.code sub_category_code,ca.name carat_name,a.name article_name,pt.name manufacturing_type_name,p.image image1,p.product_code,w.from_weight,w.to_weight,ca.name as Gold_Carat,"" as Gold_colour,c.name as Product_Type,sc.name as Category,"" as Quantity,"" as Gross_Wt,"" as Less_Wt,"" as Pure_Wt,"" as Price,"" as Rate,"" as Extra_Charge,"" as Net_Price,"" as AVG_WT,"" as Minimun_Wt,"" as Maximum_Wt,"17 WORKING DAYS" as Shipping_Days,p.product_code as product_code,CONCAT_WS("/thumb/",p.product_code,p.image) as image,"" as Wastage,p.weight_band_id,p.actual_wt,DATE_FORMAT(ap.created_at,"%d-%m-%Y") as order_date');
    $this->db->from("Approve_products ap");
    $this->all_joins();
/*    if(!empty($order_id)){
    $this->db->join('orders o', 'o.id = ap.order_id');
      $this->db->join('orders o', 'o.id = ap.order_id');
      $this->db->where('o.id',$order_id);
    }*/
    if(!empty($vendor)){
      $this->db->join('orders o', 'o.id = ap.order_id');
      $this->db->where('o.corporate',$vendor);
    }
    if(!empty($search)){
        $this->all_like_queries($search);
    }
    if($limit == true){
      $this->db->limit($params['length'],$params['start']);
       $this->db->order_by('ap.order_id DESC');
    }
    $result = $this->db->get()->result_array();
 //echo $this->db->last_query(); die();
    return $result;
  }
  private function all_joins(){
    $this->db->join('product p', 'p.id = ap.product_id','left');
    $this->db->join('karigar_master k', 'k.id = p.karigar_id', 'left');
    $this->db->join('category_master c', 'c.id = p.category_id', 'left');
    $this->db->join('sub_category_master sc', 'sc.id = p.sub_category_id', 'left');
    $this->db->join('manufacturing_type_master pt', 'pt.id = p.manufacturing_type_id', 'left');
    $this->db->join('buying_complexity_master bc', 'bc.id = p.buying_complexity', 'left');
    $this->db->join('pcs_master pc', 'pc.id = p.pcs_id', 'left');
    $this->db->join('article_master a', 'a.id = p.article_id', 'left');
    $this->db->join('carat_master ca', 'ca.id = p.carat_id', 'left');
    $this->db->join('metal_master m', 'm.id = p.metal_id', 'left');
    $this->db->join('weights w', 'w.id = p.weight_band_id', 'left');
  }
  private function all_like_queries($search){
    $this->db->where("(p.product_code LIKE '%$search%' OR  k.name LIKE '%$search%' OR  k.code LIKE '%$search%' OR  c.name LIKE '%$search%' OR  c.code LIKE '%$search%' OR  p.size LIKE '%$search%' OR  pt.name LIKE '%$search%' OR  a.name LIKE '%$search%'OR  ca.name LIKE '%$search%' OR  m.name LIKE '%$search%'OR  w.from_weight LIKE '%$search%' OR  w.to_weight LIKE '%$search%' OR p.actual_wt LIKE '%$search%')");

  }
  public function get_orders($filter_status='',$status='',$params='',$search='',$limit='',$vendor=''){
    //$this->db->select('o.*,group_concat(ap.product_id) no_of_products,c.name as c_name');
    $this->db->select('o.*,(select count(id) from Approve_products where order_id=o.id) no_of_products ,c.name as c_name');
    $this->db->from('orders o');
    $this->db->join('Approve_products ap','ap.order_id = o.id');
    $this->db->join('corporate c','c.id = o.corporate');
    $this->db->join('product p', 'p.id = ap.product_id','left');

    if(!empty($search)){
        $this->db->where("(o.id LIKE '%$search%' OR p.product_code LIKE '%$search%')");

        /**/
    }

    if(!empty($vendor))
      $this->db->where('c.id',$vendor);

    $this->db->order_by('o.id DESC');
    if($limit == true)
      $this->db->limit($params['length'],$params['start']);
      $this->db->group_by('o.id');
    $result = $this->db->get()->result_array();
    // echo $this->db->last_query();
    // print_r($result); die();
    return $result;
  }
  public function find($order_id){
    $this->db->where('id',$order_id);
    $result = $this->db->get('orders')->row_array();
    return $result['corporate'];
  }
}