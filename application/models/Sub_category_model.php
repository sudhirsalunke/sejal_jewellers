<?php

class Sub_category_model extends CI_Model {

  function __construct() {
      $this->table_name = "sub_category_master";
      parent::__construct();

  }

  public function validatepostdata(){
  	$this->form_validation->set_message('check_sub_category_code', 'Code is already exists');
    $this->form_validation->set_message('check_sub_category_name', 'Name is already exists');
  	$this->form_validation->set_rules($this->config->item('Sub_category', 'admin_validationrules'));
     if ($this->form_validation->run() == FALSE) {       	
          return FALSE;
      } else {
          return TRUE;
      }
  }
  public function store(){
  	$postdata = $this->input->post('Sub_category');
  	$encrption = $this->data_encryption->get_encrypted_id($this->table_name);
  	$insert_array = array(
  		'name' => $postdata['name'],
      'code' => $postdata['code'],
  		'category_id' => $postdata['category_id'],
  		'encrypted_id' => $encrption,
  		'updated_at' => date('Y-m-d H:i:s'),
  		'created_at' => date('Y-m-d H:i:s'),
      'pair_pcs'=>$postdata['pair_pcs'],
  		);
  	if($this->db->insert($this->table_name,$insert_array)){
  		return get_successMsg();
  	}else{
  		return get_errorMsg();
  	}
  }
  public function get($filter_status='',$status='',$params='',$search='',$limit='',$category_id=''){
    $this->db->select('scm.*,cm.name as category_name');
    $this->db->from($this->table_name.' scm');
    $this->db->join('category_master cm','scm.category_id = cm.id');
    if(!empty($filter_status))
       $this->db->order_by($status[$filter_status['column']],$filter_status['dir']);
   /* if(!empty($search)){
        $this->db->like('scm.name',$search);
        $this->db->or_like('scm.code',$search);
        $this->db->or_like('cm.name',$search);
    }*/
     if(isset($params['columns']) && !empty($params['columns'])){
      $filter_input=$params['columns'];
      $table_col_name='sub_catagory';
      $this->get_filter_value($filter_input,$table_col_name);
    }
    if(!empty($category_id)){
      $this->db->where("scm.category_id ",$category_id);
    }
    
    if($limit == true ){
      $this->db->order_by('scm.id DESC');
      if(!empty($params)){
      $this->db->limit($params['length'],$params['start']);
      }
      $result = $this->db->get()->result_array();
    }else{
      $row_array = $this->db->get()->result_array();
      $result = count($row_array);
      //echo $this->db->last_query();print_r($result);exit;
    }
    //echo $this->db->last_query();print_r($result);exit;
    return $result;
  }
  private function get_filter_value($filter_input,$table_col_name){
    $column_name=array();  
    $filter_column_name=filter_column_name($table_col_name);
    $sql='';
    $i=0;
     
     //print_r($filter_input);die;
    foreach ($filter_input as $key => $search_value){
       $column_name=$filter_column_name;
       //print_r($search_value['search']['value']);
        if(!empty($search_value['search']['value'])){
          if($i != 0){
            $sql.=' AND  ';
          }
            $sql.=''.$column_name[$key].' like "%'.$search_value['search']['value'].'%" ';
            $i++;
        

         }   
    }

    if(!empty($sql)){  
      $this->db->where($sql);  
    } 
       
  }

  public function find($id){
  	$this->db->where('id',$id);
  	$result = $this->db->get($this->table_name)->row_array();
  	return $result;
  }

  public function get_primary_key($encrypted_key){
  	$this->db->where('encrypted_id',$encrypted_key);
  	$result = $this->db->get($this->table_name)->row_array();
  	return $result['id'];
  }
  public function update(){
  	$postdata = $this->input->post('Sub_category');
  	$id = $this->get_primary_key($postdata['encrypted_id']);
   // print_r($id);die;
  	$update_array = array(
  		'name' => $postdata['name'],
  		'code' => $postdata['code'],
      'category_id' => $postdata['category_id'],
  		'updated_at' => date('Y-m-d H:i:s'),
      'pair_pcs'=>$postdata['pair_pcs'],
  		);
  	$this->db->where('id',$id);
  	if($this->db->update($this->table_name,$update_array)){
  		return get_successMsg();
  	}else{
  		return get_errorMsg();
  	}
  }

public function update_sub_cat_images(){
    $postdata = $this->input->post('Sub_category');
    $id = $this->get_primary_key($postdata['encrypted_id']);
    $sub_cat_images = $this->input->post('sub_cat_img');
    $this->db->where("sub_cat_id",$id);
    $result = $this->db->delete("sub_category_images");   //code to delete previous images     
    foreach ($sub_cat_images as $key => $value) {
        $img_array = array(
            "sub_cat_id"=>$id,
            "sub_cat_image"=>$value
          );
       //$img_status = $this->check_cat_id_with_same_image($id,$value); 

      $this->db->insert("sub_category_images",$img_array);  //code to update new images
          
    }
    return get_successMsg();    
  }
  
  public function delete($id){
  	$this->db->where('id',$id);
  	if($this->db->delete($this->table_name)){
  		return get_successMsg();
  	}else{
  		return get_errorMsg();
  	}
  }
  public function validate_minimum_stock(){
    $data['status'] = 'success';
    $data['data'] = '';
    if(empty($_POST['category_id'])){
      $data['status'] = 'failure';
      $data['error'] = 'Category should not be empty';
    }elseif(!empty($_POST)){
      foreach ($_POST as $key => $value) {
        if(empty($value)){
          $data['status'] = 'failure';
          $data['error'][$key] = 'This field is required';
        }elseif(is_numeric($value) == false){
          $data['status'] = 'failure';
          $data['error'][$key] = 'This field should be numeric';
        }
      }
    }
    return $data;
  }
  public function save_minimum_stock(){
    $category_id = $_POST['category_id'];
    unset($_POST['category_id']);
    foreach ($_POST as $key => $value) {
      $insert_array[] = array(
          'weight_range_id'=>$key,
          'stock'=>$value,
          'sub_category_id'=>$category_id,
          'created_at'=>date('Y-m-d H:i:s'),
          'updated_at'=>$category_id,
        );
    }
    $this->db->where('sub_category_id',$category_id);
    $this->db->delete('minimum_stock');
    if($this->db->insert_batch('minimum_stock',$insert_array)){
      return get_successMsg();
    }else{
      return get_errorMsg();
    }
  }
  public function find_stock($id){
    $this->db->select('*');
    $this->db->from('minimum_stock');
    $this->db->where('sub_category_id',$id);
    return $this->db->get()->result_array();
  }
  
  public function find_stock_with_weight_band($id){
    $this->db->select('minimum_stock.*, weights.from_weight, weights.to_weight');
    $this->db->from('minimum_stock');
    $this->db->where('sub_category_id',$id);
    $this->db->join('weights','minimum_stock.weight_range_id=weights.id', 'left');
    return $this->db->get()->result_array();
  }

  //code to get subcategory images
  public function sub_category_images($id){
    $this->db->where('sub_cat_id',$id);
    return $this->db->get("sub_category_images")->result_array();
  }

  //code to get subcategory name
  public function sub_category_name_by_id($id)
  {
    $this->db->select('name');
    $this->db->where('id',$id);
    return $this->db->get($this->table_name)->row();
  }
  //code to get subcategory name
  public function fetch_images_from_dropbox($id)
  {
    
  }
}    