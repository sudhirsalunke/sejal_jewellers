<?php
class Parent_sub_category_model extends CI_Model {

  function __construct() {
      $this->table_name = "parent_sub_category";
      parent::__construct();
  }

  public function validatepostdata(){
   
  	$this->form_validation->set_rules($this->config->item('parent_sub_category', 'admin_validationrules'));
     if ($this->form_validation->run() == FALSE) {       	
          return FALSE;
      } else {
          return TRUE;
      }
  }
  public function store(){
    $insert_array=array();
  	$postdata = $this->input->post('product_category');
  	$insert_array = $postdata;
    $insert_array['encrypted_id']=$this->data_encryption->get_encrypted_id();
    $insert_array['updated_at']=date('Y-m-d H:i:s');
    $insert_array['created_at']=date('Y-m-d H:i:s');
  		
  	if($this->db->insert($this->table_name,$insert_array)){
  		return get_successMsg();
  	}else{
  		return get_errorMsg();
  	}
  }
  public function get($filter_status='',$status='',$params='',$search='',$limit=''){
    $this->db->select('pr_c.*,p.name as parent_category');
    $this->db->from($this->table_name.' pr_c');
    $this->db->join('parent_category p','pr_c.parent_category=p.id','left');
    // if(!empty($filter_status))
    //    $this->db->order_by($status[$filter_status['column']],$filter_status['dir']);
    if(!empty($search)){
        $this->db->like('pr_c.name',$search);
    }
  if($limit == true){
      if(!empty($params)){
      $this->db->limit($params['length'],$params['start']);
      }
      $result = $this->db->get()->result_array();
    }else{
      $row_array = $this->db->get()->result_array();
      $result = count($row_array);
      //echo $this->db->last_query();print_r($result);exit;
    }
   // echo $this->db->last_query(); print_r($result);exit;
    return $result;
  }

  public function find_by_encrypted_id($encrypted_id){
   $this->db->where('encrypted_id',$encrypted_id);
   $result = $this->db->get($this->table_name)->row_array();
   return $result;
  }
  public function find($id){
  	$this->db->where('id',$id);
  	$result = $this->db->get($this->table_name)->row_array();
  	return $result;
  }

  // public function get_primary_key($encrypted_key){
  // 	$this->db->where('encrypted_id',$encrypted_key);
  // 	$result = $this->db->get($this->table_name)->row_array();
  // 	return $result['id'];
  // }
  public function update(){
    $postdata =$this->input->post('product_category');
  	$update_array = array(
  		'name' => $postdata['name'],
      'parent_category' => $postdata['parent_category'],
      'touch' => $postdata['touch'],
  		'updated_at' => date('Y-m-d H:i:s')
  		);
  	$this->db->where('encrypted_id',$postdata['encrypted_id']);
  	if($this->db->update($this->table_name,$update_array)){
  		return get_successMsg();
  	}else{
  		return get_errorMsg();
  	}
  }
  public function delete($id){
  	$this->db->where('encrypted_id',$id);
  	if($this->db->delete($this->table_name)){
  		return get_successMsg();
  	}else{
  		return get_errorMsg();
  	}
  }

  public function check_pc_used($id){
      $this->db->where('',$id);
      $this->db->from('');
     return $this->db->count_all_results();
 }
}    