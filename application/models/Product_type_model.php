<?php

class Product_type_model extends CI_Model {

  function __construct() {
      $this->table_name = "product_type";
      parent::__construct();
  }

  public function validatepostdata(){
    $this->form_validation->set_message('check_product_type_exist', 'Product type already exists');
  	$this->form_validation->set_rules($this->config->item('Product_type', 'admin_validationrules'));
     if ($this->form_validation->run() == FALSE) {       	
          return FALSE;
      } else {
          return TRUE;
      }
  }
  public function store(){
  	$postdata = $this->input->post('Product_type');
  	$encrption = $this->data_encryption->get_encrypted_id($this->table_name);
  	$insert_array = array(
  		'name' => $postdata['name'],
  		'description' => $postdata['description'],
  		'encrypted_id' => $encrption,
  		'updated_at' => date('Y-m-d H:i:s'),
  		'created_at' => date('Y-m-d H:i:s')
  		);
  	if($this->db->insert($this->table_name,$insert_array)){
  		return get_successMsg();
  	}else{
  		return get_errorMsg();
  	}
  }
  public function get($filter_status='',$status='',$params='',$search='',$limit=''){
    $this->db->select('*');
    $this->db->from($this->table_name);
    if(!empty($filter_status))
       $this->db->order_by($status[$filter_status['column']],$filter_status['dir']);
    if(!empty($search)){
        $this->db->like('name',$search);
        $this->db->or_like('description',$search);
    }
    if($limit == true)
    	if(!empty($params)){
      $this->db->limit($params['length'],$params['start']);
      }
    $result = $this->db->get()->result_array();
    return $result;
  }

  public function find($id){
  	$this->db->where('id',$id);
  	$result = $this->db->get($this->table_name)->row_array();
  	return $result;
  }

  public function get_primary_key($encrypted_key){
  	$this->db->where('encrypted_id',$encrypted_key);
  	$result = $this->db->get($this->table_name)->row_array();
  	return $result['id'];
  }
  public function update(){
  	$postdata = $this->input->post('Product_type');
  	$id = $this->get_primary_key($postdata['encrypted_id']);
  	$update_array = array(
  		'name' => $postdata['name'],
  		'description' => $postdata['description'],
  		'updated_at' => date('Y-m-d H:i:s')
  		);
  	$this->db->where('id',$id);
  	if($this->db->update($this->table_name,$update_array)){
  		return get_successMsg();
  	}else{
  		return get_errorMsg();
  	}
  }
  public function delete($id){
  	$this->db->where('id',$id);
  	if($this->db->delete($this->table_name)){
  		return get_successMsg();
  	}else{
  		return get_errorMsg();
  	}
  }
}    