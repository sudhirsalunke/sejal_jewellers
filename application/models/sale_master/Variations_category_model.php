<?php
class Variations_category_model extends CI_Model {

  function __construct() {
      $this->table_name = "variation_category";
      parent::__construct();
  }

  public function validatepostdata(){
   
  	$this->form_validation->set_rules($this->config->item('Variations_category', 'admin_validationrules'));
     if ($this->form_validation->run() == FALSE) {       	
          return FALSE;
      } else {
          return TRUE;
      }
  }
  public function store(){
    $insert_array=array();
  	$postdata = $this->input->post('Variations_category');  
  	$insert_array = $postdata;
    $insert_array['encrypted_id']=$this->data_encryption->get_encrypted_id();
    $insert_array['updated_at']=date('Y-m-d H:i:s');
    $insert_array['created_at']=date('Y-m-d H:i:s');
  		
  	if($this->db->insert($this->table_name,$insert_array)){
  		return get_successMsg();
  	}else{
  		return get_errorMsg();
  	}
  }
  public function get($filter_status='',$status='',$params='',$search='',$limit=''){
    $this->db->select('cs.*');
    $this->db->from($this->table_name.' cs');
    if(isset($params['columns']) && !empty($params['columns'])){
      $filter_input=$params['columns'];
      $table_col_name="variation_category_table";
      $this->get_filter_value($filter_input,$table_col_name);
    }
   /* if(!empty($search)){
        $this->db->like('cs.name',$search);
    }*/
    if($limit == true)
    $this->db->limit($params['length'],$params['start']);
    $this->db->order_by('cs.id','DESC');
    $result = $this->db->get()->result_array();
   // echo $this->db->last_query(); print_r($result);exit;
    return $result;
  }
  private function get_filter_value($filter_input,$table_col_name){
    $column_name=array();  
    $filter_column_name=filter_column_name($table_col_name);
    $sql='';
    $i=0;
     
     //print_r($filter_input);die;
    foreach ($filter_input as $key => $search_value){
       $column_name=$filter_column_name;
       //print_r($search_value['search']['value']);
        if(!empty($search_value['search']['value'])){
          if($i != 0){
            $sql.=' AND  ';
          }
            $sql.=''.$column_name[$key].' like "%'.$search_value['search']['value'].'%" ';
            $i++;
        

         }   
    }

    if(!empty($sql)){  
      $this->db->where($sql);  
    } 
       
  }

  public function find_by_encrypted_id($encrypted_id){
   $this->db->where('encrypted_id',$encrypted_id);
   $result = $this->db->get($this->table_name)->row_array();
   return $result;
  }
  public function find($id){
  	$this->db->where('id',$id);
  	$result = $this->db->get($this->table_name)->row_array();
  	return $result;
  }

  // public function get_primary_key($encrypted_key){
  // 	$this->db->where('encrypted_id',$encrypted_key);
  // 	$result = $this->db->get($this->table_name)->row_array();
  // 	return $result['id'];
  // }
  public function update(){
    $postdata =$this->input->post('Variations_category');
    $update_array = $postdata;
  	// $update_array = array(
  	// 	'name' => $postdata['name'],
   //    'hc_id' => $postdata['parent_category'],
   //    'touch' => $postdata['touch'],
  	// 	'updated_at' => date('Y-m-d H:i:s')
  	// 	);
  	$this->db->where('encrypted_id',$_POST['encrypted_id']);
  	if($this->db->update($this->table_name,$update_array)){      
  		return get_successMsg(1);
  	}else{
  		return get_errorMsg();
  	}
  }
  public function delete($id){
  	$this->db->where('encrypted_id',$id);
  	if($this->db->delete($this->table_name)){
  		return get_successMsg();
  	}else{
  		return get_errorMsg();
  	}
  }

  public function check_cs_exits($id){
      $this->db->where('color_stone_id',$id);
      $this->db->from('sales_invoice');
     return $this->db->count_all_results();
 }
}    