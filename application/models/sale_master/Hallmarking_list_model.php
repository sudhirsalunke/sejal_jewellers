<?php
class Hallmarking_list_model extends CI_Model {

  function __construct() {
      $this->table_name = "hallmarking_list";
      parent::__construct();
  }

  public function validatepostdata(){
   
  	$this->form_validation->set_rules($this->config->item('hallmarking_list', 'admin_validationrules'));
     if ($this->form_validation->run() == FALSE) {       	
          return FALSE;
      } else {
          return TRUE;
      }
  }
 /* public function store(){
    $insert_array=array();
  	$postdata = $this->input->post('hallmarking_list');
  	$insert_array = $postdata;
    $insert_array['encrypted_id']=$this->data_encryption->get_encrypted_id();
    $insert_array['updated_at']=date('Y-m-d H:i:s');
    $insert_array['created_at']=date('Y-m-d H:i:s');
  		
  	if($this->db->insert($this->table_name,$insert_array)){
  		return get_successMsg();
  	}else{
  		return get_errorMsg();
  	}
  }*/
  public function get($filter_status='',$status='',$params='',$search='',$limit=''){
    $this->db->select('hl.*,hc.name as hc_name,hc.code as code');
    $this->db->from($this->table_name.' hl');
    $this->db->join('hallmarking_center hc','hl.hc_id=hc.id','left');
    $this->db->order_by('hl.id DESC');
  /*  if(!empty($search)){
        $this->db->like('hl.name',$search);
    }*/
    if(isset($params['columns']) && !empty($params['columns'])){
      $filter_input=$params['columns'];
      $table_col_name='hallmarking_list_table';
      $this->get_filter_value($filter_input,$table_col_name);
    }
    if($limit == true)
      $this->db->limit($params['length'],$params['start']);
    $result = $this->db->get()->result_array();
   // echo $this->db->last_query(); print_r($result);exit;
    return $result;
  }
  private function get_filter_value($filter_input,$table_col_name){
    $column_name=array();  
    $filter_column_name=filter_column_name($table_col_name);
    $sql='';
    $i=0;
     
     //print_r($filter_input);die;
    foreach ($filter_input as $key => $search_value){
       $column_name=$filter_column_name;
       //print_r($search_value['search']['value']);
        if(!empty($search_value['search']['value'])){
          if($i != 0){
            $sql.=' AND  ';
          }
            $sql.=''.$column_name[$key].' like "%'.$search_value['search']['value'].'%" ';
            $i++;
        

         }   
    }

    if(!empty($sql)){  
      $this->db->where($sql);  
    } 
       
  }

  public function export_hallmarking_list($filter_status='',$status='',$params='',$search='',$limit=''){
    $this->db->select('hc.name as HM Center,hc.code as HM Code, hl.c_name as Company Name, hl.owner as Owner, hl.ap_name as Authorized Person,  hl.mobile_no as Mobile No, hl.phone_no as Phone No, hl.address as Address');
    $this->db->from($this->table_name.' hl');
    $this->db->join('hallmarking_center hc','hl.hc_id=hc.id','left');
    $this->db->order_by('hl.id DESC');
    if(!empty($search)){
        $this->db->like('hl.name',$search);
    }
    if($limit == true)
      $this->db->limit($params['length'],$params['start']);
    $result = $this->db->get()->result_array();
   // echo $this->db->last_query(); print_r($result);exit;
    return $result;
  }
  public function store(){
    $postdata = $this->input->post('hallmarking_list');
    $insert_array=array();
    $hallmarking_center = array();
    $hallmarking_center=$this->hallmarking_center($postdata);    
    $insert_array = array(    
      'hc_id' => $hallmarking_center['hc_id'],
      'encrypted_id'=>$this->data_encryption->get_encrypted_id(),
      'c_name' => $postdata['c_name'],
      'owner' => $postdata['owner'],
      'ap_name' => $postdata['ap_name'],
      'address' => $postdata['address'],
      'city' => $postdata['city'],
      'state' => $postdata['state'],
      'pincode'=>$postdata['pincode'],
      'email' => $postdata['email'],
      'mobile_no' => $postdata['mobile_no'],
      'phone_no' => $postdata['phone_no'],
      'icom_no' => $postdata['icom_no'],
      'limit' => $postdata['limit'],      
      'created_at' => date('Y-m-d H:i:s')
    );
      
    if($this->db->insert($this->table_name,$insert_array)){
      return get_successMsg();
    }else{
      return get_errorMsg();
    }
  }

  public function update(){
    $postdata = $this->input->post('hallmarking_list');
    $update_array=array();
    $hallmarking_center = array();
    $hallmarking_center=$this->hallmarking_center($postdata);    
    $update_array = array(    
      'hc_id' => $hallmarking_center['hc_id'],      
      'c_name' => $postdata['c_name'],
      'owner' => $postdata['owner'],
      'ap_name' => $postdata['ap_name'],
      'address' => $postdata['address'],
      'city' => $postdata['city'],
      'state' => $postdata['state'],
      'pincode'=>$postdata['pincode'],
      'email' => $postdata['email'],
      'mobile_no' => $postdata['mobile_no'],
      'phone_no' => $postdata['phone_no'],
      'icom_no' => $postdata['icom_no'],
      'limit' => $postdata['limit'],      
      'updated_at' => date('Y-m-d H:i:s')
    );
    $this->db->where('encrypted_id',$postdata['encrypted_id']);
    if($this->db->update($this->table_name,$update_array)){
      return get_successMsg(1);
    }else{
      return get_errorMsg();
    }
  }

  private function hallmarking_center($postdata)
  {
     if(empty($postdata['id'])){
      $hallmarking_center['name'] = $postdata['hc_id'];
      $hallmarking_center['code'] = $postdata['code'];
      $hallmarking_center['encrypted_id']=$this->data_encryption->get_encrypted_id();      
      $hallmarking_center['created_at']=date('Y-m-d H:i:s');
      $this->db->insert('hallmarking_center',$hallmarking_center);
      $postdata['hc_id'] = $this->db->insert_id();
      }else{
      $hallmarking_center['name'] = $postdata['hc_id'];
      $hallmarking_center['code'] = $postdata['code'];
      $hallmarking_center['updated_at']=date('Y-m-d H:i:s');
      $this->db->where('id',$postdata['id']);
      $this->db->update('hallmarking_center',$hallmarking_center);
      $postdata['hc_id'] =$postdata['id'];
    }
      return $postdata;
  }
 
  public function delete($id){
    $this->db->select('hc_id');
    $this->db->where('encrypted_id',$id);
    $result = $this->db->get($this->table_name)->row_array();
    $this->db->where('encrypted_id',$id);
    if($this->db->delete($this->table_name)){
        $this->db->where('id',$result['hc_id']);
        $this->db->delete('hallmarking_center');
      return get_successMsg();
    }else{
      return get_errorMsg();
    }
  }
  public function getPrintData($hm_id){
    $this->db->select('hc.code as code,hc.name as hc_name,hl.address, c.name as city,s.name as state,hl.pincode,hl.phone_no,hl.mobile_no');
    $this->db->from($this->table_name.' hl');
    $this->db->join('hallmarking_center hc','hl.hc_id=hc.id','left');
    $this->db->join('state s','s.id=hl.state','left');
    $this->db->join('city c','c.id=hl.city','left');
    $this->db->where('hl.encrypted_id',$hm_id);
    $result = $this->db->get($this->table_name)->row_array();
    return $result;
  }
  public function find_by_encrypted_id($encrypted_id){
   $this->db->where('encrypted_id',$encrypted_id);
   $result = $this->db->get($this->table_name)->row_array();
   return $result;
  }
  public function find($id){
  	$this->db->where('id',$id);
  	$result = $this->db->get($this->table_name)->row_array();
  	return $result;
  }

  // public function get_primary_key($encrypted_key){
  // 	$this->db->where('encrypted_id',$encrypted_key);
  // 	$result = $this->db->get($this->table_name)->row_array();
  // 	return $result['id'];
  // }
  


  public function check_pc_used($id){
      $this->db->where('',$id);
      $this->db->from('');
     return $this->db->count_all_results();
 }
}    