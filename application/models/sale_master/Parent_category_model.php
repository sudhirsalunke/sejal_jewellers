<?php
class Parent_category_model extends CI_Model {

  function __construct() {
      $this->table_name = "parent_category";
      $this->table_codes = "parent_category_codes";
      parent::__construct();
  }

  public function validatepostdata(){
   $data = array();
   $old_post=$_POST;
   foreach ($_POST['parent_category']['code'] as $key => $value) {
     $_POST['product_code']=$value;
  	$this->form_validation->set_rules($this->config->item('parent_category', 'admin_validationrules'));
     if ($this->form_validation->run() == FALSE) {       	
          $data['name']=strip_tags(form_error('parent_category[name]'));
          $data['product_code_'.$key]=strip_tags(form_error('product_code'));
      } 
   }
      return $data;
  }
  public function store(){
    $insert_array=array();
  	$postdata = $this->input->post('parent_category');
    //print_r($postdata);exit;
  	$insert_array = $postdata;
    if(!empty($postdata['few_box'])){
      $few_box=$postdata['few_box'];
    }else{
      $few_box='0';
    }
    $insert_array['few_box']=$few_box;   
    $insert_array['code']=(!empty($postdata['code'])) ? json_encode($postdata['code']) : '';
    $insert_array['variant']=(!empty($postdata['variant'])) ? json_encode($postdata['variant']) : '';
    $insert_array['encrypted_id']=$this->data_encryption->get_encrypted_id();
    $insert_array['updated_at']=date('Y-m-d H:i:s');
    $insert_array['created_at']=date('Y-m-d H:i:s');
  	if($this->db->insert($this->table_name,$insert_array)){
      $pc_id = $this->db->insert_id();
      foreach ($postdata['code'] as $key => $value) {
        $code_data  = array('parent_category_id'=>$pc_id,
                            'code_name'=>$value,
                            'created_at'=>date('Y-m-d H:i:s'),
                            'updated_at' =>date('Y-m-d H:i:s'));
        $this->db->insert($this->table_codes,$code_data);
      }
      if(!empty($postdata['variant'])){
        foreach ($postdata['variant'] as $v_key => $v_value) {
          if(!empty($v_value)){
            $variant_data  = array('parent_category_id'=>$pc_id,
                              'name'=>$v_value,
                              'created_at'=>date('Y-m-d H:i:s'),
                              'updated_at' =>date('Y-m-d H:i:s'));
            $this->db->insert('product_variants',$variant_data);
          }
        }
      }

  		return get_successMsg();
  	}else{
  		return get_errorMsg();
  	}
  }
  public function get($filter_status='',$status='',$params='',$search='',$limit=''){
    $this->db->select('pc.*,pc.id,cm.name category_name');
    $this->db->from($this->table_name.' pc');
    $this->db->join('product_category cm','cm.id = pc.category_id','left');
    $this->db->join($this->table_codes.' pcode','pcode.id = pc.id','left');
    // if(!empty($filter_status))
    //    $this->db->order_by($status[$filter_status['column']],$filter_status['dir']);
      $this->db->order_by('pc.id DESC');
/*    if(!empty($search)){
        $this->db->like('name',$search);
    }*/
    if(isset($params['columns']) && !empty($params['columns'])){
      $filter_input=$params['columns'];
      $table_col_name='parent_category_table';
      $this->get_filter_value($filter_input,$table_col_name);
    }

    if($limit == true){
     if(!empty($params)){
      $this->db->limit($params['length'],$params['start']);
      }
      $result = $this->db->get()->result_array();
    }else{
      $row_array = $this->db->get()->result_array();
      $result = count($row_array);
      //echo $this->db->last_query();print_r($result);exit;
    }
    // echo $this->db->last_query();die;
    return $result;
  }
  private function get_filter_value($filter_input,$table_col_name){
    $column_name=array();  
    $filter_column_name=filter_column_name($table_col_name);
    $sql='';
    $i=0;
     
     //print_r($filter_input);die;
    foreach ($filter_input as $key => $search_value){
       $column_name=$filter_column_name;
       //print_r($search_value['search']['value']);
        if(!empty($search_value['search']['value'])){
          if($i != 0){
            $sql.=' AND  ';
          }
            $sql.=''.$column_name[$key].' like "%'.$search_value['search']['value'].'%" ';
            $i++;
        

         }   
    }

    if(!empty($sql)){  
      $this->db->where($sql);  
    } 
       
  }

  public function find_by_encrypted_id($encrypted_id){
   $this->db->where('encrypted_id',$encrypted_id);
   $result = $this->db->get($this->table_name)->row_array();
   return $result;
  }
  public function find($id){
  	$this->db->where('id',$id);
  	$result = $this->db->get($this->table_name)->row_array();
  	return $result;
  }

  // public function get_primary_key($encrypted_key){
  // 	$this->db->where('encrypted_id',$encrypted_key);
  // 	$result = $this->db->get($this->table_name)->row_array();
  // 	return $result['id'];
  // }
  public function update(){

    $postdata =$this->input->post('parent_category');
    //print_r($postdata);exit;
    $ids=$postdata['ids'];  
     
    foreach ($postdata['code'] as $key => $value) {
      if (!empty($postdata['ids'][$key])) {
        $this->db->where('id',$postdata['ids'][$key]);
        $this->db->update("parent_category_codes",array('code_name'=>$value,'updated_at'=>date('Y-m-d H:i:s')));
      }else{
        $insert_array=array(
                        'parent_category_id'=>$_POST['parent_category_id'],
                        'code_name'=>$value,
                        'created_at'=>date('Y-m-d H:i:s'),
                        'updated_at'=>date('Y-m-d H:i:s'),
                      );
        $this->db->insert('parent_category_codes',$insert_array);
        $ids[]=$this->db->insert_id();
      }
    }
    /*deleted removed one*/
    $this->db->where('parent_category_id',$_POST['parent_category_id']);
    $this->db->where_not_in('id',$ids);
    $this->db->delete('parent_category_codes');
    $this->update_variants($postdata);
   if(!empty($postdata['few_box'])){
      $few_box=$postdata['few_box'];
    }else{
      $few_box='0';
    }
    $update_array = array(
      'name' => $postdata['name'],
      'kundan_category' => $postdata['kundan_category'],
      'stone_category' => $postdata['stone_category'],
      'black_beads_category' => $postdata['black_beads_category'],
      'few_box' => $few_box,
      'updated_at' => date('Y-m-d H:i:s'),
      'category_id' => $postdata['category_id'],
      'app_order' => @$postdata['app_order'],
      'code' => (!empty($postdata['code'])) ? json_encode($postdata['code']) : ''
      );
    $this->db->where('encrypted_id',$postdata['encrypted_id']);
  	if($this->db->update($this->table_name,$update_array)){
  		return get_successMsg();
  	}else{
  		return get_errorMsg();
  	}
  }
  private function update_variants($postdata){
   if(!empty($postdata['variant'])){
      $variant_ids=@$postdata['variant_ids'];
      foreach ($postdata['variant'] as $key => $value) {
        if (!empty($postdata['variant_ids'][$key])) {
          if(!empty($value)){
            $this->db->where('id',$postdata['variant_ids'][$key]);
            $this->db->update("product_variants",array('name'=>$value,'updated_at'=>date('Y-m-d H:i:s')));
          }
        }else{
          if(!empty($value)){
            $insert_array=array(
                          'parent_category_id'=>$_POST['parent_category_id'],
                          'name'=>$value,
                          'created_at'=>date('Y-m-d H:i:s'),
                          'updated_at'=>date('Y-m-d H:i:s'),
                        );
          $this->db->insert('product_variants',$insert_array);
          $variant_ids[]=$this->db->insert_id();
          }
        }
      }
      /*deleted removed one*/
      if(!empty($variant_ids)){
        $this->db->where('parent_category_id',$_POST['parent_category_id']);
        $this->db->where_not_in('id',$variant_ids);
        $this->db->delete('product_variants');
      }else{
        $this->db->where('parent_category_id',$_POST['parent_category_id']);
        $this->db->delete('product_variants');
      }
    }
  }
  public function delete($id){
  	$this->db->where('encrypted_id',$id);
  	if($this->db->delete($this->table_name)){
  		return get_successMsg();
  	}else{
  		return get_errorMsg();
  	}
  }

  public function check_pc_used($id){
      $this->db->where('parent_category',$id);
      $this->db->from('product_category');
      $product_category =$this->db->count_all_results();

      $this->db->where('parent_category_id',$id);
      $this->db->from('manufacturing_order_mapping');
      $mom =$this->db->count_all_results();

      return  $product_category+$mom;

  }
  public function parent_category_name_by_id($id)
  {
    $this->db->select('name');
    $this->db->where('id',$id);
    return $this->db->get($this->table_name)->row();
  }

  public function find_name($name)
  {
    $this->db->select('pc.code_name as product');
    $this->db->from('parent_category p');
    $this->db->join('parent_category_codes pc','pc.parent_category_id =p.id');
    $this->db->where('p.name',$name);
    return $this->db->get()->result_array();
  }
  public function fetch_from_dropbox(){
    $this->db->where('type','2');
    $this->db->set('status','1');
    $this->db->update('fetch_from_dropbox');
  }
  public function update_images($path,$pc_id){
    $insert_array = array(
      'path'=>$path,
      'pc_id'=>$pc_id,
      'created_at'=>date('Y-m-d H:i:s'),
      'updated_at'=>date('Y-m-d H:i:s'),
      );
    $this->db->insert('parent_cat_images',$insert_array);
  }
  public function get_images($id){
    $this->db->select('pci.*,pc.name');
    $this->db->from('parent_cat_images pci');
    $this->db->join($this->table_name.' pc','pc.id = pci.pc_id');
    $this->db->where('pci.pc_id',$id);
    $result = $this->db->get()->result_array();
    return $result;
  }
  public function get_pc_codes($pc_id){
    $codes =  $this->db->get_where($this->table_codes,array('parent_category_id'=>$pc_id))->result_array();

     foreach ($codes as $key => $value) {
       $this->db->where('pc_code_id',$value['id']);
       $codes[$key]['stock'] = $this->db->from('minimum_stock')->get()->result_array();
      
    } 
   
    return $codes;

  }

  public function store_minimum_stock(){
    $post = $this->input->post('minimum_stock');
    foreach ($post as $key => $value) {
        $this->db->where('pc_code_id',$value['pc_code_id']);
        $this->db->delete('minimum_stock');
        $value['weight_range_id'] = array_filter($value['weight_range_id'], function($value) { return $value !== ''; });

        foreach ($value['weight_range_id'] as $wr_key => $wr_value) {
          $insert_array=array('weight_range_id'=>$wr_key,
                              'pc_code_id'=>$value['pc_code_id'],
                              'stock'=>$wr_value,
                              'created_at'=>date('Y-m-d H:i:s'),
                              'updated_at'=>date('Y-m-d H:i:s'),
                              );
          $this->db->insert('minimum_stock',$insert_array);
        }
    }
    return get_successMsg();
  }

/*  public function get_category_codes($pc_id){
    return $this->db->get_where('parent_category_codes',array('parent_category_id'=>$pc_id))->result_array();
  }*/
    public function get_category_codes($pc_id){
     $this->db->select('*');
    $this->db->from('parent_category_codes');
       if(!empty($pc_id)){
      $this->db->where('parent_category_id',$pc_id);
    }

     return $this->db->get()->result_array();
  }
  public function get_variants($parent_category_id=''){
    $this->db->select('*');
    $this->db->from('product_variants');
    if(!empty($parent_category_id)){
      $this->db->where('parent_category_id',$parent_category_id);
    }
    return $this->db->get()->result_array();
  }
}    