<?php
class Category_model extends CI_Model {

  function __construct() {
      $this->table_name = "category_master";
      parent::__construct();
  }

  public function validatepostdata(){
  	$this->form_validation->set_message('check_category_code', 'Code is already exists');
    $this->form_validation->set_message('check_category_name', 'Name is already exists');
  	$this->form_validation->set_rules($this->config->item('Category', 'admin_validationrules'));
     if ($this->form_validation->run() == FALSE) {       	
          return FALSE;
      } else {
          return TRUE;
      }
  }
  public function store(){
  	$postdata = $this->input->post('Category');
  	$encrption = $this->data_encryption->get_encrypted_id($this->table_name);
  	$insert_array = array(
  		'name' => $postdata['name'],
      'code' => $postdata['code'],
      'department_id' =>$postdata['department_id'],
      'purity' => $postdata['purity'],
      'wastage' => $postdata['wastage'],
  		'labore' => $postdata['labore'],
      /*'pair_pcs'=>$postdata['pair_pcs'],*/
  		'encrypted_id' => $encrption,
  		'updated_at' => date('Y-m-d H:i:s'),
  		'created_at' => date('Y-m-d H:i:s')
  		);
  	if($this->db->insert($this->table_name,$insert_array)){
  		return get_successMsg();
  	}else{
  		return get_errorMsg();
  	}
  }
  public function get($filter_status='',$status='',$params='',$search='',$limit='',$department_id='',$code_name=''){
    $this->db->select("cat.*,dep.name as department");
    $this->db->from($this->table_name.' cat');
    $this->db->join('departments dep','dep.id=cat.department_id','left');
    /*if(!empty($filter_status))
       $this->db->order_by($status[$filter_status['column']],$filter_status['dir']);
     */
    if(isset($params['columns']) && !empty($params['columns'])){
      $filter_input=$params['columns'];
      $table_col_name="Catagory";
      $this->get_filter_value($filter_input,$table_col_name);
    }
   /* if(!empty($search)){
        $this->db->like('name',$search);
        $this->db->or_like('code',$search);
    }*/
    if(!empty($department_id)){
      $this->db->where('cat.department_id',$department_id);
    }
   if($limit == true){
      $this->db->order_by('id','desc');
       if(!empty($params)){
        $this->db->limit($params['length'],$params['start']);
        }
      $result = $this->db->get()->result_array();
    //echo $this->db->last_query();print_r($result);exit;
    }else{
      $row_array = $this->db->get()->result_array();
      $result = count($row_array);
      //echo $this->db->last_query();print_r($result);exit;
    }
    return $result;
  }
  private function get_filter_value($filter_input,$table_col_name){
    $column_name=array();  
    $filter_column_name=filter_column_name($table_col_name);
    $sql='';
    $i=0;
     
     //print_r($filter_input);die;
    foreach ($filter_input as $key => $search_value){
       $column_name=$filter_column_name;
       //print_r($column_name);exit;
        if(!empty($search_value['search']['value'])){
          if($i != 0){
            $sql.=' AND  ';
          }
            $sql.=''.$column_name[$key].' like "%'.$search_value['search']['value'].'%" ';
            $i++;
        

         }   
    }
   // print_r($sql);exit;

    if(!empty($sql)){  
      $this->db->where($sql);  
    } 
   
       
  }

  public function find($id){
  	$this->db->where('id',$id);
  	$result = $this->db->get($this->table_name)->row_array();
    return $result;
  }

  public function get_primary_key($encrypted_key){
  	$this->db->where('encrypted_id',$encrypted_key);
  	$result = $this->db->get($this->table_name)->row_array();
  	return $result['id'];
  }
  public function update(){
  	$postdata = $this->input->post('Category');
  	$id = $this->get_primary_key($postdata['encrypted_id']);
  	$update_array = array(
  		'name' => $postdata['name'],
  		'code' => $postdata['code'],
      'department_id' =>$postdata['department_id'],
      'purity' => @$postdata['purity'],
      'wastage' => @$postdata['wastage'],
      'labore' => @$postdata['labore'],
      /*'pair_pcs'=>$postdata['pair_pcs'],*/
  		'updated_at' => date('Y-m-d H:i:s')
  		);
  	$this->db->where('id',$id);
  	if($this->db->update($this->table_name,$update_array)){
  		return get_successMsg();
  	}else{
  		return get_errorMsg();
  	}
  }
  public function delete($id){
  	$this->db->where('id',$id);
  	if($this->db->delete($this->table_name)){
  		return get_successMsg();
  	}else{
  		return get_errorMsg();
  	}
  }

  public function get_category_name(){
    $this->db->select('name');
    $result = $this->db->get($this->table_name)->result_array();
   return array_column($result, 'name');
  }
  public function get_category_name_dep($department_id=''){
    $this->db->select('id,name,code');
    $this->db->where('department_id',$department_id);
    if(!empty($department_id)){
      $result = $this->db->get($this->table_name)->result_array();
      return $result;
    }
  }

  public function find_name($name,$department_id)
  {
    $this->db->select('id,code as item_category_code');
    $this->db->from($this->table_name);
    $this->db->where('name',$name);
    $this->db->where('department_id',$department_id);
    $result=$this->db->get()->result_array();
   /* echo $this->db->last_query();
    print_r($result);*/
    return $result; 
  }

}    