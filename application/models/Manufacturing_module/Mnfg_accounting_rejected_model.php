<?php

class Mnfg_accounting_rejected_model extends CI_Model {

  function __construct() {
    $this->table_name = "manufacturing_accounting";
       parent::__construct();
  }

  public function validate_tag_products(){
    $data['status'] = 'success';
    if(!empty($_POST['kr'])){
      foreach ($_POST['kr'] as $key => $value) {
        //print_r($_POST['kr']);
          if($value['net_wt']<'0.1'){
          $data['status'] = 'failure';
          $data['error']['net_wt_'.$key] = 'Net Weight should be greater than 0';
        }
        if(!is_numeric($value['net_wt'])){
          $data['status'] = 'failure';
          $data['error']['net_wt_'.$key] = 'Net Weight should be number';
        }
        if(empty($value['net_wt'])){
          $data['status'] = 'failure';
          $data['error']['net_wt_'.$key] = 'Net Weight is required';
        }
       
      }
    }
    return $data;
  }
 
  public function get($filter_status='',$params='',$search='',$limit='',$order_id=''){
    $this->db->select('ma.manufacturing_order_id  order_id,mo.order_name name,mo.order_date,ma.updated_at as rejected_date,w.from_weight,w.to_weight,km.name karigar_name,pm.name sub_cat_name,ma.quantity as rejected_qty');    
    $this->join_query();
  
    if(!empty($order_id)){
      $this->db->where('ma.manufacturing_order_id',$order_id);
    }
    if(!empty($search)){
       $this->db->where("(mo.id LIKE '%$search%' OR mo.order_name LIKE '%$search%')");
      }
  $this->db->order_by('ma.id','AESC');
    if(!empty($params) && $limit==true){
      $this->db->limit($params['length'],$params['start']);
    }
    $result = $this->db->get()->result_array();
    //print_r($this->db->last_query()); print_r($result); die;
    return $result;

  }
  public function get_order_wise_list($filter_status='',$status='',$params='',$search='',$limit='',$status=''){
    $this->db->select('ma.manufacturing_order_id  order_id,mo.order_name name,mo.order_date,ma.updated_at as rejected_date');
    $this->join_query();
    $this->db->group_by('ma.manufacturing_order_id');  
     if(!empty($search)){
       $this->db->where("(ma.manufacturing_order_id LIKE '%$search%' OR mo.order_name LIKE '%$search%')");
      }

    if(!empty($params) && $limit==true){
      $this->db->limit($params['length'],$params['start']);
    }
    $result = $this->db->get()->result_array();
    //print_r($this->db->last_query());die;
    return $result;
  }
  private function join_query(){
    $this->db->from('manufacturing_accounting ma');
    $this->db->join('manufacturing_order mo','mo.id=ma.manufacturing_order_id');
    $this->db->join('weights w','w.id = ma.weight_range_id');
    $this->db->join('karigar_master km','km.id = ma.karigar_id');
    $this->db->join('parent_category pm','pm.id = ma.parent_category_id');
    $this->db->where('ma.status','1');
    $this->db->where('ma.is_status_back','1');
  }

  public function update(){
  $i = 0;
  //print_r($_POST['kr']);
  foreach ($_POST['kr'] as $key => $value) {
    $pdf_data[$i] = $insert;
    $kmm_data = $this->receive_product_details_by_id($key);
    $pdf_data[$i]['kmm_id'] = $key;
    $pdf_data[$i]['name'] = $kmm_data['name'];
    $pdf_data[$i]['karigar_name'] = $kmm_data['karigar_name'];
    $pdf_data[$i]['weights'] = $kmm_data['from_weight'] .'-'.$kmm_data['to_weight'];
    $pdf_data[$i]['from_weight'] = $kmm_data['from_weight'];
    $pdf_data[$i]['to_weight'] = $kmm_data['to_weight'];
    $pdf_data[$i]['quantity'] = $value['quantity'];
    $pdf_data[$i]['net_wt'] = $value['net_wt'];
    $i++;
  }
  return $pdf_data;
  }
  public function check_net_wt($id,$net_wt){
   
    $this->db->where('id',$id);
    $this->db->set('status','0');
    $this->db->set('is_status_back','0');
    $this->db->set('updated_at',date('Y-m-d'));
    $this->db->set('net_wt',$net_wt);
    $this->db->update('manufacturing_accounting');
  }

  public function get_engage_karigars()
  {
    $this->db->select('distinct (karigar_id)');
    $this->db->from("manufacturing_accounting");
    $this->db->where("status",'1');
    $this->db->where("is_status_back",'1');
    $this->db->group_by("karigar_id");
    //echo $this->db->last_query();die;
    $result = $this->db->get()->result_array();
    return $result;
  }

  public function rejected_products($id){
    $this->db->select('ma.id ma_id,ma.quantity,ma.net_wt,mop.product_code as name,w.from_weight,w.to_weight');
    $this->db->from('manufacturing_accounting ma');
    $this->db->join('manufacturing_order_mapping mop','mop.id=ma.parent_category_id','left');
    $this->db->join('weights w','w.id = ma.weight_range_id');
    $this->db->where('ma.status','1');
    $this->db->where('ma.is_status_back','1');

    $this->db->where('ma.karigar_id',$id);
    $this->db->group_by('ma.id');
    $result = $this->db->get()->result_array();
    return $result;
  } 
  private function receive_product_details_by_id($id){
    $this->db->select('kmm.karigar_id as karigar_id,km.name as karigar_name,pm.name,mop.parent_category_id,mop.id as mop_id,mop.manufacturing_order_id as order_id,mop.product_code as product_code,(kmm.quantity - kmm.receive_quantity) quantity,kmm.id as kmm_id,w.from_weight,w.to_weight');
    $this->db->from("Karigar_manufacturing_mapping kmm");
    $this->db->join('manufacturing_order_mapping mop','mop.id=kmm.mop_id','left');
    $this->db->join('parent_category pm','pm.id = mop.parent_category_id');;
    $this->db->join('karigar_master km','km.id=kmm.karigar_id','left');
    $this->db->join('weights w','w.id = mop.weight_range_id');
    $this->db->join('manufacturing_accounting ma','ma.kmm_id = kmm.id');
    $this->db->where("ma.id",$id);
    $result = $this->db->get()->row_array();
    return $result;
  }
}
