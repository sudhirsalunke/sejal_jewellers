<?php

class Ready_product_model extends CI_Model {

  function __construct() {
  	parent::__construct();
    $this->table_name = "department_ready_product";
  }
  
  public function store($data){
   //print_r($_POST);die;
    if (!empty($data['tag_product'])) {
      $this->session->set_userdata('print_tag_product_session',$data['tag_product']);
      $this->db->insert('make_set',array('created_at' => date('Y-m-d H:i:s'),'updated_at' => date('Y-m-d H:i:s') ));
        $make_set_id =$this->db->insert_id();
        $quantity = $data['quantity'];
        $product_code= $data['product_code'];
        $department_id= $data['department_id'];
        $product= $data['product'];
        $karigar_id= $data['karigar_id'];
        $pcs = $_POST['pcs'];
        $net_wt = $_POST['net_wt'];
        $gr_wt = $_POST['gr_wt'];
        $category_id = $_POST['category_id'];
        $item_category_code = $_POST['item_category_code'];
        $insert_array = array();
        $i=0;
      foreach ($data['tag_product'] as $key => $value){
        $product_id=get_product_id($product[$value]);
         //print_r($product_id['id']);die;
        $msqr_array = array('make_set_id' => $make_set_id,
                            'quantity' =>$quantity[$value],
                            'department_id'=>$department_id[$value],
                            'tag_id' => $value,
                            'qty_remaining'=>0,
                            'status'=>'1',
                            'pcs'=>@$pcs[$value],
                            );
        $this->db->insert('make_set_quantity_relation',$msqr_array);
          $msqr_id =$this->db->insert_id();
          $insert_array[$i]['msqr_id']=$msqr_id;
          $insert_array[$i]['ms_id']=$make_set_id;
          $insert_array[$i]['chitti_no'] = $make_set_id;
          $insert_array[$i]['department_id'] = $department_id[$value];
          $insert_array[$i]['product_code']=$product_code[$value];
          $insert_array[$i]['net_wt']=$net_wt[$value];
          $insert_array[$i]['gr_wt']=$gr_wt[$value];
          $insert_array[$i]['product']=$product[$value];
          $insert_array[$i]['karigar_id']=$karigar_id[$value];
          $insert_array[$i]['quantity']=$quantity[$value];
          $insert_array[$i]['item_category_code']=$item_category_code[$value];
          $insert_array[$i]['category_id']=$category_id[$value];
          $insert_array[$i]['product_id']=$product_id['id'];
         // $insert_array[$i]['barcode_path'] = $this->create_barcode($product_code[$value]);
          // $insert_array[$i]['custom_net_wt']=$net_wt[$value] + (10 / 100) * $net_wt[$value];
          // $insert_array[$i]['custom_gr_wt']=$gr_wt[$value] + (10 / 100) * $net_wt[$value];
          $insert_array[$i]['created_at'] = date('Y-m-d H:i:s');
          $insert_array[$i]['updated_at'] = date('Y-m-d H:i:s');
          $i++;
      }
        if($this->db->insert_batch($this->table_name,$insert_array)){
           $this->db->set('status','2');
           $this->db->where_in('id',$data['tag_product']);
           $this->db->update('tag_products');
          return get_successMsg();
        }else{
          return get_errorMsg();
        }
      
    }else{
        $data['created_at'] = date('Y-m-d H:i:s');
        $data['updated_at'] = date('Y-m-d H:i:s');
        if($this->db->insert($this->table_name,$data)){
          return get_successMsg();
        }
    }
   
  }

  public function get($filter_status='',$status='',$params='',$search='',$limit='',$department_id=''){
    
/*    $this->db->select('drp.id,msqr.make_set_id as chitti_no,drp.quantity,pm.name as sub_cat_name,tp.net_wt,drp.gr_wt,drp.net_wt as drp_net_wt,km.name as karigar_name,drp.product_code as drp_product_code,tp.department_id,tp.image,tp.product_code as tp_product_code ,drp.id as drp_id,rp.Product_name,drp.weight,drp.product');*/
  $this->db->select('drp.id,drp.chitti_no,drp.quantity,drp.net_wt,drp.gr_wt,km.name as karigar_name,drp.product_code ,drp.department_id,drp.id as drp_id,drp.weight,drp.product,DATE_FORMAT(drp.created_at,"%d-%m-%Y")  created_at,DATE_FORMAT(drp.sales_date,"%d-%m-%Y")  sales_date,d.name as department_name,drp.custom_stone_wt,drp.custom_checker_wt,drp.custom_kundan_pure,drp.custom_gr_wt,drp.  custom_net_wt');
  $this->db->from($this->table_name.' drp');
/*  $this->db->join('make_set_quantity_relation msqr','msqr.id = drp.msqr_id','left');
  $this->db->join('tag_products tp','msqr.tag_id = tp.id','left');*/
/*  $this->db->join('parent_category pm','pm.id = tp.sub_category_id','left');
  $this->db->join('quality_control qc','drp.qc_id =qc.id AND drp.qc_id IS NOT NULL','left');
  $this->db->join('Receive_products rp','qc.receive_product_id =rp.id ','left');
  $this->db->join('prepare_kundan_karigar_order po','po.id =rp.kmm_id ','left');*/
 /* $this->db->join('karigar_master km','(po.karigar_id =km.id AND drp.qc_id IS NOT NULL)OR(tp.karigar_id =km.id AND drp.qc_id IS NULL)OR(drp.karigar_id =km.id AND drp.qc_id IS NULL)OR(drp.kundan_id =km.id AND drp.qc_id IS NULL)OR(qc.karigar_id =km.id)','left');*/
  $this->db->join('karigar_master km','drp.karigar_id =km.id','left');
  $this->db->join('departments d','drp.department_id =d.id','left');
  //$this->all_like_queries($search);
      /*if((!empty($department_id) && $department_id != '0')  || $department_id == '1' ||  $department_id == '5' || $department_id == '7'  ||  $department_id == '8'){
        $this->db->where('drp.department_id',$department_id);
      }*//*else{ // comment this code for admin
        $this->db->where('drp.department_id',$department_id);
      }*/
      if((!empty($department_id))){
        $this->db->where('drp.department_id',$department_id);
      }

    if (@$_GET['status']=="pending") {
      $this->db->where('drp.status',1);
       if($department_id == '2'){
             $table_col_name="mfg_ready_pro_aprv_by_sales_bom";
          }else{
              $table_col_name="manufacturing_ready_product_approved_by_sales";
          }
    }else{
      $this->db->where('drp.status',0);
      if($department_id == '0'  || $department_id == '1' ||  $department_id == '5' || 
              $department_id == '7'  ||  $department_id == '8'  || $department_id == '11' ){                        
                  $table_col_name="manufacturing_ready_product";
              }else{
                     $table_col_name="mfg_ready_product_dept_wise";
              }
    }
    if(isset($params['columns']) && !empty($params['columns'])){
      $filter_input=$params['columns'];
      $this->get_filter_value($filter_input,$table_col_name);
    }
    if($limit == true){
      $this->db->order_by('drp.id','DESC');
     // $this->db->order_by('qc.id','DESC');
      $this->db->limit($params['length'],$params['start']);
      $result = $this->db->get()->result_array();
    }else{
      $row_array = $this->db->get()->result_array();
      $result = count($row_array);
      //echo $this->db->last_query();print_r($result);exit;
    }
    // echo $table_col_name;
// echo $this->db->last_query();print_r($result);exit;
    return $result;
  }
  private function get_filter_value($filter_input,$table_col_name){
    $column_name=array();  
    $filter_column_name=filter_column_name($table_col_name);
    $sql='';
    $i=0;
     
     //print_r($filter_input);die;
    foreach ($filter_input as $key => $search_value){
       $column_name=$filter_column_name;
       //print_r($column_name);
       //print_r($search_value['search']['value']);
        if(!empty($search_value['search']['value'])){
          if($i != 0){
            $sql.=' AND  ';
          }
            $sql.=''.$column_name[$key].' like "%'.$search_value['search']['value'].'%" ';
            $i++;
        

         }   
    }

    if(!empty($sql)){  
      $this->db->where($sql);  
    } 
       
  }
  

  private function all_like_queries($search){
    $this->db->where("(pm.name LIKE '%$search%' OR tp.product_code LIKE '%$search%' OR msqr.make_set_id LIKE '%$search%')");
  }

  // public function sales_to_manufacture(){ /*moved to sales_stock*/
  //  // $postdata =$this->input->post('product_rec');
  //   $this->db->set('status',3);
  //   $this->db->where('id',$_POST['id']);
  //  // $this->db->where_in('id',$postdata);
  //   if($this->db->update($this->table_name)){
  //     //echo $this->db->last_query();exit;
  //     return get_successMsg();
  //   }else{
  //     return get_errorMsg();
  //   }
  // }
  public function find($rp_id){
    $this->db->select('drp.id,msqr.make_set_id,drp.quantity,tp.image,tp.product_code,pm.name as sub_cat_name,tp.net_wt,drp.net_wt as drp_net_wt,drp.gr_wt as drp_gr_wt,drp.department_id,drp.product_code,drp.net_wt as net_wt ,msqr.pcs as pcs,d.name as department_name,pm.name as category_name,drp.voucher_no,DATE_FORMAT(drp.sales_date,"%d-%m-%Y")sales_date,DATE_FORMAT(drp.repair_date,"%d-%m-%Y") as  repair_date,drp.chitti_no,drp.product as type,drp.barcode_path,drp.custom_net_wt,drp.custom_gr_wt,drp.custom_stone_wt,drp.custom_checker_wt,drp.custom_kundan_pure,drp.custom_kun_wt,drp.custom_black_beads_wt,drp.product as sub_category,drp.kundan_pc,lm.name as location_name,drp.location_id');
    $this->db->from('department_ready_product drp');
    $this->db->join('make_set_quantity_relation msqr','msqr.id=drp.msqr_id','left');
    $this->db->join('location_master lm','lm.id=drp.location_id','left');
    $this->db->join('tag_products tp','msqr.tag_id = tp.id','left');
    $this->db->join('parent_category_codes pcc', 'pcc.code_name = tp.sub_code','left');
    $this->db->join('parent_category pm', 'pm.id = pcc.parent_category_id','left');
    //$this->db->join('parent_category pm','pm.id = tp.sub_category_id','left');
    $this->db->join('departments d','d.id = drp.department_id','left');
    $this->db->where('drp.id',$rp_id);
    $result = $this->db->get()->row_array();
    // echo $this->db->last_query();
    // print_r($result);
    // die;
    return $result;
  }

  public function get_ready_product_details($filter_status='',$status='',$params='',$search='',$limit='',$department_id, $product_code=''){
    $this->db->select('drp.*,km.name as karigar_name');
    $this->db->from('department_ready_product drp');
    $this->db->join('karigar_master km','drp.karigar_id =km.id','left');
    $this->db->where('drp.product_code',$product_code);   
    if((!empty($department_id))){
        $this->db->where('drp.department_id',$department_id);
     }
  
    if (@$_GET['status']=="rejected_chitti") {
          $this->db->where('drp.status','6');
    }else{
      $this->db->where('drp.status !=6');
    }
    if($limit == true){
      $this->db->order_by('drp.id','DESC');
      if(!empty($params)){
        $this->db->limit($params['length'],$params['start']);
       }
      $result = $this->db->get()->result_array();
      //echo $this->db->last_query(); print_r($result); die;
    }else{
      $row_array = $this->db->get()->result_array();
      $result = count($row_array);    
       //echo $this->db->last_query(); print_r($result); die;
    }

    return $result;
  }


 

  public function transfer_to_sales(){
    if($_SESSION['location_id']=='2'){
      $location_id=2;
    }else{
      $location_id=1;
    }
    if(!empty($_POST['rp'])){
       $date=date('Y-m-d');
       $voucher_no = "MNFTSAL".mt_rand();
      foreach ($_POST['rp'] as $key => $value) {
        $this->db->set('status',2);
        $this->db->where('id',$value);
        $this->db->set('sales_date',$date);
        $this->db->set('voucher_no',$voucher_no);
        $this->db->set('location_id',$location_id);
        if($this->db->update('department_ready_product')){
         $ready_product_id=$value;
         $this->db->where('id',$ready_product_id);
         $drp_data = $this->db->get('department_ready_product')->row_array();
          $sales_stock=array(
                              'drp_id'=>$value,
                              'product_code'=>$drp_data['product_code'],
                              'status'=>0,
                              'approve_date' =>$date,
                              'created_at' =>date('Y-m-d H:i:s'),
                              );
          $this->db->insert('sale_stock',$sales_stock);
        }
      }
      $_SESSION['rp_id']=$_POST['rp'];
    }
    return get_successMsg();
  }

  public function repair_products(){
   // print_r($_POST['rp']);die;
    if(!empty($_POST['rp'])){     
      foreach ($_POST['rp'] as $key => $value) {
        $ready_product_id=$value;
       
        //echo $this->db->last_query();
          $this->db->where('id',$ready_product_id);
         $old_data = $this->db->get('department_ready_product')->row_array();
      
     $insert_array = array(
                      'product_code' => $old_data['product_code'],
                      'chitti_no' => $old_data['chitti_no'],
                      'karigar_id' => $old_data['karigar_id'],
                      'quantity_o' => $old_data['quantity'],
                      'quantity' => $old_data['quantity'],
                      'gr_wt_o' => $old_data['gr_wt'],
                      'net_wt_o' => $old_data['net_wt'],
                      'few_wt_o' => $old_data['few_wt'],
                      'kundan_pure_o' => $old_data['kundan_pure'],
                      'mina_wt_o' => $old_data['mina_wt'],
                      'wax_wt_o' => $old_data['wax_wt'],
                      'color_stone_o' => $old_data['color_stone'],
                      'moti_o' => $old_data['moti'],
                      'checker_wt_o' => $old_data['checker_wt'],
                      'checker_o' => $old_data['checker'],
                      'checker_pcs_o' => $old_data['checker_pcs'],
                      'checker_amt_o' => $old_data['checker_amt'],
                      'kundan_pc_o' => $old_data['kundan_pc'],
                      'kundan_rate_o' => $old_data['kundan_rate'],
                      'kundan_amt_o' => $old_data['kundan_amt'],
                      'stone_wt_o' => $old_data['stone_wt'],
                      'color_stone_rate_o' => $old_data['color_stone_rate'],
                      'stone_amt_o' => $old_data['stone_amt'],
                      'black_beads_wt_o' => $old_data['black_beads_wt'],
                      'ready_product_id' => $old_data['id'],
                      'department_id' => $old_data['department_id'],
                      'status' => '0',
                      'created_at' => date('Y-m-d H:i:s'),
                      'other_wt_o' =>$old_data['other_wt'],
                      'total_wt_o' =>$old_data['total_wt'],
                      'issue_product_date' =>date('Y-m-d'),
                      );
     // $this->db->insert('repair_product',$insert_array);
        if($this->db->insert('repair_product',$insert_array)){
           $this->db->set('status','4'); 
          $this->db->set('repair_date',date('Y-m-d H:i:s'));             
          $this->db->where('id',$ready_product_id);
          $this->db->update('department_ready_product');

           
          }
      }   //die;  
            $_SESSION['rp_id']=$_POST['rp']; 
    return get_successMsg();
    }
     return get_errorMsg();
  }

  private function get_details_of_ready_product($rp_id){
    $this->db->select('*');
    $this->db->from($this->table_name);
    $this->db->where('id',$rp_id);
    $result = $this->db->get()->row_array();
    return $result;
  }

  public function store_excel_data($insert_array){
   $stock_product_code=$this->insert_stock_into_tag_products($insert_array);
    $this->store_from_stock_excel($insert_array);
  }

  private function insert_stock_into_tag_products($data){
    if(!empty($data['parent_category_id'])){
      $insert_array=array();
      foreach ($data['parent_category_id'] as $key => $value) {
        $insert_array[] = array(
            'karigar_id'=>$data['karigar_id'][$key],
            'department_id'=>$data['department_id'][$key],
            'sub_code' =>$data['sub_code'][$key],
            'gr_wt' =>$data['gross_wt'][$key],
            'net_wt' =>$data['net_wt'][$key],
            'quantity'=>1,
            'product_code'=>$data['product_code'][$key],
            'status'=>6,
            'created_at'=>date('Y-m-d H:i:s'),
            'item_category_code'=>$data['parent_category_id'][$key],
            );
        //$this->db->insert('tag_products',$insert_array);
        /*echo $this->db->last_query();
        print_r($insert_array);*/
      }
    /*   echo"<pre>tag_products";
 print_r($insert_array);
 echo"</pre>";*/
      if (!empty($insert_array)) {
        $this->db->insert_batch('tag_products',$insert_array);
      } 
       
    }
  }

  private function store_from_stock_excel($insert_array){
    if(!empty($insert_array['parent_category_id'])){
      $insert_drp_array=array();
      foreach ($insert_array['parent_category_id'] as $key => $value) {
                $product_id=get_product_id($insert_array['product'][$key]);
        $category_id=get_category_id($insert_array['parent_category_id'][$key]);
         $insert_drp_array[] = array(
              'chitti_no' =>$insert_array['chitti_no'],  
              'quantity' =>$insert_array['quantity'][$key],    
              'gr_wt' =>$insert_array['gross_wt'][$key],
              'few_wt' =>$insert_array['few_wt'][$key],        
              'net_wt' =>$insert_array['net_wt'][$key],
              'kundan_pc' => $insert_array['kundan_pc'][$key],
              'color_stone' =>$insert_array['color_stone'][$key],
              'kundan_amt' =>$insert_array['kundan_amt'][$key],
              'product_code' =>$insert_array['product_code'][$key],
              'product' =>$insert_array['product'][$key],
              'wax_wt'=>$insert_array['wax_wt'][$key],
              'moti'=>$insert_array['moti'][$key],
              'product_from'=>'7',
              'department_id'=>$insert_array['department_id'][$key],
              'color_stone_rate' => $insert_array['color_stone_rate'][$key],
              'stone_amt' => $insert_array['stone_amt'][$key],
              'stone_wt' => $insert_array['stone_wt'][$key],
              'checker_amt' => $insert_array['checker_amt'][$key],
              'checker_rate' => $insert_array['checker'][$key],
              'checker_pcs' => $insert_array['checker_pcs'][$key],
              'kundan_pure' =>$insert_array['kundan_pure'][$key],
              'kundan_wt' =>$insert_array['kundan_wt'][$key],
              'kundan_rate' =>$insert_array['kundan_rate'][$key],
              'checker_wt' => $insert_array['checker_wt'][$key],
              'other_wt' => $insert_array['other_wt'][$key],
              'karigar_id' => $insert_array['karigar_id'][$key],
              'black_beads_wt' => @$insert_array['black_beads_wt'][$key],
              'total_wt' => $insert_array['total_wt'][$key],
              'mina_wt' => $insert_array['mina_wt'][$key], 
              'checker' => $insert_array['checker'][$key],
              'item_category_code' => $insert_array['parent_category_id'][$key],  
              //'barcode_path' => $this->create_barcode($insert_array['product_code'][$key]),
              'category_id' => $category_id['id'],
              'product_id' => $product_id['id'],
              'custom_net_wt'=>$insert_array['net_wt'][$key] + (10 / 100) * $insert_array['net_wt'][$key],
              'custom_gr_wt'=>$insert_array['gross_wt'][$key] + (10 / 100) * $insert_array['net_wt'][$key],           
              'status'=>0,
              'created_at' =>date('Y-m-d H:i:s'),
              'updated_at' =>date('Y-m-d H:i:s'),
            );
     
      }
 /*     echo"<pre>department_ready_product";
  print_r($insert_drp_array);
  echo"</pre>";*/
      if (!empty($insert_drp_array)) {
       $this->db->insert_batch('department_ready_product',$insert_drp_array);
      } 
       
    }
  }

  /*private function store_from_stock_excel($insert_array,$key){
      $insert_drp_array   =array(); 
        $insert_drp_array = array(
        'chitti_no' =>$insert_array['chitti_no'],  
        'quantity' =>$insert_array['quantity'][$key],    
        'gr_wt' =>$insert_array['gross_wt'][$key],
        'few_wt' =>$insert_array['few_wt'][$key],        
        'net_wt' =>$insert_array['net_wt'][$key],
        'kundan_pc' => $insert_array['kundan_pc'][$key],
        'color_stone' =>$insert_array['color_stone'][$key],
        'kundan_amt' =>$insert_array['kundan_amt'][$key],
        'product_code' =>$insert_array['product_code'][$key],
        'product' =>$insert_array['parent_category_id'][$key],
        'wax_wt'=>$insert_array['wax_wt'][$key],
        'moti'=>$insert_array['moti'][$key],
        'product_from'=>'7',
        'department_id'=>$insert_array['department_id'][$key],
        'color_stone_rate' => $insert_array['color_stone_rate'][$key],
        'stone_amt' => $insert_array['stone_amt'][$key],
        'stone_wt' => $insert_array['stone_wt'][$key],
        'checker_amt' => $insert_array['checker_amt'][$key],
        'checker_rate' => $insert_array['checker'][$key],
        'checker_pcs' => $insert_array['checker_pcs'][$key],
        'kundan_pure' =>$insert_array['kundan_pure'][$key],
        'kundan_wt' =>$insert_array['kundan_wt'][$key],
        'kundan_rate' =>$insert_array['kundan_rate'][$key],
        'checker_wt' => $insert_array['checker_wt'][$key],
        'other_wt' => $insert_array['other_wt'][$key],
        'karigar_id' => $insert_array['karigar_id'][$key],
        'black_beads_wt' => @$insert_array['black_beads_wt'][$key],
        'total_wt' => $insert_array['total_wt'][$key],
        'mina_wt' => $insert_array['mina_wt'][$key], 
        'checker' => $insert_array['checker'][$key],
        'item_category_code' => $insert_array['parent_category_id'][$key],             
        'status'=>0,
        'created_at' =>date('Y-m-d H:i:s'),
        'updated_at' =>date('Y-m-d H:i:s'),
      );
     //print_r($insert_drp_array);
       // die;
   $this->db->insert('department_ready_product',$insert_drp_array);

  }*/
    private function find_name($name)
  {
    $this->db->select('kundan_category,stone_category,black_beads_category');
    $this->db->from('parent_category');
    $this->db->where('name',$name);
    return $this->db->get()->row_array();
  }
  public function store_from_kundan($rp_id,$key,$code=''){
      $percentage=$this->find_name(trim($_POST['name'][$key]));
      $stone_category=($percentage['stone_category'] / 100) * $_POST['stone_wt'][$key];
      $kundan_category=($percentage['kundan_category'] / 100) * $_POST['kundan_pure'][$key];
      $black_beads_category=($percentage['black_beads_category'] / 100) * $_POST['black_beads_wt'][$key];
      $insert_drp_array   =array(); 
    if($_POST['action_type'] == 'repair_stock'){   
        $repair_stock_data=$this->add_repair_stock($_POST,$key);
      }
    if($_POST['action_type'] == 'stock' || $_POST['action_type'] == 'repair_stock'){
        //print_r($percentage);die;
        $product_id=get_product_id($_POST['parent_category_id'][$key]);
        $category_id=get_category_id($_POST['category_id']);
        //print_r($category_id);die;
        $insert_drp_array = array(
        'chitti_no' =>$code,  
        'quantity' =>$_POST['quantity'][$key],    
        'gr_wt' =>$_POST['gross_wt'][$key],
        'few_wt' =>$_POST['few_wt'][$key],        
        'net_wt' =>$_POST['net_wt'][$key],
        'kundan_pc' => $_POST['kundan_pc'][$key],
        'color_stone' =>$_POST['color_stone'][$key],
        'kundan_amt' =>$_POST['kundan_amt'][$key],
        'product_code' =>$_POST['product_code'][$key],
        'product' =>$_POST['parent_category_id'][$key],
        'wax_wt'=>$_POST['wax_wt'][$key],
        'moti'=>$_POST['moti'][$key],
        'product_from'=>'6',
        'department_id'=>$_POST['department_id'],
        'color_stone_rate' => $_POST['color_stone_rate'][$key],
        'stone_amt' => $_POST['stone_amt'][$key],
        'stone_wt' => $_POST['stone_wt'][$key],
        'checker_amt' => $_POST['checker_amt'][$key],
        'checker_rate' => $_POST['checker_rate'][$key],
        'checker_pcs' => $_POST['checker_pcs'][$key],
        'kundan_pure' =>$_POST['kundan_pure'][$key],
        'kundan_wt' =>$_POST['kundan_wt'][$key],
        'kundan_rate' =>$_POST['kundan_rate'][$key],
        'checker_wt' => $_POST['checker_wt'][$key],
        'other_wt' => $_POST['other_wt'][$key],
        'karigar_id' => $_POST['kariger_id'],
        'black_beads_wt' => $_POST['black_beads_wt'][$key],
        'total_wt' => $_POST['total_wt'][$key],
        'mina_wt' => $_POST['mina_wt'][$key], 
        'checker' => $_POST['checker_rate'][$key],
        'item_category_code' => $_POST['category_id'],
        'barcode_path' => $this->create_barcode($_POST['product_code'][$key]),
        'category_id' => $category_id['id'],
        'product_id' => $product_id['id'],
        'custom_gr_wt'=>$_POST['gross_wt'][$key],            
        'custom_net_wt'=>$_POST['net_wt'][$key]+$stone_category+$stone_category+$black_beads_category ,
        'custom_stone_wt'=>$_POST['stone_wt'][$key]-$stone_category,            
        'custom_kun_wt'=>$_POST['kundan_pure'][$key]-$kundan_category,            
        'custom_black_beads_wt'=>$_POST['black_beads_category'][$key]-$black_beads_category,            
        'status'=>'0',
        'created_at' =>date('Y-m-d H:i:s'),
        'updated_at' =>date('Y-m-d H:i:s'),
      );
      // print_r($_POST);die;
       
    }else{   
      
         $get_details = $this->get_product_details_by_rp_id($rp_id);

         $insert_drp_array = array(
          'msqr_id' =>@$get_details['msqr_id'],
          'ms_id' =>@$get_details['ms_id'],
          'chitti_no' =>@$get_details['ms_id'],
          'quantity' =>$_POST['quantity'][$key],
          'qc_id' =>$rp_id,
          'gr_wt' =>$_POST['gross_wt'][$key],
          'few_wt' =>$_POST['few_wt'][$key],        
          'net_wt' =>$_POST['net_wt'][$key],
          'kundan_pc' => $_POST['kundan_pc'][$key],
          'color_stone' =>$_POST['color_stone'][$key],
          'kundan_amt' =>$_POST['kundan_amt'][$key],
          'product_code' =>$_POST['product_code'][$key],
          'product' =>$_POST['name'][$key],
          'wax_wt'=>$_POST['wax_wt'][$key],
          'moti'=>$_POST['moti'][$key],
          'product_from'=>'1',
          'department_id'=>$_POST['department_id'][$key],
          'color_stone_rate' => $_POST['color_stone_rate'][$key],
          'stone_amt' => $_POST['stone_amt'][$key],
          'stone_wt' => $_POST['stone_wt'][$key]-$stone_category,          
          'checker_amt' => $_POST['checker_amt'][$key],
          'checker_rate' => $_POST['checker'][$key],
          'checker_pcs' => $_POST['checker_pcs'][$key],
          'kundan_pure' =>$_POST['kundan_pure'][$key],
          'kundan_wt' =>$_POST['kundan_wt'][$key],
          'kundan_rate' =>$_POST['kundan_rate'][$key],
          'checker_wt' => $_POST['checker_wt'][$key],
          'other_wt' => $_POST['other_wt'][$key],
          'total_wt' => $_POST['kundan_amt'][$key]+$_POST['stone_wt'][$key]+$_POST['other_wt'][$key],
          'custom_total_wt' => $_POST['kundan_amt'][$key]+($_POST['stone_wt'][$key]-$stone_category)+$_POST['other_wt'][$key],          
          //'total_wt' => $_POST['total_wt'][$key],
          'karigar_id' => $_POST['kariger_id'],
          'mina_wt' => $_POST['mina_wt'][$key],
          'checker' => $_POST['checker'][$key], 
          'item_category_code' => $_POST['item_category_code'][$key],
          'barcode_path' => $this->create_barcode($_POST['product_code'][$key]),
          'category_id' => $_POST['category_id'][$key],
          'product_id' => $_POST['product_id'][$key],
          'custom_gr_wt'=>$_POST['gross_wt'][$key],

          'custom_net_wt'=>$_POST['net_wt'][$key]+$stone_category+$stone_category+$black_beads_category ,

          'custom_stone_wt'=>$_POST['stone_wt'][$key]-$stone_category,            
          'custom_kun_wt'=>$_POST['kundan_pure'][$key]-$kundan_category,            
          'custom_black_beads_wt'=>$_POST['black_beads_category'][$key]-$black_beads_category,            
          'status'=>'0',
          'created_at' =>date('Y-m-d H:i:s'),
          'updated_at' =>date('Y-m-d H:i:s'),
        );

        
    }

    //print_r($insert_drp_array);die;

    $this->db->insert('department_ready_product',$insert_drp_array);
  }
  private function  add_repair_stock($post_data,$key){
        $this->load->library('zend'); 
    $this->zend->load('Zend/Barcode');
                if (!empty($post_data['product_code'][$key]) || !empty($post_data['category_id'])) {
                $untique = uniqid();
                $file = Zend_Barcode::draw('code128', 'image', array('text' => $post_data['product_code'][$key], 'drawText' => false), array());
                if (!is_dir(FCPATH . "uploads/barcode/" . $untique))
                    mkdir(FCPATH . "uploads/barcode/" . $untique);
                imagepng($file, FCPATH . "uploads/barcode/" . $untique . "/" . $untique . ".png");
                $insert['karigar_id'] = $post_data['kariger_id'];
                $insert['product_code'] = $post_data['product_code'][$key];
                $sub_code=explode('RE', $post_data['code'][$key]);
                //print_r($sub_code);die;
                $insert['sub_code'] = $sub_code['1'];
                $insert['item_category_code'] = $post_data['category_id'];
                $insert['barcode_path'] = $untique . "/" . $untique . ".png";
                $insert['department_id'] = $post_data['department_id'];
                $insert['status'] = 0;
                $insert['created_at'] = date('Y-m-d H:i:s');
                $this->db->insert('repair_stock', $insert);
                // print_r($insert);
                //  echo $this->db->last_query();die;
                // print_r($inserted_id);
               return true;
            }

  }

  public function update_from_new_stock(){   
    // print_r($_POST);die;
  $update_drp_array   =array(); 
        $update_drp_array = array(        
        'quantity' =>$_POST['quantity'],    
        'gr_wt' =>$_POST['gross_wt'],
        'few_wt' =>$_POST['few_wt'],        
        'net_wt' =>$_POST['net_wt'],
        'kundan_pc' => $_POST['kundan_pc'],
        'color_stone' =>$_POST['color_stone'],
        'kundan_amt' =>$_POST['kundan_amt'],
        'wax_wt'=>$_POST['wax_wt'],
        'moti'=>$_POST['moti'],
        'product_type'=>'7',
        'color_stone_rate' => $_POST['color_stone_rate'],
        'stone_amt' => $_POST['stone_amt'],
        'stone_wt' => $_POST['stone_wt'],
        'checker_amt' => $_POST['checker_amt'],
        'checker_rate' => $_POST['checker_rate'],
        'checker_pcs' => $_POST['checker_pcs'],
        'kundan_pure' =>$_POST['kundan_pure'],
        'kundan_wt' =>$_POST['kundan_wt'],
        'kundan_rate' =>$_POST['kundan_rate'],
        'checker_wt' => $_POST['checker_wt'],
        'checker' => $_POST['checker_rate'],
        'other_wt' => $_POST['other_wt'],
        'black_beads_wt' => @$_POST['black_beads_wt'],
        'total_wt' => $_POST['total_wt'],
        'mina_wt' => $_POST['mina_wt'], 
        'checker' => $_POST['checker'],  
        'karigar_id' => $_POST['kariger_id'],
        'custom_net_wt'=>@$_POST['custom_net_wt'], 
        'custom_gr_wt'=>@$_POST['custom_gr_wt'],
        'stone_category'=>@$_POST['stone_category'], 
        'kundan_category'=>@$_POST['kundan_category'], 
        'ch_category'=>@$_POST['ch_category'], 
        'custom_stone_wt'=>@$_POST['custom_stone_wt'], 
        'custom_checker_wt'=>@$_POST['custom_checker_wt'], 
        'custom_kundan_pure'=>@$_POST['custom_kundan_pure'],         
        'updated_at' =>date('Y-m-d H:i:s'),
      );
   
  
    //print_r($update_drp_array);die;
    $this->db->where('product_code',$_POST['product_code']);
    $this->db->update('department_ready_product',$update_drp_array);
  }

   public function update_from_kundan_receipt_code(){ 
     $update_kundan_recipt_array=array();
     $postdata=$_POST;
    
      foreach ($postdata['rp_id'] as $key => $value) {  
       $update_kundan_recipt_array = array(
           'gross_wt' => $postdata['gross_wt'][$key],
           'few_wt' => $postdata['few_wt'][$key],
           'net_wt' => $postdata['net_wt'][$key],
           'wax_wt' => $postdata['wax_wt'][$key],
           'color_stone' => $postdata['color_stone'][$key],
           'moti' => $postdata['moti'][$key],
           'kundan_pure' => $postdata['kundan_pure'][$key],
           'kundan_pc' => $postdata['kundan_pc'][$key],
           'stone_wt' => $postdata['stone_wt'][$key],
           'mina_wt' => $postdata['mina_wt'][$key],
           'kundan_wt' =>$postdata['kundan_wt'][$key],
           'kundan_rate' =>$postdata['kundan_rate'][$key],
           'other_wt' => $postdata['other_wt'][$key],
           'stone_amt' => $postdata['stone_amt'][$key],
           'kundan_amt' => $postdata['kundan_amt'][$key],         
           'checker_wt' => $postdata['checker_wt'][$key],
           'checker_pcs' => $postdata['checker_pcs'][$key],
           'checker@' => $postdata['checker'][$key],             
           'checker_amt' => $postdata['checker_amt'][$key],
           'color_stone_rate' => $postdata['color_stone_rate'][$key],
           'black_beads_wt' => $postdata['black_beads_wt'][$key],
            'total_wt' => $_POST['kundan_amt'][$key]+$_POST['stone_wt'][$key]+$_POST['other_wt'][$key],
           //'total_wt' => $postdata['total_wt'][$key],
           'updated_at' => date('Y-m-d'),       
        );
   //print_r($update_kundan_recipt_array);die;
      $this->db->where('id',$postdata['receive_product_id'][$key]);
      $this->db->update('Receive_products',$update_kundan_recipt_array);
  
      }   
  
  //die;
  }


  public function get_product_details_by_rp_id($rp_id){
    $this->db->select('qc.quantity,qc.gross_wt,tp.net_wt,qc.kundan_amt,qc.kundan_pc,qc.color_stone,qc.wax_wt,qc.moti,qc.id as rp_id,pm.name,msqr.id msqr_id,km.name as km_name,tp.id tp_id,tp.image,qc.gross_wt as gross_wt,d.name d_name,km.id as k_id,ms.id ms_id,rp.few_wt,tp.product_code as tp_code,rp.id,tp.department_id as department_id');
    $this->db->from('quality_control qc');
    $this->db->join('Receive_products rp',"qc.receive_product_id = rp.id",'left');
    $this->db->join('prepare_kundan_karigar_order po','po.id = rp.kmm_id','left');
    $this->db->join('make_set_quantity_relation msqr','msqr.id = po.msqr_id','left');
    $this->db->join('make_set ms','ms.id = msqr.make_set_id','left');
    $this->db->join('tag_products tp','qc.id = tp.id','left');
    $this->db->join('parent_category_codes pcc', 'pcc.code_name = tp.sub_code');
    $this->db->join('parent_category pm', 'pm.id = pcc.parent_category_id');
    //$this->db->join('parent_category pm','pm.id = tp.sub_category_id','left');
    $this->db->join('karigar_master km','km.id = po.karigar_id','left');
    $this->db->join('departments d','d.id = po.department_id','left');
    $this->db->where('qc.id',$rp_id);
    $result = $this->db->get()->row_array();
    //echo $this->db->last_query();
    return $result;
  }

  public function delete($product_code){
    $date=date('Y-m-d H:i:s');
    $this->db->set('delete_at',$date);
    $this->db->set('status','5');
    $this->db->where('product_code',$product_code);
    if($this->db->update($this->table_name)){
      return get_successMsg();
    }else{
      return get_errorMsg();
    }
  } 


  public function last_product_code($sub_code, $item_category_code) {
        $this->db->select('ifnull(count(id)+1,1) as max_count');
        $this->db->from('tag_products');
        $this->db->where('sub_code', $sub_code);
        $this->db->where('item_category_code', $item_category_code);
        $this->db->limit(1, 0);
        $result = $this->db->get()->row_array();
                //echo $this->db->last_query();
        
        return $result;
    }
    
 public function update_product_barcode(){
     //print_r($_POST['rp']);die;
    if(!empty($_POST['rp'])){
       $drp_id=$_POST['rp'];
          foreach ($drp_id as $key => $value){
            $barcode_chk=$this->find($value);
         ;
            $update_drp=array();
             if(empty($barcode_chk['barcode_path'])){
                $update_drp['barcode_path'] = $this->create_barcode($barcode_chk['product_code']);
              }
             $update_drp['custom_net_wt']=$barcode_chk['net_wt'] + (10 / 100) * $barcode_chk['net_wt'];
             $update_drp['custom_gr_wt']=$barcode_chk['drp_gr_wt'] + (10 / 100) * $barcode_chk['net_wt'];

             $this->db->where('id',$value);
             $this->db->where('product_code',$barcode_chk['product_code']);
             $this->db->update($this->table_name,$update_drp);
              $_SESSION['drp_id']=$drp_id;
         }
        return get_successMsg();
      }else{
         return get_errorMsg();
      }
 }

 private function create_barcode($product_code){
   if(!is_dir(FCPATH."uploads/barcode/$product_code")){
        mkdir(FCPATH."uploads/barcode/$product_code",0777);
    }
      $file = Zend_Barcode::draw('code128', 'image', array('text' => $product_code, 'drawText' => false), array());
     $store_image = imagepng($file,FCPATH."uploads/barcode/$product_code/".$product_code.".png");
     return "$product_code/".$product_code.".png";
  }

  public function update_location(){
     if(!empty($_POST['drp_id'])){     
       $voucher_no=$this->find($_POST['drp_id']);             
        
       $this->db->set('location_id',$_POST['location_id']);
       $this->db->where('id',$_POST['drp_id']);
       $this->db->where('product_code',$_POST['product_code']);
       $this->db->update($this->table_name);
       $response=get_successMsg();
       $response['voucher_no']=$voucher_no['voucher_no'];
       $response['status']="success";
        return  $response;
      }else{
         return get_errorMsg();
      }
 }
  
}//class
?>
