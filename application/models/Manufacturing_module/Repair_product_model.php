<?php
class Repair_product_model extends CI_Model {

  function __construct() {
  	parent::__construct();
    $this->table_name = "repair_product";
  }
  public function validate_postdata(){
    //print_r($_POST);die;
        if($_POST['department_id'] !='2' && $_POST['department_id']!='3' && $_POST['department_id'] !='6' && $_POST['department_id']!='10' && $_POST['department_id']!='11'){
          $this->form_validation->set_rules($this->config->item('repair_product', 'admin_validationrules'));
              $data['status']= 'success';
            if($this->form_validation->run()===FALSE){
              $data['status']= 'failure';
              $data['error'] = $this->getErrorMsg($_POST['id']);
         
              
            }
        }else{
          $this->form_validation->set_rules($this->config->item('repair_product_dep', 'admin_validationrules'));
              $data['status']= 'success';
            if($this->form_validation->run()===FALSE){
              $data['status']= 'failure';         
              $data['error'] = $this->getErrorMsg_dep($_POST['id']);
              
            }
      }

    return $data;
  }
  private function getErrorMsg($id){
    return array(
                 'quantity_'.$id => strip_tags(form_error('quantity')),                  
                 'gr_wt_'.$id => strip_tags(form_error('gr_wt')),
                 'net_wt_'.$id => strip_tags(form_error('net_wt')),
                 'kundan_pc_'.$id => strip_tags(form_error('kundan_pc')),
                 'kundan_rate_'.$id => strip_tags(form_error('kundan_rate')),
                 'kundan_amt_'.$id => strip_tags(form_error('kundan_amt')),
                 'checker_'.$id => strip_tags(form_error('checker')),
                 'checker_pcs_'.$id => strip_tags(form_error('checker_pcs')),
                 'stone_amt_'.$id => strip_tags(form_error('stone_amt')),                           
            
                 
    );
  }
    private function getErrorMsg_dep($id){
    return array(
                                  
                 'gr_wt_'.$id => strip_tags(form_error('gr_wt')),
                 'net_wt_'.$id => strip_tags(form_error('net_wt')),
                                          
            
                 
    );
  }
 
  // public function get($filter_status='',$status='',$params='',$search='',$limit='',$department_id='',$repair_status=""){    
  //   //$this->db->select('id,gr_wt,net_wt,quantity,product_code,department_id,chitti_no');
  //    $this->db->select('*');
  //   $this->db->from('department_ready_product');    
  //   if(!empty($repair_status)){
  //     $this->db->where('status',$repair_status);
  //   }
        
  //   if(!empty($department_id)){
  //     $this->db->where('department_id',$department_id);
  //   }

  //   if(isset($params['columns']) && !empty($params['columns'])){
  //     $filter_input=$params['columns'];
  //     $this->get_filter_value($filter_input,'manufacturing_repair_product');
  //   }
  //   if($limit == true){
  //     $this->db->order_by('id','DESC');
  //      $this->db->limit($params['length'],$params['start']);
  //     $result = $this->db->get()->result_array();
  //   // echo $this->db->last_query(); echo "<pre>"; print_r($result);exit;
  //   }else{
  //     $row_array = $this->db->get()->result_array();
  //     $result = count($row_array);
  //     //echo $this->db->last_query();print_r($result);exit;
  //   }
  //   //echo $this->db->last_query();print_r($result);exit;
  //   return $result;
  // }
 
  public function get($filter_status='',$status='',$params='',$search='',$limit='',$department_id='',$repair_status=""){ 
  //print_r($repair_status);exit;   
    //$this->db->select('id,gr_wt,net_wt,quantity,product_code,department_id,chitti_no');
     $this->db->select('*');
    $this->db->from('repair_product');    
    if($repair_status != ""){
      $this->db->where('status',$repair_status);
    }
        
    if(!empty($department_id)){
      $this->db->where('department_id',$department_id);
    }

    if(isset($params['columns']) && !empty($params['columns'])){
      $filter_input=$params['columns'];
      $this->get_filter_value($filter_input,'manufacturing_repair_product');
    }
    if($limit == true){
      $this->db->order_by('id','DESC');
       $this->db->limit($params['length'],$params['start']);
      $result = $this->db->get()->result_array();
     //echo $this->db->last_query(); echo "<pre>"; print_r($result);exit;
    }else{
      $row_array = $this->db->get()->result_array();
      $result = count($row_array);
      //echo $this->db->last_query();print_r($result);exit;
    }
    //echo $this->db->last_query();print_r($result);exit;
    return $result;
  }


  public function get_sent_repair_product($filter_status='',$status='',$params='',$search='',$limit='',$department_id=''){    
    //$this->db->select('id,gr_wt,net_wt,quantity,product_code,department_id,chitti_no');
     $this->db->select('*');
    $this->db->from('repair_product');    
    $this->db->where('status','1'); 
    if(!empty($department_id)){
      $this->db->where('department_id',$department_id);
    }

    if(isset($params['columns']) && !empty($params['columns'])){
      $filter_input=$params['columns'];
      $this->get_filter_value($filter_input,'manufacturing_repair_product');
    }
    if($limit == true){
      $this->db->order_by('id','DESC');
       $this->db->limit($params['length'],$params['start']);
      $result = $this->db->get()->result_array();
  //echo $this->db->last_query(); echo "<pre>"; print_r($result);exit;
    }else{
      $row_array = $this->db->get()->result_array();
      $result = count($row_array);
      //echo $this->db->last_query();print_r($result);exit;
    }
    //echo $this->db->last_query();print_r($result);exit;
    return $result;
  }



    public function get_differnt($filter_status='',$status='',$params='',$search='',$limit='',$department_id=''){    
    $this->db->select('rp.*,km.name as karigar_name');
    $this->db->from($this->table_name.' rp');  
    $this->db->join('karigar_master km','km.id=rp.karigar_id');
    $this->db->where('status','2');  
    if(!empty($department_id)){
      $this->db->where('department_id',$department_id);
    }

    if(isset($params['columns']) && !empty($params['columns'])){
      $filter_input=$params['columns'];
      $this->get_filter_value($filter_input,'manufacturing_repair_product');
    }
    if($limit == true){
     // $this->db->group_by('karigar_id');
      $this->db->order_by('id','DESC');
      $this->db->limit($params['length'],$params['start']);
      $result = $this->db->get()->result_array();
    }else{
      $row_array = $this->db->get()->result_array();
      $result = count($row_array);
      //echo $this->db->last_query();print_r($result);exit;
    }
   // echo $this->db->last_query();exit;print_r($result);exit;
    return $result;
  }

  private function get_filter_value($filter_input,$table_col_name){
    $column_name=array();  
    $filter_column_name=filter_column_name($table_col_name);
    $sql='';
    $i=0;     
     //print_r($filter_input);die;
    foreach ($filter_input as $key => $search_value){
       $column_name=$filter_column_name;
       //print_r($search_value['search']['value']);
        if(!empty($search_value['search']['value'])){
          if($i != 0){
            $sql.=' AND  ';
          }
            $sql.=''.$column_name[$key].' like "%'.$search_value['search']['value'].'%" ';
            $i++;
        

         }   
    }

    if(!empty($sql)){  
      $this->db->where($sql);  
    } 
       
  }
  public function get_repair_details($rp_id){
    $this->db->select('quantity,net_wt,gr_wt,product_code,ready_product_id, DATE_FORMAT(ready_product_date, "%d-%m-%Y") as date,department_id ');
    $this->db->from('repair_product');
    $this->db->where('status','2');    
    $this->db->where('ready_product_id',$rp_id);
    $result = $this->db->get()->result_array();
    //echo $this->db->last_query();print_r($result)die;
    return $result;
  }

  public function store($insert_array){     
  unset($insert_array['rp']);
    unset($insert_array['id']);
    $insert_array['status'] = 1;
 //print_r($insert_array);exit;
 if(!empty($insert_array ['net_wt']) && !empty($insert_array ['gr_wt'])){
            if($this->db->insert($this->table_name,$insert_array)){
                $update_array=array(
                    'net_wt'=>$insert_array ['net_wt'],
                    'gr_wt'=>$insert_array ['gr_wt'],
                    'quantity'=>@$insert_array ['quantity'],
                    'few_wt'=>@$insert_array ['few_wt'],
                    'kundan_pure'=>@$insert_array ['kundan_pure'],
                    'mina_wt'=>@$insert_array ['mina_wt'],
                    'wax_wt'=>@$insert_array ['wax_wt'],
                    'color_stone'=>@$insert_array ['color_stone'],
                    'moti'=>@$insert_array ['moti'],
                    'checker_wt'=>@$insert_array ['checker_wt'],
                    'checker'=>@$insert_array ['checker'],
                    'checker_pcs'=>@$insert_array ['checker_pcs'],
                    'checker_amt'=>@$insert_array ['checker_amt'],
                    'kundan_pc'=>@$insert_array ['kundan_pc'],
                    'kundan_rate'=>@$insert_array ['kundan_rate'],
                    'kundan_amt'=>@$insert_array ['kundan_amt'],
                    'stone_wt'=>@$insert_array ['stone_wt'],
                    'color_stone_rate'=>@$insert_array ['color_stone_rate'],
                    'stone_amt'=>@$insert_array ['stone_amt'],
                    'other_wt'=>@$insert_array ['other_wt'],
                    'total_wt'=>@$insert_array ['total_wt'],
                    'karigar_id'=>@$insert_array ['karigar_id'],
                    'department_id'=>@$insert_array ['department_id'],
                    'status'=>'0',
                    'repair_date'=>date('Y-m-d H:i:s'),

                  );
               
                $this->db->where('id',$insert_array['ready_product_id']);
                $this->db->update('department_ready_product',$update_array);                
                $data['status']= 'success';
                            
             }else{
              $data['status']= 'faliure';
             }
              return $data;
       }
             
               
  }

  public function update($insert_array,$id){
      unset($insert_array['rp']);
    unset($insert_array['id']);
    $insert_array['status'] = '2';
// print_r($insert_array);exit;
 if(!empty($insert_array ['net_wt']) && !empty($insert_array ['gr_wt'])){
            $this->db->where('id',$id); 
            if($this->db->update($this->table_name,$insert_array)){
                $update_array=array(
                    'net_wt'=>$insert_array ['net_wt'],
                    'gr_wt'=>$insert_array ['gr_wt'],
                    'quantity'=>@$insert_array ['quantity'],
                    'few_wt'=>@$insert_array ['few_wt'],
                    'kundan_pure'=>@$insert_array ['kundan_pure'],
                    'mina_wt'=>@$insert_array ['mina_wt'],
                    'wax_wt'=>@$insert_array ['wax_wt'],
                    'color_stone'=>@$insert_array ['color_stone'],
                    'moti'=>@$insert_array ['moti'],
                    'checker_wt'=>@$insert_array ['checker_wt'],
                    'checker'=>@$insert_array ['checker'],
                    'checker_pcs'=>@$insert_array ['checker_pcs'],
                    'checker_amt'=>@$insert_array ['checker_amt'],
                    'kundan_pc'=>@$insert_array ['kundan_pc'],
                    'kundan_rate'=>@$insert_array ['kundan_rate'],
                    'kundan_amt'=>@$insert_array ['kundan_amt'],
                    'stone_wt'=>@$insert_array ['stone_wt'],
                    'color_stone_rate'=>@$insert_array ['color_stone_rate'],
                    'stone_amt'=>@$insert_array ['stone_amt'],
                    'other_wt'=>@$insert_array ['other_wt'],
                    'total_wt'=>@$insert_array ['total_wt'],
                    'karigar_id'=>@$insert_array ['karigar_id'],
                    'department_id'=>@$insert_array ['department_id'],
                    'status'=>0,
                    'repair_date'=>date('Y-m-d H:i:s'),

                  );
               
                $this->db->where('id',$insert_array['ready_product_id']);
                $this->db->update('department_ready_product',$update_array);                
                $data['status']= 'success';
                            
             }else{
              $data['status']= 'faliure';
             }
              return $data;
       }
  }
  public function update_repair($id,$status){
    //print_r($status);exit;
     $this->db->set('status', $status);   
     $this->db->where('id', $id);   
     $this->db->update('repair_product'); 



     return true;
  }

  public function get_repair_product_by_id($id){
     // $this->db->where('id',$id);
     // $old_data = $this->db->get('department_ready_product')->row_array();
     // $insert_array = array(
     //                  'product_code' => $old_data['product_code'],
     //                  'chitti_no' => $old_data['chitti_no'],
     //                  'karigar_id' => $old_data['karigar_id'],
     //                  'quantity_o' => $old_data['quantity'],
     //                  'gr_wt_o' => $old_data['gr_wt'],
     //                  'net_wt_o' => $old_data['net_wt'],
     //                  'few_wt_o' => $old_data['few_wt'],
     //                  'kundan_pure_o' => $old_data['kundan_pure'],
     //                  'mina_wt_o' => $old_data['mina_wt'],
     //                  'wax_wt_o' => $old_data['wax_wt'],
     //                  'color_stone_o' => $old_data['color_stone'],
     //                  'moti_o' => $old_data['moti'],
     //                  'checker_wt_o' => $old_data['checker_wt'],
     //                  'checker_rate_o' => $old_data['checker_wt'],
     //                  'checker_o' => $old_data['checker'],
     //                  'checker_pcs_o' => $old_data['checker_pcs'],
     //                  'checker_amt_o' => $old_data['checker_amt'],
     //                  'kundan_pc_o' => $old_data['kundan_pc'],
     //                  'kundan_rate_o' => $old_data['kundan_rate'],
     //                  'kundan_amt_o' => $old_data['kundan_amt'],
     //                  'stone_wt_o' => $old_data['stone_wt'],
     //                  'color_stone_rate_o' => $old_data['color_stone_rate'],
     //                  'stone_amt_o' => $old_data['stone_amt'],
     //                  'ready_product_id' => $old_data['id'],
     //                  'department_id' => $old_data['department_id'],
     //                  'status' => 0,
     //                  'created_at' => date('Y-m-d H:i:s'),
     //                  'other_wt_o' =>$old_data['other_wt'],
     //                  'total_wt_o' =>$old_data['total_wt'],
     //                  'issue_product_date' =>date('Y-m-d')
     //                  );
     //  $this->db->insert('repair_product',$insert_array);

      return true;
  }
      public function get_repair_max_code($postdata) {
        if(!empty($postdata['item_category_code'])){
            $this->db->select("pc.*,(select ifnull(count(*)+1,1) from repair_stock where  product_code like concat('RE',`pcs`.`code_name`,'".$postdata['item_category_code']."','%'))as max_count,concat('RE',pcs.code_name) as code");             
            $this->db->from('parent_category pc');
            $this->db->join('parent_category_codes pcs','pcs.parent_category_id = pc.id','left');
            $this->db->join('category_master cm','cm.id = pc.category_id','left');
            $result = $this->db->get()->result_array();
            // echo $this->db->last_query();
            // print_r($result);die;
            return $result;
        }
    }

}//class
?>
