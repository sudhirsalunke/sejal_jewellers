<?php

class Mnfg_accounting_received_model extends CI_Model {

  function __construct() {
    $this->table_name = "quality_control";
    $this->table_name1 = "generate_packing_list";
    parent::__construct();
  }
 
 /* public function get($filter_status='',$status='',$params='',$search='',$limit='',$status='',$order_id=''){
    $this->db->select('kmm.id,mo.id order_id,DATE_FORMAT(kmm.created_at, "%d-%m-%Y") as order_date,DATE_FORMAT(kmm.delivery_date, "%d-%m-%Y") as delivery_date,pm.name sub_cat_name,w.from_weight,w.to_weight,kmm.quantity, kmm.approx_weight,km.name karigar_name,IF(rp.module = 2,sum(rp.quantity),0) rp_quantity,mom.product_code');
    $this->db->from('Karigar_manufacturing_mapping kmm');
    $this->db->join('manufacturing_order_mapping mom','mom.id=kmm.mop_id');
    $this->db->join('manufacturing_order mo','mo.id=mom.manufacturing_order_id');
    $this->db->join('parent_category pm','pm.id = mom.parent_category_id');
    $this->db->join('weights w','w.id = mom.weight_range_id');
    $this->db->join('karigar_master km','km.id = kmm.karigar_id');
    $this->db->join('Receive_products rp','rp.kmm_id = kmm.id','left');
    $this->db->group_by('kmm.id');
    if(!empty($order_id)){
      // $this->db->where('mo.id',$order_id);
      $this->db->where('pm.category_id',$order_id);
    }
    if(!empty($status)){
      $this->db->where('kmm.status',$status);
    }else{
      $this->db->where('kmm.status','0');
    }
     if(!empty($search)){
       $this->db->where("(pm.name LIKE '%$search%')");
      }
    $this->db->order_by('kmm.id','DESC');
    if(!empty($params) && $limit==true){
      $this->db->limit($params['length'],$params['start']);
    }
    $result = $this->db->get()->result_array();
    // print_r($this->db->last_query());die;
    return $result;

  }*/

   public function get($filter_status='',$params='',$search='',$limit='',$status='',$order_id=''){
      $this->db->select('ma.manufacturing_order_id order_id,ma.receipt_code,ma.quantity,mo.order_name name,mo.order_date,km.name karigar_name,pm.name sub_cat_name,w.from_weight,w.to_weight');
    $this->db->from('manufacturing_accounting ma');
    $this->db->join('manufacturing_order mo','mo.id=ma.manufacturing_order_id');
    $this->db->join('karigar_master km','km.id = ma.karigar_id');
    $this->db->join('parent_category pm','pm.id = ma.parent_category_id');
    $this->db->join('weights w','w.id = ma.weight_range_id');
    $this->db->where('ma.status','0');
    $this->db->where('ma.is_status_back','0');
    if(!empty($order_id)){
      $this->db->where('ma.manufacturing_order_id',$order_id);
    }
  /*  if(!empty($status)){
      $this->db->where('ma.status',$status);
      //$this->db->where('ma.is_status_back','0');
    }else{
      $this->db->where('ma.status','0');
    }*/
    if(!empty($search)){
       $this->db->where("(ma.receipt_code LIKE '%$search%' OR ma.manufacturing_order_id LIKE '%$search%' OR mo.order_name LIKE '%$search%' OR pm.name LIKE '%$search%')");
    }
    
    $this->db->order_by('ma.id','DESC');
    if(!empty($params) && $limit==true){
      $this->db->limit($params['length'],$params['start']);
    }
    $result = $this->db->get()->result_array();
    //print_r($this->db->last_query()); print_r($result); die;
    return $result;

  }
  public function get_order_wise_list($filter_status='',$status='',$params='',$search='',$limit='',$status=''){
    $this->db->select('ma.manufacturing_order_id order_id,ma.receipt_code,mo.order_name name,mo.order_date');
    $this->db->from('manufacturing_accounting ma');
    $this->db->join('manufacturing_order mo','mo.id=ma.manufacturing_order_id');
    $this->db->group_by('ma.receipt_code');
    $this->db->where('ma.status','0');
    $this->db->where('ma.is_status_back','0');
    /*if(!empty($status)){
      $this->db->where('ma.status',$status);
    }else{
      $this->db->where('ma.status','0');
    }*/
     if(!empty($search)){
       $this->db->where("(ma.receipt_code LIKE '%$search%' OR ma.manufacturing_order_id LIKE '%$search%' OR mo.order_name LIKE '%$search%')");
      }
    $this->db->order_by('ma.id','DESC');
    if(!empty($params) && $limit==true){
      $this->db->limit($params['length'],$params['start']);
    }
    $result = $this->db->get()->result_array();
    //print_r($this->db->last_query());die;
    return $result;
  }

 
  public function get_receipt_order($filter_status='',$status='',$params='',$search='',$limit='',$hallmarking ='',$id=''){
    
   // $this->db->select('rp.receipt_code,sum(rp.quantity) as quantity,mo.id order_id,mo.order_date,km.name karigar_name,pm.name,w.from_weight,w.to_weight,rp.id rp_id,sum(qc.quantity) as qc_quantity'); /*old*/
    $this->db->select('rp.receipt_code,(select SUM(quantity) from Receive_products where receipt_code= rp.receipt_code and status="4") as quantity,mo.id order_id,mo.order_date,km.name karigar_name,pm.name,w.from_weight,w.to_weight,rp.id rp_id,sum(qc.quantity) as qc_quantity');
    $this->db->from('Receive_products rp');
    $this->db->join('quality_control qc','qc.receive_product_id=rp.id','left');
    // $this->db->join('quality_control_hallmarking qch','qch.qc_id=qc.id','left');
    $this->db->join('Karigar_manufacturing_mapping kmm','rp.kmm_id=kmm.id');
    $this->db->join('manufacturing_order_mapping mom','mom.id=kmm.mop_id');
    $this->db->join('manufacturing_order mo','mo.id=mom.manufacturing_order_id');
    $this->db->join('parent_category pm','pm.id = mom.parent_category_id');
    $this->db->join('karigar_master km','km.id = kmm.karigar_id');
    $this->db->join('weights w','w.id = mom.weight_range_id');
    $this->db->where('rp.status','4');
    $this->db->where('rp.module','2');
    if(!empty($id))
      $this->db->where('rp.receipt_code',$id);
    if(!empty($search))
      $this->all_like_queries($search);
    if($limit == true)
      $this->db->limit($params['length'],$params['start']);
    if(!empty($filter_status)){
      $this->db->order_by('rp.created_at',$filter_status['dir']);
    }
    $this->db->where('rp.receipt_code is NOT NULL', NULL, FALSE);
    $this->db->order_by('rp.id','DESC');
    $this->db->group_by('rp.receipt_code');
    $result = $this->db->get()->result_array();
    // print_r($result);echo $this->db->last_query();exit;
    return $result;
  }

  public function get_by_receipt_code($code){
    $this->db->select('rp.*,sum(qc.quantity) as qc_quantity, w.from_weight, w.to_weight, mo.order_name, km.name km_name,km.id as karigar_id,pm.name,(select SUM(quantity) from Receive_products where receipt_code= rp.receipt_code) as rp_quantity,rp.quantity as rp_qnt,sum(qch.quantity) qch_quantity');
    $this->db->from('Receive_products rp');
    $this->db->join('quality_control qc','rp.id = qc.receive_product_id','left');
    $this->db->join('quality_control_hallmarking qch','qch.qc_id = qc.id','left');
    $this->db->join("Karigar_manufacturing_mapping kmm","kmm.id = rp.kmm_id");
    $this->db->join('manufacturing_order_mapping mop','mop.id=kmm.mop_id');
    $this->db->join('manufacturing_order mo','mo.id=mop.manufacturing_order_id');
    $this->db->join('parent_category pm','pm.id = mop.parent_category_id');
    $this->db->join('karigar_master km','km.id=kmm.karigar_id');
    $this->db->join('weights w','w.id = mop.weight_range_id');
    $this->db->where('rp.receipt_code',$code);
    $this->db->where('rp.status','4');
    $this->db->group_by('rp.id');
    return $this->db->get()->result_array();
  }

  private function all_like_queries($search){
    $this->db->where("(rp.quantity LIKE '%$search%' OR rp.receipt_code LIKE '%$search%' OR mo.id LIKE '%$search%' OR mo.order_date LIKE '%$search%' OR km.name LIKE '%$search%' OR pm.name LIKE '%$search%' OR w.from_weight LIKE '%$search%' OR w.to_weight LIKE '%$search%')");
  }
  public function update($id){
  	$this->db->where('id',$id);
  	$this->db->set('status','5');
  	if($this->db->update('Receive_products')){
  		return true;
  	}else{
  		return false;
  	}
  }
  public function find($rp_id){
    $this->db->select('rp.*,km.name,(w.from_weight+w.to_weight*rp.quantity/2) weight');
    $this->db->from('Receive_products rp');
    $this->db->join("Karigar_manufacturing_mapping kmm","kmm.id = rp.kmm_id");
    $this->db->join('manufacturing_order_mapping mop','mop.id=kmm.mop_id','left');
    $this->db->join('parent_category pm','pm.id = mop.parent_category_id');;
    $this->db->join('karigar_master km','km.id=kmm.karigar_id','left');
    $this->db->join('weights w','w.id = mop.weight_range_id');
    $this->db->where('rp.id',$rp_id);
    $result = $this->db->get()->row_array();
    return $result;
  }
  public function get_rec_qnt_by_kmm_id($order_id){
    $this->db->select('sum(quantity) qnt');
    $this->db->from('manufacturing_accounting');
    $this->db->where('manufacturing_order_id',$order_id);
    $this->db->where('status !=','1');
    $this->db->where('is_status_back','0');   
    $result = $this->db->get()->row_array();
    //echo $this->db->last_query();print_r($result); die;
    return $result;
  }
  
}
