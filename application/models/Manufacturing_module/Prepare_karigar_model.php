<?php

class Prepare_karigar_model extends CI_Model {

  function __construct() {
  	//$this->table_name = "departments";
    parent::__construct();
  }
  public function validation(){
    $this->form_validation->set_rules($this->config->item('prepare_karigar', 'admin_validationrules'));
    $data['status']= 'success';
    if($this->form_validation->run()===FALSE){
      $data['status']= 'failure';
      $data['data']= '';
      $data['error'] = $this->getErrorMsg("create");
    }
    return $data;
  }
  public function chng_kar_validation(){
    $this->form_validation->set_rules($this->config->item('prepare_karigar_to_change', 'admin_validationrules'));
    $data['status']= 'success';
    if($this->form_validation->run()===FALSE){
      $data['status']= 'failure';
      $data['data']= '';
      $data['error'] = $this->getErrorMsg("change");
    }
    return $data;
  }
  
  private function getErrorMsg($status=""){
    if($status == 'create'){
       return array(
        'quantity'=>strip_tags(form_error('quantity')),
        'mop_id'=>strip_tags(form_error('mop_id')),
        'karigar_id'=>strip_tags(form_error('karigar_id'))
      );
    }else{
       return array(
        'change_karigar_id'=>strip_tags(form_error('karigar_id'))
      );
    }
   
  }
  public function store($insert_array){
    if(!isset($insert_array['status']))
    $insert_array['status'] = '0';
    $insert_array['created_at'] = date('Y-m-d H:i:s');
    $insert_array['updated_at'] = date('Y-m-d H:i:s');
    //print_r($insert_array);die;
    if($this->db->insert('Karigar_manufacturing_mapping',$insert_array)){
      $insert_id = $this->db->insert_id();
      if($insert_array['department_id'] !='2' && $insert_array['department_id'] !='3' && $insert_array['department_id'] !='6' && $insert_array['department_id'] !='10'){
        $status = $this->update_quantity($insert_array['mop_id']);
      }else{
        $status = $this->update_weight($insert_array['mop_id']);
      }
      // $data['result'] = $this->Karigar_order_model->find($this->db->insert_id());   
      // $data['sub_cat_img'] = $this->Manufacturing_department_order_model->sub_cat_images(
      // $this->Karigar_order_model->find($id)['mom_id']);  
      if($status == true){
        $this->db->set('status','1');
        $this->db->where('id',$insert_array['mop_id']);
        $this->db->update('manufacturing_order_mapping');
      }
      return $insert_id;
    }else{
      return get_errorMsg();
    }
  }

  private function update_quantity($id){
    $this->db->select('mop.quantity,sum(kmm.quantity) as allocate_qnty');
    $this->db->from('manufacturing_order_mapping mop');
    $this->db->join('Karigar_manufacturing_mapping kmm','kmm.mop_id=mop.id');
    $this->db->where('mop.id',$id);
    $this->db->group_by('mop.id');
    $result = $this->db->get()->row_array();
    if(($quantity + $result['allocate_qnty']) == $result['quantity']){
        return true;
    }else{
        return false;
    }
  } 
  private function update_weight($id){
    $this->db->select('mop.weight,sum(kmm.total_weight) as allocate_wt');
    $this->db->from('manufacturing_order_mapping mop');
    $this->db->join('Karigar_manufacturing_mapping kmm','kmm.mop_id=mop.id','left');
    $this->db->where('mop.id',$id);
    $this->db->group_by('mop.id');
    $result = $this->db->get()->row_array();
    if(($weight + $result['allocate_wt']) == $result['weight']){
        return true;
    }else{
        return false;
    }
  } 
  public function change()
  {
    $update_kairger_arr = array(
                           "created_at" =>  date('Y-m-d H:i:s'),
                           "updated_at" =>  date('Y-m-d H:i:s'),
                           "karigar_id" =>  $_POST['karigar_id']
                        );
    $this->db->where("id",$_POST['kmm_id']);
    if($this->db->update('Karigar_manufacturing_mapping',$update_kairger_arr)){
        return get_successMsg();
    }
    else{
        return get_errorMsg();
    }
    
  }
  // public function get($filter_status='',$status='',$params='',$search='',$limit=''){
  //   $this->db->select('*');
  //   $this->db->from($this->table_name);
  //   if(!empty($filter_status))
  //      $this->db->order_by($status[$filter_status['column']],$filter_status['dir']);
  //   if(!empty($search)){
  //       $this->db->like('name',$search);
  //   }
  //   if($limit == true)
  //   	$this->db->limit($params['length'],$params['start']);
  //   $result = $this->db->get()->result_array();
  //   return $result;
  // }
  // public function find($id){
  // 	$this->db->where('id',$id);
  // 	$result = $this->db->get($this->table_name)->row_array();
  // 	return $result;
  // }
}