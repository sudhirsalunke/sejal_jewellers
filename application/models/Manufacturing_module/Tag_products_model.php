<?php
class Tag_products_model extends CI_Model {

    function __construct() {
        $this->table_name = "tag_products";
        $this->table_name1 = "quality_control";
        parent::__construct();
    }

    public function validate() {
        $data['status'] = "success";

        $this->form_validation->set_rules($this->config->item('tag_products', 'admin_validationrules'));
        if ($this->form_validation->run() === FALSE) {
            $data['status'] = 'failure';
            $data['data'] = '';
            $data['error'] = $this->getErrorMsg();
        }
        return $data;
    }

    public function validate_tag_products() {

           $data['status'] = 'success';
             
      // print_r($check_net['qc_net_wt_max']);die;
        if (!empty($_POST['tag_products'])) {
            $enterd_net_wt = array_filter(array_column($_POST['tag_products'], 'net_weight'));
            $select_billing_category = array_filter(array_column($_POST['tag_products'], 'billing_category_id'));
            if (empty($enterd_net_wt) && empty($select_billing_category)) {
                $data['status'] = 'failure1';
                $data['error'] = 'Please Enter at least one product value';
            } else {
                $net_sum = 0;
               
          
               $check_net=$this->check_net_wt_range($_POST['qc_id']); 
               //   print_r($check_net);die;
             // print_r($_POST['tag_products']);die;
                foreach ($_POST['tag_products'] as $key => $value) {
                    $net_sum += $value['net_weight'];
                      if (!empty($value['net_weight']) || !empty($value['billing_category_id'])) {
                        if ($value['net_weight'] < '0.1') {
                            $data['status'] = 'failure';
                            $data['error'][$key]['net_weight_error_' . $key] = 'Net Weight should be greater than 0';
                        } else if (!is_numeric($value['net_weight'])) {
                            $data['status'] = 'failure';
                            $data['error'][$key]['net_weight_error_' . $key] = 'Net Weight should be number';
                        }else  if ($value['gr_wt'] < '0.1') {
                            $data['status'] = 'failure';
                            $data['error'][$key]['gr_wt_error_' . $key] = 'Gross Weight should be greater than 0';
                        } else if (!is_numeric($value['gr_wt'])) {
                            $data['status'] = 'failure';
                            $data['error'][$key]['gr_wt_error_' . $key] = 'Gross Weight should be number';
                        }else if($value['net_weight'] > $value['gr_wt']){
                          $data['status'] = 'failure';
                          $data['error'][$key]['net_weight_error_'.$key] = 'Net Weight should not be greater then '.$value['gr_wt'];
                        } /* else if($value['net_weight'] > $value['qc_net_wt_max']){
                          $data['status'] = 'failure';
                          $data['error'][$key]['net_weight_error_'.$key] = 'Net Weight should not be greater then '.$value['qc_net_wt_max'];
                        }*/ else if (empty($value['net_weight'])) {
                            $data['status'] = 'failure';
                            $data['error'][$key]['net_weight_error_' . $key] = 'Net Weight is required';
                        }else if (empty($value['gr_wt'])) {
                            $data['status'] = 'failure';
                            $data['error'][$key]['gr_wt_error_' . $key] = 'Gross Weight is required';
                        } else if (empty($value['billing_category_id'])) {
                            $data['status'] = 'failure';
                            $data['error'][$key]['billing_category_id_error_' . $key] = 'Item category is required';
                        }/*else if($sum > $value['qc_net_wt_max']){                            
                          $data['status'] = 'failure';
                          $data['error'][$key]['net_weight_error_'.$key] ='Net Weight should not be greater then '.$value['qc_net_wt_max'];
                        }else if($sum < $value['qc_net_wt_max']){
                          $data['status'] = 'failure';
                          $data['error'] [$key]['net_weight_error_'.$key]='Net Weight should not be less then '.$value['qc_net_wt_min'];
                        } */
                       /* else if($check_net['remaining_net_wt'] <= 0){
                          $data['status'] = 'failure';
                          $data['error'] [$key]['net_weight_error_'.$key]='Net Weight Exist';
                        }*/else if($net_sum < $check_net['qc_net_wt_min'] && $_POST['remaining_qty_tag']=='1' &&  $check_net['remaining_net_wt'] >= 0){
                          $data['status'] = 'failure';
                          $data['error'] [$key]['net_weight_error_'.$key]='Net Weight should not be less then '.$check_net['qc_net_wt_min'];
                        }else if($net_sum > $check_net['qc_net_wt_max']){                            
                          $data['status'] = 'failure';
                          $data['error'][$key]['net_weight_error_'.$key] =$check_net['qc_net_wt_max'].'Net Weight should not be greater then '.$check_net['qc_net_wt_max'];
                        }
                    }
                }
            }
           // echo $max_net;
        }
        
        return $data;
    }

    public function getErrorMsg() {
        return array(
            'karigar_id' => strip_tags(form_error('karigar_id')),
            'sub_category_id' => strip_tags(form_error('sub_category_id')),
            'net_wt' => strip_tags(form_error('net_wt')),
            'gr_wt' => strip_tags(form_error('gr_wt')),
            'Quantity' => strip_tags(form_error('Quantity')),
            'is_sub_cat' => strip_tags(form_error('is_sub_cat'))
        );
    }

    public function get_engaged_karigar($department_id='') {
        $this->db->select('tp.karigar_id,km.name');
        $this->db->from('tag_products tp');
        $this->db->join('karigar_master km', 'km.id = tp.karigar_id');
        $this->db->where('status', '0');
        if (!empty($department_id)) {
            $this->db->where('tp.department_id', $department_id);
        }
        $this->db->group_by('tp.karigar_id');
        $result = $this->db->get()->result_array();
        return $result;
    }

    public function get_parent_category($department_id='') {
        $this->db->select('tp.*,pc.name,pc.id as id,w.from_weight,w.to_weight,tp.weight_range_id,qc.qc_net_wt_max,qc.qc_net_wt_min');
        $this->db->from('tag_products tp');
        $this->db->join('quality_control qc', 'qc.id = tp.qc_id','left');
        $this->db->join('parent_category_codes pcc', 'pcc.code_name = tp.sub_code','left');
        $this->db->join('parent_category pc', 'pc.id = pcc.parent_category_id','left');
        $this->db->join('weights w', 'w.id = tp.weight_range_id','left');
        $this->db->where(array('tp.status' => '0', 'tp.karigar_id' => $_POST['karigar_id']));
        $this->db->group_by('tp.sub_category_id,tp.weight_range_id');
        if (!empty($department_id)) {
            $this->db->where('tp.department_id', $department_id);
        }

        $result = $this->db->get()->result_array();
        return $result;
    }

    private function check_net_wt_range($qc_id='') {
        $this->db->select('qc.qc_net_wt_max,qc.qc_net_wt_min,qc.remaining_net_wt');
        $this->db->from('quality_control qc');   
        $this->db->where('qc.id', $qc_id);
        $result = $this->db->get()->row_array();
        return $result;
    }
    public function store($insert_array) {
        $inserted_id = array();
        // print_r($insert_array);die;
        foreach ($insert_array['tag_products'] as $key => $value) {
            //print_r($value);die;
            if (!empty($value['net_weight']) || !empty($value['item_category_code'])) {
                //print_r($value['billing_category_id']);die;
                // print_r($category); die;
                // $untique = uniqid();
                // $file = Zend_Barcode::draw('code128', 'image', array('text' => $value['product_code'], 'drawText' => false), array());
                // if (!is_dir(FCPATH . "uploads/barcode/" . $untique))
                //     mkdir(FCPATH . "uploads/barcode/" . $untique);
                // imagepng($file, FCPATH . "uploads/barcode/" . $untique . "/" . $untique . ".png");
                $insert['net_wt'] = $value['net_weight'];
                $insert['gr_wt'] = $value['gr_wt'];
                $insert['created_at'] = date('Y-m-d H:i:s');
                $insert['karigar_id'] = $insert_array['karigar_id'];
                $insert['quantity'] = 1;
                $insert['status'] = 1;
                $insert['product_code'] = $value['product_code'];
                $insert['billing_category_id'] = $value['billing_category_id'];
                $insert['barcode_path'] = $untique . "/" . $untique . ".png";
                $insert['department_id'] = $value['department_id'];
         /*       $insert['remaining_net_wt'] =$value['qc_net_wt_max'] - $value['net_weight'];
                $insert['qc_net_wt_max'] =$value['qc_net_wt_max'] - $value['net_weight'];
                $insert['qc_net_wt_min'] =$value['qc_net_wt_min'] - $value['net_weight'];*/
               // $item_category_code = explode('-', $value['product_code']);
                $insert['item_category_code'] = $value['item_category_code'];
                //$insert['id'] = $value['id'];
                $this->db->where('id', $value['id']);
                $this->db->update('tag_products', $insert);
                $inserted_id[] = $value['id'];
                $category=$this->Category_model->find($value['billing_category_id']);
                insert_max_count($value['billing_category_id'],$value['item_category_code'],$value['sub_code'],$value['department_id'],$category['name']);
                // echo $this->db->last_query();
                // print_r($inserted_id);
            }//die;
            $check_net=$this->check_net_wt_range($insert_array['qc_id']); 
            $update_net['qc_net_wt_max'] =$check_net['qc_net_wt_max'] - $value['net_weight'];
            $update_net['qc_net_wt_min'] =$check_net['qc_net_wt_min'] - $value['net_weight'];
            $update_net['remaining_net_wt'] =$check_net['remaining_net_wt'] - $value['net_weight'];

            $this->db->where('id', $insert_array['qc_id']);
            $this->db->update('quality_control',$update_net);
        }
        $total_recieved_qty = count($insert_array['tag_products']);
        if ($total_recieved_qty == $insert_array['remaining_qty_tag']) {
            $this->db->set('status', '11');/*tagging done*/
            $this->db->where('id', $insert_array['qc_id']);
            $this->db->update('quality_control');
        }
        return y_encrypt(implode(',', $inserted_id));
    }

    public function get($status = 0, $filter_status = '', $status1 = '', $params = '', $search = '', $limit = '', $department_id = '') {
        $this->db->select('tg.*,pm.name as sub_category_name,km.name karigar_name,qc.weight as gr_wt,tg.gr_wt as tg_gr_wt');
        $this->db->from($this->table_name . ' tg');
        $this->db->join('parent_category_codes pcc', 'pcc.code_name = tg.sub_code');
        $this->db->join('parent_category pm', 'pm.id = pcc.parent_category_id');
        //$this->db->join('parent_category pm', 'pm.id = tg.sub_category_id');
        $this->db->join('quality_control qc', 'qc.id = tg.qc_id');
        $this->db->join('karigar_master km', 'km.id = tg.karigar_id');
        $this->db->where('tg.status', $status);
        if (!empty($department_id)) {
            $this->db->where('tg.department_id', $department_id);
        }
        $this->db->order_by('km.name', 'asc');

        // $this->all_like_queries($search);

        if (isset($params['columns']) && !empty($params['columns'])) {
            $filter_input = $params['columns'];
            $table_col_name = "tag_products";
            $this->get_filter_value($filter_input, $table_col_name);
        }



        if ($limit == true) {
            $this->db->limit($params['length'], $params['start']);
            $result = $this->db->get()->result_array();
        } else {
            $row_array = $this->db->get()->result_array();
            $result = count($row_array);
            //echo $this->db->last_query();print_r($result);exit;
        }
        return $result;
    }

    private function get_filter_value($filter_input, $table_col_name) {
        $column_name = array();
        $filter_column_name = filter_column_name($table_col_name);
        $sql = '';
        $i = 0;

        //print_r($filter_input);die;
        foreach ($filter_input as $key => $search_value) {
            $column_name = $filter_column_name;
            //print_r($search_value['search']['value']);
            if (!empty($search_value['search']['value'])) {
                if ($i != 0) {
                    $sql .= ' AND  ';
                }
                $sql .= '' . $column_name[$key] . ' like "%' . $search_value['search']['value'] . '%" ';
                $i++;
            }
        }

        if (!empty($sql)) {
            $this->db->where($sql);
        }
    }

    private function all_like_queries($search) {
        $this->db->where("(tg.product_code LIKE '%$search%' OR pm.name LIKE '%$search%' OR km.name LIKE '%$search%')");
    }

    public function get_sub_category_name($id) {
        $this->db->select('pm.name,pm.id,w.id as weight_range_id');
        $this->db->from('quality_control qc');
        $this->db->join('Receive_products rp', 'rp.id=qc.receive_product_id');
        $this->db->join('Karigar_manufacturing_mapping kmm', 'rp.kmm_id=kmm.id');
        $this->db->join('manufacturing_order_mapping mom', 'mom.id=kmm.mop_id');
        $this->db->join('manufacturing_order mo', 'mo.id=mom.manufacturing_order_id');
        $this->db->join('parent_category pm', 'pm.id = mom.parent_category_id');
        $this->db->join('karigar_master km', 'km.id = kmm.karigar_id');
        $this->db->join('weights w', 'w.id = mom.weight_range_id');
        $this->db->where('qc.id', $id);
        $result = $this->db->get()->row_array();
        return $result;
    }

    public function remove_products() {

        if (empty($_POST['qc_id']) || empty($_POST['id'])) {
            $data['status'] = "error";
            $data['msg'] = "missing parameter";
        } else {
            $this->db->set('status', '8');
            $this->db->where('id', $_POST['qc_id']);
            $this->db->update('quality_control');

            $this->db->where('id', $_POST['id']);
            $this->db->delete($this->table_name);

            $data['status'] = "success";
        }
        return $data;
    }

    public function print_data($qc_id) {
        $this->db->select('pm.name as sub_category,tp.quantity,tp.product_code,km.name as karigar_name,tp.id,tp.net_wt,qc_id,tp.*,qc.weight as gr_wt');
        $this->db->from('tag_products tp');
        $this->db->join('quality_control qc','qc.id = tp.qc_id');
        $this->db->join('parent_category_codes pcc', 'pcc.code_name = tp.sub_code');
        $this->db->join('parent_category pm', 'pm.id = pcc.parent_category_id');
        // $this->db->join('parent_category pm', 'pm.id = tp.sub_category_id');
        $this->db->join('karigar_master km', 'km.id = tp.karigar_id');
        $this->db->where_in('tp.id', explode(',', $qc_id));
        return $result = $this->db->get()->result_array();
    }

    public function get_tag_done_products() {
        $this->db->select('GROUP_CONCAT(qc_id) as qc_ids');
        $result = $this->db->get('tag_products')->row_array();
        return explode(',', @$result['qc_ids']);
    }

    public function get_by_qc_id($qc_id) {
        $this->db->select('tp.*,pm.name as sub_category,km.name as karigar_name');
        $this->db->from('tag_products tp');
        $this->db->join('parent_category_codes pcc', 'pcc.code_name = tp.sub_code');
        $this->db->join('parent_category pm', 'pm.id = pcc.parent_category_id');
        //$this->db->join('parent_category pm', 'pm.id = tp.sub_category_id');
        $this->db->join('karigar_master km', 'km.id = tp.karigar_id');
        $this->db->where('qc_id', $qc_id);
        return $result = $this->db->get()->result_array();
    }

    public function get_quantity_by_id($id) {
        $this->db->select('sum(quantity) qnt');
        $this->db->from('tag_products');
        $this->db->where('qc_id', $id);
        $this->db->where('status > 0');
        $result = $this->db->get()->row_array();
        return $result['qnt'];
    }

    public function get_by_filter($filter = array(),$department_id='') {
        $this->db->select('tp.*,pm.name sub_category,km.name karigar_name,pm.code pm_code,w.from_weight,w.to_weight,qc.qc_net_wt_max,qc.qc_net_wt_min,tp.department_id,qc.weight as gr_wt');
        $this->db->from('tag_products tp');
        $this->db->join('quality_control qc', 'qc.id = tp.qc_id');
        //$this->db->join('parent_category pm', 'pm.id = tp.sub_category_id');
        $this->db->join('parent_category_codes pcc', 'pcc.code_name = tp.sub_code');
        $this->db->join('parent_category pm', 'pm.id = pcc.parent_category_id');
        $this->db->join('karigar_master km', 'km.id = tp.karigar_id');
        $this->db->join('weights w', 'w.id = tp.weight_range_id');
        if (!empty($department_id)) {
            $this->db->where('tp.department_id', $department_id);
        }
        $this->db->where($filter);
        $result = $this->db->get()->result_array();
        return $result;
    }


    public function get_max_code($postdata,$department_id) {
        if(!empty($postdata['item_category_code'])){
            // $this->db->select('pc.*,(select ifnull(count(id)+1,1) from tag_products where item_category_code = "'.$postdata['item_category_code'].'" and pcs.code_name = sub_code) as max_count,pcs.code_name as code');
    /*        $this->db->select("pc.*,(select ifnull(count(*)+1,1) from tag_products where  product_code like concat(`pcs`.`code_name`,'".$postdata['item_category_code']."','%'))as max_count,pcs.code_name as code");  */
        $this->db->select("pc.*, ifnull((select max_count+1 from product_code_max_count where product_code like concat('%',`pcs`.`code_name`,'".$postdata['item_category_code']."','%') and department_id='".$department_id."' and  sub_code=`pcs`.`code_name` and item_category_code='".$postdata['item_category_code']."'),1) as max_count,pcs.code_name as code");             
            $this->db->from('parent_category pc');
            $this->db->join('parent_category_codes pcs','pcs.parent_category_id = pc.id','left');
            $this->db->join('category_master cm','cm.id = pc.category_id','left');
            $result = $this->db->get()->result_array();
           /* echo $this->db->last_query();
            print_r($result);die;*/
            return $result;
        }
    }


    function last_product_code($sub_code, $item_category_code, $qc_id = '') {
        $this->db->select('product_code');
        $this->db->from('tag_products');
      // if ($qc_id != ''):
      //       $this->db->where('qc_id', $qc_id);
      //   endif;
        // $this->db->where('sub_code', $sub_code);
        // $this->db->where('item_category_code', $item_category_code);
        $this->db->where("product_code like concat('".$sub_code."','".$item_category_code."','%')");
       //$this->db->order_by('product_code', 'desc');
        $this->db->limit(1, 0);
        $result = $this->db->get()->row_array();
        return $result;
    }

    function check_product_code($product_code,$sub_code, $item_category_code, $qc_id = '') {
        $this->db->select('product_code');
        $this->db->from('tag_products');
      if ($qc_id != ''):
            $this->db->where('qc_id', $qc_id);
        endif;
        $this->db->where('sub_code', $sub_code);
        $this->db->where('item_category_code', $item_category_code);
        $this->db->where("product_code like '".$product_code."%'");
        // $this->db->order_by('product_code', 'desc');
        // $this->db->limit(1, 0);
        $result = $this->db->get()->row_array();
        // echo $this->db->last_query();
        // print_r($result);
        return $result;
    }

  public function get_max_product_code($postdata){
    $this->db->select('product_code');
    $this->db->from('tag_products tp');
/*    $this->db->where("tp.item_category_code",$postdata['item_code']);
    $this->db->where("tp.sub_code",$postdata['product_code']);*/
    $this->db->where("product_code like concat('".$postdata['product_code']."','".$postdata['item_code']."','%')");
    $result = $this->db->get()->row_array();
    if(empty($result['count'])){
      $result['count'] = 0;
    }
    return $result['count'];
  }
  public function check_tag_product_code($sub_code,$item_category_code,$product_code){
   
    $this->db->select('count(product_code) as count');
    $this->db->from('tag_products tp');
    // $this->db->where("tp.sub_code",$sub_code);
    // $this->db->where("tp.item_category_code",$item_category_code);
    // $this->db->where("tp.product_code",$product_code); 
    $this->db->where("tp.product_code like '".$product_code."'");
    $result = $this->db->get()->row_array();
 /*   echo $this->db->last_query();
    print_r($result);*/
    if(empty($result['count'])){
      $result['count'] = 0;
    }
    return $result['count'];
  }

  
  public function get_billing_catgory($qc_id,$department_id=""){
    /*$this->db->select('cm.id,cm.name,cm.code,cm.department_id,(select ifnull(count(id)+1,1) from tag_products where item_category_code = cm.code and sub_code = tp.sub_code) as max_count,tp.qc_net_wt_max,tp.qc_net_wt_min,tp.department_id,qc.weight as gr_wt');  */

    $this->db->select("cm.id,cm.name,cm.code,cm.department_id,(select ifnull(count(id)+1,1) from tag_products where product_code like concat(tp.sub_code,cm.code,'%')) as max_count,qc.qc_net_wt_max,qc.qc_net_wt_min,tp.department_id,qc.weight as gr_wt");
    $this->db->from('tag_products tp');
    $this->db->join('quality_control qc','qc.id = tp.qc_id');
    $this->db->join('Receive_products rp','rp.id = qc.receive_product_id');
    $this->db->join('category_master cm','rp.department_id = cm.department_id');
    $this->db->group_by('cm.id');
    $this->db->where('qc.module','2');
    $this->db->where("(tp.status='0' or qc.status='8')");
    if (!empty($department_id)) {
        $this->db->where('tp.department_id', $department_id);
    }
    /**/
    $this->db->where('qc.id',$qc_id);
    $result = $this->db->get()->result_array();
    return $result;
  }

  public function product_code_max_count_store(){
    $this->db->select("cm.id as category_id,cm.name as category,tp.item_category_code,(select ifnull(max_count+1,1) from product_code_max_count where product_code like concat('%',tp.sub_code,cm.code,'%') and department_id=cm.department_id and  sub_code=tp.sub_code)) as max_count,tp.department_id,tp.product_code");
    // (select ifnull(max_count+1,1) from product_code_max_count where product_code like concat('%',tp.sub_code,cm.code,'%') and department_id=cm.department_id and  sub_code=tp.sub_code)
    $this->db->from('tag_products tp');
    $this->db->join('category_master cm','tp.department_id = cm.department_id');
    $this->db->group_by('cm.id');   
    $result = $this->db->get()->result_array();
    //echo $this->db->last_query();die;
    return $result;

  }

}
?>
