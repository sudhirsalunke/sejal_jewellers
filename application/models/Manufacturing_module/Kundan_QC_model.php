<?php

class Kundan_QC_model extends CI_Model {
  function __construct() {
  	$this->table_name = "quality_control";
    parent::__construct();
  }
  public function get($filter_status='',$status='',$params='',$search='',$limit='',$department_id=''){  	
     if(isset($_GET['status']) && $_GET['status'] == 'pending' ||  $_GET['status'] == 'send_qc_pending'){
      $this->db->select('rp.quantity,rp.id as rp_id,pm.name,msqr.id,km.name as km_name,tp.id tp_id,tp.product_code,rp.department_id,tp.net_wt,po.receive_gr_wt as gr_wt');
            $this->db->from('Receive_products rp');   
            $this->db->join('prepare_kundan_karigar_order po','po.id = rp.kmm_id');
            $this->db->join('make_set_quantity_relation msqr','msqr.id = po.msqr_id');
            $this->db->join('tag_products tp','msqr.tag_id = tp.id');
            $this->db->join('quality_control qc','qc.id = tp.qc_id','left');
            //$this->db->join('weights w','w.id = tp.weight_range_id');
            $this->db->join('parent_category_codes pcc', 'pcc.code_name = tp.sub_code');
            $this->db->join('parent_category pm', 'pm.id = pcc.parent_category_id');
            //$this->db->join('parent_category pm','pm.id = tp.sub_category_id');
            $this->db->join('karigar_master km','km.id = po.karigar_id');     
        if(!empty($department_id)){
          $this->db->where('rp.department_id',$department_id);
        }
    
       $this->db->order_by('rp.id','DESC');
     }else if(isset($_GET['status']) && $_GET['status'] == 'rejected'){
      $this->db->select('rp.quantity,qc.id as qc_id,rp.id as rp_id,pm.name,msqr.id,km.name as km_name,tp.id tp_id,tp.product_code,tp.net_wt,po.receive_gr_wt as gr_wt');
      $this->db->from('quality_control qc');
      $this->db->join('Receive_products rp','rp.id = qc.receive_product_id');
    $this->db->join('prepare_kundan_karigar_order po','po.id = rp.kmm_id');
    $this->db->join('make_set_quantity_relation msqr','msqr.id = po.msqr_id');
    $this->db->join('tag_products tp','msqr.tag_id = tp.id');
    //$this->db->join('quality_control qc','qc.id = tp.qc_id','left');
    //$this->db->join('weights w','w.id = tp.weight_range_id');
    $this->db->join('parent_category_codes pcc', 'pcc.code_name = tp.sub_code');
    $this->db->join('parent_category pm', 'pm.id = pcc.parent_category_id');
    //$this->db->join('parent_category pm','pm.id = tp.sub_category_id');
    $this->db->join('karigar_master km','km.id = po.karigar_id');
       if(!empty($department_id)){
          $this->db->where('qc.department_id',$department_id);
        }
       $this->db->order_by('qc.id','DESC');
     }
    //$this->db->select('po.*,po.id as po_id,pm.name,msqr.id,km.name as km_name,tp.id tp_id,w.from_weight,w.to_weight,tp.product_code');
    // $this->db->from('prepare_kundan_karigar_order po');


/*  	if(!empty($filter_status)){
      $this->db->order_by('rp.id',$filter_status['dir']);
    }*/

    if($_GET['status'] == 'rejected' || $_GET['status'] == 'pending'|| $_GET['status'] == 'send_qc_pending'){
       $table_col_name="kundan_qc_tbl";
    }else{

       $table_col_name="kundan_QC";
    }

    if(isset($params['columns']) && !empty($params['columns'])){
      $filter_input=$params['columns'];
      $this->get_filter_value($filter_input,$table_col_name);
    }

                      
 /*   if(!empty($search)){
        $this->db->where("(km.name LIKE '%$search%' OR pm.name LIKE '%$search%' OR tp.product_code LIKE '%$search%')");
    }*/
    if(isset($_GET['status']) && $_GET['status'] == 'send_qc_pending'){
      $this->db->where('rp.status','8');
      $this->db->where('rp.module','3');
    }elseif(isset($_GET['status']) && $_GET['status'] == 'pending'){
      $this->db->where('rp.status','0');
      $this->db->where('rp.module','3');
    }elseif(isset($_GET['status']) && $_GET['status'] == 'complete'){
      $this->db->where('qc.status','8');
      $this->db->where('qc.module','3');
    }elseif(isset($_GET['status']) && $_GET['status'] == 'rejected'){
      $this->db->where('qc.status','0');
      $this->db->where('qc.module','3');
    }
    if($limit == true){
     
      $this->db->limit($params['length'],$params['start']);
      $result = $this->db->get()->result_array();
      // echo $this->db->last_query();print_r($result);exit;
    }else{
      $row_array = $this->db->get()->result_array();
      $result = count($row_array);
      //echo $this->db->last_query();print_r($result);exit;
    }
  //echo $this->db->last_query();die;
  	return $result;
  }
  private function get_filter_value($filter_input,$table_col_name){
    $column_name=array();  
    $filter_column_name=filter_column_name($table_col_name);
    $sql='';
    $i=0;
     
     //print_r($filter_input);die;
    foreach ($filter_input as $key => $search_value){
       $column_name=$filter_column_name;
       //print_r($search_value['search']['value']);
        if(!empty($search_value['search']['value'])){
          if($i != 0){
            $sql.=' AND  ';
          }
            $sql.=''.$column_name[$key].' like "%'.$search_value['search']['value'].'%" ';
            $i++;
        

         }   
    }

    if(!empty($sql)){  
      $this->db->where($sql);  
    } 
       
  }
  public function update($array,$pk){
  	$this->db->where($pk);
  	if($this->db->update($this->table_name,$array)){
  		return get_successMsg();
  	}else{
  		return get_errorMsg();
  	}
  }
  public function send_kundan_qc($rp_id){
    $this->db->where('id',$rp_id);
    $this->db->set('status','0');
    if($this->db->update('Receive_products')){
      return true;
    }else{
      return false;
    }
  
  }
  public function send_to_receive_products(){
  	$result = $this->find($_POST['id']);
  	$insert_array = array(
  			'module'=>'3',
  			'quantity'=>$result['quantity'],
  			'status'=>'0',
  			'kmm_id'=>$result['id'],
  			'karigar_id'=>$result['karigar_id'],
  			'department_id'=>$result['department_id'],
  		);
  	if($this->db->insert('Receive_products',$insert_array)){
  		$update_arr = array('status'=>2);
  		$pk = array('id'=>$_POST['id']);
  		$this->update($update_arr,$pk);
  		return get_successMsg();
  	}else{
  		return get_errorMsg();
  	}
  }
  public function find($id){
  	$this->db->where('id',$id);
  	return $this->db->get($this->table_name)->row_array();
  }
  public function store($insert_array){
    //print_r($insert_array);die;
    $insert_array['module'] = '3';
    if(isset($insert_array['rejected']) && $insert_array['rejected'] == true){
      $insert_array['status'] = '0';
      unset($insert_array['rejected']);
    }else{
      $insert_array['status'] = '8';
    }
    $insert_array['created_at'] = date('Y-m-d H:i:s');
    if($this->db->insert($this->table_name,$insert_array)){
      $insert_id = $this->db->insert_id();
      $this->db->set('status','5');/*Mfg kundan QC Accept status*/
      $this->db->where('id',$insert_array['receive_product_id']);
      $this->db->update('Receive_products');
      if($insert_array['status'] != '0'){
        // $get_details = $this->get_product_details_by_qc_id($insert_id);
        // $insert_drp_array = array(
        //     'msqr_id' =>$get_details['msqr_id'],
        //     'ms_id' =>$get_details['ms_id'],
        //     'chitti_no' =>$get_details['ms_id'],
        //     'quantity' =>$get_details['quantity'],
        //     'qc_id' =>$get_details['qc_id'],
        //     'gr_wt' =>$_POST['weight'],
        //     'net_wt' =>$get_details['net_wt'],
        //     'kundan_pcs' =>$get_details['kundan_pc'],
        //     'color_stone' =>$get_details['color_stone'],
        //     'kundan' =>$get_details['kundan_amt'],
        //     'product_code' =>$get_details['product_code'],
        //     'wax_wt'=>$get_details['wax_wt'],
        //     'moti'=>$get_details['moti'],
        //     'created_at' =>date('Y-m-d H:i:s'),
        //     'updated_at' =>date('Y-m-d H:i:s'),
        //   );
        // $this->db->insert('department_ready_product',$insert_drp_array);
        $this->db->set('status','11');
        $this->db->where('id',$insert_id);/*Mfg kundan QC Accept status*/
        $this->db->update('quality_control');
      }
      return true;
    }
  }
  public function ready_products(){
    foreach ($_POST['Qc'] as $key => $value) {
      $get_details = $this->get_product_details_by_qc_id($value);
      $insert_array = array(
          'msqr_id' =>$get_details['msqr_id'],
          'ms_id' =>$get_details['ms_id'],
          'chitti_no' =>$get_details['ms_id'],
          'quantity' =>$get_details['quantity'],
          'qc_id' =>$get_details['qc_id'],
          'gr_wt' =>$get_details['gross_wt'],
          'net_wt' =>$get_details['net_wt'],
          'kundan_pcs' =>$get_details['kundan_pc'],
          'color_stone' =>$get_details['color_stone'],
          'kundan' =>$get_details['kundan_amt'],
          'product_code' =>$get_details['product_code'],
          'created_at' =>date('Y-m-d H:i:s'),
          'updated_at' =>date('Y-m-d H:i:s'),
        );
      $this->db->insert('department_ready_product',$insert_array);
      $this->db->set('status','11');
      $this->db->where('id',$value);
      $this->db->update('quality_control');
      $pdf_data[] = $get_details;
    }
    return $pdf_data;
  }
  public function get_product_details_by_qc_id($qc_id){
    $this->db->select('rp.quantity,rp.net_wt,rp.kundan_amt,rp.kundan_pc,rp.color_stone,rp.wax_wt,rp.moti,qc.id as qc_id,rp.id as rp_id,pm.name,msqr.id msqr_id,km.name as km_name,tp.id tp_id,tp.image,rp.gross_wt as gross_wt,d.name d_name,km.id as k_id,tp.product_code,ms.id ms_id,msqr.pcs');
    $this->db->from('quality_control qc');
    $this->db->join('Receive_products rp','rp.id = qc.receive_product_id');
    $this->db->join('prepare_kundan_karigar_order po','po.id = rp.kmm_id');
    $this->db->join('make_set_quantity_relation msqr','msqr.id = po.msqr_id');
    $this->db->join('make_set ms','ms.id = msqr.make_set_id');
    $this->db->join('tag_products tp','msqr.tag_id = tp.id');
    $this->db->join('parent_category_codes pcc', 'pcc.code_name = tp.sub_code');
    $this->db->join('parent_category pm', 'pm.id = pcc.parent_category_id');
    //$this->db->join('parent_category pm','pm.id = tp.sub_category_id');
    $this->db->join('karigar_master km','km.id = po.karigar_id');
    $this->db->join('departments d','d.id = po.department_id');
    $this->db->where('qc.id',$qc_id);    
    $result = $this->db->get()->row_array();
    return $result;
  }
}