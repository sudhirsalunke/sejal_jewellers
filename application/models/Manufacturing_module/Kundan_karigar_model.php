<?php
class Kundan_karigar_model extends CI_Model {

  function __construct() {
  	$this->table_name = "prepare_kundan_karigar_order";
    parent::__construct();
  }
  public function validate(){
    //print_r($_POST);die;
    $data['status']= 'success';
    $postdata=$_POST;
    $error=array();
    //print_r($_POST['id']);die;
     $data['error'] = array();

               $this->form_validation->set_rules('kundan_pc', 'k pcs', 'trim|numeric|required|greater_than_equal_to[0.1]');
                  $this->form_validation->set_rules('k_gold', 'k gold', 'trim|numeric|required|greater_than_equal_to[0.1]');
                  $this->form_validation->set_rules('kundan_rate', 'k wt', 'trim|numeric|required|greater_than_equal_to[0.1]');
                  $this->form_validation->set_rules('kundan_amt', 'k amount', 'trim|numeric|required|greater_than_equal_to[0.1]');
              if($this->form_validation->run() == FALSE) {        
                $data['status'] = 'failure';
                $error = $this->receive_order_msg($_POST['id']);
                $data['error']= array_merge_recursive($data['error'],$error);
              }
      

    return $data;
  }
  

    public function receive_order(){
    $data['status'] = 'success';
     $error=array();
    //print_r($_POST['id']);die;
     $data['error'] = array();
    if(empty(array_filter($_POST['product']['kundan_pc']))){
     echo json_encode(array('status'=>'failure1','error'=>'Please Enter at least one product k pcs value'));die;
    
    }
     if(empty(array_filter($_POST['product']['k_gold']))){
     echo json_encode(array('status'=>'failure1','error'=>'Please Enter at least one product k gold value'));die;
    
    }
    if(empty(array_filter($_POST['product']['kundan_rate']))){
     echo json_encode(array('status'=>'failure1','error'=>'Please Enter at least one product k @ value'));die;
    
    }
    if(empty(array_filter($_POST['product']['kundan_amt']))){
     echo json_encode(array('status'=>'failure1','error'=>'Please Enter at least one product k amount value'));die;
    
    }
      foreach ($_POST ['product']['ids'] as $key => $value) {
            $this->value = $value;
        if(!empty($_POST['product']['k_pcs'][$key]) && !empty($_POST['product']['k_gold'][$key])){
                $_POST['id'] = $value;
                //$_POST['quantity'] = 1;
                $_POST['kundan_pc'] = $_POST['product']['kundan_pc'][$key];
                $_POST['k_gold'] = $_POST['product']['k_gold'][$key];
                $_POST['kundan_rate'] = $_POST['product']['kundan_rate'][$key];
                $_POST['kundan_amt'] = $_POST['product']['kundan_amt'][$key];
             
                //$this->form_validation->set_rules('quantity', 'Quantity', 'required|greater_than_equal_to[0.1]');
                $this->form_validation->set_rules('kundan_pc', 'k pcs', 'trim|numeric|required|greater_than_equal_to[0.1]');
                $this->form_validation->set_rules('k_gold', 'k gold', 'trim|numeric|required|greater_than_equal_to[0.1]');
                $this->form_validation->set_rules('kundan_rate', 'k wt', 'trim|numeric|required|greater_than_equal_to[0.1]');
                $this->form_validation->set_rules('kundan_amt', 'k amount', 'trim|numeric|required|greater_than_equal_to[0.1]');
           if ($this->form_validation->run() == FALSE) {        
              $data['status'] = 'failure';
              $error= $this->receive_order_msg($value);
              $data['error']= array_merge_recursive($data['error'],$error);
            }
      }
  }

      return $data;
  }

  private function receive_order_msg($key){
    return array(
      //'quantity'.$key=>strip_tags(form_error('quantity')),
      'k_gold_'.$key=>strip_tags(form_error('k_gold')),
      'kundan_pc_'.$key=>strip_tags(form_error('kundan_pc')),
      'kundan_rate_'.$key=>strip_tags(form_error('kundan_rate')),
      'kundan_amt_'.$key=>strip_tags(form_error('kundan_amt')),
      );
     //return $report;
  }


 /* private function getErrorMsg(){
    return array(      
        'quantity'=>strip_tags(form_error('quantity')),
        'kundan_pc'=>strip_tags(form_error('k_pcs')),
        'k_gold'=>strip_tags(form_error('k_gold')),
      );
  }*/
  public function get($filter_status='',$status='',$params='',$search='',$limit='',$department_id=''){
    // if($_GET['status'] == 'complete'){
      $this->db->select('po.*,pm.name,msqr.id as msqr_id,km.name as km_name,(po.quantity-po.receive_qnt) as quantity,tp.product_code,po.issue_voucher,w.from_weight,w.to_weight,msqr.pcs,po.department_id,tp.net_wt,qc.weight as gr_wt,km.wastage,km.kund_rate,tp.gr_wt as tg_gr_wt');
    // }else{
  	 //$this->db->select('po.*,pm.name,msqr.id as msqr_id,km.name as km_name,(sum(po.quantity)-sum(po.receive_qnt)) as quantity,tp.product_code,po.issue_voucher');
    // }
  	$this->db->from('prepare_kundan_karigar_order po');
  	$this->db->join('make_set_quantity_relation msqr','msqr.id = po.msqr_id');
  	$this->db->join('tag_products tp','msqr.tag_id = tp.id');
    $this->db->join('quality_control qc','qc.id = tp.qc_id');
  	//$this->db->join('parent_category pm','pm.id = tp.sub_category_id');
    $this->db->join('parent_category_codes pcc', 'pcc.code_name = tp.sub_code');
    $this->db->join('parent_category pm', 'pm.id = pcc.parent_category_id');
    $this->db->join('karigar_master km','km.id = po.karigar_id');
  	$this->db->join('weights w','w.id = tp.weight_range_id');
  
    if(!empty($filter_status)){
      $this->db->order_by('po.id',$filter_status['dir']);
    }
   if(@$_GET['status'] == 'pending'){ 
      $table_col_name='kundan_karigar_pending';
      }else{  
         $table_col_name='kundan_karigar';
      } 
    if(isset($params['columns']) && !empty($params['columns'])){
      $filter_input=$params['columns'];    
      $this->get_filter_value($filter_input,$table_col_name);
    }
/*    if(!empty($search)){
        $this->db->where("(km.name LIKE '%$search%' OR pm.name LIKE '%$search%' OR tp.product_code LIKE '%$search%' OR po.issue_voucher LIKE '%$search%')");
    }*/
   /*  if(!empty($department_id)){
      $this->db->where('po.department_id',$department_id);
    }*/
    if($_GET['status'] == 'pending'){
    	$this->db->where('po.status',0);
      $this->db->group_by('po.issue_voucher');
    }
    if($_GET['status'] == 'complete'){
      $this->db->where('po.status',1);
      // $this->db->where('po.status',1);
      // $this->db->group_by('po.issue_voucher');
    }
    
    if($limit == true){
      $this->db->limit($params['length'],$params['start']);
      $result = $this->db->get()->result_array();
      //echo $this->db->last_query();print_r($result);exit;
    }else{
      $row_array = $this->db->get()->result_array();
      $result = count($row_array);
      //echo $this->db->last_query();print_r($result);exit;
    }
  	return $result;
  }
  private function get_filter_value($filter_input,$table_col_name){
    $column_name=array();  
    $filter_column_name=filter_column_name($table_col_name);
    $sql='';
    $i=0;
     
     //print_r($filter_input);die;
    foreach ($filter_input as $key => $search_value){
       $column_name=$filter_column_name;
       //print_r($search_value['search']['value']);
        if(!empty($search_value['search']['value'])){
          if($i != 0){
            $sql.=' AND  ';
          }
            $sql.=''.$column_name[$key].' like "%'.$search_value['search']['value'].'%" ';
            $i++;
        

         }   
    }

    if(!empty($sql)){  
      $this->db->where($sql);  
    } 
       
  }
  public function update($array,$pk){
  	$this->db->where($pk);
  	if($this->db->update($this->table_name,$array)){
  		return get_successMsg();
  	}else{
  		return get_errorMsg();
  	}
  }
  public function send_to_receive_products(){
    $check_error = $this->check_quantity();
    if($check_error['status'] == 'failure'){
      echo json_encode($check_error);die;
    }      
    $result = $this->find($_POST['id']);
        //print_r($_POST);
    //print_r($result);
      $insert_array = array(
          'module'=>'3',
          'quantity'=>$_POST['quantity'],
          'date'=>date('Y-m-d'),
          'status'=>'5',
          'kmm_id'=>$result['id'],
          'karigar_id'=>$result['karigar_id'],
          'department_id'=>$result['department_id'],
          'kundan_pc'=>$_POST['kundan_pc'],
          'k_gold'=>$_POST['k_gold'],
          'kundan_amt'=>$_POST['kundan_amt'],
          'kundan_rate'=>$_POST['kundan_rate'],
          'net_wt'=>$_POST['net_wt'],
          'gross_wt'=>$_POST['gross_wt'],
        );
     if($this->db->insert('Receive_products',$insert_array)){
      $quality_array = array(
              'module'=>'3',
              'status'=>'11',
              'receive_product_id'=>$this->db->insert_id(),
              'kundan_id'=>$result['karigar_id'],
              'quantity'=>$_POST['quantity'],
              'weight'=>$_POST['net_wt'],
              'net_wt'=>$_POST['net_wt'],
              'gross_wt'=>$_POST['gross_wt'],
              'department_id'=>$result['department_id'],
              'user_id'=>$this->session->userdata('user_id'),
        );
          $this->db->insert('quality_control',$quality_array);

      $is_update = $this->check_quantity();
      if($is_update['is_update'] == true && $is_update['status'] == 'success'){
    		$update_arr = array('status'=>2);
    		$pk = array('id'=>$_POST['id']);
    		$this->update($update_arr,$pk);
      }
      $update_arr = array('receive_qnt'=>$is_update['quantity']);
      $update_arr = array('receive_gr_wt'=>$_POST['gross_wt']);

      $pk = array('id'=>$_POST['id']);
      $this->update($update_arr,$pk);
  		return get_successMsg();
  	}else{
  		return get_errorMsg();
  	}
  }
  private function check_quantity(){
    $data = array();
    $this->db->select('quantity,receive_qnt');
    $this->db->from($this->table_name);
    $this->db->where('id',$_POST['id']);
    $result = $this->db->get()->row_array();
    if(($result['receive_qnt'] + $_POST['quantity']) == $result['quantity']){
      $data['status'] = 'success';
      $data['is_update'] = true;
      $data['quantity'] = $result['receive_qnt'] + $_POST['quantity'];
    }elseif(($result['receive_qnt'] + $_POST['quantity']) < $result['quantity']){
      $data['status'] = 'success';
      $data['is_update'] = false;
      $data['quantity'] = $result['receive_qnt'] + $_POST['quantity'];
    }elseif(($result['receive_qnt'] + $_POST['quantity']) > $result['quantity']){
      $data['status'] = 'failure';
      $data['is_update'] = false;
      $data['error']['quantity'] = 'Quantity exceeded';
    }
    return $data;
  }
  public function find($id){
  	$this->db->where('id',$id);
  	return $this->db->get($this->table_name)->row_array();
  }
  public function reprint($issue_voucher){
    $this->db->select('msqr.quantity,tp.product_code,tp.net_wt,msqr.make_set_id,tp.image,msqr.tag_id,pm.name as s_name');
    $this->db->from('prepare_kundan_karigar_order pkko');
    $this->db->join('make_set_quantity_relation msqr','pkko.msqr_id=msqr.id');
    $this->db->join('tag_products tp','tp.id=msqr.tag_id');
    $this->db->join('quality_control qc','qc.id=tp.qc_id');
    $this->db->join('parent_category_codes pcc', 'pcc.code_name = tp.sub_code');
    $this->db->join('parent_category pm', 'pm.id = pcc.parent_category_id');
    //$this->db->join('parent_category pm','pm.id = tp.sub_category_id');
    $this->db->where('pkko.issue_voucher',$issue_voucher);
    $result = $this->db->get()->result_array();
    return $result;
  }
}