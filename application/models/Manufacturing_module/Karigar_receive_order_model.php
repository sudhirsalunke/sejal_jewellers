<?php
class Karigar_receive_order_model extends CI_Model {

  function __construct() {
  	$this->table_name = "Receive_products";
    parent::__construct();
  }

  public function validate_excel(){
    if(!empty($_POST['rp_id'])){
        
        if(empty($_POST['type'])){
          unset($_POST['type']);
        }
      $postdata = $_POST;
      $data['status'] = 'success';
   //print_r($postdata['type']);die;

      foreach ($postdata['rp_id'] as $key => $value) {    
        if($postdata['action_type']=='stock'  && empty($postdata['parent_category_id'][$key])){
          $data['status'] = 'failure';
          $data['error'][] = 'Row '.($key+1).' Please select product can not be empty';
        }
        if($postdata['action_type']=='stock'   && empty($postdata['product_code'][$key])){
          $data['status'] = 'failure';
          $data['error'][] = 'Row '.($key+1).' Iteam Code can not be empty';
        }
                
        if($postdata['action_type']=='stock'  && empty($postdata['net_wt'][$key])){
          $data['status'] = 'failure';
          $data['error'][] = 'Row '.($key+1).' Net wt can not be empty';
        }

        if($postdata['action_type'] == 'repair_stock' && empty($postdata['parent_category_id'][$key])){
          $data['status'] = 'failure';
          $data['error'][] = 'Row '.($key+1).' Please select product can not be empty';
        }
        if( $postdata['action_type'] == 'repair_stock'  && empty($postdata['product_code'][$key])){
          $data['status'] = 'failure';
          $data['error'][] = 'Row '.($key+1).' Iteam Code can not be empty';
        }
                
        if($postdata['action_type'] == 'repair_stock'  && empty($postdata['net_wt'][$key])){
          $data['status'] = 'failure';
          $data['error'][] = 'Row '.($key+1).' Net wt can not be empty';
        }
        

        if($postdata['action_type']=='excel'  && empty($postdata['product_code'][$key])){
          $data['status'] = 'failure';
          $data['error'][] = 'Row '.($key+1).' Iteam Code can not be empty';
        }
           

        if($postdata['action_type']=='excel'  && empty($postdata['parent_category_id'][$key])){
          $data['status'] = 'failure';
          $data['error'][] = 'Row '.($key+1).' Product can not map in deparment';
        }
    
                
        if($postdata['action_type'] =='excel' && empty($postdata['net_wt'][$key])){
          $data['status'] = 'failure';
          $data['error'][] = 'Row '.($key+1).' Net wt can not be empty';
        }

         if($postdata['action_type'] =='edit'  &&  empty($postdata['net_wt'][$key])){
          $data['status'] = 'failure';
          $data['error'][] = 'Row '.($key+1).' Net wt can not be empty';
        }
        
        if($postdata['wax_wt'][$key] < 0.00){
          $data['status'] = 'failure';
          $data['error'][] = 'Row '.($key+1).' Wax wt should be greater than 0.00';
        }
       
        if(!empty($postdata['wax_wt'][$key]) && empty($postdata['kundan_pc'][$key])){
          $data['status'] = 'failure';
          $data['error'][] = 'Row '.($key+1).' Kundan PC can not be empty';
        }
        if(!empty($postdata['kundan_pc'][$key]) && empty($postdata['kundan_rate'][$key])){
          $data['status'] = 'failure';
          $data['error'][] = 'Row '.($key+1).' Kundan @ can not be empty';
        }
        if((!empty($postdata['color_stone'][$key]) || !empty($postdata['moti'][$key])) && empty($postdata['stone_amt'][$key])){
          $data['status'] = 'failure';
          $data['error'][] = 'Row '.($key+1).' Stone amount can not be empty';
        }
        if((!empty($postdata['checker'][$key]))  && empty($postdata['checker_pcs'][$key])){
          $data['status'] = 'failure';
          $data['error'][] = 'Row '.($key+1).'checker @ can not be empty';
        }
        if(($postdata['type']=='store' && !empty($postdata['checker_wt'][$key]))  && empty($postdata['checker'][$key])){
          $data['status'] = 'failure';
          $data['error'][] = 'Row '.($key+1).' checker pcs can not be empty';
        }
        
        if(($postdata['action_type']=='stock'  && !empty($postdata['checker_pcs'][$key]))  && empty($postdata['checker_rate'][$key])){
          $data['status'] = 'failure';
          $data['error'][] = 'Row '.($key+1).'checker @ can not be empty';
        }

        if(($postdata['action_type']=='stock'  && !empty($postdata['checker_wt'][$key]))  && empty($postdata['checker_pcs'][$key])){
          $data['status'] = 'failure';
          $data['error'][] = 'Row '.($key+1).' checker pcs can not be empty';
        } 

        if(($postdata['action_type'] == 'repair_stock' && !empty($postdata['checker_pcs'][$key]))  && empty($postdata['checker_rate'][$key])){
          $data['status'] = 'failure';
          $data['error'][] = 'Row '.($key+1).'checker @ can not be empty';
        }

        if(($postdata['action_type'] == 'repair_stock' && !empty($postdata['checker_wt'][$key]))  && empty($postdata['checker_pcs'][$key])){
          $data['status'] = 'failure';
          $data['error'][] = 'Row '.($key+1).' checker pcs can not be empty';
        } 

        ///print_r($postdata['type'] );die;
        // if($postdata['type'] != 'store'){
        //   $result_product=$this->Tag_products_model->check_tag_product_code($postdata['code'][$key],$postdata['category_id'],$postdata['product_code'][$key]);
        // } 

        // if($postdata['action_type'] != 'repair_stock'){
        //   $result_product=$this->Tag_products_model->check_tag_product_code($postdata['code'][$key],$postdata['category_id'],$postdata['product_code'][$key]);
        // }
        // if($postdata['action_type'] != 'edit'){
        //   $result_product=$this->Tag_products_model->check_tag_product_code($postdata['code'][$key],$postdata['category_id'],$postdata['product_code'][$key]);
        // }
   

        if(!empty($postdata['product_code'][$key])  && $result_product == '1'){
          $data['status'] = 'failure';
          $data['error'][] = 'Row '.($key+1).' Product code already taken other User';
        }
        
      }
    }
    // print_r($postdata['type']);
    // print_r($data);die;
    return $data;

  }

  public function get($filter_status='',$status='',$params='',$search='',$limit='',$department_id=''){
  	$this->db->select('rp.quantity,pm.id,pm.name,msqr.id,km.name as km_name,tp.product_code,w.from_weight,w.to_weight,rp.net_wt,(CASE WHEN qc.gross_wt is NULL THEN qc.weight  ELSE  rp.gross_wt END)as gross_wt');
    $this->db->from('quality_control qc');
    $this->db->join('Receive_products rp','rp.id = qc.receive_product_id');
  	$this->db->join('prepare_kundan_karigar_order po','po.id = rp.kmm_id');
  	$this->db->join('make_set_quantity_relation msqr','msqr.id = po.msqr_id');
  	$this->db->join('tag_products tp','msqr.tag_id = tp.id');
  	// $this->db->join('parent_category pm','pm.id = tp.sub_category_id');
    $this->db->join('parent_category_codes pcc', 'pcc.code_name = tp.sub_code');
    $this->db->join('parent_category pm', 'pm.id = pcc.parent_category_id');
  	$this->db->join('karigar_master km','km.id = po.karigar_id');
    $this->db->join('weights w','w.id = tp.weight_range_id');
    if(!empty($department_id)){
      $this->db->where('qc.department_id',$department_id);
    }
  	
  	if(!empty($filter_status)){
      $this->db->order_by('rp.id',$filter_status['dir']);
    }
    if(isset($params['columns']) && !empty($params['columns'])){
      $filter_input=$params['columns'];
      $table_col_name="received_products";
      $this->get_filter_value($filter_input,$table_col_name);
    }
  /*  if(!empty($search)){
        $this->db->where("(km.name LIKE '%$search%' OR pm.name LIKE '%$search%' OR tp.product_code LIKE '%$search%')");
    }*/
    $this->db->where('qc.status','11');
    $this->db->where('rp.module','3');

    if($limit == true){
      $this->db->order_by('qc.id','DESC');
      $this->db->limit($params['length'],$params['start']);
      $result = $this->db->get()->result_array();
      //echo $this->db->last_query();print_r($result);exit;
    }else{
      $row_array = $this->db->get()->result_array();
      $result = count($row_array);
      //echo $this->db->last_query();print_r($result);exit;
    }
  	return $result;



    // $this->db->select('po.*,pm.name,msqr.id as msqr_id,km.name as km_name,(po.quantity-po.receive_qnt) as quantity,tp.product_code,po.issue_voucher,w.from_weight,w.to_weight,msqr.pcs');
    // $this->db->from('prepare_kundan_karigar_order po');
    // $this->db->join('make_set_quantity_relation msqr','msqr.id = po.msqr_id');
    // $this->db->join('tag_products tp','msqr.tag_id = tp.id');
    // $this->db->join('parent_category pm','pm.id = tp.sub_category_id');
    // $this->db->join('karigar_master km','km.id = po.karigar_id');
    // $this->db->join('weights w','w.id = tp.weight_range_id');
    // $this->db->where('po.status','1');
    // if($limit == true)
    //   $this->db->limit($params['length'],$params['start']);
    // if(!empty($filter_status)){
    //   $this->db->order_by('po.id',$filter_status['dir']);
    // }
    // if(!empty($search)){
    //     $this->db->where("(km.name LIKE '%$search%' OR pm.name LIKE '%$search%' OR tp.product_code LIKE '%$search%' OR tp.issue_voucher LIKE '%$search%')");
    // }

    // $this->db->where('po.status',1);
    // $result = $this->db->get()->result_array();
    // echo '<pre>';
    // print_r($result);
    // echo '</pre>';die;
    // return $result;
  }
   private function get_filter_value($filter_input,$table_col_name){
    $column_name=array();  
    $filter_column_name=filter_column_name($table_col_name);
    $sql='';
    $i=0;
     
     //print_r($filter_input);die;
    foreach ($filter_input as $key => $search_value){
       $column_name=$filter_column_name;
       //print_r($search_value['search']['value']);
        if(!empty($search_value['search']['value'])){
          if($i != 0){
            $sql.=' AND  ';
          }
            $sql.=''.$column_name[$key].' like "%'.$search_value['search']['value'].'%" ';
            $i++;
        

         }   
    }

    if(!empty($sql)){  
      $this->db->where($sql);  
    } 
       
  }
  public function validate(){
    $this->form_validation->set_rules($this->config->item('karigar_receive_order', 'admin_validationrules'));
    $data['status']= 'success';
    if($this->form_validation->run()===FALSE){
      $data['status']= 'failure';
      $data['data']= '';
      $data['error'] = $this->getErrorMsg();
    }
    return $data;
  }
  private function getErrorMsg(){
    return array(
      'kariger_id'=>strip_tags(form_error('kariger_id')),
      'gross_wt'=>strip_tags(form_error('gross_wt')),
      'receive_wt'=>strip_tags(form_error('receive_wt')),
      'few_wt'=>strip_tags(form_error('few_wt')),
      'net_wt'=>strip_tags(form_error('net_wt')),
      'mina_wt'=>strip_tags(form_error('mina_wt')),
      'kundan_wt'=>strip_tags(form_error('kundan_wt')),
      'other_wt'=>strip_tags(form_error('other_wt')),
      'stone_wt'=>strip_tags(form_error('stone_wt')),
      'stone_amt'=>strip_tags(form_error('stone_amt')),
      'kundan_amt'=>strip_tags(form_error('kundan_amt')),
    );
  }
  public function get_details($id){
    $this->db->select('qc.receive_product_id,po.karigar_id as karigar_id,km.name as karigar_name,pm.id,pm.name,tp.product_code,tp.id tp_id,po.quantity,po.id as receive_product_kmm_id,qc.id as rp_id, rp.net_wt,rp.kundan_pc,rp.kundan_wt,rp.kundan_amt,rp.few_wt,qc.department_id,rp.kundan_rate,rp.k_gold,ROUND(rp.k_gold,2)as kundan_pure,rp.total_wt,rp.checker_wt,rp.checker_pcs,rp.checker@ as checker,rp.checker_amt,rp.other_wt,rp.color_stone_rate,rp.mina_wt,rp.wax_wt,rp.color_stone,rp.moti,rp.gross_wt,rp.stone_amt,rp.stone_wt,pm.id as product_id,cm.id as category_id,tp.item_category_code as item_category_code,rp.black_beads_wt');
    $this->db->from("quality_control qc");
    $this->db->join("Receive_products rp",'qc.receive_product_id = rp.id');
    $this->db->join('prepare_kundan_karigar_order po','po.id = rp.kmm_id');
    $this->db->join('make_set_quantity_relation msqr','msqr.id=po.msqr_id');
    $this->db->join('tag_products tp','tp.id=msqr.tag_id');
    $this->db->join('parent_category_codes pcc', 'pcc.code_name = tp.sub_code');
    $this->db->join('parent_category pm', 'pm.id = pcc.parent_category_id');
    $this->db->join('category_master cm', 'cm.code = tp.item_category_code');
    //$this->db->join('parent_category pm','pm.id = tp.sub_category_id');
    $this->db->join('karigar_master km','km.id=po.karigar_id','left');
    $this->db->where("qc.module","3");
    $this->db->where("qc.status","11");
    $this->db->where("po.karigar_id",$id);
    $result = $this->db->get()->result_array();
    //echo $this->db->last_query();print_r($result);die;
    return $result;
  }
  public function get_receive_product_details($rp_id){
    $this->db->select('po.karigar_id as karigar_id,km.name as karigar_name,pm.name,tp.sub_category_id,tp.id as tp_id,rp.quantity,po.id as receive_product_kmm_id,rp.id as rp_id,tp.product_code');
    $this->db->from("Receive_products rp");
    $this->db->join('prepare_kundan_karigar_order po','po.id=rp.kmm_id');
    $this->db->join('make_set_quantity_relation msqr','msqr.id=po.msqr_id');
    $this->db->join('tag_products tp','tp.id=msqr.tag_id');
    $this->db->join('parent_category_codes pcc', 'pcc.code_name = tp.sub_code');
    $this->db->join('parent_category pm', 'pm.id = pcc.parent_category_id');
    //$this->db->join('parent_category pm','pm.id = tp.sub_category_id');
    $this->db->join('karigar_master km','km.id=po.karigar_id','left');
    $this->db->where("rp.id",$rp_id);
    $result = $this->db->get()->row_array();
    return $result;
  }

   private function create_product_code($postdata) {
   // print_r($postdata);
        if(!empty($postdata['parent_category_id'])){
          foreach ($postdata['parent_category_id'] as $key => $value) {
              if ($postdata['product_code'][$key] == '') {
                    unset($postdata['parent_category_id'][$key]);
                }
            }
           }
        $product_code_array = array();
          foreach ($postdata['parent_category_id'] as $key => $value) {
            $res = $this->Customer_order_model->last_product_code($postdata['code'][$key],$postdata['category_id']);
         // print_r($res);
            if (empty($res)) {
                $product_code_array[$postdata['code'][$key]] = '0';
            } else {
                $last_product_code = $res['product_code'];
                $last_product_code = explode('-', $last_product_code);
                $last_product_code = end($last_product_code);
                $product_code_array[$postdata['code'][$key]] = $last_product_code;
            }
        }

        foreach ($postdata['parent_category_id'] as $key => $value) {

            $item_category_code = explode('-', $postdata['product_code'][$key]);
            $item_category_code = reset($item_category_code);
    
            if (isset($product_code_array[$postdata['code'][$key]])) {

                $postdata['product_code'][$key] = $item_category_code . '-' . ($product_code_array[$postdata['code'][$key]] + 1);

                $product_code_array[][$product_code_array[$postdata['product_code'][$key]]]= ($product_code_array[$postdata['code'][$key]] + 1);

            }
//die;
        $postdata = $postdata;
            //print_r($postdata);
        return true;
    }
  }
  public function create_kundan_receipt_code(){
    $code = "MNFREC".mt_rand();
    $postdata = $_POST;
  //print_r($_POST);die;
 //        print_r($data);die;

  if($_POST['action_type'] =='stock'){
        //$this->create_product_code($postdata);        
        $stock_product_code=$this->insert_stock_into_tag_products($postdata);
      }//die;
    foreach ($postdata['rp_id'] as $key => $value) {      
      $data = array(
         'receipt_code' => $code,
         'status' => "4",/*genrate karigar recipt */
         //'kmm_id' => $postdata['kariger_id'][$key],
         'kundan_id' => $postdata['kariger_id'][$key],
         'quantity' => $postdata['quantity'][$key],
         'gross_wt' => $postdata['gross_wt'][$key],
         'net_wt' => $postdata['net_wt'][$key],
         'few_wt' => $postdata['few_wt'][$key],
         'wax_wt' => $postdata['wax_wt'][$key],
         'color_stone' => $postdata['color_stone'][$key],
         'moti' => $postdata['moti'][$key],
         'kundan_pure' => $postdata['kundan_pure'][$key],
         'kundan_pc' => $postdata['kundan_pc'][$key],
         'stone_wt' => $postdata['stone_wt'][$key],
         'mina_wt' => $postdata['mina_wt'][$key],
         'kundan_wt' =>$postdata['kundan_wt'][$key],
         'kundan_rate' =>$postdata['kundan_rate'][$key],
         'other_wt' => $postdata['other_wt'][$key],
         'stone_amt' => $postdata['stone_amt'][$key],
         'kundan_amt' => $postdata['kundan_amt'][$key],         
         'checker_wt' => $postdata['checker_wt'][$key],
         'checker_pcs' => $postdata['checker_pcs'][$key],
         'checker@' => $postdata['checker'][$key],         
         'checker_amt' => $postdata['checker_amt'][$key],
         'product_code' => $postdata['name'][$key],
         'total_wt' => $postdata['total_wt'][$key],
         'black_beads_wt' => $postdata['black_beads_wt'][$key],
         'color_stone_rate' => $postdata['color_stone_rate'][$key],
         'created_at' => date('Y-m-d'),
         'module' => '3',
      );

     //print_r($data);die;
      if($_POST['action_type'] !='stock' || $_POST['action_type'] !='edit' || $_POST['action_type'] !='repair_stock'){
        $this->db->where('id',$postdata['rp_id'][$key]);   
        $this->db->update("quality_control",$data);
        $this->db->where("id",$value);
        $this->db->set("status",'3');
        $this->db->update('prepare_kundan_karigar_order');
      }
     

      $rp_ids[] = $postdata['rp_id'][$key];
    
      $this->Ready_product_model->store_from_kundan($postdata['rp_id'][$key],$key,$code);

      $result = $this->get_receive_product_details($this->db->insert_id());
      $data['name'] = $result['name'];
      $data['quantity'] = $result['quantity'];
      $data['tp_id'] = $result['tp_id'];
      $final_result [] = $data;
    }/*die;*/
    $_SESSION['kundan_receive_products'] = $rp_ids;
    return $final_result;
     
  }
  private function insert_stock_into_tag_products($data){
    //print_r($data);die;

    if(!empty($data['parent_category_id'])){
      foreach ($data['parent_category_id'] as $key => $value) {
        $insert_array =array();
        $sub_category_id=$this->find_name($data['code'][$key]);
        //print_r($category);die;
    
        $insert_array = array(
            'karigar_id'=>$data['kariger_id'],
            'department_id'=>$data['department_id'],
            'sub_code' =>$data['code'][$key],
            'qc_id'=>$this->db->insert_id(),
            'quantity'=>1,
            'product_code'=>$data['product_code'][$key],
            'status'=>5,
            'created_at'=>date('Y-m-d H:i:s'),
            'item_category_code'=>$data['category_id'],
            'sub_category_id'=> $sub_category_id->id,
            );
      

        $category=$this->find_category_name($insert_array['item_category_code'],$insert_array['department_id']);
           insert_max_count($category->id,$insert_array['item_category_code'],$insert_array['sub_code'],$insert_array['department_id'],$category->name);
       $this->db->insert('tag_products',$insert_array);
  
      }
      //die;
       
    }
  }
    private function find_category_name($code,$department_id)
  {
    $this->db->select('id,name,code as item_category_code');
    $this->db->from('category_master');
    $this->db->where('code',$code);
    $this->db->where('department_id',$department_id);
    //echo $this->db->last_query();die;
    $result=$this->db->get()->row();
    return $result; 
  }
  private function find_name($code='')
  {
    $this->db->select('p.id as id');
    $this->db->from('parent_category p');
    $this->db->join('parent_category_codes pc','pc.parent_category_id =p.id');
    $this->db->where('pc.code_name',$code);
    return $this->db->get()->row();
  }

  public function get_engaged_kundan_karigar(){
    $this->db->select('distinct(rp.karigar_id) as karigar_id,km.name as karigar_name');
    $this->db->from("quality_control qc");
    $this->db->join("Receive_products rp","rp.id = qc.receive_product_id");
   // $this->db->join("prepare_kundan_karigar_order pko","pko.id = rp.kmm_id");
    $this->db->join('karigar_master km','km.id=rp.karigar_id');
    $this->db->where("qc.module",'3');
    $this->db->where("qc.status",'11');
    $this->db->group_by("rp.karigar_id");
    $result = $this->db->get()->result_array();
    return $result;
  }
  public function find($id){
    $this->db->select('qc.*,po.karigar_id,km.name km_name,km_prev.name prev_k_name,drp.product_code,tp.image,drp.custom_net_wt,drp.custom_gr_wt,drp.custom_stone_wt,drp.custom_checker_wt,drp.custom_kundan_pure,drp.custom_kun_wt,drp.custom_black_beads_wt,tp.barcode_path,drp.product as sub_category ,rp.kundan_pc');
    $this->db->from('quality_control qc');
    $this->db->join('Receive_products rp',"rp.id=qc.receive_product_id");
    $this->db->join('prepare_kundan_karigar_order po','po.id=rp.kmm_id');
    $this->db->join('make_set_quantity_relation msqr','msqr.id=po.msqr_id');
    $this->db->join('tag_products tp','tp.id=msqr.tag_id');
    $this->db->join('department_ready_product drp','drp.product_code=tp.product_code');
    $this->db->join('parent_category_codes pcc', 'pcc.code_name = tp.sub_code');
    $this->db->join('parent_category pm', 'pm.id = pcc.parent_category_id');
    //$this->db->join('parent_category pm','pm.id = tp.sub_category_id');
    $this->db->join('karigar_master km','km.id=po.karigar_id','left');
    $this->db->join('karigar_master km_prev','km_prev.id=tp.karigar_id','left');
    $this->db->where("qc.id",$id);
    return $this->db->get()->row_array(); 
  }
  public function find_by_rp_id($id){
    $this->db->select('rp.*,po.karigar_id,km.name km_name,km_prev.name prev_k_name,tp.product_code,tp.image,tp.net_wt,km.id as kundan_id');
    $this->db->from('Receive_products rp');
    $this->db->join('prepare_kundan_karigar_order po','po.id=rp.kmm_id');
    $this->db->join('make_set_quantity_relation msqr','msqr.id=po.msqr_id');
    $this->db->join('tag_products tp','tp.id=msqr.tag_id');
    $this->db->join('parent_category_codes pcc', 'pcc.code_name = tp.sub_code');
    $this->db->join('parent_category pm', 'pm.id = pcc.parent_category_id');
    //$this->db->join('parent_category pm','pm.id = tp.sub_category_id');
    $this->db->join('karigar_master km','km.id=po.karigar_id','left');
    $this->db->join('karigar_master km_prev','km_prev.id=tp.karigar_id','left');
    $this->db->where("rp.id",$id);
    return $this->db->get()->row_array(); 
  }
}
