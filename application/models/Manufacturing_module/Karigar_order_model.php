<?php

class Karigar_order_model extends CI_Model {

  function __construct() {
  	$this->table_name = "Karigar_manufacturing_mapping";
    $this->table_name1 = "karigar_master";
    parent::__construct();
  }
  public function validate_quantity(){
    $this->form_validation->set_rules($this->config->item('receive_order', 'admin_validationrules'));
    $data['status']= 'success';
    if($this->form_validation->run()===FALSE){
      $data['status']= 'failure';
      $data['data']= '';
      $data['error'] = $this->getErrorMsg();
    }
    return $data;
  }
  private function getErrorMsg(){
    return array(
      'kmm_quantity'=>strip_tags(form_error('kmm_quantity'))
    );
  }
  public function validate_postdata(){
    $data['status'] = 'success';
    $postdata = $_POST;
    $error = array();
    $this->form_validation->set_rules('karigar_id', 'Karigar Name', 'required');
    if($postdata['dep_id'] != '2'  && $postdata['dep_id'] !='3' && $postdata['dep_id']!='6' && $postdata['dep_id'] !='10'){
      $this->form_validation->set_message('qnt_validation','Quantity Exceeded');
    }else{
      $this->form_validation->set_message('wt_validation','weight Exceeded');
    }
    //print_r($postdata);die;
    if(!empty($postdata['mop_id'])){
      foreach ($postdata['mop_id'] as $key => $value) {
        
        $_POST['mop_id'] = $value;
         if($postdata['department_id'][$key] != '2' && $postdata['department_id'][$key] !='3' && $postdata['department_id'][$key]!='6' && $postdata['department_id'][$key] !='10'){
        $_POST['quantity'] = $postdata['quantity'][$key];
            $this->form_validation->set_rules('quantity', 'Quantity',
              array('required','numeric','greater_than[0]',array('qnt_validation',
                function($str){
                  $this->db->select('mop.quantity,sum(kmm.quantity) as kmm_qnt');
                  $this->db->from('manufacturing_order_mapping mop');
                  $this->db->join('Karigar_manufacturing_mapping kmm','kmm.mop_id = mop.id','left');
                  $this->db->where('mop.id',$_POST['mop_id']);
                  $result = $this->db->get()->row_array();
                  if(($result['kmm_qnt'] + $_POST['quantity']) > $result['quantity']){
                    return false;
                  }else{
                    return true;
                  }
                })
              ));
          }else{
            $_POST['weight'] = $postdata['weight'][$key];
            $this->form_validation->set_rules('weight', 'Weight',
              array('required','numeric','greater_than_equal_to[0.1]',array('wt_validation',
                function($str){
                  $this->db->select('mop.weight,sum(kmm.total_weight) as kmm_wt');
                  $this->db->from('manufacturing_order_mapping mop');
                  $this->db->join('Karigar_manufacturing_mapping kmm','kmm.mop_id = mop.id','left');
                  $this->db->where('mop.id',$_POST['mop_id']);
                  $result = $this->db->get()->row_array();
                  if(($result['kmm_wt'] + $_POST['weight']) > $result['weight']){
                    return false;
                  }else{
                    return true;
                  }
                })
              ));
          }
        
        if($this->form_validation->run()===FALSE){
          $error['karigar_id']=strip_tags(form_error('karigar_id'));
          $error['quantity_'.$key]=strip_tags(form_error('quantity'));
          $error['weight_'.$key]=strip_tags(form_error('weight'));
          $data['status']= 'failure';
          $data['data']= '';
          $data['error'] =$error;
        }
      }
    }else{/*on empty*/
      $data =get_errorMsg();
    }

    return $data;
  }
  public function get_order_wise_list($filter_status='',$status='',$params='',$search='',$limit='',$status='',$department_id=''){
    $this->db->select('mo.id order_id,mo.order_name name,mo.order_date');
    $this->db->from('Karigar_manufacturing_mapping kmm');
    $this->db->join('manufacturing_order_mapping mom','mom.id=kmm.mop_id');
    $this->db->join('manufacturing_order mo','mo.id=mom.manufacturing_order_id');
    $this->db->join('parent_category pm','pm.id = mom.parent_category_id');
    $this->db->join('weights w','w.id = mom.weight_range_id');
    $this->db->join('karigar_master km','km.id = kmm.karigar_id');
    $this->db->join('Receive_products rp','rp.kmm_id = kmm.id','left');
    if(!empty($department_id)){
      $this->db->where('kmm.department_id',$department_id);
    }
    if(!empty($status)){
      $this->db->where('kmm.status',$status);
    }else{
      $this->db->where('kmm.status','0');
    }
    $this->db->group_by('mo.id');
     /*if(!empty($search)){
       $this->db->where("(mo.id LIKE '%$search%' OR mo.order_name LIKE '%$search%')");
      }*/
    if(isset($params['columns']) && !empty($params['columns'])){
      $filter_input=$params['columns'];
      $table_col_name='received_orders_by_order_id';
      $this->get_filter_value($filter_input,$table_col_name);
    }   


    if(!empty($params) &&  $limit == true){
      $this->db->order_by('kmm.id','DESC');
      $this->db->limit($params['length'],$params['start']);
      $result = $this->db->get()->result_array();
    }else{
      $row_array = $this->db->get()->result_array();
      $result = count($row_array);
      //echo $this->db->last_query();print_r($result);exit;
    }

    // print_r($this->db->last_query());die;
    return $result;
  }
  public function get($filter_status='',$status='',$params='',$search='',$limit='',$status='',$order_id='',$department_id=''){
    $this->db->select('kmm.id,mo.id order_id, DATE_FORMAT(kmm.created_at, "%d-%m-%Y '."||".' %T ") as order_date,DATE_FORMAT(kmm.delivery_date, "%d-%m-%Y") as delivery_date,pm.name sub_cat_name,w.from_weight,w.to_weight,kmm.quantity, kmm.approx_weight,km.name karigar_name,IF(rp.module = 2,sum(rp.quantity),0) rp_quantity,mom.product_code,kmm.total_weight as total_weight,mom.weight,kmm.department_id,kmm.receive_weight');
    $this->db->from('Karigar_manufacturing_mapping kmm');
    $this->db->join('manufacturing_order_mapping mom','mom.id=kmm.mop_id');
    $this->db->join('manufacturing_order mo','mo.id=mom.manufacturing_order_id');
    $this->db->join('parent_category pm','pm.id = mom.parent_category_id');
    $this->db->join('weights w','w.id = mom.weight_range_id');
    $this->db->join('karigar_master km','km.id = kmm.karigar_id');
    $this->db->join('Receive_products rp','rp.kmm_id = kmm.id','left');
    if(!empty($order_id)){
      $this->db->where('mo.id',$order_id);
    }
    if(!empty($department_id)){
      $this->db->where('kmm.department_id',$department_id);
    }
    if(!empty($status)){
      $this->db->where('kmm.status',$status);
    }else{
      $this->db->where('kmm.status','0');
    }
     /*if(!empty($search)){
       $this->db->where("(mo.id LIKE '%$search%')");
      }*/
    $ci = &get_instance();
    $get_url = $ci->uri->segment(1);
    if(@$ci->uri->segment(2) =='view'){
       $table_col_name='received_orders';
    }else if(@$get_url =='Order_sent'){
       $table_col_name='order_sent';
    }else{
       $table_col_name='Karigar_order';
    }  

    if(isset($params['columns']) && !empty($params['columns'])){
      $filter_input=$params['columns'];
      //$table_col_name="Karigar_order";
      $this->get_filter_value($filter_input,$table_col_name);
    }  

    $this->db->group_by('kmm.id');
    if(!empty($params) && $limit == true){
      $this->db->order_by('kmm.id','DESC');
      $this->db->limit($params['length'],$params['start']);
      $result = $this->db->get()->result_array();
      //echo $this->db->last_query();print_r($result);exit;
    }else{
      $row_array = $this->db->get()->result_array();
      $result = count($row_array);
      //echo $this->db->last_query();print_r($row_array);exit;
    }
    //print_r($this->db->last_query());die;
    return $result;

  }
  private function get_filter_value($filter_input,$table_col_name){
    $column_name=array();  
    $filter_column_name=filter_column_name($table_col_name);
    $sql='';
    $i=0;
     
     //print_r($filter_input);die;
    foreach ($filter_input as $key => $search_value){
       $column_name=$filter_column_name;
       //print_r($search_value['search']['value']);
        if(!empty($search_value['search']['value'])){
          if($i != 0){
            $sql.=' AND  ';
          }
            $sql.=''.$column_name[$key].' like "%'.$search_value['search']['value'].'%" ';
            $i++;
        

         }   
    }

    if(!empty($sql)){  
      $this->db->where($sql);  
    } 
       
  }

  public function get_engaged_karigar($department_id)
  {
      $this->db->select('count(DISTINCT(kmm.karigar_id)) as count');
      //$this->db->select('kmm.id,mo.id order_id,mo.order_date,scm.name sub_cat_name,w.from_weight,w.to_weight,kmm.quantity, kmm.approx_weight,km.name karigar_name,sum(rp.quantity) rp_quantity');s
      $this->db->from('Karigar_manufacturing_mapping kmm');
      $this->db->join('karigar_master km','km.id = kmm.karigar_id');
      $this->db->where('kmm.status','1');
      if(!empty($department_id)){
      $this->db->where('kmm.department_id',$department_id);
      }

     
      $result = $this->db->get()->row_array();
    /*  echo $this->db->last_query();
      die;*/
      return $result['count'];
  }

  public function find($id){
  	$this->db->select('kmm.id,mom.id AS mom_id,mo.id order_id,DATE_FORMAT(kmm.created_at, "%d-%m-%Y") as assign_date,DATE_FORMAT(kmm.delivery_date, "%d-%m-%Y") as delivery_date,mo.order_date,pm.name sub_cat_name,w.from_weight,w.to_weight,kmm.quantity,kmm.approx_weight,km.name karigar_name,mom.product_code,km.id karigar_id,sum(kmm.total_weight) as total_weight,kmm.department_id');
  	$this->db->from('Karigar_manufacturing_mapping kmm');
  	$this->db->join('manufacturing_order_mapping mom','mom.id=kmm.mop_id','left');
  	$this->db->join('manufacturing_order mo','mo.id=mom.manufacturing_order_id','left');
  	// $this->db->join('sub_category_master scm','scm.id = mom.sub_category_id');
    $this->db->join('parent_category pm','pm.id = mom.parent_category_id','left');
    $this->db->join('weights w','w.id = mom.weight_range_id','left');
    $this->db->join('karigar_master km','km.id = kmm.karigar_id','left');
  	$this->db->where('kmm.id',$id);
   
  	$result = $this->db->get()->row_array();
    $result['view_delivery_date'] = date('d-m-Y', strtotime("+12 day"));
  	return $result;
  }
  public function update($where,$update){
    $this->db->where($where);
    $this->db->update('Karigar_manufacturing_mapping',$update);
  }
  public function store($insert_array){
  	$result = $this->find($insert_array['kmm_id']);
  	$insert_array['karigar_id'] = $result['karigar_id'];
    if($this->db->insert('Receive_products',$insert_array)){
      $status = $this->check_quanity($insert_array['kmm_id']);
      if($status == true){
        $this->update(array('id'=>$insert_array['kmm_id']),array('status'=>'2'));
      }
      return get_successMsg();
    }else{
      return get_errorMsg();
    }
  }
  private function check_quanity($kmm_id){
    $result = $this->find($kmm_id);
    $this->db->select('sum(quantity) quantity');
    $this->db->from('Receive_products');
    $this->db->where('kmm_id',$kmm_id);
    $rp_quantity = $this->db->get()->row_array();
    if($result['quantity'] == $rp_quantity['quantity']){
      return true;
    }else{
      return false;
    }
  }
  function view_karigar_details($karigar_id){
    $this->db->select('kmm.approx_weight,kmm.delivery_date,mop.manufacturing_order_id as order_id,mop.product_code,sum(rp.quantity) as receive_qty,sum(kmm.quantity) as quantity,kmm.created_at karigar_engaged_date');
    $this->db->from('Karigar_manufacturing_mapping kmm');
    $this->db->join('manufacturing_order_mapping mop','mop.id=kmm.mop_id');
    $this->db->join('Receive_products rp','rp.kmm_id=kmm.id','left');
    $this->db->where('kmm.karigar_id',$karigar_id);
    $this->db->where('kmm.status >= 1');
    $this->db->group_by('mop.id');
    $result = $this->db->get()->result_array();
    return $result;
  }

   //delete subcategory from department order
  public function delete($id){
    $this->db->where('id',$id);
    if($this->db->delete('Karigar_manufacturing_mapping')){

      return get_successMsg();
    }else{
      return get_errorMsg();
    }
  }
  public function get_department_id($kmm_id){
    $this->db->select('kmm.department_id');
    $this->db->from('Karigar_manufacturing_mapping kmm');
    $this->db->where('kmm.id',$kmm_id);
    $result = $this->db->get()->row_array();
    return $result;
  }

    // public function get_karigar_name(){
    //     $this->db->select('name');
    //     $result = $this->db->get($this->table_name1)->result_array();
    //     return array_column($result, 'name');
    // } 

    // public function update_order_quantity($order_details)
    // {

    //   $this->db->insert('Karigar_manufacturing_mapping', $order_details);
    // }
}