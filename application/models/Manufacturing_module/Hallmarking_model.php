<?php
class Hallmarking_model extends CI_Model {

  function __construct() {
    $this->table_name = "quality_control_hallmarking";
    parent::__construct();
  }
  public function store($insert_array){
/*  print_r($insert_array);
  return $insert_array;die;*/
    if(!empty($insert_array ['weight'])){
      $original_quantity = $insert_array['original_quantity'];
      $manufacturing_qch_id = $insert_array['manufacturing_qch_id'];
      unset($insert_array['manufacturing_qch_id']);
      $qc_id =  $insert_array['qc_id'];
      unset($insert_array['qc_id']);
      // unset($insert_array['si_detail_id']);
      // unset($insert_array['drp_id']);
      // unset($insert_array['hallmarking_center_id']);
      // unset($insert_array['person_name']);
      // unset($insert_array['sid_id']);
      // unset($insert_array['remaining_qty']);
      // unset($insert_array['HM_send_date']);
      unset($insert_array['original_quantity']);
      // $insert_array['HM_receive_date'] = date('Y-m-d H:i:s');
      //$this->db->insert('quality_control',$insert_array);
      if($original_quantity == $insert_array['quantity']){
        $qty = $this->get_sum_count($manufacturing_qch_id,$manufacturing_qch_id);
        $this->db->insert($this->table_name,$insert_array);
        $this->db->set('remaining_qty',($insert_array['quantity'] + $qty));
        $this->db->set('status','8');
        $this->db->where('id',$manufacturing_qch_id);
        $this->db->update($this->table_name);
        $this->db->where('id',$qc_id);
        $this->db->update('quality_control',array('status'=>'8'));
        $this->Quality_control_model->insert_into_tags($insert_array,$qc_id);

      }else{
        $qty = $this->get_sum_count($manufacturing_qch_id,$manufacturing_qch_id);
        $this->db->insert($this->table_name,$insert_array);
        $this->db->set('remaining_qty',($insert_array['quantity'] + $qty));
        $this->db->where('id',$manufacturing_qch_id);
        $this->db->update($this->table_name);
        
      }
          $data['status']= 'success';
           return $data;
    }
  }
  private function get_sum_count($manufacturing_qch_id,$qc_id){
    $this->db->select('sum(remaining_qty) qty');
    $this->db->from($this->table_name);
    // $this->db->where('qc_id',$qc_id);
    // $this->db->where('id !=',$manufacturing_qch_id);
    $this->db->where('id',$manufacturing_qch_id);
    $result = $this->db->get()->row_array();
    return $result['qty'];
  }
  private function get_error_status($get_total_quantity,$get_qc_quantity,$insert_array){
  	if($get_qc_quantity['quantity'] < ($get_total_quantity + $insert_array['quantity'])){
  		$data['status']= 'failure';
    	$data['data']= '';
    	$data['error'] = array('quantity'=>'Quantity Exceeded');
    	echo json_encode($data);die;
  	}elseif ($insert_array['quantity'] == 0) {
  		$data['status']= 'failure';
    	$data['data']= '';
    	$data['error'] = array('quantity'=>'Quantity should be greater than zero');
    	echo json_encode($data);die;
  	}
  	return true;
  }
  private function get_previous_qch_quanity($postdata){
  	$this->db->select('sum(quantity) quantity');
  	$this->db->from($this->table_name);
  	$this->db->where('qc_id',$postdata['qc_id']);
  	$result = $this->db->get()->row_array();
  	return $result['quantity'];
  }
  public function get($filter_status='',$status='',$params='',$search='',$limit='',$department_id=''){
  	$this->db->select('qch.id,qch.created_at HM_send_date,mo.order_name,pm.name sub_cat_name,rp.receipt_code,qch.quantity,km.name km_name,w.from_weight,w.to_weight,qch.remaining_qty,qc.hm_net_wt,qc.weight as hm_gr_wt');
    $this->db->from($this->table_name.' qch');
    $this->db->join('quality_control qc','qc.id=qch.qc_id');
    $this->db->join('Receive_products rp','rp.receipt_code=qch.receipt_code');
    $this->db->join('Karigar_manufacturing_mapping kmm','rp.kmm_id=kmm.id');
    $this->db->join('manufacturing_order_mapping mom','mom.id=kmm.mop_id');
    $this->db->join('manufacturing_order mo','mo.id=mom.manufacturing_order_id');
    $this->db->join('karigar_master km','km.id=kmm.karigar_id','left');
    $this->db->join('weights w','w.id = mom.weight_range_id');
    $this->db->join('parent_category pm','pm.id = mom.parent_category_id');
    $this->db->where('qch.module','2');
    $this->db->where('qch.status','7');/*added for removing tag products*/
    //$this->db->having('qc_quantity > 0');
    //$this->where_cond();
    if(!empty($department_id)){
      $this->db->where('qch.department_id',$department_id);
    }
    if(!empty($id))
      $this->db->where('qch.id' , $id);
   //$this->all_like_queries($search);
    if(isset($params['columns']) && !empty($params['columns'])){
      $filter_input=$params['columns'];
      $table_col_name="manufacturing_hallmarking";
      $this->get_filter_value($filter_input,$table_col_name);
    }

    // if (!empty($search)) {
    //   $this->db->like('mo.order_name',$search);
    // }
 
    $this->db->group_by('qch.id');


    if($limit == true){
      $this->db->order_by('qch.id','DESC');
      $this->db->limit($params['length'],$params['start']);
      $result = $this->db->get()->result_array();
    }else{
      $row_array = $this->db->get()->result_array();
      $result = count($row_array);
      //echo $this->db->last_query();print_r($result);exit;
    }

    // print_r($result);
     // echo $this->db->last_query();die;

    return $result;
  }
  private function get_filter_value($filter_input,$table_col_name){
    $column_name=array();  
    $filter_column_name=filter_column_name($table_col_name);
    $sql='';
    $i=0;
     
     //print_r($filter_input);die;
    foreach ($filter_input as $key => $search_value){
       $column_name=$filter_column_name;
       //print_r($search_value['search']['value']);
        if(!empty($search_value['search']['value'])){
          if($i != 0){
            $sql.=' AND  ';
          }
            $sql.=''.$column_name[$key].' like "%'.$search_value['search']['value'].'%" ';
            $i++;
        

         }   
    }

    if(!empty($sql)){  
      $this->db->where($sql);  
    } 
       
  }
  private function where_cond(){
  	if(!empty($_GET['status']) && $_GET['status'] == 'receive')
  		$this->db->where('qch.status','7');
  	elseif (!empty($_GET['status']) && $_GET['status'] == 'rejected') {
  		$this->db->where('qch.status','0');
  	}
  }
  private function all_like_queries($search){
    $this->db->where("(mo.order_name LIKE '%$search%' OR pm.name LIKE '%$search%' OR km.name LIKE '%$search%')");
  }
  public function insert_in_prepared_order($result,$karigar_id){
    $insert_array = array(
        'corporate_products_id'=>$result['corporate_product_id'],
        'quantity'=>$result['quantity'],
        'karigar_id'=>$karigar_id,
        'hm_rejected_id'=>$result['id'],
        'status'=>'0',
        'created_at'=>date('Y-m-d H:i:sa'),
        'updated_at'=>date('Y-m-d H:i:sa'),
      );
    if($this->db->insert('Prepare_product_list',$insert_array)){
      return true;
    }else{
      return false;
    }
  }
  public function update($array,$where){
    $this->db->where($where);
    $this->db->update($this->table_name,$array);
  }
  public function find($id){
    $this->db->where('id',$id);
    return $this->db->get($this->table_name)->row_array();
  }
  public function export_hallmarking_products($filter_status='',$status='',$params='',$search='',$limit='',$hallmarking =''){
    $this->db->select('"" Vendor_Code,cp.work_order_id as Order_No, "" Link_No,cp.CW_quantity as PCS,qc.total_gr_wt as Gross_Wt, DATE_FORMAT(qlt.HM_send_date,  "%d-%m-%Y") as HM_Send_date,DATE_FORMAT( qc.HM_receive_date ,  "%d-%m-%Y" ) as HM_Receive_Date,"" Delivery_Date,"" Assay_No, "" XRF_No, cp.Item_number as Item_Id,cm.name as Config_Id, cp.CW_unit as Unit_Id,"" HM_Code,cp.id cp_id');
    $this->db->from($this->table_name.' qc');
    $this->db->join('corporate_products cp','cp.id=qc.corporate_product_id','left');
    $this->db->join('Receive_products rp','rp.corporate_product_id=qc.corporate_product_id','left');
    $this->db->join('quality_control qlt','qlt.id=qc.qc_id','left');
    $this->db->join('product p','p.product_code = cp.sort_Item_number','left');
    $this->db->join('carat_master cm','cm.id = p.carat_id','left');
    $this->db->join('email_orders eo','eo.order_id = cp.order_id');
    $this->db->join('corporate corp','corp.id = eo.corporate');
    if(!empty($_POST['search']))
      $search = $_POST['search'];
    $this->all_like_queries($search);
    $this->db->where('qc.status','1');
    if(!empty($_POST['product_code']))
      $this->export_where_conditions();
    $result = $this->db->get()->result_array();
    //echo $this->db->last_query(); die();
    return $result;

  }
  
  public function get_order_data($qc_id)
  {
    $this->db->select('mop.manufacturing_order_id,s_cat.name');
    $this->db->from('quality_control qc');
    $this->db->join('Receive_products rp','rp.id = qc.receive_product_id');
    $this->db->join('Karigar_manufacturing_mapping kmm','kmm.id = rp.kmm_id');
    $this->db->join('manufacturing_order_mapping mop','mop.id = kmm.mop_id');
    $this->db->join('sub_category_master s_cat','s_cat.id = mop.sub_category_id');
    $this->db->where("qc.id",$qc_id);
    $result = $this->db->get()->row_array();
    return $result;
  }

  public function hallmarking_qc($insert_data){
    if($this->db->insert_batch($this->table_name,$insert_data)){
      return 'success';
    }else{
      return 'error';
    }
  }
  public function get_all_qc_quantity($qc_id){
    $this->db->select('sum(quantity) qnt');
    $this->db->from('quality_control');
    $this->db->where('manufacturing_qch_id',$qc_id);
    $result = $this->db->get()->row_array();
    return $result['qnt'];
  }
} 