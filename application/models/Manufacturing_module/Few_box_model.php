<?php

class Few_box_model extends CI_Model {

  function __construct() {
  	parent::__construct();
    $this->table_name = "repair_product";
     $this->table_name1 = "few_weight";
  }
  
 
  public function get($filter_status='',$status='',$params='',$search='',$limit='',$department_id=''){    
    $this->db->select('*');
    $this->db->from('few_weight');    

    if(isset($params['columns']) && !empty($params['columns'])){
      $filter_input=$params['columns'];
      $this->get_filter_value($filter_input,'few_box');
    }
    if($limit == true){
      $this->db->order_by('id','DESC');
      $this->db->limit($params['length'],$params['start']);
      $result = $this->db->get()->result_array();
    }else{
      $row_array = $this->db->get()->result_array();
      $result = count($row_array);
      //echo $this->db->last_query();print_r($result);exit;
    }
    //echo $this->db->last_query();print_r($result);exit;
    return $result;
  }

  public function get_product(){    
    $this->db->select('id,name,few_box');
    $this->db->from('parent_category');    
    $this->db->where('few_box','1');  
    $this->db->order_by('id','DESC');
    $result = $this->db->get()->result_array();
    return $result;
  }

  public function validatepostdata(){
   if(empty($_POST['few_box']['id'])){
    $this->form_validation->set_rules($this->config->item('few_box', 'admin_validationrules'));
   }else{
    $this->form_validation->set_rules($this->config->item('few_box_update', 'admin_validationrules'));
   }
    //print_r($this->form_validation->run());
     if ($this->form_validation->run() == FALSE) {        
          return FALSE;
      } else {
          return TRUE;
      }
  }
  public function store(){
    $postdata = $this->input->post('few_box');
    $product_name=$this->parent_category_model->find($postdata['product']);
    $insert_array = array(
      'product_id' => $postdata['product'],
      'product_name' => $product_name['name'],      
      'weight' => $postdata['few_weight'],
      'updated_at' => date('Y-m-d H:i:s'),
      'created_at' => date('Y-m-d H:i:s'),     
      );
    if($this->db->insert($this->table_name1,$insert_array)){
      return get_successMsg();
    }else{
      return get_errorMsg();
    }
  }

  public function update(){
    $postdata = $this->input->post('few_box');
    //$product_name=$this->parent_category_model->find($postdata['product']);
    $update_array = array(
      /*'product_id' => $postdata['product'],
      'product_name' => $product_name['name'],*/      
      'weight' => $postdata['few_weight'],
      'updated_at' => date('Y-m-d H:i:s'),
      'created_at' => date('Y-m-d H:i:s'),
      );
    $this->db->where('id', $postdata['id']);
    if($this->db->update($this->table_name1,$update_array)){
      return get_successMsg();
    }else{
      return get_errorMsg();
    }
  }

  public function delete($id){
    $this->db->where('id',$id);
    if($this->db->delete($this->table_name1)){
      return get_successMsg();
    }else{
      return get_errorMsg();
    }
  }
  private function get_filter_value($filter_input,$table_col_name){
    $column_name=array();  
    $filter_column_name=filter_column_name($table_col_name);
    $sql='';
    $i=0;     
     //print_r($filter_input);die;
    foreach ($filter_input as $key => $search_value){
       $column_name=$filter_column_name;
       //print_r($search_value['search']['value']);
        if(!empty($search_value['search']['value'])){
          if($i != 0){
            $sql.=' AND  ';
          }
            $sql.=''.$column_name[$key].' like "%'.$search_value['search']['value'].'%" ';
            $i++;       

         }   
    }

    if(!empty($sql)){  
      $this->db->where($sql);  
    } 
       
  }


  public function few_weight_details($rp_id){
    $this->db->select('quantity,net_wt,gross_wt as gr_wt,few_wt,Product_code,Product_name,rp.department_id,mo.order_name ');
    $this->db->from('Receive_products rp');
    $this->db->join('manufacturing_order mo','rp.order_id =mo.id');
    $this->db->where('Product_name',$rp_id);
    $result = $this->db->get()->result_array();
    //echo $this->db->last_query();
    return $result;
  }
    public function find($id){
    $this->db->where('id',$id);
    $result = $this->db->get($this->table_name1)->row_array();
    return $result;
  }

}//class
?>