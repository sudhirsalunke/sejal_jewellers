<?php

class Received_orders_model extends CI_Model {

  function __construct() {
    $this->table_name = "quality_control";
    $this->table_name1 = "generate_packing_list";
    parent::__construct();
  }
  public function get($filter_status='',$status='',$params='',$search='',$limit='',$hallmarking ='',$id=''){

    $this->db->select('rp.id,rp.quantity  ro_quantity,mo.id order_id,mo.order_date,km.name karigar_name,w.from_weight,w.to_weight,pm.name sub_cat_name,mom.product_code');
    $this->db->from('Receive_products rp');
    $this->db->join('Karigar_manufacturing_mapping kmm','rp.kmm_id=kmm.id');
    $this->db->join('manufacturing_order_mapping mom','mom.id=kmm.mop_id');
    $this->db->join('manufacturing_order mo','mo.id=mom.manufacturing_order_id');
    //$this->db->join('sub_category_master scm','scm.id = mom.sub_category_id');
    $this->db->join('parent_category pm','pm.id = mom.parent_category_id');
    $this->db->join('karigar_master km','km.id = kmm.karigar_id');
    $this->db->join('weights w','w.id = mom.weight_range_id');
    $this->db->where('rp.status','4');
    $this->db->where('rp.module','2');
    // $this->db->where('rp.receipt_code is NULL', NULL, FALSE);
    $this->db->having('ro_quantity > 0');
    if(!empty($id))
      $this->db->where('rp.id',$id);
    if(!empty($search))
      $this->all_like_queries($search);
    if($limit == true)
      $this->db->limit($params['length'],$params['start']);
    $this->db->group_by('rp.id');
    if(!empty($filter_status)){
      $this->db->order_by('rp.id',$filter_status['dir']);
    }
        $this->db->order_by('rp.id','DESC');
    $result = $this->db->get()->result_array();
    // echo $this->db->last_query();die;
    return $result;
  }

  public function get_receipt_order($filter_status='',$status='',$params='',$search='',$limit='',$hallmarking ='',$id='',$department_id=''){
    
   // $this->db->select('rp.receipt_code,sum(rp.quantity) as quantity,mo.id order_id,mo.order_date,km.name karigar_name,pm.name,w.from_weight,w.to_weight,rp.id rp_id,sum(qc.quantity) as qc_quantity'); /*old*/
    $this->db->select('rp.receipt_code,DATE_FORMAT(rp.date,"%d-%m-%Y") AS receipt_date,km.code km_code,(select SUM(quantity) from Receive_products where receipt_code = rp.receipt_code and status="4" and is_skip_qc="0") as quantity,mo.id order_id,mo.order_date,km.name karigar_name,pm.name,w.from_weight,w.to_weight,rp.id rp_id,sum(qc.quantity) as qc_quantity,sum(qc.weight) as qc_weight,(select SUM(weight) from Receive_products where receipt_code = rp.receipt_code and status="4" and is_skip_qc="0") as weight,rp.department_id,(select SUM(net_wt) from Receive_products where receipt_code = rp.receipt_code and status="4" and is_skip_qc="0") as net_wt ,rp.gross_wt');
    $this->db->from('Receive_products rp');
    $this->db->join('quality_control qc','qc.receive_product_id=rp.id','left');
    // $this->db->join('quality_control_hallmarking qch','qch.qc_id=qc.id','left');
    $this->db->join('Karigar_manufacturing_mapping kmm','rp.kmm_id=kmm.id');
    $this->db->join('manufacturing_order_mapping mom','mom.id=kmm.mop_id');
    $this->db->join('manufacturing_order mo','mo.id=mom.manufacturing_order_id');
    $this->db->join('parent_category pm','pm.id = mom.parent_category_id');
    $this->db->join('karigar_master km','km.id = kmm.karigar_id','left');
    $this->db->join('weights w','w.id = mom.weight_range_id','left');
    $this->db->where('rp.status','4');
    $this->db->where('rp.module','2');
    $this->db->where('rp.is_skip_qc','0');
    if(!empty($department_id)){
      $this->db->where('rp.department_id',$department_id);
    }
    if(!empty($id)){
      $this->db->where('rp.receipt_code',$id);
    }
/*    if(!empty($search))
      $this->all_like_queries($search);*/

    if(isset($params['columns']) && !empty($params['columns'])){
      $filter_input=$params['columns'];
      $table_col_name="received_receipt";
      $this->get_filter_value($filter_input,$table_col_name);
    }

    if(!empty($filter_status)){
      $this->db->order_by('rp.created_at',$filter_status['dir']);
    }
    $this->db->where('rp.receipt_code is NOT NULL', NULL, FALSE);
    $this->db->group_by('rp.receipt_code');

    if($limit == true){
      $this->db->order_by('rp.id','DESC');
      $this->db->limit($params['length'],$params['start']);
      $result = $this->db->get()->result_array();
    }else{
      $row_array = $this->db->get()->result_array();
      $result = count($row_array);
      //echo $this->db->last_query();print_r($result);exit;
    }
/*echo $department_id;
 print_r($result);echo $this->db->last_query();exit;*/
    return $result;
  }
  public function get_sending_to_qc($filter_status='',$status='',$params='',$search='',$limit='',$hallmarking ='',$id='',$department_id=''){
    
  $this->db->select('rp.receipt_code,DATE_FORMAT(rp.date,"%d-%m-%Y") AS receipt_date,km.code km_code,(select SUM(quantity) from Receive_products where receipt_code = rp.receipt_code and status="7" and is_skip_qc="0") as quantity,mo.id order_id,mo.order_date,km.name karigar_name,pm.name,w.from_weight,w.to_weight,rp.id rp_id,sum(qc.quantity) as qc_quantity,sum(qc.weight) as qc_weight,(select SUM(weight) from Receive_products where receipt_code = rp.receipt_code and status="7" and is_skip_qc="0") as weight,rp.department_id,(select SUM(net_wt) from Receive_products where receipt_code = rp.receipt_code and status="7" and is_skip_qc="0") as net_wt ,rp.gross_wt');
    $this->db->from('Receive_products rp');
    $this->db->join('quality_control qc','qc.receive_product_id=rp.id','left');
    $this->db->join('Karigar_manufacturing_mapping kmm','rp.kmm_id=kmm.id');
    $this->db->join('manufacturing_order_mapping mom','mom.id=kmm.mop_id');
    $this->db->join('manufacturing_order mo','mo.id=mom.manufacturing_order_id');
    $this->db->join('parent_category pm','pm.id = mom.parent_category_id');
    $this->db->join('karigar_master km','km.id = kmm.karigar_id','left');
    $this->db->join('weights w','w.id = mom.weight_range_id','left');
    $this->db->where('rp.status','7');
    $this->db->where('rp.module','2');
    $this->db->where('rp.is_skip_qc','0');
    if(!empty($department_id)){
      $this->db->where('rp.department_id',$department_id);
    }
    if(!empty($id)){
      $this->db->where('rp.receipt_code',$id);
    }

    if(isset($params['columns']) && !empty($params['columns'])){
      $filter_input=$params['columns'];
      $table_col_name="received_receipt";
      $this->get_filter_value($filter_input,$table_col_name);
    }

    if(!empty($filter_status)){
      $this->db->order_by('rp.created_at',$filter_status['dir']);
    }
    $this->db->where('rp.receipt_code is NOT NULL', NULL, FALSE);
    $this->db->group_by('rp.receipt_code');

    if($limit == true){
      $this->db->order_by('rp.id','DESC');
      $this->db->limit($params['length'],$params['start']);
      $result = $this->db->get()->result_array();
    }else{
      $row_array = $this->db->get()->result_array();
      $result = count($row_array);
      //echo $this->db->last_query();print_r($result);exit;
    }
    return $result;
  }

  public function get_by_receipt_code($code){
    $this->db->select('rp.*,sum(qc.quantity) as qc_quantity, w.from_weight, w.to_weight, mo.order_name, km.name km_name,pm.name,(select SUM(quantity) from Receive_products where receipt_code= rp.receipt_code) as rp_quantity,rp.quantity as rp_qnt,sum(qch.quantity) qch_quantity,sum(qc.weight) as qc_weight,rp.weight as rp_wt,km.id as karigar_id,rp.id as rp_id');
    $this->db->from('Receive_products rp');
    $this->db->join('quality_control qc','rp.id = qc.receive_product_id','left');
    $this->db->join('quality_control_hallmarking qch','qch.qc_id = qc.id','left');
    $this->db->join('Karigar_manufacturing_mapping kmm','kmm.id = rp.kmm_id');
    $this->db->join('manufacturing_order_mapping mop','mop.id=kmm.mop_id');
    $this->db->join('manufacturing_order mo','mo.id=mop.manufacturing_order_id');
    $this->db->join('parent_category pm','pm.id = mop.parent_category_id');
    $this->db->join('karigar_master km','km.id=kmm.karigar_id');
    $this->db->join('weights w','w.id = mop.weight_range_id');
    $this->db->where('rp.receipt_code',$code);
    $this->db->where('rp.status','4');
    $this->db->where('rp.is_skip_qc','0');
    $this->db->group_by('rp.id');
    return $this->db->get()->result_array();
  }
  private function get_filter_value($filter_input,$table_col_name){
    $column_name=array();  
    $filter_column_name=filter_column_name($table_col_name);
    $sql='';
    $i=0;
     
     //print_r($filter_input);die;
    foreach ($filter_input as $key => $search_value){
       $column_name=$filter_column_name;
       //print_r($search_value['search']['value']);
        if(!empty($search_value['search']['value'])){
          if($i != 0){
            $sql.=' AND  ';
          }
            $sql.=''.$column_name[$key].' like "%'.$search_value['search']['value'].'%" ';
            $i++;
        

         }   
    }

    if(!empty($sql)){  
      $this->db->where($sql);  
    } 
       
  }

  private function all_like_queries($search){
    $this->db->where("(rp.quantity LIKE '%$search%' OR rp.receipt_code LIKE '%$search%' OR mo.id LIKE '%$search%' OR mo.order_date LIKE '%$search%' OR km.name LIKE '%$search%' OR pm.name LIKE '%$search%' OR w.from_weight LIKE '%$search%' OR w.to_weight LIKE '%$search%')");
  }
  public function update($id){
    $this->db->where('id',$id);
    $this->db->set('status','5');
    if($this->db->update('Receive_products')){
      return true;
    }else{
      return false;
    }
  }  
  public function send_mfg_qc($receipt_code){
  	$this->db->where('receipt_code',$receipt_code);
  	$this->db->set('status','4');
  	if($this->db->update('Receive_products')){
  		return true;
  	}else{
  		return false;
  	}
  }
  public function find($rp_id){
    $this->db->select('rp.*,km.name,(w.from_weight+w.to_weight*rp.quantity/2) weight,rp.weight as bom_weight');
    $this->db->from('Receive_products rp');
    $this->db->join("Karigar_manufacturing_mapping kmm","kmm.id = rp.kmm_id");
    $this->db->join('manufacturing_order_mapping mop','mop.id=kmm.mop_id','left');
    $this->db->join('parent_category pm','pm.id = mop.parent_category_id');;
    $this->db->join('karigar_master km','km.id=kmm.karigar_id','left');
    $this->db->join('weights w','w.id = mop.weight_range_id');
    $this->db->where('rp.id',$rp_id);
    $result = $this->db->get()->row_array();
    return $result;
  }
  public function get_rec_qnt_by_kmm_id($kmm_id){
    $this->db->select('sum(quantity) qnt');
    $this->db->from('Receive_products');
    $this->db->where('kmm_id',$kmm_id);
    $this->db->where('module','2');
    $result = $this->db->get()->row_array();
    return $result;
  }

    public function get_rec_wt_by_kmm_id($kmm_id){
    $this->db->select('sum(receive_weight) wt');
    $this->db->from('Karigar_manufacturing_mapping');
    $this->db->where('id',$kmm_id);
    $this->db->where('status','1');
    $result = $this->db->get()->row_array();
    return $result;
  }
}
