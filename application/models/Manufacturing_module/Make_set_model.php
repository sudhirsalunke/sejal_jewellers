<?php

class Make_set_model extends CI_Model {

  function __construct() {
  	parent::__construct();
    $this->table_name = "make_set";
  }
  public function get($filter_status='',$status='',$params='',$search='',$limit='',$id=''){
    $this->db->select('ms.*');
    $this->db->from($this->table_name.' ms');

    //$this->db->join('make_set_quantity_relation');
   // $this->db->join('department_ready_product drp','ms.id=drp.ms_id','left');
   // $this->db->where('drp.ms_id is null');
    $this->all_like_queries($search);
    if($limit == true){
      $this->db->order_by('ms.id','desc');
      $this->db->limit($params['length'],$params['start']);
    }
    $result = $this->db->get()->result_array();
    // print_r($result);
    // echo $this->db->last_query();exit;
    return $result;
  }
  public function get_list($id){
    $this->db->select('msr.*,pm.name as name,tp.quantity as qnt,tp.id as tp_id,tp.product_code');
    $this->db->from('make_set_quantity_relation msr');
    $this->db->join('tag_products tp','tp.id = msr.tag_id');
    $this->db->join('make_set ms','ms.id = msr.make_set_id');
    $this->db->join('parent_category pm','pm.id = tp.sub_category_id');
    $this->db->where('msr.status','0');
    $this->db->where('msr.make_set_id',$id);
    $this->db->group_by('msr.id');
   // $this->db->join('department_ready_product drp','ms.id=drp.ms_id','left');
   // $this->db->where('drp.ms_id is null');
    // $this->all_like_queries($search);
    // if($limit == true){
    //   $this->db->order_by('ms.id','desc');
    //   $this->db->limit($params['length'],$params['start']);
    // }
    $result = $this->db->get()->result_array();
    // print_r($result);
    // echo $this->db->last_query();exit;
    return $result;
  }
  private function all_like_queries($search){
    $this->db->where("(ms.id LIKE '%$search%')");
  }
  public function validatepostdata(){
  	$data['status'] = 'success';
   	$data['error'] ='';
  	$postData = $this->input->post();
  	foreach ($postData['qch_id'] as $key => $qch_id) {
  		$quantity = $postData['quantity'][$key];
        $res = $this->check_quantity($qch_id,$quantity,$this->input->post('id'));
        if($res['status'] == 'failure'){
          $data['status'] = 'failure';
          $data['error']['quantity_'.$qch_id] = $res['error'];
        }
  	}
  	return $data;
  }
  private function check_quantity($qch_id,$quantity,$ms_id=''){
  	$data['status'] = 'success';
    $data['error'] = '';
    if($quantity=='' || $quantity=='0' || $quantity < 0 || (is_numeric($quantity) && strpos($quantity, ".") !== false))
    {
      $this->error = true;
      $data['status'] = 'failure';
      $data['error'] = 'Please enter valid Quantity.';
    }
    elseif(!is_numeric($quantity))
    {
      $this->error = true;
      $data['status'] = 'failure';
      $data['error'] = 'Please enter numeric value.';
    }
    else{
    	if(empty($ms_id))
	      	$qty_sql = 'SELECT sum(msr.quantity) FROM make_set_quantity_relation msr WHERE qch.id=msr.qch_id group by msr.qch_id';
	    else
	      	$qty_sql = 'SELECT sum(msr.quantity) FROM make_set_quantity_relation msr WHERE qch.id=msr.qch_id AND msr.make_set_id != "'.$ms_id.'" group by msr.qch_id';
    	$this->db->select('qch.quantity,(qch.quantity - IF(('.$qty_sql.' ) is null,0,('.$qty_sql.'))) quantity');
    	$this->db->from('quality_control_hallmarking qch');
    	$this->db->where('qch.id',$qch_id);
    	$row = $this->db->get()->row_array();
    	if($row['quantity']<$quantity){
    		$data['status'] = 'failure';
          	$data['error'] = 'Quantity limit Exceeded.';
    	}
    }
    return $data;
  }
  public function store($postData){
  	$data = array('created_at' => date("Y-m-d H:i:s"),'updated_at' => date("Y-m-d H:i:s"));
  	$this->db->insert($this->table_name,$data);
  	$id = $this->db->insert_id();
  	foreach ($postData['qch_id'] as $key => $qch_id) {
  		$data = array(
  			"make_set_id" => $id,
  			"qch_id" => $qch_id,
  			"quantity" => $postData['quantity'][$key],
        "tag_id"=> $key,
        "qty_remaining"=>$postData['quantity'][$key],
  		);
  		$this->db->insert('make_set_quantity_relation',$data);
      $this->db->set('status',1);
      $this->db->where('id',$key);
      $this->db->update('tag_products');
  	}
  	return $id;
  }
  public function update($postData){
  	$data = array('updated_at' => date("Y-m-d H:i:s"));
  	$this->db->where('id',$postData['id']);
  	$this->db->update($this->table_name,$data);
  	$id = $this->db->insert_id();
        $delete_array = array();
        foreach ($postData['msr_id'] as $msr_id_key => $msrv) {
            if(!isset($postData['qch_id'][$msr_id_key])){
                $this->db->where('id', $msr_id_key);
                $this->db->delete('make_set_quantity_relation');
            }
        }
  	foreach ($postData['qch_id'] as $key => $qch_id) {
  		$data = array(
  			"make_set_id" => $postData['id'],
  			"qch_id" => $qch_id,
  			"quantity" => $postData['quantity'][$key],
        'tag_id'=>$key,
  		);
  		if($postData['msr_id'][$key]=="")
  			$this->db->insert('make_set_quantity_relation',$data);
  		else
  			$this->db->update('make_set_quantity_relation',$data,array('id' => $postData['msr_id'][$key]));
  	}
  	return $postData['id'];
  }
  public function find($id){
  	$this->db->where('ms.id',$id);
  	$this->db->select('msr.*');
  	$this->db->from($this->table_name.' ms');
  	$this->db->join('make_set_quantity_relation msr','msr.make_set_id=ms.id');
  	$res = $this->db->get()->result_array();
  	$data = array();
  	foreach ($res as $key => $value) 
  		$data[$value['qch_id']] = $value;
  	return $data;
  }
  public function delete($id){
  	$this->db->where('id',$id);
  	if($this->db->delete($this->table_name)){
              $this->db->where('make_set_id', $id);
              $this->db->delete('make_set_quantity_relation');
  		return get_successMsg();
  	}else{
  		return get_errorMsg();
  	}
  }

  public function check_ready_products($msqr_id){
   $this->db->where('msqr_id',$msqr_id);
   $this->db->from('department_ready_product');
   return $this->db->count_all_results();
  }

  public function update_msqr($update_data,$id){
    $this->db->where('id',$id);
    if($this->db->update('make_set_quantity_relation',$update_data)){
      return get_successMsg();
    }else{
      return get_errorMsg();
    }
  }

  public function update_tag($update_data,$id){
    $this->db->where('id',$id);
    if($this->db->update('tag_products',$update_data)){
      return get_successMsg();
    }else{
      return get_errorMsg();
    }
  }
}
?>