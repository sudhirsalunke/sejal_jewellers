<?php

class Manufacturing_dashboard_model extends CI_Model {

  function __construct() {
  	$this->table_name = "departments";
    parent::__construct();
  }
  public function get_pending_receive_product_count(){
  	$this->db->select('kmm.id,mo.id order_id,DATE_FORMAT(kmm.created_at, "%d-%m-%Y") as order_date,DATE_FORMAT(kmm.delivery_date, "%d-%m-%Y") as delivery_date,pm.name sub_cat_name,w.from_weight,w.to_weight,kmm.quantity, kmm.approx_weight,km.name karigar_name,sum(rp.quantity) rp_quantity,mom.product_code');
  	$this->db->from('Karigar_manufacturing_mapping kmm');
  	$this->db->join('manufacturing_order_mapping mom','mom.id=kmm.mop_id');
  	$this->db->join('manufacturing_order mo','mo.id=mom.manufacturing_order_id');
  	$this->db->join('parent_category pm','pm.id = mom.parent_category_id');
    $this->db->join('weights w','w.id = mom.weight_range_id');
    $this->db->join('karigar_master km','km.id = kmm.karigar_id');
    $this->db->join('Receive_products rp','rp.kmm_id = kmm.id','left');
    $this->db->group_by('kmm.id');
    $this->db->where('kmm.status','1');
    $this->db->order_by('kmm.id','DESC');
   // echo $this->db->last_query();
  	$result = $this->db->get()->result_array();
  	$total_qty = 0;
  	$assign_qty = 0;
  	foreach ($result as $key => $value) {
      $rp_quantity = $this->Received_orders_model->get_rec_qnt_by_kmm_id($value['id']);
  		$total_qty += $value['quantity'];
  		$assign_qty += $rp_quantity['qnt'];
  	}
  	return $total_qty - $assign_qty;
  }
  public function get_pending_qc(){
  	$this->db->select('rp.receipt_code,(select SUM(quantity) from Receive_products where receipt_code= rp.receipt_code and is_skip_qc="0" and status="4") as rp_quantity,mo.id order_id,mo.order_date,km.name karigar_name,pm.name,w.from_weight,w.to_weight,rp.id rp_id,sum(qc.quantity) as quantity');
    $this->db->from('Receive_products rp');
    $this->db->join('quality_control qc','qc.receive_product_id=rp.id','left');
    $this->db->join('Karigar_manufacturing_mapping kmm','rp.kmm_id=kmm.id');
    $this->db->join('manufacturing_order_mapping mom','mom.id=kmm.mop_id');
    $this->db->join('manufacturing_order mo','mo.id=mom.manufacturing_order_id');
    $this->db->join('parent_category pm','pm.id = mom.parent_category_id');
    $this->db->join('karigar_master km','km.id = kmm.karigar_id');
    $this->db->join('weights w','w.id = mom.weight_range_id');
    $this->db->where('rp.status','4');
    $this->db->where('rp.module','2');
    $this->db->where('rp.is_skip_qc','0');
    $this->db->where('rp.receipt_code is NOT NULL', NULL, FALSE);
    $this->db->order_by('rp.id','DESC');
    $this->db->group_by('rp.receipt_code');
    $result = $this->db->get()->result_array();
    $total_qty = 0;
  	$rp_quantity = 0;
  	foreach ($result as $key => $value) {
  		$total_qty += $value['quantity'];
  		$rp_quantity += $value['rp_quantity'];
  	}
  	return $rp_quantity - $total_qty;
  }
  public function get_rejected_qc($department_id){
    // $this->db->select('qc.id,qc.receipt_code,qc.status,qc.quantity qc_quantity,mo.id order_id,mo.order_date,km.name karigar_name,w.from_weight,w.to_weight,pm.name sub_cat_name,mom.product_code');
    // $this->db->from('quality_control qc');
    // $this->db->join('Receive_products rp','rp.receipt_code=qc.receipt_code');
    // $this->db->join('Karigar_manufacturing_mapping kmm','rp.kmm_id=kmm.id');
    // $this->db->join('manufacturing_order_mapping mom','mom.id=kmm.mop_id');
    // $this->db->join('manufacturing_order mo','mo.id=mom.manufacturing_order_id');
    // $this->db->join('parent_category pm','pm.id = mom.parent_category_id');
    // $this->db->join('karigar_master km','km.id = kmm.karigar_id');
    // $this->db->join('weights w','w.id = mom.weight_range_id');
    // $this->db->where('qc.module','2');
    // $this->db->where("(qc.status='8' OR qc.status='0')");
    // $this->db->where("qc.status",'0');
    // $this->db->group_by('qc.id');
    // $result = $this->db->get()->result_array();
    // print_r($result);
    // $total_qty = 0;
    // foreach ($result as $key => $value) {
    //   $total_qty += $value['qc_quantity'];
    // }
    // return $total_qty;
  // print_r($get_rejected_qc);die;
    if($department_id=='2' || $department_id=='3' || $department_id =='6' || $department_id=='10'){
    $_GET['status'] = 'rejected';
    $get_rejected_qc = $this->Quality_control_model->get('','','','',true,'',$department_id);
      return sizeof($get_rejected_qc);
    }else{
     $tot=0;
        $_GET['status'] = 'rejected';
      $get_rejected_qc = $this->Quality_control_model->get('','','','',true,'',$department_id);
        foreach ($get_rejected_qc as $key => $value) {
          $rejected_res = $this->Manufacturing_department_order_model->check_rejected_quantity($value['id']);
          $ammended_res = $this->Quality_control_model->get_ammended_products($value['id']);
         
          $tot+=($value['qc_quantity']-$rejected_res['rejected_quantity'] - $ammended_res['rejected_quantity']);
        }
      return $tot;
    }
  }
   public function get_accepted_qc($department_id){
     $this->db->select('qc.id,qc.receipt_code,qc.status,qc.quantity qc_quantity,mo.id order_id,mo.order_date,km.name karigar_name,w.from_weight,w.to_weight,pm.name sub_cat_name,mom.product_code');
    $this->db->from('quality_control qc');
    // $this->db->join('quality_control_hallmarking qch','qch.qc_id = qc.id','left');
    $this->db->join('Receive_products rp','rp.id=qc.receive_product_id');
    $this->db->join('Karigar_manufacturing_mapping kmm','rp.kmm_id=kmm.id');
    $this->db->join('manufacturing_order_mapping mom','mom.id=kmm.mop_id');
    $this->db->join('manufacturing_order mo','mo.id=mom.manufacturing_order_id');
    $this->db->join('parent_category pm','pm.id = mom.parent_category_id');
    $this->db->join('karigar_master km','km.id = kmm.karigar_id');
    $this->db->join('weights w','w.id = mom.weight_range_id');
    $this->db->join('tag_products tg','tg.qc_id = qc.id','left');
    $this->db->where('qc.module','2');
    $this->db->where("(tg.status='0' or qc.status='4')");
    if(!empty($department_id)){
      $this->db->where('qc.department_id',$department_id);
    }
    $this->db->group_by('qc.id');
    $result = $this->db->get()->result_array();
    //echo $this->db->last_query();print_r($result);exit;
    $total_qty = 0;
/*    $hm_quantity = $this->Quality_control_model->get_quantity_by_id('1');
      print_r($hm_quantity);*/
  	foreach ($result as $key => $value) {
      $tp_quantity = $this->Tag_products_model->get_quantity_by_id($value['id']);
      $hm_quantity = $this->Quality_control_model->get_quantity_by_id($value['id']);
     /* print_r($value['id']);*/
  		$total_qty += ($value['qc_quantity']-$tp_quantity-$hm_quantity);
  	}

    return $total_qty;
  }
  public function get_sent_hm($department_id){
  $this->db->select('qch.id,qch.created_at HM_send_date,mo.order_name,pm.name sub_cat_name,rp.receipt_code,qch.quantity,km.name km_name,w.from_weight,w.to_weight,qch.remaining_qty');
    $this->db->from('quality_control_hallmarking qch');
    $this->db->join('quality_control qc','qc.id=qch.qc_id');
    $this->db->join('Receive_products rp','rp.receipt_code=qch.receipt_code');
    $this->db->join('Karigar_manufacturing_mapping kmm','rp.kmm_id=kmm.id');
    $this->db->join('manufacturing_order_mapping mom','mom.id=kmm.mop_id');
    $this->db->join('manufacturing_order mo','mo.id=mom.manufacturing_order_id');
    $this->db->join('karigar_master km','km.id=kmm.karigar_id','left');
    $this->db->join('weights w','w.id = mom.weight_range_id');
    $this->db->join('parent_category pm','pm.id = mom.parent_category_id');
    $this->db->where('qch.module','2');
    $this->db->where('qch.status','7');
    $this->db->order_by('qch.id','desc');
     if(!empty($department_id)){
      $this->db->where('qch.department_id',$department_id);
    }
    $this->db->group_by('qch.id');
    $result = $this->db->get()->result_array();
    $total_qty = 0;
  	foreach ($result as $key => $value) {
      $total_qty += $value['quantity'] - $value['remaining_qty'];
  	}
    return $total_qty;
  }
  public function get_ammended_products(){
    $this->db->select('sum(ap.quantity) count');
    $this->db->from('Amend_products ap');
    $this->db->where('ap.module','2');
    $result = $this->db->get()->row_array();
    if(empty($result['count'])){
        return 0;
    }
    return $result['count'];
  }
}