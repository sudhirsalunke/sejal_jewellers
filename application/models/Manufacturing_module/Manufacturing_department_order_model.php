<?php
class Manufacturing_department_order_model extends CI_Model {

  function __construct() {
  	$this->table_name1 = "manufacturing_order";
  	$this->table_name2 = "manufacturing_order_mapping";
    $this->table_name3 = "manufacturing_order_variant_mapping";    
    parent::__construct();
    $this->load->model('Sub_category_model');
  }
  public function validate_order_sheet(){
      if($_POST['dep_id']=='2' || $_POST['dep_id'] == '8' || $_POST['dep_id'] == '3' || $_POST['dep_id'] =='6' || $_POST['dep_id'] =='10' ){
        unset($_POST['department_order[quantity]']);
      $this->form_validation->set_rules($this->config->item('department_order_flow', 'admin_validationrules'));
      }else{
         unset($_POST['department_order[weight]']);
  	$this->form_validation->set_rules($this->config->item('department_order', 'admin_validationrules'));
      }
 /*   print_r($_POST);

    die;*/
    $data['status']= 'success';
    if($this->form_validation->run()===FALSE){
      $data['status']= 'failure';
      $data['data']= '';
      $data['error'] = $this->getErrorMsg();
    }
    return $data;
  }
  private function getErrorMsg(){
    if($_POST['dep_id'] == '2' || $_POST['dep_id'] == '8' || $_POST['dep_id'] == '3' || $_POST['dep_id'] =='6' || $_POST['dep_id'] =='10' ){ $display_name='weight'; }else{ $display_name='quantity'; } 
    return array(
      'order_date'=>strip_tags(form_error('department_order[order_date]')),
      'department_id'=>strip_tags(form_error('department_order[department_id]')),
      'sub_category_id'=>strip_tags(form_error('department_order[sub_category_id]')),
      'weight_range_id'=>strip_tags(form_error('department_order[weight_range_id]')),
      $display_name=>strip_tags(form_error('department_order['.$display_name.']')),
      //'quantity'=>strip_tags(form_error('department_order[quantity]')),
      'department_id'=>strip_tags(form_error('department_order[department_id]')),
      'order_name'=>strip_tags(form_error('department_order[order_name]')),
    );
  }
  public function store($insert_array,$id='',$qc_rejected = false){

  	if(empty($insert_array)){
  		$data['status']= 'failure';
  		$data['error']['common']= 'Please Provide at least one sub category';
			return $data;
  	}
  	if(!empty($id)){
      foreach ($insert_array as $key => $value) {
        if(!array_key_exists('id', $value)){
          $array[] = $value;
        }
      }
      if(!empty($array))
        $this->insert_order_mapping($array,$id);
  	}else{
      $id = $this->create_order($insert_array);
      $mapping_data = $this->insert_order_mapping($insert_array,$id);
      if($qc_rejected == true){
        $data['status']= 'success';
        $data['order_id']= $mapping_data;
        return $data;
      }

  	}
    $data['status']= 'success';
    $data['order_id']= $id;
    return $data;
  }

  private function insert_order_mapping($insert_array,$id){
    $count = 0;
   //print_r($insert_array);
    foreach ($insert_array as $key => $value) {
      $count++;
      $sub_cat_name        = $this->Parent_category_model->parent_category_name_by_id($value['sub_category']);
      $mapping_array = array(
        'manufacturing_order_id'=>$id,
        'parent_category_id'=>$value['sub_category'],
        'weight_range_id'=>$value['weight_range_id'],
        'weight'=>@$value['weight'],        
        'quantity'=>@$value['quantity'],
        'rejected_weight'=>@$value['rejected_weight'],
        'rejected_qc_id'=>@$value['rejected_qc_id'],
        'status'=>'0',
        'product_code'=>$sub_cat_name->name."0".$count,
        'department_id'=>$value['department_id'],
        'created_at'=>date('Y-m-d H:i:sa'),
        'updated_at'=>date('Y-m-d H:i:sa'),
        );
        
        //$this->db->insert('manufacturing_order_mapping',$mapping_array);
        if($this->db->insert('manufacturing_order_mapping',$mapping_array)){
        $last_in_id = $this->db->insert_id();
            if(@$value['variant'] !=''){
                foreach ($value['variant'] as $key1 => $variant) {
                  $variant_array = array(
                    'mfg_order_map_id'=>$last_in_id,
                    'product_variants_id'=>$variant,                                                     
                    'created_at'=>date('Y-m-d H:i:sa'),
                    'updated_at'=>date('Y-m-d H:i:sa'),
                    );
         
                $this->db->insert('manufacturing_order_variant_mapping',$variant_array);
                }
              }  
        }
        

        //code for adding submiting subcategory images against order
        if(array_key_exists("sub_cat_img_id",$value))
        {         
           $sub_cat_img_id_arr = explode("#",$value['sub_cat_img_id']);
           foreach ($sub_cat_img_id_arr as $value_id) {
               if( $value_id != ""){
                  $data_arr = array(
                        'manufacturing_order_map_id'=>$last_in_id,
                        'sub_cat_img_id'=> $value_id
                     );
                  $this->db->insert('manufacturing_order_subcat_img_mapping',$data_arr);
               }
            }
        }
      }
      return $last_in_id;
      
    /*foreach ($insert_array as $key => $value) {
      $mapping_array[] = array(
        'manufacturing_order_id'=>$id,
        'sub_category_id'=>$value['sub_category'],
        'weight_range_id'=>$value['weight_range_id'],
        'quantity'=>$value['quantity'],
        'status'=>'0',
        'created_at'=>date('Y-m-d H:i:sa'),
        'updated_at'=>date('Y-m-d H:i:sa')
        );
    }
    $this->db->insert_batch('manufacturing_order_mapping',$mapping_array);*/
    
  }
  private function create_order($insert_array){
    foreach ($insert_array as $key => $value) {
        $array[$value['department_id']] = array(
          'department_id'=>$value['department_id'],
          'order_name'=>$value['order_name'],
          'order_date'=>date('Y-m-d',strtotime($value['order_date'])),
          'status'=>'0',
          'created_at'=>date('Y-m-d H:i:sa'),
          'updated_at'=>date('Y-m-d H:i:sa')
          );
    }
    foreach ($array as $key => $value) {
      $this->db->insert($this->table_name1,$array[$key]);
      $id = $this->db->insert_id();
    }
    return $id; 
  }
  public function get($filter_status,$status,$params,$search,$limit,$department_id){
  	$this->db->select('mo.id,mo.is_sale,mo.order_name,mo.order_date,d.name,sum(kmm.quantity) as kariger_engaged_qty');
  	$this->db->from($this->table_name1.' mo');
  	$this->db->join('departments d','mo.department_id = d.id','left');
    $this->db->join('manufacturing_order_mapping mop','mop.manufacturing_order_id = mo.id','left');
    $this->db->join('Karigar_manufacturing_mapping kmm','kmm.mop_id = mop.id','left');
    $this->db->where("(mo.is_sale ='0' OR (mo.is_sale='1' AND sent='1'))");
    $this->db->where("mop.status",'0');
    if(!empty($department_id)){
      $this->db->where('d.id',$department_id);
    }
    $this->db->group_by('mo.id');
   /* if($search!="")
    {
       $this->db->where('mo.id',$search);
    }*/
    if(isset($params['columns']) && !empty($params['columns'])){
      $filter_input=$params['columns'];

      $table_col_name="department_order";
      $this->get_filter_value($filter_input,$table_col_name);
    }
 

    if(!empty($params) && $limit == true){
      $this->db->order_by('mo.created_at', 'DESC');
      $this->db->limit($params['length'],$params['start']);
      $result = $this->db->get()->result_array();
    }else{
      $row_array = $this->db->get()->result_array();
      $result = count($row_array);
      //echo $this->db->last_query();print_r($result);exit;
    }

    //echo $this->db->last_query(); die;
  	return $result;
  }
  private function get_filter_value($filter_input,$table_col_name){
    $column_name=array();  
    $filter_column_name=filter_column_name($table_col_name);
    $sql='';
    $i=0;
     
     //print_r($filter_input);die;
    foreach ($filter_input as $key => $search_value){
       $column_name=$filter_column_name;
       //print_r($search_value['search']['value']);
        if(!empty($search_value['search']['value'])){
          if($i != 0){
            $sql.=' AND  ';
          }
            $sql.=''.$column_name[$key].' like "%'.$search_value['search']['value'].'%" ';
            $i++;
        

         }   
    }

    if(!empty($sql)){  
      $this->db->where($sql);  
    } 
       
  }
  public function find($id){
  	$this->db->from($this->table_name1);
  	$this->db->where('id',$id);
  	$result = $this->db->get()->row_array();
  	return $result;
  }
  
  public function print_order($id){
    $this->db->select('kmm.id,mom.id AS mom_id,mo.id order_id,DATE_FORMAT(kmm.created_at, "%d-%m-%Y") as assign_date,DATE_FORMAT(kmm.delivery_date, "%d-%m-%Y") as delivery_date,mo.order_date,pm.name sub_cat_name,w.from_weight,w.to_weight,kmm.quantity,kmm.approx_weight,km.name karigar_name,mom.product_code,km.id karigar_id,mvm.mfg_order_map_id,group_concat(pv.name) as p_variant,kmm.mop_id,kmm.total_weight,kmm.department_id,mom.weight');
    $this->db->from('Karigar_manufacturing_mapping kmm');
    $this->db->join('manufacturing_order_mapping mom','mom.id=kmm.mop_id','left');
    $this->db->join('manufacturing_order mo','mo.id=mom.manufacturing_order_id','left');
    $this->db->join('manufacturing_order_variant_mapping mvm','mvm.mfg_order_map_id = mom.id','left');
    $this->db->join('product_variants pv','pv.id=mvm.product_variants_id','left');
    
    // $this->db->join('sub_category_master scm','scm.id = mom.sub_category_id');
    $this->db->join('parent_category pm','pm.id = mom.parent_category_id','left');
    $this->db->join('weights w','w.id = mom.weight_range_id','left');
    $this->db->join('karigar_master km','km.id = kmm.karigar_id','left');
    $this->db->where('kmm.id',$id);
    $this->db->group_by('mvm.mfg_order_map_id');
   
    $result = $this->db->get()->row_array();
    $result['view_delivery_date'] = date('d-m-Y', strtotime("+12 day"));
    return $result;
  }
  public function get_mom_details($id,$department_id){
    $this->db->select('mop.*,pm.name,mo.order_name,w.from_weight,w.to_weight,mop.id as tr_count,pm.id sub_category,w.id weight_range_id,mop.quantity as quanity,sum(kmm.quantity) as assign_quantity, sum(kmm.approx_weight) as approx_weight,pci.path,mo.department_id,mop.weight,sum(kmm.total_weight) as total_weight ');
    $this->db->from($this->table_name2.' mop');
    $this->db->join($this->table_name1.' mo','mo.id = mop.manufacturing_order_id');
    $this->db->join('parent_category pm','pm.id = mop.parent_category_id');
    $this->db->join('weights w','w.id = mop.weight_range_id');
    $this->db->join('Karigar_manufacturing_mapping kmm','kmm.mop_id = mop.id','left');
     $this->db->join('manufacturing_order_subcat_img_mapping mos','mos.manufacturing_order_map_id = mop.id','left');
    $this->db->join('parent_cat_images pci','pci.id = mos.sub_cat_img_id','left');
    $this->db->where('mop.id',$id);
    if(!empty($department_id)){
      $this->db->where('mop.department_id',$department_id);
    }
    $this->db->group_by('mop.id');
    $this->db->order_by('mop.id','DESC');
    $result = $this->db->get()->row_array();
    return $result;
  }
  public function get_all_subcategories_by_order_id($order_id,$order_date,$department,$filter_status='',$status='',$param='$_REQUEST',$search='',$limit='',$variant=''){
    if($variant!= true){
    $this->db->select('mop.*,pm.name,w.from_weight,w.to_weight,mop.id as tr_count,pm.id sub_category,'.$department.' as department,"'.$order_date.'" as order_date ,w.id weight_range_id,mop.quantity as quanity,sum(kmm.quantity) as assign_quantity, sum(kmm.approx_weight) as approx_weight,pci.path,mop.manufacturing_order_id as order_id ,sum(kmm.total_weight) as total_weight');
  }else{
    $this->db->select('mop.*,pm.name,w.from_weight,w.to_weight,mop.id as tr_count,pm.id sub_category,'.$department.' as department,"'.$order_date.'" as order_date ,w.id weight_range_id,mop.quantity as quanity,sum(kmm.quantity) as assign_quantity, sum(kmm.approx_weight) as approx_weight,pci.path,group_concat(pv.name) as p_variant,mop.manufacturing_order_id as order_id,sum(kmm.total_weight) as total_weight');
  }
    $this->db->from('manufacturing_order_mapping mop');
    $this->db->join('parent_category pm','pm.id = mop.parent_category_id');
    $this->db->join('weights w','w.id = mop.weight_range_id');
    $this->db->join('Karigar_manufacturing_mapping kmm','kmm.mop_id = mop.id','left');
    $this->db->join('manufacturing_order_subcat_img_mapping mos','mos.manufacturing_order_map_id = mop.id','left');
    $this->db->join('parent_cat_images pci','pci.id = mos.sub_cat_img_id','left');
    if($variant == true){
        $this->db->join('manufacturing_order_variant_mapping mvm','mvm.mfg_order_map_id = mop.id','left');
        $this->db->join('product_variants pv','pv.id=mvm.product_variants_id','left');
    }

    $this->db->where('mop.manufacturing_order_id',$order_id);
    $this->db->where('mop.status','0');
    if(!empty($department_id)){
      $this->db->where('mop.department_id',$department_id);
    }


   /* if($search!="")
    {
        $this->db->like('pm.name', $search);
    }*/
    //$param=$_REQUEST;
  //print_r($param['columns'][3]['search']['value']);
   if(isset($param['columns'][4]['search']['value']) && !empty($param['columns'][4]['search']['value'])){
      $this->db->like('pm.name', $param['columns'][4]['search']['value']);
    }
    
  	$this->db->group_by('mop.id');

    if($limit == true){
      $this->db->order_by('mop.id','DESC');
      
      $result = $this->db->get()->result_array();
    }else{
      $row_array = $this->db->get()->result_array();
      $result = count($row_array);
      //echo $this->db->last_query();print_r($result);exit;
    }

  //echo $this->db->last_query();print_r($result);die;
  	return $result;
  }

  public function get_dept_by_order_id_data($order_id,$order_date,$department,$filter_status='',$status='',$param='$_REQUEST',$search='',$limit='',$variant=''){
    if($variant!= true){
    $this->db->select('mop.*,pm.name,w.from_weight,w.to_weight,mop.id as tr_count,pm.id sub_category,'.$department.' as department,"'.$order_date.'" as order_date ,w.id weight_range_id,mop.quantity as quanity,sum(kmm.quantity) as assign_quantity, sum(kmm.approx_weight) as approx_weight,pci.path,mop.manufacturing_order_id as order_id,sum(kmm.total_weight) as total_weight');
  }else{
    $this->db->select('mop.*,pm.name,w.from_weight,w.to_weight,mop.id as tr_count,pm.id sub_category,'.$department.' as department,"'.$order_date.'" as order_date ,w.id weight_range_id,mop.quantity as quanity,sum(kmm.quantity) as assign_quantity, sum(kmm.approx_weight) as approx_weight,pci.path,group_concat(pv.name) as p_variant,mop.manufacturing_order_id as order_id,sum(kmm.total_weight) as total_weight');
  }
    $this->db->from('manufacturing_order_mapping mop');
    $this->db->join('parent_category pm','pm.id = mop.parent_category_id');
    $this->db->join('weights w','w.id = mop.weight_range_id');
    $this->db->join('Karigar_manufacturing_mapping kmm','kmm.mop_id = mop.id','left');
    $this->db->join('manufacturing_order_subcat_img_mapping mos','mos.manufacturing_order_map_id = mop.id','left');
    $this->db->join('parent_cat_images pci','pci.id = mos.sub_cat_img_id','left');
    if($variant == true){
        $this->db->join('manufacturing_order_variant_mapping mvm','mvm.mfg_order_map_id = mop.id','left');
        $this->db->join('product_variants pv','pv.id=mvm.product_variants_id','left');
    }

    $this->db->where('mop.manufacturing_order_id',$order_id);
    $this->db->where('mop.status','0');
    if(!empty($department_id)){
      $this->db->where('mop.department_id',$department_id);
    }


   /* if($search!="")
    {
        $this->db->like('pm.name', $search);
    }*/
    //$param=$_REQUEST;
  //print_r($param['columns'][4]['search']['value']);
    if(isset($param['columns'][4]['search']['value']) && !empty($param['columns'][4]['search']['value'])){
      $this->db->like('pm.name', $param['columns'][4]['search']['value']);
    }
    $this->db->group_by('mop.id');

    if($limit == true){
      $this->db->order_by('mop.id','DESC');      
      $result = $this->db->get()->result_array();
    }else{
      $row_array = $this->db->get()->result_array();
      $result = count($row_array);
      //echo $this->db->last_query();print_r($result);exit;
    }

  //echo $this->db->last_query();print_r($result);die;
    return $result;
  }


  //delete subcategory from department order
  public function delete($id){
    $this->db->where('id',$id);
    if($this->db->delete('manufacturing_order_mapping')){

      return get_successMsg();
    }else{
      return get_errorMsg();
    }
  }
  //remove order
  public function delete_order($id){
    $this->db->where('manufacturing_order_id',$id);
    if($this->db->delete($this->table_name2))
    $this->db->where('id',$id);
    if($this->db->delete($this->table_name1)){
      return get_successMsg();
    }else{
      return get_errorMsg();
    }
  }
  

  //Fetching subcategory images for particular order
  public function sub_cat_images($manu_order_id)
  {    
    
      $this->db->select('*');
      $this->db->from('sub_category_images');
      $this->db->join('manufacturing_order_subcat_img_mapping', 'manufacturing_order_subcat_img_mapping.sub_cat_img_id = sub_category_images.id');
      $this->db->where('manufacturing_order_subcat_img_mapping.manufacturing_order_map_id',$manu_order_id);
      return $this->db->get()->result_array();
  }

  //Fetching subcategory images for particular order
  public function get_engaged_karigar()
  {       
    $this->db->select('count(DISTINCT(ppl.KARIGAR_id)) as count');
    $this->db->from('Prepare_product_list ppl');
    $this->db->join('karigar_master k','k.id = ppl.karigar_id');
    $this->db->where('ppl.status','3');
    $result = $this->db->get()->row_array();
    return $result['count'];
  }
  public function get_parent_cat_img($mom_id){
    $this->db->select('mop.*,w.from_weight,w.to_weight,kmm.quantity as available_quantity,kmm.total_weight as available_weight');
    $this->db->from('manufacturing_order_mapping mop');
    $this->db->join('manufacturing_order_subcat_img_mapping mos','mos.manufacturing_order_map_id = mop.id','left');
    $this->db->join('Karigar_manufacturing_mapping kmm', 'kmm.mop_id  =mop.manufacturing_order_id ','left');
    $this->db->join('parent_cat_images pci','pci.id = mos.sub_cat_img_id','left');
    $this->db->join('weights w','w.id = mop.weight_range_id');
    $this->db->join('parent_category pm','pm.id = mop.parent_category_id');
    $this->db->where('mop.id',$mom_id);
    $result = $this->db->get()->result_array();
    return $result;
  }
  public function check_rejected_quantity($qc_id){
    $this->db->select('qc.quantity,sum(mom.quantity) as rejected_quantity');
    $this->db->from('quality_control qc');
    $this->db->join('manufacturing_order_mapping mom','mom.rejected_qc_id = qc.id');
    $this->db->where('qc.id',$qc_id);
    return $this->db->get()->row_array();
  }
  public function check_rejected_weight($qc_id){
    $this->db->select('qc.weight,sum(mom.weight) as rejected_weight');
    $this->db->from('quality_control qc');
    $this->db->join('manufacturing_order_mapping mom','mom.rejected_qc_id = qc.id');
    $this->db->where('qc.id',$qc_id);
    return $this->db->get()->row_array();
  }
  public function get_max_id(){
    $this->db->select('max(id) max_id');
    $this->db->from($this->table_name1);
    $result = $this->db->get()->row_array();
    if(!empty($result['max_id'])){
      return '1';
    }else{
      return $result['max_id'] + 1;
    }
  }
}
