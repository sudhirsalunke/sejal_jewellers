<?php
class Mfg_dep_direct_order_model extends CI_Model {

  function __construct() {
      $this->table_name = "sale_stock";
      parent::__construct();
  }


  public function validatepostdata(){
    //print_r($_POST);die;
    if($_POST['dep_id']=='2'){
        unset($_POST['quantity']);
      $this->form_validation->set_rules($this->config->item('mfg_dep_direct_order_wt','admin_validationrules'));

    }else{
         unset($_POST['weight']);
        $this->form_validation->set_rules($this->config->item('mfg_dep_direct_order_qty','admin_validationrules'));

      }

         if ($this->form_validation->run() == FALSE) {        
          return FALSE;
      } else {
          return TRUE;
      }
  }



   public function store(){
    $insert_dep_order_array=array();
    $mapping_array=array();
    $insert_dep_direct_order_array=$_POST;
    $order_name="Drt_order_".mt_rand();
    $receipt_code = "MNFREC".mt_rand();
    $postdata = $_POST;
    $count=0;
    $i=0;
   // print_r($postdata);
    $insert_dep_order_array['department_id']=$postdata['dep_id'];  
    $insert_dep_order_array['order_name']=@$postdata['order_name'];   
    $insert_dep_order_array['status']='0';
    $insert_dep_order_array['order_date']=date('Y-m-d');
    $insert_dep_order_array['created_at']=date('Y-m-d H:i:s');
    $insert_dep_order_array['updated_at']=date('Y-m-d H:i:s'); 
  // print_r($insert_dep_order_array);die;
    if ($this->db->insert('manufacturing_order',$insert_dep_order_array)) { 

      $id=$this->db->insert_id(); 
      foreach ($insert_dep_direct_order_array['product_name'] as $key => $value) {  
        
      $sub_cat_name   = $this->Parent_category_model->parent_category_name_by_id($value);
            $count++;
       
         if(@$postdata['is_skip_qc'][$key] && !empty($postdata['is_skip_qc'][$key])) {
            $is_skip_qc = @$postdata['is_skip_qc'][$key] ;
         }else{

           $is_skip_qc='0';
        }           
         
              $mapping_array = array(
                'manufacturing_order_id'=>$id,
                'parent_category_id'=>$value,
                'weight_range_id'=>@$postdata['weight_range_id'][$key],
                'weight'=>@$postdata['weight'][$key],        
                'quantity'=>@$postdata['quantity'][$key],
                'product_code'=>$sub_cat_name->name."0".$count,
                'status'=>'1',
                'department_id'=>$postdata['dep_id'],
                'created_at'=>date('Y-m-d H:i:sa'),
                'updated_at'=>date('Y-m-d H:i:sa'),
                );
                if ($this->db->insert('manufacturing_order_mapping',$mapping_array)){
                      $mop_id=$this->db->insert_id(); 
                        if($postdata['dep_id'] == '2' || $postdata['dep_id']=='3' || $postdata['dep_id'] =='6' || $postdata['dep_id']=='10'){
                         $approx_weight=@$postdata['weight'][$key];
                      }else{
                        $approx_weight=@$postdata['gross_wt'][$key];
                      }
                        $insert_array = array(
                            'karigar_id'=>$postdata['kariger_id'],
                            'mop_id'=>$mop_id,
                            'department_id'=>$postdata['dep_id'],
                            'quantity'=>@$postdata['quantity'][$key],                    
                            'total_weight'=>@$postdata['weight'][$key],
                            'receive_quantity'=>@$postdata['quantity'][$key],
                            'receive_weight'=>@$postdata['weight'][$key], 
                            'approx_weight'=>@$approx_weight,
                            'total_weight'=>@$postdata['weight'][$key],           
                            'status'=>'2',
                            'created_at'=>date('Y-m-d H:i:s'),
                            'updated_at'=>date('Y-m-d H:i:s'),
                            'delivery_date'=>date('Y-m-d',strtotime("+12 day"))
                            );
                
                            if ($this->db->insert('Karigar_manufacturing_mapping',$insert_array)){
                                  $kmm_id=$this->db->insert_id(); 
                                  $insert['date'] = date('Y-m-d');
                                  $insert['module'] = '2';
                                  $insert['status'] = '7';
                                  $insert['quantity'] = @$postdata['quantity'][$key];        
                                  $insert['kmm_id'] = $kmm_id;
                                  $insert['order_id'] =$id;
                                  $insert['department_id'] = $postdata['dep_id'];
                                  $insert['karigar_id'] = $postdata['kariger_id'];
                                  $insert['receipt_code'] = $receipt_code;
                                  $insert['net_wt'] = @$postdata['net_wt'][$key];
                                  $insert['wastage'] = @$postdata['wastage'][$key];
                                  $insert['pure'] = @$postdata['pure'][$key];
                                  $insert['gross_wt'] =@$postdata['gross_wt'][$key];
                                  $insert['amount'] = @$postdata['amount'][$key];        
                                  $insert['Product_name'] = $sub_cat_name->name; 
                                  $insert['tagging_status'] = @$postdata['tagging_status'];
                                  $insert['few_wt'] = @$postdata['few_wt'][$key];
                                  $insert['stone_wt'] = @$postdata['stone_wt'][$key];
                                  $insert['weight'] = @$postdata['weight'][$key];         
                                  //$insert['is_skip_qc'] =$is_skip_qc;
                                 $insert['is_skip_qc'] = (isset($postdata['is_skip_qc'][$key]) && !empty($postdata['is_skip_qc'][$key])) ? $postdata['is_skip_qc'][$key] : '0';
                    
                                   //print_r($value['tagging_status']);
                                   //print_r($postdata['tagging_status'][$key]);die;
                              if ($this->db->insert('Receive_products',$insert)){
                                              $this->store_receive_products($insert,$this->db->insert_id(),$value);
                                         if(!empty( $insert['few_wt'])){
                                          $this->Quality_control_model->update_few_wt_box($insert['Product_name'],$insert['few_wt']);
                                        }

                              }
                                    
                                
                                        

                            }

            }
            $data["status"]="success";  
            $data["receipt_code"]=$receipt_code; 

          } 
     
      return $data;
    }else{
      return get_errorMsg();
    }
  }

  private function store_receive_products($data,$rp_id){
    
      $insert_array =array(
          'user_id'=>$this->session->userdata('user_id'),
          'quantity'=>$data['quantity'],
          'weight'=>$data['gross_wt'],
          'gross_wt'=>$data['gross_wt'],
          'net_wt'=>$data['net_wt'],   
          'few_wt'=>$data['few_wt'],  
          'department_id'=>$data['department_id'],      
          'is_skip_qc'=>'1',
          'module'=>'2',
          'status'=>'8',
          'receive_product_id'=>$rp_id,
          'receipt_code'=>$data['receipt_code'],
          'qc_net_wt_max'=>$data['net_wt'] + (5 / 100) * $data['net_wt'],
          'qc_net_wt_min'=>$data['net_wt'] - (5 / 100) * $data['net_wt'],
          'remaining_net_wt'=>$data['net_wt'], 
        );
        if($this->db->insert('quality_control',$insert_array)){
          //echo $this->db->last_query();
        $this->insert_into_tags($insert_array,$this->db->insert_id());
        }

    }

    private function insert_into_tags($inserted_data,$qc_id){
    $qc_data = $this->Quality_control_model->find_by_qc_id($qc_id);
    $details = $this->Quality_control_model->get_voucher_details($qc_id);
    //print_r($qc_data['net_wt'] * (5 / 100) + $qc_data['net_wt']);die;
    $code = $this->Parent_category_model->get_category_codes($details["pm_id"]);
    $qc_quantity=$qc_data['quantity']; 
    $qc_net_wt_max= $qc_data['net_wt'] + (5 / 100) * $qc_data['net_wt'] ;
    $qc_net_wt_min= $qc_data['net_wt'] - (5 / 100) * $qc_data['net_wt'] ;
    for($i=0;$i<$qc_quantity;$i++){
      $data[] = array(
        'karigar_id'=>$details['karigar_id'],
        'sub_category_id'=>$details['pm_id'],
        'weight_range_id'=>$details['weight_range_id'],
        'sub_code' => $code[0]['code_name'],
        'sub_category_id'=>$code[0]['id'],
        'gr_wt'=>$qc_data['gross_wt'],  
        'net_wt'=>$qc_data['net_wt'],  
        'created_at' =>date('Y-m-d H:i:s'),
        'quantity'=>1,
        'qc_id'=>$qc_id,
        // 'qc_net_wt_max'=>$qc_net_wt_max /$qc_quantity,
        // 'qc_net_wt_min'=>$qc_net_wt_min /$qc_quantity,
        // 'qc_net_wt_max'=>$qc_data['net_wt'] + (5 / 100) * $qc_data['net_wt'],
        // 'qc_net_wt_min'=>$qc_data['net_wt'] - (5 / 100) * $qc_data['net_wt'],
        'department_id'=>$qc_data['department_id'],
        );
    }
//print_r($data);die;
    if(!empty($data)){
    $this->db->insert_batch('tag_products',$data);
    }
  }

  private function product_from_recive_store($data,$rp_id){  
    $product_code = $data['Product_name'].mt_rand();
    if($data['department_id']=='2' || $data['department_id']=='3' || $data['department_id'] =='6' || $data['department_id']=='10'){
         $insert_drp_array = array(    
        'quantity' =>@$data['quantity'],
        'gr_wt' =>$data['weight'],
        'net_wt' =>$data['net_wt'],
        'receive_product_id'=>$rp_id,
        'department_id'=>$data['department_id'],
        'product_code'=>$product_code,        
        'product'=>$data['Product_name'],
        'karigar_id'=>$data['karigar_id'],
        'chitti_no'=>$data['receipt_code'],
        'status'=>'0',
        'product_from'=>'1',
        'created_at' =>date('Y-m-d H:i:s'),
        'updated_at' =>date('Y-m-d H:i:s'),
      );
    }else{
      $insert_drp_array = array(    
        'quantity' =>@$data['quantity'],
        'weight' =>@$data['weight'],
        'gr_wt' =>$data['gross_wt'],
        'net_wt' =>$data['net_wt'],
        'receive_product_id'=>$rp_id,
        'department_id'=>$data['department_id'],
        'product_code'=>$product_code,
        'product'=>$data['Product_name'],
        'karigar_id'=>$data['karigar_id'],
        'chitti_no'=>$data['receipt_code'],
        'status'=>'0',
        'product_from'=>'1',
        'created_at' =>date('Y-m-d H:i:s'),
        'updated_at' =>date('Y-m-d H:i:s'),
      );
    }
    $this->db->insert('department_ready_product',$insert_drp_array);
  }

   

  public function receive_product_details_by_receipt_code($receipt_code){
    $this->db->select('kmm.karigar_id as karigar_id,km.name as karigar_name,pm.name,mop.parent_category_id,mop.id as mop_id,mop.manufacturing_order_id as order_id,mop.product_code as product_code, kmm.receive_quantity as quantity,kmm.id as kmm_id,w.from_weight,w.to_weight,kmm.receive_weight as weight,kmm.department_id,kmm.total_weight,rp.receipt_code,rp.net_wt,rp.pure,rp.amount,rp.wastage ,rp.gross_wt,rp.stone_wt');
    $this->db->from("Receive_products rp");
    $this->db->join('Karigar_manufacturing_mapping kmm','kmm.id=rp.kmm_id','left');
    $this->db->join('manufacturing_order_mapping mop','mop.id=kmm.mop_id','left');
    $this->db->join('parent_category pm','pm.id = mop.parent_category_id');;
    $this->db->join('karigar_master km','km.id=kmm.karigar_id','left');
    $this->db->join('weights w','w.id = mop.weight_range_id');
    $this->db->where("rp.receipt_code",$receipt_code);
    $result = $this->db->get()->result_array();
    return $result;
  }


 

}    
?>