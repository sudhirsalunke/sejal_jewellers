<?php
class Accounting_department_module extends CI_Model {

  function __construct() {
  	$this->table_name = "manufacturing_accounting";
    parent::__construct();
  }
  public function get($filter_status='',$status='',$params='',$search='',$limit='',$status='',$order_id='',$group_by=''){
  	if(!empty($order_id)){
  		$this->db->select('kmm.id,mo.id order_id,DATE_FORMAT(kmm.created_at, "%d-%m-%Y") as order_date,DATE_FORMAT(kmm.delivery_date, "%d-%m-%Y") as delivery_date,pm.name sub_cat_name,w.from_weight,w.to_weight,kmm.quantity, kmm.approx_weight,km.name karigar_name');
  	}else{
  		$this->db->select('mo.id order_id,mo.order_name name,mo.order_date');
  	}
    $this->db->from('Karigar_manufacturing_mapping kmm');
    $this->db->join('manufacturing_order_mapping mom','mom.id=kmm.mop_id');
    $this->db->join('manufacturing_order mo','mo.id=mom.manufacturing_order_id');
    $this->db->join('parent_category pm','pm.id = mom.parent_category_id');
    $this->db->join('weights w','w.id = mom.weight_range_id');
    $this->db->join('karigar_master km','km.id = kmm.karigar_id');
    $this->db->where('kmm.status','1');
    if(!empty($order_id)){
    	$this->db->where('mo.id',$order_id);
    }
    if(empty($group_by)){
	    $this->db->group_by('mo.id');
	    $this->db->order_by('mo.id','desc');
    }else{
    	$this->db->group_by($group_by);
	    $this->db->order_by($group_by,'desc');
    }

   	if(!empty($search)){
     $this->db->where("(mo.id LIKE '%$search%' OR mo.order_name LIKE '%$search%')");
    }
    if(!empty($params) && $limit==true){
      $this->db->limit($params['length'],$params['start']);
    }
    $result = $this->db->get()->result_array();
    return $result;
  }
}