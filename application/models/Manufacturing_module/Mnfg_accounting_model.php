<?php

class Mnfg_accounting_model extends CI_Model {

  function __construct() {
    $this->table_name = "quality_control";
    $this->table_name1 = "generate_packing_list";
    parent::__construct();
  }
  public function validatepostdata($postdata){
    $error_array = array();
    
    $this->form_validation->set_message('is_natural_no_zero','Quantity should be greater than zero');
    $this->form_validation->set_message('check_qc_qnt','Quantity Exceeded');
    $data = array();
    $data['status']= 'success';
    $_POST['qc'] = array();
    // print_r($postdata['qc']);exit;
    foreach ($postdata['qc']['receive_product_id'] as $key => $value) {
      $_POST['quantity'] = $postdata['qc']['quantity'][$value];
      $_POST['weight'] = $postdata['qc']['weight'][$value];
      $_POST['receive_product_id'] = $value;
     

      $this->form_validation->set_rules($this->config->item('Manufacturing_quality_control_all_qc', 'admin_validationrules'));
      if ($postdata['qc']['hallmarking'][$value] == '1') {
        $this->form_validation->set_message('qnt_validation','Quantity must be less than total quantity');
        $_POST['net_wt'] = $postdata['qc']['net_wt'][$value];
        $_POST['hallmarking_quantity'] = $postdata['qc']['hallmarking_quantity'][$value];
        // echo '<pre>';
        // print_r($_POST['net_wt']);
        // echo '</pre>';
        $this->form_validation->set_rules('hc_id', 'Hallmarking center', 'required');
        $this->form_validation->set_rules('hc_logo', 'Hallmarking Logo', 'required');
        $this->form_validation->set_rules('net_wt', 'Net Wt', 'required|numeric');
        $this->form_validation->set_rules(
          'hallmarking_quantity', 'Qauntity',
            array('required','is_natural_no_zero',array('qnt_validation',
              function($str){
                if(($_POST['hallmarking_quantity'] > 0) && $_POST['quantity'] < $_POST['hallmarking_quantity']){
                  return false;
                }else{
                  return true;
                }
              })
            )
          );
      }else{
        $_POST['hallmarking_quantity'] = $_POST['quantity'];
      }
      if($this->form_validation->run()===FALSE){
        $error_array['weight_'.$value] = strip_tags(form_error('weight'));
        $error_array['quantity_'.$value] = strip_tags(form_error('quantity'));
         if ($postdata['qc']['hallmarking'][$value] == '1') {
          $error_array['hc_id'] = strip_tags(form_error('hc_id'));
          $error_array['hc_logo'] = strip_tags(form_error('hc_logo'));
          $error_array['net_wt_'.$value] = strip_tags(form_error('net_wt'));
          $error_array['hallmarking_quantity_'.$value] = strip_tags(form_error('hallmarking_quantity'));
        }

        $data['status']= 'failure';
        $data['data']= '';
        $data['error'] = $error_array;
      }
    }
    return $data;
  }
  public function validate_tag_products(){
    $data['status'] = 'success';
    if(!empty($_POST['kr'])){
      foreach ($_POST['kr'] as $key => $value) {
        //print_r($_POST['kr']);
          if($value['net_wt']<'0.1'){
          $data['status'] = 'failure';
          $data['error']['net_wt_'.$key] = 'Net Weight should be greater than 0';
        }
        if(!is_numeric($value['net_wt'])){
          $data['status'] = 'failure';
          $data['error']['net_wt_'.$key] = 'Net Weight should be number';
        }
        if(empty($value['net_wt'])){
          $data['status'] = 'failure';
          $data['error']['net_wt_'.$key] = 'Net Weight is required';
        }

        if($value['quantity']<'0.1'){
          $data['status'] = 'failure';
          $data['error']['quantity_'.$key] = 'Quantity should be greater than 0';
        }
        if(!is_numeric($value['quantity'])){
          $data['status'] = 'failure';
          $data['error']['quantity_'.$key] = 'Quantity should be number';
        }
        if(empty($value['quantity'])){
          $data['status'] = 'failure';
          $data['error']['quantity_'.$key] = 'Quantity is required';
        }

        $this->db->select('quantity,receive_quantity');
        $this->db->where('id',$key);
        $this->db->where('status','1');
        $kmm_res = $this->db->get('Karigar_manufacturing_mapping')->row_array();
     if($kmm_res['receive_quantity'] + $value['quantity']> $kmm_res['quantity']){
        $data['status'] = 'failure';
        $data['error']['quantity_'.$key] = 'Quantity exceeded';
      }
       
       
      }
    }
    return $data;
  }
  public function validate_rejected_data($id){
    $data['status']= 'success';
    $error_array = array();
    $this->form_validation->set_rules($this->config->item('validate_rejected_data', 'admin_validationrules'));
    if($this->form_validation->run()===FALSE){
      $error_array['quantity_'.$id] = strip_tags(form_error('quantity'));
      $error_array['weight_'.$id] = strip_tags(form_error('weight'));
      $data['status']= 'failure';
      $data['data']= '';
      $data['error'] = $error_array;
    }
    return $data;
  }
  public function get($filter_status='',$status='',$params='',$search='',$limit='',$hallmarking =''){
    // $this->db->select('qc.id,qc.receipt_code,qc.status,qc.quantity qc_quantity,mo.id order_id,mo.order_date,km.name karigar_name,w.from_weight,w.to_weight,pm.name sub_cat_name,mom.product_code,sum(tp.quantity) tp_quantity,IF(qch.status = "7","7","0") qch_status');
    $this->db->select('qc.id,qc.receipt_code,qc.status,qc.quantity qc_quantity,mo.id order_id,mo.order_date,km.name karigar_name,w.from_weight,w.to_weight,pm.name sub_cat_name,mom.product_code');
    $this->db->from('quality_control qc');
    // $this->db->join('quality_control_hallmarking qch','qch.qc_id = qc.id','left');
    $this->db->join('Receive_products rp','rp.id=qc.receive_product_id');
    $this->db->join('Karigar_manufacturing_mapping kmm','rp.kmm_id=kmm.id');
    $this->db->join('manufacturing_order_mapping mom','mom.id=kmm.mop_id');
    $this->db->join('manufacturing_order mo','mo.id=mom.manufacturing_order_id');
    $this->db->join('parent_category pm','pm.id = mom.parent_category_id');
    $this->db->join('karigar_master km','km.id = kmm.karigar_id');
    $this->db->join('weights w','w.id = mom.weight_range_id');
    $this->db->where('qc.module','2');
    $this->db->where('qc.quantity > 0');
    if($_GET['status'] == 'complete'){
      $this->db->where("qc.status",'8');
      // $this->db->having("qch_status != '7'");
    }
    if($_GET['status'] == 'rejected')
      $this->db->where("qc.status",'0');
    if(!empty($id))
      $this->db->where('qc.id',$id);
    if(!empty($search))
      $this->all_like_queries($search);
    if($limit == true)
      $this->db->limit($params['length'],$params['start']);
    $this->db->group_by('qc.id');
    //if(!empty($filter_status)){
      // $this->db->order_by('rp.receipt_code','desc');
      $this->db->order_by('mo.id','desc');
   // }
    $result = $this->db->get()->result_array();
    // echo $this->db->last_query();die;
    return $result;
  }
  
  

  
  public function find($rp_id){
    $this->db->where('receive_product_id',$rp_id);
    $this->db->where('status!=','0');
    return $this->db->get($this->table_name)->row_array();
  }
  public function find_by_qc_id($qc_id){
    $this->db->where('id',$qc_id);
    return $this->db->get($this->table_name)->row_array();
  }

  private function all_like_queries($search){
    $this->db->where("(mo.id LIKE '%$search%' OR pm.name LIKE '%$search%' OR rp.receipt_code LIKE '%$search%'OR km.name LIKE '%$search%')");
  }
   
  
  public function get_karigar_id($cp_id){
    $this->db->select('karigar_id');
    $this->db->from('Prepare_product_list');
    $this->db->where('corporate_products_id',$cp_id);
    $result = $this->db->get()->row_array();
    return $result['karigar_id'];
  }
 
  public function update($array,$where){
    $this->db->where($where);
    $this->db->update($this->table_name,$array);
  }
  
  private function receive_product_details_by_id($id){
    $this->db->select('kmm.karigar_id as karigar_id,km.name as karigar_name,pm.name,mop.parent_category_id,mop.id as mop_id,mop.manufacturing_order_id as order_id,mop.product_code as product_code,(kmm.quantity - kmm.receive_quantity) quantity,kmm.id as kmm_id,w.from_weight,w.to_weight');
    $this->db->from("Karigar_manufacturing_mapping kmm");
    $this->db->join('manufacturing_order_mapping mop','mop.id=kmm.mop_id','left');
    $this->db->join('parent_category pm','pm.id = mop.parent_category_id');;
    $this->db->join('karigar_master km','km.id=kmm.karigar_id','left');
    $this->db->join('weights w','w.id = mop.weight_range_id');
    $this->db->where("kmm.id",$id);
    $result = $this->db->get()->row_array();
    return $result;
  }

  public function get_engage_karigars()
  {
    $this->db->select('distinct(kmm.karigar_id) as karigar_id,km.name as karigar_name,pm.name,mop.parent_category_id,mop.id as mop_id,mop.manufacturing_order_id as order_id,mop.product_code as product_code,kmm.quantity');
    $this->db->from("Karigar_manufacturing_mapping kmm");
    $this->db->join('manufacturing_order_mapping mop','mop.id=kmm.mop_id','left');
    $this->db->join('parent_category pm','pm.id = mop.parent_category_id');;
    $this->db->join('karigar_master km','km.id=kmm.karigar_id','left');
    $this->db->where("kmm.status",'1');
    $this->db->group_by("kmm.karigar_id");
    //echo $this->db->last_query();die;
    $result = $this->db->get()->result_array();
    return $result;
  }
  public function store(){
    $receipt_code = "MNFREC".mt_rand();
    $i = 0;
    foreach ($_POST['kr'] as $key => $value) {
      $data = $this->Karigar_order_model->get_department_id($key);
      //print_r($key);
      $insert['receipt_code'] = $receipt_code;
      $insert['manufacturing_order_id'] = $data['id'];
      $insert['department_id'] = $data['department_id'];
      $insert['karigar_id'] = $_POST['kariger_id'];
      $insert['kmm_id'] = $key;
      $insert['parent_category_id'] = $value['parent_category_id'];
      $insert['weight_range_id'] = $value['weight_range_id']; 
      $insert['quantity'] = $value['quantity'];
      $insert['net_wt'] = $value['net_wt'];
      $insert['status'] = '0';
      $insert['created_at'] = date('Y-m-d');
      $kmm_res = $this->check_quantity($key,$value['quantity']);
      if($kmm_res['status'] = 'success'){
        $this->db->where('id',$key);
        $this->db->set('receive_quantity',$kmm_res['receive_quantity']);
        if($kmm_res['is_update'] == true){
          $this->db->set('status','2');
        }
        $this->db->update('Karigar_manufacturing_mapping');
      }
      $pdf_data[$i] = $insert;
      $kmm_data = $this->receive_product_details_by_id($key);
      $pdf_data[$i]['kmm_id'] = $key;
      $pdf_data[$i]['name'] = $kmm_data['name'];
      $pdf_data[$i]['karigar_name'] = $kmm_data['karigar_name'];
      $pdf_data[$i]['weights'] = $kmm_data['from_weight'] .'-'.$kmm_data['to_weight'];
      $pdf_data[$i]['from_weight'] = $kmm_data['from_weight'];
      $pdf_data[$i]['to_weight'] = $kmm_data['to_weight'];
      $pdf_data[$i]['net_wt'] = $value['net_wt'];
      $i++;
    }
     return $pdf_data;
  }

  
  public function check_quantity($id,$quantity){
    $this->db->select('quantity,receive_quantity');
    $this->db->where('id',$id);
    $this->db->where('status','1');
    $kmm_res = $this->db->get('Karigar_manufacturing_mapping')->row_array();
      if($kmm_res['receive_quantity'] + $quantity == $kmm_res['quantity']){
      $data['status'] = 'success';
      $data['is_update'] = true;
      $data['receive_quantity'] = $kmm_res['receive_quantity'] + $quantity;
    }else if($kmm_res['receive_quantity'] + $quantity < $kmm_res['quantity']){
      $data['status'] = 'success';
      $data['is_update'] = false;
      $data['receive_quantity'] = $kmm_res['receive_quantity'] + $quantity;
    }else if($kmm_res['receive_quantity'] + $quantity > $kmm_res['quantity']){
      $data['status'] = 'error';
      $data['error']['quantity'][$id] = 'Quantity exceeded';
    }
    //echo $this->db->last_query();print_r($data);
    return $data;
  }
    public function get_total_qty($ids){

      $this->db->select('SUM(quantity) as total_qty');
      $this->db->where_in('id',explode(',', $ids));
      $result = $this->db->get('quality_control')->row_array();
      return $result['total_qty'];
    }
  public function receive_product($id){
    $this->db->select('kmm.karigar_id as karigar_id,km.name as karigar_name,pm.name,mop.parent_category_id,mop.id as mop_id,mop.manufacturing_order_id as order_id,mop.product_code as product_code,(kmm.quantity - kmm.receive_quantity) quantity,kmm.id as kmm_id,w.from_weight,w.to_weight,pm.id as parent_category_id ,mop.weight_range_id');
    $this->db->from("Karigar_manufacturing_mapping kmm");
    $this->db->join('manufacturing_order_mapping mop','mop.id=kmm.mop_id','left');
    $this->db->join('parent_category pm','pm.id = mop.parent_category_id');;
    $this->db->join('karigar_master km','km.id=kmm.karigar_id','left');
    $this->db->join('weights w','w.id = mop.weight_range_id');
    $this->db->where("kmm.status",'1');
    $this->db->where("kmm.karigar_id",$id);
    $result = $this->db->get()->result_array();
    return $result;
  }

  
   
}