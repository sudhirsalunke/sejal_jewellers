<?php
class Sales_return_product_model extends CI_Model {

  function __construct() {
  	parent::__construct();
    $this->table_name = "department_ready_product";
  }
  
  

  public function get($filter_status='',$status='',$params='',$search='',$limit='',$department_id=''){
    
  $this->db->select('drp.id,drp.chitti_no,drp.quantity,drp.net_wt,drp.gr_wt,km.name as karigar_name,drp.product_code ,drp.department_id,drp.id as drp_id,drp.weight,drp.product,DATE_FORMAT(drp.created_at,"%d-%m-%Y")  created_at,DATE_FORMAT(drp.sales_return_date,"%d-%m-%Y")  sales_return_date,d.name as department_name,drp.voucher_no');
  $this->db->from($this->table_name.' drp');

  $this->db->join('karigar_master km','drp.karigar_id =km.id','left');
  $this->db->join('departments d','drp.department_id =d.id','left');
        $this->db->where('drp.status',8);

    if((!empty($department_id))){
      $this->db->where('drp.department_id',$department_id);
    }

          if($department_id == '0'  || $department_id == '1' ||  $department_id == '5' || 
              $department_id == '7'  ||  $department_id == '8'  || $department_id == '11'){
              $table_col_name="sales_voucher_return_product";
          }else{
             $table_col_name="mfg_ready_pro_aprv_by_sales_bom";
          }
    
    if(isset($params['columns']) && !empty($params['columns'])){
      $filter_input=$params['columns'];
      $this->get_filter_value($filter_input,$table_col_name);
    }
    if($limit == true){
      $this->db->order_by('drp.id','DESC');
     // $this->db->order_by('qc.id','DESC');
      $this->db->limit($params['length'],$params['start']);
      $result = $this->db->get()->result_array();
    }else{
      $row_array = $this->db->get()->result_array();
      $result = count($row_array);
      //echo $this->db->last_query();print_r($result);exit;
    }
    // echo $table_col_name;
// echo $this->db->last_query();print_r($result);exit;
    return $result;
  }

  private function get_filter_value($filter_input,$table_col_name){
    $column_name=array();  
    $filter_column_name=filter_column_name($table_col_name);
    $sql='';
    $i=0;
     
     //print_r($filter_input);die;
    foreach ($filter_input as $key => $search_value){
       $column_name=$filter_column_name;
       //print_r($column_name);
       //print_r($search_value['search']['value']);
        if(!empty($search_value['search']['value'])){
          if($i != 0){
            $sql.=' AND  ';
          }
            $sql.=''.$column_name[$key].' like "%'.$search_value['search']['value'].'%" ';
            $i++;
        

         }   
    }

    if(!empty($sql)){  
      $this->db->where($sql);  
    } 
       
  }
  

  private function all_like_queries($search){
    $this->db->where("(pm.name LIKE '%$search%' OR tp.product_code LIKE '%$search%' OR msqr.make_set_id LIKE '%$search%')");
  }

  public function transfer_to_sales_return_voucher(){
    if(!empty($_POST['voucher_no'])){
       $date=date('Y-m-d');
       $rp_id=array();

      foreach ($_POST['voucher_no'] as $key => $value) {
        $this->db->select('id');
        $this->db->from('department_ready_product');
        $this->db->where('voucher_no',$value);
        $result = $this->db->get()->row();
        $rp_id[]=$result->id;
        $this->db->set('status',1);
        $this->db->where('voucher_no',$value);
        $this->db->where('status',8);
        $this->db->set('sales_date',$date);
        $this->db->update('department_ready_product');
      }
           
      $_SESSION['rp_id']=$rp_id;
    }
    return get_successMsg();
  }



  
  
}//class
?>
