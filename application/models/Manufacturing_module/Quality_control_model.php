<?php

class Quality_control_model extends CI_Model {

  function __construct() {
    $this->table_name = "quality_control";
    $this->table_name1 = "generate_packing_list";
    parent::__construct();
  }
  public function validatepostdata($postdata){
    $error_array = array();
    $this->form_validation->set_message('is_natural_no_zero','Quantity should be greater than zero');
    $this->form_validation->set_message('check_qc_qnt','Quantity Exceeded');
    $data = array();
    $data['status']= 'success';
    $_POST['qc'] = array();
    //print_r($postdata);exit;
    foreach ($postdata['qc']['receive_product_id'] as $key => $value) {
      $_POST['quantity'] = $postdata['qc']['quantity'][$value];
      $_POST['weight'] = $postdata['qc']['weight'][$value];
      $_POST['hallmarking_quantity'] = $postdata['qc']['hallmarking_quantity'][$value];
      
      $_POST['receive_product_id'] = $value;
      /*echo '<pre>';
      print_r($_POST);
      echo '</pre>';*/
      $this->form_validation->set_rules($this->config->item('Manufacturing_quality_control_all_qc', 'admin_validationrules'));
        if ($postdata['qc']['hallmarking'][$value] == '1') {
            $this->form_validation->set_message('qnt_validation','Quantity must be less than total quantity');
            $_POST['net_wt'] = $postdata['qc']['net_wt'][$value];
            $_POST['hallmarking_quantity'] = $postdata['qc']['hallmarking_quantity'][$value];
            $this->form_validation->set_rules('hc_id', 'Hallmarking center', 'required');
            $this->form_validation->set_rules('hc_logo', 'Hallmarking Logo', 'required');
            $this->form_validation->set_rules('net_wt', 'Net Wt', 'required|greater_than_equal_to[0.1]');
            $this->form_validation->set_rules(
              'hallmarking_quantity', 'Qauntity',
                array('required','is_natural_no_zero',array('qnt_validation',
                  function($str){
                    if(($_POST['hallmarking_quantity'] > 0) && $_POST['quantity'] < $_POST['hallmarking_quantity']){
                      return false;
                    }else{
                      return true;
                    }
                  })
                )
              );
        }else{
            $_POST['hallmarking_quantity'] = $_POST['quantity'];
        }
        if($this->form_validation->run()===FALSE){
          $error_array['weight_'.$value] = strip_tags(form_error('weight'));
          $error_array['quantity_'.$value] = strip_tags(form_error('quantity'));
           if ($postdata['qc']['hallmarking'][$value] == '1') {
            $error_array['hc_id'] = strip_tags(form_error('hc_id'));
            $error_array['hc_logo'] = strip_tags(form_error('hc_logo'));
            $error_array['net_wt_'.$value] = strip_tags(form_error('net_wt'));
            $error_array['hallmarking_quantity_'.$value] = strip_tags(form_error('hallmarking_quantity'));
          }

          $data['status']= 'failure';
          $data['data']= '';
          $data['error'] = $error_array;
        }
    }
    return $data;
  }

   public function validateBomPostdata($postdata){
    $error_array = array();
    $data = array();
    $data['status']= 'success';
    $_POST['qc'] = array();
    foreach ($postdata['qc']['receive_product_id'] as $key => $value) {
      $_POST['department_id'] = $postdata['qc']['department_id'][$value];
      $_POST['bom_weight'] = $postdata['qc']['bom_weight'][$value]; 
      $_POST['bom_nt_wt'] = $postdata['qc']['bom_nt_wt'][$value];             
      $_POST['receive_product_id'] = $value;     
    
        $this->form_validation->set_message('check_qc_wt','Weight Exceeded');
        $this->form_validation->set_rules($this->config->item('Mfg_qc_ctr_all_qc_bom', 'admin_validationrules'));   
        $this->form_validation->set_message('wt_validation','Net Weight must be less than  Weight');

        $this->form_validation->set_rules(
      'bom_nt_wt', 'Net Weight',
        array('required','greater_than_equal_to[0.1]',array('wt_validation',
          function($str){
            if(($_POST['bom_weight'] > 0) && $_POST['bom_weight'] < $_POST['bom_nt_wt']){
              return false;
            }else{
              return true;
            }
          })
        )
      );       

    
      if($this->form_validation->run()===FALSE){
        $error_array['bom_weight_'.$value] = strip_tags(form_error('bom_weight'));
        $error_array['bom_nt_wt_'.$value] = strip_tags(form_error('bom_nt_wt')); 
        $data['status']= 'failure';
        $data['data']= '';
        $data['error'] = $error_array;
      }
    }
    return $data;
  }

  public function validate_rejected_data($id){
    $data['status']= 'success';
    $error_array = array();
    $this->form_validation->set_rules($this->config->item('validate_rejected_data', 'admin_validationrules'));
    if($this->form_validation->run()===FALSE){
      $error_array['quantity_'.$id] = strip_tags(form_error('quantity'));
      $error_array['weight_'.$id] = strip_tags(form_error('weight'));
      $data['status']= 'failure';
      $data['data']= '';
      $data['error'] = $error_array;
    }
    return $data;
  }


  public function validate_rejected_bom_data($id){
    $data['status']= 'success';
    $error_array = array();
    $this->form_validation->set_rules($this->config->item('validate_rejected_bom_data', 'admin_validationrules'));
    if($this->form_validation->run()===FALSE){
      $error_array['rejected_weight_'.$id] = strip_tags(form_error('weight'));
      $data['status']= 'failure';
      $data['data']= '';
      $data['error'] = $error_array;
    }
    return $data;
  }
  public function get($filter_status='',$status='',$params='',$search='',$limit='',$hallmarking ='',$department_id=''){
    // $this->db->select('qc.id,qc.receipt_code,qc.status,qc.quantity qc_quantity,mo.id order_id,mo.order_date,km.name karigar_name,w.from_weight,w.to_weight,pm.name sub_cat_name,mom.product_code,sum(tp.quantity) tp_quantity,IF(qch.status = "7","7","0") qch_status');
    $this->db->select('qc.id,qc.receipt_code,qc.status,qc.quantity qc_quantity,mo.id order_id,mo.order_date,km.name karigar_name,km.id karigar_id,w.from_weight,w.to_weight,pm.name sub_cat_name,mom.product_code,qc.department_id,qc.weight as qc_weight,kmm.receive_weight,qc.hc_id,qc.net_wt as net_wt');
    $this->db->from('quality_control qc');
    //$this->db->join('quality_control_hallmarking qch','qch.qc_id = qc.id','left');
    $this->db->join('Receive_products rp','rp.id=qc.receive_product_id');
    $this->db->join('Karigar_manufacturing_mapping kmm','rp.kmm_id=kmm.id');
    $this->db->join('manufacturing_order_mapping mom','mom.id=kmm.mop_id');
    $this->db->join('manufacturing_order mo','mo.id=mom.manufacturing_order_id');
    $this->db->join('parent_category pm','pm.id = mom.parent_category_id');
    $this->db->join('karigar_master km','km.id = kmm.karigar_id');
    $this->db->join('weights w','w.id = mom.weight_range_id');
    $this->db->join('tag_products tg','tg.qc_id = qc.id','left');
    $this->db->where('qc.module','2');
    if(!empty($department_id)){
      $this->db->where('qc.department_id',$department_id);
    }

    if($department_id =='2' || $department_id=='3' || $department_id =='6' || $department_id=='10'){
      $this->db->where('qc.weight > 0');
    }else{
      $this->db->where('qc.quantity > 0');
    }
    if($_GET['status'] == 'complete'){
      $table_col_name='manufacturing_quality_control';
      $this->db->where("(tg.status='0' or qc.status='8')");
      //$this->db->where("qc.status",'7');
      //$this->db->having("qch_status = '7'");
    }
    if($_GET['status'] == 'rejected'){
      $this->db->where("qc.status",'0');
        if($department_id =='2' || $department_id=='3' || $department_id =='6' || $department_id=='10'){
           $table_col_name='mfg_qc_ctl_reje_bom_orders';
      
      }else{
          $table_col_name='manufacturing_quality_control_rejected_orders';
        
      }

    }
    if(!empty($id)){
      $this->db->where('qc.id',$id);
    }
    /*if(!empty($search)){
      $this->all_like_queries($search);
    }*/
    if(isset($params['columns']) && !empty($params['columns'])){
      $filter_input=$params['columns'];
      $this->get_filter_value($filter_input,$table_col_name);
    }   
    $this->db->group_by('qc.id');
    if($limit == true){
       $this->db->order_by('qc.id','DESC');
       if(!empty($params)){
           $this->db->limit($params['length'],$params['start']);
       }
      $result = $this->db->get()->result_array();
      //echo $this->db->last_query();print_r($result);exit;

    }else{
      $row_array = $this->db->get()->result_array();
      $result = count($row_array);
      //echo $this->db->last_query();print_r($result);exit;
    }
    

 // echo $this->db->last_query();print_r($result);exit;
    
    return $result;
  }
  private function get_filter_value($filter_input,$table_col_name){
    $column_name=array();  
    $filter_column_name=filter_column_name($table_col_name);
    $sql='';
    $i=0;
     
     //print_r($filter_input);die;
    foreach ($filter_input as $key => $search_value){
       $column_name=$filter_column_name;
       //print_r($search_value['search']['value']);
        if(!empty($search_value['search']['value'])){
          if($i != 0){
            $sql.=' AND  ';
          }
            $sql.=''.$column_name[$key].' like "%'.$search_value['search']['value'].'%" ';
            $i++;
        

         }   
    }

    if(!empty($sql)){  
      $this->db->where($sql);  
    } 
       
  }
  public function store($insert_array){
    $insert_array['module'] = '2';
    $recieved_qc_qnty = $this->get_quantity_by_rp_id($insert_array['receive_product_id']);
    $recieved_rp_qnty = $this->Receive_products_model->find($insert_array['receive_product_id']);
    $status = $this->get_error_status($recieved_qc_qnty,$recieved_rp_qnty,$insert_array);
    if($status == true){
      $insert_array['user_id'] = $this->session->userdata('user_id');
      if(isset($insert_array['rejected']) && $insert_array['rejected'] == true){
        $insert_array['status'] = '0';
        unset($insert_array['rejected']);
      }else{
        $insert_array['status'] = '8';
      }
      $insert_array['created_at'] = date('Y-m-d H:i:s');
      $insert_array['remaining_qty_tag']=$insert_array['quantity'];
      if($this->db->insert($this->table_name,$insert_array)){
        $inserted_id = $this->db->insert_id();
        if($this->get_quantity_by_rp_id($insert_array['receive_product_id']) == $recieved_rp_qnty['quantity']){
           $this->Receive_products_model->update(array('receive_product_id'=>$insert_array['receive_product_id'],'status'=>'5'));
        }
        
        $result_data['status']='success';
        $result_data['receive_product_id']  = $insert_array['receive_product_id'];
        $result_data['qc_id'] = $inserted_id;
        return $result_data;
      }else{
        return get_errorMsg();
      }
    }    
  }
  
  public function store_all($insert_array){
   //print_r($insert_array);die;
    if(isset($insert_array['rejected']) && $insert_array['rejected'] == true ){
      $insert_array['status'] = '0';
      unset($insert_array['rejected']);
    }else{
      $insert_array['status'] = '8';
    }
    $this->session->unset_userdata($_SESSION['hm_id']);
    $iteration = $insert_array;
   // print_r($iteration);die;
    $insert_tag = false;
    foreach ($iteration['qc']['receive_product_id'] as $key => $value) {
      unset($insert_array['qc']);
      $recieved_details = $this->Received_orders_model->find($value);
      $insert_array['receipt_code'] = $recieved_details['receipt_code'];
      $insert_array['quantity'] = $iteration['qc']['quantity'][$value];
      //$insert_array['karigar_id'] = $iteration['qc']['karigar_id'][$value];
      $insert_array['karigar_id'] = $insert_array['karigar_id'];
     // $insert_array['weight'] = $iteration['qc']['gross_wt'][$value];
      if($insert_array['department_id']=='2' || $insert_array['department_id']=='3' || $insert_array['department_id'] =='6' || $insert_array['department_id']=='10'){
        $insert_array['net_wt'] = $iteration['qc']['bom_nt_wt'][$value];
        $insert_array['weight'] = $iteration['qc']['bom_weight'][$value];
      }else{
        $insert_array['weight'] = $recieved_details['gross_wt'];
        $insert_array['remaining_qty_tag']=$insert_array['quantity'];
        $insert_array['net_wt'] = $iteration['qc']['weight'][$value];
      }
      $insert_array['product_code'] = $iteration['qc']['product_code'][$value];
      $insert_array['hm_quantity'] = $iteration['qc']['hallmarking_quantity'][$value];
      $insert_array['hm_net_wt'] = $iteration['qc']['net_wt'][$value];
      
      $insert_array['module'] = '2';
      $insert_array['user_id'] = $this->session->userdata('user_id');
     
      if(isset($iteration['qc']['hallmarking'][$value]) && $iteration['qc']['hallmarking'][$value] == true  && $insert_array['status'] !=='0'){
        $insert_array['status'] = '7';
         $insert_tag =true;
        $h_qty = $iteration['qc']['hallmarking_quantity'][$value];
      }else if ($insert_array['status'] =='0') {
        $insert_array['status'] ='0';/*rejected QC*/
        
      }else{
        $insert_array['status'] = '8';
        $insert_tag =true;
      }
    
      $insert_array['created_at'] = date('Y-m-d H:i:s');
      $insert_array['receive_product_id'] = $value;
      $insert_array['department_id']=$insert_array['department_id'];
      $receive_product_id = $insert_array['receive_product_id'];
      $department_id =$insert_array['department_id'];

   //print_r($insert_array);die;
  /*    if()
    print_r($insert_array);die*/;
      // unset($insert_array['hm_net_wt']);
      if($this->db->insert($this->table_name,$insert_array)){
        $qc_id = $this->db->insert_id();        
        if($insert_tag == true){
            if($department_id == '0'  || $department_id == '1'
               ||  $department_id == '5'  ||  $department_id == '7'  ||  $department_id == '8' || $department_id == '11')
            {
              $this->insert_into_tags($insert_array,$qc_id);
            }else{
              $this->product_from_qc($insert_array,$qc_id);
          }    
      }

        if(isset($iteration['qc']['hallmarking'][$value]) && $iteration['qc']['hallmarking'][$value] == true && $insert_array['status'] !=='0'){
          $hm_insert = $insert_array;
          $hm_insert['net_wt'] = $insert_array['hm_net_wt'];
          $hm_insert['status'] = '7';
          $hm_insert['quantity'] = $h_qty;
          $hm_insert['qc_id'] = $qc_id;
          unset($hm_insert['hm_quantity']);
          unset($hm_insert['hm_net_wt']);
          unset($hm_insert['weight']);
          unset($hm_insert['receive_product_id']);
          unset($hm_insert['remaining_qty_tag']);
          //unset($hm_insert['department_id']);
          unset($hm_insert['product_code']);          
        //     echo '<pre>';
        // print_r($insert_array);
        // echo '</pre>';
          $this->db->insert('quality_control_hallmarking',$hm_insert);
          $hallmarking_id[] = $this->db->insert_id();
          }
        if($department_id == '2' || $department_id=='3' || $department_id =='6' || $department_id=='10') {
               if($this->get_weight_by_rp_id($receive_product_id) ==  $recieved_details['bom_weight']){
               $this->Receive_products_model->update(array('receive_product_id'=>$receive_product_id,'status'=>'5'));
            }
        }else{
            if($this->get_quantity_by_rp_id($receive_product_id) ==  $recieved_details['quantity']){
               $this->Receive_products_model->update(array('receive_product_id'=>$receive_product_id,'status'=>'5'));
            }
        }

      }
      unset($insert_array['qc_id']);
    }
   

    if(!empty($hallmarking_id)){
      $_SESSION['hm_id'] = $hallmarking_id;
      echo json_encode(array('status'=>'hallmarking'));die;
    }else{

      return get_successMsg(); 
    }
       
  }
  
  private function get_error_status($recieved_qc_qnty,$recieved_rp_qnty,$insert_array){
    if($recieved_rp_qnty['quantity'] < ($recieved_qc_qnty + $insert_array['quantity'])){
      $data['status']= 'failure';
      $data['data']= '';
      $data['error'] = array('quantity'=>'Quantity Exceeded');
      echo json_encode($data);die;
    }elseif ($insert_array['quantity'] == 0) {
      $data['status']= 'failure';
      $data['data']= '';
      $data['error'] = array('quantity'=>'Quantity should be greater than zero');
      echo json_encode($data);die;
    }
    return true;
  }

  private function where_cond($hallmarking){
    if($hallmarking == true)
    $this->db->where('eo.corporate','1');
    if(!empty($_GET['status']) && $_GET['status'] == 'all' && $hallmarking == false){
      $this->db->where('rp.status','2');
    }elseif(!empty($_GET['status']) && $_GET['status'] == 'pending' && $hallmarking == false){
      $this->db->where('rp.status','1');
    }elseif(!empty($_GET['status']) && $_GET['status'] == 'hallmarking_pending' && $hallmarking == false){
      $this->db->where('rp.status','3');
    }elseif(!empty($_GET['status']) && $_GET['status'] == 'complete' && $hallmarking == false){
      $this->db->where('qc.status','1');
    }elseif(!empty($_GET['status']) && $_GET['status'] == 'rejected' && $hallmarking == false){
      $this->db->where('qc.hallmarking is null');
      $this->db->where('qc.status','0');
      $this->db->or_where('qc.status','5');
    }elseif(!empty($_GET['status']) && $_GET['status'] == 'amended' && $hallmarking == false){
      $this->db->where('rp.status','5');
    }elseif(!empty($_GET['status']) && $_GET['status'] == 'amended' && $hallmarking == true){
      $this->db->where('rp.status','5');
    }elseif(!empty($_GET['status']) && $_GET['status'] == 'send' && $hallmarking == true){
      $this->db->where('qc.status','2');
    }elseif(!empty($_GET['status']) && $_GET['status'] == 'pending' && $hallmarking == true){
      $this->db->where('qc.status','1');
    }elseif(!empty($_GET['status']) && $_GET['status'] == 'receive' && $hallmarking == true){
      $this->db->where('qch.status','3');
    }elseif(!empty($_GET['status']) && $_GET['status'] == 'rejected' && $hallmarking == true){
      $this->db->where('rp.status','0');
      $this->db->where('qc.hallmarking is not null');
    }
  }
  private function join_cond($hallmarking){
    if(!empty($_GET['status']) && $_GET['status'] == 'pending' && $hallmarking == false){
      $this->db->join('Receive_products rp','rp.corporate_product_id = qc.corporate_product_id','right');
      return ;
    }
    $this->db->join('Receive_products rp','rp.id = qc.receive_product_id');
    $this->db->join('corporate_products cp','cp.id = qc.corporate_product_id');
    $this->db->join('email_orders eo','eo.order_id = cp.order_id');
    $this->db->join('corporate corp','corp.id = eo.corporate');
    $this->db->join('quality_control_hallmarking qch','qc.id = qch.qc_id','left');
  }
  public function send_to_hallmarking($qc_id){
    $date = date('Y-m-d');
    // $this->db->set('HM_send_date',$date);
    // $this->db->set('status','9');
    $update_array = array('HM_send_date' => $date,
                          'status'=>'9',
                          'hc_id'=>$_POST['hallmarking_center_id'],
                          'hc_logo'=>$_POST['hallmarking_center_logo'],
                          'total_gr_wt'=>$_POST['gross_weight'],
                          );
    $this->db->where('id',$qc_id);
    $this->db->where('status!=','0');
    if($this->db->update($this->table_name,$update_array)){
      // $this->db->select('receive_product_id');
      // $this->db->from($this->table_name);
      // $this->db->where('id',$qc_id);
      // $result = $this->db->get()->row_array();
      // $this->Receive_products_model->update(array('receive_product_id'=>$result['receive_product_id'],'status'=>'3'));
      return get_successMsg();
    }else{
      return get_errorMsg();
    }
  }
  public function find($rp_id){
    $this->db->where('receive_product_id',$rp_id);
    $this->db->where('status!=','0');
    return $this->db->get($this->table_name)->row_array();
  }
  public function find_by_qc_id($qc_id){
    $this->db->select('net_wt,department_id,if(hm_quantity, (quantity - hm_quantity),quantity) as quantity,(CASE WHEN gross_wt is NULL THEN weight  ELSE  gross_wt END) as gross_wt');
    $this->db->where('id',$qc_id);
    $this->db->from($this->table_name);
    $result = $this->db->get()->row_array();
    return $result;
   
  }

  
  public function send_to_prepare_order($corporate_product_id){
    // $this->db->set('status','0');
    // $this->db->where('id',$corporate_product_id);
    // if($this->db->update('corporate_products')){

    // $this->db->where('corporate_product_id',$corporate_product_id);
    // $this->db->delete($this->table_name);
    // $this->db->where('corporate_product_id',$corporate_product_id);
    // $this->db->delete('Receive_products');
    //   return get_successMsg();
    // }else{
    //   return get_errorMsg();
    // }
}
  private function all_like_queries($search){
    $this->db->where("(mo.id LIKE '%$search%' OR pm.name LIKE '%$search%' OR rp.receipt_code LIKE '%$search%'OR km.name LIKE '%$search%')");
  }
   public function export_qc_products($filter_status='',$status='',$params='',$search='',$limit='',$hallmarking =''){
    
    $this->db->select('cp.work_order_id as WO_Id,"" WO_Srl,cp.Item_number as Item_Id,qc.quantity as PCS,cp.sort_Item_number as Ext_Item_Id,am.description as Article_Desc,qc.total_gr_wt as g_wt');
    $this->db->from($this->table_name.' qc');
    $this->db->join('corporate_products cp','cp.id=qc.corporate_product_id','left');
    $this->db->join('Receive_products rp','rp.corporate_product_id=qc.corporate_product_id','left');
    $this->db->join('product p','p.product_code = cp.sort_Item_number','left');
    $this->db->join('carat_master cm','cm.id = p.carat_id','left');
    $this->db->join('article_master am','am.id = p.article_id','left');
    $this->db->join('email_orders eo','eo.order_id = cp.order_id');
    $this->db->join('corporate corp','corp.id = eo.corporate');
    if(empty($_POST['product_code'])){
      $this->all_like_queries($search);
      $this->db->where('qc.status','3');
      $this->db->where('eo.corporate','1');
    }else{
      $this->export_where_conditions();
    }
    $result = $this->db->get()->result_array();
    return $result;
  }
  private function export_where_conditions(){
    $sql = '';
    foreach ($_POST['product_code'] as $key => $value) {
      if(is_array($value))
        $value = $value['value'];
      if($key == 0)
        $sql .= 'cp.id = '.$value;
      else
        $sql .= ' OR cp.id = '.$value;
    }
    $this->db->where($sql);
  }
  public function generate_packing_list()
  { 
    
    foreach ($_POST['corporate_product_id'] as $key => $value) {
      $pdata = $this->find($value['value']);
      $data['corporate_product_id'] = $pdata['corporate_product_id'];
      $this->db->insert($this->table_name1,$data);
      $this->db->set('status','');
      $this->db->where('corporate_product_id',$value['value']);
      $this->db->update($this->table_name);
    }
    return get_successMsg();  
  }
  public function get_remark($id){
    $this->db->where('id',$id);
    return $this->db->get($this->table_name)->row_array();
  }
  public function amend_product($qc_id,$receive_product_id)
  {
      $this->db->set('status','6');
      $this->db->where('id',$qc_id);
      $this->db->update($this->table_name);
      $this->db->set('status','5');
      $this->db->where('id',$receive_product_id);
      $this->db->update('Receive_products');
      
  }
  public function get_quantity($qc_id){
    $this->db->select('quantity');
    $this->db->from($this->table_name);
    $this->db->where('id',$qc_id);
    return $this->db->get()->row_array();
  }
  private function get_quantity_by_rp_id($rp_id){
    $this->db->select('sum(qc.quantity) qc_quantity');
    $this->db->from($this->table_name.' qc');
    // $this->db->join('quality_control_hallmarking qch','qch.qc_id = qc.id','left');
    $this->db->where('qc.receive_product_id',$rp_id);
    $result = $this->db->get()->row_array();
    return $result['qc_quantity'];
  }

  private function get_weight_by_rp_id($rp_id){
    $this->db->select('sum(qc.weight) qc_weight');
    $this->db->from($this->table_name.' qc');
    $this->db->where('qc.receive_product_id',$rp_id);
    $result = $this->db->get()->row_array();
    return $result['qc_weight'];
  }

  public function get_karigar_id($cp_id){
    $this->db->select('karigar_id');
    $this->db->from('Prepare_product_list');
    $this->db->where('corporate_products_id',$cp_id);
    $result = $this->db->get()->row_array();
    return $result['karigar_id'];
  }
  public function insert_in_prepared_order($result,$karigar_id){
    $insert_array = array(
        'corporate_products_id'=>$result['corporate_product_id'],
        'quantity'=>$result['quantity'],
        'karigar_id'=>$karigar_id,
        'qc_rejected_id'=>$result['id'],
        'status'=>'0',
        'created_at'=>date('Y-m-d H:i:sa'),
        'updated_at'=>date('Y-m-d H:i:sa'),
      );
    if($this->db->insert('Prepare_product_list',$insert_array)){
      return true;
    }else{
      return false;
    }
  }
  public function update($array,$where){
    $this->db->where($where);
    $this->db->update($this->table_name,$array);
  }
  public function get_engage_karigars($department_id='')
  {
    $this->db->select('distinct(kmm.karigar_id) as karigar_id,km.name as karigar_name,pm.name,mop.parent_category_id,mop.id as mop_id,mop.manufacturing_order_id as order_id,mop.product_code as product_code,kmm.quantity,pm.few_box');
    $this->db->from("Karigar_manufacturing_mapping kmm");
    $this->db->join('manufacturing_order_mapping mop','mop.id=kmm.mop_id','left');
    $this->db->join('parent_category pm','pm.id = mop.parent_category_id');;
    $this->db->join('karigar_master km','km.id=kmm.karigar_id','left');
    $this->db->where("kmm.status",'1');
    if(!empty($department_id)){
      $this->db->where('kmm.department_id',$department_id);
    }
    $this->db->group_by("kmm.karigar_id");
    $result = $this->db->get()->result_array();
    return $result;
  }
  public function receive_product($id,$department_id=""){
    $this->db->select('kmm.karigar_id as karigar_id,km.name as karigar_name,pm.name,mop.parent_category_id,mop.id as mop_id,mop.manufacturing_order_id as order_id,mop.product_code as product_code,(kmm.quantity - kmm.receive_quantity) quantity,kmm.id as kmm_id,w.from_weight,w.to_weight,kmm.approx_weight,kmm.department_id,d.tagging_status,pm.few_box,(kmm.total_weight - kmm.receive_weight) as weight,km.wastage as wastage');
    $this->db->from("Karigar_manufacturing_mapping kmm");
    $this->db->join('manufacturing_order_mapping mop','mop.id=kmm.mop_id','left');
    $this->db->join('parent_category pm','pm.id = mop.parent_category_id');
    $this->db->join('karigar_master km','km.id=kmm.karigar_id','left');
    $this->db->join('departments d','d.id=kmm.department_id','left');
    $this->db->join('weights w','w.id = mop.weight_range_id');
    $this->db->where("kmm.status",'1');
    $this->db->where("kmm.karigar_id",$id);
    if(!empty($department_id)){
      $this->db->where('kmm.department_id',$department_id);
    }
    $result = $this->db->get()->result_array();
    return $result;
  }
  private function receive_product_details_by_id($id){
    $this->db->select('kmm.karigar_id as karigar_id,km.name as karigar_name,pm.name,mop.parent_category_id,mop.id as mop_id,mop.manufacturing_order_id as order_id,mop.product_code as product_code,(kmm.quantity - kmm.receive_quantity) quantity,kmm.id as kmm_id,w.from_weight,w.to_weight,(kmm.total_weight - kmm.receive_weight) as weight,kmm.department_id,kmm.total_weight');
    $this->db->from("Karigar_manufacturing_mapping kmm");
    $this->db->join('manufacturing_order_mapping mop','mop.id=kmm.mop_id','left');
    $this->db->join('parent_category pm','pm.id = mop.parent_category_id');;
    $this->db->join('karigar_master km','km.id=kmm.karigar_id','left');
    $this->db->join('weights w','w.id = mop.weight_range_id');
    $this->db->where("kmm.id",$id);
    $result = $this->db->get()->row_array();
    return $result;
  }
  
  public function create_receipt_code(){
    $receipt_code = "MNFREC".mt_rand();
    $i = 0;

    foreach ($_POST['kr'] as $key => $value) {
      $net_wt[$key]=$value['net_wt']; 
        if(!empty($net_wt[$key])){
        $insert['date'] = date('Y-m-d');
        $insert['module'] = '2';
        $insert['status'] = '7';
        $insert['quantity'] = $value['quantity'];        
        $insert['kmm_id'] = $key;
        $data = $this->Karigar_order_model->get_department_id($key);
        $insert['order_id'] = $data['id'];
        $insert['department_id'] = $value['department_id'];
        $insert['karigar_id'] = $_POST['kariger_id'];
        $insert['receipt_code'] = $receipt_code;
        $insert['net_wt'] = $value['net_wt'];
        $insert['wastage'] = $value['wastage'];
        $insert['pure'] = $value['pure'];
         if($value['department_id'] !='2' && $value['department_id']!='3' && $value['department_id'] !='6' && $value['department_id']!='10'){
          $insert['gross_wt'] = $value['gross_wt'];
          $insert['weight'] = @$value['gross_wt'];
         }else{
          $insert['gross_wt'] = $value['weight'];
          $insert['weight'] = @$value['weight'];
         }
        $insert['amount'] = $value['amount'];        
        $insert['Product_name'] = $value['product_name']; 
        $insert['tagging_status'] = $value['tagging_status'];
        $insert['few_wt'] = @$value['few_wt'];
        $insert['stone_wt'] = @$value['stone_wt'];
                 
        $insert['is_skip_qc'] = (isset($value['is_skip_qc']) && !empty($value['is_skip_qc'])) ? $value['is_skip_qc'] : '0';
       /* print_r($insert['is_skip_qc']);
         print_r($value['tagging_status']);die;exit;*/
        $this->db->insert('Receive_products',$insert);
        $this->store_receive_products($insert,$this->db->insert_id());
        if(!empty($insert['few_wt'])){
            $this->update_few_wt_box($insert['Product_name'],$insert['few_wt']);
        }
   

        if($value['department_id'] !='2' && $value['department_id']!='3' && $value['department_id'] !='6' && $value['department_id']!='10'){
            $kmm_res = $this->check_quantity($key,$value['quantity']);
            if($kmm_res['status'] = 'success'){
              $this->db->where('id',$key);
              $this->db->set('receive_quantity',$kmm_res['receive_quantity']);
              if($kmm_res['is_update'] == true){
                $this->db->set('status','2');
              }
              $this->db->update('Karigar_manufacturing_mapping');
            }
        }else{
            $kmm_res_w = $this->check_weight($key,$value['weight']);              
            if($kmm_res_w['status'] = 'success'){
              $this->db->where('id',$key);
              $this->db->set('receive_weight',$kmm_res_w['receive_weight']);
              if($kmm_res_w['is_update'] == true){
                $this->db->set('status','2');
              }
              $this->db->update('Karigar_manufacturing_mapping');
            }
        }

        $pdf_data[$i] = $insert;
        $kmm_data = $this->receive_product_details_by_id($key);
        $pdf_data[$i]['kmm_id'] = $key;
        $pdf_data[$i]['name'] = $kmm_data['name'];
        $pdf_data[$i]['karigar_name'] = $kmm_data['karigar_name'];
        $pdf_data[$i]['quantity'] = $value['quantity'];   
        $pdf_data[$i]['weight'] = $value['weight'];       
        $pdf_data[$i]['weights'] = $kmm_data['from_weight'] .'-'.$kmm_data['to_weight'];
        $pdf_data[$i]['from_weight'] = $kmm_data['from_weight'];
        $pdf_data[$i]['to_weight'] = $kmm_data['to_weight'];
        $pdf_data[$i]['total_weight'] = $value['total_weight']; 
        $pdf_data[$i]['department_id'] = $value['department_id'];
        $pdf_data[$i]['stone_wt'] = $value['stone_wt'];
        $i++;
      }
    }
     return $pdf_data;
    }
    public function store_receive_products($data,$rp_id){
      //print_r($rp_id);die;
      $insert_array =array(
          'user_id'=>$this->session->userdata('user_id'),
          'quantity'=>$data['quantity'],
          'karigar_id'=>$data['karigar_id'],          
          'gross_wt'=>$data['gross_wt'],
          'weight'=>$data['gross_wt'],
          'net_wt'=>$data['net_wt'],   
          'few_wt'=>$data['few_wt'],  
          'department_id'=>$data['department_id'],      
          'is_skip_qc'=>'1',
          'module'=>'2',
          'status'=>'8',
          'receive_product_id'=>$rp_id,
          'receipt_code'=>$data['receipt_code'],
          'qc_net_wt_max'=>$data['net_wt'] + (5 / 100) * $data['net_wt'],
          'qc_net_wt_min'=>$data['net_wt'] - (5 / 100) * $data['net_wt'],
          'remaining_net_wt'=>$data['net_wt'],
        );
        if($this->db->insert('quality_control',$insert_array)){
          //echo $this->db->last_query();
        $this->insert_into_tags($insert_array,$this->db->insert_id());
        }

    }

  public function update_few_wt_box($product_name,$few_weight){        
        $this->db->set('weight', 'weight - ' . (float) $few_weight, FALSE);
        $this->db->where("product_name",$product_name);
        $this->db->update('few_weight');
 

    }

  public function product_from_recive_store($data,$rp_id){  
    $product_code = $data['Product_name'].mt_rand();
    if($data['department_id']=='2' || $data['department_id']=='3' || $data['department_id'] =='6' || $data['department_id']=='10'){
         $insert_drp_array = array(    
        'quantity' =>@$data['quantity'],
        'gr_wt' =>$data['weight'],
        'net_wt' =>$data['net_wt'],
        'receive_product_id'=>$rp_id,
        'department_id'=>$data['department_id'],
        'product_code'=>$product_code,        
        'product'=>$data['Product_name'],
        'karigar_id'=>$data['karigar_id'],
        'chitti_no'=>$data['receipt_code'],
        'status'=>'0',
        'product_from'=>'1',
        'created_at' =>date('Y-m-d H:i:s'),
        'updated_at' =>date('Y-m-d H:i:s'),
      );
    }else{
      $insert_drp_array = array(    
        'quantity' =>@$data['quantity'],
        'weight' =>@$data['weight'],
        'gr_wt' =>$data['gross_wt'],
        'net_wt' =>$data['net_wt'],
        'receive_product_id'=>$rp_id,
        'department_id'=>$data['department_id'],
        'product_code'=>$product_code,
        'product'=>$data['Product_name'],
        'karigar_id'=>$data['karigar_id'],
        'chitti_no'=>$data['receipt_code'],
        'status'=>'0',
        'product_from'=>'1',
        'created_at' =>date('Y-m-d H:i:s'),
        'updated_at' =>date('Y-m-d H:i:s'),
      );
    }
    $this->db->insert('department_ready_product',$insert_drp_array);
  }
/*  public function insert_into_tags($inserted_data,$qc_id){
    $qc_data = $this->find_by_qc_id($qc_id);
    $details = $this->get_voucher_details($qc_id);
    $code = $this->Parent_category_model->get_category_codes($details['pm_id']);
   // print_r($code[0]['code_name']);die;
    //$max_count = $this->Tag_products_model->get_max_code($code[0]['code_name']);
 
    for($i=0;$i<$qc_data['quantity'];$i++){
      $data[] = array(
        'karigar_id'=>$details['karigar_id'],
        'sub_category_id'=>$details['pm_id'],
        'weight_range_id'=>$details['weight_range_id'],
        //'product_code'=>$code[0]['code_name'].'-'.($max_count+$i),
        'sub_code' => $code[0]['code_name'],
        //'sub_code_id' => ($max_count+$i),
        'quantity'=>1,
        'qc_id'=>$qc_id,
        'qc_net_wt_max'=>$qc_data['net_wt'] + (10 / 100) * $qc_data['net_wt'],
        'qc_net_wt_min'=>$qc_data['net_wt'] - (10 / 100) * $qc_data['net_wt'],
        'department_id'=>$qc_data['department_id'],
        );
    }
 
    $this->db->insert_batch('tag_products',$data);
  }*/
    public function insert_into_tags($inserted_data,$qc_id){
    $qc_data = $this->find_by_qc_id($qc_id);
    $details = $this->get_voucher_details($qc_id);
    //print_r($details['pm_id']);
    $code = $this->Parent_category_model->get_category_codes($details['pm_id']);
       //echo $this->db->last_query();
    //print_r($details);
 //print_r($code);die;
    //print_r($code[0]['code_name']);die;
   // print_r($code[0]['code_name']);die;
    //$max_count = $this->Tag_products_model->get_max_code($code[0]['code_name']);
   /* $qc_data = $this->find_by_qc_id($qc_id);*/
    // $qc_quantity=$qc_data['quantity']-$qc_data['hm_quantity'];    
    //print_r($qc_data);die; 
   //print_r($inserted_data);die;
    $qc_quantity=$qc_data['quantity']; 
    for($i=0;$i<$qc_quantity;$i++){
      $data[] = array(
        'karigar_id'=>$details['karigar_id'],
        'sub_category_id'=>$details['pm_id'],
        'weight_range_id'=>$details['weight_range_id'],
        //'product_code'=>$code[0]['code_name'].'-'.($max_count+$i),
        'sub_code' => $code[0]['code_name'],
        'sub_category_id'=>$code[0]['id'],
        //'sub_code_id' => ($max_count+$i),
        'quantity'=>1,
        'created_at'=>date('Y-m-d H:i:s'),
        'qc_id'=>$qc_id,
        'gr_wt'=>$qc_data['gross_wt'],  
        'net_wt'=>$qc_data['net_wt'],       
        // 'qc_net_wt_max'=>$qc_data['net_wt'] + (5 / 100) * $qc_data['net_wt'],
        // 'qc_net_wt_min'=>$qc_data['net_wt'] - (5 / 100) * $qc_data['net_wt'],
        'department_id'=>$qc_data['department_id'],
        );
    }
//print_r($data);die;
    if(!empty($data)){
    $this->db->insert_batch('tag_products',$data);
    }
  }

  private function product_from_qc($data,$qc_id){  
    //print_r($data);die;
    $product_code = $data['product_code'].mt_rand();
      $insert_drp_array = array(    
        'quantity' =>$data['quantity'],
        'gr_wt' =>$data['weight'],
        'net_wt' =>$data['net_wt'],
        'receive_product_id'=>$data['receive_product_id'],
        'qc_id'=>$qc_id,
        'department_id'=>$data['department_id'],
        'product_code'=>$product_code,
        'product'=>$data['product_code'],
        'weight' =>$data['weight'], 
        'chitti_no'=>$data['receipt_code'], 
        'karigar_id' =>$data['karigar_id'],      
        'status'=>'0',
        'product_from'=>'2',
        'created_at' =>date('Y-m-d H:i:s'),
        'updated_at' =>date('Y-m-d H:i:s'),
      );
    $this->db->insert('department_ready_product',$insert_drp_array);
  }

    public function check_quantity($id,$quantity){
      $this->db->select('quantity,receive_quantity');
      $this->db->where('id',$id);
      $this->db->where('status','1');
      $kmm_res = $this->db->get('Karigar_manufacturing_mapping')->row_array();
      if($kmm_res['receive_quantity'] + $quantity == $kmm_res['quantity']){
        $data['status'] = 'success';
        $data['is_update'] = true;
        $data['receive_quantity'] = $kmm_res['receive_quantity'] + $quantity;
      }else if($kmm_res['receive_quantity'] + $quantity < $kmm_res['quantity']){
        $data['status'] = 'success';
        $data['is_update'] = false;
        $data['receive_quantity'] = $kmm_res['receive_quantity'] + $quantity;
      }else if($kmm_res['receive_quantity'] + $quantity > $kmm_res['quantity']){
        $data['status'] = 'error';
        $data['error']['quantity'][$id] = 'Quantity exceeded';
      }
      return $data;
    }
    public function check_weight($id,$weight){
      $this->db->select('total_weight,receive_weight');
      $this->db->where('id',$id);
      $this->db->where('status','1');
      $kmm_res = $this->db->get('Karigar_manufacturing_mapping')->row_array();
      if($kmm_res['receive_weight'] + $weight == $kmm_res['total_weight']){
        $data['status'] = 'success';
        $data['is_update'] = true;
        $data['receive_weight'] = $kmm_res['receive_weight'] + $weight;
      }else if($kmm_res['receive_weight'] + $weight < $kmm_res['total_weight']){
        $data['status'] = 'success';
        $data['is_update'] = false;
        $data['receive_weight'] = $kmm_res['receive_weight'] + $weight;
      }else if($kmm_res['receive_weight'] + $weight > $kmm_res['total_weight']){
        $data['status'] = 'error';
        $data['error']['weight'][$id] = 'Weight exceeded';
      }

      return $data;
    }
    public function get_total_qty($ids){

      $this->db->select('SUM(quantity) as total_qty');
      $this->db->where_in('id',explode(',', $ids));
      $result = $this->db->get('quality_control')->row_array();
      return $result['total_qty'];
    }

    public function get_printdata($checked_ids){
      $this->db->select('qc.total_gr_wt,d.name as department_name,pm.name as sub_category,d.id department_id,mop.manufacturing_order_id,pm.id as sub_category_id,w.id weight_range_id,rp.karigar_id,w.from_weight,w.to_weight,mo.order_name,group_concat(mvm.product_variants_id) as p_variant,qc.weight as qc_weight ,d.id as dep_id');
      $this->db->from('quality_control qc');
      $this->db->join('Receive_products rp','qc.receive_product_id=rp.id','left');
      $this->db->join('departments d','rp.department_id=d.id','left');
      $this->db->join('Karigar_manufacturing_mapping kmm','rp.kmm_id=kmm.id','left');
      $this->db->join('manufacturing_order_mapping mop','kmm.mop_id=mop.id','left');
      $this->db->join('manufacturing_order mo','mo.id=mop.manufacturing_order_id','left');
      $this->db->join('manufacturing_order_variant_mapping mvm','mvm.mfg_order_map_id = mop.id','left');
      $this->db->join('product_variants pv','pv.id=mvm.product_variants_id','left');
      $this->db->join('weights w','w.id = mop.weight_range_id','left');
      $this->db->join('parent_category pm','pm.id = mop.parent_category_id','left');
      $this->db->where_in('qc.id',$checked_ids);
      $this->db->group_by('mvm.mfg_order_map_id');
      return $this->db->get()->result_array();
    }
/*  public function get_product_by_receipt_code($code){
    $this->db->select('rp.*,km.name karigar_name,pm.name as name,mop.manufacturing_order_id order_id,w.from_weight,w.to_weight,
      if(qc.quantity, (rp.quantity - sum(qc.quantity)),rp.quantity) as quantity,
      if(qc.weight, (rp.weight - sum(qc.weight)),rp.weight) as weight,rp.karigar_id as karigar_id,w.from_weight,w.to_weigh');
    $this->db->from('Receive_products rp');
    $this->db->join('departments d','rp.department_id=d.id','left');
    $this->db->join('Karigar_manufacturing_mapping kmm','rp.kmm_id=kmm.id','left');
    $this->db->join('manufacturing_order_mapping mop','kmm.mop_id=mop.id','left');
    $this->db->join('weights w','w.id = mop.weight_range_id');
    $this->db->join('parent_category pm','pm.id = mop.parent_category_id');
    $this->db->join('karigar_master km','km.id=rp.karigar_id');
    $this->db->join('quality_control qc','qc.receive_product_id=rp.id','left');
    $this->db->where('rp.receipt_code',$code);
    $this->db->group_by('rp.id');
    $result = $this->db->get()->result_array();    
    //$this->db->group_by('rp.receipt_code');

    return $result;
  }*/


  public function get_product_qc_by_receipt_code($code){
   $this->db->select('rp.*,km.name karigar_name,pm.name as name,mop.manufacturing_order_id order_id,w.from_weight,w.to_weight, if(qc.quantity, (rp.quantity - sum(qc.quantity)),rp.quantity) as quantity,if(qc.weight, (rp.weight - sum(qc.weight)),rp.weight) as weight');
   $this->db->from('Receive_products rp');
   $this->db->join('departments d','rp.department_id=d.id','left');
   $this->db->join('Karigar_manufacturing_mapping kmm','rp.kmm_id=kmm.id','left');
   $this->db->join('manufacturing_order_mapping mop','kmm.mop_id=mop.id','left');
   $this->db->join('weights w','w.id = mop.weight_range_id');
   $this->db->join('parent_category pm','pm.id = mop.parent_category_id');
   $this->db->join('karigar_master km','km.id=kmm.karigar_id','left');
   $this->db->join('quality_control qc','qc.receive_product_id=rp.id','left');
   $this->db->where('rp.receipt_code',$code);
   $this->db->where('rp.is_skip_qc !=','1');
   $this->db->group_by('rp.id');
   $result = $this->db->get()->result_array();

   return $result;
 }
   public function get_product_by_receipt_code($code){
   $this->db->select('rp.*,km.name karigar_name,pm.name as name,mop.manufacturing_order_id order_id,w.from_weight,w.to_weight, if(qc.quantity, (rp.quantity - sum(qc.quantity)),rp.quantity) as quantity,if(qc.weight, (rp.weight - sum(qc.weight)),rp.weight) as weight');
   $this->db->from('Receive_products rp');
   $this->db->join('departments d','rp.department_id=d.id','left');
   $this->db->join('Karigar_manufacturing_mapping kmm','rp.kmm_id=kmm.id','left');
   $this->db->join('manufacturing_order_mapping mop','kmm.mop_id=mop.id','left');
   $this->db->join('weights w','w.id = mop.weight_range_id');
   $this->db->join('parent_category pm','pm.id = mop.parent_category_id');
   $this->db->join('karigar_master km','km.id=kmm.karigar_id','left');
   $this->db->join('quality_control qc','qc.receive_product_id=rp.id','left');
   $this->db->where('rp.receipt_code',$code);
   $this->db->where('rp.is_skip_qc !=','1');
   $this->db->group_by('rp.id');
   $result = $this->db->get()->result_array();

   return $result;
 }
 
  public function get_qc_quantity($rp_id){
    $this->db->select('sum(quantity) quantity');
    $this->db->from('quality_control');
    $this->db->where('receive_product_id',$rp_id);
    $result = $this->db->get()->row_array();
    return $result['quantity'];
  }
  public function get_hallmarking_data($checked_ids){
      $this->db->select('qch.net_wt as hm_net_wt,qch.quantity,d.name as department_name,pm.name as sub_category,d.id department_id,mop.manufacturing_order_id,pm.id as sub_category_id,w.id weight_range_id,rp.karigar_id,w.from_weight,w.to_weight,qch.hc_logo,hc.name hc_name,qch.quantity as hm_qty,qc.weight as hm_gr_wt');
      $this->db->from('quality_control_hallmarking qch');
      $this->db->join('quality_control qc','qc.id = qch.qc_id');
      $this->db->join('Receive_products rp','qc.receive_product_id=rp.id','left');
      $this->db->join('departments d','rp.department_id=d.id','left');
      $this->db->join('Karigar_manufacturing_mapping kmm','rp.kmm_id=kmm.id','left');
      $this->db->join('manufacturing_order_mapping mop','kmm.mop_id=mop.id','left');
      $this->db->join('weights w','w.id = mop.weight_range_id');
      $this->db->join('parent_category pm','pm.id = mop.parent_category_id');
      $this->db->join('hallmarking_center hc','hc.id = qch.hc_id');
      $this->db->where_in('qch.id',$checked_ids);
      return $this->db->get()->row_array();
    }
    public function get_ammended_products($qc_id){
      $this->db->select('qc.quantity,sum(ap.quantity) as rejected_quantity');
      $this->db->from('quality_control qc');
      $this->db->join('Amend_products ap','ap.qc_id = qc.id','left');
      $this->db->where('qc.id',$qc_id);
      $this->db->where('ap.module',2);
      return $this->db->get()->row_array();
    }

    public function get_wt_products($qc_id){
      $this->db->select('qc.weight,sum(ap.weight) as rejected_weight');
      $this->db->from('quality_control qc');
      $this->db->join('Amend_products ap','ap.qc_id = qc.id','left');
      $this->db->where('qc.id',$qc_id);
      $this->db->where('ap.module',2);
      return $this->db->get()->row_array();
    }

    public function get_ammended_list(){
      $this->db->select('ap.quantity as am_quantity,ap.rejected_weight,km.name karigar_name,pm.name as name,mop.manufacturing_order_id order_id,w.from_weight,w.to_weight,qc.receipt_code');
      $this->db->from('Amend_products ap');
      $this->db->join('quality_control qc','ap.qc_id = qc.id');
      $this->db->join('Receive_products rp','qc.receive_product_id=rp.id');
      $this->db->join('departments d','rp.department_id=d.id');
      $this->db->join('Karigar_manufacturing_mapping kmm','rp.kmm_id=kmm.id');
      $this->db->join('manufacturing_order_mapping mop','kmm.mop_id=mop.id');
      $this->db->join('weights w','w.id = mop.weight_range_id');
      $this->db->join('parent_category pm','pm.id = mop.parent_category_id');
      $this->db->join('karigar_master km','km.id=kmm.karigar_id');
      $this->db->where('ap.module',2);
      $result = $this->db->get()->result_array();
      return $result; 
    }
    public function insert_ammended_products($insert_array){
      $this->db->insert('Amend_products',$insert_array);
    }
    public function get_voucher_details($qc_id){
      $this->db->select('km.name karigar_name,pm.name as name,mop.manufacturing_order_id order_id,w.from_weight,w.to_weight,qc.receipt_code,km.id karigar_id,pm.id pm_id,w.id as weight_range_id');
      $this->db->from('quality_control qc');
      $this->db->join('Receive_products rp','qc.receive_product_id=rp.id','left');
      $this->db->join('departments d','rp.department_id=d.id');
      $this->db->join('Karigar_manufacturing_mapping kmm','rp.kmm_id=kmm.id','left');
      $this->db->join('manufacturing_order_mapping mop','kmm.mop_id=mop.id','left');
      $this->db->join('weights w','w.id = mop.weight_range_id','left');
      $this->db->join('parent_category pm','pm.id = mop.parent_category_id','left');
      $this->db->join('karigar_master km','km.id=kmm.karigar_id','left');
      $this->db->where('qc.id',$qc_id);  
      //$this->db->where('qc.is_skip_qc','1');       
      $result = $this->db->get()->row_array();
      //echo $this->db->last_query();print_r($result);die;
      return $result;
    }
  public function get_quantity_by_id($id){
    $this->db->select('sum(quantity) qnt');
    $this->db->from('quality_control_hallmarking');
    $this->db->where('qc_id',$id);
    $this->db->where('status','7');
    $result = $this->db->get()->row_array();
    //echo $this->db->last_query();print_r($result);die;
    return $result['qnt'];
  }  
}
