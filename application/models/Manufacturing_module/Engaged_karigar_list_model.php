<?php
class Engaged_karigar_list_model extends CI_Model {
  function __construct() {
      $this->table_name = "Prepare_product_list";
      parent::__construct();
  }
  public function get($filter_status='',$status='',$params='',$search='',$limit='',$karigar_id='',$department_id=''){
    if(empty($karigar_id))
      $this->db->select('kmm.karigar_id,mo.id order_id,DATE_FORMAT(mo.order_date, "%d-%m-%Y") as order_date,pm.name sub_cat_name,w.from_weight,w.to_weight,sum(kmm.quantity) quantity, sum(kmm.approx_weight) as approx_weight,km.name karigar_name,mom.product_code,kmm.created_at,GROUP_CONCAT(kmm.id) as kmm_ids,kmm.department_id,sum(kmm.total_weight) as total_weight ,mom.weight');
    else{
      $this->db->select('kmm.id kmm_id,DATE_FORMAT(kmm.created_at, "%d-%m-%Y '."||".' %T") as   
        karigar_engaged_date,DATE_FORMAT(kmm.delivery_date, "%d-%m-%Y") as delivery_date,pm.name,mo.id as order_id,kmm.quantity,kmm.approx_weight,w.from_weight,w.to_weight,km.name k_name,kmm.department_id,kmm.total_weight, mom.weight');
    }
    $this->db->from('Karigar_manufacturing_mapping kmm');
    $this->db->join('manufacturing_order_mapping mom','mom.id=kmm.mop_id');
    $this->db->join('manufacturing_order mo','mo.id=mom.manufacturing_order_id');
    $this->db->join('parent_category pm','pm.id = mom.parent_category_id');
    $this->db->join('weights w','w.id = mom.weight_range_id');
    $this->db->join('karigar_master km','km.id = kmm.karigar_id');
    // $this->db->join('Receive_products rp','rp.kmm_id = kmm.id','left');

    if(!empty($department_id)){
      $this->db->where('kmm.department_id',$department_id);
    }

    if(!empty($karigar_id)){
      $this->db->where('kmm.karigar_id',$karigar_id);
    }
    else{
      $this->db->group_by('kmm.karigar_id');
    }

    $this->db->where('kmm.status','1');
   /*  if(!empty($search)){
        $this->db->like('km.name',$search);  
    }*/

    $ci = &get_instance();
    $get_url = $ci->uri->segment(2);

    if(@$get_url =='reprint_order'){
       $table_col_name='manufacturing_details_of_engaged_karigar_list';
    }else{
       $table_col_name='Manufacturing_Engaged_karigar_list';
    }
    if(isset($params['columns']) && !empty($params['columns'])){
      $filter_input=$params['columns'];
      $this->get_filter_value($filter_input,$table_col_name);
    }

    if($limit == true){
      $this->db->order_by('kmm.id','DESC');
      $this->db->limit($params['length'],$params['start']);
      $result = $this->db->get()->result_array();
      //echo $this->db->last_query();print_r($result);exit;
    }else{
      $row_array = $this->db->get()->result_array();
      $result = count($row_array);
      //echo $this->db->last_query();print_r($result);exit;
    }
  // echo $this->db->last_query();print_r($result);exit;
    return $result;

  }

  private function get_filter_value($filter_input,$table_col_name){
    $column_name=array();  
    $filter_column_name=filter_column_name($table_col_name);
    $sql='';
    $i=0;
     
     //print_r($filter_input);die;
    foreach ($filter_input as $key => $search_value){
       $column_name=$filter_column_name;
       //print_r($search_value['search']['value']);
        if(!empty($search_value['search']['value'])){
          if($i != 0){
            $sql.=' AND  ';
          }
            $sql.=''.$column_name[$key].' like "%'.$search_value['search']['value'].'%" ';
            $i++;
        

         }   
    }

    if(!empty($sql)){  
      $this->db->where($sql);  
    } 
       
  }
  public function delete($id){
    $this->db->where('karigar_id',$id);
    $this->db->where('status','1');
    if($this->db->delete($this->table_name)){
      return get_successMsg();
    }else{
      return get_errorMsg();
    }
  }
  private function all_like_queries($search){
    $this->db->where("(`k`.`name` LIKE '%$search%')");
  }
  public function receive_quantity($kmm_ids){
    $this->db->select('rp.quantity quantity,w.from_weight,w.to_weight');
    $this->db->from('Receive_products rp');
    $this->db->join('Karigar_manufacturing_mapping kmm','kmm.id = rp.kmm_id');
    $this->db->join('manufacturing_order_mapping mom','mom.id = kmm.mop_id');
    $this->db->join('weights w','w.id = mom.weight_range_id');
    $this->db->where_in('kmm_id',array_map('intval', explode(',',$kmm_ids)));
    $result = $this->db->get()->result_array();
    $pcs = 0;
    $weight = 0;
    foreach ($result as $key => $value) {
      $pcs += $value['quantity']; 
      $weight += (($value['from_weight'] + $value['to_weight'])/2) * $value['quantity'];
    }
    return array('pcs'=>$pcs,'weight'=>$weight);
  }

    public function receive_weight($kmm_ids){
    $this->db->select('sum(rp.weight) weight,w.from_weight,w.to_weight, sum(kmm.total_weight) total_weight');
    $this->db->from('Receive_products rp');
    $this->db->join('Karigar_manufacturing_mapping kmm','kmm.id = rp.kmm_id');
    $this->db->join('manufacturing_order_mapping mom','mom.id = kmm.mop_id');
    $this->db->join('weights w','w.id = mom.weight_range_id');
    $this->db->where_in('kmm_id',array_map('intval', explode(',',$kmm_ids)));
    $result = $this->db->get()->result_array();
   // print_r($result);die;
    $receive_wt = 0;
    $weight = 0;
    foreach ($result as $key => $value) {
      $receive_wt += $value['weight']; 
      $weight +=  $value['total_weight']-$value['weight'];
    }
    return array('receive_wt'=>$receive_wt,'weight'=>$weight);
  }
}
