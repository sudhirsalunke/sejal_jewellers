<?php

class Prepare_kundan_karigar_order_model extends CI_Model {

  function __construct() {
  	$this->table_name = "prepare_kundan_karigar_order";
    parent::__construct();
  }
  public function get_department_id($msqr_id){
  	$this->db->select('rp.department_id,tp.sub_category_id order_id');
  	$this->db->from('make_set_quantity_relation msqr');
  	$this->db->join('tag_products tp','tp.id=msqr.tag_id');
  	$this->db->join('quality_control qc','qc.id=tp.qc_id');
  	$this->db->join('Receive_products rp','rp.receipt_code=qc.receipt_code');
  	$this->db->where('msqr.id',$msqr_id);
  	$result = $this->db->get()->row_array();
    //echo $this->db->last_query();print_r($result);die;
  	return $result;
  }
  public function store($postdata){
    $check_quantity = $this->check_quantity($postdata['quantity'],$postdata['msqr_id']);
    //print_r($postdata);die;
    if($this->db->insert($this->table_name,$postdata)){
      //deduct_msqr_quantity($postdata['msqr_id'],$postdata['quantity']);
      if($check_quantity == true){
    		$this->db->set('status','1');
    		$this->db->where('id',$postdata['msqr_id']);
    		$this->db->update('make_set_quantity_relation');
      }
  		return true;
  	}else{
  		return false;
  	}
  }
  public function check_quantity($quantity,$msqr_id,$validate=false){
    $this->db->select('sum(quantity) p_quantity');
    $this->db->from($this->table_name);
    $this->db->where('msqr_id',$msqr_id);
    $this->db->group_by('msqr_id');
    $p_result = $this->db->get()->row_array();
    $this->db->select('sum(tp.quantity) t_quantity');
    $this->db->from('make_set_quantity_relation msqr');
    $this->db->join('tag_products tp','tp.id=msqr.tag_id');
    $this->db->where('msqr.id',$msqr_id);
    $t_result = $this->db->get()->row_array();
    if(($p_result['p_quantity']+$quantity) == $t_result['t_quantity']){
      return true;
    }else{
      if($validate == true){
        if(($p_result['p_quantity']+$quantity) > $t_result['t_quantity'])
        {
          return false;
        }else{
          return true;
        }
      }else{
        return false;
      }
      
    }
  }
  public function get_pdf_details($msqr_ids){/*used in tag print dont changed*/
    $this->db->select('msqr.quantity,tp.product_code,tp.net_wt,msqr.make_set_id,tp.gr_wt as gross_wt,tp.image,msqr.tag_id,pm.name as s_name,km.name as karigar_name,msqr.pcs');
    $this->db->from('make_set_quantity_relation msqr');
    $this->db->join('tag_products tp','tp.id=msqr.tag_id');
    $this->db->join('quality_control qc','qc.id=tp.qc_id');
    //$this->db->join('parent_category pm','pm.id = tp.sub_category_id');
    $this->db->join('parent_category_codes pcc', 'pcc.code_name = tp.sub_code');
    $this->db->join('parent_category pm', 'pm.id = pcc.parent_category_id');
    $this->db->join('Receive_products rp','rp.receipt_code=qc.receipt_code');
    $this->db->join('prepare_kundan_karigar_order ppko','msqr.id=ppko.msqr_id');
    $this->db->join('karigar_master km','km.id=ppko.karigar_id');

    $this->db->group_by('msqr.id');
    $this->db->where_in('msqr.id',$msqr_ids);

    $result = $this->db->get()->result_array();
    return $result;
  }
  public function find($po_id){
    $this->db->select('po.karigar_id,km.name km_name,km_prev.name prev_k_name,tp.Product_code,tp.image,tp.net_wt,tp.product_code');
    $this->db->join('make_set_quantity_relation msqr','msqr.id = po.msqr_id');
    $this->db->join('tag_products tp','tp.id=msqr.tag_id');
    $this->db->join('parent_category_codes pcc', 'pcc.code_name = tp.sub_code');
    $this->db->join('parent_category pm', 'pm.id = pcc.parent_category_id');
    //$this->db->join('parent_category pm','pm.id = tp.sub_category_id');
    $this->db->join('karigar_master km','km.id=po.karigar_id','left');
    $this->db->join('karigar_master km_prev','km_prev.id=tp.karigar_id','left');
    $this->db->where('po.id',$po_id);
    return $this->db->get($this->table_name.' po')->row_array();
  }
}