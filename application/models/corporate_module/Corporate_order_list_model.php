  <?php
class Corporate_order_list_model extends CI_Model {
  function __construct() {
      $this->table_name = "Prepare_product_list";
      parent::__construct();
  }
public function get($filter_status='',$status='',$params='',$search='',$limit=''){
    $this->db->select('cp.order_id,GROUP_CONCAT(DISTINCT(k.name)  SEPARATOR ",") as name,GROUP_CONCAT(DISTINCT(k.id)  SEPARATOR ",") as k_id,count(cp.sort_Item_number) as no_of_design,sum(cp.quantity) as total_weight,cp.id as cp_id,cp.work_order_id,cp.email_order_id ,DATE_FORMAT(eo.date, "%d-%m-%Y") as order_date,c.name as corporate,sum(cp.quantity) as cp_weight,sum(cp.net_weight) as net_weight');
    $this->db->from($this->table_name.' ppl');
    $this->db->join('karigar_master k','k.id = ppl.karigar_id');
    $this->db->join('corporate_products cp','cp.id = ppl.corporate_products_id');
    $this->db->join('email_orders eo','eo.id =cp.email_order_id');
    $this->db->join('corporate c','c.id = eo.corporate');
    $this->db->where('ppl.status !=0');
    $sql='';  
  
    $i=0;
    if(!empty($params['columns'])){
        foreach ($params['columns'] as $key => $value ){
            if(!empty($value['search']['value']) && ($key == 0 OR $key == 1 OR $key == 2 OR $key == 3 OR $key == 5)){ 
              if(!empty($value['search']['value'])){
                if($i != 0){
                  $sql.=' AND ';
                }
                $sql.='(cp.work_order_id like "%'.$value['search']['value'].'%" '; 
                $sql.='OR k.name like "%'.$value['search']['value'].'%" '; 
                $sql.='OR DATE_FORMAT(eo.date, "%d-%m-%Y") like "%'.$value['search']['value'].'%" ';
                $sql.='OR cp.sort_Item_number like "%'.$value['search']['value'].'%" ';
                $sql.='OR c.name like "%'.$value['search']['value'].'%")';   
                
                $i++;

            }  
          } 
        }
    }

  if(!empty($sql)){  
    $this->db->where($sql);  
  }   

      $this->db->group_by('ppl.order_name');
    if($limit == true){
      $this->db->order_by('ppl.id','DESC');
      $result = $this->db->get()->result_array();
    // echo $this->db->last_query(); print_r($result);exit;
    }else{
      $row_array = $this->db->get()->result_array();
      $result = count($row_array);
      
   //  echo $this->db->last_query(); print_r($result);exit;
    }
  //echo $this->db->last_query(); print_r($result);exit;
    return $result;
  }
 


  public function get_order_status($filter_status='',$status='',$params='',$search='',$limit='',$order_id='',$order_id_status=''){
    $this->db->select('cp.*,k.name as karigar_name,cp.quantity as cp_weight,(CASE cp.status WHEN "0"  THEN "NOT ASSIGNED TO KARIGAR" WHEN "1" THEN "NOT SENT TO KARIGAR" WHEN "2" THEN "KARIGAR ENGAGED" WHEN "3" THEN "PENDING PRODUCT FOR QC" WHEN "4" THEN "QC REJECTED" WHEN "5" THEN "QC Amend" WHEN "6" THEN "PENDING FOR HALLMARKING" WHEN "7" THEN "SENT FOR HALLMARKING" WHEN "8" THEN "HALLMARKING QC ACCEPTED" WHEN "9" THEN "STOCK PRODUCTS" WHEN "10" THEN "PRODUCT SENT" WHEN "11" THEN "PRODUCT APPROVED"
      WHEN "12" THEN "PRODUCT REJECTED" WHEN "13" THEN "GENERATE PACKING LIST" WHEN "14" THEN "PACKING LIST " WHEN "15" THEN "PRODUCT APPROVED Caratlane" WHEN "16" THEN "PRODUCT REJECTED " END) as status,DATE_FORMAT(cp.proposed_delivery_date, "%d-%m-%Y") as proposed_delivery_date');
    $this->db->from('corporate_products  cp');
    $this->db->join('Prepare_product_list ppl','cp.id = ppl.corporate_products_id');
    $this->db->join('karigar_master k','k.id = ppl.karigar_id');
    if(!empty($order_id)){
      $this->db->where('cp.email_order_id',$order_id);
    } 
    if(!empty($order_id_status)){
      $this->db->where('cp.status',$order_id_status);
    }
    $sql='';  
  
    $i=0;
    if(!empty($params['columns'])){
        foreach ($params['columns'] as $key => $value ){
            if(!empty($value['search']['value']) && ($key == 0 OR $key == 1 OR $key == 2 OR $key == 3)){ 
              if(!empty($value['search']['value'])){
                if($i != 0){
                  $sql.=' AND ';
                }
                $sql.='(cp.work_order_id like "%'.$value['search']['value'].'%" '; 
                $sql.='OR cp.sort_Item_number like "%'.$value['search']['value'].'%" ';
                $sql.='OR k.name like "%'.$value['search']['value'].'%")';
                $i++;

            }  
          } 
        }
    }

  if(!empty($sql)){  
    $this->db->where($sql);  
  }   
    //$this->db->group_by('cp.work_order_id');

    if($limit == true){
      $this->db->order_by('cp.id','DESC');

      $result = $this->db->get()->result_array();
    //echo $this->db->last_query(); print_r($result);exit;
    }else{
      $row_array = $this->db->get()->result_array();
      $result = count($row_array);
      
   //  echo $this->db->last_query(); print_r($result);exit;
    }
  //echo $this->db->last_query(); print_r($result);exit;
    return $result;
  }

   public function get_orderwise_status($filter_status='',$status='',$params='',$search='',$limit='',$order_id='',$karigar_id=''){
    //print_r($karigar_id);die;

          $this->db->select('cp.*,k.name as karigar_name,cp.quantity as cp_weight,(CASE cp.status WHEN "0"  THEN "NOT ASSIGNED TO KARIGAR" WHEN "1" THEN "NOT SENT TO KARIGAR" WHEN "2" THEN "KARIGAR ENGAGED" WHEN "3" THEN "PENDING PRODUCT FOR QC" WHEN "4" THEN "QC REJECTED" WHEN "5" THEN "QC Amend" WHEN "6" THEN "PENDING FOR HALLMARKING" WHEN "7" THEN "SENT FOR HALLMARKING" WHEN "8" THEN "HALLMARKING QC ACCEPTED" WHEN "9" THEN "STOCK PRODUCTS" WHEN "10" THEN "PRODUCT SENT" WHEN "11" THEN "PRODUCT APPROVED"
      WHEN "12" THEN "PRODUCT REJECTED" WHEN "13" THEN "GENERATE PACKING LIST" WHEN "14" THEN "PACKING LIST " WHEN "15" THEN "PRODUCT APPROVED Caratlane" WHEN "16" THEN "PRODUCT REJECTED " END) as status,DATE_FORMAT( ppl.karigar_delivery_date,  "%d-%m-%Y" ) as karigar_delivery_date,DATE_FORMAT(ppl.karigar_engaged_date, "%d-%m-%Y") as karigar_engaged_date, DATE_FORMAT(DATE_ADD(ppl.karigar_engaged_date, INTERVAL 12 DAY), "%d-%m-%Y") as proposed_delivery_date,sum(ppl.quantity) as quantity,sum(rp.quantity) as rp_quantity');
    $this->db->from('corporate_products  cp');
    $this->db->join('Prepare_product_list ppl','cp.id = ppl.corporate_products_id');
    $this->db->join('karigar_master k','k.id = ppl.karigar_id');
    $this->db->join('Receive_products rp','cp.id=rp.corporate_product_id AND ppl.karigar_id=rp.karigar_id','left');
        if(!empty($karigar_id)){
           $this->db->where('ppl.karigar_id',$karigar_id);
        }


      if(!empty($order_id)){
        $this->db->where('cp.work_order_id',$order_id);
      }

    if(isset($params['columns']) && !empty($params['columns'])){
      $filter_input=$params['columns'];
      $this->get_filter_value($filter_input,'Corporate_order_Details_list');
    }
/*    $sql='';  
  
    $i=0;
    if(!empty($params['columns'])){
        foreach ($params['columns'] as $key => $value ){
            if(!empty($value['search']['value']) && ($key == 0 OR $key == 1 OR $key == 2)){ 
              if(!empty($value['search']['value'])){
                if($i != 0){
                  $sql.=' AND ';
                }
                $sql.='(cp.sort_Item_number like "%'.$value['search']['value'].'%" '; 
                $sql.='OR k.name like"%'.$value['search']['value'].'%")';
                $i++;

            }  
          } 
        }
    }

  if(!empty($sql)){  
    $this->db->where($sql);  
  } */  
    //$this->db->group_by('cp.work_order_id');

    if($limit == true){
      $this->db->order_by('cp.id','DESC');

      $result = $this->db->get()->result_array();
    //echo $this->db->last_query(); print_r($result);exit;
    }else{
      $row_array = $this->db->get()->result_array();
      $result = count($row_array);
      
   //  echo $this->db->last_query(); print_r($result);exit;
    }
  //echo $this->db->last_query(); print_r($result);exit;
    return $result;
  }

  private function get_filter_value($filter_input,$table_col_name){
    $column_name=array();  
    $filter_column_name=filter_column_name($table_col_name);
    $sql='';
    $sql1='';
    $i=0;
    $i2=0;
     
     //print_r($filter_input);die;
    foreach ($filter_input as $key => $search_value){
       $column_name=$filter_column_name;
      // print_r($column_name[$key]);
       //print_r($search_value['search']['value']);exit;
        if(!empty($search_value['search']['value'])){
          if($i != 0 && ($column_name[$key] != "count(DISTINCT cp.sort_Item_number)" && $column_name[$key] != "sum(ppl.quantity)" && $column_name[$key] != "sum(rp.quantity)") ){
            $sql.=' AND  ';
          }
          if($i2 != 0 &&(($column_name[$key] != "count(DISTINCT cp.sort_Item_number)" || $column_name[$key] != "sum(ppl.quantity)")|| $column_name[$key] != "sum(rp.quantity)")){
             $sql1.=' AND  ';
          }
           
           if(($column_name[$key] != "count(DISTINCT cp.sort_Item_number)") && ($column_name[$key] != "sum(ppl.quantity)") &&  ($column_name[$key] != "sum(rp.quantity)")  ){
           
            $sql.=''.$column_name[$key].' like "%'.$search_value['search']['value'].'%" ';
            
           }
           if($column_name[$key] == "sum(ppl.quantity)"){
            $sql1.="sum(ppl.quantity)=".$search_value['search']['value']."";
           }
           if($column_name[$key] == "count(DISTINCT cp.sort_Item_number)"){
             $sql1.="count(DISTINCT cp.sort_Item_number) =".$search_value['search']['value'].""; 
           }
            if($column_name[$key] == "sum(rp.quantity)"){
              $sql1.="sum(rp.quantity) =".$search_value['search']['value']."";  
            }
            $i++;
            $i2++;
        

         }   
    }

    if(!empty($sql)){  
      $this->db->where($sql);  
    } 
   if(!empty($sql1)){  
      $this->db->having($sql1);  
    } 
   
       
  }
}