<?php
class Import_bill_excel_model extends CI_Model {

  function __construct() {
    $this->table_name = "quality_control_hallmarking";
    $this->insert_gt_array=array();
    $this->existing_ids=array();
    parent::__construct();

  }
  private function validateExceldata($insert_array = '',$challan_no){
   $insert_gt_excel=array();
   $data = array();
   $existing_ids =array();
   $quantity =$gross_wt=$net_wt=0;
   $quantity_excel =$gross_wt_excel=$net_wt_excel=0;
   /*basic validation*/
    $this->form_validation->set_rules($this->config->item('gt_excel', 'admin_validationrules'));
    if ($this->form_validation->run() == FALSE) {
      $data['error']['challan_no'] = strip_tags(form_error('challan_no'));
      return $data;break;
      }
   
   /**/
   /*import excel heading validation*/
   $table_rows = $this->db->list_fields('gt_excel');
   $heading_excel = array_keys(@$insert_array[0]);
   $diff_array = array_diff($heading_excel,$table_rows);

   if (count($diff_array) != 0) {
      $data['error']['heading_col']="";
      $df_key=1;
     foreach ($diff_array as $df_value) {
       $data['error']['heading_col'] .= $df_key.'. '.strtoupper($df_value)." column not exits. <br>";
       $df_key++;
     }
     return $data;break;
   }

   $chitti_no = $this->get_chitti_no()+1;
    foreach ($insert_array as $key => $value) {
      if ($key == 0) {
       continue;
      }

      $rowno = $key+1;
      $code = @$value['item'];
      $article_code = @$value['article_code'];

      $work_order = @$value['wo'];
      $sr_no_excel = str_replace('$','',@$value['wo_srl']);
        // if (!empty($code) && !empty($article_code)) {
      if (!empty($work_order) && !empty($sr_no_excel)) {
            $this->db->select('qch.*,eo.corporate');
            if (count($existing_ids) != 0) {
             $this->db->where_not_in('qch.id', $existing_ids);
            }

             $quantity =str_replace("$", "", $value['quantity']);
             $gross_wt =str_replace("$", "",$value['gross_wt_gm']); 
             $net_wt  =str_replace("$", "",$value['net_wt_gm']);
            // $this->db->where('cp.item_number', $code);
            // $this->db->where('cp.sort_Item_number',$article_code);
            $this->db->where('qch.quantity',$quantity);
            $this->db->where('round(qch.total_gr_wt,3)',round($gross_wt,3));
            $this->db->where('round(qch.total_net_wt,3)',round($net_wt,3));
            $this->db->where('cp.work_order_id',$work_order);
            $this->db->where('ROUND(cp.line_number)',$sr_no_excel);
            $this->db->where('qch.status','8');
            $this->db->from('quality_control_hallmarking qch');
            $this->db->join('corporate_products cp','qch.corporate_product_id=cp.id','left');
            $this->db->join('email_orders eo','cp.order_id=eo.order_id');
            $query = $this->db->get()->row_array();
/*            echo count($query);
            echo $this->db->last_query();
            print_r($query);exit;*/

              if (count($query) < 1) {
                  $data['error'][$key] = 'Row '.$rowno.' of work order ID/serial number/Quantity/Gross Weight/Net Weight not Matched' ;
              } else {
                 // $quantity +=$query['quantity'];
                 // $gross_wt +=$query['total_gr_wt']; 
                 // $net_wt +=$query['total_net_wt'];

                 // $quantity_excel +=str_replace("$", "", $value['quantity']);
                 // $gross_wt_excel +=str_replace("$", "",$value['gross_wt_gm']); 
                 // $net_wt_excel +=str_replace("$", "",$value['net_wt_gm']);
                 // if (str_replace("$", "", $value['quantity']) != $query['quantity']) {
                 //  $data['error'][$key] = 'Row '.$rowno.' Item Number '.$value['item'].' Quantity Not Matched' ;

                 // }elseif (str_replace("$", "", $value['gross_wt_gm']) != $query['total_gr_wt']) {
                 //  $data['error'][$key] = 'Row '.$rowno.' Item Number '.$value['item'].' Gross Weight Not Matched' ;
                 // }elseif (str_replace("$", "", $value['net_wt_gm']) != $query['total_net_wt']) {
                 //  $data['error'][$key] = 'Row '.$rowno.' Item Number '.$value['item'].' Net Weight Not Matched' ;
                 // }else{
                  $existing_ids[]=$query['id'];
                  /* building insertion array*/
                  $value['wo'] =str_replace("$", "",$value['wo']);
                  $value['wo_srl'] =str_replace("$", "",$value['wo_srl']);
                  $value['int_wh'] = str_replace("$", "",$value['int_wh']);
                  $value['trans_date'] = date('Y-m-d',strtotime(str_replace("$", "",$value['trans_date'])));
                  $value['item'] =str_replace("$", "",$value['item']);
                  $value['article_code'] = str_replace("$", "",$value['article_code']);
                  $value['article_desc'] =str_replace("$", "",$value['article_desc']);
                  $value['hsn_code'] =str_replace("$", "",$value['hsn_code']);
                  $value['metal_type'] =str_replace("$", "",$value['metal_type']);
                  $value['config'] = str_replace("$", "",$value['config']);
                  $value['quantity'] = str_replace("$", "",$value['quantity']);
                  $value['gross_wt_gm'] = str_replace("$", "",$value['gross_wt_gm']);
                  $value['net_wt_gm'] = str_replace("$", "",$value['net_wt_gm']);
                  $value['pure_wt_gm'] =str_replace("$", "",$value['pure_wt_gm']);
                  $value['wqty'] =str_replace("$", "",$value['wqty']);
                  $value['mk_value'] = str_replace("$", "",$value['mk_value']);
                  $value['total_cost'] = str_replace("$", "",$value['total_cost']);
                  $value['hm_charges'] = str_replace("$", "",$value['hm_charges']);
                  $value['shipment_value'] = str_replace("$", "",$value['shipment_value']);  

                  $value['qch_id']=$query['id'];
                  $value['qc_id']=$query['qc_id'];
                  
                  $value['chitti_no']=$chitti_no;
                  $value['challan_no']=$challan_no;
                  $value['corporate_product_id']=$query['corporate_product_id'];
                  $email_order_id = email_order_id($query['corporate_product_id']);
                  $value['email_order_id']=$email_order_id;
                  $value['created_at']=date('Y-m-d H:i:s');
                  $value['corporate']=$query['corporate'];
                  $insert_gt_excel[]=$value;
                  // $insert_stock[$is_key]['corporate_product_id']=$query['corporate_product_id'];
                  // $insert_stock[$is_key]['created_at']=date('Y-m-d H:i:s');
                  // $insert_stock[$is_key]['corporate']=$query['corporate'];
                  // $insert_stock[$is_key]['status']='1';
                  // $insert_stock[$is_key]['receive_product_id ']=$query['receive_product_id'];
                  // $is_key++;
                // }
                  //print_r($value);
                  if($query['corporate']=='1'){
                   corporate_product_update($email_order_id,'10',$value['item']);//status 10-PRODUCT SENT 
                  }else{
                     corporate_product_update($email_order_id,'13',$value['item']);//status   13-GENERATE PACKING LIST
                  }
              }
        }else{
          //$data['error'][$key] = 'Row '.$rowno.' have blank data' ;
        }
      }
      $this->insert_gt_array=$insert_gt_excel;
      $this->existing_ids=$existing_ids;
      
      return $data;
  }
  public function store(){
    $_POST['created_at'] = date('Y-m-d H:i:s');
    //print_r($_FILES['file_0']);
    if(empty($_FILES['file_0']))
      {
      $excel_error['status'] = 'failure';
      $excel_error['error'][0] = "Please upload GT Excel";
      echo json_encode($excel_error);die;
      }else{           
          $status = imageValidation($_FILES['file_0'],array('xlsx','xls','csv'));
              if($status['status'] == 'fail'){
                $data['status']= 'failure';
                $data['error'][0] = "Invalid file format of an uploaded file ,Only 'xlsx,xls,csv allowed'";
                echo json_encode($data);die;
              }else{
                $excel_result = $this->excel_lib->import('',$_FILES['file_0']);
              }
      } 
       // print_r($vdata); die();
   //print_r($excel_result); die();
    if($excel_result['status'] == 'success'){
        $vdata = $this->validateExceldata($excel_result['result'],@$_POST['challan_no']);
        if(!isset($vdata['error'])){
          $insert_array = $this->insert_gt_array;
          //print_r($insert_array);die;
            if (count($insert_array) != 0) {
              $this->db->where_in('id',$this->existing_ids);
              $this->db->update('quality_control_hallmarking',array("status"=>"9",'HM_send_date'=>date('Y-m-d')));
              $this->db->insert_batch('gt_excel',$insert_array);
            }
           }
           else
           {
              $cerror['status'] = 'failure';
              $cerror['data'] = '';
              $cerror['error'] = $vdata['error'];
              echo json_encode($cerror);die;
           }
        
    }elseif($excel_result['status'] == 'failure'){
      $excel_error = array();
      $excel_error['status'] = 'failure';
      $excel_error['error'][0] = "Excel Format is wrong. Please upload correct excel format";
      echo json_encode($excel_error);die;
    }

    return get_successMsg();
  }

  function get_chitti_no(){
    $this->db->group_by('challan_no');
    $this->db->from('gt_excel');
    return $this->db->count_all_results();

  }
  
  
} 