<?php
class Design_tracking_model extends CI_Model {

  function __construct() {
      $this->table_details ="quality_control";
      $this->table_name = "corporate_products";
      parent::__construct();
  }
  public function get_email_order_data($type="",$limit=""){
    $this->db->select('id,order_id,DATE_FORMAT(date , "%d-%m-%Y") as order_date ,corporate');
    $this->db->from('email_orders');
    if($type !="all"){
      $this->db->where('corporate',$type);
    }

    $result = $this->db->get()->result_array();
    if($limit == true){
      $result = count($result);
    }
    return $result;
  }  
  

  public function get_email_order_corporate_products_data($email_order_id){
    $this->db->select('id,DATE_FORMAT(proposed_delivery_date , "%d-%m-%Y") as delivery_date,count(CW_quantity) as total_design,email_order_id ,sort_Item_number as desgin_code');
    $this->db->from($this->table_name);
    $this->db->where('email_order_id',$email_order_id);
    $this->db->group_by('email_order_id');
    $result = $this->db->get()->row_array();
    //echo $this->db->last_query();die;  
    return $result;
  }

  public function get_email_order_sent_to_karigar_data($email_order_id){
    $this->db->select('id,count(quantity) as assign_design,corporate_products_id,karigar_id,email_order_id,DATE_FORMAT(created_at , "%d-%m-%Y") as sent_date');
    $this->db->from('Prepare_product_list');
    $this->db->where('email_order_id',$email_order_id);
   // $this->db->group_by('email_order_id');
     $this->db->where('module','1');
    $result = $this->db->get()->row_array();
   //echo $this->db->last_query(); print_r($result); die;
    return $result;
  }

  public function get_email_order_received_from_karigar_data($email_order_id){
    $this->db->select('id,count(quantity) as received_design,corporate_product_id,karigar_id,email_order_id ,DATE_FORMAT(date , "%d-%m-%Y") as received_date');
    $this->db->from('Receive_products');
    $this->db->where('email_order_id',$email_order_id);
    //$this->db->group_by('email_order_id');
    $result = $this->db->get()->row_array();
    //echo $this->db->last_query();die;
    return $result;
  }

  public function get_email_order_qc_pass_data($email_order_id){
    $this->db->select('id,count(quantity) as qc_pass_design,corporate_product_id,email_order_id ,DATE_FORMAT(created_at , "%d-%m-%Y") as qc_pass_date');
    $this->db->from($this->table_details);
    $this->db->where('email_order_id',$email_order_id);
    //$this->db->where('status','1');//qc_pass
    $this->db->where('module','1');
    //$this->db->group_by('email_order_id');
    $result = $this->db->get()->row_array();
    // echo $this->db->last_query(); print_r($result); die;
    return $result;
  }
  public function get_email_order_qc_rejecte_data($email_order_id){
    $this->db->select('id,count(quantity) as qc_rejecte_design,corporate_product_id,email_order_id,DATE_FORMAT(created_at , "%d-%m-%Y") as qc_reject_date');
    $this->db->from($this->table_details);
    $this->db->where('email_order_id',$email_order_id);
   // $this->db->where('status','0');
    $this->db->where('module','1');//qc_rejecte
    //$this->db->group_by('email_order_id');
    $result = $this->db->get()->row_array();
    // echo $this->db->last_query(); print_r($result); die;
    return $result;
  }
  public function get_email_order_send_hm_data($email_order_id){
    $this->db->select('id,count(quantity) as send_hm_design,corporate_product_id,email_order_id,DATE_FORMAT(created_at , "%d-%m-%Y") as send_hm_date');
    $this->db->from($this->table_details);
    $this->db->where('email_order_id',$email_order_id);
    //$this->db->where('status','2');//send_hm
    $this->db->where('module','1');
    //$this->db->group_by('email_order_id');
    $result = $this->db->get()->row_array();
    // echo $this->db->last_query(); print_r($result); die;
    return $result;
  }
  public function get_email_order_accept_hm_data($email_order_id){
    $this->db->select('id,count(quantity) as accept_hm_design,corporate_product_id,email_order_id ,DATE_FORMAT(created_at , "%d-%m-%Y") as accept_hm_date');
    $this->db->from($this->table_details);
    $this->db->where('email_order_id',$email_order_id);
   // $this->db->where('status','3');//accept_hm
    $this->db->where('module','1');
    //$this->db->group_by('email_order_id');
    $result = $this->db->get()->row_array();
    // echo $this->db->last_query(); print_r($result); die;
    return $result;
  }
  public function get_email_order_caratelane_product_sent_data($email_order_id){
    $this->db->select('id,count(quantity) as product_sent_caratlane_design,quality_control_id,email_order_id ,DATE_FORMAT(created_at , "%d-%m-%Y") as sent_caratelane_date');
    $this->db->from('generate_packing_list');
    $this->db->where('email_order_id',$email_order_id);
    //$this->db->where('status','0');//product_sent_caratelane
    $this->db->group_by('email_order_id');
    $result = $this->db->get()->row_array();
    // echo $this->db->last_query(); print_r($result); die;
    return $result;
  }

    public function get_email_order_reliance_product_sent_data($email_order_id){
    $this->db->select('id,count(quantity) as product_sent_reliance_design,challan_no,corporate_product_id,email_order_id , DATE_FORMAT(created_at , "%d-%m-%Y") as sent_reliance_date');
    $this->db->from('gt_excel');
    $this->db->where('email_order_id',$email_order_id);
    //$this->db->where('status','0');//product_sent_reliance
    $this->db->group_by('email_order_id');
    $result = $this->db->get()->row_array();
    // echo $this->db->last_query(); print_r($result); die;
    return $result;
  }

  
  public function get_karigar_name($email_order_id){
    $this->db->select('km.name as karigar_name');
    $this->db->from('Receive_products rp');
    $this->db->join('karigar_master km','km.id=rp.karigar_id');
    $this->db->where('rp.email_order_id',$email_order_id);
    $this->db->where('rp.module','1');
    //$this->db->group_by('rp.email_order_id');
    $result = $this->db->get()->row_array();
   //echo $this->db->last_query(); print_r($result); die;
    return $result['karigar_name'];
  }

  
} //class 