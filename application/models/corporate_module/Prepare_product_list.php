<?php

class Prepare_product_list extends CI_Model {

  function __construct() {
      $this->table_name = "Prepare_product_list";
      parent::__construct();
  }
  public function get($filter_status='',$status='',$params='',$search='',$limit='',$karigar_id=''){
    $this->db->select('ppl.corporate_products_id  as no_of_products,k.name,k.id as karigar_id,weight_band,cp.net_weight,cp.quantity as cp_weight');
    $this->db->from($this->table_name.' ppl');
    $this->db->join('karigar_master k','k.id = ppl.karigar_id');
    $this->db->join('corporate_products cp','cp.id = ppl.corporate_products_id');
    $this->db->where('ppl.status','0');
    $sql='';
    if(!empty($karigar_id)){
      $this->db->where('karigar_id',$karigar_id);
      if(!empty($search)){
        $this->all_like_queries($search);  
      }
  
    if($limit == true)
      $this->db->limit($params['length'],$params['start']);
      //$this->db->group_by('karigar_id');
   // $this->db->order_by('k.id','DESC');
      $res = $this->db->get()->result_array();
      //echo $this->db->last_query(); print_r($result);exit;
      //echo $this->db->last_query(); 
      return $res;
    }     
  
   /* if(!empty($search)){
        $this->all_like_queries($search);  
      }*/
  /*   if(!empty($filter_status)){
      $this->db->order_by($status[$filter_status['column']],$filter_status['dir']);
    } */
    //print_r($params['columns']);
    $i=0;
  foreach ($params['columns'] as $key => $value ){
      if(!empty($value['search']['value']) && ($key == 0 OR $key == 1 OR $key == 2)){ 
        if(!empty($value['search']['value'])){
          if($i != 0){
            $sql.=' AND ';
          }
          $sql.='(k.name like "%'.$value['search']['value'].'%" '; 
          $sql.='OR ppl.corporate_products_id like"%'.$value['search']['value'].'%" ';
          $sql.='OR cp.weight_band like"%'.$value['search']['value'].'%")';
          $i++;

      }  
    } 
  }

  if(!empty($sql)){  
    $this->db->where($sql);  
  }   
    $this->db->order_by('ppl.id','DESC');
   

    //$this->db->group_by('karigar_id');
    $result = $this->db->get()->result_array();
 //echo $this->db->last_query(); print_r($result);exit;
    return $result;
  }

public function get_new($filter_status='',$status='',$params='',$search='',$limit='',$karigar_id=''){
    $this->db->select('cp.order_id,GROUP_CONCAT(DISTINCT(k.name)  SEPARATOR ",") as name,GROUP_CONCAT(DISTINCT(k.id)  SEPARATOR ",") as k_id,count(cp.sort_Item_number) as no_of_design,sum(cp.quantity) as total_weight');
    $this->db->from($this->table_name.' ppl');
    $this->db->join('karigar_master k','k.id = ppl.karigar_id');
    $this->db->join('corporate_products cp','cp.id = ppl.corporate_products_id');
    $this->db->where('ppl.status','0');
    $sql='';
    if(!empty($karigar_id)){
      $this->db->where('karigar_id',$karigar_id);
      if(!empty($search)){
        $this->all_like_queries($search);  
      }
  
    if($limit == true)
      $this->db->limit($params['length'],$params['start']);
      
   // $this->db->order_by('k.id','DESC');
      $res = $this->db->get()->result_array();
      //echo $this->db->last_query(); die;
      return $res;
    }     
  
   /* if(!empty($search)){
        $this->all_like_queries($search);  
      }*/
  /*   if(!empty($filter_status)){
      $this->db->order_by($status[$filter_status['column']],$filter_status['dir']);
    } */
    //print_r($params['columns']);
    $i=0;
  foreach ($params['columns'] as $key => $value ){
      if(!empty($value['search']['value']) && ($key == 0 OR $key == 1 OR $key == 2)){ 
        if(!empty($value['search']['value'])){
          if($i != 0){
            $sql.=' AND ';
          }
          $sql.='(k.name like "%'.$value['search']['value'].'%" '; 
          $sql.='OR cp.order_id like "%'.$value['search']['value'].'%" '; 
          $sql.='OR ppl.corporate_products_id like"%'.$value['search']['value'].'%" ';
          $sql.='OR cp.weight_band like"%'.$value['search']['value'].'%")';
          $i++;

      }  
    } 
  }

  if(!empty($sql)){  
    $this->db->where($sql);  
  }   
     $this->db->group_by('ppl.order_name');

    $this->db->order_by('ppl.id','DESC');

    //$this->db->group_by('karigar_id');
    $result = $this->db->get()->result_array();
  //echo $this->db->last_query(); print_r($result);exit;
    return $result;
  }
  private function all_like_queries($search){
    $this->db->where("(`k`.`name` LIKE '%$search%')");

  }
  public function store($insert_array){
    $this->db->insert($this->table_name,$insert_array);
  }
}