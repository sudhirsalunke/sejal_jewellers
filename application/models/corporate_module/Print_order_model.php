<?php

class Print_order_model extends CI_Model {

  function __construct() {
      $this->table_name = "Prepare_product_list";
      parent::__construct();
  }
  
  public function get($karigar_id){
 // print_r($_POST);exit;
    $this->db->select('group_concat(round(line_number) ORDER BY cp.id ASC) as line_numbers,group_concat(`cp`.`id` SEPARATOR ",") as `UID`,  group_concat( (`cp`.`size_master`)ORDER BY cp.id ASC SEPARATOR "/") as size_master ,
 group_concat(`cp`.`finding_cost` SEPARATOR "/") as finding_cost,
 group_concat(`cp`.`weight_band` SEPARATOR "/") as weight_band,group_concat( `cp`.`quantity` SEPARATOR "/" ) as cp_weight_band,km.name,km.code as karigar_code,cp.order_id,cp.CW_quantity as pcs,cp.CW_unit as unit,p.size,p.product_code,p.image,DATE_FORMAT( eo.date,  "%d-%m-%Y" ) as date,CONCAT(w.from_weight,"-",w.to_weight) as weight,DATE_FORMAT( cp.proposed_delivery_date,  "%d-%m-%Y" ) as proposed_delivery_date,group_concat(ppl.barcode_path SEPARATOR ",") as barcode_path,ppl.status as ppl_status,sum(ppl.quantity) as ppl_quantity,cp.size as cp_size,cp.net_weight,eo.corporate,cp.product_master,p.category_id,p.sub_category_id,scm.code as sub_category,scm.name as sub_category_name,pair_pcs,p.weight_band_id,DATE_FORMAT( ppl.karigar_engaged_date,  "%d-%m-%Y" ) as karigar_engaged_date,DATE_FORMAT( ppl.karigar_delivery_date,  "%d-%m-%Y" ) as karigar_delivery_date,cp.quantity as cp_weight,(select round(( cp.quantity*tolerance / 100 ),2) from tolerance_master where cp.quantity >= from_wt  and cp.quantity  <= to_wt) as tolerance_wt,(select round(( cp.net_weight*tolerance / 100 ),2) from tolerance_master where cp.net_weight >= from_wt  and cp.net_weight  <= to_wt) as tolerance_c_wt,cp.quantity as karigar_wt,(km.limit_wt-km.engaged_wt) as limit_wt ,ppl.karigar_id as karigar_id,eo.corporate,group_concat(cp.barcode_path SEPARATOR ",") as cp_barcode_path');/*cp size nd net weight added*/
    $this->db->from($this->table_name.' ppl');
    $this->db->join('corporate_products cp','cp.id = ppl.corporate_products_id');
    $this->db->join('email_orders eo','eo.order_id = cp.order_id');
    $this->db->join('product p','cp.sort_Item_number = p.product_code');
    $this->db->join('category_master cm','cm.id = p.category_id','left');
    $this->db->join('sub_category_master scm','scm.id = p.sub_category_id','left');
    $this->db->join('weights w','w.id = p.weight_band_id','left');/*added left join*/
    $this->db->join('karigar_master km','km.id = ppl.karigar_id');
    $this->db->where('ppl.karigar_id',$karigar_id);
    $this->db->where('ppl.status','0');
     if(!isset($_POST['filter']) && empty($_POST['filter_o']['order_id']) && empty($_POST['filter_o']['weight_range']) && empty($_POST['filter_o']['category']) ){
          if($_GET['order_id'] != ""){
            $this->db->where('cp.order_id',$_GET['order_id']);
           }
       }
  /*  if (isset($_POST['filter'])) {
        if(!empty($_POST['filter']['order_id'])){
           $this->db->where_in('cp.order_id',$_POST['filter']['order_id']);           
        }
         if(!empty($_POST['filter']['product_code'])){
            $this->db->where_in('p.product_code',$_POST['filter']['product_code']);
        }
    }
*/
 
    if (isset($_POST['filter'])) {
      
        if(!empty($_POST['filter']['order_id'])){            
           $this->db->where_in('cp.order_id',$_POST['filter']['order_id']);
        }
         if(!empty($_POST['filter']['product_code'])){
            $this->db->where_in('cp.sort_Item_number',$_POST['filter']['product_code']);
        }
        if(!empty($_POST['filter']['weight_range'])){
         // print_r($_POST['filter']['weight_range']);die;
          $this->db->where_in('cp.weight_band',$_POST['filter']['weight_range']);
        }

   /*     if(!empty($_POST['filter']['pcs'])){
           //$this->db->where_in('cp.CW_quantity',$_POST['filter']['pcs']);
        }*/
        if(!empty($_POST['filter']['category'])){
          $this->db->where_in('cp.product_master',$_POST['filter']['category']);
        }
// print_r($_POST['filter']['size']);die;
        if(!empty($_POST['filter']['size'])){
          $this->db->where_in('cp.size_master',$_POST['filter']['size']);
        }
    
     if (isset($_POST['filter_o'])) {
        if(!empty($_POST['filter_o']['order_id'])){
           $order_id=explode(',', $_POST['filter_o']['order_id']);
           $this->db->where_in('cp.order_id',$order_id);
        }
         if(!empty($_POST['filter_o']['product_code'])){
           $product_code=explode(',', $_POST['filter_o']['product_code']);
            $this->db->where_in('cp.sort_Item_number',$product_code);
        }

        if(!empty($_POST['filter_o']['weight_range'])){
           $weight_range=explode(',', $_POST['filter_o']['weight_range']);
           $this->db->where_in('cp.weight_band',$weight_range);
        }

       /* if(!empty($_POST['filter_o']['pcs'])){
           $pcs=explode(',', $_POST['filter_o']['pcs']);
           $this->db->where_in('cp.sort_Item_number',$pcs);
        }*/

        if(!empty($_POST['filter_o']['size'])){
           $size=explode(',', $_POST['filter_o']['size']);
           $this->db->where_in('cp.size_master',$size);
        }
         if(!empty($_POST['filter_o']['category'])){
           $category=explode(',', $_POST['filter_o']['category']);
           $this->db->where_in('cp.product_master',$category);
        }
    }
    }
    
    $this->db->group_by(array('cp.sort_Item_number','cp.work_order_id','cp.order_id'));
    $this->db->order_by('cp.id');
    $result = $this->db->get()->result_array();
    //echo $this->db->last_query();print_r($result);exit;
    return $result;
  }
  public function make_engaged($karigar_id)
  {  
    $data['status'] = '1';
    $data['updated_at'] = date('Y-m-d H:i:s');
    $data['karigar_engaged_date'] = date('Y-m-d H:i:s');
    $data['karigar_delivery_date'] = date('Y-m-d',strtotime("+12 day"));
    $this->db->select('ppl.*,cp.sort_Item_number');
    $this->db->from('Prepare_product_list ppl');
    $this->db->join('corporate_products cp','cp.id = ppl.corporate_products_id');
    $this->db->join('karigar_master km','km.id = ppl.karigar_id');
    $this->db->where('ppl.karigar_id',$karigar_id);
    $this->db->where('ppl.status !=','2');
 /*
    if (isset($_POST['filter'])) {
        if(!empty($_POST['filter']['order_id'])){
            $this->db->where('cp.order_id',$_POST['filter']['order_id']);
        }
         if(!empty($_POST['filter']['product_code'])){
            $this->db->where('cp.sort_Item_number',$_POST['filter']['product_code']);
        }
    }*/

    if (isset($_POST['filter_o'])) {
       
       if(empty($_POST['filter_o']['order_id']) && empty($_POST['filter_o']['weight_range']) && empty($_POST['filter_o']['category']) ){
          if($_GET['order_id'] != ""){
            $this->db->where('cp.order_id',$_GET['order_id']);
           }
       }
        
        if(!empty($_POST['filter_o']['order_id'])){
           $order_id=explode(',', $_POST['filter_o']['order_id']);
           $this->db->where_in('cp.order_id',$order_id);
        }
         if(!empty($_POST['filter_o']['product_code'])){
           $product_code=explode(',', $_POST['filter_o']['product_code']);
           $this->db->where_in('cp.sort_Item_number',$product_code);
        }

        if(!empty($_POST['filter_o']['weight_range'])){
           $weight_range=explode(',', $_POST['filter_o']['weight_range']);
           $this->db->where_in('p.weight_band_id',$weight_range);
        }
/*
        if(!empty($_POST['filter_o']['pcs'])){
           $pcs=explode(',', $_POST['filter_o']['pcs']);
           $this->db->where_in('cp.sort_Item_number',$pcs);
        }*/

        if(!empty($_POST['filter_o']['size'])){
           $size=explode(',', $_POST['filter_o']['size']);
           $this->db->where_in('cp.size_master',$size);
        }
        if(!empty($_POST['filter_o']['category'])){
           $category=explode(',', $_POST['filter_o']['category']);
           $this->db->where_in('cp.product_master',$category);
        }
    }
    
    $result = $this->db->get()->result_array();
  //echo $this->db->last_query(). "<pre>"; print_r($result); echo"</pre>"; die;
    foreach ($result as $key => $value) {
      //print_r($value);
      $engaged_wt=0.00;
      $this->db->where('id',$value['id']);
      $data['receive_weight']=$value['weight'];
      $this->db->update('Prepare_product_list',$data);
      date_update('Prepare_product_list',$value['id'],'insert','updated_at','update');
      corporate_product_update($value['email_order_id'],'2',$value['sort_Item_number']);//status 2-KARIGAR ENGAGED
    }
    return true;
    //$inserted_id = $this->db->get_where('Prepare_product_list',array('karigar_id'=>$karigar_id,"status" =>'0'))->result_array();

    // $this->db->where('karigar_id');
    // $this->db->where('status !=','2');
    
    
    // if($this->db->update($this->table_name,$data)){
    //     foreach ($inserted_id as $key2 => $value2) {
    //       date_update('Prepare_product_list',$value2['id'],'insert','updated_at','update');
    //     }
    //     return true;
    //   }else{
    //     return false;
    //   }
  }

  public function get_product_code($karigar_id,$order_id){
     $this->db->select('cp.order_id,p.product_code,cp.sort_Item_number');/*fetching products by order_id*/
    $this->db->from($this->table_name.' ppl');
    $this->db->join('corporate_products cp','cp.id = ppl.corporate_products_id');
    $this->db->join('email_orders eo','eo.order_id = cp.order_id');
    $this->db->join('product p','cp.sort_Item_number = p.product_code');
    $this->db->where('ppl.karigar_id',$karigar_id);
    $this->db->where('ppl.status','0');
    $this->db->where_in('cp.order_id',$order_id);
    $this->db->group_by('cp.sort_Item_number');
    $this->db->order_by('cp.id');
    $result = $this->db->get()->result_array();
    // echo $this->db->last_query();print_r($result);exit;
    return $result;
  }

  public function get_orders_by_krigar($karigar_id){
     $this->db->select('cp.order_id');/*fetching products by order_id*/
    $this->db->from($this->table_name.' ppl');
    $this->db->join('corporate_products cp','cp.id = ppl.corporate_products_id');
    $this->db->join('email_orders eo','eo.order_id = cp.order_id');
    $this->db->where('ppl.karigar_id',$karigar_id);
    $this->db->where('ppl.status','0');
    $this->db->group_by('cp.order_id');
    $this->db->order_by('cp.id');
    $result = $this->db->get()->result_array();
    // echo $this->db->last_query();print_r($result);exit;
    return $result;
  }

  public function update_karigar_wt_limit(){
  $this->db->set('engaged_wt', 'engaged_wt + ' . (float) $_POST["karigar_wt"], FALSE);
  $this->db->where('id',$_POST['karigar']);
  $this->db->update('karigar_master');
 
    return true;
  }

/*  public function store_wt_karigar(){
    $this-get_karigar_wt_limit();
    $this->db->select('ppl.*,cp.sort_Item_number');
    $this->db->from('Prepare_product_list ppl');
    $this->db->join('corporate_products cp','cp.id = ppl.corporate_products_id');
    $this->db->join('karigar_master km','km.id = ppl.karigar_id');
    $this->db->where('ppl.karigar_id',$karigar_id);
    $this->db->where('ppl.status !=','2');


  }*/

   
}    
