<?php
class Engaged_karigar_list_model extends CI_Model {
  function __construct() {
      $this->table_name = "Prepare_product_list";
      parent::__construct();
  }
  public function get($filter_status='',$status='',$params='',$search='',$limit='',$karigar_id=''){
      if($limit == true){
        $this->db->select('ppl.id,ppl.karigar_id,GROUP_CONCAT(ppl.corporate_products_id) as engaged_products,GROUP_CONCAT(rp.corporate_product_id) as rp_products,k.name,DATE_FORMAT( eo.date,  "%d-%m-%Y" ) as order_date, DATE_FORMAT( ppl.karigar_engaged_date,  "%d-%m-%Y" ) as karigar_engaged_date,sum(ppl.quantity)as total_pieces,count(DISTINCT cp.sort_Item_number) as no_of_products, DATE_FORMAT(DATE_ADD(ppl.karigar_engaged_date, INTERVAL 12 DAY), "%d-%m-%Y") as proposed_delivery_date ,DATE_FORMAT( ppl.karigar_delivery_date,  "%d-%m-%Y" ) as karigar_delivery_date,ppl.id,sum(cp.quantity) as cp_weight,cp.net_weight,sum(rp.quantity) as total_received_pieces,sum(rp.gross_wt) as gross_wt');
      }else{

        $this->db->select('count(distinct (k.id)) as cont_record');
        
      }

    $this->db->from($this->table_name.' ppl');
    $this->col_join();  
    
    if(!empty($karigar_id)){
      $this->get_karigar_product_detaile($karigar_id);    
    }

    $status=@$_GET['status'];
    if($status=='due_date'){
      $table_col_name='Due_date_tracking_list';
        
    }else{
      $table_col_name='Engaged_karigar_list';
    }

   if(isset($params['columns']) && !empty($params['columns'])){
      $filter_input=$params['columns'];
    $this->get_filter_value($filter_input,$table_col_name);
    }

      $this->db->group_by('k.id');
    if($limit == true){
      $this->db->order_by('ppl.id','DESC');
    if(!empty($params)){
        $this->db->limit($params['length'],$params['start']);
    }
      $result = $this->db->get()->result_array();
     //echo $this->db->last_query(); print_r($result);exit;
    }else{
      $row_array = $this->db->get()->result_array();
      $result = count($row_array);
      
     // echo $this->db->last_query(); print_r($result);exit;
    }

      return $result;

  }  


private function col_join(){
    $due_date = date('Y-m-d',strtotime('+7 DAY'));
    $today = date('Y-m-d');
    $this->db->join('corporate_products cp','cp.id = ppl.corporate_products_id','left');
    $this->db->join('product p','p.product_code = cp.sort_Item_number','left');
    $this->db->join('email_orders eo','eo.order_id = cp.order_id','left');
    $this->db->join('karigar_master k','k.id = ppl.karigar_id','left');
    $this->db->join('Receive_products rp','ppl.corporate_products_id = rp.corporate_product_id','left');
    $this->db->where('(ppl.status = "1" OR ppl.status = "2")');
    if (@$_GET['status']=='due_date') {
      $this->db->where('((ppl.karigar_delivery_date <= "'.$due_date.'") AND (ppl.karigar_delivery_date >= "'.$today.'"))');
   }
      $this->db->having('sum(ppl.quantity) != count(rp.corporate_product_id)');/*uncomment*/
   
    
  } 

  public function get_by_due_date($filter_status='',$status='',$params='',$search='',$limit='',$type="",$corporate=""){
       // print_r($corporate);exit;
      if($limit == true){
           $this->db->select('ppl.id,ppl.karigar_id,ppl.corporate_products_id as engaged_products,k.name,DATE_FORMAT( eo.date,  "%d-%m-%Y" ) as order_date, DATE_FORMAT( ppl.karigar_engaged_date,  "%d-%m-%Y" ) as karigar_engaged_date,ppl.quantity as quantity, cp.sort_Item_number as product_code, DATE_FORMAT(DATE_ADD(ppl.karigar_engaged_date, INTERVAL 12 DAY), "%d-%m-%Y") as proposed_delivery_date ,DATE_FORMAT( ppl.karigar_delivery_date,  "%d-%m-%Y" ) as karigar_delivery_date,ppl.id,cp.quantity as cp_weight,cp.net_weight,cp.order_id as order_id,cp.line_number as line_number');
        
      }else{
        $this->db->select('*');  
      }

    $this->db->from($this->table_name.' ppl');
    $this->col_join1($type,$corporate);  
      $table_col_name='due_date_tracking';

   if(isset($params['columns']) && !empty($params['columns'])){
      $filter_input=$params['columns'];
    $this->get_filter_value($filter_input,$table_col_name);
    }
          if($limit == true){
      $this->db->order_by('ppl.id','DESC');
    if(!empty($params)){
        $this->db->limit($params['length'],$params['start']);
    }
      $result = $this->db->get()->result_array();
    // echo $this->db->last_query(); print_r($result);exit;
    }else{
      $row_array = $this->db->get()->result_array();
      $result = count($row_array);
      
   //  echo $this->db->last_query(); print_r($result);exit;
    }

      return $result;

  }
  private function col_join1($type,$corporate){
    //print_r($type);exit;
    $due_date = date('Y-m-d',strtotime('+3 DAY'));
    $today = date('Y-m-d');
    $this->db->join('corporate_products cp','cp.id = ppl.corporate_products_id','left');
    $this->db->join('product p','p.product_code = cp.sort_Item_number','left');
    $this->db->join('email_orders eo','eo.order_id = cp.order_id','left');
    $this->db->join('karigar_master k','k.id = ppl.karigar_id','left');
    // $this->db->join('Receive_products rp','ppl.corporate_products_id = rp.corporate_product_id','left');
    $this->db->where('(ppl.status = "1")');
    if(@$_GET['status'] == "due_date"){
      $this->db->where('((ppl.karigar_delivery_date <= "'.$due_date.'") AND (ppl.karigar_delivery_date >= "'.$today.'"))');
    }else{
      $this->db->where('ppl.karigar_delivery_date < "'.$today.'"');
      if(!empty($corporate)){
        $this->db->where('eo.corporate="'.$corporate.'"');
      }
    }
      
    }
    
    

   
  private function get_filter_value($filter_input,$table_col_name){
    $column_name=array();  
    $filter_column_name=filter_column_name($table_col_name);
    $sql='';
    $sql1='';
    $i=0;
    $i2=0;
     
     //print_r($filter_input);die;
    foreach ($filter_input as $key => $search_value){
       $column_name=$filter_column_name;
      // print_r($column_name[$key]);
       //print_r($search_value['search']['value']);exit;
        if(!empty($search_value['search']['value'])){
          if($i != 0 && ($column_name[$key] != "count(DISTINCT cp.sort_Item_number)" && $column_name[$key] != "sum(ppl.quantity)" && $column_name[$key] != "sum(rp.quantity)") ){
            $sql.=' AND  ';
          }
          if($i2 != 0 &&(($column_name[$key] != "count(DISTINCT cp.sort_Item_number)" || $column_name[$key] != "sum(ppl.quantity)")|| $column_name[$key] != "sum(rp.quantity)")){
             $sql1.=' AND  ';
          }
           
           if(($column_name[$key] != "count(DISTINCT cp.sort_Item_number)") && ($column_name[$key] != "sum(ppl.quantity)") &&  ($column_name[$key] != "sum(rp.quantity)")  ){
           
            $sql.=''.$column_name[$key].' like "%'.$search_value['search']['value'].'%" ';
            
           }
           if($column_name[$key] == "sum(ppl.quantity)"){
            $sql1.="sum(ppl.quantity)=".$search_value['search']['value']."";
           }
           if($column_name[$key] == "count(DISTINCT cp.sort_Item_number)"){
             $sql1.="count(DISTINCT cp.sort_Item_number) =".$search_value['search']['value'].""; 
           }
            if($column_name[$key] == "sum(rp.quantity)"){
              $sql1.="sum(rp.quantity) =".$search_value['search']['value']."";  
            }
            $i++;
            $i2++;
        

         }   
    }

    if(!empty($sql)){  
      $this->db->where($sql);  
    } 
   if(!empty($sql1)){  
      $this->db->having($sql1);  
    } 
   
       
  }

  private function get_karigar_product_detaile($karigar_id){
    $this->db->where('karigar_id',$karigar_id);  
    $this->db->group_by('karigar_id');
    return $this->db->get()->row_array();
  }
  public function get_karigar_pending_product($id)
  {
    $this->db->select('GROUP_CONCAT(ppl.corporate_products_id) as products');
    $this->db->from($this->table_name.' ppl');
    $this->db->join('corporate_products cp','cp.id = ppl.corporate_products_id');
    $this->db->join('product p','p.product_code = cp.sort_Item_number');
    $this->db->join('email_orders eo','eo.order_id = cp.order_id');
    $this->db->join('karigar_master k','k.id = ppl.karigar_id');
    $this->db->where('ppl.status','0');
    if(!empty($id)){
      $this->db->where('ppl.karigar_id',$id);
      $this->db->group_by('ppl.karigar_id');
      return $this->db->get()->row('products');    
    }
    $this->db->group_by('ppl.karigar_id');
    $result = $this->db->get()->row('products');
   //echo $this->db->last_query();die;
    return $result;
  }
  public function delete($id){
    $this->db->where('karigar_id',$id);
    $this->db->where('status','1');
    if($this->db->delete($this->table_name)){
      return get_successMsg();
    }else{
      return get_errorMsg();
    }
  }
  private function all_like_queries($search){
    //$this->db->where("(`k`.`name` LIKE '%$search%' || eo.order_id LIKE '%$search%' || cp.sort_Item_number LIKE '%$search%')");
    $this->db->where("(k.name LIKE '%$search%')");
  }
  function get_total_pieces($ids){
    $this->db->select('sum(quantity) as quantity');
    $this->db->from('Prepare_product_list');
    $this->db->where("corporate_products_id in ($ids)");
    $result = $this->db->get()->row_array();
    return  $result['quantity'];
  }
  function get_total_received_pieces($ids,$total_result=false){
    $ids = rtrim($ids,',');
    if($total_result == false)
      $this->db->select('sum(quantity) as quantity');
    else
      $this->db->select('corporate_product_id');

    $this->db->from('Receive_products');
    $this->db->where("corporate_product_id in ($ids)");
    if($total_result == false)
       $result = $this->db->get()->row_array();
    else
       $result = $this->db->get()->result_array();
    if($total_result == false)
       return  $result['quantity'];
    else
       return  $result;
  }

  function view_karigar_details($filter_status='',$params='',$limit='',$karigar_id=''){    
    if($limit == true){
      $this->db->select('ppl.id,cp.sort_Item_number ,cp.order_id,DATE_FORMAT(ppl.karigar_engaged_date, "%d-%m-%Y") as karigar_engaged_date, DATE_FORMAT(DATE_ADD(ppl.karigar_engaged_date, INTERVAL 12 DAY), "%d-%m-%Y") as proposed_delivery_date,sum(ppl.quantity) as quantity,sum(rp.quantity) as rp_quantity, km.name as karigar_name,cp.net_weight,cp.weight_band,ppl.karigar_id,DATE_FORMAT( ppl.karigar_delivery_date,  "%d-%m-%Y" ) as karigar_delivery_date,cp.quantity as cp_weight,rp.gross_wt');
     }else{
      $this->db->select('count(ppl.id) as cont_record');
     } 
    $this->db->from('Prepare_product_list ppl');
    $this->db->join('corporate_products cp','ppl.corporate_products_id=cp.id','left');
    $this->db->join('karigar_master km','km.id=ppl.karigar_id','left');
    $this->db->join('Receive_products rp','cp.id=rp.corporate_product_id AND ppl.karigar_id=rp.karigar_id','left');
    $this->db->where('ppl.karigar_id',$karigar_id);
    // $this->db->where('ppl.status','1');
    $this->db->where('(ppl.status = "1" OR ppl.status = "2")');

    if (@$_GET['status']=='due_date') {
      $this->db->where('((DATE_ADD(ppl.karigar_engaged_date, INTERVAL 12 DAY) <= DATE_ADD(CURDATE(), INTERVAL 3 DAY)) AND (DATE_ADD(ppl.karigar_engaged_date, INTERVAL 12 DAY)>= CURDATE()))');

    }
    $table_col_name='Engaged_karigar_Details_list';
    if(isset($params['columns']) && !empty($params['columns'])){
      $filter_input=$params['columns'];
      $this->get_filter_value($filter_input,$table_col_name);
    }
   // print_r($params['start']);

    //echo $this->db->last_query();die;
    //$result=array();

    if($limit == true){
    $this->db->group_by('ppl.corporate_products_id');
      $this->db->limit($params['length'],$params['start']);
      $result = $this->db->get()->result_array();
     /* echo $this->db->last_query();
      print_r($result);
      die;*/
    }else{
      $row_array = $this->db->get()->row_array();
      $result = $row_array['cont_record'];
      //echo $this->db->last_query();die;
    }
    //$result = $this->db->get()->result_array();
    return $result;
  }
   function export_karigar_details($karigar_id){
   $this->db->select('cp.sort_Item_number ,cp.order_id,DATE_FORMAT(ppl.karigar_engaged_date, "%d-%m-%Y") as karigar_engaged_date, DATE_FORMAT(DATE_ADD(ppl.karigar_engaged_date, INTERVAL 12 DAY), "%d-%m-%Y") as proposed_delivery_date,sum(ppl.quantity) as quantity,sum(rp.quantity) as rp_quantity, km.name as karigar_name,cp.net_weight,cp.weight_band,ppl.karigar_id,DATE_FORMAT( ppl.karigar_delivery_date,  "%d-%m-%Y" ) as karigar_delivery_date');
    $this->db->where('ppl.karigar_id',$karigar_id);
    $this->db->from('Prepare_product_list ppl');
    $this->db->join('corporate_products cp','ppl.corporate_products_id=cp.id','left');
    $this->db->join('karigar_master km','km.id=ppl.karigar_id','left');
    $this->db->join('Receive_products rp','cp.id=rp.corporate_product_id AND ppl.karigar_id=rp.karigar_id','left');
    if (@$_GET['status']=='due_date') {
      $this->db->where('((DATE_ADD(ppl.karigar_engaged_date, INTERVAL 12 DAY) <= DATE_ADD(CURDATE(), INTERVAL 3 DAY)) AND (DATE_ADD(ppl.karigar_engaged_date, INTERVAL 12 DAY)>= CURDATE()))');

    }
      
    $this->db->group_by('ppl.corporate_products_id');
    //echo $this->db->last_query();die;
    //$result=array();
    $result = $this->db->get()->result_array();
    return $result;
  }
  public function get_sum_engage_weight($ids){
    $wt_bnd = 0;
    if(!empty($ids)){
      foreach ($ids as $key => $value) {
        $this->db->select('weight_band,net_weight');
        $this->db->from('corporate_products');
        $this->db->where("id",$value);
        $result = $this->db->get()->row_array();
        if(!empty($result)){      
          //foreach ($result as $key => $value) {
            if(!empty($result['weight_band'])){
              $wt_bnd += get_mid_value($result['weight_band']);
            }else{
              $wt_bnd +=$result['net_weight'];
            }
          //}
      }
    }
  }
  return $wt_bnd;
  }
   public function get_sum_engage_actule_weight($ids){
    $wt_bnd = 0;
    if(!empty($ids)){
      foreach ($ids as $key => $value) {
        $this->db->select('quantity,net_weight');
        $this->db->from('corporate_products');
        $this->db->where("id",$value);
        $result = $this->db->get()->row_array();
        if(!empty($result)){      
          //foreach ($result as $key => $value) {
            if(!empty($result['quantity'])){
              $wt_bnd += $result['quantity'];
            }else{
              $wt_bnd +=$result['net_weight'];
            }
          //}
      }
    }
  }
  return $wt_bnd;
  }
  public function get_sum_rp_weight($ids){
    $wt_bnd = 0;
    if(!empty($ids)){
      foreach ($ids as $key => $value) {
        $this->db->select('weight_band');
        $this->db->from('corporate_products');
        $this->db->where("id",$value['corporate_product_id']);
        $result = $this->db->get()->row_array();
        if(!empty($result)){      
          //foreach ($result as $key => $value) {
            if(!empty($result['weight_band'])){
              $wt_bnd += get_mid_value($result['weight_band']);
            }
          //}
      }
    }
    }
    return $wt_bnd;
  }
  public function get_karigar_engaged(){
    $this->db->select('km.name,km.id');
    $this->db->from('Prepare_product_list ppl');
    $this->db->join('karigar_master km', 'ppl.karigar_id = km.id');
    $this->db->join('corporate_products cp','cp.id = ppl.corporate_products_id');
    $this->db->join('product p','cp.sort_Item_number = p.product_code');
    $this->db->group_by('ppl.karigar_id');
    $this->db->where('ppl.status','1');
    $result = $this->db->get()->result_array();
    return $result;
  }
  public function get_karigar_engaged_orders($karigar_id){
    $this->db->select('eo.id,eo.order_id,count(ppl.id) count');
    $this->db->from($this->table_name.' ppl');
    $this->db->join('corporate_products cp','cp.id = ppl.corporate_products_id');
    $this->db->join('email_orders eo','eo.order_id = cp.order_id');
    $this->db->join('product p','cp.sort_Item_number = p.product_code');
    $this->db->where('ppl.status','1');
    $this->db->where('ppl.karigar_id',$karigar_id);
    $this->db->group_by('eo.order_id');
    $result = $this->db->get()->result_array();
    return $result;
  }
  /*public function get_karigar_engaged_products($karigar_id,$order_id,$prodcut_code,$category_name,$article_id){
    $this->db->select('scm.name as sub_cat_name, cp.weight_band ,cp.sort_Item_number,sum(rp.quantity) as rp_quantity,cp.order_id,cp.line_number as line_number,c.name cat_name,eo.id email_order_id,cp.id corporate_product_id,eo.corporate,sum(ppl.quantity) as quantity,ppl.*,p.corporate, cp.size as cr_size, cp.size_master,cp.quantity as tolerance_wt');
    $this->db->from('Prepare_product_list ppl');
    $this->db->join('corporate_products cp','ppl.corporate_products_id = cp.id' ,'left');
    $this->db->join('product p','cp.sort_Item_number = p.product_code', 'left');
    $this->db->join('category_master c', 'c.id = p.category_id', 'left');
    $this->db->join('sub_category_master scm', 'scm.id = p.sub_category_id', 'left');    
    $this->db->join('weights w', 'w.id = p.weight_band_id', 'left');
    $this->db->join('email_orders eo','eo.order_id = cp.order_id');
    $this->db->join('Receive_products rp','cp.id=rp.corporate_product_id AND ppl.karigar_id=rp.karigar_id','left');
    $this->db->where('ppl.karigar_id',$karigar_id);
    $this->db->where('ppl.status','1');
    $this->db->where('eo.id',$order_id);
    if(!empty($prodcut_code)){$this->db->where("(cp.sort_Item_number LIKE '%$prodcut_code%')");}
    if(!empty($category_name)){$this->db->where('c.name',$category_name);}   
    if(!empty($article_id)){$this->db->where('p.article_id',$article_id);}   
     
    $this->db->group_by('ppl.corporate_products_id');
    $result = $this->db->get()->result_array();
    //echo $this->db->last_query();
    return $result;
  }*/
  public function get_karigar_engaged_products($karigar_id,$order_id,$prodcut_code,$category_name,$article_id,$barcode_id,$old_val,$type){
    $this->db->select('scm.name as sub_cat_name, cp.weight_band ,cp.sort_Item_number,sum(rp.quantity) as rp_quantity,cp.order_id,cp.line_number as line_number,c.name cat_name,eo.id email_order_id,cp.id corporate_product_id,eo.corporate,sum(ppl.quantity) as quantity,ppl.*,p.corporate, cp.size as cr_size, cp.size_master,
(select round(( cp.quantity*tolerance / 100 ),2) from tolerance_master where cp.quantity >= from_wt  and cp.quantity  <= to_wt) as tolerance_wt,cp.quantity as cp_quantity,(select round(( cp.net_weight*tolerance / 100 ),2) from tolerance_master where cp.net_weight >= from_wt  and cp.net_weight  <= to_wt) as tolerance_c_wt,cp.net_weight,ppl.karigar_id as karigar_id,ppl.barcode_path as barcode_path,cp.order_id as order_id,ppl.id as barcode_id,cp.sort_Item_number as product_code');
    $this->db->from('Prepare_product_list ppl');
    $this->db->join('corporate_products cp','ppl.corporate_products_id = cp.id' ,'left');
    $this->db->join('product p','cp.sort_Item_number = p.product_code', 'left');
    $this->db->join('category_master c', 'c.id = p.category_id', 'left');
    $this->db->join('sub_category_master scm', 'scm.id = p.sub_category_id','left');    
    $this->db->join('weights w', 'w.id = p.weight_band_id', 'left');
    $this->db->join('email_orders eo','eo.order_id = cp.order_id');
    $this->db->join('Receive_products rp','cp.id=rp.corporate_product_id AND ppl.karigar_id=rp.karigar_id','left');
    
    $this->db->where('ppl.status','1');
    
    if(!empty($prodcut_code)){$this->db->where("(cp.sort_Item_number LIKE '%$prodcut_code%')");}
    if(!empty($category_name)){$this->db->where('c.name',$category_name);}   
    if(!empty($article_id)){$this->db->where('p.article_id',$article_id);} 
    if($type == "html"){
      $this->db->where('ppl.karigar_id',$karigar_id);
      $this->db->where('eo.id',$order_id);
       if(!empty($barcode_id)){
           $old_val= $old_val . $barcode_id;
            $old_val= explode(',',$old_val);
            $this->db->where_not_in('ppl.id',$old_val);
            //$this->db->group_by('ppl.corporate_products_id');
      } 
        $this->db->group_by('ppl.corporate_products_id');
         $result = $this->db->get()->result_array();
        // print_r($result);
         //echo $this->db->last_query();exit;

    }else{
      if(!empty($barcode_id)){
           //$old_val = $old_val . $barcode_id;
           //$old_val= explode(',',$old_val);
          //print_r($old_val);
        $this->db->where_in('ppl.id',$barcode_id);
       $this->db->group_by('ppl.corporate_products_id');
      } 

       $result = $this->db->get()->result_array();
      //echo $this->db->last_query();
      // print_r($result);exit;
    }
    

   
    return $result;
    
  }
    public function get_karigar_engaged_name($karigar_id){
    $this->db->select('name as karigar_name');
    $this->db->from('karigar_master');
    $this->db->where('id',$karigar_id);
    $result = $this->db->get()->result_array();
    return $result;
  }
}
