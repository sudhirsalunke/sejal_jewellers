<?php

class Corporate_Dashboard_model extends CI_Model {

  function __construct() {
      parent::__construct();
  }
  public function get_order_not_to_sent_karigar($corporate){
  	$this->db->select('DATEDIFF(NOW(), ppl.karigar_engaged_date) as created_at,count(ppl.quantity) as count');
    $this->db->from('Prepare_product_list ppl');
    $this->db->join('email_orders eo','eo.id = ppl.email_order_id');
    $this->db->where('ppl.status','1');
    $this->db->where('ppl.module','1');
    // if(isset($from_date) && $from_date !=''){
    //  $from_dt=date('Y-m-d',strtotime($from_date));
    // $this->db->where("ppl.karigar_engaged_date > date_format('".$from_dt."','%Y-%m-%d')");
    // }
    // if(isset($to_date) && $to_date !=''){    
    //   $to_dt=date('Y-m-d',strtotime($to_date));  
    //   $this->db->where("ppl.karigar_engaged_date < date_format('".$to_dt."','%Y-%m-%d')");
    // }
    if(isset($corporate) && $corporate !=''){      
      $this->db->where("eo.corporate",$corporate);
      }
    $this->db->having('created_at < 2');
    $result = $this->db->get()->row_array();
//echo $this->db->last_query();print_r($result);die;
    if($result['count'] == ''){
      return 0;
    }else{
    return $result['count'];
    }
  }
  public function get_received_products($corporate)
  {
    $this->db->select('DATEDIFF(NOW(), rp.date) as created_at,count(rp.quantity) as count');
    $this->db->from('Receive_products rp');
    $this->db->join('email_orders eo','eo.id = rp.email_order_id');
    $this->db->where('rp.status','1');
    $this->db->where('rp.module','1');
    if(isset($corporate) && $corporate !=''){      
      $this->db->where("eo.corporate",$corporate);
     }
    $this->db->having('created_at < 2'); 
    $result = $this->db->get()->row_array();
    //echo $this->db->last_query();print_r($result);die;
    if($result['count'] == ''){
      return 0;
    }else{
      return $result['count'];
    }
  }

  public function get_sent_to_hallmarking($corporate)
  {
    $this->db->select('DATEDIFF(NOW(), qc.HM_send_date) as created_at,count(qc.quantity) as count');
    $this->db->from('quality_control qc');
    $this->db->join('email_orders eo','eo.id = qc.email_order_id');
    $this->db->where('qc.status','2');
    $this->db->where('qc.module','1');
    // if(isset($from_date) && $from_date !=''){
    //  $from_dt=date('Y-m-d',strtotime($from_date));
    // $this->db->where("qc.HM_send_date > date_format('".$from_dt."','%Y-%m-%d')");
    // }
    // if(isset($to_date) && $to_date !=''){    
    //   $to_dt=date('Y-m-d',strtotime($to_date));  
    //   $this->db->where("qc.HM_send_date < date_format('".$to_dt."','%Y-%m-%d')");
    // }
    if(isset($corporate) && $corporate !=''){      
      $this->db->where("eo.corporate",$corporate);
     }
    $this->db->having('created_at < 2'); 
    $result = $this->db->get()->row_array();
    //echo $this->db->last_query();print_r($result);die;
    if($result['count'] == ''){
      return 0;
    }else{
      return $result['count'];
    }
  }
  public function get_hallmarking_qc_accept($corporate)
  {
    $this->db->select('DATEDIFF(NOW(), qch.HM_send_date) as created_at,count(qch.quantity) as count');
    $this->db->from('quality_control_hallmarking qch');
     $this->db->join('email_orders eo','eo.id = qch.email_order_id');
    $this->db->where('qch.status','8');
    $this->db->where('qch.module','1');
    // if(isset($from_date) && $from_date !=''){
    //  $from_dt=date('Y-m-d',strtotime($from_date));
    // $this->db->where("qch.HM_send_date > date_format('".$from_dt."','%Y-%m-%d')");
    // }
    // if(isset($to_date) && $to_date !=''){    
    //   $to_dt=date('Y-m-d',strtotime($to_date));  
    //   $this->db->where("qch.HM_send_date < date_format('".$to_dt."','%Y-%m-%d')");
    // }
    if(isset($corporate) && $corporate !=''){      
      $this->db->where("eo.corporate",$corporate);
     }
    $this->db->having('created_at < 2'); 
    $result = $this->db->get()->row_array();
    //echo $this->db->last_query();print_r($result);die;
    if($result['count'] == ''){
      return 0;
    }else{
      return $result['count'];
    }
  }
  public function get_products_set($corporate)
  {
    $this->db->select('DATEDIFF(NOW(), created_at) as created_at,sum(quantity)  as count');
    $this->db->from('gt_excel');
    $this->db->where('status','1');
    // if(isset($from_date) && $from_date !=''){
    //  $from_dt=date('Y-m-d',strtotime($from_date));
    // $this->db->where("created_at > date_format('".$from_dt."','%Y-%m-%d')");
    // }
    // if(isset($to_date) && $to_date !=''){    
    //   $to_dt=date('Y-m-d',strtotime($to_date));  
    //   $this->db->where("created_at < date_format('".$to_dt."','%Y-%m-%d')");
    // }
    if(isset($corporate) && $corporate !=''){      
      $this->db->where("corporate",$corporate);
     }
    $this->db->having('created_at < 2'); 
    $result = $this->db->get()->row_array();
    //echo $this->db->last_query();print_r($result);die;
    if($result['count'] == ''){
      return 0;
    }else{
      return $result['count'];
    }
  }
 



  public function get_email_order_data($corporate){
    $this->db->select('count(id) as count ,date_format(date,"%Y-%m-%d"),date');
    $this->db->from('email_orders');
    // if(isset($from_date) && $from_date !=''){
    //  $from_dt=date('Y-m-d',strtotime($from_date));
    // $this->db->where("date > date_format('".$from_dt."','%Y-%m-%d')");
    // }
    // if(isset($to_date) && $to_date !=''){    
    //   $to_dt=date('Y-m-d',strtotime($to_date));  
    //   $this->db->where("date < date_format('".$to_dt."','%Y-%m-%d')");
    // }
    if(isset($corporate) && $corporate !=''){
      
      $this->db->where("corporate",$corporate);
    }
 
    $result = $this->db->get()->row_array();
  //echo $this->db->last_query(); print_r($result); die;
    return $result['count'];
  } 


   public function get_design_count($corporate){
    $this->db->select('count(cp.sort_Item_number) as count');
    $this->db->from('corporate_products cp');
    $this->db->join('email_orders eo','eo.id = cp.email_order_id');
    // if(isset($from_date) && $from_date !=''){
    //  $from_dt=date('Y-m-d',strtotime($from_date));
    // $this->db->where("cp.created_at > date_format('".$from_dt."','%Y-%m-%d')");
    // }
    // if(isset($to_date) && $to_date !=''){    
    //   $to_dt=date('Y-m-d',strtotime($to_date));  
    //   $this->db->where("cp.created_at < date_format('".$to_dt."','%Y-%m-%d')");
    // }
    if(isset($corporate) && $corporate !=''){
      
      $this->db->where("eo.corporate",$corporate);
    }
 
    $result = $this->db->get()->row_array();
    return $result['count'];
  } 
  public function get_nt_assign_karigar_count($from_date,$to_date,$corporate){
    $this->db->select("DATEDIFF(NOW(), cp.created_at) as created_at_ctn,count(cp.CW_quantity) as count ,cp.work_order_id as order_id ,DATE_FORMAT(cp.created_at , '%d-%m-%Y') as order_date ,cp.sort_Item_number");       
    $this->db->from('corporate_products cp');
    $this->db->join('email_orders eo','eo.id = cp.email_order_id');
    $this->db->where('cp.status','0');
    if(isset($from_date) && $from_date !=''){
     $from_dt=date('Y-m-d',strtotime($from_date));
    $this->db->where("cp.created_at > date_format('".$from_dt."','%Y-%m-%d')");
    }
    if(isset($to_date) && $to_date !=''){    
      $to_dt=date('Y-m-d',strtotime($to_date));  
      $this->db->where("cp.created_at < date_format('".$to_dt."','%Y-%m-%d')");
    }
    if(isset($corporate) && $corporate !=''){
      
      $this->db->where("eo.corporate",$corporate);
    }
    $this->db->having('created_at_ctn < 2');
    $result = $this->db->get()->row_array();
    //echo $this->db->last_query();die;
    if($result['count'] == ''){
      return 0;
    }else{
    return $result['count'];
    }
  }

  private function fill_missing_data($result_array,$start_date,$end_date,$type){
    $exists_dates = array_column($result_array,'created_at');
    $period = new DatePeriod( new DateTime($start_date),new DateInterval('P1D'),new DateTime($end_date));
    $missing_dates = array();
//print_r($period);die;
    foreach ($period as $key => $value) {/*build missing days array*/
        if (!in_array($value->format('d-m-Y'),$exists_dates)) {
          if($type == 'order'){
            $missing_dates[]=array('order_count'=>0,'order_date'=>$value->format('d-m-Y'),'created_at'=>$value->format('d-m-Y'));
          }else{
              $missing_dates[]=array('design_count'=>0,'design_date'=>$value->format('d-m-Y'),'created_at'=>$value->format('d-m-Y'));
          }
        }
    }

    $result = array_merge($result_array,$missing_dates); /*merge array*/

    usort($result, array($this, "sort_by_created_at")); 

    return $result;
  }
  private function sort_by_created_at($a,$b){
        if ($a["created_at"] == $b["created_at"]) {
            return 0;
        }
        return ($a["created_at"] < $b["created_at"]) ? -1 : 1;
  }

  private function get_max_value($array=array()){
    $max=0;
    if (!empty($array)) {
      $max =MAX($array);
    }
    return $max;
  }
  public function get_email_order_data_chart($start_date,$end_date,$type,$corporate){
    $this->db->select('count(order_id) as order_count , DATE_FORMAT(created_at , "%d-%m-%Y") as order_date ,DATE_FORMAT(created_at , "%d-%m-%Y") as created_at ');
    $this->db->from('email_orders');
    //$this->db->where("date_format(created_at,'%Y-%m-%d') BETWEEN '".$start_date."' AND '".$end_date."'");
    $this->db->where("created_at > date_format('".$start_date."','%Y-%m-%d')");
    $this->db->where("created_at < date_format('".$end_date."','%Y-%m-%d')");
    if(isset($corporate) && $corporate !=''){      
      $this->db->where("corporate",$corporate);
    }
    $this->db->group_by('date_format(created_at,"%d-%m-%Y")');
    $result_array = $this->db->get()->result_array();
    $result = $this->fill_missing_data($result_array,$start_date,$end_date,$type);
    //echo $this->db->last_query(); print_r($result);die;
    $labels =json_encode(array_column($result, 'order_date'));
    $data =json_encode(array_column($result, 'order_count'));
    $max=$this->get_max_value(array_column($result, 'order_count'));      
    return array('labels'=>$labels,'data'=>$data,'max'=>$max+2);
    
  } 

  public function get_email_design_data_chart($start_date,$end_date,$type){
    $this->db->select('count(sort_Item_number) as design_count , DATE_FORMAT(created_at , "%d-%m-%Y") as design_date ,DATE_FORMAT(created_at , "%d-%m-%Y") as created_at ');
    $this->db->from('corporate_products');
    $this->db->where("date_format(created_at,'%Y-%m-%d') BETWEEN '".$start_date."' AND '".$end_date."'");
    $this->db->group_by('date_format(created_at,"%d-%m-%Y")');
    $result_array = $this->db->get()->result_array();
    $result = $this->fill_missing_data($result_array,$start_date,$end_date,$type);
    //echo $this->db->last_query(); print_r($result);die;
    $labels =json_encode(array_column($result, 'design_date'));
    $data =json_encode(array_column($result, 'design_count'));
    $max=$this->get_max_value(array_column($result, 'design_count'));      
    return array('labels'=>$labels,'data'=>$data,'max'=>$max+2);
    
  } 

  public function get_count_email_order_due_data($corporate="",$day){
       $today = date('Y-m-d');
      $check_today =Date('Y-m-d', strtotime("+".$day."days"));
     // $check_today ='2018-06-13';
    $this->db->select('count(DISTINCT cp.order_id) as count');
    $this->db->from('email_orders eo');
    $this->db->join('corporate_products cp','cp.email_order_id = eo.id');
    $this->db->join('Prepare_product_list ppl','ppl.corporate_products_id=cp.id');
    if($day == "exceed"){
     $this->db->where('cp.proposed_delivery_date <',$today); 
    }else if($day == "today"){
      $this->db->where('cp.proposed_delivery_date',$today); 
    }else{
     $this->db->where('cp.proposed_delivery_date',$check_today);  
    }
    if(isset($corporate) && $corporate !=''){
      
      $this->db->where("eo.corporate",$corporate);
    }
    $this->db->where('ppl.status','1');
    $result = $this->db->get()->row_array();
    if($result['count'] == ""){
      $result['count'] = 0;
    }
    return $result['count'];
  }

  public function get_email_order_due_data($params='',$search='',$limit='',$day,$type){
       $today = date('Y-m-d');

      $check_today =Date('Y-m-d', strtotime("+".$day."days"));
    $this->db->select('cp.order_id as order_no,eo.net_weight as weight,DATE(eo.date) as order_date,cp.proposed_delivery_date as delievery_date,count(cp.sort_Item_number) as no_of_products,c.name as corporate');
    $this->db->from('email_orders eo');
    $this->db->join('corporate_products cp','cp.email_order_id = eo.id');
    $this->db->join('corporate c','c.id = eo.corporate');
    $this->db->join('Prepare_product_list ppl','ppl.corporate_products_id=cp.id');
    if($type != 'all'){
      $this->db->where('eo.corporate',$type); 
    }
    if($day == "exceed"){
     $this->db->where('cp.proposed_delivery_date <',$today); 
    }else{
     $this->db->where('cp.proposed_delivery_date',$check_today);  
    }
    $this->db->where('ppl.status','1');
    $this->db->group_by('cp.order_id');
     if(isset($params['columns']) && !empty($params['columns'])){
      $filter_input=$params['columns'];      
     // print_r($filter_input);exit;
      $table_col_name="corporate_due_date_reminder";
    $this->get_filter_value($filter_input,$table_col_name);
    }
    /*if(!empty($search)){
      $this->db->where("(co.party_name LIKE '%$search%' OR km.name LIKE '%$search%' OR co.id LIKE '%$search%'OR co.order_name LIKE '%$search%' OR co.parent_category_id LIKE '%$search%')");
    }*/  
    if($limit == true){
      $this->db->order_by('eo.id','desc');
      $this->db->limit($params['length'],$params['start']);
      $result = $this->db->get()->result_array();
      //echo $this->db->last_query();print_r($result);exit;
    }else{
      $row_array = $this->db->get()->result_array();
      $result = count($row_array);
      //echo $this->db->last_query();print_r($result);exit;
    }
    return $result;
  }

    private function get_filter_value($filter_input,$table_col_name){
    $column_name=array();  
    $filter_column_name=filter_column_name($table_col_name);
    $sql='';
    $i=0;
     
    // print_r($filter_input);die;
    foreach ($filter_input as $key => $search_value){
       $column_name=$filter_column_name;
      //print_r($search_value['search']['value']);exit;
        if(!empty($search_value['search']['value'])){
          if($i != 0 && $column_name[$key] != "count(cp.sort_Item_number)" ){
            $sql.=' AND  ';
          }

          if($column_name[$key] == "count(cp.sort_Item_number)"){
          //  print_r($search_value['search']['value']);exit;
            $sql1="count('cp.sort_Item_number') =".$search_value['search']['value'].""; 
          }else{
             $sql.=''.$column_name[$key].' like "%'.$search_value['search']['value'].'%" ';
          }
           
            $i++;
        

         }   
    }

    if(!empty($sql)){  
      $this->db->where($sql); 
    } 
    if(!empty($sql1)){  
      $this->db->having($sql1); 
    } 
   
       
  }


}
