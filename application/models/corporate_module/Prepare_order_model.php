<?php

class Prepare_order_model extends CI_Model {

  function __construct() {
      $this->table_name = "corporate_products";
      parent::__construct();
  }
  public function validatepostdata(){
    $this->error=false;
   $data['status'] = 'success';
   $data['error'] ='';
    $this->form_validation->set_rules($this->config->item('Prepare_Order', 'admin_validationrules'));
    if($this->form_validation->run()===FALSE){
      $data['status']= 'failure';
      $data['data']= '';
      $data['error']['karigar'] = 'Please select Karigar';
    }
     elseif(!array_key_exists('products', $_POST)){
      $data['status']= 'failure';
      $data['data']= '';
      $data['error']['products'][0] = 'Please Select Atleast One Product';
    }
    elseif(array_key_exists('products', $_POST)){
      foreach ($_POST['products'] as $key => $value) {
        $corporate_products_id = $value;
        $quantity = $_POST['quantity'][$value][0];
        $res = $this->check_quantity($corporate_products_id,$quantity);
        if($res['status'] == 'failure'){
          $data['status'] = 'failure';
          $data['error']['products'.$corporate_products_id][0] = $res['error'];
        }
        //return $data;
      }
    }
    return $data;
  
  }
 
  public function get($karigar_id='',$limit=10,$offset=0,$is_grp_by=true){
    $this->db->select("corporate_products_id, sum(quantity) ppl_quantity");
    $this->db->from ('Prepare_product_list');
    if(!empty($karigar_id)){
       $this->db->where('corporate_products_id NOT IN (select corporate_products_id FROM Prepare_product_list where karigar_id = '.$karigar_id.' AND status = 1)');

      }
    
    $this->db->group_by('corporate_products_id');
    $sql_result = $this->db->get()->result_array();
   // echo $this->db->last_query();echo "<pre>"; print_r($sql_result);exit;; 
    
    $cp_arr= '';
    $cp_arr1= '';
    foreach ($sql_result as $key => $value) {
      $this->db->select("id as cpid");
      $this->db->from ('corporate_products');
      $this->db->where('id', $value['corporate_products_id']);
      $this->db->where('CW_quantity <= '.$value['ppl_quantity']);
      $cpid = $this->db->get()->row('cpid'); 

      if(!empty($cpid)){
          if(empty($cp_arr)){
            $cp_arr = "'".$cpid."'";
          }
          else
          {
            $cp_arr .= ",'".$cpid."'";
          }
      }
      $this->db->select("id as cpid");
      $this->db->from ('corporate_products');
      $this->db->where('id', $value['corporate_products_id']);
      $this->db->where('CW_quantity > '.$value['ppl_quantity']);
      $cpid1 = $this->db->get()->row('cpid'); 
      if(!empty($cpid1)){
          if(empty($cp_arr1)){
            $cp_arr1 = "'".$cpid1."'";
          }
          else
          {
            $cp_arr1 .= ",'".$cpid1."'";
          }
      }

    }

    $is_select = false;
    if(!empty($_POST['filter']['id'])){              
      $this->db->select("*,p.karigar_id product_karigar_id,cp.*,cp.id cp_id,SUM(CASE WHEN ppl.status != '2' THEN ppl.quantity ELSE 0 END) AS ppl_quantity,(CASE WHEN cp.id IN (".$_POST['filter']['id'].") THEN '1' ELSE '0' END ) as selected ,GROUP_CONCAT(DISTINCT `cp`.`id` ORDER BY `cp`.`id` ASC  SEPARATOR ',') as merge_id, sum(cp.CW_quantity) cw_quantity,cp.quantity as cp_weight,(select round(( cp.quantity*tolerance / 100 ),2) from tolerance_master where cp.quantity >= from_wt  and cp.quantity  <= to_wt) as tolerance_wt,(select round(( cp.net_weight*tolerance / 100 ),2) from tolerance_master where cp.net_weight >= from_wt  and cp.net_weight  <= to_wt) as tolerance_c_wt");
 
    }else{

      if(empty($_POST['filter']['id'])){
        unset($_POST['filter']['id']);
      }
          

      if(!empty($_POST['filter'])){
        foreach ($_POST['filter'] as $key => $value) {
          if(isset($value['name']) && $value['name'] == 'cp.id' && $value['value'] !== ''){
            $is_select=true;
            $this->db->select("*,cp.id ,p.karigar_id product_karigar_id,cp.id cp_id,SUM(CASE WHEN ppl.status != '2' THEN ppl.quantity ELSE 0 END) AS ppl_quantity,(CASE WHEN cp.id IN (".$value['value'].") THEN '1' ELSE '0' END ) as selected,cp.net_weight as cp_net_weight,cp.size as cp_size,sum(cp.CW_quantity) cw_quantity, GROUP_CONCAT(DISTINCT `cp`.`id` ORDER BY `cp`.`id` ASC  SEPARATOR ',') as merge_id,cp.quantity as cp_weight,(select round(( cp.quantity*tolerance / 100 ),2) from tolerance_master where cp.quantity >= from_wt  and cp.quantity  <= to_wt) as tolerance_wt,(select round(( cp.net_weight*tolerance / 100 ),2) from tolerance_master where cp.net_weight >= from_wt  and cp.net_weight  <= to_wt) as tolerance_c_wt");
          }
        }
          if($is_select==false){
            $this->db->select("*,cp.id ,cp.id cp_id,SUM(CASE WHEN ppl.status != '2' THEN ppl.quantity ELSE 0 END) AS ppl_quantity,cp.net_weight as cp_net_weight,cp.size as cp_size,p.karigar_id product_karigar_id,sum(cp.CW_quantity) cw_quantity, GROUP_CONCAT(DISTINCT `cp`.`id` ORDER BY `cp`.`id` ASC  SEPARATOR ',') as merge_id,cp.quantity as cp_weight,(select round(( cp.quantity*tolerance / 100 ),2) from tolerance_master where cp.quantity >= from_wt  and cp.quantity  <= to_wt) as tolerance_wt,(select round(( cp.net_weight*tolerance / 100 ),2) from tolerance_master where cp.net_weight >= from_wt  and cp.net_weight  <= to_wt) as tolerance_c_wt");
          }
      }else{
        $this->db->select("*,cp.id ,cp.id cp_id,SUM(CASE WHEN ppl.status != '2' THEN ppl.quantity ELSE 0 END) AS ppl_quantity,cp.net_weight as cp_net_weight,cp.size as cp_size,p.karigar_id product_karigar_id,sum(cp.CW_quantity) cw_quantity, GROUP_CONCAT(DISTINCT `cp`.`id`
        ORDER BY `cp`.`id` ASC  SEPARATOR ',') as merge_id,cp.quantity as cp_weight,(select round(( cp.quantity*tolerance / 100 ),2) from tolerance_master where cp.quantity >= from_wt  and cp.quantity  <= to_wt) as tolerance_wt,(select round(( cp.net_weight*tolerance / 100 ),2) from tolerance_master where cp.net_weight >= from_wt  and cp.net_weight  <= to_wt) as tolerance_c_wt");
      }
    }
    $this->db->from('corporate_products cp');
    $this->db->join('product p','cp.sort_Item_number = p.product_code');
    $this->db->join('weights w','w.id = p.weight_band_id','left');
    $this->db->join('email_orders eo','eo.order_id = cp.order_id');
    $this->db->join('Prepare_product_list ppl','ppl.corporate_products_id = cp.id','left');
   // print_r($karigar_id);
 // echo $this->db->last_query();die;
    if(!empty($karigar_id)){
     /* $this->db->or_where('(cp.id NOT IN (SELECT `corporate_products_id` FROM `Prepare_product_list` ppl where ppl.karigar_id !='.$karigar_id.' AND ppl.status = 1))');
      */
      $this->db->order_by('selected','DESC');
      $this->db->order_by('cp.id','ASC');
    }
    $this->db->where('cp.status!=','2');
    if(!empty($cp_arr)){  
   $this->db->group_start();
           $sale_ids_chunk = array_chunk(explode(',', $cp_arr),25);
          // print_r($sale_ids_chunk);exit;
           
       foreach($sale_ids_chunk as $cp_ar)
       {  
           $cp_ar_id = implode(',',$cp_ar);
           //print_r($cp_ar_id);exit;
           $this->db->where('cp.id NOT IN ('.$cp_ar_id.')');
          
       }
       
      
    }
      //$this->db->where('cp.id  NOT IN ('.$cp_arr.')');
    if(!empty($_POST['filter']['id'])){
      if($cp_arr1!='')
      {
        $this->db->or_where('`cp.id` IN ('.$_POST['filter']['id'].",".$cp_arr1.')');
      }
      else{
        $this->db->or_where('`cp.id`  IN ('.$_POST['filter']['id'].')'); /*existing or where-*/
      }
     
    }
    $this->where_coditions();
    $this->db->group_end();
    if($is_grp_by == true){
      $this->db->group_by('cp.weight_band,cp.size_master,cp.order_id,cp.sort_Item_number,cp.quantity');
    }else{
      $this->db->group_by('cp.id');
    }
    //$this->db->limit($limit,$offset*$limit);
    $data['result'] = $this->db->get()->result_array();
  // echo $this->db->last_query();
    //print_r($data);die();

    if(!empty($data['result'])){
      foreach ($data['result'] as $d_key => $d_value){
        if(isset($d_value['selected']) && $d_value['selected'] == '1'){
          //print_r($d_value['merge_id']);
          $quantity = $this->get_quantity_by_karigar_id($karigar_id,trim($d_value['merge_id']));
          if(!empty($quantity)){
            $data['result'][$d_key]['ppl_quantity'] = $quantity;
          }
        }
        $data['cp_ids'][$d_key] = trim($d_value['merge_id']);
      }
    }
    return $data;
  }


 /* private function get_quantity_by_karigar_id($karigar_id,$cp_id){
    $this->db->select('sum(quantity) as karigar_qnty');
    $this->db->from('Prepare_product_list');
    $this->db->where('karigar_id',$karigar_id);
    $this->db->where('corporate_products_id',$cp_id);
    $result = $this->db->get()->row_array();
    return $result['karigar_qnty'];
  }*/

   private function get_quantity_by_karigar_id($karigar_id,$cp_id){
    //print_r($cp_id);
    $this->db->select('sum(ppl.quantity) as karigar_qnty');
    $this->db->from('Prepare_product_list ppl');
    $this->db->join('corporate_products cp','cp.id = ppl.corporate_products_id','left');
    $this->db->join('email_orders eo','eo.order_id = cp.order_id','left');
    $this->db->where('ppl.karigar_id',$karigar_id);
    $this->db->where('ppl.corporate_products_id IN ('.$cp_id.')');
    $this->db->where('ppl.status!=','2');
    $this->db->group_by('cp.weight_band,cp.size_master,cp.order_id,cp.sort_Item_number');
    $result = $this->db->get()->row_array();
  /*    echo '<pre>';
    print_r($result);
    echo '</pre>';*/
    return $result['karigar_qnty'];
  }


  /*public function get($karigar_id='',$limit=10,$offset=0){
    $is_select = false;
    if(!empty($_POST['filter']['id'])){
      $this->db->select("*,cp.id cp_id,(CASE WHEN cp.id IN (".$_POST['filter']['id'].") THEN '1' ELSE '0' END ) as selected");
    }else{

      if(empty($_POST['filter']['id'])){
        unset($_POST['filter']['id']);
      }
      if(!empty($_POST['filter'])){
        foreach ($_POST['filter'] as $key => $value) {
          if(isset($value['name']) && $value['name'] == 'cp.id' && $value['value'] !== ''){
            $is_select=true;
            $this->db->select("*,cp.id cp_id,(CASE WHEN cp.id IN (".$value['value'].") THEN '1' ELSE '0' END ) as selected");
          }

        }
        if($is_select==false){
          $this->db->select("*,cp.id cp_id");
        }
      }else{
        $this->db->select("*,cp.id cp_id");
      }
    }
    $this->db->from('corporate_products cp');
    $this->db->join('product p','cp.sort_Item_number = p.product_code');
    $this->db->join('weights w','w.id = p.weight_band_id','left');
    $this->db->join('email_orders eo','eo.order_id = cp.order_id');
    if(!empty($karigar_id)){
      $this->db->or_where('cp.id NOT IN (SELECT `corporate_products_id` FROM `Prepare_product_list` ppl where ppl.karigar_id!='.$karigar_id.' OR ppl.status = "1") ');
    }else{
      $this->db->where('`cp.id` NOT IN (SELECT `corporate_products_id` FROM `Prepare_product_list`)');
    }
    $this->db->where('cp.status!=','2');
    if(!empty($_POST['filter']['id'])){
     $this->db->or_where('`cp.id` IN ('.$_POST['filter']['id'].')');
     
    }
    $this->where_coditions();
    $this->db->group_by('cp.id');
    $this->db->limit($limit,$offset*10);
    $data['result'] = $this->db->get()->result_array();
    // echo $this->db->last_query();
    // echo '<pre>';
    // print_r($data['result']);
    // echo '</pre>';die;
    if(!empty($data['result']))
      $data['page_count'] = $offset+1;
    echo $this->db->last_query();
    print_r($data); die();
    return $data;
  }*/
 public function store($insert_arr){
  //print_r($insert_arr);die;

    if(!empty($insert_arr['id'])){
      $this->db->where('`corporate_products_id` IN ('.$insert_arr['id'].')');
      if(!empty($insert_arr['prev_karigar_id'])){
        $this->db->where('karigar_id',$insert_arr['prev_karigar_id']);
        $this->db->where('status','0');
      }
      $this->db->delete('Prepare_product_list');
    }
    //echo $this->db->last_query();die;
    $data['status'] = 'failure';
   // print_r($insert_arr['quantity']);die;
    foreach ($insert_arr['products'] as $key => $value) {
      //print_r($value);
      $email_order_id = email_order_id($value); 
      $insert[$key]['created_at'] = date('Y-m-d H:i:s');
      $insert[$key]['karigar_engaged_date'] = date('Y-m-d');
      $insert[$key]['karigar_delivery_date'] = date("Y-m-d" , strtotime('+12 DAY'));
      if(!empty($insert_arr['action_type']) && $insert_arr['action_type']=="prepare_order"){
        $insert[$key]['karigar_id'] =@$insert_arr['karigar_id'];   
      }else{
        $insert[$key]['karigar_id'] =@$insert_arr['karigar_id'][$value][0];
      } 

      $insert[$key]['corporate_products_id'] =$value;
      $insert[$key]['email_order_id'] =@$email_order_id;      
      $insert[$key]['status'] ='0';
      $insert[$key]['order_name'] =@$insert_arr['order_name'][$value][0];
      $insert[$key]['sort_Item_number'] =@$insert_arr['sort_Item_number'][$value][0];      
      $insert[$key]['quantity'] =@$insert_arr['quantity'][$value][0];
      $insert[$key]['weight'] =@$insert_arr['weight'][$value][0];
    }
    //die;
    // print_r($insert);die;
    if($this->db->insert_batch('Prepare_product_list',$insert)){
        
     foreach ($insert as $key1 => $value1) {
      $inserted_id = $this->db->get_where('Prepare_product_list',array("karigar_id" => $value1['karigar_id'], "corporate_products_id" => $value1['corporate_products_id'], "status" => '0'))->result_array();
      foreach ($inserted_id as $key2 => $value2) {
      // print_r($value2);exit;
        $value=array();
         if(empty($value2['barcode_path'])){
          $value['barcode_path'] = $this->create_barcode($value2['id']);
        }
        //date_update('Prepare_product_list',$value2['id'],'insert','created_at','insert');
         $this->db->where('id',$value2['id']);
         $this->db->update('Prepare_product_list',$value);
      }
    }

      $data['karigar_id'] = $insert_arr['karigar_id'];
      $data['status'] = 'success';
    }
    foreach ($insert_arr['products'] as $key => $value) {
    
      $this->db->select('sum(ppl.quantity) quantity');
      $this->db->from('Prepare_product_list ppl');
      $this->db->where('ppl.corporate_products_id', $value);
      $ppl_quantity = $this->db->get()->row_array();
      $this->db->select('CW_quantity');
      $this->db->from('corporate_products cp');
      $this->db->where('cp.id', $value);
      $corporate_quantity = $this->db->get()->row_array();
      if($corporate_quantity['CW_quantity'] == $ppl_quantity['quantity'])
      {
        $this->db->set('status','1');
        $this->db->where('id',$value);
        $this->db->update('corporate_products');
      }
    }
    return $data;
  }

  private function getErrorMsg(){
    return  array(
      'products'=>strip_tags(form_error('products')),
      'quantity'=>strip_tags(form_error('quantity'))
        );
      
  }

  function itemNum_subStr($strin_data)
{
 $strin_data="12";
 echo substr($strin_data,2,13);

}
 private function create_barcode($product_code){
   if(!is_dir(FCPATH."uploads/barcode/$product_code")){
        mkdir(FCPATH."uploads/barcode/$product_code",0777);
    }
      $file = Zend_Barcode::draw('code128', 'image', array('text' => $product_code, 'drawText' => true), array());
     $store_image = imagepng($file,FCPATH."uploads/barcode/$product_code/".$product_code.".png");
     return "$product_code/".$product_code.".png";
  }

  private function where_coditions(){   
    $whr = array();

    if(!empty($_POST['filter'])){
      foreach ($_POST['filter'] as $key => $value) {
        if(!empty($value['value'])){
          $whr[$value['name']][] = $value['value'];
        }
      }
    }

    //print_r($whr);
    if(!empty($whr)){
      $whr_html='';
      $i = 0;
      $status = 0;
    foreach ($whr as $w_key => $w_value) {
      //print_r($w_value);
      if(count($w_value)>1){
          $j=0;
          if($i == 0)
            $whr_html .="(";
          else
            $whr_html .="AND(";
          foreach ($w_value as $k => $v) {
            $status = 1;
            if($j==0)
              $whr_html .= " $w_key = '$v' ";
            else
              $whr_html .= "OR $w_key = '$v' ";
            $j++;
          }
          $whr_html .=")";
        }else{
          if($i == 0){
            
            $whr_html .="(";
            if(strpos($w_key, 'LIKE') !== false){/*like query in AND*/
                $status = 1;
                $whr_html .=" ".str_replace('_LIKE', '', $w_key)." LIKE '%".$w_value['0']."%'";
            }elseif($w_key !== 'cp.id'){
              $status = 1;
              $whr_html .= " $w_key = '".$w_value['0']."' ";
            }else{
              $cp_whr1 = '';
              $explode1 = explode(',', $w_value['0']);
              foreach ($explode1 as $e_key1 => $e_value1) {
                if($e_key1 == 0)
                  $cp_whr1 .= "'".$e_value1."'";
                else
                  $cp_whr1 .= ",'".$e_value1."'";
              }
              $whr_html .= " $w_key in (".$cp_whr1.") ";
            }
            $whr_html .=")";
          }
          else{
            if(strpos($w_key, 'LIKE') !== false){/*like query in AND*/
                $status = 1;
                $whr_html .=" AND(".str_replace('_LIKE', '', $w_key)." LIKE '%".$w_value['0']."%')";
            }elseif($w_key !== 'cp.id'){
              $status = 1;
              $whr_html .="AND(";
              $whr_html .= " $w_key = '".$w_value['0']."' ";
              $whr_html .=")";

            }else{
             //  $cp_whr = '';
             //  $whr_html .="OR(";
             //  $explode = explode(',', $w_value['0']);
             //  foreach ($explode as $e_key => $e_value) {
             //    if($e_key == 0)
             //      $cp_whr .= "'".$e_value."'";
             //    else
             //      $cp_whr .= ",'".$e_value."'";
             //  }
             // // $whr_html .= " $w_key in (".$cp_whr.")";
             //  $whr_html .=")";
            }
          }
          
        }
        $i++;
      }
      if($status == 0)
        $this->db->or_where($whr_html);
      else
         $this->db->where($whr_html);
    }
  }
  public function get_products_by_order($order_id,$params,$limit){
    $this->db->select('ppl.*,cp.sort_Item_number,cp.proposed_delivery_date,p.image,km.name, DATE_FORMAT( ppl.karigar_engaged_date, "%d-%m-%Y" ) as karigar_engaged_date ,DATE_FORMAT( ppl.karigar_delivery_date, "%d-%m-%Y" ) as karigar_delivery_date,cp.size_master as size,cp.line_number,cp.quantity as Weight,cp.net_weight as net_weight');
    
    $this->db->from('Prepare_product_list ppl');
    $this->db->join('corporate_products cp','cp.id=ppl.corporate_products_id');
    $this->db->join('product p','p.product_code=cp.sort_Item_number');
    $this->db->join('karigar_master km','km.id=ppl.karigar_id');
    $this->db->where('cp.order_id',str_replace('%20',' ',$order_id));
    $this->db->where('ppl.status','1');
    $tbl_name='Products_sent_by_order';
    $filter_input=$params['columns'];
    $this->get_filter_value($filter_input,$tbl_name);

    if($limit == true){
        $this->db->order_by('ppl.id','DESC');
      if(!empty($params)){
        $this->db->limit($params['length'],$params['start']);
      }
        $result = $this->db->get()->result_array();
        //echo $this->db->last_query();print_r($result);exit;
    }else{
       $row_array = $this->db->get()->result_array();
       $result = count($row_array);
      //echo $this->db->last_query();print_r($result);exit;
    }



 /*   if (!empty($_REQUEST['search']['value'])) {
      $search = $_REQUEST['search']['value'];
      $this->db->where("(km.name like '%".$search."%' OR cp.sort_Item_number like '%".$search."%')");
    }*/
    /*select box*/
/*    if(!empty($params['columns'][1]['search']['value'])){
      $km_id=$params['columns'][1]['search']['value'];
      $this->db->where('km.id',$km_id);
    }*/

    //echo $this->db->last_query();print_r($result);exit();  
    return $result;
  }

  public function get_prepared_order($filter_status,$status,$params,$search,$limit){
/*    if (isset($_POST['corporate']) && !empty($_POST['corporate'])) {
       $this->db->where('eo.corporate',$_POST['corporate']);
    }*/
    if($limit == true){
      $this->db->select("sum(IF(ppl.status = '1', 1, 0)) assign_products,eo.*,c.name corporate, DATE_FORMAT( ppl.karigar_engaged_date,'%d-%m-%Y' ) as karigar_engaged_date,DATE_FORMAT( DATE_ADD(ppl.karigar_engaged_date, INTERVAL 12 DAY),'%d-%m-%Y' ) as delivery_date");
    }else{
      $this->db->select('count(distinct (eo.id)) as cont_record');
    }
    $this->db->from('email_orders eo');
    $this->db->join('corporate_products cp','eo.order_id=cp.order_id');
    $this->db->join('corporate c','c.id=eo.corporate');
    $this->db->join('Prepare_product_list ppl','ppl.corporate_products_id=cp.id');
    $this->db->where('ppl.status','1');

    $filter_input=$params['columns']; 
    $tbl_name='Products_sent';    
    $this->get_filter_value($filter_input,$tbl_name);
    if(!empty($filter_status)){
      $this->db->order_by($status[$filter_status['column']],$filter_status['dir']);
    } 

    if($limit == true){
    $this->db->group_by('eo.id');
      $this->db->order_by('eo.id','DESC');
      $this->db->limit($params['length'],$params['start']);
      $result = $this->db->get()->result_array();
      //echo $this->db->last_query(); print_r($result);     
    }else{
      $row_array = $this->db->get()->row_array();
      $result = $row_array['cont_record'];
      //echo $this->db->last_query(); echo $result; die;      
    }
    //$result = $this->db->get()->result_array();
  // echo $this->db->last_query();die;

    return $result;
  }

 
  public function email_orders_date($order_id){
    $this->db->select('DATE_FORMAT(ppl.karigar_engaged_date,"%d-%m-%Y" ) as karigar_engaged_date');
    $this->db->from('Prepare_product_list ppl');
    $this->db->join('corporate_products cp','ppl.corporate_products_id=cp.id');
    $this->db->where('cp.work_order_id',$order_id);
    $result = $this->db->get()->row_array();
    return $result['karigar_engaged_date'];
  }
  private function get_filter_value($filter_input,$tbl_name){
  $column_name=array();  
 
  
  $filter_column_name=filter_column_name($tbl_name);
  $sql='';
  $i=0;
   
   //print_r($filter_input);die;
  foreach ($filter_input as $key => $search_value){
     $column_name=$filter_column_name;
     //print_r($search_value['search']['value']);
      if(!empty($search_value['search']['value'])){
        if($i != 0){
          $sql.=' AND  ';
        }
          $sql.=''.$column_name[$key].' like "%'.$search_value['search']['value'].'%" ';
          $i++;
      

       }   
  }

  if(!empty($sql)){  
    $this->db->where($sql);  
  } 
   
       
  }


  public function check_quantity($corporate_products_id,$quantity)
  {  
 
    if($quantity=='' || $quantity=='0' || (is_numeric($quantity) && strpos($quantity, ".") !== false))
    {
      $this->error = true;
      $data['status'] = 'failure';
      $data['error'] = 'Please enter valid Quantity.';
    }
    elseif(!is_numeric($quantity))
    {
      $this->error = true;
      $data['status'] = 'failure';
      $data['error'] = 'Please enter numeric value.';
    }
    else{
      $data['status'] = 'success';
      $data['error'] = '';
      $this->db->select('CW_quantity as corporate_quantity');
      $this->db->from('corporate_products cp');
      $this->db->where('id', $corporate_products_id);
      $corporate_quantity = $this->db->get()->row('corporate_quantity');
      $this->db->select('sum(ppl.quantity) CW_quantity');
      $this->db->from('Prepare_product_list ppl');
      $this->db->where('corporate_products_id', $corporate_products_id);
      $ppl_quantity = $this->db->get()->row('CW_quantity');
      $this->db->select('karigar_id');
      $this->db->from('Prepare_product_list ppl');
      $this->db->where('corporate_products_id', $corporate_products_id);
      $this->db->where('status','0');
      $ppl_karigar = $this->db->get()->row('karigar_id');
      $karigar_id = $_POST['karigar_id'];
      if($karigar_id==$ppl_karigar)
      {
        $total_qnty = $quantity;
      }elseif($_POST['mode']=='edit')
      {
        $total_qnty = $quantity;
      }
      else
      {
        $total_qnty = $quantity + $ppl_quantity;
      }
      if ($total_qnty > $corporate_quantity) {
          $this->error = true;
          $data['status'] = 'failure';
          $data['error'] = 'Quantity limit Exceeded.';
      } 
    }   
      return $data;
  }

  /*karigar list*/
    public function get_karigar_by_order($order_id){
    $this->db->select('count(ppl.id) as total,km.name,km.id');
    $this->db->from('Prepare_product_list ppl');
    $this->db->join('corporate_products cp','cp.id=ppl.corporate_products_id');
    $this->db->join('product p','p.product_code=cp.sort_Item_number');
    $this->db->join('karigar_master km','km.id=ppl.karigar_id');
    $this->db->where('cp.order_id',$order_id);
    $this->db->where('ppl.status','1');
    $this->db->group_by('km.id');
    $result = $this->db->get()->result_array();
    return $result;
  }

  /*export data*/
   public function get_products_by_order_excel_export($order_id,$search,$karigar){
    $this->db->select('cp.order_id as Order_NO,cp.line_Number as Line_Number,cp.item_number as Item_ID,cp.sort_Item_number as Code,weight_band as Weight,km.name as Karigar_Name, DATE_FORMAT( ppl.karigar_engaged_date,  "%d-%m-%Y" ) as Order_Given_Date,DATE_FORMAT(ppl.karigar_delivery_date,  "%d-%m-%Y" ) as  Proposed_Delivery_Date,p.image,cp.size_master as size,cp.quantity as cp_weight,cp.net_weight,eo.corporate');
    //$this->db->select('ppl.*,cp.sort_Item_number,cp.proposed_delivery_date,p.image,km.name, DATE_FORMAT( ppl.karigar_engaged_date,  "%d-%m-%Y" ) as karigar_engaged_date');
    $this->db->from('Prepare_product_list ppl');
    $this->db->join('corporate_products cp','cp.id=ppl.corporate_products_id');
    $this->db->join('email_orders eo','eo.id=cp.email_order_id');
    $this->db->join('product p','p.product_code=cp.sort_Item_number');
    $this->db->join('karigar_master km','km.id=ppl.karigar_id');
    $this->db->where('cp.order_id',$order_id);
    $this->db->where('ppl.status','1');
   
    if (!empty($search)) {
      $this->db->where("(km.name like '%".$search."%' OR cp.sort_Item_number like '%".$search."%')");
    }
    if(!empty($karigar)){
      $this->db->where('km.id',$karigar);
    }
     $this->db->order_by('ppl.id','DESC');

    $result = $this->db->get()->result_array();
    return $result;
  }

  public function get_filters($cp_ids,$filters){
    $cp_ids = implode(',',$cp_ids);
    $result['orders'] = $this->get_orders($cp_ids,$filters);
    $result['categories'] = $this->get_categories($cp_ids,$filters);
    $result['weight_range'] =  $this->get_weight_ranges($cp_ids,$filters);
    return $result;
  }
  private function get_orders($ids,$filters){
    $this->db->select('count(cp.id)-count(ppl.id) as pending_products,count(cp.id) total_products,eo.*,c.name corporate,c.id as corporate_id');
    $this->db->from('email_orders eo');
    $this->db->join('corporate_products cp','eo.order_id=cp.order_id');
    $this->db->join('Prepare_product_list ppl','cp.id=ppl.corporate_products_id','left');
    $this->db->join('corporate c','c.id=eo.corporate');
    $this->db->where('eo.status','1');
    $this->db->where("cp.id in ($ids)");
    $this->db->group_by('eo.id');
    $result = $this->db->get()->result_array();
    if(!empty($result) && !empty($filters)){
      foreach ($result as $key => $value) {
        foreach ($filters as $f_key => $f_value) {
          if($f_value['name'] == 'cp.order_id'){
            if($f_value['value'] == $value['order_id']){
              $result[$key]['selected'] = '1';
            }
          }
        }
      }
    }
    return $result;
  }
  private function get_categories($ids,$filters){
    // $this->db->select('DISTINCT(product_master), COUNT(id) as product_count');
    $this->db->select('DISTINCT(product_master),(select COUNT(id) from corporate_products where id in('.$ids.') and product_master = cp.product_master) as product_count');
    $this->db->from('corporate_products cp');
    $this->db->where('cp.product_master !=','Is null');
    $this->db->where('cp.product_master !=','');
    $this->db->where('cp.status =','0');
    // $this->db->where("cp.id in ($ids)");
    $this->db->group_by('product_master');
    $result = $this->db->get()->result_array();
    // echo $this->db->last_query();die;
    if(!empty($result) && !empty($filters)){
      foreach ($result as $key => $value) {
        foreach ($filters as $f_key => $f_value) {
          if($f_value['name'] == 'cp.product_master'){
            if($f_value['value'] == $value['product_master']){
              $result[$key]['selected'] = '1';
            }
          }
        }
      }
    }
    return $result;
  }



  function get_weight_ranges($ids,$filters){
    $this->db->select('DISTINCT(weight_band)');
    $this->db->where('weight_band !=','Is null');
    $this->db->from('corporate_products');
    $this->db->where("id in ($ids)");
    $result = $this->db->get()->result_array();
    if(!empty($result) && !empty($filters)){
      foreach ($result as $key => $value) {
        foreach ($filters as $f_key => $f_value) {
          if($f_value['name'] == 'cp.weight_band'){
            if($f_value['value'] == $value['weight_band']){
              $result[$key]['selected'] = '1';
            }
          }
        }
      }
    }
    return $result;
  }
 

  public function due_date_reminder_mail(){
    $delivery_date = Date('Y-m-d', strtotime("+3 days"));
    //$delivery_date ="2018-03-09";
    //print_r($delivery_date);exit;
    $this->db->select('km.name as Karigar_Name,cp.order_id as Order_No,cp.sort_Item_number as Product_Code,cp.line_number as Line_Number,cp.quantity as Weight');
    $this->db->from('email_orders eo');
    $this->db->join('corporate_products cp','eo.order_id=cp.order_id');
    $this->db->join('corporate c','c.id=eo.corporate');
    $this->db->join('Prepare_product_list ppl','ppl.corporate_products_id=cp.id');
    $this->db->join('karigar_master km','ppl.karigar_id=km.id');
    $this->db->where('ppl.status','1');
    $this->db->where('cp.proposed_delivery_date',$delivery_date);
    $data = $this->db->get()->result_array();
   //echo $this->db->last_query();exit;
    // echo "<pre>";print_r($data);exit;
   if(!empty($data)){
   $filename="d_date_reminder_mail".date('YmdHis').'.xlsx';
    $headings = array('Karigar_Name','Order_No','Product_Code','Line_Number','Weight');
    $this->excel_lib->export($data,$headings,$filename,'','',$save=true);
      $view ='Products_sent/due_date_reminder';
      $subject ='Karigar Due Date Reminder';
      $from_name = "sejal jewellers";
      $email = "Nilesh@shilpijewels.com,KAUSHIK@shilpijewels.com";
      $upload_path = dirname(dirname(dirname(dirname(__FILE__)))) ."/uploads/excel/";
      $this->email_lib->send($data,$email,$subject,$view,'order@shilpijewels.com',$from_name,$upload_path,$filename,"excel");
    }   
  }

}
