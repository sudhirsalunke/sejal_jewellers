<?php
class Stock_model extends CI_Model {

  function __construct() {
  	$this->table_name = "stock";
    parent::__construct();
  }
  public function store($corporate_product_id,$status=5,$corporate='',$rp_id){
      if($corporate !=='1'){
      $email_order_id = email_order_id($corporate_product_id);  
      $insert_array['created_at'] = date('Y-m-d H:i:s');
      $insert_array['updated_at'] = date('Y-m-d H:i:s');
      $insert_array['corporate'] = $corporate;
      $insert_array['corporate_product_id'] = $corporate_product_id;
      $insert_array['email_order_id']=$email_order_id;
      $insert_array['receive_product_id'] = $rp_id;
      $this->db->insert($this->table_name,$insert_array);
    }
    $this->db->set('status',$status);
    $this->db->where('corporate_product_id',$corporate_product_id);
    $this->db->where('receive_product_id',$rp_id);
    $this->db->update('quality_control');
    return get_successMsg();  	
  }

  public function get($filter_status='',$status='',$params='',$search='',$limit='',$corporate='') {

    if($limit == true)
      $this->db->limit($params['length'],$params['start']);  

    //echo $this->db->last_query(); die();
    if(empty($_GET['status'])  || @$_GET['status']=="stock"){
    $this->db->select('s.*,s.created_at,s.quantity,cp.sort_Item_number as challan_no,qc.total_gr_wt as gross_gm,qc.total_net_wt as net_gm, qc.cp_size as size,cm.name as category_name');
    $this->db->from($this->table_name.' s');
    $this->db->join('corporate_products cp','cp.id = s.corporate_product_id');
    $this->db->join('quality_control qc','qc.id = s.qc_id','LEFT');
    $this->db->join('product p','p.product_code = cp.sort_Item_number');
    $this->db->join('category_master cm','cm.id = p.category_id');
    $this->db->where('s.status','1');
    $this->db->order_by('s.id','DESC');    
    }else{    
      $this->where_cond($corporate);
      $this->db->from('gt_excel ge');
     
     //$this->db->group_by('ge.challan_no');
      if(empty($_GET['status'])  || @$_GET['status']=="sent"){
        $this->db->select('ge.*,sum(ge.quantity) as quantity,sum(ge.gross_wt_gm) gross_gm,sum(ge.net_wt_gm) as net_gm,sum(ge.mk_value) total_amount,ge.id');   
        $this->db->group_by('ge.challan_no');     
      }else{
        $this->db->select('ge.*,ge.quantity as quantity,ge.gross_wt_gm gross_gm,ge.net_wt_gm as net_gm,ge.mk_value total_amount,ge.id');     
         }
        $this->db->order_by('ge.id','DESC');
  } 
      
      if(empty($_GET['status'])  || @$_GET['status']=="stock"){
         
        $table_col_name='Stock';
        }else{
        
          $table_col_name='All_stock_products';
        }
      
   if (isset($params['columns']) && !empty($params['columns'])){
      $filter_input=$params['columns'];
      $this->get_filter_value($filter_input,$table_col_name);
    }
  /* if(!empty($search)){
       
    $this->all_like_queries($search,$status);
        
    }*/
    /* if(!empty($filter_status)){
      $this->db->order_by($status[$filter_status['column']],$filter_status['dir']);
    } */
 // echo $this->db->last_query(); die();
    
   //  print_r($corporate);

    if($limit == true){
      $this->db->limit($params['length'],$params['start']);
      $result = $this->db->get()->result_array();
    }else{
      $row_array = $this->db->get()->result_array();
      $result = count($row_array);
      //echo $this->db->last_query();print_r($result);exit;
    }

    return $result;
  }

  private function where_cond($corporate){

  if(!empty($_GET['status']) && $_GET['status'] == 'sent'){
      $this->db->where('ge.status','1');
    }
    else if(!empty($_GET['status']) && $_GET['status'] == 'accepted'){
      $this->db->where('ge.status','2');
    }
    else if(!empty($_GET['status']) && $_GET['status'] == 'rejected'){
      $this->db->where('(ge.status ="0")');
    }  
    
    if(!empty($corporate)){
      $this->db->where('ge.corporate',$corporate);
    }

  }

  private function get_filter_value($filter_input,$table_col_name){
    $column_name=array();  
    $filter_column_name=filter_column_name($table_col_name);
    $sql='';
    $i=0;
     
     //print_r($filter_input);die;
    foreach ($filter_input as $key => $search_value){
       $column_name=$filter_column_name;
       //print_r($search_value['search']['value']);
        if(!empty($search_value['search']['value'])){
          if($i != 0){
            $sql.=' AND  ';
          }
            $sql.=''.$column_name[$key].' like "%'.$search_value['search']['value'].'%" ';
            $i++;
        

         }   
    }

    if(!empty($sql)){  
      $this->db->where($sql);  
    }     
       
  }
  private function all_join_queries(){
    $this->db->join('corporate_products cp','cp.id = s.corporate_product_id');
    $this->db->join('quality_control qc','qc.corporate_product_id = cp.id');
    $this->db->join('Receive_products rp','rp.corporate_product_id = cp.id');
  }
  private function all_like_queries($search,$status){
      if($_GET['status'] =="stock"){
           $this->db->where("(`cp`.`sort_Item_number` LIKE '%$search%' OR cp.order_id LIKE '%$search%' OR date_format(s.created_at,'%d-%m-%Y') LIKE '%$search%')");
      }else{
        $this->db->where("(ge.challan_no LIKE '%$search%' OR date_format(ge.created_at,'%d-%m-%Y') LIKE '%$search%')");
      } 
  }

  // public function approve($id=''){
  //   $this->db->set('status','2');
  //   $this->db->where('challan_no',$_POST['id']);
  //   if($this->db->update('gt_excel')){
  //     return true;
  //   }
  //   else{
  //     return false;
  //   }
  // }
  // public function Reject($id=''){
  //   $this->db->set('status','0');
  //   $this->db->where('challan_no',$_POST['id']);
  //   if($this->db->update('gt_excel')){
  //     return true;
  //   }
  //   else{
  //     return false;
  //   }
  // }

  public function find_gt_excel($id){
    $this->db->where('id',$id);
   return $this->db->get('gt_excel')->row_array();
  }

  public function get_karigar_id_ppl($cp_id){
   $this->db->select('karigar_id');
   $this->db->where('corporate_products_id',$cp_id);
   $result = $this->db->get('Prepare_product_list')->row_array();
   return @$result['karigar_id'];

  }

  public function get_receive_product_id($cp_id){
   $this->db->select('id');
   $this->db->where('corporate_product_id',$cp_id);
   $result = $this->db->get('Receive_products')->row_array();
   return @$result['id'];

  }
  

  public function send_to_karigar($result){
/*    if(!empty($result['karigar_id'])){
        $this->db->where('karigar_id',$result['karigar_id']);
        $this->db->where('status','0');
        $this->db->delete('Prepare_product_list');
        }*/
        $email_order_id = email_order_id($result['corporate_product_id']);
        $insert_array = array(
        'corporate_products_id'=>$result['corporate_product_id'],
        'email_order_id'=>$email_order_id,
        'quantity'=>$result['quantity'],
        'karigar_id'=>$result['karigar_id'],
        'gt_excel_id'=>$result['id'],
        'status'=>'0',
        'created_at'=>date('Y-m-d H:i:sa'),
        'updated_at'=>date('Y-m-d H:i:sa'),
        'karigar_engaged_date' => date('Y-m-d'),
        'karigar_delivery_date' => date("Y-m-d" , strtotime('+12 DAY')),
      );
    if($this->db->insert('Prepare_product_list',$insert_array)){
      $insert_array = array(
        'corporate_product_id'=>$result['corporate_product_id'],
        'email_order_id'=>$email_order_id,
        'gt_excel_id'=>$result['id'],
        'quantity'=>$result['quantity'],
        'karigar_id'=>$result['karigar_id'],
        'corporate'=>$result['corporate'],
        'receive_product_id'=>$result['receive_product_id'],
        'status'=>'3',
        'created_at'=>date('Y-m-d H:i:sa'),
        'updated_at'=>date('Y-m-d H:i:sa'),
      );

      if($this->db->insert('stock',$insert_array)){
      $this->db->set('status','3');
      $this->db->where('id',$result['id']);
      $this->db->update('gt_excel');
       $this->db->set('status','5');
      $this->db->where('id',$result['id']);
      $this->db->update('quality_control');
      corporate_product_update($email_order_id,'1',$result['item']);//status 1-NOT SENT TO KARIGAR Reliance
      }
     $response=get_successMsg();
     $response['karigar_id'] =$result['karigar_id'];
     return $response;
    }else{
      return get_errorMsg();
    }
  
  }

  public function send_to_stock($result){
   $email_order_id = email_order_id($result['corporate_product_id']); 
   $insert_array = array(
        'corporate_product_id'=>$result['corporate_product_id'],
        'gt_excel_id'=>$result['id'],
        'email_order_id'=>$email_order_id,
        'qc_id'  =>$result['qc_id'],
        'quantity'=>$result['quantity'],
        'karigar_id'=>$result['karigar_id'],
        'corporate'=>$result['corporate'],
        'receive_product_id'=>$result['receive_product_id'],
        'status'=>'1',
        'created_at'=>date('Y-m-d H:i:sa'),
        'updated_at'=>date('Y-m-d H:i:sa'),
      );
      if($this->db->insert('stock',$insert_array)){      
        $this->db->set('status','4');
        $this->db->where('id',$result['id']);
        $this->db->update('gt_excel');
        corporate_product_update($email_order_id,'9',$result['item']);//status 9-STOCK PRODUCTS
        return get_successMsg();
      }else{
        return get_errorMsg();
      }
  }
  public function update($update_array,$update_where){
    $this->db->where($update_where);
    $this->db->update($this->table_name,$update_array);
  }
  public function find($st_id){
    $this->db->where('id',$st_id);
    return $this->db->get($this->table_name)->row_array();
  }

  public function insert_in_prepared_order($result){
    $email_order_id = email_order_id($result['corporate_product_id']);
    $insert_array = array(
        'corporate_products_id'=>$result['corporate_product_id'],
        'email_order_id'=>$email_order_id,
        'quantity'=>$result['quantity'],
        'karigar_id'=>$result['karigar_id'],
        'stock_id'=>$result['id'],
        'status'=>'0',
        'created_at'=>date('Y-m-d H:i:sa'),
        'updated_at'=>date('Y-m-d H:i:sa'),
        'karigar_engaged_date' => date('Y-m-d'),
        'karigar_delivery_date' => date("Y-m-d" , strtotime('+12 DAY')),
      );
    //print_r($insert_array);die;
    if($this->db->insert('Prepare_product_list',$insert_array)){
      return true;
    }else{
      return false;
    }
  }

}