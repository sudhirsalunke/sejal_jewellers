<?php
class Search_order_model extends CI_Model {

  function __construct() {
      $this->table_name = "email_orders";
      parent::__construct();
  }
  public function validatepostdata(){ 
   
  	$imagevalidation = true;
    //$_POST['Order']['gross_weight'];
    //print_r($_FILES['file']);die;
  	$this->form_validation->set_rules($this->config->item('search_order', 'admin_validationrules'));
    if (isset($_POST['Order']['corporate']) && $_POST['Order']['corporate'] ==1) {
     $this->form_validation->set_rules('Order[order_id]', 'Order Id', 'trim|required');
    }
    $data['status']= 'success';
  if (empty($_FILES['file']['name']))
	{
		$imagevalidation = false;
	}else{
    $status = imageValidation($_FILES['file'],array('xlsx','xls'));
    if($status['status'] == 'fail'){
      $data['status']= 'failure';
      $data['error'][0] = "Invalid file format of an uploaded file ,Only 'xlsx,xls allowed'";
      echo json_encode($data);die;
    }
  }
    if($this->form_validation->run()===FALSE || $imagevalidation == false){
      $data['status']= 'failure';
      $data['data']= '';
      //$data['error'] = $this->getErrorMsg();
      $err = $this->getErrorMsg();
      $i=0;
      foreach ($err as $key => $value) {
        $data['error'][$i] = $value;
        $i++;
      }
    }
    return $data;
  }
  public function check_exits_caratlane_order($order_id){
    $this->db->where('order_id',$order_id);
    $this->db->from('email_orders');
   return $this->db->count_all_results(); 

  }

  public function ChkDuplicateExceldata($insert_array = '',$from_excel=false){
    $data = array();
    $product_code = array();
    foreach ($insert_array as $key => $value) {
      $rowno = $key+2;
      $code = $value['external_item_number'];
      // if(!in_array($value['external_item_number'], $product_code)){
      //   $product_code[] = $value['external_item_number'];
      // }else{
      //   $data['error'][$key] = 'Row '.$rowno.' External Item Number '.$value['external_item_number'].' repeated' ;
      //   return $data;
      // }
        if (!empty($code)) {
            $this->db->select('*');
            $this->db->where('external_item_number', $code);
            $this->db->where('order_id', $_POST['Order']['order_id']);
              $query = $this->db->get('corporate_products')->row_array();
              if (count($query) > 0) {
                  $data['error'][$key] = 'Row '.$rowno.' External Item Number '.$value['external_item_number'].' have already exist' ;
              } else {
                //$data['error'] ='';
              }
            }else{
              $data['error'][$key] = 'Row '.$rowno.' have blank data' ;
           // return false;
        }
      }
      return $data;
  }
  public function validateExistExceldata($insert_array = '',$from_excel=false){
    $data = array();
    $corporate_id = $_POST['Order']['corporate'];
    $net_weight = 0;
    $weight_band=0;
    $gross_weight  = $_POST['Order']['gross_weight'];
    $exits_line_number=array();
/*    print_r($corporate_id);
     print_r($insert_array);die;*/
    $sheet_check_order_no=array();
    foreach ($insert_array as $key => $value){
      $rowno = $key+2;
/*      print_r($value);

       if(empty($value['line_number'])){
        echo "Excel Format is wrong. Please upload correct excel format";die;
         $data['error'][] = "Excel Format is wrong. Please upload correct excel format";
       }else{
       }die;*/
      if($corporate_id == 1){
         if(empty($value['line_number'])){
            //echo "Excel Format is wrong. Please upload correct excel format";
            $data['error'][] = "Excel Format is wrong. Please upload correct excel format";
             return $data;die;
          }else{
            $sort_number = itemNum_subStr(@$value['item_number']);
            $code = $sort_number;
          }
      }
      else{
        if(!empty($value['line_number'])){
           // echo "Excel Format is wrong. Please upload correct excel format";
            $data['error'][] = "Excel Format is wrong. Please upload correct excel format ";
             return $data;die;
           }else{
          $sort_number = @$value['external_item_number'];
          $code = @$value['external_item_number'];
         }
      }

        if ($corporate_id==2) {
               //print_r($value);
       if(!empty($value['line_number'])){
        //echo "Excel Format is wrong. Please upload correct excel format";die;
         $data['error'][] = "Excel Format is wrong. Please upload correct excel format";
                 return $data;die;
       }else{
              $order_exits =$this->check_exits_caratlane_order($value['work_order_id']);
           
              if($order_exits >=1){
                $data['error'][] = 'Row '.$rowno.' : Order Id '.$value['work_order_id'].' Already Exits' ;
              }
              else{
                $sheet_check_order_no[]=$value['work_order_id'];
              }           
              if(!isset($value['net_weight']) || empty($value['net_weight']) || $value['net_weight'] ==0.00){
                 $data['error'][] = 'Row '.$rowno.' Net Weight '.$value['net_weight'].' Should not be empty' ;
              }
              if(!isset($value['product_code_shilpi']) || empty($value['product_code_shilpi'])){
               echo $value['product_code_shilpi'];
                 $data['error'][] = 'Row '.$rowno.' Product code shilpi '.$value['product_code_shilpi'].' Should not be empty' ;
              } 
              $net_weight+= $value['net_weight'];
        }

      }else{
              if (isset($value['item_number']) && !empty($value['item_number'])) {
            
                $item_no = $value['item_number'];
                $item_no_decoed =decode_item($item_no);
                //print_r($item_no_decoed['weight_band']);
                //$mid_value=get_mid_value($item_no_decoed['weight_band']);
                $weight_band+=$value['quantity'];


                if(count($item_no_decoed['karatage'])==0){
                   $data['error'][] = 'Row '.$rowno.' Item No '.substr($item_no,0,1).' wrong karatage Number' ;
                }
                 if(empty($item_no_decoed['product_master'])){
                   $data['error'][] = 'Row '.$rowno.' Item No '.substr($item_no,1,3).' wrong Product Master' ;
                }
                 if(empty($item_no_decoed['complexity'])){
                   $data['error'][] = 'Row '.$rowno.' Item No '.substr($item_no,4,3).' wrong Complexity' ;
                }
              
                if(empty($item_no_decoed['weight_band'])){
                   $data['error'][] = 'Row '.$rowno.' Item No '.substr($item_no,7,1).' wrong weight_band' ;
                }

                if(empty($item_no_decoed['vendor_code'])){
                   $data['error'][] = 'Row '.$rowno.' Item No '.substr($item_no,8,2).' wrong vendor_code' ;
                }
                 if(empty($item_no_decoed['serial_no'])){
                   $data['error'][] = 'Row '.$rowno.' Item No '.substr($item_no,10,4).' wrong serial_no' ;
                }
                 if(empty($item_no_decoed['size_master'])){
                   $data['error'][] = 'Row '.$rowno.' Item No '.substr($item_no,14,2).' wrong size_master' ;
                }
                 if(empty($item_no_decoed['finding_cost'])){
                   $data['error'][] = 'Row '.$rowno.' Item No '.substr($item_no,16,2).' wrong Finding Cost' ;
                }
                if(empty($item_no_decoed['stone_cold'])){
                 $data['error'][] = 'Row '.$rowno.' Item No '.substr($item_no,18,2).' wrong Stone Cold' ;
                }
              }
              if (isset($value['line_number']) && !empty($value['line_number'])) {
                if(in_array($value['line_number'],$exits_line_number)) {
                 
                    $data['error'][] = 'Row '.$rowno.' Line Number '.$value['line_number'].' already Exits..' ;

                }else{
                 $exits_line_number[]=$value['line_number'];  
                }
              }
           

        }
        if (!empty($code)) {
         $query = $this->db->query("select * from product p left join Approve_products ap on p.id= ap.product_id join orders o on o.id = ap.order_id where p.product_code = '".$code."' and p.status = '1'")->row_array();
            if(count($query) < 1 || empty($query['shortlisted_id'])) {
                $data['error'][$key] = 'Row '.$rowno.' Product '.$code.' not found in approve products' ;
            }elseif($query['corporate'] !== $_POST['Order']['corporate']){
              $data['error'][$key] = 'Row '.$rowno.' Product '.$code.' wrong corporate selected' ;
            }
      }else{
          return false;
      }
    }   
      if ($corporate_id==2) {
        if((int)$net_weight != (int)$gross_weight){
          $data['error'][]="wrong Gross Weight";
        }
     }else{
       if((int)$weight_band != (int)$gross_weight){
          $data['error'][]="wrong Gross Weight";
        }
     }
      /*print_r($gross_weight);
      print_r($weight_band);       
      die;*/
     return $data;
  }

  public function find($order_id=''){
    $result = array();
    $result['status'] = 'success';
  	if(!empty($order_id)){
      $_POST['serach_text'] = $order_id;
    }
    $this->db->where('order_id',@$_POST['serach_text']);
    $result['data'] = $this->db->get($this->table_name)->row_array();
    return $result;
  }
  public function store(){
    //print_r($_FILES);exit;
    $_POST['created_at'] = date('Y-m-d H:i:s');
    $time=date("H:i:s");
  	$_POST['Order']['date'] = date('Y-m-d '.$time,strtotime($_POST['Order']['date']));
    $excel_result = $this->excel_lib->import('corporate_products',$_FILES['file']);
    if($excel_result['status'] == 'success' && sizeof($excel_result['result']) != 0) {
      $vdata = $this->validateExistExceldata($excel_result['result'] ,true);
      if(!isset($vdata['error'])){
         $cdata = $this->ChkDuplicateExceldata($excel_result['result'] ,true);
          if(!isset($cdata['error'])){
            $date = date('Y-m-d H:i:s');
            $result = $this->find(@$_POST['Order']['order_id']);
            if(empty($result['data'])){
              if ($_POST['Order']['corporate'] ==2) {/*caratlane*/
                $inserted_orders =array();
                foreach ($excel_result['result'] as $ex_key => $ex_value) {
                  if (!in_array($ex_value['work_order_id'], $inserted_orders)) {
                  $insert_array = array();
                  $insert_array = $_POST['Order'];
                  $insert_array['order_id']=$ex_value['work_order_id'];
                  $insert_array['created_at'] = $date;
                  $insert_array['status'] = '1';
                  $this->db->insert($this->table_name,$insert_array);                  
                  $inserted_id = $this->db->insert_id();
                  date_update('email_orders',$inserted_id,'insert','created_at','insert');
                  $inserted_orders[]=$ex_value['work_order_id'];
                  }
                }
              }else{/*reliance and other corporates*/
                $insert_array = array();
                $insert_array = $_POST['Order'];
                $insert_array['created_at'] = $date;
                $this->db->insert($this->table_name,$insert_array);
                $inserted_id = $this->db->insert_id();
                date_update('email_orders',$inserted_id,'insert','created_at','insert');
              }
            }
            else{
              $insert_array = array();
              $insert_array = $_POST['Order'];
              $insert_array['updated_at'] = $date;
              $this->db->where('order_id',$_POST['Order']['order_id']);
              $this->db->update($this->table_name,$insert_array);
              $inserted_id = $this->db->get_where('email_orders',array('order_id'=>$_POST['Order']['order_id']))->row('id');
              date_update('email_orders',$inserted_id,'insert','updated_at','update');
            }
              
  	         $this->upload_products($excel_result,$_POST['Order']['order_id'],$_POST['Order']['corporate']);
           }
           else
           {
              $cerror['status'] = 'failure';
              $cerror['data'] = '';
              $cerror['error'] = $cdata['error'];
              echo json_encode($cerror);die;
           }
        }
       else
       {
          $verror['status'] = 'failure';
          $verror['data'] = '';
          $verror['error'] = $vdata['error'];
          echo json_encode($verror);die;
       }
    }elseif($excel_result['status'] == 'failure'){
      $excel_error = array();
     // print_r($excel_result['difference']);
      $error = $this->get_differnceMSG($excel_result['difference']);
      $excel_error['status'] = 'failure';
      $excel_error['error'][0] = "Excel Format is wrong. Please upload correct excel format";
      echo json_encode($excel_error);die;
    }elseif (sizeof($excel_result['result']) == 0) {
       $excel_error['status'] = 'failure';
       $excel_error['error'][0] = "Excel Format is wrong. Please upload correct excel format";
      echo json_encode($excel_error);die;
    }
    //echo $this->db->last_query();die;
  	return get_successMsg();
  }

  private function upload_products($result,$order_id,$corporate=""){
          if ($corporate != 2) {
             $this->db->where('order_id',$order_id);
             $this->db->update($this->table_name,array("status"=>'1'));
          }
           
             $prepare_order = array();
            foreach ($result['result'] as $key => $value) {
     
                 //print_r($email_order_id);
              if ($corporate=='2') {
                 $email_order_id=$this->email_order_get_id($value['work_order_id']);
                 $value['order_id'] = $value['work_order_id'];              
                 $value['sort_Item_number']=$value['external_item_number'];
                 $value['email_order_id'] = $email_order_id;
              }else{
                 $email_order_id=$this->email_order_get_id($order_id);
                 $value['order_id'] = $order_id;
                 $value['sort_Item_number']=itemNum_subStr($value['item_number']);
                 $value['temp_sku_number']=@$value['temp_sku_number'];
                 $value['set_sku_number']=@$value['set_sku_number'];
                 $value['set_parent_id']=@$value['set_parent_id'];
                 $value['wo_reference_type']=@$value['wo_reference_type'];
                 $value['special_remarks']=@$value['special_remarks'];
                 $value['reference_type']=@$value['reference_type'];
                 $value['customer_order']=@$value['customer_order'];
                 $value['email_order_id'] = $email_order_id;
                 
                 
              }

              if (isset($value['proposed_delivery_date'])) {
                // $pdate = date_create($value['proposed_delivery_date']);
                // $value['proposed_delivery_date'] = date_format($pdate,"Y-m-d");
                $value['proposed_delivery_date'] = date("Y-m-d",strtotime($value['proposed_delivery_date']));
              }
             
              $value['status'] = '0';           
              $value['created_at'] = date('Y-m-d H:i:s');
             // $value['barcode_path'] = $this->create_barcode($value['external_item_number']);
              /*item no details*/
              if (isset($value['item_number']) && $corporate == 1){
                $item_no_decoed =decode_item($value['item_number']);
                $item_no_decoed['karatage']=json_encode($item_no_decoed['karatage']);
              
                $insert_data = array_merge($value,$item_no_decoed);
              }else{
                $insert_data = $value;
              }
             /*end*/
              $prepare_order = $this->store_corporate_products($insert_data,$prepare_order);

            }//die;
           /* print_r($insert_data);
             die;*/
            $this->Prepare_order_model->store($prepare_order);
            $success['status'] = 'success';
            $success['data'] = '';
            $success['error'] = '';
           echo json_encode($success);die;
      
  }
private function email_order_get_id($order_id)
{
  $this->db->select('id');
  $this->db->where('order_id',$order_id);
  $this->db->where('status','1');
  $this->db->from($this->table_name);
  $result = $this->db->get()->row_array();
/*  echo $this->db->last_query();
  print_r($result);die;*/
  return $result['id'];
}

  private function create_barcode($product_code){
   if(!is_dir(FCPATH."uploads/barcode/$product_code")){
        mkdir(FCPATH."uploads/barcode/$product_code",0777);
    }
      $file = Zend_Barcode::draw('code128', 'image', array('text' => $product_code, 'drawText' => false), array());
     $store_image = imagepng($file,FCPATH."uploads/barcode/$product_code/".$product_code.".png");
     return "$product_code/".$product_code.".png";
  }
  private function store_corporate_products($data,$prepare_order){
    //print_r($data);exit;
    $details = $this->check_design_code($data['sort_Item_number']);
   // print_r($details);exit;
  	$this->db->insert('corporate_products',$data);
    $cp_id = $this->db->insert_id();
    if(!empty($cp_id) && !empty($details)){
      // $insert['created_at'] = date('Y-m-d H:i:s');
      // $insert['karigar_engaged_date'] = date('Y-m-d');
      // $insert['karigar_delivery_date'] = date("Y-m-d" , strtotime('+12 DAY'));
      // $insert['karigar_id'] =$details['karigar_id'];
      // $insert['corporate_products_id'] =$cp_id;
      // $insert['email_order_id'] =$data['email_order_id'];
      // $insert['status'] ='0';
      // $insert['quantity'] =$data['quantity'];
      //$this->db->insert('Prepare_product_list',$insert);
      $prepare_order['products'][]=$cp_id;
      $prepare_order['quantity'][$cp_id][]=$data['cw_quantity'];
      $prepare_order['weight'][$cp_id][]=@$data['quantity'];
      $prepare_order['order_name'][$cp_id][]=$data['order_id'];
      $prepare_order['sort_Item_number'][$cp_id][]=$data['sort_Item_number'];
      $prepare_order['karigar_id'][$cp_id][]=$details['karigar_id'];
      $prepare_order['id']="";
      $prepare_order['prev_karigar_id']="";
    }
    //print_r($prepare_order);die;
    return $prepare_order;
  }

  private function check_design_code($design_code){
    $this->db->select('karigar_id');
    $this->db->where('product_code',$design_code);
    $this->db->where('karigar_id !=',"");
    $result = $this->db->get('product')->row_array();
    return $result;
  }

  private function get_differnceMSG($diff){
    $error['status']= 'failure';
    $error['data']= '';
    foreach ($diff as $key => $value) {
      $error['error'][$key] =$value.' Column not found in excel' ;
    }
    return array_filter($error);
  }
  private function getErrorMsg(){
    $array= array(
      'order_id'=>strip_tags(form_error('Order[order_id]')),
      'date'=>strip_tags(form_error('Order[date]')),
      'gross_weight'=>strip_tags(form_error('Order[gross_weight]')),
      'net_weight'=>strip_tags(form_error('Order[net_weight]')),
      'corporate'=>strip_tags(form_error('Order[corporate]')),
      'less_weight'=>strip_tags(form_error('Order[less_weight]'))
      
    );
  	if (empty($_FILES['file']['name']))
	{
		$array['file'] = 'Please upload product excel';
	}
    return $array;
  }
  
  public function get(){
   $this->db->select("count(cp.id)-count(ppl.id) as pending_products,count(cp.id) total_products,eo.*,c.name corporate,c.id as corporate_id");
    ///$this->db->select("sum(IF(cp.status = '0', 1, 0)) as pending_products,count(cp.id) total_products,eo.*,c.name corporate");
    //$this->db->select("(case when cp.status='0' then count(cp.id) else null end) as pending_products,count(cp.id) total_products,eo.*,c.name as corporate");
    $this->db->from('email_orders eo');
    $this->db->join('corporate_products cp','eo.order_id=cp.order_id');
    $this->db->join('Prepare_product_list ppl','cp.id=ppl.corporate_products_id','left');
    $this->db->join('corporate c','c.id=eo.corporate');
    $this->db->where('eo.status','1');
    $this->db->group_by('eo.id');
    $this->db->having("count(cp.id)-count(ppl.id) >=",1);
    //$this->db->having("(select count(id) from Prepare_product_list where corporate_products_id IN(group_concat(cp.id))) !=",'0');
    //$this->db->having("sum(IF(cp.status = '0', 1, 0)) >=",1);
    $result = $this->db->get()->result_array();
/*    echo $this->db->last_query();
    echo"<pre>";
    print_r($result);exit();*/

    return $result;
    // echo $this->db->last_query();
    // print_r($result);exit();
    
  }
}