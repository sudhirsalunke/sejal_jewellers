<?php

class Print_Stickers_model extends CI_Model {

  function __construct() {
      $this->table_name = "quality_control";
      $this->table_name1 = "admin";
      parent::__construct();
  }

 
  public function get($id){
    $this->db->select('qc.created_at,cp.order_id,cp.sort_Item_number as product_code, c.name as category_name,qc.size as size_old,qc.quantity,qc.total_gr_wt as gr_wt,qc.total_net_wt as net_wt,qc.purity,ppl.barcode_path,cp.line_number,qc.cp_size as size');
    $this->db->from($this->table_name.' qc');
    $this->db->join('corporate_products cp','cp.id = qc.corporate_product_id','left');
     $this->db->join('Prepare_product_list ppl','cp.id = ppl.corporate_products_id','left');
    $this->db->join('Receive_products rp','rp.id = qc.receive_product_id','left');    
    $this->db->join('product p', 'p.product_code = cp.sort_Item_number','left');
    $this->db->join('category_master c', 'c.id = p.category_id', 'left');
    $this->db->where('qc.id',$id);
    $result = $this->db->get()->result_array();
 // echo $this->db->last_query(); exit();
    return $result;
  }
  public function get_username($id)
  {
    $this->db->select('username');
    $this->db->from($this->table_name1);
    $this->db->where('id',$id);
    $result = $this->db->get()->row('username');
    return $result;
  }
  public function store($data){
  $this->db->set('purity',$data['purity']) ;
    $this->db->where('id',$data['qc_id']);  
    if($this->db->update($this->table_name)){
        return get_successMsg();
      }else{
        return get_errorMsg();
      }
    }
  
}    
