<?php

class Generate_chitti_model extends CI_Model {

  function __construct() {
  	$this->table_name = "gt_excel";
  	$this->load->library('excel');
    parent::__construct();
  }

    public function export_chitti($challan_no){
    $this->db->select('ge.*,date_format(ge.created_at,"%D-%b-%Y") as created_at,sum(ge.quantity) as quantity,sum(ge.gross_wt_gm) gross_gm,sum(ge.net_wt_gm) as net_gm,sum(ge.mk_value) total_amount,sum(ge.pure_wt_gm) pure_gm,sum(ge.wqty) total_wqty,cm.name as category_name,IFnull(cm.purity,0.00) purity,IFnull(cm.wastage,0.00) as wastage,cm.labore,c.name as corporate_name');
    $this->db->where('ge.challan_no',$challan_no);
    $this->db->where('ge.status !=','0');
    $this->db->from($this->table_name.' ge');
    $this->db->join('corporate_products cp','ge.corporate_product_id=cp.id','left');
    $this->db->join('product p','cp.sort_Item_number=p.product_code','left');
    $this->db->join('category_master cm','p.category_id=cm.id','left');
    $this->db->join('corporate c','ge.corporate=c.id','left');
   	$result = $this->db->get()->row_array();
   	$this->genrate_excel($result);
  }

  function genrate_excel($result){
		 // Create new Spreadsheet object
		$spreadsheet = new PHPExcel();
		// Set document properties
		$spreadsheet->getProperties()->setCreator('Asd')
		        ->setLastModifiedBy('ascra')
		        ->setTitle('chitti')
		        ->setSubject('Chitti ExcelSheet ')
		        ->setDescription('This ExcelSheet Prepared By Ascra Ltd.')
		        ->setKeywords('chitti corporate model')
		        ->setCategory('Confidential');
		// Add some data

		$spreadsheet->setActiveSheetIndex(0)
				->mergeCells('A1:I1')
				->mergeCells('B2:E2')
				->mergeCells('B3:E3')
				->mergeCells('B4:E4')
				->setCellValue('A1', 'LABORE ISSUE')
		        ->setCellValue('A2', 'Name')
		        ->setCellValue('B2',$result['corporate_name']." Approval")

		        ->setCellValue('A3', 'Date')
		        ->setCellValue('B3', $result['created_at'])

		        ->setCellValue('F2', 'Chitti No.')
		        ->setCellValue('G2', $result['chitti_no']);

		 $gst_5=($result['total_amount']*5)/100;
		 $final_amount = $result['total_amount']+$gst_5;
  		// Miscellaneous glyphs, UTF-8
		$spreadsheet->setActiveSheetIndex(0)  
		            ->setCellValue('A5', 'Product')
		            ->setCellValue('B5', 'Gross Wt')
		            ->setCellValue('C5', 'St Wt')
		            ->setCellValue('D5', 'Net Wt')
		            ->setCellValue('E5', 'Purity')
		            ->setCellValue('F5', 'Wastage')
		            ->setCellValue('G5', 'Pure')
		            ->setCellValue('H5', 'Labore/Gm')
		            ->setCellValue('I5', 'MK Value')
		            ->setCellValue('A6', $result['category_name'])
		            ->setCellValue('B6', $result['gross_gm'])
		            ->setCellValue('C6', "0")
		            ->setCellValue('D6', $result['net_gm'])
		            ->setCellValue('E6', $result['purity']." %")
		            ->setCellValue('F6', $result['wastage']." %")
		            ->setCellValue('G6', $result['pure_gm']+$result['wqty'])
		            ->setCellValue('H6', $result['labore'])
		            ->setCellValue('I6', $result['total_amount'])
		            ->setCellValue('A8','Stone amount')
		            ->setCellValue('A8','0.00')
		            ->setCellValue('A10','GST 3% On Stone Charge')
		            ->setCellValue('I10','0.0')
		            ->setCellValue('A11','GST 5% On Making Charges')
		            ->setCellValue('I11',$gst_5)
		            ->setCellValue('A14','Total')
		            ->setCellValue('B14',$result['gross_gm'])
		            ->setCellValue('C14','00')
		            ->setCellValue('D14',$result['net_gm'])
		            ->setCellValue('G14',$result['pure_gm']+$result['wqty'])
		            ->setCellValue('A15','Remarks')
		            ->setCellValue('A16',$result['challan_no'])
		            ->setCellValue('F16','Total')
		            ->setCellValue('I16',$final_amount);

		 $spreadsheet->getActiveSheet()->getStyle('A1:I17')->applyFromArray(array('font' => array('name'=>'Bookman Old Style','size' => 10,'color' => array('rgb' => 'FF000000')),'borders' => array(
			    	'allborders' => array(
			    		'style' => PHPExcel_Style_Border::BORDER_THIN
    				),
    				'outline' => array(
			    		'style' => PHPExcel_Style_Border::BORDER_THICK
    				)
				),
				'alignment' => array(
		            'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER,
		        	)
		 		));

		 $spreadsheet->getActiveSheet()->getStyle('A1:G1')->applyFromArray(array(
			'font' => array('name'=>'Bookman Old Style','size' => 10,'bold' => true),
			'alignment' => array(
		            'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER,
		        	),
			'fill' =>array('type' => PHPExcel_Style_Fill::FILL_SOLID,'color' => 
					array('rgb' => 'e5a9c3')),
			'borders' => array(
			    	'outline' => array(
			    		'style' => PHPExcel_Style_Border::BORDER_THICK))

			));

		$spreadsheet->getActiveSheet()->getStyle('A2:I3')->applyFromArray(array(
			'font'  => array(
					'bold'=>true,
			        ),
			));

		 $spreadsheet->getActiveSheet()->getStyle('B14:I14')->applyFromArray(array(
			'font'  => array(
					'bold'=>true,
			        ),
			));

		$spreadsheet->getActiveSheet()->getStyle('B2')->applyFromArray(array(
			'font'  => array(
					'bold'=>true,
			        'color' => array('rgb' => 'ed1e1e'),
			        ),
			'fill' =>array('type' => PHPExcel_Style_Fill::FILL_SOLID,'color' => 
					array('rgb' => 'ffff4d')),
			));

		$spreadsheet->getActiveSheet()->getStyle('A14')->applyFromArray(array(
			'font'  => array(
					'bold'=>true,
			        'color' => array('rgb' => 'ed1e1e'),
			        ),
			'fill' =>array('type' => PHPExcel_Style_Fill::FILL_SOLID,'color' => 
					array('rgb' => 'ffff4d')),
			));

		$spreadsheet->getActiveSheet()->getStyle('A15')->applyFromArray(array(
			'font'  => array(
					'bold'=>true,
			        'color' => array('rgb' => 'ed1e1e'),
			        ),
			'fill' =>array('type' => PHPExcel_Style_Fill::FILL_SOLID,'color' => 
					array('rgb' => 'ffff4d')),
			));

		$spreadsheet->getActiveSheet()->getStyle('A16')->applyFromArray(array(
			'font'  => array(
					'bold'=>true,
			        'color' => array('rgb' => 'ed1e1e'),
			        ),
			'fill' =>array('type' => PHPExcel_Style_Fill::FILL_SOLID,'color' => 
					array('rgb' => 'ffff4d')),
			));
		$spreadsheet->getActiveSheet()->getStyle('F16')->applyFromArray(array(
			'font'  => array(
					'bold'=>true,),
			
			));
		$spreadsheet->getActiveSheet()->getStyle('I16')->applyFromArray(array(
			'font'  => array(
					'bold'=>true),
			));

		$spreadsheet->getActiveSheet()->getStyle('A5:I5')->applyFromArray(array('font' => array('name'=>'Bookman Old Style','size' => 10,'bold' => true,'color' => array('rgb' => 'FF000000')),'borders' => array(
			    	'bottom' => array(
			    		'style' => PHPExcel_Style_Border::BORDER_THICK
    				)
  				)));

		$spreadsheet->getActiveSheet()->getStyle('A14:I14')->applyFromArray(array('borders' => array(
			    	'bottom' => array(
			    		'style' => PHPExcel_Style_Border::BORDER_THICK
    				)
  				)));
		

		foreach(range('A','R') as $columnID) {
		    $spreadsheet->getActiveSheet()->getColumnDimension($columnID)
		        ->setAutoSize(true);
		}
	
		$spreadsheet->getActiveSheet()
		    ->getStyle('A1:R200')
		    ->getAlignment()
		    ->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_LEFT);

		// Rename worksheet
		$spreadsheet->getActiveSheet()->setTitle('Iwo_chitti');
		// Set active sheet index to the first sheet, so Excel opens this as the first sheet
		$spreadsheet->setActiveSheetIndex(0);
		// Redirect output to a client’s web browser (Xlsx)
		header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
		header('Content-Disposition: attachment;filename="IWOchitti.xlsx"');
		header('Cache-Control: max-age=0');
		// If you're serving to IE 9, then the following may be needed
		header('Cache-Control: max-age=1');
		// If you're serving to IE over SSL, then the following may be needed
		header('Expires: Mon, 26 Jul 1997 05:00:00 GMT'); // Date in the past
		header('Last-Modified: ' . gmdate('D, d M Y H:i:s') . ' GMT'); // always modified
		header('Cache-Control: cache, must-revalidate'); // HTTP/1.1
		header('Pragma: public'); // HTTP/1.0

		$writer = PHPExcel_IOFactory::createWriter($spreadsheet, 'Excel2007');
		ob_end_clean();
		$writer->save('php://output');	
  }

  public function get_data($challan_no,$filter_status='',$status='',$params='',$search='',$limit=''){
   	$this->db->select('ge.*,ge.article_code,rp.category_name,net_wt_gm,gross_wt_gm,ge.created_at');
  	$this->db->where('challan_no',$challan_no);
  	$this->db->from($this->table_name.' ge');
  	$this->db->join('Receive_products rp','rp.corporate_product_id=ge.corporate_product_id','left');
    $this->db->join('corporate_products cp','cp.id=ge.corporate_product_id','left');
    if(!empty($search)){
        $this->db->where('cp.sort_item_number',$search);
    }
    if($limit == true)
    	$this->db->limit($params['length'],$params['start']);

    $this->db->group_by('ge.id');
    $this->db->order_by('ge.id','DESC');

    $result = $this->db->get()->result_array();
  /*  echo $this->db->last_query();
    print_r($result);die;*/
    return $result;
  }

public function accept_reject(){
	
	$this->db->set('status', $_POST['status']);
	$this->db->where('id',$_POST['id']);
	if ($this->db->update('gt_excel')) {		
		$this->db->select('email_order_id,item,status');
		$this->db->where('id',$_POST['id']);
		$this->db->from($this->table_name);
		$result = $this->db->get()->row_array();
		//print_r($result);die;
		if($result['status']=='1'){
			corporate_product_update($result['email_order_id'],'11',$result['item']);//status 11-PRODUCT APPROVED Reliance
		}else{
			corporate_product_update($result['email_order_id'],'12',$result['item']);//status 12-PRODUCT REJECTED Reliance
		}
		return get_successMsg();
	}
}


}
