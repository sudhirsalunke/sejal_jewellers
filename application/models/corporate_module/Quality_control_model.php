<?php
class Quality_control_model extends CI_Model {

  function __construct() {
    $this->table_name = "quality_control";
    $this->table_name1 = "generate_packing_list";
    parent::__construct();
  }
  public function validatepostdata(){
    $this->form_validation->set_rules($this->config->item('Quality_control', 'admin_validationrules'));

    $data['status']= 'success';
    if($this->form_validation->run()===FALSE){
      $data['status']= 'failure';
      $data['data']= '';
      $data['error'] = $this->getErrorMsg();
    } 

    return $data;
  }
  private function getErrorMsg(){
    return array(
      'quantity'=>strip_tags(form_error('quantity')),
      'total_gr_wt'=>strip_tags(form_error('total_gr_wt')),
      'total_net_wt'=>strip_tags(form_error('total_net_wt')),
      'cp_size'=>strip_tags(form_error('cp_size')),
      'stone_wt'=>strip_tags(form_error('stone_wt')),
      'less_wt'=>strip_tags(form_error('less_wt'))
    );
  }
  public function get($filter_status='',$status='',$params='',$search='',$limit='',$hallmarking =''){
    if($limit == true){
    $this->db->select('qc.created_at,qc.quantity,qc.receive_product_id,qc.cp_size size,qc.total_gr_wt,qc.total_net_wt,qc.total_gr_wt,rp.category_name,cp.sort_item_number as product_code,qc.id as qc_id,qc.status,qc.status as qc_status,qc.corporate_product_id,eo.order_id,eo.corporate,corp.name as corporate_name,sum(qch.quantity) qch_quantity,sbm.name as sub_category,hc.name as hc_name,cp.work_order_id,cp.line_number,cm.id,qc.id');
  }else{
     $this->db->select('count(distinct (qc.id)) as cont_record');
  }
    $this->db->from($this->table_name.' qc');
    $this->join_cond($hallmarking);
    $this->where_cond($hallmarking);
    // $this->db->where('eo.corporate','1');
    //print_r($params);die;
    if (isset($params['columns']) && !empty($params['columns'])){
      $filter_input=$params['columns'];
    $this->get_filter_value($filter_input);
    }

    if(!empty($filter_status)){
      $this->db->order_by($status[$filter_status['column']],$filter_status['dir']);
    }
 /* */
    //$this->all_like_queries($search);
   /* if (isset($_REQUEST['columns'][1]['search']['value']) && !empty($_REQUEST['columns'][1]['search']['value']) && $_GET['status'] == 'rejected') {
      $this->db->where('eo.corporate',$_REQUEST['columns'][1]['search']['value']);
    }*/
    $this->db->group_by('qc.id');
    if($limit == true){
    $this->db->limit($params['length'],$params['start']);
    $this->db->order_by('qc.id','DESC');
    $result = $this->db->get()->result_array();
    }else{
        $row_array = $this->db->get()->result_array();
        $result = count($row_array);
    }
   
 /*   if(!empty($filter_status) && $_GET['status'] != 'rejected'){
      $this->db->order_by('eo.corporate',$filter_status['dir']);
    }*/
 //;
    
   //echo $this->db->last_query();die;
    return $result;
  }

  private function get_filter_value($filter_input){
  $column_name=array();  
  $filter_column_name=filter_column_name('Quality_control');
  $sql='';
  $i=0;   
   //print_r($filter_input);die;
  foreach ($filter_input as $key => $search_value){
     $column_name=$filter_column_name;
     //print_r($search_value['search']['value']);
      if(!empty($search_value['search']['value'])){
        if($i != 0){
          $sql.=' AND  ';
        }
          $sql.=''.$column_name[$key].' like "%'.$search_value['search']['value'].'%" ';
          $i++;
       }   
  }
  if(!empty($sql)){  
    $this->db->where($sql);  
  }   
       
  }
  private function all_like_queries($search){
    $this->db->where("(`rp`.`Product_code` LIKE '%$search%' OR cp.order_id LIKE '%$search%' OR corp.name LIKE '%$search%' OR DATE_FORMAT(qc.created_at,'%d-%m-%Y') LIKE '%$search%' OR rp.category_name LIKE '%$search%' OR sbm.name LIKE '%$search%')");
  }
  public function store($insert_array,$from_rp=false){
    $corporate_id=$this->get_corporate_by_cp_id($insert_array['corporate_product_id']);
   //print_r($insert_array);die;
    $weight_band=$this->get_weight_band($insert_array['corporate_product_id'],$corporate_id);
    $recieved_qc_qnty = $this->get_quantity_by_rp_id($insert_array['receive_product_id']);
    $recieved_rp_qnty = $this->Receive_products_model->find($insert_array['receive_product_id']);
    $email_order_id = email_order_id($insert_array['corporate_product_id']);
    //$insert_array['email_order_id']=$email_order_id; 
    $status = $this->get_error_status($recieved_qc_qnty,$recieved_rp_qnty,$insert_array,$weight_band);
    //print_r($status);die;
    if($status == true){
      unset($insert_array['from_wt_torlen']);
      unset($insert_array['to_wt_torlen']);
      $insert_array['user_id'] = $this->session->userdata('user_id');
      if(isset($insert_array['rejected']) && $insert_array['rejected'] == true){
        $insert_array['status'] = '0';
        $product_status = 'reject';
          corporate_product_update($insert_array['email_order_id'],'4',$insert_array['product_code']);//status 4-QC REJECTED
        unset($insert_array['rejected']);
      }elseif(isset($insert_array['amend']) && $insert_array['amend'] == true){
        $insert_array['status'] = '6';
        $product_status = 'amend';
        corporate_product_update($insert_array['email_order_id'],'5',$insert_array['product_code']);//status 5-QC AMENDED
        unset($insert_array['amend']);
      }else{
        $insert_array['status'] = '1';
        $product_status = 'accept';
        if($insert_array['corporate']=='1'){
        corporate_product_update($insert_array['email_order_id'],'6',$insert_array['product_code']);//status 6-PENDING FOR HALLMARKING
        }else{
           corporate_product_update($insert_array['email_order_id'],'13',$insert_array['product_code']);//status 13-GENERATE PACKING LIST Caratlane
        }
      }
      $insert_array['created_at'] = date('Y-m-d H:i:s');
      $insert_array['module']='1';/*added module In Qc*/
      unset($insert_array['btn']);
         //unset($insert_array['sort_Item_number']);
         //unset($insert_array['email_order_id']);
      if($this->db->insert($this->table_name,$insert_array)){
        $inserted_id = $this->db->insert_id();
        //echo $this->db->last_query();
        date_update('quality_control',$inserted_id,$product_status,'created_at','insert');

        if($this->get_quantity_by_rp_id($insert_array['receive_product_id']) == $recieved_rp_qnty['quantity']){
           $this->Receive_products_model->update(array('receive_product_id'=>$insert_array['receive_product_id'],'status'=>'2'));
        }

        if($recieved_rp_qnty['is_skip_hm'] == '1' && $from_rp== false){
          $hm['corporate_product_id'] = $insert_array['corporate_product_id'];
          $hm['email_order_id'] = $insert_array['email_order_id'];
          $hm['receive_product_id'] = $recieved_rp_qnty['id'];
          $hm['qc_id'] = $inserted_id;
          $hm['total_gr_wt'] = $insert_array['total_gr_wt'];
          $hm['total_net_wt'] = $insert_array['total_net_wt'];
          $hm['size'] = $insert_array['size'];
          $hm['quantity'] = $insert_array['quantity'];
          $hm['barcode_path'] = $insert_array['barcode_path'];
          $hm['created_at'] = date("Y-m-d H:i:s");         
          $this->Hallmarking_model->store($hm);
          $this->Hallmarking_model->set_exported_qch($inserted_id);
        }
        $result_data['status']='success';
        $result_data['receive_product_id']  = $insert_array['receive_product_id'];
        $result_data['qc_id'] = $inserted_id;
        $result_data['corporate'] = $corporate_id;
        return $result_data;
      }else{
        return get_errorMsg();
      }
    }    
  }

  private function get_error_status($recieved_qc_qnty,$recieved_rp_qnty,$insert_array,$weight_band=""){
     
    if($recieved_rp_qnty['quantity'] < ($recieved_qc_qnty + $insert_array['quantity'])){
      $data['status']= 'failure';
      $data['data']= '';
      $data['error'] = array('quantity'=>'Quantity Exceeded');
      echo json_encode($data);die;
    }elseif ($insert_array['quantity'] == 0 ) {
      $data['status']= 'failure';
      $data['data']= '';
      $data['error'] = array('quantity'=>'Quantity should be greater than zero');
      echo json_encode($data);die;
    }
    elseif ((is_numeric($insert_array['quantity']) && strpos($insert_array['quantity'], ".") !== false)) {
      $data['status']= 'failure';
      $data['data']= '';
      $data['error'] = array('quantity'=>'Quantity should be integer only');
      echo json_encode($data);die;
    //}elseif (!empty($weight_band)) {
      }elseif (!empty($insert_array['total_gr_wt']) && !empty($insert_array['btn']['accept_btn']) ){
      // print_r($_POST);
      // if ($insert_array['total_gr_wt'] < $weight_band[0]  || $insert_array['total_gr_wt'] > $weight_band[1]) {
      //   $data['status']= 'failure';
      //   $data['data']= '';
      //   $data['error'] = array('total_gr_wt'=>'Gross Weight should be in '.$weight_band[0].' - '.$weight_band[1]);
      //   echo json_encode($data);die;
      // }
      if ($insert_array['total_net_wt'] < $insert_array['from_wt_torlen']  || $insert_array['total_net_wt'] > $insert_array['to_wt_torlen']) {
        $data['status']= 'failure';
        $data['data']= '';
        $data['error'] = array('total_net_wt'=>'Net Weight should be in '.$insert_array['from_wt_torlen'].' - '.$insert_array['to_wt_torlen']);
        echo json_encode($data);die;
      }

 
   
    }
    return true;
  }

  private function where_cond($hallmarking){
    //if($hallmarking == true)
    if($_GET['status'] != 'rejected')
      $this->db->where('eo.corporate','1');
    if(!empty($_GET['status']) && $_GET['status'] == 'all' && $hallmarking == false){
      $this->db->where('rp.status','2');
    }elseif(!empty($_GET['status']) && $_GET['status'] == 'pending' && $hallmarking == false){
      $this->db->where('rp.status','1');
    }elseif(!empty($_GET['status']) && $_GET['status'] == 'hallmarking_pending' && $hallmarking == false){
      $this->db->where('rp.status','3');
    }elseif(!empty($_GET['status']) && $_GET['status'] == 'complete' && $hallmarking == false){
      $this->db->where('qc.status','1');
    }elseif(!empty($_GET['status']) && $_GET['status'] == 'rejected' && $hallmarking == false){
      $this->db->where('qc.hallmarking is null');
      $this->db->where('qc.status','0');
      $this->db->where('qc.module','1');
      $this->db->where('qc.status !=','5');
    }elseif(!empty($_GET['status']) && $_GET['status'] == 'amended' && $hallmarking == false){
      $this->db->where('rp.status','5');
    }elseif(!empty($_GET['status']) && $_GET['status'] == 'send' && $hallmarking == true){
      $this->db->where('qc.status','2');
    }elseif(!empty($_GET['status']) && $_GET['status'] == 'pending' && $hallmarking == true){
      $this->db->where('qc.status','1');
    }elseif(!empty($_GET['status']) && $_GET['status'] == 'receive' && $hallmarking == true){
      $this->db->where('qch.status','3');
    }elseif(!empty($_GET['status']) && $_GET['status'] == 'rejected' && $hallmarking == true){
      $this->db->where('rp.status','0');
      $this->db->where('qc.hallmarking is not null');
    }
  }
  private function join_cond($hallmarking){

    if(!empty($_GET['status']) && $_GET['status'] == 'pending' && $hallmarking == false){
      $this->db->join('Receive_products rp','rp.corporate_product_id = qc.corporate_product_id','right');
      return ;
    }
    $this->db->join('Receive_products rp','rp.id = qc.receive_product_id');
    $this->db->join('corporate_products cp','cp.id = qc.corporate_product_id');
    $this->db->join('email_orders eo','eo.order_id = cp.order_id');
    $this->db->join('corporate corp','corp.id = eo.corporate');
    $this->db->join('category_master cm','cm.name = rp.category_name','left');
    $this->db->join('quality_control_hallmarking qch','qc.id = qch.qc_id','left');
    $this->db->join('product p','cp.sort_Item_number=p.product_code','left');
    $this->db->join('sub_category_master sbm','p.sub_category_id = sbm.id','left');
    $this->db->join('hallmarking_center hc','qc.hc_id = hc.id','left');
    
  }
  public function send_to_hallmarking($qc_id){

    $date = date('Y-m-d');
    $this->db->set('hc_id',$_POST['hc_id']);
    $this->db->set('HM_send_date',$date);
    $this->db->set('status','2');
    $this->db->where('id',$qc_id);
    $this->db->where('status!=','0');
    if($this->db->update($this->table_name)){
      // echo $this->db->last_query();die;
      date_update('quality_control',$qc_id,'insert','updated_at','update');
      $data=$this->Quality_control_model->get_karigar_id_from_qc_id($qc_id);
      corporate_product_update($data['email_order_id'],'7',$data['sort_Item_number']);//status 7-SENT FOR HALLMARKING
      // $this->db->select('receive_product_id');
      // $this->db->from($this->table_name);
      // $this->db->where('id',$qc_id);
      // $result = $this->db->get()->row_array();
      // $this->Receive_products_model->update(array('receive_product_id'=>$result['receive_product_id'],'status'=>'3'));
      return get_successMsg();
    }else{
      return get_errorMsg();
    }
  }
  public function find($rp_id){
    $this->db->where('receive_product_id',$rp_id);
    $this->db->where('status!=','0');
    return $this->db->get($this->table_name)->row_array();
  }
  public function find_by_qc_id($qc_id){
    $this->db->where('id',$qc_id);
    return $this->db->get($this->table_name)->row_array();
  }
  

  public function send_to_prepare_order($corporate_product_id){
    // $this->db->set('status','0');
    // $this->db->where('id',$corporate_product_id);
    // if($this->db->update('corporate_products')){

    // $this->db->where('corporate_product_id',$corporate_product_id);
    // $this->db->delete($this->table_name);
    // $this->db->where('corporate_product_id',$corporate_product_id);
    // $this->db->delete('Receive_products');
    //   return get_successMsg();
    // }else{
    //   return get_errorMsg();
    // }
}
  
   public function export_qc_products($filter_status='',$status='',$params='',$search='',$limit='',$hallmarking ='' ){
    
    $this->db->select('cp.work_order_id as WO_Id,"" WO_Srl,cp.Item_number as Item_Id,qc.quantity as PCS,cp.sort_Item_number as product_code,am.description as Article_Desc,qc.total_gr_wt as g_wt,cp.line_number,qc.total_net_wt as net_wt,qc.less_wt');
    $this->db->from($this->table_name.' qc');
    $this->db->join('Receive_products rp','rp.id = qc.receive_product_id','left');
    $this->db->join('corporate_products cp','cp.id = qc.corporate_product_id','left');
    $this->db->join('product p','p.product_code = cp.sort_Item_number','left');
    $this->db->join('carat_master cm','cm.id = p.carat_id','left');
    $this->db->join('article_master am','am.id = p.article_id','left');
    $this->db->join('email_orders eo','eo.order_id = cp.order_id');
    $this->db->join('corporate corp','corp.id = eo.corporate','left');
/*    if(!empty($product_code)){
      $this->db->where_in('rp.product_code',$product_code);
    }*/

    if(empty($_POST['product_code'])){
     // $this->all_like_queries($search);
      $this->db->where('qc.status','1');
      $this->db->where('eo.corporate','1');
    }else{
        $this->db->where('qc.status','1');
        $this->db->where('eo.corporate','1');
        $this->db->where_in('qc.id',$_POST['product_code']);

      //$this->export_where_conditions();
    }
     $this->db->order_by('qc.id','DESC');
    $result = $this->db->get()->result_array();   
    return $result;
  }
  private function export_where_conditions(){
    $sql = '';
    foreach ($_POST['product_code'] as $key => $value) {
      if(is_array($value))
        $value = $value['value'];
      if($key == 0)
        $sql .= 'cp.id = '.$value;
      else
        $sql .= ' OR  cp.id = '.$value;
    }
    $this->db->where($sql);
  }
  public function generate_packing_list()
  { 
    
    foreach ($_POST['corporate_product_id'] as $key => $value) {
      $pdata = $this->find($value['value']);
      $data['corporate_product_id'] = $pdata['corporate_product_id'];
      $this->db->insert($this->table_name1,$data);
      $this->db->set('status','');
      $this->db->where('corporate_product_id',$value['value']);
      $this->db->update($this->table_name);
    }
    return get_successMsg();  
  }
  public function get_remark($id){
    $this->db->select('qc.*,cp.sort_Item_number  product_code');
    $this->db->from($this->table_name.' qc');
    $this->db->where('qc.id',$id);
    $this->db->join('corporate_products cp','cp.id = qc.corporate_product_id');
    $result = $this->db->get()->row_array();
    return $result;
  }
  public function amend_product($qc_id,$receive_product_id)
  {
      $this->db->set('status','6');
      $this->db->where('id',$qc_id);
      $this->db->update($this->table_name);
      $this->db->set('status','5');
      $this->db->where('id',$receive_product_id);
      $this->db->update('Receive_products');
      
  }
  public function get_quantity($qc_id){
    $this->db->select('quantity');
    $this->db->from($this->table_name);
    $this->db->where('id',$qc_id);
    return $this->db->get()->row_array();
  }
  private function get_quantity_by_rp_id($rp_id){
    $this->db->select('sum(quantity) quantity');
    $this->db->from($this->table_name);
    $this->db->where('receive_product_id',$rp_id);
    $result = $this->db->get()->row_array();
    return $result['quantity'];
  }
  public function get_karigar_id($cp_id){
    $this->db->select('karigar_id');
    $this->db->from('Prepare_product_list');
    $this->db->where('corporate_products_id',$cp_id);
    $result = $this->db->get()->row_array();
    // echo $this->db->last_query(); print_r($result); die;
    return $result['karigar_id'];
  }
  public function insert_in_prepared_order($result,$karigar_id){
     /*  if(!empty($karigar_id)){
        $this->db->where('karigar_id',$karigar_id);
        $this->db->delete('Prepare_product_list');
        }*/
        //print_r($karigar_id);
    $email_order_id = email_order_id($result['corporate_product_id']);
    $insert_array = array(
        'corporate_products_id'=>$result['corporate_product_id'],
        'email_order_id'=>$email_order_id,
        'quantity'=>$result['quantity'],
        'karigar_id'=>$karigar_id,
        'qc_rejected_id'=>$result['id'],
        'status'=>'0',
        'created_at'=>date('Y-m-d H:i:sa'),
        'updated_at'=>date('Y-m-d H:i:sa'),
        'karigar_engaged_date' => date('Y-m-d'),
        'karigar_delivery_date' => date("Y-m-d" , strtotime('+12 DAY')),
      );
    //print_r($insert_array);die;
    if($this->db->insert('Prepare_product_list',$insert_array)){
      return true;
    }else{
      return false;
    }
  }
  public function update($array,$where){
    $this->db->where($where);
    $this->db->update($this->table_name,$array);
  }

  public function get_weight_band($cp_id,$corporate=""){
  $response=array();
   $this->db->select('weight_band,item_number,net_weight');
   $this->db->where('id',$cp_id);
   $result = $this->db->get('corporate_products')->row_array();
   if ($corporate==2) {
    $variation = $result['net_weight'] *10/100;
    $response[0]=$result['net_weight']-$variation;
    $response[1]=$result['net_weight']+$variation;
   }else{
      if (empty($result['weight_band'])) {
          $decode_item = decode_item($result['item_number']);
          $weight_band = $decode_item['weight_band'];
      }else{
          $weight_band = $result['weight_band'];
      }
      $response = explode('-',$weight_band);
   }

   return $response; 
  }

  public function get_corporate_by_cp_id($cp_id){
    $this->db->select('eo.corporate');
    $this->db->where('cp.id',$cp_id);
    $this->db->from('corporate_products cp');
    $this->db->join('email_orders eo','cp.order_id=eo.order_id');
    $result = $this->db->get()->row_array();
    return $result['corporate'];
  }

  public function insert_in_stock($result,$data){
   $email_order_id = email_order_id($result['corporate_product_id']);
   $insert_array = array(
        'corporate_product_id'=>$result['corporate_product_id'],
        'email_order_id'=>$email_order_id,
        'qc_id'=>$result['id'],
        'quantity'=>$result['quantity'],
        'karigar_id'=>$data['karigar_id'],
        'corporate'=>$data['corporate'],
        'receive_product_id'=>$data['receive_product_id'],
        'status'=>'1',
        'created_at'=>date('Y-m-d H:i:sa'),
        'updated_at'=>date('Y-m-d H:i:sa'),
      );
    if($this->db->insert('stock',$insert_array)){
      return true;
    }else{
      return false;
    }
  }
   function get_karigar_id_from_qc_id($qc_id){
    $this->db->select('rp.karigar_id,qc.receive_product_id,eo.corporate,cp.sort_Item_number,cp.email_order_id');
    $this->db->where('qc.id',$qc_id);
    $this->db->from('quality_control qc');
    $this->db->join('Receive_products rp','qc.receive_product_id=rp.id','left');
    $this->db->join('corporate_products cp','rp.corporate_product_id=cp.id','left');
    $this->db->join('email_orders eo','cp.order_id=eo.order_id');
    $result = $this->db->get()->row_array();
    return $result;
  }

}