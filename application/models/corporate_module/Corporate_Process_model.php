<?php

class Corporate_Process_model extends CI_Model {

  function __construct() {
      parent::__construct();
  }
  public function get_karigar_product_list($status=''){
  	$this->db->select('count(ppl.id) as count');
    $this->db->from('Prepare_product_list ppl');
    $this->db->join('karigar_master k','k.id = ppl.karigar_id');
    if($status == '')
      $this->db->where('ppl.status','0');
    else
      $this->db->where('ppl.status','1');
  	$result = $this->db->get()->row_array();
  	return $result['count'];
  }
    public function product_not_assign_to_karigar(){
    $this->db->select('count(id) as count');
    $this->db->from('corporate_products');
    $this->db->where('status','0');

    //$this->db->or_where('status','1');
    $result1 = $this->db->get()->row_array();
 //  print_r($result1);
   $this->db->select('count(*) as total_cnt');
   $this->db->from('corporate_products cp');
    //$this->db->join('Prepare_product_list ppl','ppl.email_order_id = cp.email_order_id AND cp.id !=ppl.corporate_products_id','left');
    $this->db->where('cp.status','1');
    $this->db->where('cp.id NOT IN (select corporate_products_id from Prepare_product_list)');
    $result2 = $this->db->get()->row_array();
   // print_r($result2);die();
    // $result2['count'] = count($result2);
   //  print_r(count($result2));exit;
    //echo $this->db->last_query();print_r($result);die;
    $result = $result1['count'] + $result2['total_cnt'];
    return $result;
  }
  public function get_engaged_karigar($due_date=""){
    $this->db->select('count(DISTINCT(ppl.KARIGAR_id)) as count');
    $this->db->from('Prepare_product_list ppl');
    //$this->db->join('karigar_master k','k.id = ppl.karigar_id');
    //$this->db->where('ppl.status','1');
    $this->db->where('(ppl.status = "1" OR ppl.status = "2")');
    if (!empty($due_date)) {
      $this->db->where('((ppl.karigar_delivery_date <= DATE_ADD(NOW(), INTERVAL 3 DAY)) AND (ppl.karigar_delivery_date>= NOW()))');
    }

    $result = $this->db->get()->row_array();
    //echo $this->db->last_query();print_r($result);die;
    return $result['count'];
  }
  public function get_received_products()
  {
    $this->db->select('count(rp.id) as count');
    $this->db->from('Receive_products rp');
    //$this->db->where('rp.status','0');
    $this->db->or_where('rp.status','1');
   // $this->db->or_where('rp.status','2');
   // $this->db->or_where('rp.status','3');
   // $this->db->or_where('rp.status','4');
    $result = $this->db->get()->row_array();
    return $result['count'];
  }
  public function pending_qc_products(){
    $this->db->select('count(rp.id) as count');
    $this->db->from('Receive_products rp');
    $this->db->where('rp.status','1');
    $result = $this->db->get()->row_array();
    return $result['count'];
  }
  public function get_caratlane_gpl($status=''){
    $this->db->where('status',$status);
    $result = $this->db->get('generate_packing_list')->result_array();
    return count($result);
  }

   public function get_amend_pro_ctn(){
    $this->db->select('count(id) as count');
    $this->db->from('Amend_products');
    $this->db->where('status','1');
    $result = $this->db->get()->row_array();
    return $result['count'];
  }

    public function get_engaged_karigar_by_order(){
    $this->db->select('DISTINCT(count(ppl.email_order_id)) as count');
    $this->db->from('Prepare_product_list ppl');
    $this->db->join('karigar_master k','k.id = ppl.karigar_id');
    $this->db->where('ppl.status','1');
    $this->db->where('ppl.module','1');
    $this->db->group_by('ppl.email_order_id');
    $result = $this->db->get()->row_array();
    // echo $this->db->last_query();die;
    return $result['count'];
  }


 
 

}
