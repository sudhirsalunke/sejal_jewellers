<?php

class Corporate_Products_model extends CI_Model {

  function __construct() {
      parent::__construct();
  }
  public function find($id){
  	$this->db->select('*');
    $this->db->from('corporate_products cp');
    $this->db->where('cp.id',$id);
  	$result = $this->db->get()->row_array();
  	return $result;
  }
  public function get_corporates_by_corporate_id($corporate_id){
    $this->db->select('eo.corporate');
    $this->db->where('cp.id',$corporate_id);
    $this->db->from('corporate_products cp');
    $this->db->join('email_orders eo','eo.order_id=cp.order_id');
    return $this->db->get()->row_array();
  }
  public function get_corporates(){
    return $this->db->get('corporate')->result_array();
  }

  function get_weight_ranges(){
    $this->db->select('DISTINCT(weight_band)');
    $this->db->where('weight_band !=','Is null');
    $this->db->from('corporate_products');
    return $this->db->get()->result_array();
  }
  function get_categories(){
    $this->db->select('DISTINCT(product_master), COUNT(id) as product_count');
    $this->db->where('product_master !=','Is null');
    $this->db->where('product_master !=','');
    $this->db->where('status =','0');
    $this->db->from('corporate_products');
    $this->db->group_by('product_master');
    return $this->db->get()->result_array();
  }

     public function corporate_get($cp_id)
    {

      if($cp_id == 1){$this->db->select('cp.id,cp.Item_number,cp.order_id');}
      else{ $this->db->select('cp.id,cp.sort_Item_number as Item_number,cp.order_id');}     
      $this->db->from('corporate_products cp');
      $this->db->join('email_orders eo','eo.order_id=cp.order_id');
      $this->db->where('eo.corporate',$cp_id);
      $result = $this->db->get()->result_array();
        return $result;
    } 

    public function update_sort_Item_number($id,$sort_Item)
    {
        $this->db->where('id',$id);
        $this->db->update('corporate_products',$sort_Item);
    }
    public function get_corporate_by_ids($ids){
      $this->db->select('p.product_code,p.image');
      $this->db->from('corporate_products cp');
      $this->db->join('product p','p.product_code = cp.sort_Item_number');
      $this->db->where_in('cp.id',$ids);
      $result = $this->db->get()->result_array();
      return $result;
    }
}  
