<?php
class Generate_packing_list_model extends CI_Model {

  function __construct() {
      $this->table_name = "generate_packing_list";
      parent::__construct();
  }
  public function validatepostdata(){
    $data['status'] = 'success';
    $data['error'] ='';
    $data['error']['gold_rate']="";
    $data['error']['products']="";
      if (!isset($_POST['product_code']) && count(@$_POST['product_code']) ==0) {
          $data['status']= 'failure';
          $data['data']= '';
          $data['error']['products'] = 'Please select products..';
        }
   $this->form_validation->set_rules('gold_rate', 'Gold Rate', 'trim|required|numeric');
    $this->form_validation->set_rules('other_charges', 'other charges', 'trim|required|numeric');
   if($this->form_validation->run()===FALSE){
      $data['status']= 'failure';
      $data['data']= '';
      $data['error']['gold_rate']=strip_tags(form_error('gold_rate'));
      $data['error']['other_charges']=strip_tags(form_error('other_charges'));

    } 
   
   // $this->error=false;
   // $data['status'] = 'success';
   // $data['error'] ='';

   //  if(!array_key_exists('quality_control_id', $_POST)){
   //    //print_r($_POST); die();
   //    $data['status']= 'failure';
   //    $data['data']= '';
   //    $data['error']['products'][0] = '*Please select products. ';
   //  }
   
    return $data;
  
  }
 public function get($filter_status='',$status='',$params='',$search='',$limit=''){
    $this->db->select('*,qc.created_at,qc.quantity,qc.receive_product_id,qc.cp_size as size,qc.total_gr_wt,qc.total_net_wt,rp.category_name,rp.product_code,qc.id,qc.status,qc.status as qc_status,qc.corporate_product_id,eo.order_id,eo.corporate,corp.name as corporate_name,c.id as category_id,qc.id');
    $this->db->from('quality_control qc');
    $this->join_cond();
    $this->db->where('qc.status','1');
    $this->db->where('eo.corporate','2');
    //$this->all_like_queries($search);
  /*  if(!empty($filter_status)){
      $this->db->order_by($status[$filter_status['column']],$filter_status['dir']);
    }  */
    if($limit == true){
      $this->db->limit($params['length'],$params['start']);
    }
    $this->db->group_by('qc.id');
    if(!empty($filter_status)){
      //$this->db->order_by('eo.corporate',$filter_status['dir']);
      $this->db->order_by('eo.corporate','DESC');
    }
     if (isset($params['columns']) && !empty($params['columns'])){
      $filter_input=$params['columns'];
      $table_col_name='Generate_packing_list_create';
      $this->get_filter_value($filter_input,$table_col_name);
    }
   

    if($limit == true){
      $this->db->order_by('qc.id','DESC');
      $this->db->limit($params['length'],$params['start']);
      $result = $this->db->get()->result_array();
      //echo $this->db->last_query();print_r($result);exit;
    }else{
      $row_array = $this->db->get()->result_array();
      $result = count($row_array);
      //echo $this->db->last_query();print_r($row_array);exit;
    }
    return $result;
  }
  
  public function get_table_data($filter_status='',$status='',$params='',$search='',$limit='',$id='')
  { 
    $this->db->select('qc.id qc_id,cp.*,cp.id cp_id, cp.id as corporate_id,cp.sort_Item_number as product_code,cp.order_id as order_id,qc.*,count(gp.id) as no_of_products,sum(qc.quantity) as gp_quantity,DATE_FORMAT(gp.created_at,"%d-%m-%Y %T") as gp_created_at,gp.*');
    $this->db->from('generate_packing_list gp');
    $this->db->join('quality_control qc','qc.id = gp.quality_control_id','left');
    $this->db->join('corporate_products cp','cp.id = qc.corporate_product_id');
  /*  $this->db->join('corporate_products cp','cp.id = qc.corporate_product_id');
    $this->db->join('product p', 'p.product_code = cp.sort_Item_number');
    $this->db->join('category_master c', 'c.id = p.category_id', 'left');*/
    //
    // $this->db->where('gp.status','0');
    if($id!='')
      $this->db->where('gp.order_no',$id);

    if (@$_GET['status']=="approved") {
      $this->db->where('gp.status','1');
    }else if (@$_GET['status']=="rejected") {
      $this->db->where('gp.status','2');
    }else{
      $this->db->where('gp.status','0');
    }
     if (isset($params['columns']) && !empty($params['columns'])){
      $filter_input=$params['columns'];
      $table_col_name='Packing_list';
      $this->get_filter_value($filter_input,$table_col_name);
    }
 /*    if(!empty($search)){
      $this->db->where("(gp.order_no LIKE '%$search%' OR DATE_FORMAT(gp.created_at,'%d-%m-%Y') LIKE '%$search%')");
    }*/
    if(!empty($filter_status)){
      $this->db->order_by($status[$filter_status['column']],$filter_status['dir']);
    } 
    $this->db->group_by('gp.order_no');
    

    if($limit == true){
    $this->db->order_by('gp.id','DESC');
      $this->db->limit($params['length'],$params['start']);
      $result = $this->db->get()->result_array();
    }else{
      $row_array = $this->db->get()->result_array();
      $result = count($row_array);
      //echo $this->db->last_query();print_r($result);exit;
    }

  // echo $this->db->last_query(); print_r($result); die();
    return $result;
  }
 
  public function get_data_by_order_id($filter_status='',$status='',$params='',$search='',$limit='',$order_id){ 
   $this->db->select('*,c.name as category_name,qc.quantity as qc_quantity,gp.created_at as gp_date,rp.size as rp_size,gp.status as gp_status,gp.id as gp_id,c.id');
    $this->db->from('generate_packing_list gp');
    $this->db->join('quality_control qc','qc.id = gp.quality_control_id','left');
    $this->db->join('Receive_products rp','rp.id = qc.receive_product_id','left');
    $this->db->join('corporate_products cp','cp.id = qc.corporate_product_id','left');
    $this->db->join('product p', 'p.product_code = cp.sort_Item_number','left');
    $this->db->join('category_master c', 'c.id = p.category_id', 'left');
    //$this->db->where('gp.status','0');
//print_r($params['columns']);  
  if(isset($params['columns']) && !empty($params['columns'])){
        $filter_input=$params['columns'];
        $table_col_name='packing_order_view';
        $this->get_filter_value($filter_input,$table_col_name);
      }
    if($order_id!='')
      $this->db->where('gp.order_no',$order_id);

     if(@$_GET['status']=="approved") {
      $this->db->where('gp.status','1');
    }else if(@$_GET['status']=="rejected") {
      $this->db->where('gp.status','2');
    }
  /*  if(!empty($search)){
     $this->all_like_queries($search);
    }*/
    $this->db->order_by('gp.id','DESC');
    $result = $this->db->get()->result_array();
/*    echo $this->db->last_query();
    print_r($result);exit;*/
    return $result;
  }
  public function get_export_data($id='')
  { 
   // $this->db->select('cp.order_id carat_lane_order,cp.product_code_shilpi as Shilpi_design_code,external_item_number Caratlane_sku_code,c.name as Category,"" prod_Wt,"" Less_wt,"" Net_Wt,"" Wastage,"" Lab_per_grm,"" Other_charge,"" Total_lab,"" Gold_Rate_995,"" Gold_Rate_22_kt,"" Gold_Rate_pure_with_lab,"" Unit_price,gp.*,qc.id qc_id,cp.*,cp.id cp_id, cp.id as corporate_id,cp.external_item_number as product_code,cp.order_id as order_id,qc.*');

    //old query 
/*  $this->db->select('cp.order_id carat_lane_order,cp.product_code_shilpi as Shilpi_design_code,sort_Item_number Caratlane_sku_code,c.name as Category,qc.total_gr_wt prod_Wt,qc.stone_wt Stone_Wt,(qc.total_gr_wt - qc.stone_wt) as Net_Wt,concat(c.wastage," %") Wastage,round(((qc.total_gr_wt - qc.stone_wt)*(92/100)/0.995),2) as prod_metal,round(((qc.total_gr_wt - qc.stone_wt)*(c.wastage/100)/0.995),2) as Lab_per_grm,gp.other_charges as Other_charge, round((((qc.total_gr_wt - qc.stone_wt)*(c.wastage/100)/0.995) * gp.gold_rate),0) as labour,gp.gold_rate as gold_rate_995,round(gp.other_charges+round((((qc.total_gr_wt - qc.stone_wt)*(c.wastage/100)/0.995) * gp.gold_rate),0),0) as total_labour');*/

  $this->db->select('cp.order_id carat_lane_order,cp.product_code_shilpi as Shilpi_design_code,sort_Item_number Caratlane_sku_code,c.name as Category,qc.total_gr_wt prod_Wt,IFnull(qc.stone_wt,0) Stone_Wt,(qc.total_gr_wt - IFnull(qc.stone_wt,0)) as Net_Wt,concat(c.wastage," %") Wastage,round(((qc.total_gr_wt - IFnull(qc.stone_wt,0))*(92/100)/0.995),2) as prod_metal,round(((qc.total_gr_wt - IFnull(qc.stone_wt,0))*(c.wastage/100)/0.995),2) as Lab_per_grm,gp.other_charges as Other_charge, round((((qc.total_gr_wt - IFnull(qc.stone_wt,0))*(c.wastage/100)/0.995) * gp.gold_rate),0) as labour,gp.gold_rate as gold_rate_995_With_GST,round(gp.other_charges+round((((qc.total_gr_wt - IFnull(qc.stone_wt,0))*(IFnull(c.wastage,0)/100)/0.995) * gp.gold_rate),0),0) as total_labour');
    // $this->db->select('gp.*');

    $this->db->from('quality_control qc');
    $this->whr_join_cond(); 
    if($id!='')
      $this->db->where('gp.order_no',$id);
   //$this->db->group_by('gp.order_no');
    $result = $this->db->get()->result_array();
    //echo $this->db->last_query();
    // print_r($result);exit;
    return $result;
  }
  public function get_export_main_heading_data($id='')
  { 
    $this->db->select('DATE_FORMAT( gp.created_at, "%d-%m-%Y" ) as gp_created_at');
    $this->db->from('quality_control qc');
    $this->whr_join_cond(); 
    if($id!='')
      $this->db->where('gp.order_no',$id);
   //$this->db->group_by('gp.order_no');
    $result = $this->db->get()->result_array();
    //echo $this->db->last_query();
    // print_r($result);exit;
    return $result;
  }
  private function whr_join_cond(){
    $this->db->join('corporate_products cp','cp.id = qc.corporate_product_id','left');
    $this->db->join('generate_packing_list gp','gp.quality_control_id = qc.id','left');
    $this->db->join('product p','p.product_code = cp.sort_Item_number','left');
    $this->db->join('category_master c', 'c.id = p.category_id', 'left');
    $this->db->where('gp.status','0');


  }
  private function join_cond(){
    $this->db->join('Receive_products rp','rp.id = qc.receive_product_id');
    $this->db->join('corporate_products cp','cp.id = qc.corporate_product_id');
    $this->db->join('product p','p.product_code = cp.sort_Item_number','left');
    $this->db->join('category_master c', 'c.id = p.category_id', 'left');
    $this->db->join('email_orders eo','eo.order_id = cp.order_id');
    $this->db->join('corporate corp','corp.id = eo.corporate');

  }
  public function store($insert_arr){
   $order_id = $this->db->query("select max(order_no) as order_id from generate_packing_list")->row('order_id');
   
    foreach ($_POST['product_code'] as $key => $value) {
      $data1['quality_control_id'] = $value;
      $email_order_id=$this->email_order_id($value);      
      $data1['email_order_id'] = $email_order_id;
      $data1['order_no'] = $order_id+1;
      $data1['gold_rate'] = $_POST['gold_rate'];
      $data1['quantity'] = $_POST['quantity'];
      $data1['other_charges'] = $_POST['other_charges'];
      $data1['status'] = '0';
      $data1['created_at'] = date('Y-m-d H:i:s');
  
      $this->db->insert($this->table_name,$data1);
      $qc_data=$this->Quality_control_model->get_karigar_id_from_qc_id($value);
      corporate_product_update($qc_data['email_order_id'],'14',$qc_data['sort_Item_number']);//status 14-PACKING LIST Caratlane
      $this->db->set('status','7');
      $this->db->where('id',$value);
      $this->db->update('quality_control');

      $data['status'] = 'success';
    }
    
    return $data;
  }
 /* private function all_like_queries($search){
    $this->db->where("(cp.sort_Item_number LIKE '%$search%' OR cp.order_id LIKE '%$search%' OR c.name LIKE '%$search%')");

  }*/
  private function email_order_id($qc_id)
{
  $this->db->select('email_order_id');
  $this->db->where('id',$qc_id);
  $this->db->from('quality_control');
  $result = $this->db->get()->row_array();
  return $result['email_order_id'];
}


  public function get_category_list($category_ids){
    // $sql = $this->db->query('select * from category_master where id IN ('.$cat_arr.')');
    $this->db->where_in('id',$category_ids);
    $result = $this->db->get('category_master')->result_array();
    return $result;
  }

  public function change_status(){
    $post=$this->input->post();
    if (!empty($post['id']) && !empty($post['status'])) {
      $this->db->set('status',$post['status']);
      $this->db->where('id',$post['id']);
      if ($this->db->update($this->table_name)) {
          $this->db->select('quality_control_id');
          $this->db->where('id',$_POST['id']);
          $this->db->from($this->table_name);
          $result = $this->db->get()->row_array();
          $qc_data=$this->Quality_control_model->get_karigar_id_from_qc_id($result['quality_control_id']);
          if($post['status']=='1'){
            corporate_product_update($qc_data['email_order_id'],'15',$qc_data['sort_Item_number']);//status 15-PRODUCT APPROVED Caratlane
          }else{
            corporate_product_update($qc_data['email_order_id'],'16',$qc_data['sort_Item_number']);//status 16-PRODUCT REJECTED Caratlane
          }
        return get_successMsg();
      }
    }else{
      return get_errorMsg();
    }
  
  }

  private function get_filter_value($filter_input,$table_col_name){
    $column_name=array();  
    $filter_column_name=filter_column_name($table_col_name);
    $sql='';
    $i=0;
     
     //print_r($filter_input);die;
    foreach ($filter_input as $key => $search_value){
       $column_name=$filter_column_name;
       //print_r($search_value['search']['value']);
        if(!empty($search_value['search']['value'])){
          if($i != 0){
            $sql.=' AND  ';
          }
            $sql.=''.$column_name[$key].' like "%'.$search_value['search']['value'].'%" ';
            $i++;
        

         }   
    }

    if(!empty($sql)){  
      $this->db->where($sql);  
    }     
       
  }
  public function find_qc_id($id){
    $this->db->select('*');
    $this->db->from($this->table_name);
    $this->db->where('id',$id);
    return $this->db->get()->row_array();
  }

  public function update($array,$where){
    $this->db->where($where);
    $this->db->update($this->table_name,$array);
  }
  public function insert_in_stock($result,$data,$qc_id){  
   $email_order_id = email_order_id($result['corporate_product_id']);   
   $insert_array = array(
        'corporate_product_id'=>$result['corporate_product_id'],
        'email_order_id'=>$email_order_id,
        'qc_id'=>$result['id'],
        'quantity'=>$result['quantity'],
        'karigar_id'=>$data['karigar_id'],
        'corporate'=>$data['corporate'],
        'receive_product_id'=>$data['receive_product_id'],
        'status'=>'1',
        'created_at'=>date('Y-m-d H:i:sa'),
        'updated_at'=>date('Y-m-d H:i:sa'),
      );
    if($this->db->insert('stock',$insert_array)){
      return true;
    }else{
      return false;
    }
  }
 
}