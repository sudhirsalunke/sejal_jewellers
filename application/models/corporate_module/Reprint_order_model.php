<?php

class Reprint_order_model extends CI_Model {

  function __construct() {
      $this->table_name = "Prepare_product_list";
      parent::__construct();
  }

 
  
  public function get($karigar_id){

    /*$this->db->select('group_concat(distinct round(line_number) line_number) as line_numbers,group_concat(distinct `cp`.`id` SEPARATOR ",") as `UID`, group_concat( `cp`.`size_master` SEPARATOR "/") as size_master,
 group_concat( `cp`.`finding_cost` SEPARATOR "/") as finding_cost,
 group_concat( `cp`.`weight_band` SEPARATOR "/") as weight_band,km.name,km.code as karigar_code,cp.order_id,cp.CW_quantity as pcs,cp.CW_unit as unit,p.size,p.product_code,p.image,DATE_FORMAT( eo.date,  "%d %M %Y" ) as date,CONCAT(w.from_weight,"-",w.to_weight) as weight,DATE_FORMAT(cp.proposed_delivery_date,"%d %M %Y") as proposed_delivery_date,cp.barcode_path,ppl.status as ppl_status, DATE_FORMAT( ppl.karigar_engaged_date,  "%d %b %Y" ) as karigar_engaged_date,cp.size as cp_size,cp.net_weight,(select sum(quantity) from Prepare_product_list where corporate_products_id IN(group_concat( distinct `cp`.`id`))) as ppl_quantity,eo.corporate,cp.product_master,DATE_FORMAT( ppl.karigar_delivery_date,  "%d %b %Y" ) as karigar_delivery_date');*/
 $this->db->select('group_concat(distinct round(line_number) ORDER BY cp.id ASC) as line_numbers,group_concat(distinct `cp`.`id` SEPARATOR ",") as `UID`, group_concat( (`cp`.`size_master`)ORDER BY cp.id ASC SEPARATOR "/") as size_master,group_concat( `cp`.`finding_cost` SEPARATOR "/") as finding_cost,
 group_concat(distinct `cp`.`weight_band` SEPARATOR "/") as weight_band,group_concat( `cp`.`quantity` SEPARATOR "/" ) as cp_weight_band,km.name,km.code as karigar_code,cp.order_id,cp.CW_quantity as pcs,cp.CW_unit as unit,p.size,p.product_code,p.image,DATE_FORMAT( eo.date,  "%d %M %Y" ) as date,CONCAT(w.from_weight,"-",w.to_weight) as weight,DATE_FORMAT(cp.proposed_delivery_date,"%d %M %Y") as proposed_delivery_date,group_concat(ppl.barcode_path SEPARATOR ",") as barcode_path,ppl.status as ppl_status, DATE_FORMAT( ppl.karigar_engaged_date,  "%d-%m-%Y" ) as karigar_engaged_date,cp.size as cp_size,cp.net_weight,(select sum(quantity) from Prepare_product_list where corporate_products_id IN(group_concat( distinct `cp`.`id`))) as ppl_quantity,eo.corporate,cp.product_master,DATE_FORMAT( ppl.karigar_delivery_date,  "%d-%m-%Y" ) as karigar_delivery_date,cp.quantity as cp_weight,(select round(( cp.quantity*tolerance / 100 ),2) from tolerance_master where cp.quantity >= from_wt  and cp.quantity  <= to_wt) as tolerance_wt,(select round(( cp.net_weight*tolerance / 100 ),2) from tolerance_master where cp.net_weight >= from_wt  and cp.net_weight  <= to_wt) as tolerance_c_wt,group_concat(cp.barcode_path SEPARATOR ",") as cp_barcode_path');
    $this->db->from($this->table_name.' ppl');
    $this->db->join('corporate_products cp','cp.id = ppl.corporate_products_id');
    $this->db->join('email_orders eo','eo.order_id = cp.order_id');
    $this->db->join('product p','cp.sort_Item_number = p.product_code');
    $this->db->join('weights w','w.id = p.weight_band_id','left');
    $this->db->join('karigar_master km','km.id = ppl.karigar_id');
    $this->db->where('ppl.karigar_id',$karigar_id);
    //$this->db->where('ppl.status','1');
    $this->db->where('(ppl.status = "1" OR ppl.status = "2")');
    //print_r($_POST['filter_o']);
        if (isset($_POST['filter'])) {
        if(!empty($_POST['filter']['order_id'])){            
           $this->db->where_in('cp.order_id',$_POST['filter']['order_id']);
        }
         if(!empty($_POST['filter']['product_code'])){
            $this->db->where_in('cp.sort_Item_number',$_POST['filter']['product_code']);
        }
        if(!empty($_POST['filter']['weight_range'])){
         // print_r($_POST['filter']['weight_range']);die;
          $this->db->where_in('cp.weight_band',$_POST['filter']['weight_range']);
        }

   /*     if(!empty($_POST['filter']['pcs'])){
           //$this->db->where_in('cp.CW_quantity',$_POST['filter']['pcs']);
        }*/
        if(!empty($_POST['filter']['category'])){
           $this->db->where_in('cp.product_master',$_POST['filter']['category']);
        }
// print_r($_POST['filter']['size']);die;
        if(!empty($_POST['filter']['size'])){
          $this->db->where_in('cp.size_master',$_POST['filter']['size']);
        }
    }
     if (isset($_POST['filter_o'])) {
        if(!empty($_POST['filter_o']['order_id'])){
           $order_id=explode(',', $_POST['filter_o']['order_id']);
           $this->db->where_in('cp.order_id',$order_id);
        }
         if(!empty($_POST['filter_o']['product_code'])){
           $product_code=explode(',', $_POST['filter_o']['product_code']);
            $this->db->where_in('cp.sort_Item_number',$product_code);
        }

        if(!empty($_POST['filter_o']['weight_range'])){
           $weight_range=explode(',', $_POST['filter_o']['weight_range']);
           $this->db->where_in('cp.weight_band',$weight_range);
        }

       /* if(!empty($_POST['filter_o']['pcs'])){
           $pcs=explode(',', $_POST['filter_o']['pcs']);
           $this->db->where_in('cp.sort_Item_number',$pcs);
        }*/
          if(!empty($_POST['filter_o']['category'])){
           $category=explode(',', $_POST['filter_o']['category']);
           $this->db->where_in('cp.product_master',$category);
        }


        if(!empty($_POST['filter_o']['size'])){
           $size=explode(',', $_POST['filter_o']['size']);
           $this->db->where_in('cp.size_master',$size);
        }
    }


       $this->db->group_by(array('cp.sort_Item_number','cp.work_order_id','cp.order_id'));



    $this->db->order_by('cp.id','DESC');
    //$this->db->group_by('cp.id');
    $result = $this->db->get()->result_array();
    //echo $this->db->last_query();exit;print_r($result);exit;
    return $result;
  }

    public function get_product_code($karigar_id,$order_id){
     $this->db->select('cp.order_id,p.product_code');/*fetching products by order_id*/
    $this->db->from($this->table_name.' ppl');
    $this->db->join('corporate_products cp','cp.id = ppl.corporate_products_id');
    $this->db->join('email_orders eo','eo.order_id = cp.order_id');
    $this->db->join('product p','cp.sort_Item_number = p.product_code');
    $this->db->where('ppl.karigar_id',$karigar_id);
    $this->db->where('ppl.status','1');
    $this->db->where_in('cp.order_id',$order_id);
    $this->db->group_by('cp.sort_Item_number');
    $this->db->order_by('cp.id','DESC');
    $result = $this->db->get()->result_array();
    // echo $this->db->last_query();print_r($result);exit;
    return $result;
  }

  public function get_orders_by_krigar($karigar_id){
     $this->db->select('cp.order_id');/*fetching products by order_id*/
    $this->db->from($this->table_name.' ppl');
    $this->db->join('corporate_products cp','cp.id = ppl.corporate_products_id');
    $this->db->join('email_orders eo','eo.order_id = cp.order_id');
    $this->db->where('ppl.karigar_id',$karigar_id);
    $this->db->where('ppl.status','1');
    $this->db->group_by('cp.order_id');
    $this->db->order_by('cp.id');
    $result = $this->db->get()->result_array();
    // echo $this->db->last_query();print_r($result);exit;
    return $result;
  }

  function get_categories(){
    $this->db->select('DISTINCT(product_master), COUNT(id) as product_count');
    $this->db->where('product_master !=','Is null');
    $this->db->where('product_master !=','');   
    $this->db->from('corporate_products');
    $this->db->group_by('product_master');
    return $this->db->get()->result_array();
  }

public function sent_to_karigar_reprint($data){
  // print_r($data);exit;
 $this->db->select('group_concat(distinct round(line_number) ORDER BY cp.id ASC) as line_numbers,group_concat(distinct `cp`.`id` SEPARATOR ",") as `UID`, group_concat( (`cp`.`size_master`)ORDER BY cp.id ASC SEPARATOR "/") as size_master,

 group_concat( `cp`.`finding_cost` SEPARATOR "/") as finding_cost,
 group_concat(distinct `cp`.`weight_band` SEPARATOR "/") as weight_band,group_concat( `cp`.`quantity` SEPARATOR "/" ) as cp_weight_band,km.name,km.code as karigar_code,cp.order_id,cp.CW_quantity as pcs,cp.CW_unit as unit,p.size,p.product_code,p.image,DATE_FORMAT( eo.date,  "%d %M %Y" ) as date,CONCAT(w.from_weight,"-",w.to_weight) as weight,DATE_FORMAT(cp.proposed_delivery_date,"%d %M %Y") as proposed_delivery_date,ppl.barcode_path,ppl.status as ppl_status, DATE_FORMAT( ppl.karigar_engaged_date,  "%d-%m-%Y" ) as karigar_engaged_date,cp.size as cp_size,cp.net_weight,(select sum(quantity) from Prepare_product_list where corporate_products_id IN(group_concat( distinct `cp`.`id`))) as ppl_quantity,eo.corporate,cp.product_master,DATE_FORMAT( ppl.karigar_delivery_date,  "%d-%m-%Y" ) as karigar_delivery_date,cp.quantity as cp_weight,(select round(( cp.quantity*tolerance / 100 ),2) from tolerance_master where cp.quantity >= from_wt  and cp.quantity  <= to_wt) as tolerance_wt,(select round(( cp.net_weight*tolerance / 100 ),2) from tolerance_master where cp.net_weight >= from_wt  and cp.net_weight  <= to_wt) as tolerance_c_wt,ppl.id as ppl_id,cp.barcode_path as cp_barcode_path');
    $this->db->from($this->table_name.' ppl');
    $this->db->join('corporate_products cp','cp.id = ppl.corporate_products_id');
    $this->db->join('email_orders eo','eo.order_id = cp.order_id');
    $this->db->join('product p','cp.sort_Item_number = p.product_code');
    $this->db->join('weights w','w.id = p.weight_band_id','left');
    $this->db->join('karigar_master km','km.id = ppl.karigar_id');
    $this->db->where('cp.order_id',$data['order_id']);
    if(!empty($data['Design_Id'])){
      $this->db->where('cp.sort_Item_number',$data['Design_Id']);
    }
     if(!empty($data['Karigar_Name'])){
      $this->db->where('km.name',$data['Karigar_Name']);
    }
     if(!empty($data['Order_Date'])){
      $this->db->where('DATE(eo.date)',$data['Order_Date']);
    }
     if(!empty($data['Proposed_Delivery_Date'])){
      $this->db->where('cp.proposed_delivery_date',$data['Proposed_Delivery_Date']);
    }
    $this->db->where('ppl.status','1');
    $this->db->group_by(array('cp.sort_Item_number','cp.work_order_id','ppl.corporate_products_id'));
    $this->db->order_by('cp.id','DESC');
    $result = $this->db->get()->result_array();
  //echo $this->db->last_query();echo "<pre>";print_r($result);exit;
    return $result;
  }

}    
