<?php

class Receive_products_model extends CI_Model {

  function __construct() {
      $this->table_name = "Receive_products";
      parent::__construct();
  }

  public function validation(){
    $postdata = $_POST['received_products'];
    $data['status'] = 'success';
    if(empty(array_filter($postdata['gross_wt']))){
     $data['status'] = 'error';
     $data['error']  = 'Please Enter at least one product gross weight value';
    
    } 
    //print_r($postdata );die;
        foreach ($postdata['id'] as $key => $value) {
          if(!empty($_POST['received_products']['gross_wt'][$key])){
                $this->value = $value;
                    // print_r($value);
                $_POST['quantity'] = $_POST['received_products']['quantity'][$key];
                $_POST['size'] = $_POST['received_products']['size'][$key];
                $_POST['gross_wt'] = $_POST['received_products']['gross_wt'][$key];
                $_POST['net_wt'] = $_POST['received_products']['net_wt'][$key];
                $_POST['less_wt'] = $_POST['received_products']['less_wt'][$key];
                $_POST['corporate_product_id'] = $_POST['received_products']['corporate_product_id'][$key];
                $_POST['from_wt_torlen'] = $_POST['received_products']['from_wt_torlen'][$key];
                $_POST['to_wt_torlen'] = $_POST['received_products']['to_wt_torlen'][$key];
                $_POST['order_id'] = $_POST['received_products']['email_order_id'][$key];
                $this->form_validation->set_rules('quantity', 'Quantity', 'required|received_products_qnt_val|greater_than_equal_to[0.1]');
                // $_POST['stone_wt'] = $_POST['received_products']['stone_wt'][$key];
                // $this->form_validation->set_rules('size', 'Size', 'required|numeric');

                  $this->form_validation->set_rules('gross_wt', 'Gross Wt', 'required|greater_than_equal_to[0.1]');
                if(isset($_POST['received_products']['qc'][$value])){
                      $this->form_validation->set_rules('less_wt', 'less Wt', 'required|greater_than_equal_to[0.0]');
                  // $this->form_validation->set_rules('stone_wt', 'Stone Wt', 'numeric'); }
                  //         )));
                  $this->form_validation->set_rules('net_wt', 'Net Wt', array('required', array(
                                  'check_net_wt',
                                  function($str)
                                  {
                                        $corporate_id=$this->Quality_control_model->get_corporate_by_cp_id($_POST['received_products']['corporate_product_id'][$this->value]);
                                          if ($_POST['net_wt'] < $_POST['from_wt_torlen']  || $_POST['net_wt'] > $_POST['to_wt_torlen'] ) {
                                       $this->form_validation->set_message('check_net_wt', 'Net Weight should be in '.number_format($_POST['from_wt_torlen'] , 2, '.', '') .' - '.number_format($_POST['to_wt_torlen'] , 2, '.', ''));
                                      return false;
                                    }else{
                                      return true;
                                    }

                                /*    if($corporate_id == '2'){
                                          $weight_band=$this->Quality_control_model->get_weight_band($_POST['received_products']['corporate_product_id'][$this->value],$corporate_id);
                                        if ($_POST['net_wt'] < $weight_band[0]  || $_POST['net_wt'] > $weight_band[1]) {
                                           $this->form_validation->set_message('check_net_wt', 'Net Weight should be in '.$weight_band[0].' - '.$weight_band[1]);
                                          return false;
                                        }else{
                                          return true;
                                        }
                                    }else{
                                      if ($_POST['net_wt'] < $_POST['from_wt_torlen']  || $_POST['net_wt'] > $_POST['to_wt_torlen'] ) {
                                       $this->form_validation->set_message('check_net_wt', 'Net Weight should be in '.$_POST['from_wt_torlen'] .' - '.$_POST['to_wt_torlen']);
                                      return false;
                                    }else{
                                      return true;
                                    }

                                    }*/
                                   
                                  }
                          )));
                   
                }

                if ($this->form_validation->run() == FALSE) {
                  $data['status'] = 'failure';
                  $data['error'][$value] = $this->get_errors($value);
                }
              $this->form_validation->reset_validation();
              }//die;
      }  
    return $data;
  }
 
  private function get_errors($key){
    return array(
      'quantity_error_'.$key=>strip_tags(form_error('quantity')),
      'size_error_'.$key=>strip_tags(form_error('size')),
      'gross_wt_error_'.$key=>strip_tags(form_error('gross_wt')),
      'net_wt_error_'.$key=>strip_tags(form_error('net_wt')),
      'less_wt_error_'.$key=>strip_tags(form_error('less_wt')),
      // 'stone_wt_error_'.$key=>strip_tags(form_error('stone_wt')),
    );
  }
  private function getErrorMsg(){
    return array(
      'quantity'=>strip_tags(form_error('product[quantity]')),
      'size'=>strip_tags(form_error('product[size]'))
    );
  }
  public function store(){
    $insert_array = array();
    $postdata = $this->input->post('received_products');
   //print_r($postdata);die;
    
    $id=@$postdata['id'];
    foreach ($id as $key => $value) {
          if(!empty($postdata['gross_wt'][$value])){
                // $date = date("Y-m-d H:i:s", strtotime($postdata['date'][$i]));
                $date = date("Y-m-d H:i:s");
                $email_order_id = email_order_id($postdata['corporate_product_id'][$value]);
                $insert_array = array(
                  'Product_code' => $postdata['product_code'][$value],
                  'category_name' => $postdata['category_name'][$value],
                  'date' => $date,
                  'quantity' => $postdata['quantity'][$value],
                  'net_wt' => $postdata['net_wt'][$value],
                  'gross_wt' => $postdata['gross_wt'][$value],
                  'less_wt' => $postdata['less_wt'][$value],
                  'line_number' => $postdata['line_number'][$value],
                  'status' => '1',
                  'module' => '1',
                  'size' => $postdata['size'][$value],
                  'corporate_product_id' => $postdata['corporate_product_id'][$value],
                 // 'email_order_id'=>$email_order_id,
                  'karigar_id' => $postdata['karigar_id'][$value],
                  'email_order_id' => $email_order_id,
                  'corporate_id' => $postdata['corporate'][$value],
                  'order_id' => $postdata['order_id'][$value],
                  'barcode_path' => $postdata['barcode_path'][$value],
                  );
                //print_r($email_order_id);die;
                if(!empty($postdata['qc'][$value])){
                  $insert_array['is_skip_qc'] = '1';

                }
                $this->db->insert($this->table_name,$insert_array);

                $id = $this->db->insert_id();
                $this->update_karigar_wt_limit($insert_array);
                
                if(isset($insert_array['is_skip_qc']) && $insert_array['is_skip_qc'] == '1'){
                  $this->insert_into_qc($postdata,$id,$insert_array,$value);
                  if($postdata['corporate'][$value]=='1'){
                    corporate_product_update($email_order_id,'6',$postdata['product_code'][$value]);//status  6-PENDING FOR HALLMARKING
                  }else{
                    corporate_product_update($email_order_id,'13',$postdata['product_code'][$value]);//status  13-GENERATE PACKING LIST Caratlane
                  }
                }else{
                corporate_product_update($email_order_id,'3',$postdata['product_code'][$value]);//status 3-PENDING PRODUCT FOR QC
                }
                date_update('Receive_products',$id,'insert','created_at','insert');
                $cp_id = $postdata['corporate_product_id'][$value];
                $order_id = $postdata['email_order_id'][$value];
                $is_update = $this->get_product_quantities($cp_id,$order_id);
                if($is_update == true){
                  $sdata['status'] = '2';
                  $this->db->select('ppl.id');
                  $this->db->from('Prepare_product_list ppl');
                  $this->db->join('corporate_products cp','cp.id = ppl.corporate_products_id');
                  $this->db->where('cp.id', $cp_id);
                  $this->db->where('cp.order_id', $order_id);
                  $this->db->where('ppl.status !=', '2');
                  $ppl_id = $this->db->get()->row_array();
                  $this->db->where('id',$ppl_id['id']);
                  $this->db->update('Prepare_product_list',$sdata);
                }
                $respons=get_successMsg();
                $respons['data']="Added Products Successfully";
              }
      }  
    return $respons;
  }

  public function update_karigar_wt_limit($insert_array){
  $this->db->set('engaged_wt', 'engaged_wt - ' . (float) $insert_array["gross_wt"], FALSE);
  $this->db->where('id',$insert_array['karigar_id']);
  $this->db->update('karigar_master'); 
    return true;
  }
  private function insert_into_qc($postdata,$id,$insert_array,$value){
    // if(!empty($postdata['qc'][$key])){
        $qc['is_skip_qc'] = '1';
        $qc['corporate_product_id'] = $insert_array['corporate_product_id'];
        $qc['email_order_id']  = $insert_array['email_order_id'];
        $qc['receive_product_id'] = $id;
        $qc['total_gr_wt'] = $postdata['gross_wt'][$value];
        $qc['total_net_wt'] = $postdata['net_wt'][$value];
        $qc['less_wt'] = $postdata['less_wt'][$value];
        $qc['product_code'] = $postdata['product_code'][$value];
        $qc['corporate'] = $postdata['corporate'][$value];
        // $qc['stone_wt'] = $postdata['stone_wt'][$value];
        $qc['size'] = $postdata['size'][$value];
        $qc['cp_size'] = $postdata['size'][$value];
        $qc['quantity'] = $postdata['quantity'][$value];
        $qc['created_at'] = date("Y-m-d H:i:s");
        $qc['barcode_path']  = $insert_array['barcode_path'];
        $qc_data = $this->Quality_control_model->store($qc,$from_rp=true);
      // }
  }
  private function get_product_quantities($product_code,$order_id){
    $this->db->select('sum(ppl.quantity) as quantity');
    $this->db->from('Prepare_product_list ppl');
    $this->db->join('corporate_products cp','cp.id = ppl.corporate_products_id');
    $this->db->where('cp.sort_Item_number', $product_code);
    $this->db->where('cp.order_id', $order_id);
    $cp_quantity = $this->db->get()->row_array();
    $this->db->select('sum(quantity) as quantity');
    $this->db->from('Receive_products');
    $this->db->where('product_code',$product_code);
    $this->db->where('order_id',$order_id);
    $rp_quantity = $this->db->get()->row_array();
    if($cp_quantity['quantity'] == $rp_quantity['quantity']){
      return true;
    }else{
      echo 'true';die;
      return false;
    }
  }
  public function update($update_array){
    $this->db->where('id',$update_array['receive_product_id']);
    $this->db->set('status',$update_array['status']);
    return $this->db->update($this->table_name);
  }
  public function get($filter_status='',$status='',$params='',$search='',$limit=''){
    if($limit == true){
    $this->db->select('rp.*, rp.date,cp.id as corporate_id,corp.name as corporate_name,sum(qc.quantity) qc_quanity,sbm.name as sub_category,cp.product_code_shilpi,corp.id as cp_id,c.id,rp.id as rpp_id,cp.sort_Item_number as Product_code,cp.email_order_id');
    }else{
      $this->db->select('count(distinct (rp.id)) as cont_record');
    }
    $this->db->from($this->table_name.' rp');
    $this->db->join('corporate_products cp','cp.sort_Item_number = rp.Product_code','left');
    $this->db->join('email_orders eo','eo.order_id = cp.order_id','left');
    $this->db->join('quality_control qc','qc.receive_product_id = rp.id','left');
    $this->db->join('category_master c', 'c.name= rp.category_name', 'left');
    $this->db->join('corporate corp','corp.id = eo.corporate','left');
    $this->db->join('product p','cp.sort_Item_number=p.product_code','left');
    $this->db->join('sub_category_master sbm','p.sub_category_id = sbm.id','left');
   /* if (isset($_REQUEST['columns'][1]['search']['value']) && !empty($_REQUEST['columns'][1]['search']['value'])) {
      $this->db->where('eo.corporate',$_REQUEST['columns'][1]['search']['value']);
    }*/
 if(isset($params['columns']) && !empty($params['columns'])){
      $filter_input=$params['columns'];
    $this->get_filter_value($filter_input);
    }
    $this->db->where('rp.status','1');
 /*   if(!empty($filter_status)){
      $this->db->order_by($status[$filter_status['column']],$filter_status['dir']);
    }*/
 /*   if(!empty($search)){
        $this->all_like_queries($search);
    }*/

    if($limit == true){
     $this->db->group_by('rp.id');
     $this->db->order_by('rp.id','DESC');
     $this->db->limit($params['length'],$params['start']);
     $result = $this->db->get()->result_array();
    }else{
      $row_array = $this->db->get()->row_array();
      $result = $row_array['cont_record'];
      //echo $this->db->last_query(); echo $result; die;      
    }
    //echo $this->db->last_query();
    /*
    print_r($result);die;*/
    return $result;
  }

  private function get_filter_value($filter_input){
    //print_r($filter_input['columns']);
  $column_name=array();  
  $filter_column_name=filter_column_name('Receive_products');
  //print_r($column_name);
    $sql='';
    $i=0;
   
   //print_r($column_name);
  foreach ($filter_input as $key => $serch_value){
     $column_name=$filter_column_name;
     //print_r($column_name[$key]);
      if(!empty($serch_value['search']['value'])){
        if($i != 0){
          $sql.=' AND  ';
        }
          $sql.=''.$column_name[$key].' like "%'.$serch_value['search']['value'].'%" ';
          $i++;
      

       }   
  }

  if(!empty($sql)){  
    $this->db->where($sql);  
  } 
   
       
  }

/*  private function all_like_queries($search){
    $this->db->where("(`rp`.`Product_code` LIKE '%$search%' OR cp.order_id LIKE '%$search%' OR cp.line_number LIKE '%$search%' OR DATE_FORMAT(rp.date,'%d-%m-%Y') LIKE '%$search%')");
  }*/
  private function all_join_queries(){
    $this->db->join('corporate_products cp','cp.sort_Item_number = rp.Product_code');
    $this->db->join('email_orders eo','eo.order_id = cp.order_id');
    $this->db->join('corporate corp','corp.id = eo.corporate');
  }
  private function get_CorporateId($code)
  {
    $result = $this->db->get_where('corporate_products',array("sort_Item_number" => $code))->row('id');
    return $result;
  }
  public function getNameByProductCode($Product_code)
  {
    $this->db->select("c.name as category_name,cp.id as corporate_product_id,cp.line_number");
    $this->db->from('product p');
    $this->db->join('corporate_products cp', 'cp.sort_Item_number = p.product_code');
    $this->db->join('category_master c', 'c.id = p.category_id', 'left');
    $this->db->join('Prepare_product_list ppl', 'cp.id = ppl.corporate_products_id');
    $this->db->where('product_code',$Product_code);
    $this->db->where('ppl.status','1');
    $result = $this->db->get()->result_array();
    return $result;
  }
  public function find($id){
  $this->db->select('rp.*,corporate,cp.quantity as cp_quantity,(select round(( cp.quantity*tolerance / 100 ),2) from tolerance_master where cp.quantity >= from_wt  and cp.quantity  <= to_wt) as tolerance_wt,(select round(( cp.net_weight*tolerance / 100 ),2) from tolerance_master where cp.net_weight >= from_wt  and cp.net_weight  <= to_wt) as tolerance_c_wt,cp.net_weight as cp_net_weight,cp.sort_Item_number,cp.email_order_id');
  $this->db->from('Receive_products rp');
  $this->db->join('product p','p.product_code = rp.Product_code','left');
  $this->db->join('corporate_products cp','cp.id = rp.corporate_product_id','left');  
  $this->db->where('rp.id',$id);
  return $this->db->get()->row_array();
  }
  public function get_karigar_by_productCode(){
    $array = array();
    $this->db->select('km.name,km.id');
    $this->db->from('Prepare_product_list ppl');
    $this->db->join('karigar_master km', 'ppl.karigar_id = km.id');
    $this->db->where('ppl.corporate_products_id',$_POST['cp_id']);
    $data = $this->db->get()->result_array();
    if(!empty($data)){
      foreach ($data as $key => $value) {
        $array[$value['id']]['name'] = $value['name'];
        $array[$value['id']]['id'] = $value['id'];
      }
    }
    return $array;
  }


  public function get_orders_by_productCode(){
    //foreach ($product_code as $key => $value) {
      $this->db->select('cp.order_id');
      $this->db->from('corporate_products cp');
      $this->db->join('Prepare_product_list ppl','ppl.corporate_products_id = cp.id');
      $this->db->where('cp.id',$_POST['cp_id']);
      $result = $this->db->get()->row_array();
      $data[$result['order_id']]['name'] = $result['order_id'];
    //}
    // $data = array_values($data);
    return $data;
  }
  public function get_category_name(){
        $this->db->select('name');
        $this->db->from('category_master');
        $result = $this->db->get()->result_array();
        return $result;
  }
  public function get_article_master(){
        $this->db->select('id,name');
        $this->db->from('article_master');
        $result = $this->db->get()->result_array();
        return $result;
  }

  public function check_corporate($id){
    $this->db->distinct();
    $this->db->select('corporate_id');
    $this->db->from($this->table_name.' rp');   
    $this->db->where('rp.status=','1');
    $this->db->where('rp.id IN ('.$id.')');
    $result=$this->db->get()->result_array();
  /*  echo $this->db->last_query();
    print_r($result);die;    */
    return $result;
    
  }
 
}