<?php

class Hallmarking_model extends CI_Model {

  function __construct() {
    $this->table_name = "quality_control_hallmarking";
    parent::__construct();
  }
  public function store($insert_array){
    //print_r($insert_array);
  	$get_total_quantity = $this->get_previous_qch_quanity($insert_array);
  	$get_qc_quantity = $this->Quality_control_model->get_quantity($insert_array['qc_id']);
    $get_qc_data = $this->Quality_control_model->get_karigar_id_from_qc_id($insert_array['qc_id']);
   //print_r($data);die;
  	$status = $this->get_error_status($get_total_quantity,$get_qc_quantity,$insert_array);
  	if($status){
	  	$qc_data = $this->Quality_control_model->find_by_qc_id($insert_array['qc_id']);    
	  	$insert_array['receive_product_id'] = $qc_data['receive_product_id'];
	  	$insert_array['corporate_product_id'] = $qc_data['corporate_product_id'];
      $insert_array['email_order_id'] = $qc_data['email_order_id'];
      $insert_array['barcode_path'] = $qc_data['barcode_path'];
      $insert_array['created_at'] = date('Y-m-d H:i:s');
      $date = date('Y-m-d');
      $insert_array['HM_receive_date'] = $date;
	  	if(isset($insert_array['rejected']) && $insert_array['rejected'] == true){
	  		$insert_array['status'] = '0';
        $product_status = 'HM Qc reject';
	  		unset($insert_array['rejected']);
	  	}else{
	  		$insert_array['status'] = '1';
        $product_status = 'HM Qc accept';
         corporate_product_update($get_qc_data['email_order_id'],'8',$get_qc_data['sort_Item_number']);//status 8-HALLMARKING QC ACCEPTED
	  	}
	  	if($this->db->insert($this->table_name,$insert_array)){
        $inserted_id = $this->db->insert_id();
        date_update('quality_control_hallmarking',$inserted_id,$product_status,'created_at','insert');
	  		if($this->get_previous_qch_quanity($insert_array) == $get_qc_quantity['quantity']){
	  			$this->db->where('id',$insert_array['qc_id']);
	  			$this->db->set('status','3');
	  			$this->db->update('quality_control');
	  		}
	  		return get_successMsg();
	  	}else{
	  		return get_errorMsg();
	  	}
  	}
  }
  private function get_error_status($get_total_quantity,$get_qc_quantity,$insert_array){
  	if($get_qc_quantity['quantity'] < ($get_total_quantity + $insert_array['quantity'])){
  		$data['status']= 'failure';
    	$data['data']= '';
    	$data['error'] = array('quantity'=>'Quantity Exceeded');
    	echo json_encode($data);die;
  	}elseif ($insert_array['quantity'] == 0) {
  		$data['status']= 'failure';
    	$data['data']= '';
    	$data['error'] = array('quantity'=>'Quantity should be greater than zero');
    	echo json_encode($data);die;
  	}
  	return true;
  }
  private function get_previous_qch_quanity($postdata){
  	$this->db->select('sum(quantity) quantity');
  	$this->db->from($this->table_name);
  	$this->db->where('qc_id',$postdata['qc_id']);
  	$result = $this->db->get()->row_array();
  	return $result['quantity'];
  }
  public function get($filter_status='',$status='',$params='',$search='',$limit=''){
    if($limit == true){
  	$this->db->select('qch.created_at,qch.quantity,qch.receive_product_id,qc.cp_size as size,qch.total_gr_wt,qch.total_net_wt,rp.category_name,cp.sort_item_number as product_code,qch.id,qc.status,qch.status as qc_status,qch.corporate_product_id,eo.order_id,eo.corporate,eo.corporate,corp.name as corporate_name,hc.name as hc_name,cp.work_order_id,cp.line_number,cm.id,qc.id as qc_id,qch.id as qch_id');
    }else{
      $this->db->select('count(qch.id) as cont_record');
     } 
    $this->db->from($this->table_name.' qch');
    $this->join_cond();
    $this->where_cond();
   /* if(!empty($search)){
    $this->all_like_queries($search);
  }*/
    if (isset($params['columns']) && !empty($params['columns'])){
      $filter_input=$params['columns'];
    $this->get_filter_value($filter_input);
    }
    if(!empty($filter_status)){
      $this->db->order_by($status[$filter_status['column']],$filter_status['dir']);
    }
    if($limit == true){
    $this->db->limit($params['length'],$params['start']);
    $this->db->order_by('qc.id','DESC');
    $result = $this->db->get()->result_array();
   }else{
      $row_array = $this->db->get()->row_array();
      $result = $row_array['cont_record'];
     //echo $this->db->last_query();die;
   }
    return $result;
  }
  private function where_cond(){
  	if(!empty($_GET['status']) && $_GET['status'] == 'receive')
  		$this->db->where('qch.status','1');
  	elseif (!empty($_GET['status']) && $_GET['status'] == 'rejected') {
  		$this->db->where('qch.status','0');
      $this->db->where('qch.module','1');
      $this->db->or_where('qch.status','5');
  	}elseif (!empty($_GET['status']) && $_GET['status'] == 'exported') {
      $this->db->where('qch.status','8');
    }
  }
  private function get_filter_value($filter_input){
    $column_name=array();  
    $filter_column_name=filter_column_name('Hallmarking');
    $sql='';
    $i=0;   
     //print_r($filter_input);die;
    foreach ($filter_input as $key => $search_value){
       $column_name=$filter_column_name;
       //print_r($search_value['search']['value']);
        if(!empty($search_value['search']['value'])){
          if($i != 0){
            $sql.=' AND  ';
          }
            $sql.=''.$column_name[$key].' like "%'.$search_value['search']['value'].'%" ';
            $i++;
         }   
    }
    if(!empty($sql)){  
      $this->db->where($sql);  
    }   
       
  }

  private function join_cond(){
    $this->db->join('Receive_products rp','rp.id = qch.receive_product_id');
    $this->db->join('corporate_products cp','cp.id = qch.corporate_product_id');
    $this->db->join('email_orders eo','eo.order_id = cp.order_id');
    $this->db->join('category_master cm','cm.name = rp.category_name','left');
    $this->db->join('corporate corp','corp.id = eo.corporate');
    $this->db->join('quality_control qc','qc.id = qch.qc_id','left');
    $this->db->join('hallmarking_center hc','qch.hc_id = hc.id','left');
  }
  private function all_like_queries($search){
    $this->db->where("(rp.Product_code LIKE '%$search%' OR cp.order_id LIKE '%$search%' OR corp.name LIKE '%$search%' OR DATE_FORMAT( qch.created_at,'%d-%m-%Y') LIKE '%$search%')");
  }
  public function insert_in_prepared_order($result,$karigar_id){
    $insert_array = array(
        'corporate_products_id'=>$result['corporate_product_id'],
        'quantity'=>$result['quantity'],
        'karigar_id'=>$karigar_id,
        'hm_rejected_id'=>$result['id'],
        'status'=>'0',
        'created_at'=>date('Y-m-d H:i:sa'),
        'updated_at'=>date('Y-m-d H:i:sa'),
      );
    if($this->db->insert('Prepare_product_list',$insert_array)){
      return true;
    }else{
      return false;
    }
  }
  public function update($array,$where){
    $this->db->where($where);
    $this->db->update($this->table_name,$array);
  }
  public function find($id){
    $this->db->where('id',$id);
    return $this->db->get($this->table_name)->row_array();
  }
  function accepted_products($selcted_products=''){

    $this->db->select('p.product_code Product_Code ,cp.work_order_id as Order_No,qc.cp_size Size,qc.quantity Quantity,qc.total_net_wt Actual_Net_Wt,cp.line_number as line_number , qc.id');
    $this->db->from($this->table_name.' qc');
    $this->db->join('corporate_products cp','cp.id = qc.corporate_product_id');
    $this->db->join('product p','p.product_code = cp.sort_Item_number','left');
    $this->db->where('qc.status','8');
    if (!empty($selcted_products)) {
     $this->db->where_in('qc.qc_id',$selcted_products);

    }

    return $this->db->get()->result_array();
  }
  public function export_hallmarking_products($filter_status='',$status='',$params='',$search='',$limit='',$hallmarking ='',$selcted_products=''){
    $this->db->select('cm.name as Karatage,cp.work_order_id as Order_No, @s:=@s+1 as Link_No,cp.Item_number as Design_No,qc.quantity as PCS,qc.total_gr_wt as Gross_Wt,"" Stone_Wt,qc.total_net_wt as Net_Wt,"" Purity,"" Number,DATE_FORMAT( qc.HM_receive_date ,  "%d-%m-%Y" ) as Date , qc.id', false);
    $this->db->from($this->table_name.' qc, (SELECT @s:= 0) AS s');
    $this->db->join('Receive_products rp','rp.id = qc.receive_product_id');
    $this->db->join('corporate_products cp','cp.id = qc.corporate_product_id');
    $this->db->join('quality_control qlt','qlt.id=qc.qc_id','left');
    $this->db->join('product p','p.product_code = cp.sort_Item_number','left');
    $this->db->join('carat_master cm','cm.id = p.carat_id','left');
    $this->db->join('email_orders eo','eo.order_id = cp.order_id');
    $this->db->join('corporate corp','corp.id = eo.corporate');
    /*selected data export*/
    $this->where_cond();
    if (!empty($selcted_products)) {
     $this->db->where_in('qc.id',$selcted_products);
    }
    /**/
    if(!empty($_POST['search'])){
      $search = $_POST['search'];
    }
    $this->all_like_queries($search);
    $this->db->where('qc.status','1');

    if(!empty($_POST['product_code'])){
      $this->export_where_conditions();
    }
      
    $result = $this->db->get()->result_array();
     if (!empty($selcted_products)) {
      $this->db->set('status','8');
      $this->db->where_in('id',$selcted_products);
      $this->db->update($this->table_name);
    }
    //echo $this->db->last_query(); die();
    return $result;

  }

  function set_exported_qch($selcted_products){
    $this->db->set('status','8');
    $this->db->where_in('qc_id',$selcted_products);
    if ($this->db->update($this->table_name)) {
      return get_successMsg();
    }else{
      return get_errorMsg();
    }
  }

  public function find_hm_detaile($id){
    $this->db->select('qch.*,eo.order_id,cp.line_number,cp.size_master as size');
    $this->db->from($this->table_name.' qch');
    $this->db->join('email_orders eo','eo.id = qch.email_order_id');
    $this->db->join('corporate_products cp','cp.id = qch.corporate_product_id');
    $this->db->where('qch.qc_id',$id);
    return $this->db->get($this->table_name)->row_array();
  }
  
} 