<?php

class Amend_products_model extends CI_Model {

  function __construct() {
  	$this->table_name = "Amend_products";
    parent::__construct();
  }
  
  public function get($filter_status='',$status='',$params='',$search='',$limit='',$karigar_id=''){
    $this->db->select('ap.*,corp.name as corporate,cp.order_id as order_id,qc.created_at,qc.quantity,qc.cp_size size,qc.total_gr_wt,qc.total_net_wt,rp.category_name,rp.product_code,k.name,k.id as karigar_id');
    $this->db->from($this->table_name.' ap');
    $this->all_join_queries();
    $this->db->where('ap.status','1');
   
    if(!empty($filter_status))
       $this->db->order_by($status[$filter_status['column']],$filter_status['dir']);
 /*   if(!empty($search)){
        $this->all_like_queries($search);
    }*/ 
    $ci = &get_instance();
    $get_url = $ci->uri->segment(2);
 
    if(@$get_url =='show'){
       $table_col_name='AmendView';
    }else{
       $table_col_name='AmendTbl';
    }
    if(isset($params['columns']) && !empty($params['columns'])){
      $filter_input=$params['columns'];
      $this->get_filter_value($filter_input,$table_col_name);
    }
    if(!empty($karigar_id)){
      $this->db->where('k.id',$karigar_id);
    }else{
    $this->db->group_by('k.id');
    }
 
    if($limit == true){
     $this->db->limit($params['length'],$params['start']);
     $this->db->order_by('ap.id','DESC');
      $result = $this->db->get()->result_array();
    }else{
        $row_array = $this->db->get()->result_array();
        $result = count($row_array);
        //print_r($row_array);die;
    }

    //echo $this->db->last_query(); die();
    return $result;
  }
  private function get_filter_value($filter_input,$table_col_name){
    $column_name=array();  
    $filter_column_name=filter_column_name($table_col_name);
    $sql='';
    $i=0;
     
     //print_r($filter_input);die;
    foreach ($filter_input as $key => $search_value){
       $column_name=$filter_column_name;
       //print_r($search_value['search']['value']);
        if(!empty($search_value['search']['value'])){
          if($i != 0){
            $sql.=' AND  ';
          }
            $sql.=''.$column_name[$key].' like "%'.$search_value['search']['value'].'%" ';
            $i++;
        

         }   
    }

    if(!empty($sql)){  
      $this->db->where($sql);  
    } 
   
       
  }


  private function all_join_queries(){
    $this->db->join('quality_control qc','qc.id = ap.qc_id','left');
    $this->db->join('corporate_products cp','cp.id = ap.corporate_product_id','left');
    $this->db->join('Receive_products rp','rp.id = qc.receive_product_id');
    $this->db->join('email_orders eo','eo.order_id = cp.order_id');
    $this->db->join('corporate corp','corp.id = eo.corporate');
    $this->db->join('karigar_master k','k.id = rp.karigar_id');
  }

  private function all_like_queries($search){
    $this->db->where("(`cp`.`sort_Item_number` LIKE '%$search%' OR cp.order_id LIKE '%$search%'  OR DATE_FORMAT( qc.created_at,'%d-%m-%Y') LIKE '%$search%')");
  }
   public function mark_as_amend($receive_product_id){
    $insert_array['created_at'] = date('Y-m-d H:i:s'); 
    $insert_array['receive_product_id'] = $receive_product_id;
    $this->db->select('corporate_product_id');
      $this->db->from('Receive_products');
      $this->db->where('id',$receive_product_id);
      $co_result = $this->db->get()->row_array();
    $insert_array['corporate_product_id'] = $co_result['corporate_product_id'];
    if($this->db->insert($this->table_name,$insert_array))
    {
      $this->db->set('status','7');
      $this->db->where('receive_product_id',$receive_product_id);
      if($this->db->update('quality_control')){
        $this->db->set('status','6');
        $this->db->where('id',$receive_product_id);
        $this->db->update('Receive_products');
        return get_successMsg();
      }else{
        return get_errorMsg();
      }
    }else{
        return get_errorMsg();
      }

  }
  public function store($array){
    $this->db->insert($this->table_name,$array);
    return true;
  }
  public function find($id){
    $this->db->select('*');
    $this->db->from($this->table_name);
    $this->db->where('id',$id);
    return $this->db->get()->row_array();
  }
  public function insert_in_prepared_order($result,$karigar_id){
    $email_order_id = email_order_id($result['corporate_product_id']);
    $insert_array = array(
        'corporate_products_id'=>$result['corporate_product_id'],
        'email_order_id'=>$email_order_id,
        'quantity'=>$result['quantity'],
        'karigar_id'=>$karigar_id,
        'amend_id'=>$result['id'],
        'status'=>'0',
        'created_at'=>date('Y-m-d H:i:sa'),
        'updated_at'=>date('Y-m-d H:i:sa'),
        'karigar_engaged_date' => date('Y-m-d'),
        'karigar_delivery_date' => date("Y-m-d" , strtotime('+12 DAY')),
      );
    if($this->db->insert('Prepare_product_list',$insert_array)){
      return true;
    }else{
      return false;
    }
  }
  public function update($array,$where){
    $this->db->where($where);
    $this->db->update($this->table_name,$array);
  }

  public function insert_in_stock($result,$data){
   $email_order_id = email_order_id($result['corporate_product_id']);
   $insert_array = array(
        'corporate_product_id'=>$result['corporate_product_id'],
        'email_order_id'=>$email_order_id,
        'amend_id'=>$result['id'],
        'qc_id'=>$result['qc_id'],
        'quantity'=>$result['quantity'],
        'karigar_id'=>$data['karigar_id'],
        'corporate'=>$data['corporate'],
        'receive_product_id'=>$data['receive_product_id'],
        'status'=>'1',
        'created_at'=>date('Y-m-d H:i:sa'),
        'updated_at'=>date('Y-m-d H:i:sa'),
      );
    if($this->db->insert('stock',$insert_array)){
      return true;
    }else{
      return false;
    }
  }



  function get_karigar_id_from_qc_id($qc_id){
    $this->db->select('rp.karigar_id,qc.receive_product_id,eo.corporate');
    $this->db->where('qc.id',$qc_id);
    $this->db->from('quality_control qc');
    $this->db->join('Receive_products rp','qc.receive_product_id=rp.id','left');
    $this->db->join('corporate_products cp','rp.corporate_product_id=cp.id','left');
    $this->db->join('email_orders eo','cp.order_id=eo.order_id');
    $result = $this->db->get()->row_array();
    return $result;
  }

   public function export_amend_products($filter_status='',$status='',$params='',$search='',$limit=''){
      $this->db->select('cp.sort_Item_number as Product_Code,cp.order_id as Order_Id,rp.category_name as Category,DATE_FORMAT(qc.created_at,"%d-%m-%Y")as Date ,qc.quantity as Quantity,qc.cp_size as Size,if(ap.status = 1 ,"A",IF(ap.status = 2,"Stock","Sended To Karigar")) as Status,ap.id');
    $this->db->from($this->table_name.' ap');
    $this->all_join_queries();
   // print_r($_POST['receive_product_id']);
       $this->db->where('ap.status','1');
    if(!empty($_POST['receive_product_id'])){
         $this->db->where_in('ap.id',$_POST['receive_product_id']);
    }
    $result = $this->db->get()->result_array();
    return $result;
  }

}