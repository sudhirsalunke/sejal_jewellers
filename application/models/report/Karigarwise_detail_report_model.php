<?php
class Karigarwise_detail_report_model extends CI_Model {

  function __construct() {
      $this->table_name = "manufacturing_order";
      $this->table_mop="manufacturing_order_mapping";
      $this->table_kmm="Karigar_manufacturing_mapping";
      parent::__construct();
  }

  public function get(){
   
    $this->db->select('kmm.karigar_id,kmm.id as kmm_id, kmm.created_at, kmm.delivery_date, pm.name, mo.id, kmm.quantity, kmm.approx_weight, w.from_weight, w.to_weight, km.name k_name,(kmm.quantity - kmm.receive_quantity) as bal_qty,mom.parent_category_id,pm.name as parent_category_name'); 
    $this->db->from($this->table_kmm.' kmm');
    $this->db->join($this->table_mop.' mom','mom.id=kmm.mop_id');
    $this->db->join($this->table_name.' mo','mo.id=mom.manufacturing_order_id');
    $this->db->join('parent_category pm','pm.id = mom.parent_category_id');
    $this->db->join('weights w','w.id = mom.weight_range_id');
    $this->db->join('karigar_master km','km.id = kmm.karigar_id');
    $this->db->where('kmm.status','1');
    $result = $this->db->get()->result_array();
  $response =array();
  $karigar_names=array();
  $parent_category_ids=array();
  foreach ($result as $key => $value) {
    $parent_category_id =$value['parent_category_id'];
    if (!isset($response[$value['karigar_id']][$parent_category_id]['bal_qty'])) {
      $response[$value['karigar_id']][$parent_category_id]['bal_qty']=0;
      $response[$value['karigar_id']][$parent_category_id]['bal_weight']=0.00;
    }


      $response[$value['karigar_id']][$parent_category_id]['bal_qty'] +=$value['bal_qty'];
      $response[$value['karigar_id']][$parent_category_id]['bal_weight'] += ($value['approx_weight']/$value['quantity'])*$value['bal_qty'];
      /*karigar name array*/
      $karigar_names[$value['karigar_id']]=$value['k_name'];
      /*parent category Name*/
      $parent_category_ids[$parent_category_id]=$value['parent_category_name'];
    }
    return  array('result' => $response,'karigar_names' => $karigar_names,'parent_category_ids' => $parent_category_ids);
  }

} //class 