<?php
class Category_order_ageing_report_model extends CI_Model {

  function __construct() {
      $this->table_name = "manufacturing_order";
      $this->table_mop="manufacturing_order_mapping";
      $this->table_kmm="Karigar_manufacturing_mapping";
      parent::__construct();
  }

  public function get(){
   // $this->db->select('weight gross_wt,rp.quantity,po.manufacturing_order_id,tp.weight_range_id,pc.id as parent_category_id,pc.name as parent_category');
    $today=date('Y-m-d');
    $this->db->select('kmm.delivery_date,mo.order_name,(kmm.quantity - kmm.receive_quantity) as bal_qty,DATEDIFF("'.$today.'",kmm.delivery_date) as due_days,mo.id as order_id,pc.name as parent_category,kmm.quantity,w.id as weight_range_id,concat(w.from_weight,"-",w.to_weight) as weight_name');
    $this->db->from($this->table_kmm.' kmm');
    $this->db->join($this->table_mop.' mop','kmm.mop_id = mop.id');
    $this->db->join($this->table_name.' mo','mop.manufacturing_order_id=mo.id');
    $this->db->join('parent_category pc','mop.parent_category_id=pc.id');
    $this->db->join('weights w','mop.weight_range_id=w.id');
    $this->db->where('kmm.status !=','2');
    $this->db->where('DATEDIFF("'.$today.'",kmm.delivery_date) >','0');
    $this->db->group_by('kmm.id');
    $result = $this->db->get()->result_array();
    $response=array();
    $order_names_array=array();
    foreach ($result as $key => $value) {
      $response[$value['weight_range_id']][]=$value;
      $order_names_array[$value['weight_range_id']]=$value['order_name'];
    }
    return array('result'=>$response,'order_names_array'=>$order_names_array);
  }

  

} //class 