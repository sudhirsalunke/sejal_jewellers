<?php
class Itemwise_stock_report_model extends CI_Model {

  function __construct() {
      $this->table_name = "department_ready_product";
      $this->sale_stock="sale_stock";
      $this->quality_control="quality_control";
      parent::__construct();
  }

  public function get(){
   		$this->db->select('km.name as karigar_name,drp.gr_wt,drp.net_wt,date_format(drp.created_at,"%d-%m-%y") as rec_date,drp.product_code');
   		$this->db->from($this->sale_stock.' ss');
   		$this->db->join($this->table_name.' drp','ss.drp_id=drp.id');
   		$this->db->join($this->quality_control.' qc','drp.qc_id=qc.id');
   		$this->db->join('Receive_products rp','qc.receive_product_id=rp.id');
      $this->db->join('prepare_kundan_karigar_order ppl','ppl.id = rp.kmm_id');
      $this->db->join('karigar_master km','km.id = ppl.karigar_id');
   		return $this->db->get()->result_array();
  
	}

} //class 