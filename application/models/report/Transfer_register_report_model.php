<?php
class Transfer_register_report_model extends CI_Model {

  function __construct() {
      $this->table_name = "department_ready_product";
      $this->sale_stock="sale_stock";
      $this->quality_control="quality_control";
      parent::__construct();
  }

  public function get(){
   		$this->db->select('drp.net_wt,date_format(drp.created_at,"%d-%m-%y") as rec_date,drp.product_code,drp.chitti_no,drp.quantity');
   		$this->db->from($this->sale_stock.' ss');
   		$this->db->join($this->table_name.' drp','ss.drp_id=drp.id');
   		$result = $this->db->get()->result_array();
   		$response =array();
   		foreach ($result as $key => $value) {
   			$chitti_no =$value['chitti_no'];
   			$response[$chitti_no][]=$value;
   		}
  		return $response;
	}

} //class 