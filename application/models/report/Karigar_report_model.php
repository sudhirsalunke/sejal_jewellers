<?php
class Karigar_report_model extends CI_Model {

  function __construct() {
      $this->table_name = "Karigar_manufacturing_mapping";
      parent::__construct();
  }

  public function get($filter_status='',$params='',$search='',$limit=''){
    $this->db->select('km.name as karigar_name,SUM(kmm.approx_weight) as order_weight,SUM(kmm.quantity) as total_pcs');
    $this->db->group_by('kmm.karigar_id');
    $this->db->from($this->table_name.' kmm');
    $this->db->join('karigar_master km','kmm.karigar_id = km.id','left');
  /*  if(!empty($search)){
        $this->db->like('km.name',$search);
    }*/
   if(isset($params['columns']) && !empty($params['columns'])){
      $filter_input=$params['columns'];
      $table_col_name="karigar_report_table";
      $this->get_filter_value($filter_input,$table_col_name);
    }

   if($limit == true){
      $this->db->limit($params['length'],$params['start']);
      $result = $this->db->get()->result_array();
    }else{
      $row_array = $this->db->get()->result_array();
      $result = count($row_array);
      //echo $this->db->last_query();print_r($result);exit;
    }
   // echo $this->db->last_query(); print_r($result);exit;
    return $result;
  }

    private function get_filter_value($filter_input,$table_col_name){
    $column_name=array();  
    $filter_column_name=filter_column_name($table_col_name);
    $sql='';
    $i=0;
     
     //print_r($filter_input);die;
    foreach ($filter_input as $key => $search_value){
       $column_name=$filter_column_name;
       //print_r($search_value['search']['value']);
        if(!empty($search_value['search']['value'])){
          if($i != 0){
            $sql.=' AND  ';
          }
            $sql.=''.$column_name[$key].' like "%'.$search_value['search']['value'].'%" ';
            $i++;
        

         }   
    }

    if(!empty($sql)){  
      $this->db->where($sql);  
    } 
   
       
  }


  
} //class 