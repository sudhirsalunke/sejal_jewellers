<?php
class Order_summary_report_model extends CI_Model {

  function __construct() {
      $this->table_name = "manufacturing_order";
      $this->table_mop="manufacturing_order_mapping";
      $this->table_kmm="Karigar_manufacturing_mapping";
      parent::__construct();
  }

  public function get(){
    $this->db->select('drp.weight gross_wt,rp.quantity,po.manufacturing_order_id,tp.weight_range_id,pc.id as parent_category_id,pc.name as parent_category');
    $this->db->from('department_ready_product drp');
    $this->db->join('quality_control qc','drp.qc_id = qc.id');
    $this->db->join('Receive_products rp','rp.id = qc.receive_product_id');
    $this->db->join('prepare_kundan_karigar_order po','po.id = rp.kmm_id');
    $this->db->join('make_set_quantity_relation msqr','msqr.id = po.msqr_id');
    $this->db->join('tag_products tp','tp.id = msqr.tag_id');
    $this->db->join('parent_category pc','tp.sub_category_id=pc.id');
    $this->db->where('drp.status',1);
    $result = $this->db->get()->result_array();
    $response=array();
      if(!empty($result)){
        foreach ($result as $key => $value) {
          $parent_category_id =$value['parent_category_id'];
          $weight_range_id =$value['weight_range_id'];

         $minimum_stock = $this->get_minimum_stock($parent_category_id,$weight_range_id);
          if (!isset($response[$weight_range_id][$parent_category_id]['gross_wt'])) {
             $response[$weight_range_id][$value['parent_category_id']]['gross_wt']=0.00;
          }
           if (!isset($response[$weight_range_id][$parent_category_id]['qty'])) {
             $response[$weight_range_id][$parent_category_id]['qty']=0;
          }

         $response[$weight_range_id][$parent_category_id]['gross_wt'] += $value['gross_wt'];
         $response[$weight_range_id][$parent_category_id]['qty'] += $value['quantity'];
         $response[$weight_range_id][$parent_category_id]['diff']=$minimum_stock-$response[$weight_range_id][$parent_category_id]['qty'];
      }
      return $response;
    }else{
      return "";
    }
  }

    function get_minimum_stock($parent_category_id,$weight_range_id){
      $this->db->select('ms.stock');
      $this->db->where('pcc.parent_category_id',$parent_category_id);
      $this->db->where('ms.weight_range_id',$weight_range_id);
      $this->db->from('parent_category_codes pcc');
      $this->db->join('minimum_stock ms','pcc.id=ms.pc_code_id','left');
      $result = $this->db->get()->row_array();
      return @$result['stock'];
  }

  function get_parent_category(){
   return $this->db->get('parent_category')->result_array();
  }

  function get_weights(){
    return $this->db->get('weights')->result_array();
  }

  
} //class 