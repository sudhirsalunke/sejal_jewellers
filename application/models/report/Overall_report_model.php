<?php
class Overall_report_model extends CI_Model {

  function __construct() {
      $this->corporate = "corporate";
      $this->corporate_products="corporate_products";
      parent::__construct();
  }

    public function get_all_corporates(){
      return $this->db->get('corporate')->result_array();
    }

  public function get(){
    $data=array();

    /*define*/
    $data['Total_QC_Goods']['quantity']=$data['Total_QC_Goods']['gross_weight']=0;
    $data['Total_QC_Fail_Goods']['quantity']=$data['Total_QC_Fail_Goods']['gross_weight']=0;
    $data['Total_Amended_Products']['quantity']=$data['Total_Amended_Products']['gross_weight']=0;
    $data['Total_Goods_In_Hallmarking']['quantity']=$data['Total_Goods_In_Hallmarking']['gross_weight']=0;

    $this->db->select('SUM(quantity) as quantity,SUM(total_gr_wt) as gross_weight,status');
    $this->db->from('quality_control qc');
    $this->db->where('module','1');
   // $this->db->where_in('status',array('0','1','6','2','3','7'));
    $this->db->where_in('status',array('0','1','6','2'));
    $this->db->group_by('status'); 
    $result = $this->db->get()->result_array();
   
   $status_array=array(0=>'Total_QC_Fail_Goods',1=>'Total_QC_Goods',6=>'Total_Amended_Products',2=>'Total_Goods_In_Hallmarking');
   // echo $this->db->last_query(); print_r($result);exit;
   if (isset($result[0]['status'])) {
     $data[$status_array[$result[0]['status']]]['quantity']=$result[0]['quantity'];
     $data[$status_array[$result[0]['status']]]['gross_weight']=$result[0]['gross_weight'];
   }
   if (isset($result[1]['status'])) {
      $data[$status_array[$result[1]['status']]]['quantity']=$result[1]['quantity'];
      $data[$status_array[$result[1]['status']]]['gross_weight']=$result[1]['gross_weight'];
   }
   if (isset($result[2]['status'])) {
     $data[$status_array[$result[2]['status']]]['quantity']=$result[2]['quantity'];
     $data[$status_array[$result[2]['status']]]['gross_weight']=$result[2]['gross_weight'];
   }

   if (isset($result[3]['status'])) {
     $data[$status_array[$result[3]['status']]]['quantity']=$result[3]['quantity'];
     $data[$status_array[$result[3]['status']]]['gross_weight']=$result[3]['gross_weight'];
   }
  
  
   

   
   
   

   $this->db->select('rp.quantity,cp.weight_band,cp.net_weight');
   $this->db->from('Receive_products rp');
   $this->db->join('corporate_products cp','rp.corporate_product_id=cp.id','left');
   $this->db->where('rp.status','1');
   $this->db->where('rp.module','1');
  $goods_in_qc = $this->db->get()->result_array();
  $quantity_qc=$weight_qc=0;
  foreach ($goods_in_qc as $key => $value) {
    if (!empty($value['weight_band'])) {
      $median =get_mid_value(@$value['weight_band']);
      $quantity_qc +=$value['quantity'];
      $weight_qc +=$median;
    }else{
      $quantity_qc +=$value['quantity'];
      $weight_qc +=$value['net_weight'];
    }
    
  }
  $data['Total_Goods_In_QC']['quantity']= $quantity_qc;
  $data['Total_Goods_In_QC']['gross_weight']=$weight_qc;

   $this->db->select('rp.quantity,cp.weight_band,cp.net_weight');
   $this->db->from('Receive_products rp');
   $this->db->join('corporate_products cp','rp.corporate_product_id=cp.id','left');
   $this->db->where(array('rp.module'=>'1','rp.date'=>date('Y-m-d')));
   $rec_goods = $this->db->get()->result_array();
    $quantity_rec=$weight_rec=0;
  foreach ($rec_goods as $rg_key => $rg_value) {
    if (!empty($rg_value['weight_band'])) {
      $median =get_mid_value(@$rg_value['weight_band']);
      $quantity_rec +=$rg_value['quantity'];
      $weight_rec +=$median;
    }else{
      $quantity_rec +=$rg_value['quantity'];
      $weight_rec +=$rg_value['net_weight'];
    }
  }
  $data['Total_Received_Goods_Today']['quantity']= $quantity_rec;
  $data['Total_Received_Goods_Today']['gross_weight']=$weight_rec;


  $this->db->select('SUM(quantity) as total_qty,SUM(total_gr_wt) as gross_weight');
  $this->db->from('quality_control_hallmarking');
  $this->db->where(array('status'=>'9','module'=>'1','HM_send_date'=>date('Y-m-d')));
  $today_pr_out = $this->db->get()->row_array();

  $data['Total_OUT_Goods_Today']['quantity']= (@$today_pr_out['total_qty']) ? @$today_pr_out['total_qty'] : '0';
  $data['Total_OUT_Goods_Today']['gross_weight']=(@$today_pr_out['gross_weight']) ? @$today_pr_out['gross_weight'] : '0';

  // $this->db->select('SUM(gross_weight) as gross_weight,count(id) as total_orders');
  // $this->db->from('email_orders');
  // $order_placed = $this->db->get()->row_array();

  // $data['Total_Placed_Order']['quantity']= @$order_placed['total_orders'].' Order';
  // $data['Total_Placed_Order']['gross_weight']= @$order_placed['gross_weight'];

  // $this->db->select('count(order_id) as orders,');
  // $this->db->from('corporate_products');
  // $this->db->where('status','0');
  // $this->db->group_by('order_id');
  // $order_pending = $this->db->get()->result_array();

  // $data['Total_Pending_Order']['quantity']= count($order_pending).' Order';
  // $data['Total_Pending_Order']['gross_weight']= "0";
  
  // $this->db->from('corporate_products');
  // $this->db->where('status !=','0');
  // $this->db->group_by('order_id');
  // $order_received = $this->db->get()->result_array();

  // $data['Total_Received_Order']['quantity']= count($order_received).' Order';
  // $data['Total_Received_Order']['gross_weight']= "0";

  $this->db->select('order_id,weight_band,net_weight,status');
  $tot_orders = $this->db->get_where('corporate_products')->result_array();

  $incomplete_orders=array();
  $total_orders=array();
  $pending_weight=0;
  $completed_weight=0;
      foreach ($tot_orders as $tot_k => $tot_v) { 

          if (!in_array($tot_v['order_id'], $total_orders)) { /*total orders*/
                 $total_orders[]=$tot_v['order_id'];
               }

           if (!empty($tot_v['weight_band'])){ /*for reliance product*/
                    $gross_weight =get_mid_value(@$tot_v['weight_band']);
                }else{ /*for caratlane o others*/
                    $gross_weight =$tot_v['net_weight'];
              }

              if ($tot_v['status']==0) { /*pending orders*/
               $pending_weight +=$gross_weight;
                 if (!in_array($tot_v['order_id'], $incomplete_orders)) { 
                   $incomplete_orders[]=$tot_v['order_id'];
                 }
              }else{ /*completed orders*/
                $completed_weight +=$gross_weight;
              }

      }
    $order_received = count($total_orders)-count($incomplete_orders);
    $data['Total_Placed_Order']['quantity']= count($total_orders).' Order';
    $data['Total_Placed_Order']['gross_weight']= $pending_weight+$completed_weight;
    $data['Total_Pending_Order']['quantity']= count($incomplete_orders).' Order';
    $data['Total_Pending_Order']['gross_weight']= $pending_weight;
    $data['Total_Received_Order']['quantity']= $order_received.' Order';
    $data['Total_Received_Order']['gross_weight']= $completed_weight;

    
            
   return $data;

   
  }


  public function detailed_report($report_name,$filter_status="",$params="",$search="",$limit=""){
    $func_name="get_".$report_name;

    return $this->$func_name($filter_status,$params,$search,$limit);
  }
 
  function get_total_qc_goods($filter_status="",$params="",$search="",$limit=""){
    $this->db->select('qc.quantity,qc.total_gr_wt as gross_weight,c.name as corporate_name,cp.sort_Item_number');
    $this->db->from('quality_control qc');
    $this->db->join('corporate_products cp','qc.corporate_product_id=cp.id');
    $this->db->join('email_orders eo','cp.order_id=eo.order_id');
    $this->db->join('corporate c','eo.corporate=c.id');
    $this->db->where(array('qc.module'=>'1','qc.status'=>'1'));
    //$this->db->where_in('status',array('0','1','6','2'));
    if(!empty($search)){
        $this->db->like('cp.sort_Item_number',$search);
    }
    if($limit == true){
      $this->db->limit($params['length'],$params['start']);
    } 
    $result = $this->db->get()->result_array();
    // print_r($search);print_r($result);exit();
    return $result;
    //print_r($result);exit;
 }

 function get_total_qc_fail_goods($filter_status="",$params="",$search="",$limit=""){
    $this->db->select('qc.quantity,qc.total_gr_wt as gross_weight,c.name as corporate_name,cp.sort_Item_number');
    $this->db->from('quality_control qc');
    $this->db->join('corporate_products cp','qc.corporate_product_id=cp.id');
    $this->db->join('email_orders eo','cp.order_id=eo.order_id');
    $this->db->join('corporate c','eo.corporate=c.id');
    $this->db->where(array('qc.module'=>'1','qc.status'=>'0'));
    if(!empty($search)){
        $this->db->like('cp.sort_Item_number',$search);
    }
    if($limit == true){
      $this->db->limit($params['length'],$params['start']);
    }
    //$this->db->where_in('status',array('0','1','6','2')); 
    return $result = $this->db->get()->result_array();
    //print_r($result);exit;
 }

 function get_total_amended_products($filter_status="",$params="",$search="",$limit=""){
    $this->db->select('qc.quantity,qc.total_gr_wt as gross_weight,c.name as corporate_name,cp.sort_Item_number');
    $this->db->from('quality_control qc');
    $this->db->join('corporate_products cp','qc.corporate_product_id=cp.id');
    $this->db->join('email_orders eo','cp.order_id=eo.order_id');
    $this->db->join('corporate c','eo.corporate=c.id');
    $this->db->where(array('qc.module'=>'1','qc.status'=>'6'));
    if(!empty($search)){
        $this->db->like('cp.sort_Item_number',$search);
    }
    if($limit == true){
      $this->db->limit($params['length'],$params['start']);
    }
    //$this->db->where_in('status',array('0','1','6','2')); 
    $result = $this->db->get()->result_array();
    return $result;
 }

 function get_total_goods_in_hallmarking($filter_status="",$params="",$search="",$limit=""){
    $this->db->select('qc.quantity,qc.total_gr_wt as gross_weight,c.name as corporate_name,cp.sort_Item_number,hc.name as hc_name');
    $this->db->from('quality_control qc');
    $this->db->join('corporate_products cp','qc.corporate_product_id=cp.id');
    $this->db->join('hallmarking_center hc','qc.hc_id=hc.id');
    $this->db->join('email_orders eo','cp.order_id=eo.order_id');
    $this->db->join('corporate c','eo.corporate=c.id');
    $this->db->where(array('qc.module'=>'1','qc.status'=>'2'));
    if(!empty($search)){
        $this->db->like('cp.sort_Item_number',$search);
    }
    if($limit == true){
      $this->db->limit($params['length'],$params['start']);
    }
    //$this->db->where_in('status',array('0','1','6','2')); 
    $result = $this->db->get()->result_array();
    return $result;
 }

  function get_total_goods_in_qc($filter_status="",$params="",$search="",$limit=""){
    $this->db->select('rp.quantity,cp.weight_band,cp.net_weight,c.name as corporate_name,cp.sort_Item_number');
    $this->db->from('Receive_products rp');
    $this->db->join('corporate_products cp','rp.corporate_product_id=cp.id');
    $this->db->join('email_orders eo','cp.order_id=eo.order_id');
    $this->db->join('corporate c','eo.corporate=c.id');
    $this->db->where(array('rp.module'=>'1','rp.status'=>'1'));
   // $this->db->where('rp.status','1');
    if(!empty($search)){
        $this->db->like('cp.sort_Item_number',$search);
    }
    if($limit == true){
      $this->db->limit($params['length'],$params['start']);
    }
    //$this->db->where_in('status',array('0','1','6','2')); 
    $result = $this->db->get()->result_array();
    return $result;
 }
 
  function get_total_received_goods_today($filter_status="",$params="",$search="",$limit=""){
    $this->db->select('rp.quantity,cp.weight_band,cp.net_weight,c.name as corporate_name,cp.sort_Item_number');
    $this->db->from('Receive_products rp');
    $this->db->join('corporate_products cp','rp.corporate_product_id=cp.id');
    $this->db->join('email_orders eo','cp.order_id=eo.order_id');
    $this->db->join('corporate c','eo.corporate=c.id');
    $this->db->where(array('rp.module'=>'1','rp.date'=>date('Y-m-d')));
   // $this->db->where('rp.status','1');
    if(!empty($search)){
        $this->db->like('cp.sort_Item_number',$search);
    }
    if($limit == true){
      $this->db->limit($params['length'],$params['start']);
    }
    //$this->db->where_in('status',array('0','1','6','2')); 
    $result = $this->db->get()->result_array();
    return $result;
 }

  function get_total_out_goods_today($filter_status="",$params="",$search="",$limit=""){
    $this->db->select('qch.quantity,qch.total_gr_wt as gross_weight,c.name as corporate_name,cp.sort_Item_number');
     $this->db->from('quality_control_hallmarking qch');
    $this->db->join('corporate_products cp','qch.corporate_product_id=cp.id');
    $this->db->join('email_orders eo','cp.order_id=eo.order_id');
    $this->db->join('corporate c','eo.corporate=c.id');
    $this->db->where(array('qch.status'=>'9','qch.module'=>'1','qch.HM_send_date'=>date('Y-m-d')));   
    // $this->db->where('rp.status','1');
    if(!empty($search)){
        $this->db->like('cp.sort_Item_number',$search);
    }
    if($limit == true){
      $this->db->limit($params['length'],$params['start']);
    }
    //$this->db->where_in('status',array('0','1','6','2')); 
    $result = $this->db->get()->result_array();
    return $result;
 }
  function get_total_placed_order($filter_status="",$params="",$search="",$limit=""){
    $this->db->select('group_concat(cp.weight_band) as weight_band,group_concat(cp.net_weight) as net_weight,c.name as corporate_name,eo.order_id as sort_Item_number,count(cp.id) as quantity');
    $this->db->from('corporate_products cp');
    $this->db->join('email_orders eo','cp.order_id=eo.order_id');
    $this->db->join('corporate c','eo.corporate=c.id');
    $this->db->group_by('cp.order_id');
    if(!empty($search)){
        $this->db->like('cp.order_id',$search);
    }
    if($limit == true){
      $this->db->limit($params['length'],$params['start']);
    }
    //$this->db->where_in('status',array('0','1','6','2')); 
    $result = $this->db->get()->result_array();
   // print_r($result);exit;
    return $result;
 }

 function get_total_pending_order($filter_status="",$params="",$search="",$limit=""){
    $this->db->select('group_concat(cp.weight_band) as weight_band,group_concat(cp.net_weight) as net_weight,c.name as corporate_name,eo.order_id as sort_Item_number,count(cp.id) as quantity');
    $this->db->from('corporate_products cp');
    $this->db->join('email_orders eo','cp.order_id=eo.order_id');
    $this->db->join('corporate c','eo.corporate=c.id');
    $this->db->group_by('cp.order_id');
    $this->db->where('cp.status','0');
    if(!empty($search)){
        $this->db->like('cp.order_id',$search);
    }
    if($limit == true){
      $this->db->limit($params['length'],$params['start']);
    }
    //$this->db->where_in('status',array('0','1','6','2')); 
    $result = $this->db->get()->result_array();
   // print_r($result);exit;
    return $result;
 }

  function get_total_received_order($filter_status="",$params="",$search="",$limit=""){
    $this->db->select('group_concat(cp.weight_band) as weight_band,group_concat(cp.net_weight) as net_weight,c.name as corporate_name,eo.order_id as sort_Item_number,count(cp.id) as quantity,(case when (group_concat(cp.status)) like "%0%" then "0" else "1" end) as completed');
    $this->db->from('corporate_products cp');
    $this->db->join('email_orders eo','cp.order_id=eo.order_id');
    $this->db->join('corporate c','eo.corporate=c.id');
    $this->db->group_by('cp.order_id');
    $this->db->having('completed','1');
    if(!empty($search)){
        $this->db->like('cp.order_id',$search);
    }
    if($limit == true){
      $this->db->limit($params['length'],$params['start']);
    }
    //$this->db->where_in('status',array('0','1','6','2')); 
    $result = $this->db->get()->result_array();
   // echo $this->db->last_query(); print_r($result);exit;
    return $result;
 }

 


} //class 