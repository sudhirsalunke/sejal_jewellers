<?php
class Daily_stock_summary_report_model extends CI_Model {

  function __construct() {
      $this->table_name = "manufacturing_order";
      $this->table_mop="manufacturing_order_mapping";
      $this->table_kmm="Karigar_manufacturing_mapping";
      parent::__construct();
  }

  public function get($from_date='',$to_date='',$department_id='',$status=''){
    $this->db->select('SUM(approx_weight) as order_weight,SUM(quantity) as total_pcs,SUM(total_weight) as total_weight');
    $this->db->from('Karigar_manufacturing_mapping');
    $this->db->where('DATE(created_at)>=',$from_date);
    $this->db->where('DATE(created_at)<=',$to_date);

    if(!empty($department_id)){
      $this->db->where('department_id',$department_id);
    }
    if($status != ''){   
      $this->db->where('status',$status);
    }
  
    $result = $this->db->get()->row_array();
    return $result;
  }


  public function get_previous_stock_k_return($today='',$department_id='',$status=''){
    $this->db->select('SUM(approx_weight) as order_weight,SUM(quantity) as total_pcs,SUM(total_weight) as total_weight');
    $this->db->from('Karigar_manufacturing_mapping');   
     $this->db->where('DATE(created_at)<',$today);

    if(!empty($department_id)){
      $this->db->where('department_id',$department_id);
    }
    if($status != ''){ 
      $this->db->where('status',$status);
    }
  
    $result = $this->db->get()->row_array();
    return $result;
  }

    public function get_previous_stock($today='',$department_id='',$status=''){
    $this->db->select('SUM(gr_wt) as order_weight,SUM(quantity) as total_pcs');
    $this->db->from('department_ready_product');   
     $this->db->where('DATE(created_at)<',$today);

    if(!empty($department_id)){
      $this->db->where('department_id',$department_id);
    }
    if($status != ''){ 
      $this->db->where('status',$status);
    }
  
    $result = $this->db->get()->row_array();
    
    return $result;
  }

  public function get_qc_data($from_date='',$to_date='',$department_id='',$status=''){
    $this->db->select('SUM(CASE WHEN rp.gross_wt is NULL THEN qc.gross_wt  ELSE  rp.gross_wt END) as order_weight,qc.net_wt,(select SUM(quantity) from Receive_products where status="'.$status.'" and is_skip_qc="0" and DATE(rp.date)>="'.$from_date.'" and DATE(rp.date)<="'.$to_date.'") as total_pcs,sum(qc.quantity) as qc_quantity');
    $this->db->from('Receive_products rp');
    $this->db->join('quality_control qc','qc.receive_product_id=rp.id','left');
    $this->db->where('rp.module','2');
    $this->db->where('rp.is_skip_qc','0');
    $this->db->where('DATE(rp.date)>=',$from_date);
    $this->db->where('DATE(rp.date)<=',$to_date);
    if(!empty($department_id)){
      $this->db->where('rp.department_id',$department_id);
    }
    if($status != ''){ 
      $this->db->where('rp.status',$status);
    }
  
    $result = $this->db->get()->row_array();
    return $result;
  }
  public function get_previous_qc_data($today='',$department_id='',$status=''){
  $this->db->select('(CASE WHEN rp.gross_wt is NULL THEN qc.gross_wt  ELSE  rp.gross_wt END) as order_weight,qc.net_wt,(select SUM(quantity) from Receive_products where receipt_code = rp.receipt_code and status="'.$status.'" and is_skip_qc="0") as total_pcs,sum(qc.quantity) as qc_quantity');
    $this->db->from('Receive_products rp');
    $this->db->join('quality_control qc','qc.receive_product_id=rp.id','left');
    $this->db->where('rp.module','2');
    $this->db->where('rp.is_skip_qc','0');
    $this->db->where('DATE(rp.date)<',$today);
    if(!empty($department_id)){
      $this->db->where('rp.department_id',$department_id);
    }
    if($status != ''){ 
      $this->db->where('rp.status',$status);
    }
    
    $result = $this->db->get()->row_array();
    return $result;
  }
    




  public function get_repair_data($from_date='',$to_date='',$department_id=''){
    $this->db->select('SUM(gr_wt_o) as order_weight,SUM(quantity) as total_pcs');
    $this->db->from('repair_product');
    $this->db->where('DATE(created_at)>=',$from_date);
    $this->db->where('DATE(created_at)<=',$to_date);
    if(!empty($department_id)){
      $this->db->where('department_id',$department_id);
    }
    $this->db->where('status','0');
  

    $result = $this->db->get()->row_array();
    return $result;
  }

  public function get_previous_repair_data($today='',$department_id=''){
    $this->db->select('SUM(gr_wt_o) as order_weight,SUM(quantity) as total_pcs');
    $this->db->from('repair_product');   
     $this->db->where('DATE(created_at)<',$today);
    if(!empty($department_id)){
      $this->db->where('department_id',$department_id);
    }
      $this->db->where('status','0');
      $result = $this->db->get()->row_array();
    return $result;
  }
  public function get_hm_data($from_date='',$to_date='',$department_id='',$status=''){
    $this->db->select('SUM(net_wt) as order_weight,SUM(quantity) as total_pcs');
    $this->db->from('quality_control_hallmarking');
    $this->db->where('DATE(created_at)>=',$from_date);
    $this->db->where('DATE(created_at)<=',$to_date);
    if(!empty($department_id)){
      $this->db->where('department_id',$department_id);
    }
    if($status != ''){ 
      $this->db->where('status',$status);
    }
    
    $result = $this->db->get()->row_array();
    return $result;
  }

  public function get_previous_hm_data($today='',$department_id='',$status=''){
    $this->db->select('SUM(net_wt) as order_weight,SUM(quantity) as total_pcs');
    $this->db->from('quality_control_hallmarking');   
     $this->db->where('DATE(created_at)<',$today);
    if(!empty($department_id)){
      $this->db->where('department_id',$department_id);
    }
    if($status != ''){ 
      $this->db->where('status',$status);
    }
    
    $result = $this->db->get()->row_array();
    return $result;
  }

  public function get_tagging_data($from_date='',$to_date='',$department_id='',$status=''){
    $this->db->select('SUM(net_wt) as order_weight,SUM(quantity) as total_pcs');
    $this->db->from('tag_products');
    $this->db->where('DATE(created_at)>=',$from_date);
    $this->db->where('DATE(created_at)<=',$to_date);
    $this->db->where('(status =0 or status=1)');
    if(!empty($department_id)){
      $this->db->where('department_id',$department_id);
    }
    if($status != ''){ 
    }
    
    $result = $this->db->get()->row_array();
    return $result;
  }

    public function get_kundan_data($from_date='',$to_date='',$department_id='',$status=''){
    $this->db->select('SUM(CASE WHEN qc.gross_wt is NULL THEN qc.weight  ELSE  qc.gross_wt END) as order_weight,SUM(po.quantity-po.receive_qnt) as total_pcs');
    $this->db->from('prepare_kundan_karigar_order po');
    $this->db->join('make_set_quantity_relation msqr','msqr.id = po.msqr_id');
    $this->db->join('tag_products tp','msqr.tag_id = tp.id');
    $this->db->join('quality_control qc','qc.id = tp.qc_id');
    $this->db->where('DATE(po.created_at)>=',$from_date);
    $this->db->where('DATE(po.created_at)<=',$to_date);
    if(!empty($department_id)){
      $this->db->where('po.department_id',$department_id);
    }
    if($status != ''){ 
      $this->db->where('po.status',$status);
    }
    
    $result = $this->db->get()->row_array();
    return $result;
  }



  public function  get_previous_tagging_data($today='',$department_id='',$status=''){
    $this->db->select('SUM(gr_wt) as order_weight,SUM(quantity) as total_pcs');
    $this->db->from('tag_products');   
     $this->db->where('DATE(created_at)<',$today);
     $this->db->where('(status =0 or status=1)');
    if(!empty($department_id)){
      $this->db->where('department_id',$department_id);
    }
/*    if($status != ''){ 
      $this->db->where('status',$status);
    }
    */
    $result = $this->db->get()->row_array();
    return $result;
  }

  public function get_previous_kundan_data($today='',$department_id='',$status=''){
    $this->db->select('SUM(gr_wt) as order_weight,SUM(quantity) as total_pcs');
    $this->db->from('prepare_kundan_karigar_order');   
     $this->db->where('DATE(created_at)<',$today);
    if(!empty($department_id)){
      $this->db->where('department_id',$department_id);
    }
    if($status != ''){ 
      $this->db->where('status',$status);
    }
    
    $result = $this->db->get()->row_array();
    return $result;
  }

  public function get_kundan_qc_data($from_date='',$to_date='',$department_id=''){
    $this->db->select('SUM(gross_wt) as order_weight,SUM(quantity) as total_pcs');    
    $this->db->from('Receive_products');  
    $this->db->where('DATE(date)>=',$from_date);
    $this->db->where('DATE(date)<=',$to_date);
    $this->db->where('status','0');
    $this->db->where('module','3');
    if(!empty($department_id)){
      $this->db->where('department_id',$department_id);      
    }  

    $result = $this->db->get()->row_array();
    return $result;
  }

    public function get_kundan_qc_detailing_data($from_date='',$to_date='',$department_id=''){
    $this->db->select('SUM(gross_wt) as order_weight,SUM(quantity) as total_pcs');    
    $this->db->from('quality_control');  
    $this->db->where('DATE(created_at)>=',$from_date);
    $this->db->where('DATE(created_at)<=',$to_date);
    $this->db->where('module','3');
    $this->db->where('status','11');
    if(!empty($department_id)){
      $this->db->where('department_id',$department_id);      
    }  
 

    $result = $this->db->get()->row_array();
    return $result;
  }


    public function get_kundan_qc_previous_detailing_data($today='',$department_id=''){
    $this->db->select('SUM(gross_wt) as order_weight,SUM(quantity) as total_pcs');    
    $this->db->from('quality_control');  
    $this->db->where('DATE(created_at)<',$today);
    $this->db->where('module','3');
    $this->db->where('status','11');
    if(!empty($department_id)){
      $this->db->where('department_id',$department_id);      
    }  
 

    $result = $this->db->get()->row_array();
    return $result;
  }




public function get_previous_kundan_qc_data($today='',$department_id=''){
    $this->db->select('SUM(gross_wt) as order_weight,SUM(quantity) as total_pcs');    
    $this->db->from('Receive_products');
    $this->db->where('DATE(date)<',$today);
    $this->db->where('status','0');
    $this->db->where('module','3');
    if(!empty($department_id)){
      $this->db->where('department_id',$department_id);      
    }  

    $result = $this->db->get()->row_array();
    return $result;
  }

   public function get_send_kundan_qc_data($from_date='',$to_date='',$department_id=''){
    $this->db->select('SUM(gross_wt) as order_weight,SUM(quantity) as total_pcs');    
    $this->db->from('Receive_products');  
    $this->db->where('DATE(date)>=',$from_date);
    $this->db->where('DATE(date)<=',$to_date);
    $this->db->where('status',"8");
    $this->db->where('module','3');
    if(!empty($department_id)){
      $this->db->where('department_id',$department_id);      
    }  

    $result = $this->db->get()->row_array();
    return $result;
  }


public function get_previous_send_kundan_qc_data($today='',$department_id=''){
    $this->db->select('SUM(gross_wt) as order_weight,SUM(quantity) as total_pcs');    
    $this->db->from('Receive_products');
    $this->db->where('DATE(date)<',$today);
    $this->db->where('status',"8");
    $this->db->where('module','3');
    if(!empty($department_id)){
      $this->db->where('department_id',$department_id);      
    }  

    $result = $this->db->get()->row_array();
    return $result;
  }


  public function get_ready_product_data($from_date='',$to_date='',$department_id='',$status=''){
    $this->db->select('SUM(gr_wt) as order_weight,SUM(quantity) as total_pcs');    
    $this->db->from('department_ready_product');  
    $this->db->where('DATE(created_at)>=',$from_date);
    $this->db->where('DATE(created_at)<=',$to_date);
    if(!empty($department_id)){
      $this->db->where('department_id',$department_id);      
    }
     if($status != ''){ 
      $this->db->where('status',$status);
    }
    

    $result = $this->db->get()->row_array();
    return $result;
  }
  public function get_previous_ready_product_data($today='',$department_id='',$status=''){
    $this->db->select('SUM(gr_wt) as order_weight,SUM(quantity) as total_pcs');    
    $this->db->from('department_ready_product');  
   
     $this->db->where('DATE(created_at)<',$today);
    if(!empty($department_id)){
      $this->db->where('department_id',$department_id);      
    }
     if($status != ''){ 
      $this->db->where('status',$status);
    }
    

    $result = $this->db->get()->row_array();
    return $result;
  }


  public function get_issue_to_sale_data($from_date='',$to_date='',$department_id='',$status=''){
    $this->db->select('SUM(gr_wt) as order_weight,SUM(quantity) as total_pcs');    
    $this->db->from('department_ready_product');  
    $this->db->where('DATE(sales_date)>=',$from_date);
    $this->db->where('DATE(sales_date)<=',$to_date);
    if(!empty($department_id)){
      $this->db->where('department_id',$department_id);      
    }
     if($status != ''){ 
      $this->db->where('status',$status);
    }
    

    $result = $this->db->get()->row_array();
    return $result;
  }
  public function get_previous_issue_to_sale_data($today='',$department_id='',$status=''){
    $this->db->select('SUM(gr_wt) as order_weight,SUM(quantity) as total_pcs');    
    $this->db->from('department_ready_product');  
   
     $this->db->where('DATE(sales_date)<',$today);
    if(!empty($department_id)){
      $this->db->where('department_id',$department_id);      
    }
     if($status != ''){ 
      $this->db->where('status',$status);
    }
    

    $result = $this->db->get()->row_array();
    return $result;
  }

    public function get_rejected_data($from_date='',$to_date='',$department_id='',$status=''){
    $this->db->select('SUM(gr_wt) as order_weight,SUM(quantity) as total_pcs');    
    $this->db->from('department_ready_product');  
    $this->db->where('DATE(rejected_date)>=',$from_date);
    $this->db->where('DATE(rejected_date)<=',$to_date);
    if(!empty($department_id)){
      $this->db->where('department_id',$department_id);      
    }
     if($status != ''){ 
      $this->db->where('status',$status);
    }
    

    $result = $this->db->get()->row_array();
    return $result;
  }
  public function get_previous_rejected_data($today='',$department_id='',$status=''){
    $this->db->select('SUM(gr_wt) as order_weight,SUM(quantity) as total_pcs');    
    $this->db->from('department_ready_product');  
   
     $this->db->where('DATE(rejected_date)<',$today);
    if(!empty($department_id)){
      $this->db->where('department_id',$department_id);      
    }
     if($status != ''){ 
      $this->db->where('status',$status);
    }
    

    $result = $this->db->get()->row_array();
    return $result;
  }



public function get_pending_tagging_data($from_date='',$to_date='',$department_id=''){
    $this->db->select('(CASE WHEN gross_wt is NULL THEN weight  ELSE  gross_wt END)as order_weight,SUM(quantity) as total_pcs');    
    $this->db->from('quality_control');  
    $this->db->where('DATE(created_at)>=',$from_date);
    $this->db->where('DATE(created_at)<=',$to_date);
    $this->db->where('status','8');
    $this->db->where('module','2');
    if(!empty($department_id)){
      $this->db->where('department_id',$department_id);      
    }  
 $result = $this->db->get()->row_array();
    return $result;
   
  }




public function get_previous_pending_tagging_data($today='',$department_id=''){
    $this->db->select('SUM(gross_wt) as order_weight,SUM(quantity) as total_pcs');    
    $this->db->from('quality_control');
    $this->db->where('DATE(created_at)<',$today);
    $this->db->where('status','8');
    $this->db->where('module','2');
    if(!empty($department_id)){
      $this->db->where('department_id',$department_id);      
    }  
     $result = $this->db->get()->row_array();
    return $result;
  
  }



  
} //class 