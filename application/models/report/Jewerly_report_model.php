<?php
class Jewerly_report_model extends CI_Model {

  function __construct() {
      $this->corporate = "corporate";
      $this->corporate_products="corporate_products";
      parent::__construct();
  }

  public function get($filter_status='',$params='',$search='',$limit=''){
    // $this->db->select('SUM(qc.total_gr_wt) as total_gr_weight,SUM(qc.total_net_wt) as total_net_weight,corp.name as corporate,corp.id as corporate_id');
    // $this->db->where('qc.module',1);
    // $this->db->group_by('corp.id');
    // $this->db->from('quality_control qc');
    // $this->db->join('corporate_products cp','cp.id = qc.corporate_product_id','left');
    // $this->db->join('email_orders eo','eo.order_id = cp.order_id','left');
    // $this->db->join('corporate corp','corp.id = eo.corporate','left');

    // if(!empty($search)){
    //     $this->db->like('corp.name',$search);
    // }

    // if($limit == true)
    // 	$this->db->limit($params['length'],$params['start']);
    // $result = $this->db->get()->result_array();
    // echo $this->db->last_query(); print_r($result);exit;
    // return $result;
    $this->db->select('corp.*,corp.id as corporate_id');
    $this->db->from('corporate corp');
    
    if(isset($params['columns']) && !empty($params['columns'])){
      $filter_input=$params['columns'];
      $table_col_name="jewerly_report_table";
      $this->get_filter_value($filter_input,$table_col_name);
    }


    /*if(!empty($search)){
        $this->db->like('corp.name',$search);
    }*/

    if($limit == true)
     $this->db->limit($params['length'],$params['start']);
    $result = $this->db->get()->result_array();
    // echo $this->db->last_query(); print_r($result);exit;
    return $result;
  }
  

  public function get_qc_data($corporate_id){
    $this->db->select('SUM(qc.total_gr_wt) as total_gr_weight,SUM(qc.total_net_wt) as total_net_weight');
    $this->db->where('qc.module',1);
    $this->db->group_by('eo.corporate');
    $this->db->from('quality_control qc');
    $this->db->join('corporate_products cp','cp.id = qc.corporate_product_id','left');
    $this->db->join('email_orders eo','eo.order_id = cp.order_id','left');
    $this->db->where('eo.corporate',$corporate_id);

    $result = $this->db->get()->row_array();
    return $result;
  }

  public function get_order_reciept($corporate_id){
  	
  	if ($corporate_id ==2) {
  		$this->db->select('SUM(cp.net_weight) as total_net_wt');
  	}else{
  		$this->db->select('cp.weight_band');
  	}
  	
    $this->db->from('corporate_products cp');
    $this->db->join('email_orders eo','eo.order_id = cp.order_id');
    $this->db->join('corporate corp','corp.id = eo.corporate');
    $this->db->where('corp.id',$corporate_id);
    
	if ($corporate_id ==2) {
	  		$result = $this->db->get()->row_array();
	  		return @$result['total_net_wt'];
	  		
	  	}else{
	  		$result = $this->db->get()->result_array();
	  		$wt=0;
	  		foreach ($result as $r_key => $r_value) {
	  			$mid_value = get_mid_value($r_value['weight_band']);
	  			if ($mid_value != "error") {
	  				$wt +=$mid_value;
	  			}
	  		}
	  		return $wt;
	  	}
  }

   public function get_recived_weight($corporate_id){
		if ($corporate_id ==2) {
	  		$this->db->select('SUM(cp.net_weight) as total_net_wt');
	  	}else{
	  		$this->db->select('cp.weight_band');
	  	}
        
      $this->db->from('Prepare_product_list ppl');
      $this->db->join('corporate_products cp','cp.id = ppl.corporate_products_id','left');
	    $this->db->join('email_orders eo','eo.order_id = cp.order_id');
	    $this->db->join('corporate corp','corp.id = eo.corporate');
	    $this->db->where('corp.id',$corporate_id);

       if ($corporate_id ==2) {
	  		$result = $this->db->get()->row_array();
	  		return @$result['total_net_wt'];
	  		
	  	}else{
	  		$result = $this->db->get()->result_array();
	  		$wt_rec=0;
	  		foreach ($result as $r_key => $r_value) {
	  			$mid_value = get_mid_value($r_value['weight_band']);
	  			if ($mid_value != "error") {
	  				$wt_rec +=$mid_value;
	  			}
	  		}
	  		return $wt_rec;
	  	}
  }

  public function get_metal_recived($corporate_id){
   // $this->db->select('SUM(ar.recieved_weight) as total');
   // $this->db->from('accounting_recieved ar');
   // $this->db->where('ar.type',2);/*customer*/
   // $this->db->where('c.corporate_id',$corporate_id);
   // $this->db->join('customer c','c.id=ar.karigar_id','left');
   // $result = $this->db->get()->row_array();
   return 0;//$result['total'];


  }

  public function get_qc_data_view($corporate_id,$filter_status='',$params='',$search='',$limit=''){
    $this->db->select('qc.total_gr_wt as gross_weight,qc.total_net_wt as net_weight,cp.sort_Item_number as product_code,(case when qc.status=1 then "Quality Control" else "Hallmarking" end) as from_txt');
    $this->db->where(array('qc.module'=>1,'eo.corporate'=>$corporate_id));
    $this->db->from('quality_control qc');
    $this->db->join('corporate_products cp','cp.id = qc.corporate_product_id','left');
    $this->db->join('email_orders eo','eo.order_id = cp.order_id','left');

   /* if(!empty($search)){
        $this->db->like('cp.sort_Item_number',$search);
    }*/
   if(isset($params['columns']) && !empty($params['columns'])){
      $filter_input=$params['columns'];
      $table_col_name="jewerly_report_table_detail";
      $this->get_filter_value($filter_input,$table_col_name);
    }
    if($limit == true)
     $this->db->limit($params['length'],$params['start']);

    $result = $this->db->get()->result_array();
    return $result;
  }

  public function get_order_reciept_view($corporate_id,$filter_status='',$params='',$search='',$limit=''){
      //$this->db->select('cp.sort_Item_number as product_code,cp.weight_band as gross_weight ,cp.net_weight as net_weight');
      $this->db->select('cp.order_id as product_code,group_concat(cp.weight_band) as gross_weight ,SUM(cp.net_weight) as net_weight'); 
      $this->db->from('corporate_products cp');
      $this->db->join('email_orders eo','eo.order_id = cp.order_id');
      $this->db->join('corporate corp','corp.id = eo.corporate');
      $this->db->where('corp.id',$corporate_id);

    /*  if(!empty($search)){
        $this->db->like('cp.order_id',$search);
      }*/
        if(isset($params['columns']) && !empty($params['columns'])){
      $filter_input=$params['columns'];
      $table_col_name="jewerly_report_table_detail_order_receipts";
      $this->get_filter_value($filter_input,$table_col_name);
    }

      if($limit == true){
        $this->db->limit($params['length'],$params['start']);
      }

      $this->db->group_by('cp.order_id');
      $result = $this->db->get()->result_array();
      foreach ($result as $r_key => $r_value) {
        if ($corporate_id==1) {
          $gr_wt=0.00;
          foreach (explode(',', $r_value['gross_weight']) as  $value) {
            $gr_wt +=get_mid_value($value);
          }
          $result[$r_key]['gross_weight'] = $gr_wt;
          $result[$r_key]['net_weight']="-";
        }else{
          $result[$r_key]['gross_weight'] = "-";
        }
      }  
    return $result;   
  }

  public function get_recived_weight_view($corporate_id,$filter_status='',$params='',$search='',$limit=''){
        
        $this->db->select('cp.order_id as product_code,group_concat(cp.weight_band) as gross_weight ,SUM(cp.net_weight) as net_weight,date_format(eo.created_at,"%d-%m-%Y '."||".' %T") as order_date,SUM(ppl.quantity) as received_order,(select SUM(CW_quantity) from corporate_products where order_id=cp.order_id) as total_qty');
        $this->db->from('Prepare_product_list ppl');
        $this->db->join('corporate_products cp','cp.id = ppl.corporate_products_id','left');
        $this->db->join('email_orders eo','eo.order_id = cp.order_id');
        $this->db->join('corporate corp','corp.id = eo.corporate');
        $this->db->where('corp.id',$corporate_id);
        if(!empty($search)){
        $this->db->like('cp.order_id',$search);
         }
      if(isset($params['columns']) && !empty($params['columns'])){
        $filter_input=$params['columns'];
        $table_col_name="jewerly_report_table_detail_recived_weight";
        $this->get_filter_value($filter_input,$table_col_name);
      }


        if($limit == true)
         $this->db->limit($params['length'],$params['start']);
        
        $this->db->group_by('cp.order_id');       
        $result = $this->db->get()->result_array();

       foreach ($result as $r_key => $r_value) {
        if ($corporate_id==1) {
           $gr_wt=0.00;
          foreach (explode(',', $r_value['gross_weight']) as  $value) {
            $gr_wt +=get_mid_value($value);
          }
          $result[$r_key]['gross_weight'] = $gr_wt;
          $result[$r_key]['net_weight']="-";
        }else{
          $result[$r_key]['gross_weight'] = "-";
        }
      }  
        return $result;
  }

  function get_order_pending_view($corporate_id,$filter_status='',$params='',$search='',$limit=''){
    $this->db->select('cp.order_id as product_code,group_concat(cp.weight_band) as gross_weight ,SUM(cp.net_weight) as net_weight,date_format(eo.created_at,"%d-%m-%Y '."||".' %T") as order_date,SUM(cp.CW_quantity) as received_order,(select SUM(CW_quantity) from corporate_products where order_id=cp.order_id) as total_qty');
        $this->db->from('corporate_products cp');
        $this->db->join('email_orders eo','eo.order_id = cp.order_id');
        $this->db->join('corporate corp','corp.id = eo.corporate');
        $this->db->where(array('corp.id'=>$corporate_id,'cp.status'=>'0'));
       /* if(!empty($search)){
        $this->db->like('cp.order_id',$search);
         }
*/
      if(isset($params['columns']) && !empty($params['columns'])){
        $filter_input=$params['columns'];
        $table_col_name="jewerly_report_table_detail_rorder_pending";
        $this->get_filter_value($filter_input,$table_col_name);
      }

        if($limit == true)
         $this->db->limit($params['length'],$params['start']);
        
        $this->db->group_by('cp.order_id');       
        $result = $this->db->get()->result_array();

        foreach ($result as $r_key => $r_value) {
        if ($corporate_id==1) {
           $gr_wt=0.00;
          foreach (explode(',', $r_value['gross_weight']) as  $value) {
            $gr_wt +=get_mid_value($value);
          }
          $result[$r_key]['gross_weight'] = $gr_wt;
          $result[$r_key]['net_weight']="-";
        }else{
          $result[$r_key]['gross_weight'] = "-";
        }
      }  
        return $result;
  }
  private function get_filter_value($filter_input,$table_col_name){
    $column_name=array();  
    $filter_column_name=filter_column_name($table_col_name);
    $sql='';
    $i=0;
     
     //print_r($filter_input);die;
    foreach ($filter_input as $key => $search_value){
       $column_name=$filter_column_name;
       //print_r($search_value['search']['value']);
        if(!empty($search_value['search']['value'])){
          if($i != 0){
            $sql.=' AND  ';
          }
            $sql.=''.$column_name[$key].' like "%'.$search_value['search']['value'].'%" ';
            $i++;
        

         }   
    }

    if(!empty($sql)){  
      $this->db->where($sql);  
    } 
       
  }
  
} //class 