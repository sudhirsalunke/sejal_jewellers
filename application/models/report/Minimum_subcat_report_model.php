<?php
class Minimum_subcat_report_model extends CI_Model {

  function __construct() {
      $this->table_name = "manufacturing_order";
      $this->table_mop="manufacturing_order_mapping";
      $this->table_kmm="Karigar_manufacturing_mapping";
      parent::__construct();
  }

  public function get(){
    $this->db->select('drp.weight gross_wt,rp.quantity,po.manufacturing_order_id,tp.weight_range_id,pc.id as parent_category_id,pc.name as parent_category');
    $this->db->from('department_ready_product drp');
    $this->db->join('quality_control qc','drp.qc_id = qc.id');
    $this->db->join('Receive_products rp','rp.id = qc.receive_product_id');
    $this->db->join('prepare_kundan_karigar_order po','po.id = rp.kmm_id');
    $this->db->join('make_set_quantity_relation msqr','msqr.id = po.msqr_id');
    $this->db->join('tag_products tp','tp.id = msqr.tag_id');
    $this->db->join('parent_category pc','tp.sub_category_id=pc.id','left');
    $this->db->where('drp.status',0);
    // $this->db->group_by('po.manufacturing_order_id');
    //$this->db->group_by('tp.weight_range_id');
    $result = $this->db->get()->result_array();
    $response=array();
    foreach ($result as $key => $value) {
        $parent_category_id =$value['parent_category_id'];
        $weight_range_id =$value['weight_range_id'];

        $parent_category_codes = $this->get_parent_category_codes($parent_category_id);
        foreach ($parent_category_codes as $pcc_key => $pcc_value) {
          $pc_code_id = $pcc_value['id'];
          // $minimum_stock = $this->get_minimum_stock($pcc_value['id'],$weight_range_id);

            if (!isset($response[$weight_range_id][$parent_category_id][$pc_code_id]['gross_wt'])) {
             $response[$weight_range_id][$parent_category_id][$pc_code_id]['gross_wt']=0.00;
          }
           if (!isset($response[$weight_range_id][$parent_category_id][$pc_code_id]['qty'])) {
             $response[$weight_range_id][$parent_category_id][$pc_code_id]['qty']=0;
          }

         $response[$weight_range_id][$parent_category_id][$pc_code_id]['gross_wt'] += $value['gross_wt'];
         $response[$weight_range_id][$parent_category_id][$pc_code_id]['qty'] += $value['quantity'];
         //$response[$weight_range_id][$parent_category_id][$pc_code_id]['diff']=$minimum_stock-$response[$weight_range_id][$parent_category_id][$pc_code_id]['qty'];
        }   
    }
    return $response;
    // echo '<pre>';
    // print_r($response);
    // print_r($result);
    // echo '</pre>';die;
  }

  //   function get_minimum_stock($pc_code_id,$weight_range_id){
  //     $this->db->select('ms.stock');
  //     $this->db->where('ms.pc_code_id',$pc_code_id);
  //     $this->db->where('ms.weight_range_id',$weight_range_id);
  //     $this->db->from('minimum_stock ms');
  //     $result = $this->db->get()->row_array();
  //     return @$result['stock'];
  // }

  function get_parent_category(){
   $result = $this->db->get('parent_category')->result_array();
   foreach ($result as $key => $value) {
     $result[$key]['codes']=$this->get_parent_category_codes($value['id']);
   }
   return $result;
  }

    function get_parent_category_codes($parent_category_id){
   return $this->db->get_where('parent_category_codes',array('parent_category_id'=>$parent_category_id))->result_array();
  }

   function get_weights(){
   return $this->db->get('weights')->result_array();
  }

  
} //class 