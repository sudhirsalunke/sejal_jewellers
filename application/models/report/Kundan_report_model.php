<?php
class Kundan_report_model extends CI_Model {

  function __construct() {
      $this->table_name = "prepare_kundan_karigar_order";
      $this->karigar_master="karigar_master";
      parent::__construct();
  }

  public function get(){
   		$this->db->select('pkk.karigar_id,km.name as karigar_name,SUM(drp.gr_wt) as weight,SUM(pkk.quantity) as total_qty,pkk.issue_voucher chitti_no,date_format(pkk.created_at,"%d-%m-%Y") as issue_date');
   		$this->db->from($this->table_name.' pkk');
   		$this->db->join($this->karigar_master.' km','pkk.karigar_id=km.id');
   		$this->db->join('department_ready_product drp','pkk.msqr_id=drp.msqr_id','left');
   		$this->db->where('pkk.status !=','2');
      $this->db->group_by('pkk.msqr_id');
   		$result=  $this->db->get()->result_array();
      return $result;
  
	}

} //class 