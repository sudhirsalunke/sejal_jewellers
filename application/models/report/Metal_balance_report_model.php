<?php
class Metal_balance_report_model extends CI_Model {

  function __construct() {
      $this->table_name = "accounting";
      $this->karigar_master="karigar_master";
      $this->accounting_recieved="accounting_recieved";
      parent::__construct();
  }

  public function get(){
   		$this->db->select('a.karigar_customer_id,km.name,SUM(a.weight) as tot_weight,SUM(ac.recieved_weight) as rec_weight');
   		$this->db->from($this->table_name.' a');
   		$this->db->join($this->karigar_master.' km','a.karigar_customer_id=km.id');
   		$this->db->join($this->accounting_recieved.' ac','ac.karigar_id=a.karigar_customer_id AND ac.type=1','left');
   		$this->db->where('a.type',1);
   		$this->db->group_by('a.karigar_customer_id');
   		$result=  $this->db->get()->result_array();
      return $result;
  
	}

} //class 