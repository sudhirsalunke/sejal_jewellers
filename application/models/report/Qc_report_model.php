<?php
class Qc_report_model extends CI_Model {

  function __construct() {
      $this->table_name = "quality_control";
      parent::__construct();
  }

  public function get(){
   
    $this->db->select('rp.karigar_id,km.name,qc.status,qc.quantity'); 
    $this->db->from($this->table_name.' qc');
    $this->db->join('Receive_products rp','qc.receive_product_id=rp.id');
    $this->db->join('karigar_master km','km.id = rp.karigar_id');
    //$this->db->where('km.status','1');
    $this->db->where('qc.module','2');
    $this->db->where("(qc.status='8' OR qc.status='0' OR qc.status='11')");
    $result = $this->db->get()->result_array();
    
    $response =array();
    $karigar_names=array();
    
    foreach ($result as $key => $value) {
      $karigar_id = $value['karigar_id'];
        if (!isset($response[$karigar_id])) {
          $response[$karigar_id]['accept']=0;
          $response[$karigar_id]['reject']=0;
          $response[$karigar_id]['total_qty']=0;
        }

        if ($value['status']=='8' || $value['status']=='11') {
          $response[$karigar_id]['accept'] += $value['quantity'];
        }else if ($value['status']=='0') {
          $response[$karigar_id]['reject'] += $value['quantity'];
        }
        $response[$karigar_id]['total_qty'] += $value['quantity'];
        if (!empty($response[$karigar_id]['accept'])) {
           $response[$karigar_id]['percentages'] = round(($response[$karigar_id]['accept']*100)/$response[$karigar_id]['total_qty'],2).'%';
        }else{
           $response[$karigar_id]['percentages']="-";
        }
        $response[$karigar_id]['karigar_name']=$value['name'];
      }
    return  array('result' => $response);
  }

} //class 