<?php
class Order_tracking_report_model extends CI_Model {

  function __construct() {
     parent::__construct();
  }

//   public function get($filter_status='',$params='',$search='',$limit='',$type=''){
   
//     $this->db->select('co.id as id,co.status as order_status, order_name, co.order_date, quantity, delievery_date, expected_date, party_name,co.company_name as company_name,km.name as karigar_name,co.karigar_id');  
//     $this->db->from('customer_orders co');
//     $this->db->join('karigar_master km','co.karigar_id=km.id');
//     $this->db->where('co.status','2'); 
    
//     if(isset($params['columns']) && !empty($params['columns'])){
//       $filter_input=$params['columns'];
//       $table_col_name="Karigar_order_tracking_tbl";
//       $this->get_filter_value($filter_input,$table_col_name);
//     }


// /*    if($limit == true){
//        $this->db->limit($params['length'],$params['start']);
//     }
//         $result = $this->db->get()->result_array();*/

//           if($limit == true){
//      $this->db->limit($params['length'],$params['start']);
//      $this->db->order_by('co.id','DESC');
//       $result = $this->db->get()->result_array();
//     }else{
//         $row_array = $this->db->get()->result_array();
//         $result = count($row_array);
//         //print_r($row_array);die;
//     }
//      //echo $this->db->last_query(); print_r($result);exit;
//     return $result;
//   }
  
  public function get($filter_status='',$params='',$search='',$limit='',$type=''){   
    if($type == 4){
       $this->db->select('sum(net_weight) as total_weight');
    }else{
    $this->db->select('co.id,party_name,customer_id,name as karigar_name,karigar_id,weight_range_id as weight_range_id,co.parent_category_id'); 
    if($type != 3){
       $this->db->select('sum(co.weight_range_id) as weight_range');
    } 
   } 
    $this->db->from('customer_orders co');
    $this->db->join('karigar_master km','co.karigar_id=km.id');    
    if(isset($params['columns']) && !empty($params['columns'])){
      $filter_input=$params['columns'];
      if($type == '1'){
       $table_col_name="Karigar_order_tracking_tbl";
      }elseif($type=='2'){
       $table_col_name="customer_order_tracking_tbl";
      }elseif($type=='3'){
        $this->db->where('status','3'); 
        $table_col_name="in_stock_report_tbl";
      }
      $this->get_filter_value($filter_input,$table_col_name);
    }   
     if($type == '1'){
           $this->db->group_by('co.karigar_id');
         }elseif($type=='2'){
            $this->db->group_by('co.customer_id');
         }
     if($limit == true){
      if($type != 4 ){
        $this->db->limit($params['length'],$params['start']);
      }
     $this->db->order_by('co.id','DESC');
      $result = $this->db->get()->result_array();
    }else{
        $row_array = $this->db->get()->result_array();
        $result = count($row_array);
    }
   //echo $this->db->last_query(); print_r($result);exit;
    return $result;
  }


  public function report_summary_departmentWise($filter_status='',$params='',$search='',$limit='',$date){  
    $this->db->select('co.department_id,dp.name');
    $this->db->from('customer_orders co');
    // $this->db->join('karigar_master km','co.karigar_id=km.id');    
    $this->db->join('departments dp','co.department_id=dp.id','left');    
    //$this->db->where('DATE(receive_date)',$date);
      $table_col_name="report_summary_departmentWise_tbl";
    if(isset($params['columns']) && !empty($params['columns'])){
      $filter_input=$params['columns'];
      
      $this->get_filter_value($filter_input,$table_col_name);
    } 
     $this->db->group_by('co.department_id');

      if($limit == true){
     $this->db->limit($params['length'],$params['start']);
     $this->db->order_by('co.id','DESC');
      $result = $this->db->get()->result_array();
    }else{
        $row_array = $this->db->get()->result_array();
        $result = count($row_array);
        //print_r($row_array);die;
    }
    //echo $this->db->last_query(); print_r($result);exit;
    return $result;
  }

  public function get_daily_order_report($filter_status='',$params='',$search='',$limit='',$date='',$type){  
    $this->db->select('co.id,co.party_name,co.weight_range_id,co.customer_id,km.name as karigar_name,co.karigar_id,dp.name as department_name,co.parent_category_id,co.status as order_status,co.quantity,cor.created_at,co.delievery_date');
    $this->db->from('customer_orders co');
    $this->db->join('karigar_master km','co.karigar_id=km.id');    
    $this->db->join('departments dp','co.department_id=dp.id','left');    
    $this->db->join('customer_order_received cor','co.id=cor.order_id','left');    
    
    if($type =='karagir_order'){
      $this->db->where('DATE(receive_date)',$date);
      //$this->db->where('status','3');
      $table_col_name="daily_order_received_from_karigar_tbl";
    }else{
      $this->db->where('order_date',$date);
      $table_col_name="daily_order_received_tbl";
    }
    if(isset($params['columns']) && !empty($params['columns'])){
      $filter_input=$params['columns'];
      
      $this->get_filter_value($filter_input,$table_col_name);
    }    if($limit == true){
     $this->db->limit($params['length'],$params['start']);
     $this->db->order_by('co.id','DESC');
      $result = $this->db->get()->result_array();
    }else{
        $row_array = $this->db->get()->result_array();
        $result = count($row_array);
        //print_r($row_array);die;
    }
 //echo $this->db->last_query(); print_r($result);exit;
    return $result;
  }


  public function customer_orders_by_id($filter_status='',$params='',$search='',$limit='',$customer_id=''){ 
    $this->db->select('co.id,co.order_date,co.expected_date,co.quantity,co.status,km.name');  
    $this->db->from('customer_orders co');
    $this->db->join('karigar_master km','co.karigar_id=km.id');
    if(isset($params['columns']) && !empty($params['columns'])){
      $filter_input=$params['columns'];
      $table_col_name="individual_customer_order_tracking_tbl";
      $this->get_filter_value($filter_input,$table_col_name);
    }
    if($customer_id != ""){
        $this->db->where('co.customer_id',$customer_id);
    }
/*    if($limit == true){
       $this->db->limit($params['length'],$params['start']);
    }
        $result = $this->db->get()->result_array();*/

          if($limit == true){
     $this->db->limit($params['length'],$params['start']);
     $this->db->order_by('co.id','DESC');
      $result = $this->db->get()->result_array();
    }else{
        $row_array = $this->db->get()->result_array();
        $result = count($row_array);
        //print_r($row_array);die;
    }
   //echo $this->db->last_query(); print_r($result);exit;
    return $result;
  }
  

  public function karigar_orders_by_id($filter_status='',$params='',$search='',$limit='',$karigar_id=''){ 
    $this->db->select('*');  
    $this->db->from('customer_orders');
    if(isset($params['columns']) && !empty($params['columns'])){
      $filter_input=$params['columns'];
      $table_col_name="individual_karigar_order_tracking_tbl";
      $this->get_filter_value($filter_input,$table_col_name);
    }
    if($karigar_id != ""){
        $this->db->where('karigar_id',$karigar_id);
    }
          if($limit == true){
     $this->db->limit($params['length'],$params['start']);
     $this->db->order_by('id','DESC');
      $result = $this->db->get()->result_array();
    }else{
        $row_array = $this->db->get()->result_array();
        $result = count($row_array);
    }
    return $result;
  }

    public function delivery_report_in_quantity($filter_status='',$params='',$search='',$limit='',$from_date='',$to_date="",$type=""){ 
    $this->db->select('*');  
    if($type=="department"){
        $where="departments";
    }else{
       $where="category_master";
    }
    $this->db->from($where);
    if(isset($params['columns']) && !empty($params['columns'])){
      $filter_input=$params['columns'];
      $table_col_name="delivery_report_in_qty_tbl";
      $this->get_filter_value($filter_input,$table_col_name);
    }
    
  //   $this->db->group_by($type);
          if($limit == true){
     $this->db->limit($params['length'],$params['start']);
     $this->db->order_by('id','DESC');
      $result = $this->db->get()->result_array();
      //echo $this->db->last_query();print_r($result);exit;
    }else{
        $row_array = $this->db->get()->result_array();
        $result = count($row_array);
    }
    return $result;
  }

    public function order_ledger($filter_status='',$params='',$search='',$limit='',$type="",$frm_date,$to_date){ 
      $frm_date = date("Y-m-d", strtotime($frm_date));
      $to_date = date("Y-m-d", strtotime($to_date));
    $this->db->select('co.party_name,sum(cor.net_weight) as weight,co.customer_id,km.name as karigar_name,co.karigar_id');  
    $this->db->from('customer_orders co');
    $this->db->join('customer_order_received cor','cor.order_id = co.id');
    $this->db->join('karigar_master km','km.id = co.karigar_id','left');
     if($type == '1'){
       $this->db->where('DATE(send_at) >=',$frm_date);
       $this->db->where('DATE(send_at) <=',$to_date);
       $table_col_name="client_order_ledger";
       $this->db->group_by('co.customer_id');
      }else{
       $this->db->where('DATE(cor.created_at) >=',$frm_date);
       $this->db->where('DATE(cor.created_at) <=',$to_date);
       $table_col_name="karigar_order_ledger"; 
       $this->db->group_by('co.karigar_id');       
      }
    if(isset($params['columns']) && !empty($params['columns'])){
      $filter_input=$params['columns'];
      
      $this->get_filter_value($filter_input,$table_col_name);
    }
    
  //   $this->db->group_by($type);
          if($limit == true){
     $this->db->limit($params['length'],$params['start']);
     $this->db->order_by('co.id','DESC');
      $result = $this->db->get()->result_array();
      //echo $this->db->last_query();print_r($result);exit;
    }else{
        $row_array = $this->db->get()->result_array();
        $result = count($row_array);
    }
    return $result;
  }
  
    public function order_ledger_by_customer($filter_status='',$params='',$search='',$limit='',$id="",$frm_date,$to_date){ 
      $frm_date = date("Y-m-d", strtotime($frm_date));
      $to_date = date("Y-m-d", strtotime($to_date));
    // $this->db->select('co.party_name,sum(cor.net_weight) as weight,co.customer_id,dp.name,co.delievery_date,cor.send_at,count(cor.id) as total_order,(select count(*) from customer_orders c join customer_order_received cr on cr.order_id = c.id where c.delievery_date >= DATE(cr.send_at) and c.customer_id ="'.$id.'" and DATE(send_at) >="'.$frm_date.'" and DATE(send_at) <="'.$to_date.'") as ontime_orders');

    $this->db->select('co.party_name,sum(cor.net_weight) as weight,co.customer_id,dp.name,co.delievery_date,cor.send_at,count(cor.id) as total_order,dp.id as department_id');  
    $this->db->from('customer_orders co'); 
    $this->db->join('departments dp','dp.id = co.department_id','left');
    $this->db->join('customer_order_received cor','cor.order_id = co.id');
    $this->db->where('DATE(send_at) >=',$frm_date);
    $this->db->where('DATE(send_at) <=',$to_date);
        $this->db->where('co.customer_id',$id);
      
      
     $table_col_name="order_ledger_by_customer";
       $this->db->group_by('dp.id');
    
    if(isset($params['columns']) && !empty($params['columns'])){
      $filter_input=$params['columns'];
      
      $this->get_filter_value($filter_input,$table_col_name);
    }
    
  //   $this->db->group_by($type);
          if($limit == true){
     $this->db->limit($params['length'],$params['start']);
     $this->db->order_by('co.id','DESC');
      $result = $this->db->get()->result_array();
     //echo $this->db->last_query();print_r($result);exit;
    }else{
        $row_array = $this->db->get()->result_array();
        $result = count($row_array);
    }
    return $result;
  }


  public function get_ontime_orders($dept_id,$id,$frm_date,$to_date){
    $frm_date = date("Y-m-d", strtotime($frm_date));
      $to_date = date("Y-m-d", strtotime($to_date));
    $this->db->select('count(*) as ontime_orders');
    $this->db->from('customer_orders c');
    $this->db->join('customer_order_received cr','cr.order_id = c.id');
    $this->db->where('c.delievery_date >= DATE(cr.send_at)');
    $this->db->where('c.customer_id ="'.$id.'" and DATE(send_at) >="'.$frm_date.'" and DATE(send_at) <="'.$to_date.'" and c.department_id ="'.$dept_id.'"');
    $result = $this->db->get()->row_array();
    return  $result['ontime_orders'];

  }
  
  public function order_ledger_by_karigar($filter_status='',$params='',$search='',$limit='',$id="",$frm_date,$to_date){ 
      $frm_date = date("Y-m-d", strtotime($frm_date));
      $to_date = date("Y-m-d", strtotime($to_date));
    $this->db->select('co.party_name,sum(cor.net_weight) as weight,co.customer_id,dp.name,co.delievery_date,cor.send_at,count(cor.id) as total_order,(select count(*) from customer_orders c join customer_order_received cr on cr.order_id = c.id where c.delievery_date >= DATE(cr.created_at) and c.karigar_id ="'.$id.'" and DATE(cr.created_at) >="'.$frm_date.'" and DATE(cr.created_at) <="'.$to_date.'") as ontime_orders');  
    $this->db->from('customer_orders co');
    $this->db->join('departments dp','dp.id = co.department_id','left');
    $this->db->join('customer_order_received cor','cor.order_id = co.id');
    $this->db->where('DATE(cor.created_at) >=',$frm_date);
    $this->db->where('DATE(cor.created_at) <=',$to_date);
    $this->db->where('co.karigar_id',$id);
      
     $table_col_name="order_ledger_by_customer";
       $this->db->group_by('dp.id');
    
    if(isset($params['columns']) && !empty($params['columns'])){
      $filter_input=$params['columns'];
      
      $this->get_filter_value($filter_input,$table_col_name);
    }
    
  //   $this->db->group_by($type);
          if($limit == true){
     $this->db->limit($params['length'],$params['start']);
     $this->db->order_by('co.id','DESC');
      $result = $this->db->get()->result_array();
     // echo $this->db->last_query();print_r($result);exit;
    }else{
        $row_array = $this->db->get()->result_array();
        $result = count($row_array);
    }
    return $result;
  }
  
    public function delivery_performance($filter_status='',$params='',$search='',$limit='',$type=""){ 
    $this->db->select('co.id,co.parent_category_id parent_category_id ,co.expected_date expected_date,co.delievery_date delivery_date,cor. send_at,cor.created_at created_at,cor.received_qty as quantity,km.name as karigar_name,co.party_name party_name');  
    $this->db->from('customer_orders co');
    $this->db->join('customer_order_received cor','co.id = cor.order_id');
     $this->db->join('karigar_master km','co.karigar_id=km.id');    
    
   
   if($type == 1){
          $this->db->where('cor.status',2);
          $this->db->or_where('cor.status',1);
         $table_col_name="delivery_performance_karigar_tbl";
      }else{
        $this->db->where('cor.status',3);
         $table_col_name="delivery_performance_party_tbl";
      }
    if(isset($params['columns']) && !empty($params['columns'])){
      $filter_input=$params['columns'];
           
      $this->get_filter_value($filter_input,$table_col_name);
    }
    
  //   $this->db->group_by($type);
          if($limit == true){
     $this->db->limit($params['length'],$params['start']);
     $this->db->order_by('co.id','DESC');
      $result = $this->db->get()->result_array();
      //echo $this->db->last_query();print_r($result);exit;
    }else{
        $row_array = $this->db->get()->result_array();
        $result = count($row_array);
    }
    return $result;
  }
  

  
  private function get_filter_value($filter_input,$table_col_name){
    $column_name=array();  
    $filter_column_name=filter_column_name($table_col_name);
    $sql='';
    $i=0;     
    
    foreach ($filter_input as $key => $search_value){
    
       $column_name=$filter_column_name;
       //print_r($search_value['search']['value']);
        if(!empty($search_value['search']['value'])){
          if($i != 0){
            $sql.=' AND  ';
          }
            $sql.=''.$column_name[$key].' like "%'.$search_value['search']['value'].'%" ';
            $i++;
        

         }   
    }

    if(!empty($sql)){  
      $this->db->where($sql);  
    } 
       
  }



  function get_sales_data($filter_status='',$params='',$search='',$limit='',$from_date="",$to_date){
     $this->db->select(''
                . 'co.*,cor.received_qty,km.name km_name,d.name as department_name,'
                . 'c.name category_name,cor.id cor_id,'
                . 'cor.karigar_rcpt_voucher_no,'
                . 'cor.net_weight net_wt,'
                . 'cor.order_id,'
                . 'cor.status as status'
                . '');
        $this->db->from('customer_order_received cor');
        $this->db->join('customer_orders co', 'co.id = cor.order_id');
        $this->db->join('category_master c', 'co.category_id=c.id');
        $this->db->join('karigar_master km', 'co.karigar_id=km.id');
        $this->db->join('departments d', 'co.department_id=d.id');

        $this->db->where("(cor.status =1 OR cor.status=2 OR cor.status=3)");

        if (!empty($from_date) && !empty($to_date)) {
            $this->db->where('co.order_date between"' . date('Y-m-d 00:00:00', strtotime($from_date)) . '" AND "' . date('Y-m-d 23:59:59', strtotime($to_date)) . '"');
        }
     if($limit == true){
     $this->db->limit($params['length'],$params['start']);
      $result = $this->db->get()->result_array();
     // echo $this->db->last_query();print_r($result);exit;
    }else{
        $row_array = $this->db->get()->result_array();
        $result = count($row_array);
    }
    return $result;
        
  }
  
} //class 
