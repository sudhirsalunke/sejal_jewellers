<?php
class Order_status_report_model extends CI_Model {

  function __construct() {
      $this->table_name = "manufacturing_order";
      $this->table_mop="manufacturing_order_mapping";
      $this->table_kmm="Karigar_manufacturing_mapping";
      parent::__construct();
  }

  public function get($filter_status='',$params='',$search='',$limit='',$id=''){
    if(!empty($id))
      $this->db->select('mo.order_name,mo.id,kmm.approx_weight as order_weight,kmm.quantity as total_pcs,kmm.created_at,kmm.receive_quantity,rp.quantity rp_qnt,rp.date receive_date,w.from_weight,w.to_weight,pm.name item_name,kmm.status km_status');
    else  
     $this->db->select('mo.id,mo.order_name,SUM(kmm.approx_weight) as order_weight,SUM(kmm.quantity) as total_pcs');
    $this->db->from($this->table_kmm.' kmm');
    $this->db->join($this->table_mop.' mop','kmm.mop_id = mop.id','left');
    $this->db->join('weights w','w.id = mop.weight_range_id');
    $this->db->join($this->table_name.' mo','mo.id = mop.manufacturing_order_id','left');
    $this->db->join('parent_category pm','pm.id = mop.parent_category_id');
    if(!empty($id))
      $this->db->join('Receive_products rp','rp.kmm_id = kmm.id','left');
    if(!empty($search)){
        $this->db->like('mo.order_name',$search);
    }

  
    if(!empty($id)){
      $this->db->where('mo.id',$id);
    }else{
      $this->db->group_by('mo.id');
    }
    if(isset($params['columns']) && !empty($params['columns'])){
      $filter_input=$params['columns'];
      $this->get_filter_value($filter_input,'order_status_report_table');
    }
  
    if($limit == true){
      $this->db->order_by('mo.id','desc');
      $this->db->limit($params['length'],$params['start']);
      $result = $this->db->get()->result_array();
    }else{
      $row_array = $this->db->get()->result_array();
      $result = count($row_array);
      //echo $this->db->last_query();print_r($result);exit;
    }
    // echo $this->db->last_query();die;
    return $result;
  }

  private function get_filter_value($filter_input,$table_col_name){
    $column_name=array();  
    $filter_column_name=filter_column_name($table_col_name);
    $sql='';
    $i=0;
     
     //print_r($filter_input);die;
    foreach ($filter_input as $key => $search_value){
       $column_name=$filter_column_name;
       //print_r($search_value['search']['value']);
        if(!empty($search_value['search']['value'])){
          if($i != 0){
            $sql.=' AND  ';
          }
            $sql.=''.$column_name[$key].' like "%'.$search_value['search']['value'].'%" ';
            $i++;
        

         }   
    }

    if(!empty($sql)){  
      $this->db->where($sql);  
    } 
   
       
  }

  
} //class 