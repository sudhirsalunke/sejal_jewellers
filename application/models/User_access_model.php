<?php
class User_access_model extends CI_Model {

  function __construct() {
      $this->table_name = "admin";
      parent::__construct();
  }

  public function validatepostdata($update=''){
    if($update == true)
	    $this->form_validation->set_rules($this->config->item('User_access_edit', 'admin_validationrules'));
    else
      $this->form_validation->set_rules($this->config->item('User_access', 'admin_validationrules'));

    $data['status']= 'success';
    if($this->form_validation->run()===FALSE){
      $data['status']= 'failure';
      $data['data']= '';
      $data['error'] = $this->getErrorMsg();
    }
    return $data;
  }
  public function get($id='',$filter_status='',$status='',$params='',$search='',$limit=''){
    $this->db->select('a.email,a.first_name,a.last_name,concat(a.first_name," ",a.last_name) as name,a.id,a.encrypted_id,GROUP_CONCAT(Distinct mm.display_name) as master_name,GROUP_CONCAT(Distinct mm.id) as master_id,GROUP_CONCAT(Distinct c.id) as controller_id,a.department_id,a.show_dashboard,a.location_id');
    $this->db->from('admin a');
    $this->db->join('admin_controllers_mapping acm','acm.admin_id = a.id','left');
    $this->db->join('controllers c','c.id = acm.controller_id','left');
    $this->db->join('menu_master mm','mm.id = c.master_id','left');
  /*  if(!empty($search)){
        $this->all_like_queries($search);
    }*/
    //print_r($params['columns'][0]['search']['value']);
    if(isset($params['columns'][0]['search']['value']) && !empty($params['columns'][0]['search']['value']))
    {
      $this->db->where("(`a`.`first_name` LIKE '%".$params['columns'][0]['search']['value']."%' OR  `a`.`last_name` LIKE '%".$params["columns"][0]["search"]["value"]."%')");
    }else  if(isset($params['columns']) && !empty($params['columns'])){
      $filter_input=$params['columns'];
      $table_col_name='user_access';
      $this->get_filter_value($filter_input,$table_col_name);
    }
 

    if(!empty($id)){
      $this->db->where('a.id',$id);
    }
    $this->db->group_by('a.id');
    $this->db->order_by('a.id','DESC');
   // echo $this->db->last_query();die;
    $result = $this->db->get()->result_array();
    return $result;
  }
  private function all_like_queries($search){
    $this->db->where("(`a`.`first_name` LIKE '%$search%' OR  `a`.`last_name` LIKE '%$search%' OR  `a`.`email` LIKE '%$search%')");

  }
   private function get_filter_value($filter_input,$table_col_name){
    $column_name=array();  
    $filter_column_name=filter_column_name($table_col_name);
    $sql='';
    $i=0;
     
     //print_r($filter_input);die;
    foreach ($filter_input as $key => $search_value){
       $column_name=$filter_column_name;
       //print_r($search_value['search']['value']);
        if(!empty($search_value['search']['value'])){
          if($i != 0){
            $sql.=' AND  ';
          }
            $sql.=''.$column_name[$key].' like "%'.$search_value['search']['value'].'%" ';
            $i++;
        

         }   
    }

    if(!empty($sql)){  
      $this->db->where($sql);  
    } 
       
  }

  private function getErrorMsg(){
    return array(
      'first_name'=>strip_tags(form_error('User_access[first_name]')),
      'last_name'=>strip_tags(form_error('User_access[last_name]')),
      'email'=>strip_tags(form_error('User_access[email]')),
      'password'=>strip_tags(form_error('User_access[password]')),
      'controller_id'=>strip_tags(form_error('User_access[controller_id][]'))
    );
  }
  public function get_master(){
    $data = array();
    $this->db->select('m.display_name master_display_name,m.name master_name,c.*');
    $this->db->from('menu_master m');
    $this->db->join('controllers c','m.id=c.master_id');
    $res = $this->db->get()->result_array();
    foreach ($res as $val)
      $data[$val['master_id']][] = $val;
    return $data;
    //echo '<pre>'; print_r($data); echo '</pre>'; exit();
  }
  public function get_location(){
   
    $this->db->select('id,name');
    $this->db->from('location_master');
    $res = $this->db->get()->result_array();
    return $res;
  }

  public function get_department_map($admin_id){
    $data = array();
    $this->db->select('id,department_id');
    $this->db->from('user_department_mapping'); 
    $this->db->where('admin_id',$admin_id);  
    $res = $this->db->get()->result_array();
    foreach ($res as $val)
      $data['department_id']['department_id'][] = $val['department_id'];
    return $data;
    //echo '<pre>'; print_r($data); echo '</pre>'; exit();
  }
  public function store($insert){ 
  
  	$encrption = $this->data_encryption->get_encrypted_id($this->table_name);
    if($insert['location_id']=="1" || $insert['location_id']=="0"){
      $show_dashboard="Manufacturing_Dashboard";
    }else{
      $show_dashboard="sales_dashboard";

    }
  	$insert_array = array(
      'first_name' => $insert['first_name'],
  		'last_name' => $insert['last_name'],
      'email' => $insert['email'],
  		'department_id' => @$insert['department_id'][0],
      'location_id' => $insert['location_id'],
      'show_dashboard' => $show_dashboard,
      'encrypted_id' => $encrption,
  		'password' => md5($insert['password']),
  		'updated_at' => date('Y-m-d H:i:s'),
  		'created_at' => date('Y-m-d H:i:s')
  		);
  	if($this->db->insert('admin',$insert_array)){
      $insert_id =  $this->db->insert_id();
      $result = $insert['controller_id'];
      foreach ($result as $key => $value) {
        $batch_array[$key]['admin_id'] = $insert_id;
        $batch_array[$key]['controller_id'] = $value;
        $batch_array[$key]['updated_at'] = date('Y-m-d H:i:s');
        $batch_array[$key]['created_at'] = date('Y-m-d H:i:s');
      }
        if($this->db->insert_batch('admin_controllers_mapping',$batch_array)){
             $department_id = @$insert['department_id'];
             //print_r( $department_id);die;
             if(!empty($department_id)){
                foreach ($department_id as $key => $value) {
                    $department_array[$key]['admin_id'] = $insert_id;
                    $department_array[$key]['department_id'] = $value;
                    $department_array[$key]['updated_at'] = date('Y-m-d H:i:s');
                    $department_array[$key]['created_at'] = date('Y-m-d H:i:s');
                }
            $this->db->insert_batch('user_department_mapping',$department_array);  
             }
        }
      
    }
  	 return get_successMsg();
  }

  public function find($id){
  	$this->db->where('id',$id);
  	$result = $this->db->get($this->table_name)->row_array();
  	return $result;
  }

  public function get_primary_key($encrypted_key){
  	$this->db->where('encrypted_id',$encrypted_key);
  	$result = $this->db->get($this->table_name)->row_array();
  	return $result['id'];
  }
  public function update($insert){
    if($insert['location_id']=="1" || $insert['location_id']=="0"){
      $show_dashboard="Manufacturing_Dashboard";
    }else{
      $show_dashboard="sales_dashboard";

    }
    $insert_array = array(
      'first_name' => $insert['first_name'],
      'last_name' => $insert['last_name'],
      'email' => $insert['email'],
      'department_id' => @$insert['department_id'][0],
      'location_id' => $insert['location_id'],
      //'show_dashboard' => $insert['show_dashboard'],
      'show_dashboard' => $show_dashboard,
      'updated_at' => date('Y-m-d H:i:s'),
      'created_at' => date('Y-m-d H:i:s')
      );
    
    if(!empty($insert['password'])){
      $insert_array['password'] = md5($insert['password']);
    }
    $this->db->where('id',$insert['id']);
    if($this->db->update('admin',$insert_array)){
      $insert_id = $insert['id'];
      $result = $insert['controller_id'];
      $this->db->where('admin_id',$insert_id);
      $this->db->delete('admin_controllers_mapping');
      foreach ($result as $key => $value) {
        $batch_array[$key]['admin_id'] = $insert_id;
        $batch_array[$key]['controller_id'] = $value;
        $batch_array[$key]['updated_at'] = date('Y-m-d H:i:s');
        $batch_array[$key]['created_at'] = date('Y-m-d H:i:s');
      }
      if($this->db->insert_batch('admin_controllers_mapping',$batch_array)){
             $department_id = @$insert['department_id'];
              if(!empty($department_id)){
               $this->db->where('admin_id',$insert_id);
                  if($this->db->delete('user_department_mapping')){
                          foreach ($department_id as $key => $value) {
                              $department_array[$key]['admin_id'] = $insert_id;
                              $department_array[$key]['department_id'] = $value;
                              $department_array[$key]['updated_at'] = date('Y-m-d H:i:s');
                              $department_array[$key]['created_at'] = date('Y-m-d H:i:s');
                          }
                    $this->db->insert_batch('user_department_mapping',$department_array);  
                  }
            }
        }
    }
     return get_successMsg();
  }
  public function delete($id){
  	$this->db->where('id',$id);
  	if($this->db->delete($this->table_name)){
      $this->db->where('admin_id',$id);
      $this->db->delete('admin_controllers_mapping');
  		return get_successMsg();
  	}else{
  		return get_errorMsg();
  	}
  }
}    