<?php

class Party_master_model extends CI_Model {

  function __construct() {
      $this->karigar_table = "karigar_master";
      $this->customer_table = "customer";
      parent::__construct();
  }

  public function validatepostdata(){
  	//$this->form_validation->set_message('check_karigar_code', 'Code is already exists');
    if(!empty($_POST['customer_order'])){
      $_POST['Karigar'] = $_POST['customer_order'];
      $_POST['Karigar']['name'] = $_POST['Karigar']['party_name'];
    }
  	$this->form_validation->set_rules($this->config->item('Party', 'admin_validationrules'));

    if(@$_POST['Karigar']['customer_type_id']==1) {
   $this->form_validation->set_rules('Karigar[code]', 
                                    'Karigar code', 
                                    'trim|required|check_karigar_code',
                                    array('check_karigar_code' =>"Code is already exists")
                                       );
  }

     if ($this->form_validation->run() == FALSE) {       	
          return FALSE;
      } else {
          return TRUE;
      }
  }
  public function store($postdata=''){
  	$postdata = $this->input->post('Karigar');
  	$encrption = $this->data_encryption->get_encrypted_id();

     $insert_array = $postdata;
     $num_str = sprintf("%06d", mt_rand(1, 999999));
     $this->db->select('max(id)+1 as last_id');
     $result = $this->db->get('customer')->row_array();
     $last_id =$result['last_id'];
     $insert_array['client_code']='C'.$num_str.$last_id;
     $insert_array['encrypted_id']=$encrption;
     $insert_array['updated_at']=date('Y-m-d H:i:s');
     $insert_array['created_at']=date('Y-m-d H:i:s');
  	// $insert_array = array(
  	// 	'name' => $postdata['name'],
  	// 	'code' => $postdata['code'],
   //    'address' => $postdata['address'],
   //    'contact_person' => $postdata['contact_person'],
  	// 	'encrypted_id' => $encrption,
  	// 	'updated_at' => date('Y-m-d H:i:s'),
  	// 	'created_at' => date('Y-m-d H:i:s')
  	// 	);
     //print_r($insert_array);die;
     if ($insert_array['customer_type_id']==1) {
       $tbl_name=$this->karigar_table;
     }else{
      unset($insert_array['code'],$insert_array['phone_no']);
       $tbl_name=$this->customer_table;
     }
  	if($this->db->insert($tbl_name,$insert_array)){
  		return get_successMsg();
  	}else{
  		return get_errorMsg();
  	}
  }
  public function get($filter_status='',$params='',$search='',$limit='',$type='',$auto_complete=""){
    $custom_query="(SELECT c.id,c.encrypted_id,c.mobile_no,c.company_name,c.email,c.name,ct.name as type,'' as code FROM customer c join customer_type ct on customer_type_id=ct.id  ";
 //print_r($params['columns'][3]['search']['value']);exit;
    // print_r($type);die;
    // print_r($auto_complete);die;
    $where = "";
    $where1 = "";
    $where_query = "";
    $where_query1 = "";
    if((!empty($params['columns'][0]['search']['value'])) || (!empty($params['columns'][1]['search']['value'])) || (!empty($params['columns'][3]['search']['value']))){
       $where.= "where ";
       $where1.= "where ";
     }
if (isset($params['columns'][0]['search']['value']) && !empty($params['columns'][0]['search']['value'])) 
    {
      $where .=" ct.name like '%".$params['columns'][0]['search']['value']."%' or";
    }
  
    if (isset($params['columns'][1]['search']['value']) && !empty($params['columns'][1]['search']['value'])) 
    {
      $where .=" c.name like '%".$params['columns'][1]['search']['value']."%' or";
    }

    
    if (isset($params['columns'][3]['search']['value']) && !empty($params['columns'][3]['search']['value'])) 
    {
      $where .=" c.mobile_no like '%".$params['columns'][3]['search']['value']."%'";
    }
    $where_query= preg_replace('/or$/', '', $where);
    $custom_query .= $where_query; 
    if(!empty($type)){
        $custom_query .="where (c.customer_type_id='".$type."')";
      if (!empty($auto_complete)) 
    {
      $custom_query .=" AND c.name like '%".$auto_complete."%'";
    }  
    }else{
       $custom_query .="Order By c.id DESC";

    }
    $custom_query .= " ) union ( select k.id,k.encrypted_id,k.mobile_no,k.company_name,k.email,k.name,cty.name  as type,k.code as code  from karigar_master k join customer_type cty on k.customer_type_id = cty.id ";
      if (isset($params['columns'][0]['search']['value']) && !empty($params['columns'][0]['search']['value'])) 
    {
      $where1 .=" cty.name like '%".$params['columns'][0]['search']['value']."%' or";
    }

    if(isset($params['columns'][1]['search']['value']) && !empty($params['columns'][1]['search']['value'])){
        $where1 .=" k.name like '%".$params['columns'][1]['search']['value']."%' or";
      }
     if (isset($params['columns'][3]['search']['value']) && !empty($params['columns'][3]['search']['value'])) 
    {
      $where1 .="  k.mobile_no like '%".$params['columns'][3]['search']['value']."%' or";
    }
        $where_query1= preg_replace('/or$/', '', $where1);
     $custom_query .= $where_query1; 
    if(!empty($type)){
        $custom_query .=" where (k.customer_type_id='".$type."')";
      if (!empty($auto_complete)) 
      {
        $custom_query .=" AND k.name like '%".$auto_complete."%'";
      }   

    }
    $custom_query .=' ) Order By name  ASC ';

  /*  if($limit == true){
       $custom_query .="LIMIT ".$params['start'].",".$params['length'];
    }*/	

    if($limit == true ){
      /* if(!empty($params)){
      $this->db->limit($params['length'],$params['start']);
      }*/
      $result = $this->db->query($custom_query)->result_array(); 
  //echo $this->db->last_query();//echo "<pre>";print_r($result);exit;     
    }else{
      $row_array = $this->db->query($custom_query)->result_array();
      $result = count($row_array);
    //  echo $this->db->last_query();echo "<pre>";//print_r($result);exit;
    }
  // echo $this->db->last_query();print_r($result);
    return $result;
  }
  


  public function find($party_type,$id){
    if ($party_type=="partyk") {
      $tbl_name =$this->karigar_table;
    }else{
      $tbl_name =$this->customer_table;
    }
  	$this->db->where('encrypted_id',$id);
  	$result = $this->db->get($tbl_name)->row_array();
  	return $result;
  }

  public function get_primary_key($encrypted_key,$party_type){
    if ($party_type=="partyk") {
      $tbl_name =$this->karigar_table;
    }else{
      $tbl_name =$this->customer_table;
    }
  	$this->db->where('encrypted_id',$encrypted_key);
  	 $result = $this->db->get($tbl_name)->row_array();
  	return $result['id'];
  }
  public function update(){
  	$postdata = $this->input->post('Karigar');
  	// $id = $this->get_primary_key($postdata['encrypted_id']);
    $update_array=$postdata;
    $update_array['updated_at']=date('Y-m-d H:i:s');
  	// $update_array = array(
  	// 	'name' => $postdata['name'],
  	// 	'code' => $postdata['code'],
   //    'address' => $postdata['address'],
   //    'contact_person' => $postdata['contact_person'],
  	// 	'updated_at' => date('Y-m-d H:i:s')
  	// 	);
    unset($update_array['encrypted_id']);
    if ($update_array['customer_type_id']==1) {
       $tbl_name=$this->karigar_table;
     }else{
      unset($update_array['code'],$update_array['company_name'],$update_array['phone_no']);
       $tbl_name=$this->customer_table;
     }
     
  	$this->db->where('encrypted_id',$postdata['encrypted_id']);
  	if($this->db->update($tbl_name,$update_array)){
  		return get_successMsg();
  	}else{
  		return get_errorMsg();
  	}
  }
  public function delete($id,$party_type){
    if ($party_type=="partyk") {
      $tbl_name =$this->karigar_table;
    }else{
      $tbl_name =$this->customer_table;
    }
  	$this->db->where('id',$id);
  	if($this->db->delete($tbl_name)){
  		return get_successMsg();
  	}else{
  		return get_errorMsg();
  	}
  }

  public function get_karigar_name(){
        $this->db->select('name');
        $result = $this->db->get($this->table_name)->result_array();
        return array_column($result, 'name');
    } 

  public function get_party_list(){
    if (!empty($_POST['customer_type_id'])) {
        if ($_POST['customer_type_id']==1) {
          return  $this->db->get_where('karigar_master',array('customer_type_id'=>1))->result_array();
        }else{
          return  $this->db->get_where('customer',array('customer_type_id'=>$_POST['customer_type_id']))->result_array();
        }
    }else{
      return get_errorMsg();
    }
  }  
}    