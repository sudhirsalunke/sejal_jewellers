<?php

class Cron_model extends CI_Model {
	public function get_dropbox_status($id=1,$type = '1'){
		$this->db->select('status');
		$this->db->where('id',$id);
		$this->db->where('type',$type);
		$result = $this->db->get('fetch_from_dropbox')->row_array();
		return $result['status'];
	}
}	