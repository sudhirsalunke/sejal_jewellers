<?php
class Sales_recieved_product_model extends CI_Model {

  function __construct() {
      $this->table_name = "department_ready_product";
      $this->table_name_mapping="";
      parent::__construct();
  }

  public function validatepostdata(){
   
  	$this->form_validation->set_rules($this->config->item('sales_prepare_order', 'admin_validationrules'));
     if ($this->form_validation->run() == FALSE) {       	
          return FALSE;
      } else {
          return TRUE;
      }
  }
  public function store(){
    
  	$postdata = $this->input->post('product');
    /*create_order*/
    $this->db->insert($this->table_name,array('department_id' => $postdata['department_id'],
                                              'is_sale' => 1,
                                              'created_at' => date('Y-m-d H:i:s'),
                                              'updated_at' =>date('Y-m-d H:i:s'),
                                              ));
    /**/
    $order_id = $this->db->insert_id();
    $insert_array=array();
    foreach ($postdata['sub_category'] as $key => $value) {
        $insert_array[$key]['manufacturing_order_id']=$order_id;
        $insert_array[$key]['sub_category_id']=$value;
        $insert_array[$key]['weight_range_id']=$postdata['weight'][$key];
        $insert_array[$key]['quantity']=$postdata['quantity'][$key];
        $insert_array[$key]['is_sale']=1;
        $insert_array[$key]['created_at']=date('Y-m-d H:i:s');
        $insert_array[$key]['updated_at']=date('Y-m-d H:i:s');
         
      
    }
  
  if($this->db->insert_batch($this->table_name_mapping,$insert_array)){
  		return get_successMsg();
  	}else{
  		return get_errorMsg();
  	}
  }
  public function get($filter_status='',$status='',$params='',$search='',$limit='',$department_id='',$location_id=''){
    $this->db->select('pr.*,SUM(pr.gr_wt) as total_gross,SUM(pr.quantity) as total_qty,date_format(pr.created_at,"%d-%m-%Y") as created_at,pr.voucher_no,date_format(pr.sales_date,"%d-%m-%Y") as sales_date,date_format(pr.rejected_date,"%d-%m-%Y") as rejected_date');
    if (@$_GET['status']=="sended_to_manufacture") {
      $table_col_name='sales_returned_product_table';
      $this->db->where('pr.status',3);
    }else{
      $table_col_name='sales_recieved_product_table';
      $this->db->where('pr.status',1);
    }
    if(!empty($location_id)){
    $this->db->where('pr.location_id',$location_id);
    }
    $this->db->from($this->table_name.' pr');
    /*$this->db->group_by('pr.chitti_no');*/
    $this->db->group_by('pr.voucher_no');
   // $this->db->where('mo.is_sale',1);
    /*if(!empty($search)){
        $this->db->like('pr.chitti_no',$search);
    }*/
    if(!empty($department_id)){
        $this->db->where('pr.department_id',$department_id);
    }
         
  
    $this->db->order_by('pr.id','DESC');
     if (isset($params['columns']) && !empty($params['columns'])){
      $filter_input=$params['columns'];
      $this->get_filter_value($filter_input,$table_col_name);
    } 
    if($limit == true){     
      $this->db->limit($params['length'],$params['start']);
      $result = $this->db->get()->result_array();
      //echo $this->db->last_query();print_r($result);exit;
    }else{
      $row_array = $this->db->get()->result_array();
      $result = count($row_array);
    }
    return $result;
  }
  public function get_chitti_wise($chitti_no,$filter_status='',$status='',$params='',$search='',$limit='',$department_id){
    // $this->db->select('pr.*,tag.net_wt,pm.name as parent_category,pr.quantity');
    $this->db->select('pr.*');
    if (@$_GET['status']=="sended_to_manufacture") {
      $this->db->where('pr.status',3);
    }else{
      $this->db->where('pr.status',1);
    }
    $this->db->from($this->table_name.' pr');
    $this->db->join('make_set_quantity_relation msqr','pr.msqr_id=msqr.id','left');
    $this->db->join('tag_products tag','msqr.tag_id=tag.id','left');
    $this->db->join('tag_products tp','msqr.tag_id = tp.id','left');
    $this->db->join('parent_category pm','pm.id = tp.sub_category_id','left');
    if(!empty($chitti_no)){
      $this->db->where('pr.chitti_no',$chitti_no);
    }
   // $this->db->where('mo.is_sale',1);
 /*   if(!empty($search)){
        $this->db->like('pm.name',$search);
    }*/
    
     if(!empty($department_id)){
      $this->db->where('pr.department_id',$department_id);
     }
    
     if(isset($params['columns']) && !empty($params['columns'])){
      $filter_input=$params['columns'];
      $table_col_name="sales_recieved_product_show_table";
      $this->get_filter_value($filter_input,$table_col_name);
    }

    if($limit == true)
      $this->db->limit($params['length'],$params['start']);
    $result = $this->db->get()->result_array();
    return $result;
  }
  public function get_voucher_no_wise($voucher_no,$filter_status='',$status='',$params='',$search='',$limit='',$department_id){
    // $this->db->select('pr.*,tag.net_wt,pm.name as parent_category,pr.quantity');
    $this->db->select('pr.*,lm.name as location');
    if (@$_GET['status']=="sended_to_manufacture") {
      $this->db->where('pr.status','3');
    }else{
      $this->db->where('pr.status',1);
    }
    $this->db->from($this->table_name.' pr');
    $this->db->join('make_set_quantity_relation msqr','pr.msqr_id=msqr.id','left');
    $this->db->join('tag_products tag','msqr.tag_id=tag.id','left');
    $this->db->join('tag_products tp','msqr.tag_id = tp.id','left');
    $this->db->join('parent_category pm','pm.id = tp.sub_category_id','left');
    $this->db->join('location_master lm','lm.id = pr.location_id');

    if(!empty($voucher_no)){
      $this->db->where('pr.voucher_no',$voucher_no);
    }
   // $this->db->where('mo.is_sale',1);
 /*   if(!empty($search)){
        $this->db->like('pm.name',$search);
    }*/
    
     if(!empty($department_id)){
      $this->db->where('pr.department_id',$department_id);
     }
    
     if(isset($params['columns']) && !empty($params['columns'])){
      $filter_input=$params['columns'];
      $table_col_name="sales_recieved_product_show_table";
      $this->get_filter_value($filter_input,$table_col_name);
    }

 /*   if($limit == true){
      if(!empty($params)){
      $this->db->limit($params['length'],$params['start']);
      }
    }
    $result = $this->db->get()->result_array();
*/
    if($limit == true){
    
       if(!empty($params)){
      $this->db->limit($params['length'],$params['start']);
      }
      $result = $this->db->get()->result_array();
    }else{
      $row_array = $this->db->get()->result_array();
      $result = count($row_array);
      //echo $this->db->last_query();print_r($result);exit;
    }
    //echo $this->db->last_query();die;
    return $result;
  }
  private function get_filter_value($filter_input,$table_col_name){
    $column_name=array();  
    $filter_column_name=filter_column_name($table_col_name);
    $sql='';
    $i=0;
     
     //print_r($filter_input);die;
    foreach ($filter_input as $key => $search_value){
       $column_name=$filter_column_name;
       //print_r($search_value['search']['value']);
        if(!empty($search_value['search']['value'])){
          if($i != 0){
            $sql.=' AND  ';
          }
            $sql.=''.$column_name[$key].' like "%'.$search_value['search']['value'].'%" ';
            $i++;
        

         }   
    }

    if(!empty($sql)){  
      $this->db->where($sql);  
    } 
       
  }

  public function find_by_encrypted_id($encrypted_id){
   $this->db->where('encrypted_id',$encrypted_id);
   $result = $this->db->get($this->table_name)->row_array();
   return $result;
  }
  public function find($id){
  	$this->db->where('id',$id);
  	$result = $this->db->get($this->table_name)->row_array();
  	return $result;
  }

  // public function get_primary_key($encrypted_key){
  // 	$this->db->where('encrypted_id',$encrypted_key);
  // 	$result = $this->db->get($this->table_name)->row_array();
  // 	return $result['id'];
  // }
  public function update(){
    $postdata =$this->input->post('hallmarking_list');
    $update_array = $postdata;
  	// $update_array = array(
  	// 	'name' => $postdata['name'],
   //    'hc_id' => $postdata['parent_category'],
   //    'touch' => $postdata['touch'],
  	// 	'updated_at' => date('Y-m-d H:i:s')
  	// 	);
  	$this->db->where('encrypted_id',$_POST['encrypted_id']);
  	if($this->db->update($this->table_name,$update_array)){
  		return get_successMsg(1);
  	}else{
  		return get_errorMsg();
  	}
  }
  public function delete($id){
  	$this->db->where('encrypted_id',$id);
  	if($this->db->delete($this->table_name)){
  		return get_successMsg();
  	}else{
  		return get_errorMsg();
  	}
  }

 //  public function check_pc_used($id){
 //      $this->db->where('',$id);
 //      $this->db->from('');
 //     return $this->db->count_all_results();
 // }

   public function send_to_manufacture($id){
     $this->db->where('id',$id);
     if ($this->db->update($this->table_name,array('sent'=>1,'order_date'=>date('Y-m-d')))){
       return get_successMsg();
      }else{
        return get_errorMsg();
      }
   }

  public function get_location_master(){
      $result = $this->db->get('location_master')->result_array();
    return $result;
  }  
  public function get_ready_product_data($product_code){
      $this->db->where('product_code',$product_code);
      $this->db->where('status','1');
      $result = $this->db->get($this->table_name)->row_array();
    return $result;
  }

}//class    