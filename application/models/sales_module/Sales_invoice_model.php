<?php
class Sales_invoice_model extends CI_Model {

  function __construct() {
      $this->table_name = "sales_invoice";
      $this->table_name_invoice_details = "sales_invoice_details";
      parent::__construct();
  }

  public function validatepostdata(){
   
  	$this->form_validation->set_rules($this->config->item('sales_invoice', 'admin_validationrules'));
     if ($this->form_validation->run() == FALSE) {       	
          return FALSE;
      } else {
          return TRUE;
      }
  }

  public function validate_invoice_details(){
    $input =$_POST['invoice'];
    $response="";
    foreach ($input['item_no'] as $i_key => $i_value) {
      $i=$i_key+1;
      if (empty($i_value)) {
        $response .="<p>Row No $i : Item No Should Not be empty</p>";
      }
      //  if (empty($input['type'][$i_key])) {
      //   $response .="<p>Row No $i : Please Select Type</p>";
      // }

      if (!empty($input['cs_wt'][$i_key]) && empty($input['cs_a'][$i_key])) {
        $response .="<p>Row No $i : Color Stone Amount Should Not be empty</p>";
      }
       if (!empty($input['kundan_wt'][$i_key]) && empty($input['kundan_a'][$i_key])) {
        $response .="<p>Row No $i : kundan Amount Should Not be empty</p>";
      }
    }
    return $response;
    // print_r($_POST['invoice']);exit;
  }

  public function store(){
      $postdata = $this->input->post('invoice_details');
      $postdata['transaction_date']=date('Y-m-d',strtotime($postdata['transaction_date']));
      $postdata['created_at']=date('Y-m-d H:i:s');
      $postdata['updated_at']=date('Y-m-d H:i:s'); 

  if($this->db->insert($this->table_name,$postdata)){
     $last_inserted = $this->db->insert_id();
     $this->insert_invoice_details($last_inserted);
    
  		return get_successMsg();
  	}else{
  		return get_errorMsg();
  	}
  }

  public function insert_invoice_details($invoice_id){
    $insert_array=array();
    $all_data=$this->input->post('invoice');
    foreach ($all_data['item_no'] as $key => $value) {
      /*define*/
      $color_stone_wt=$all_data['cs_wt'][$key];
      $kundan_wt=$all_data['kundan_wt'][$key];
      $gross_wt =$all_data['gross_wt'][$key];
      $kundan_pcs =$all_data['kundan_pcs'][$key];
      $cs_a =$all_data['cs_a'][$key];
      $kundan_a =$all_data['kundan_a'][$key];
      $other = $all_data['other'][$key];

      $insert_array[$key]['sales_invoice_id']=$invoice_id;
      $insert_array[$key]['created_at']=date('Y-m-d H:i:s');
      $insert_array[$key]['updated_at']=date('Y-m-d H:i:s'); 

      /*new*/
      $net_wt =$gross_wt - ($color_stone_wt+$kundan_wt);
      $color_stone_amt = $color_stone_wt*$cs_a;
      $kundan_amt =$kundan_pcs*$kundan_a;

      $insert_array[$key]['gross_wt']=$gross_wt;
      $insert_array[$key]['net_wt']=$net_wt;
      $insert_array[$key]['kundan_pcs']=$kundan_pcs;
      $insert_array[$key]['cs_amt']=$color_stone_amt;
      $insert_array[$key]['kundan_amt']=$kundan_amt;
      $insert_array[$key]['total_amt']=$kundan_amt+$color_stone_amt+$other;
      /*post data*/
      //$insert_array[$key]['type']=$value;
      $insert_array[$key]['product_code']=$all_data['item_no'][$key];
      $insert_array[$key]['stock_id']=$all_data['stock_id'][$key];
      $insert_array[$key]['cs_wt']=$color_stone_wt;
      $insert_array[$key]['kundan_wt']=$kundan_wt;
      $insert_array[$key]['cs_a']=$cs_a;
      $insert_array[$key]['kundan_a']=$kundan_a;
      $insert_array[$key]['rodo']=$all_data['rodo'][$key];
      $insert_array[$key]['rodi']=$all_data['radi'][$key];
      $insert_array[$key]['other']=$other;
    }

    $this->db->insert_batch($this->table_name_invoice_details,$insert_array);

    /*update*/
    $this->db->set('status',1);
    $this->db->where_in('id',$all_data['stock_id']);
    $this->db->update('sale_stock');
  }

  public function get($filter_status='',$params='',$search='',$limit=''){
    $this->db->select('si.id as chitti_no,count(sid.id) as products,si.status,c.name as customer_name,cs.name as color_stone_name,kc.name as kundan_category_name,date_format(si.created_at,"%D %b %Y") as created_at,SUM(sid.gross_wt) as gross_wt,SUM(sid.total_amt) as total_amt,SUM(msqr.pcs) as total_pcs');
    $this->db->from($this->table_name.' si');
    $this->db->join($this->table_name_invoice_details.' sid','si.id=sid.sales_invoice_id','left');
    $this->db->join('customer c','si.customer_id=c.id');
    $this->db->join('variation_category cs','si.color_stone_id=cs.id');
    $this->db->join('variation_category kc','si.kundan_category_id=kc.id');
    $this->db->join('department_ready_product drp','sid.product_code=drp.product_code');
    $this->db->join('make_set_quantity_relation msqr','drp.msqr_id=msqr.id');

    if (@$_GET['status']!="sent") { 
      $this->db->where('(si.status=0 OR si.status=3)');
    }else{
      $this->db->where('si.status',1);
    }
    if(!empty($search)){
        $this->db->like('si.id',$search);
    }
    if($limit == true)
    	$this->db->limit($params['length'],$params['start']);

    $this->db->group_by('si.id');
    $result = $this->db->get()->result_array();
   //echo $this->db->last_query(); print_r($result);exit;
    return $result;
  }

  public function find($id){
  	$this->db->where('id',$id);
  	$result = $this->db->get($this->table_name)->row_array();
  	return $result;
  }

  public function find_view_data($id){
    $this->db->select('si.*,c.name as customer_name,cs.name as color_stone_name,kc.name as kundan_category_name');
    $this->db->where('si.id',$id);
    $this->db->from($this->table_name.' si'); 
    $this->db->join('customer c','si.customer_id=c.id');
    $this->db->join('variation_category cs','si.color_stone_id=cs.id');
    $this->db->join('variation_category kc','si.kundan_category_id=kc.id');
    $result = $this->db->get()->row_array();
    return $result;
  }

    public function invoice_details($id,$params='',$search='',$limit=''){
    //$this->db->select('sid.*,tm.name as type_name,drp.color_stone as cs,drp.kundan as kun,drp.gr_wt,drp.net_wt,drp.kundan_pcs as kun_pcs,sid.sales_invoice_id as product_invoice_id,sid.id as product_inv_detail_id');
      $this->db->select('sid.*,sid.sales_invoice_id as product_invoice_id,sid.id as product_inv_detail_id');
    $this->db->where('sid.sales_invoice_id',$id);
    $this->db->from($this->table_name_invoice_details.' sid');
   // $this->db->join('type_master tm','sid.type=tm.id');
    $this->db->join('sale_stock ss','sid.stock_id=ss.id','left');
    // $this->db->join('department_ready_product drp','ss.drp_id=drp.id','left');

     if(!empty($search)){
        $this->db->like('sid.product_code',$search);
    }
    if($limit == true)
      $this->db->limit($params['length'],$params['start']);

    $result = $this->db->get()->result_array();
    return $result;
  }

  public function update(){
    $postdata = $this->input->post('invoice_details');
    $postdata['transaction_date']=date('Y-m-d',strtotime($postdata['transaction_date']));
    $postdata['updated_at']=date('Y-m-d H:i:s'); 
    $this->db->where('id',$_POST['invoice_id']);
  if($this->db->update($this->table_name,$postdata)){
     $this->update_invoice_details($_POST['invoice_id']);
    
      return get_successMsg();
    }else{
      return get_errorMsg();
    }
  }

  public function update_invoice_details($invoice_id){
    $insert_array=array();
    $updated_ids=array();
    $new_added=array();
    $all_data=$this->input->post('invoice');
    foreach ($all_data['item_no'] as $key => $value) {

      // $insert_array['sales_invoice_id']=$invoice_id;
      // $insert_array['created_at']=date('Y-m-d H:i:s');
      $insert_array['updated_at']=date('Y-m-d H:i:s'); 
      /*post data*/
      //$insert_array['type']=$value;
      $insert_array['product_code']=$all_data['item_no'][$key];
      $insert_array['stock_id']=$all_data['stock_id'][$key];
      $insert_array['cs_wt']=$all_data['cs_wt'][$key];
      $insert_array['kundan_wt']=$all_data['kundan_wt'][$key];
      $insert_array['cs_amt']=$all_data['cs_amt'][$key];
      $insert_array['kundan_amt']=$all_data['kundan_amt'][$key];
      $insert_array['rodo']=$all_data['rodo'][$key];
      $insert_array['rodi']=$all_data['radi'][$key];
      $insert_array['other']=$all_data['other'][$key];

      if (isset($all_data['id'][$key]) && !empty($all_data['id'][$key])) {
        $updated_ids[]=$all_data['id'][$key];
        $this->db->where('id',$all_data['id'][$key]);
        $this->db->update($this->table_name_invoice_details,$insert_array);
       
      }else{
        $new_added[]=$all_data['stock_id'][$key];
        $insert_array['sales_invoice_id']=$invoice_id;
        $insert_array['created_at']=date('Y-m-d H:i:s');
        $this->db->insert($this->table_name_invoice_details,$insert_array);
        $updated_ids[]=$this->db->insert_id();
      }
    }

    /*update new added data*/
    if (count($new_added) != 0) {
      $this->db->set('status',1);
      $this->db->where_in('id',$new_added);
      $this->db->update('sale_stock');
    }
   

    $this->db->select('group_concat(id) as ids,group_concat(stock_id) as stock_ids');
    $this->db->where_not_in('id',$updated_ids);
    $this->db->where('sales_invoice_id',$invoice_id);
    $result = $this->db->get($this->table_name_invoice_details)->row_array();

    $deleted_data=explode(',', $result['ids']);
    $stock_ids=explode(',', $result['stock_ids']);

    if (count($stock_ids) != 0) {
      $this->db->set('status',0);
      $this->db->where_in('id',$stock_ids);
      $this->db->update('sale_stock');
    }
    
    $this->db->where_in('id',$deleted_data);
    $this->db->delete($this->table_name_invoice_details);

  }

  public function send_to_dispatch(){
   if (!empty($_POST['id'])) {
     $this->db->set('status',1);
     $this->db->where('id',$_POST['id']);
    if($this->db->update('sales_invoice')){
       return get_successMsg();
     }else{
       return get_errorMsg();
     }
   }else{
    return get_errorMsg();
   }
  }

  public function temp_chitti_no(){
    $this->db->select('MAX(id) as max_id');
    $result = $this->db->get($this->table_name)->row_array();
    return $result['max_id']+1;
  }


}    