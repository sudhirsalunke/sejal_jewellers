<?php
class Prepared_order_model extends CI_Model {

  function __construct() {
      $this->table_name = "manufacturing_order";
      $this->table_name_mapping="manufacturing_order_mapping";
      parent::__construct();
  }

  public function validatepostdata(){
    $data=array();
    $old_post=$_POST;
    $tr = $_POST['product']['cnt'];
    $weight_array =$_POST['product']['weight'];
    $pc_array =$_POST['product']['parent_category'];
   foreach ($_POST['product']['quantity'] as $key => $value) {
        $_POST['product']['cnt']=@$tr[$key];
        $_POST['product']['weight']=@$weight_array[$key];
        $_POST['product']['quantity']=@$value;
        $_POST['product']['parent_category']=@$pc_array[$key];
    $this->form_validation->set_rules($this->config->item('sales_prepare_order', 'admin_validationrules'));

     if ($this->form_validation->run() == FALSE) {
          $data['order_name'] =strip_tags(form_error('product[order_name]'));
          $data['department_id'] =strip_tags(form_error('product[department_id]'));
          $data['weight_'.@$tr[$key]] =strip_tags(form_error('product[weight]'));
          $data['quantity_'.@$tr[$key]] =strip_tags(form_error('product[quantity]'));
          $data['parent_category_'.@$tr[$key]] =strip_tags(form_error('product[parent_category]'));
      } 
   }
   $_POST=$old_post;
   return $data;
  	
  }
  public function store(){
    
  	$postdata = $this->input->post('product');
    /*create_order*/
    $this->db->insert($this->table_name,array('department_id' => $postdata['department_id'],
                                              'order_name' => $postdata['order_name'],
                                              'is_sale' => 1,
                                              'created_at' => date('Y-m-d H:i:s'),
                                              'updated_at' =>date('Y-m-d H:i:s'),
                                              ));
    /**/
    $order_id = $this->db->insert_id();
    $insert_array=array();
    foreach ($postdata['parent_category'] as $key => $value) {
        $insert_array[$key]['manufacturing_order_id']=$order_id;
        $insert_array[$key]['parent_category_id']=$value;
        $insert_array[$key]['weight_range_id']=$postdata['weight'][$key];
        $insert_array[$key]['quantity']=$postdata['quantity'][$key];
        $insert_array[$key]['is_sale']=1;
        $insert_array[$key]['created_at']=date('Y-m-d H:i:s');
        $insert_array[$key]['updated_at']=date('Y-m-d H:i:s');
         
      
    }
  
  if($this->db->insert_batch($this->table_name_mapping,$insert_array)){
  		return get_successMsg();
  	}else{
  		return get_errorMsg();
  	}
  }
  public function get($filter_status='',$status='',$params='',$search='',$limit=''){
    $this->db->select('mo.*,d.name as d_name,SUM(mop.quantity) as total_qty,count(Distinct(mop.parent_category_id)) as categories');
    $this->db->from($this->table_name.' mo');
    $this->db->join('departments d','mo.department_id=d.id');
    $this->db->join($this->table_name_mapping.' mop','mo.id=mop.manufacturing_order_id','left');
    $this->db->where('mo.is_sale',1);
   /* if(!empty($search)){
        $this->db->like('mo.id',$search);
    }*/
    if (@$_GET['status'] !="sent") {
      $this->db->where('mo.sent',0);
    }else{
      $this->db->where('mo.sent',1);
    }
     if(isset($params['columns']) && !empty($params['columns'])){
      $filter_input=$params['columns'];
      $table_col_name="sales_prepared_order_table";
      $this->get_filter_value($filter_input,$table_col_name);
    }

    if($limit == true){
    	$this->db->limit($params['length'],$params['start']);
    }

    $this->db->group_by('mo.id');

    $this->db->order_by('mo.id','DESC');
    $result = $this->db->get()->result_array();
   // echo $this->db->last_query(); print_r($result);exit;
    return $result;
  }
  private function get_filter_value($filter_input,$table_col_name){
    $column_name=array();  
    $filter_column_name=filter_column_name($table_col_name);
    $sql='';
    $i=0;
     
     //print_r($filter_input);die;
    foreach ($filter_input as $key => $search_value){
       $column_name=$filter_column_name;
       //print_r($search_value['search']['value']);
        if(!empty($search_value['search']['value'])){
          if($i != 0){
            $sql.=' AND  ';
          }
            $sql.=''.$column_name[$key].' like "%'.$search_value['search']['value'].'%" ';
            $i++;
        

         }   
    }

    if(!empty($sql)){  
      $this->db->where($sql);  
    } 
   
       
  }

  public function find_by_encrypted_id($encrypted_id){
   $this->db->where('encrypted_id',$encrypted_id);
   $result = $this->db->get($this->table_name)->row_array();
   return $result;
  }
  public function find($id){
  	$this->db->where('id',$id);
  	$result = $this->db->get($this->table_name)->row_array();
  	return $result;
  }
  public function order_details($id){
    $this->db->where('manufacturing_order_id',$id);
    $result = $this->db->get($this->table_name_mapping)->result_array();
    return $result;
  }

    public function show_page_data($id){
    $this->db->select('d.name as department_name,concat(wr.from_weight," - ",wr.to_weight) as weight_range,pc.name as parent_category_name,mop.quantity,mo.order_name');
    $this->db->where('manufacturing_order_id',$id);
    $this->db->from($this->table_name_mapping.' mop');
    $this->db->join($this->table_name.' mo','mop.manufacturing_order_id=mo.id','left');
    $this->db->join('departments d','mo.department_id=d.id','left');
    $this->db->join('parent_category pc','mop.parent_category_id=pc.id','left');
    $this->db->join('weights wr','mop.weight_range_id=wr.id','left');
    $result = $this->db->get()->result_array();
    return $result;
  }

  public function update(){
    $postdata = $this->input->post('product');
    /*create_order*/
    $this->db->where('id',$_POST['order_id']);
    $this->db->update($this->table_name,array('department_id' => $postdata['department_id'],
                                              'updated_at' =>date('Y-m-d H:i:s'),
                                              ));
    /**/
    $order_id = $_POST['order_id'];
    $insert_array=array();
    $added_updted_ids=array();
    foreach ($postdata['parent_category'] as $key => $value) {
        
        $insert_array['parent_category_id']=$value;
        $insert_array['weight_range_id']=$postdata['weight'][$key];
        $insert_array['quantity']=$postdata['quantity'][$key];
        $insert_array['updated_at']=date('Y-m-d H:i:s');

        if (isset($postdata['id'][$key]) && !empty($postdata['id'][$key])) {
           $added_updted_ids[]=$postdata['id'][$key];

           $this->db->where('id',$postdata['id'][$key]);
           $this->db->update($this->table_name_mapping,$insert_array);

        }else{
          $insert_array['manufacturing_order_id']=$order_id;
          $insert_array['is_sale']=1;
          $insert_array['created_at']=date('Y-m-d H:i:s');
          $this->db->insert($this->table_name_mapping,$insert_array);

          $added_updted_ids[]=$this->db->insert_id();
        }
         
      
    }
    $this->db->where('manufacturing_order_id',$order_id);
    $this->db->where_not_in('id',$added_updted_ids);
    $this->db->delete($this->table_name_mapping);

      return get_successMsg(1);
    
  }
  public function delete($id){
  	$this->db->where('encrypted_id',$id);
  	if($this->db->delete($this->table_name)){
  		return get_successMsg();
  	}else{
  		return get_errorMsg();
  	}
  }

 //  public function check_pc_used($id){
 //      $this->db->where('',$id);
 //      $this->db->from('');
 //     return $this->db->count_all_results();
 // }

 public function send_to_manufacture($id,$sent_status){
   $this->db->where('id',$id);
   if ($this->db->update($this->table_name,array('sent'=>$sent_status,'order_date'=>date('Y-m-d')))){
     return get_successMsg();
    }else{
      return get_errorMsg();
    }
 }

 public function get_order_id(){
  $this->db->select_max('id', 'order_id');
  $query = $this->db->get($this->table_name)->row_array();
  return $query['order_id']+1;
 }
}    