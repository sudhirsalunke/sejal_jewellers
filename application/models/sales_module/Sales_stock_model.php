<?php
class Sales_stock_model extends CI_Model {

  function __construct() {
      $this->table_name = "sale_stock";
      $this->dbs=$this->load->database('account',true);
      parent::__construct();
  }

  // public function validatepostdata(){
   
  // 	$this->form_validation->set_rules($this->config->item('sales_prepare_order', 'admin_validationrules'));
  //    if ($this->form_validation->run() == FALSE) {       	
  //         return FALSE;
  //     } else {
  //         return TRUE;
  //     }
  // }

  public function validatepostdata(){
    $this->form_validation->set_rules($this->config->item('sales_stock','admin_validationrules'));
     if ($this->form_validation->run() == FALSE) {        
          return FALSE;
      } else {
          return TRUE;
      }
  }
  public function store(){
    $postdata = $this->input->post('drp_id');
    //print_r($_POST);die;
    $product_code = $_POST['product_code'];
    //$postdata =$_POST;
    $insert_array=array();

    foreach ($postdata as $key => $value) {
        $insert_array[$key]['drp_id']=$value;
        $insert_array[$key]['product_code']=$product_code[$key];
        $insert_array[$key]['approve_date']=date('Y-m-d H:i:s'); 
        $insert_array[$key]['created_at']=date('Y-m-d H:i:s');
        $insert_array[$key]['updated_at']=date('Y-m-d H:i:s'); 
    }
    //print_r($insert_array);die;
      // $postdata = $this->input->post();
      // $postdata['created_at']=date('Y-m-d H:i:s');
      // $postdata['updated_at']=date('Y-m-d H:i:s'); 
  //if($this->db->insert($this->table_name,$postdata)){
    if($this->db->insert_batch($this->table_name,$insert_array)){
    $this->db->set('status',2);
    $this->db->set('approve_date',date('Y-m-d H:i:s'));
    //$this->db->where('id',$postdata['drp_id']);
    $this->db->where_in('id',$postdata);
    $this->db->update('department_ready_product');
    
      return get_successMsg();
    }else{
      return get_errorMsg();
    }
  }

 public function store_all(){
  	//$postdata = $this->input->post('drp_id');
    $location_id=$this->session->userdata('location_id');
    $voucher_no = $_POST['voucher_no'];
    //$postdata =$_POST;
    //print_r($product_code);exit;
    $department_id=$this->session->userdata('department_id');
    $insert_array=array();
    $this->db->select('id,product_code');
     $this->db->where('status',1);
    $this->db->where_in('voucher_no',$voucher_no);
    if(!empty($department_id)){
      $this->db->where('department_id',$department_id);
     }
    $result= $this->db->get('department_ready_product')->result_array();
  //echo $this->db->last_query();print_r($result);exit;
    foreach ($result as $key => $value) {
        $insert_array[$key]['drp_id']=$value['id'];
        $insert_array[$key]['product_code']=$value['product_code'];
        $insert_array[$key]['approve_date']=date('Y-m-d H:i:s'); 
        $insert_array[$key]['created_at']=date('Y-m-d H:i:s');
        $insert_array[$key]['updated_at']=date('Y-m-d H:i:s'); 
        $drp_ids[] = $value['id'];
    }

      // $postdata = $this->input->post();
      // $postdata['created_at']=date('Y-m-d H:i:s');
      // $postdata['updated_at']=date('Y-m-d H:i:s'); 
  //if($this->db->insert($this->table_name,$postdata)){
    if($this->db->insert_batch($this->table_name,$insert_array)){

    $this->db->set('status',2);
    $this->db->set('approve_date',date('Y-m-d H:i:s'));
    //$this->db->where('id',$postdata['drp_id']);
    $this->db->where_in('id',$drp_ids);
    $this->db->update('department_ready_product');
    
  		return get_successMsg();
  	}else{
  		return get_errorMsg();
  	}
  }


   public function add_stock(){
    $insert_array=array();
    $postdata = $this->input->post('sales_stock');
    $postdata['product']=$postdata['parent_category_id'];
    $postdata['status']='1';
    $postdata['product_from']='5';
    $postdata['created_at']=date('Y-m-d H:i:s');
    $postdata['updated_at']=date('Y-m-d H:i:s'); 
    unset($postdata['category_id']);
    unset($postdata['parent_category_id']);
    //print_r($postdata);die;
    if ($this->db->insert('department_ready_product',$postdata)) {                
      $insert_array['drp_id']=$this->db->insert_id(); 
      $insert_array['created_at']=date('Y-m-d H:i:s');
      $insert_array['updated_at']=date('Y-m-d H:i:s'); 
      $this->db->insert($this->table_name, $insert_array);         
      return get_successMsg();
    }else{
      return get_errorMsg();
    }
  }
  public function get($filter_status='',$params='',$search='',$limit='',$department_id='',$location_id=''){
/*    $this->db->select('ss.*,drp.quantity,pc.name as sub_category,drp.product_code,drp.chitti_no,tag.net_wt,km.code as karigar_code,km.name as karigar_name,drp.gr_wt,DATE_FORMAT(ss.created_at,"%d-%m-%Y '."||".' %T") as created_at');*/
$this->db->select('ss.*,drp.quantity,drp.product_code,drp.chitti_no,drp.net_wt,drp.gr_wt,DATE_FORMAT(ss.created_at,"%d-%m-%Y '."||".' %T") as created_at,drp.product,drp.department_id,DATE_FORMAT(ss.approve_date,"%d-%m-%Y '."||".' %T") as approve_date,drp.voucher_no,lm.name as location_name,drp.location_id');
    $this->db->from($this->table_name.' ss');
    $this->db->join('department_ready_product drp','ss.drp_id=drp.id');
    $this->db->join('location_master lm','lm.id=.drp.location_id');
/*    $this->db->join('make_set_quantity_relation msqr','drp.msqr_id=msqr.id','left');
    $this->db->join('tag_products tag','msqr.tag_id=tag.id','left');
    $this->db->join('karigar_master km','tag.karigar_id=km.id');
    $this->db->join('parent_category pc','tag.sub_category_id=pc.id','left');*/
    $this->db->where('ss.status',0);
    $this->db->where('drp.status',2);

   /* if(!empty($search)){
        $this->db->like('drp.chitti_no',$search);
    }*/
    //print_r($department_id);die;
    if(!empty($department_id)){
      $this->db->where('drp.department_id',$department_id);
    }
    
    if(!empty($location_id)){
      $this->db->where('drp.location_id',$location_id);
    }

    if(isset($params['columns']) && !empty($params['columns'])){
      $filter_input=$params['columns'];
      $table_col_name="sales_stock_table";
      $this->get_filter_value($filter_input,$table_col_name);
    }
    if($limit == true){     
      $this->db->limit($params['length'],$params['start']);
      $result = $this->db->get()->result_array();
   //echo $this->db->last_query(); print_r($result);exit;
    }else{
      $row_array = $this->db->get()->result_array();
      $result = count($row_array);
    }
    return $result;
  }
  private function get_filter_value($filter_input,$table_col_name){
    $column_name=array();  
    $filter_column_name=filter_column_name($table_col_name);
    $sql='';
    $i=0;
     
     //print_r($filter_input);die;
    foreach ($filter_input as $key => $search_value){
       $column_name=$filter_column_name;
       //print_r($search_value['search']['value']);
        if(!empty($search_value['search']['value'])){
          if($i != 0){
            $sql.=' AND  ';
          }
            $sql.=''.$column_name[$key].' like "%'.$search_value['search']['value'].'%" ';
            $i++;
        

         }   
    }

    if(!empty($sql)){  
      $this->db->where($sql);  
    } 
       
  }

  public function find_by_encrypted_id($encrypted_id){
   $this->db->where('encrypted_id',$encrypted_id);
   $result = $this->db->get($this->table_name)->row_array();
   return $result;
  }
  public function find($id){
  	$this->db->where('id',$id);
  	$result = $this->db->get($this->table_name)->row_array();
  	return $result;
  }

  public function update(){
    $postdata =$this->input->post('hallmarking_list');
    $update_array = $postdata;
  	// $update_array = array(
  	// 	'name' => $postdata['name'],
   //    'hc_id' => $postdata['parent_category'],
   //    'touch' => $postdata['touch'],
  	// 	'updated_at' => date('Y-m-d H:i:s')
  	// 	);
  	$this->db->where('encrypted_id',$_POST['encrypted_id']);
  	if($this->db->update($this->table_name,$update_array)){
  		return get_successMsg(1);
  	}else{
  		return get_errorMsg();
  	}
  }

  public function get_item_details(){
      $this->db->select('ss.id as stock_id,drp.*,"success" as result');
      $this->db->where('drp.product_code',$_POST['product_code']);
      $this->db->where('ss.status',0);
      $this->db->from($this->table_name.' ss');
      $this->db->join('department_ready_product drp','ss.drp_id=drp.id','left');
      return $this->db->get()->row_array();
  }

  public function get_stock_product(){
    $product_code = @$_GET['term'];
    if (!empty($product_code)) {
      $this->db->select('drp.product_code as value');
      $this->db->like('drp.product_code',$product_code);
      $this->db->from($this->table_name.' ss');
      $this->db->where('ss.status',0);
      $this->db->join('department_ready_product drp','ss.drp_id=drp.id','left');
      return $this->db->get()->result_array();
    }else{
      return array();
    }
  }

    public function sales_to_manufacture(){ /*Work In Progress*/
    
    $postdata =$this->input->post('drp_id');
    // $this->db->set('status',3);
    // $this->db->where('id',$_POST['id']);
    // $this->db->where_in('id',$postdata);
    /**/

     if (!empty($postdata)) {
      $department_id =$_SESSION['department_id'];
      //$this->session->set_userdata('print_tag_product_session',$data['tag_product']);
      if($department_id!='2' && $department_id !='3' && $department_id !='6' && $department_id !='10'){
      $this->db->insert('make_set',array('created_at' => date('Y-m-d H:i:s'),'updated_at' => date('Y-m-d H:i:s') ));
      $make_set_id =$this->db->insert_id();
      }
       $insert_array = array();
      foreach ($postdata as $key => $value) {

          $old_data =$this->get_drp_by_id($value);/*IN prograess*/
          $old_chitti_no =$old_data['chitti_no'];
          unset($old_data['id']);
          if($department_id!='2' && $department_id !='3' && $department_id !='6' && $department_id !='10'){
          $old_data['ms_id']=$make_set_id;
          $old_data['chitti_no'] = $make_set_id;
          }else{
            $old_data['chitti_no'] = $old_chitti_no;
          } 
          $old_data['rejected_chitti_no']=$old_chitti_no;
          $old_data['quantity']=$old_data['quantity'];
          $old_data['created_at'] = date('Y-m-d H:i:s');
          $old_data['updated_at'] = date('Y-m-d H:i:s');
          $old_data['rejected_date']=date('Y-m-d H:i:s');
          $old_data['status'] ='3';
          $insert_array[]=$old_data;
        
      }
        if($this->db->insert_batch('department_ready_product',$insert_array)){
           $this->db->set('status','6');/*reject */
           $this->db->where_in('id',$postdata);
           $this->db->update('department_ready_product');
          return get_successMsg();
        }else{
          return get_errorMsg();
        }
      
    }

    /**/
    foreach ($postdata as $key => $value) {
      $old_data = $this->get_drp_by_id($value);

    }
    if($this->db->update($this->table_name)){
      //echo $this->db->last_query();exit;
      return get_successMsg();
    }else{
      return get_errorMsg();
    }
  }

public function sales_to_manufacture_all(){ /*Work In Progress*/
    
    //$postdata =$this->input->post('drp_id');
    // $this->db->set('status',3);
    // $this->db->where('id',$_POST['id']);
    // $this->db->where_in('id',$postdata);
    /**/
    $voucher_no = $_POST['voucher_no'];
  // print_r($_POST);exit;
 
     if (!empty($voucher_no)) {
      $department_id =$_SESSION['department_id'];
      //print_r($department_id);
      //$this->session->set_userdata('print_tag_product_session',$data['tag_product']);
      if($department_id!='2' && $department_id !='3' && $department_id !='6' && $department_id !='10' && $department_id !='11'){
      $this->db->insert('make_set',array('created_at' => date('Y-m-d H:i:s'),'updated_at' => date('Y-m-d H:i:s') ));
      $make_set_id =$this->db->insert_id();
      }
      //print_r($voucher_no);die;
       $insert_array = array();
        $this->db->select('*');
        if(@$_POST['type']=="stock"){
          $status="2";
        }else if(@$_POST['type']=="ac_sales_return_voucher"){
          $status="8";
        }else{
          $status="1";
        }
        $this->db->where('status',$status);
        $this->db->where_in('voucher_no',$voucher_no);
        if(!empty($department_id)){
         $this->db->where('department_id',$department_id);
        }   if(!empty($_POST['product_code'])){
         $this->db->where('product_code',$_POST['product_code']);
        }
        $result= $this->db->get('department_ready_product')->result_array();
 // echo $this->db->last_query();print_r($result);exit;
      foreach ($result as $key => $value) {
          $old_data = $value;
         // $old_data =$this->get_drp_by_id($value);/*IN prograess*/
          $old_chitti_no =$old_data['chitti_no'];
            $drp_ids[]=$old_data['id'];
          unset($old_data['id']);
          if($department_id!='2' && $department_id !='3' && $department_id !='6' && $department_id !='10'&& $department_id !='11'){
          $old_data['ms_id']=$make_set_id;
          $old_data['chitti_no'] = $make_set_id;
          }else{
            $old_data['chitti_no'] = $old_chitti_no;
          } 
          $old_data['rejected_chitti_no']=$old_chitti_no;
          $old_data['quantity']=$old_data['quantity'];
          $old_data['created_at'] = date('Y-m-d H:i:s');
          $old_data['updated_at'] = date('Y-m-d H:i:s');
          $old_data['rejected_date']=date('Y-m-d H:i:s');
          $old_data['status'] ='3';
          $insert_array[]=$old_data;
        
        
      }
      if($this->db->insert_batch('department_ready_product',$insert_array)){
           $this->db->set('status','6');
           $this->db->where_in('id',$drp_ids);
           $this->db->update('department_ready_product');
          return get_successMsg();
        }else{
          return get_errorMsg();
        }
      
    }

    /**/
    foreach ($result as $key => $value) {
     // $old_data = $this->get_drp_by_id($value);
      $old_data = $value;

    }
    if($this->db->update($this->table_name)){
      //echo $this->db->last_query();exit;
      return get_successMsg();
    }else{
      return get_errorMsg();
    }
  }

  public function stock_to_manufacture_all(){ /*Work In Progress*/
    
    //$postdata =$this->input->post('drp_id');
    // $this->db->set('status',3);
    // $this->db->where('id',$_POST['id']);
    // $this->db->where_in('id',$postdata);
    /**/
    $product_code = $_POST['product_code'];
  // print_r($_POST);exit;
 
     if (!empty($product_code)) {
      $department_id =$_SESSION['department_id'];
      //print_r($department_id);
      //$this->session->set_userdata('print_tag_product_session',$data['tag_product']);
      if($department_id!='2' && $department_id !='3' && $department_id !='6' && $department_id !='10' && $department_id !='11'){
      $this->db->insert('make_set',array('created_at' => date('Y-m-d H:i:s'),'updated_at' => date('Y-m-d H:i:s') ));
      $make_set_id =$this->db->insert_id();
      }
      //print_r($voucher_no);die;
       $insert_array = array();
        $this->db->select('*');
        if(@$_POST['type']=="stock"){
          $status="2";
        }else if(@$_POST['type']=="ac_sales_return_voucher"){
          $status="8";
        }else{
          $status="1";
        }
        $this->db->where('status',$status);
        
        if(!empty($department_id)){
         $this->db->where('department_id',$department_id);
        }   if(!empty($_POST['product_code'])){
         $this->db->where_in('product_code',$_POST['product_code']);
        }
        $result= $this->db->get('department_ready_product')->result_array();
 //echo $this->db->last_query();print_r($result);exit;
      foreach ($result as $key => $value) {
          $old_data = $value;
         // $old_data =$this->get_drp_by_id($value);/*IN prograess*/
          $old_chitti_no =$old_data['chitti_no'];
            $drp_ids[]=$old_data['id'];
          unset($old_data['id']);
          if($department_id!='2' && $department_id !='3' && $department_id !='6' && $department_id !='10'&& $department_id !='11'){
          $old_data['ms_id']=$make_set_id;
          $old_data['chitti_no'] = $make_set_id;
          }else{
            $old_data['chitti_no'] = $old_chitti_no;
          } 
          $old_data['rejected_chitti_no']=$old_chitti_no;
          $old_data['quantity']=$old_data['quantity'];
          $old_data['created_at'] = date('Y-m-d H:i:s');
          $old_data['updated_at'] = date('Y-m-d H:i:s');
          $old_data['rejected_date']=date('Y-m-d H:i:s');
          $old_data['status'] ='3';
          $insert_array[]=$old_data;
        
        
      }
      if($this->db->insert_batch('department_ready_product',$insert_array)){
           $this->db->set('status','6');
           $this->db->where_in('id',$drp_ids);
           if($this->db->update('department_ready_product')){
            $this->db->set('status',3);
            $this->db->set('updated_at',date('Y-m-d H:i:s'));
            $this->db->where_in('id',$drp_ids);
            $this->db->update($this->table_name);
           }
          return get_successMsg();
        }else{
          return get_errorMsg();
        }
      
    }

  }
  public function stock_to_transfer_all(){     
 
    $product_code = $_POST['product_code'];
     if (!empty($product_code)) {
            $this->db->set('status',1);
            if($_SESSION['location_id']=='1' || $_SESSION['location_id']=='0'){
            $this->db->set('location_id',2);
            }else{
            $this->db->set('location_id',1);
            }
            $this->db->set('updated_at',date('Y-m-d H:i:s'));
            $this->db->where_in('product_code',$product_code);
         if($this->db->update('department_ready_product')){
        // echo  $this->db->last_query();die;
            $this->db->set('status',4);
            $this->db->set('updated_at',date('Y-m-d H:i:s'));
            $this->db->where_in('product_code',$product_code);
            $this->db->delete($this->table_name);
          return get_successMsg();
        }else{
          return get_errorMsg();
        }
   //print_r($_POST);exit; 
      
    }

  }

  public function get_drp_by_id($id){
    return $this->db->get_where('department_ready_product',array('id'=>$id))->row_array();
  }
  public function get_product_name($department_id=''){
    $this->db->select('code,name');
    $this->db->from('category_master');
    $this->db->where('department_id',$department_id);
    $result = $this->db->get()->result_array();
    //echo $this->db->last_query();print_r($result);exit;
    return $result;
  }
  public function get_product_category(){
    // $this->db->select('pc.*,(select ifnull(count(id)+1,1) from tag_products where item_category_code = cm.code and pcs.code_name = sub_code) as max_count,pcs.code_name as code,cm.code');

    $this->db->select("pc.*,(select ifnull(count(id)+1,1) from tag_products where product_code like concat(sub_code,item_category_code,'%')) as max_count,pcs.code_name as code,cm.code");
    $this->db->from('parent_category pc');
    $this->db->join('parent_category_codes pcs','pcs.parent_category_id = pc.id');
    $this->db->join('category_master cm','cm.id = pc.category_id','left');
    $result = $this->db->get()->result_array();
    return $result;
  
    /*echo $this->db->last_query(); 
    echo "<pre>";
    print_r($result ); 
    echo "</pre>";
    die;*/

  }
  public function get_ac_product_category(){
    $this->dbs->select('id,name,percentage');
    $this->dbs->from('ac_product_category');
    $result = $this->dbs->get()->result_array();
    //echo $this->db->last_query();print_r($result);exit;
    return $result;
  }


}    