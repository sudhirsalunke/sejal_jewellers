<?php

class Dashboard_model extends CI_Model {

  function __construct() {
      parent::__construct();
  }
  public function get_approved_design($vendor){
  	$this->db->select('count(ap.id) as count');
  	$this->db->from('Approve_products ap');
  	$this->db->join('orders o','o.id=ap.order_id');
  	$this->db->where('o.corporate',$vendor);
  	$result = $this->db->get()->row_array();
  	return $result['count'];
  }
  public function shortlisted_design_to_be_sent(){
  	$this->db->select('count(id) as count');
  	$this->db->from('shortlisted_products');
  	$this->db->where('status','1');
  	$result = $this->db->get()->row_array();
  	return $result['count'];
  }
  public function sent_design_for_approval($vendor){
  	$this->db->select('count(sp.id) as count');
  	$this->db->from('shortlisted_products sp');
  	$this->db->join('orders o','o.id=sp.order_id');
  	$this->db->where('o.corporate',$vendor);
  	$this->db->where('sp.status','2');
  	$result = $this->db->get()->row_array();
  	return $result['count'];
  }
  public function rejected_designs($vendor){
  	$this->db->select('count(rp.id) as count');
  	$this->db->from('rejected_products rp');
  	$this->db->join('orders o','o.id=rp.order_id');
  	$this->db->where('o.corporate',$vendor);
  	$result = $this->db->get()->row_array();
  	return $result['count'];
  }
  public function get_design_ctn(){
    //print_r($_POST);die;
    $page_title=$_POST['page_title'];
    $this->db->select('count(id) as count');
    $this->db->from('product');
    //$this->db->where('status','0');
    if($page_title == 'DESIGNS WITH IMAGES'){
    $this->db->where('image is not null');
    }else{
        $this->db->where('image',null);
    }
    $result = $this->db->get()->row_array();
    return $result['count'];
  }
}  