<?php

class Dashboard_model extends CI_Model {

  function __construct() {
  	$this->table_name = "customer_orders";
    $this->karigar_assign_table='customer_order_assign';
    parent::__construct();
  }

  public function pending_to_be_received(){
  	$this->db->select('SUM(remaining_qty) as qty');
  	$result = $this->db->get_where('customer_order_assign',array('status'=>0))->row_array();
  	if (empty($result['qty'])) {
  		$result['qty'] = 0;
  	}
  	return $result['qty'];
  }

  public function received_from_karigar(){
  	$this->db->select('SUM(received_qty) as qty');
  	$result = $this->db->get_where('customer_order_received',array('status'=>0))->row_array();
  	if (empty($result['qty'])) {
  		$result['qty'] = 0;
  	}
  	return $result['qty'];
  }

  public function send_to_customer(){
  	$this->db->select('SUM(received_qty) as qty');
  	$result = $this->db->get_where('customer_order_received',array('status'=>1))->row_array();
  	if (empty($result['qty'])) {
  		$result['qty'] = 0;
  	}
  	return $result['qty'];
  }


 }//class
