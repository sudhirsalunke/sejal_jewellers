<?php

class Email_tracking_model extends CI_Model {

  public function get($params='',$search='',$limit='',$is_count,$type,$interval){ 
    if($limit == true){
    $this->db->select('co.id order_id,co.order_name,co.party_name,co.create_order_mail,name as karigar_name,co.order_date');
      }else{
     $this->db->select('count(co.id) as cont_record');
   }	  		
    $this->db->from('customer_orders co');
    $this->db->join('karigar_master km','km.id=co.karigar_id');
    //$this->db->where('status','2');  
    $this->db->where('create_order_mail','1');
     $filter=@$_GET['filter'];

    if($filter=='k_email' && $type=='1'){
      $table_col_name='karigar_email_tracking_tbl';
    }else{
      $table_col_name='customer_email_tracking_tbl';
    }
   if(isset($params['columns']) && !empty($params['columns'])){
      $filter_input=$params['columns'];
      $this->get_filter_value($filter_input,$table_col_name);
    }
/*
    if(!empty($search))
      {

        $this->db->where("(co.order_name LIKE '%$search%' OR co.party_name LIKE '%$search%' OR co.create_order_mail LIKE '%$search%' OR name LIKE '%$search%' )");
      }*/
    
    if($limit == true){
      $this->db->limit($params['length'],$params['start']);
      $result = $this->db->get()->result_array();
    }else{
      $row_array = $this->db->get()->row_array();
      $result = $row_array['cont_record'];
    }
    //echo $this->db->last_query();
    return $result;  
    
	}

  private function get_filter_value($filter_input,$table_col_name){
    $column_name=array();  
    $filter_column_name=filter_column_name($table_col_name);
    $sql='';
    $i=0;
     
     //print_r($filter_input);die;
    foreach ($filter_input as $key => $search_value){
       $column_name=$filter_column_name;
       //print_r($search_value['search']['value']);
        if(!empty($search_value['search']['value'])){
          if($i != 0){
            $sql.=' AND  ';
          }
            $sql.=''.$column_name[$key].' like "%'.$search_value['search']['value'].'%" ';
            $i++;
        

         }   
    }

    if(!empty($sql)){  
      $this->db->where($sql);  
    } 
   
       
  }

  
}