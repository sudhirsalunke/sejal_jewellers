<?php

class Pending_to_be_recieved_model extends CI_Model {

  function __construct() {
  	$this->table_name = "customer_order_assign";
    parent::__construct();
  }

  public function validatepostdata(){
    $old_post=$_POST;
    $data=array();
    if (empty($_POST['quantity'])) {
      return array('karigar_id'=>"Please select Karigar");
    }
    foreach ($_POST['quantity'] as $key => $value) {
      $_POST['test_data']['quantity']=$value;
      $_POST['test_data']['net_wt']=$_POST['net_weight'][$key];
      $_POST['test_data']['id']=$_POST['assign_order_id'][$key];
    $this->form_validation->set_rules($this->config->item('Pending_to_be_recieved_model','admin_validationrules'));
     if ($this->form_validation->run() == FALSE) {        
          $data['karigar_id']=strip_tags(form_error('karigar_id'));
          $data['quantity_'.$key]=strip_tags(form_error('test_data[quantity]'));
          $data['net_wt_'.$key]=strip_tags(form_error('test_data[net_wt]'));
      }
    }
    $_POST=$old_post;
    return $data;
  }

  public function get($params='',$search='',$limit=''){
    $this->db->select('SUM(coa.quantity) as assign_qty,SUM(coa.remaining_qty) as remaining_qty,k.name as karigar_name,coa.karigar_id');
    $this->db->from($this->table_name.' coa');
    $this->db->join('karigar_master k','coa.karigar_id=k.id');

    if(!empty($search)){
        $this->db->like('k.name',$search);
    }
    if($limit == true){
    	$this->db->limit($params['length'],$params['start']);
    }
    $this->db->where('coa.status',0);
    $this->db->group_by('coa.karigar_id');
    $result = $this->db->get()->result_array();
    return $result;
  }

  public function get_order_by_karigar(){
    if (empty($_POST['karigar_id'])) {
      $result = get_errorMsg();
    }else{
      $result = get_successMsg();
      $this->db->select('coa.*,co.order_name');
      $this->db->from($this->table_name.' coa');
      $this->db->join('customer_orders co','coa.order_id=co.id');
      $this->db->where('coa.karigar_id',$_POST['karigar_id']);
      $result['data'] =$this->db->get()->result_array();
    }

    return $result;
  }

  public function store(){
    $postdata = $this->input->post();
    $get_chitti_no= $this->get_chitti_no();
    $insert_array=array();
    foreach ($postdata['quantity'] as $key => $value) {
      $assign_order_id =$postdata['assign_order_id'][$key];
      $get_assign_details=$this->deduct_remaining_qty($assign_order_id,$value);

      $insert_array[$key]['chitti_no']=$get_chitti_no;
      $insert_array[$key]['received_qty']=$value;
      $insert_array[$key]['order_id']=$get_assign_details['order_id'];
      $insert_array[$key]['assign_id']=$assign_order_id;
      $insert_array[$key]['net_weight']=$postdata['net_weight'][$key];
      $insert_array[$key]['created_at']=$insert_array[$key]['updated_at']=date('Y-m-d H:i:s');
    }
    $this->db->insert_batch('customer_order_received',$insert_array);
    return get_successMsg();
  }

  public function deduct_remaining_qty($assign_order_id,$qty){
   $data = $this->db->get_where('customer_order_assign',array('id'=>$assign_order_id))->row_array();
   $new_qty =$data['remaining_qty']-$qty;
   $updated_array=array('remaining_qty'=>$new_qty);
     if($new_qty==0){
        $updated_array['status']=1;/*as received*/
     }
     $this->db->where('id',$assign_order_id);
     $this->db->update('customer_order_assign',$updated_array);
     return $data;

  }

   public function get_chitti_no(){
    $this->db->select_max('chitti_no');
    $query = $this->db->get('customer_order_received')->row_array();
    return $query['chitti_no']+1;
  }

}//class