<?php

class Recieved_products_model extends CI_Model {

  function __construct() {
  	$this->table_name = "customer_order_received";
    parent::__construct();
  }

  public function get($params='',$search='',$limit=''){
    $this->db->select('cor.*,k.name as karigar_name,co.order_name');
    $this->db->from($this->table_name.' cor');
    $this->db->join('customer_order_assign coa','cor.assign_id=coa.id');
    $this->db->join('customer_orders co','cor.order_id=co.id');
    $this->db->join('karigar_master k','coa.karigar_id=k.id');

    if(!empty($search)){
        $this->db->like('k.name',$search);
    }
    if($limit == true){
    	$this->db->limit($params['length'],$params['start']);
    }
    if (empty($_GET['status'])) {
      $this->db->where('cor.status',0);
    }else{
      $this->db->where('cor.status',1);/*sent*/
    }
    $result = $this->db->get()->result_array();
    return $result;
  }

  public function sent_to_customer($id){
    $this->db->set('status',1);
    $this->db->where('id',$id);
    if($this->db->update($this->table_name)){
      return get_successMsg();
    }else{
      return get_errorMsg();
    }
  }


}//class