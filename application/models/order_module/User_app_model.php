<?php

class User_app_model extends CI_Model {

  function __construct() {
  	$this->table_name = "customer";
    $this->karigar_assign_table='customer_order_assign';
    parent::__construct();
  }



  public function validatepostdata(){
  
    $this->form_validation->set_rules($this->config->item('app_user', 'admin_validationrules'));
     if ($this->form_validation->run() == FALSE) {        
          return FALSE;
      } else {
          return TRUE;
      }
  }

  public function get($params='',$search='',$limit=''){
    $this->db->select('*');
    $this->db->from($this->table_name);
    $this->db->where('added_by','app');
    $table_col_name = "user_app_tbl";
    if(isset($params['columns']) && !empty($params['columns'])){
      $filter_input=$params['columns'];      
    $this->get_filter_value($filter_input,$table_col_name);
    }
    if($limit == true){
      $this->db->order_by('id','desc');
      $this->db->limit($params['length'],$params['start']);
      $result = $this->db->get()->result_array();

    }else{
      $row_array = $this->db->get()->result_array();

      $result = count($row_array);
       
    }

    return $result;
  }

  private function get_filter_value($filter_input,$table_col_name){
    $column_name=array();  
    $filter_column_name=filter_column_name($table_col_name);
    $sql='';
    $i=0;
     
     //print_r($filter_input);die;
    foreach ($filter_input as $key => $search_value){
       $column_name=$filter_column_name;
       //print_r($search_value['search']['value']);
        if(!empty($search_value['search']['value'])){
          if($i != 0){
            $sql.=' AND  ';
          }
            $sql.=''.$column_name[$key].' like "%'.$search_value['search']['value'].'%" ';
            $i++;
        

         }   
    }

    if(!empty($sql)){  
      $this->db->where($sql);  
    } 
   
       
  }

public function store($data){
  $data = $data['user'];
  $data['name'] = $data['first_name'] ." ". $data['middle_name']." ".$data['last_name'];
  $data['created_at'] = date('Y-m-d H:i:s');
  unset($data['first_name']);
  unset($data['middle_name']);
  unset($data['last_name']);
  $encrption = $this->data_encryption->get_encrypted_id();      
  $num_str = sprintf("%06d", mt_rand(1, 999999));
  $this->db->select('max(id)+1 as last_id');
  $result = $this->db->get('customer')->row_array();
  $last_id =$result['last_id'];
  $data['client_code']='C'.$num_str.$last_id;
  $data['encrypted_password'] = uniqid("pass");;
  $data['encrypted_id']=$encrption;
  $data['customer_type_id']=2;
  $data['added_by']='app'; 
  $this->db->insert('customer',$data);
    $response['status']="success";
    $response['msg']="Register Successfully!!!";
    $response['data'] = "";    
    $this->password_link_send_email($data['email'],$data);
    return $response;
}

private function password_link_send_email($email,$data){       
        //$data['name']= $user['name'];
       // $data['encrypted_password']= $linkdata['encrypted_password'];
       // print_r($data);exit;
        $mail['to'] = $email;
        $from="akshay@ascratech.com";
        $from_name="Shilpi_jewlwers";
        $result =$this->email_lib->send($data,$email,'Login Details','order_module/user_app/password.php',$from,$from_name);
        return $result;
    }

  public function get_user_by_id($id){
    $this->db->select('id,name,email,mobile_no,company_name,client_code');
    $this->db->from('customer');
    $this->db->where('id',$id);
    return $this->db->get()->row_array();
  }


  public function delete($id){
  
    $this->db->where('id',$id);
    $this->db->delete('customer');
  }


  public function update_app_user($data){
   if(isset($data['first_name']) || isset($data['middle_name']) || isset($data['last_name'])){
    $data['name'] =  $data['first_name'] ." " .$data['middle_name']." ".$data['last_name'];
     unset($data['first_name']);
     unset($data['middle_name']);
     unset($data['last_name']);
   }
    $this->db->where('id',$data['id']);
    $this->db->update('customer',$data);
  } 

 
  
  
}//class
