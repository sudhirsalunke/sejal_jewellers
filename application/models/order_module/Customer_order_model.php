<?php
class Customer_order_model extends CI_Model {

    private $_voucher_type = "sales voucher";
    private $_voucher_type_p = "purchase voucher";
    private $melting = "92";
    
  function __construct() {
    $this->table_name = "customer_orders";
    $this->karigar_assign_table='customer_order_assign';
    $this->dbs=$this->load->database('account',true);
    parent::__construct();
  }

  public function validatepostdata(){
    //print_r($_POST);die;
 
    $this->form_validation->set_rules($this->config->item('customer_order','admin_validationrules'));
     if ($this->form_validation->run() == FALSE) {        
          return FALSE;
      } else {
          return TRUE;
      }
  }
  
  public function receive_order(){
    $data['status'] = 'success';
    if(empty(array_filter($_POST['orders']['net_wt'])) || empty(array_filter($_POST['orders']['gross_weight']))){
     echo json_encode(array('status'=>'failure1','error'=>'Please Enter at least one product net weight value'));die;
    
    }
     
    $data['error'] = array();
    $error3 = array();
      foreach ($_POST ['orders']['ids'] as $key => $value) {
        $less_than = true;
        $this->value = $value;         
           
        if(!empty($_POST['orders']['net_wt'][$key])){
                $_POST['net_wt'] = $_POST['orders']['net_wt'][$key];
                $_POST['gross_weight'] = $_POST['orders']['gross_weight'][$key];
                $_POST['quantity'] = $_POST['orders']['quantity'][$key];
                $_POST['category'] = $_POST['orders']['category'][$key];
             
                $this->form_validation->set_rules('quantity', 'Quantity', 'required|order_qnt_vald|greater_than_equal_to[0.1]');
                $this->form_validation->set_rules('net_wt', 'Net Wt', 'trim|numeric|required|greater_than_equal_to[0.1]]');
                $this->form_validation->set_rules('category', 'Category', 'trim|required');
           
            if($_POST['orders']['net_wt'][$key] > $_POST['orders']['gross_weight'][$key] ){
                 $less_than = false;
              
           }
         /*
            $this->form_validation->set_rules($this->config->item('receive_order_validation','admin_validationrules'));*/
           if ($this->form_validation->run() == FALSE || $less_than == false)  {        
              $data['status'] = 'failure';
              if($less_than == false){
                 $error3 = array('net_wt_'.$key => 'Net weight should be less than or equal to gross weight' );   
              }
              $error= $this->receive_order_msg($value);
              $data['error']= array_merge_recursive($data['error'],$error,$error3);
            }

      }
  }

      return $data;
  }

  private function receive_order_msg($key){
     $report=array(
      'net_wt_'.$key=>strip_tags(form_error('net_wt')),
      'quantity_'.$key=>strip_tags(form_error('qnt')),
      'category_'.$key=>strip_tags(form_error('category')),
      );
    return $report;
      }

  private  function get_suffix($department_name,$v_type){
      if($department_name=="Bombay" || $department_name=="Emerald" || $department_name=="Calcutta" || $department_name=="Trios" || $department_name="BOM" || $department_name="Chains" || $department_name="Kolkatta" || $department_name="other" || $department_name="Calcutta" || $department_name="CORPORATE ( RELIANCE )" || $department_name="CORPORATE ( CARATLANE )"){
            if($v_type=="sales"){ $suffix = "SV";  }else{  $suffix = "PV"; }
            return  $suffix;
        }else if($department_name=="Antique"){
          if($v_type=="sales"){ $suffix = "ANSV";  }else{  $suffix = "ANPV"; }
            return  $suffix;
        }else if($department_name=="Mangalsutra"){
          if($v_type=="sales"){ $suffix = "MSSV";  }else{  $suffix = "MSPV"; }
            return  $suffix;
        }else if($department_name=="Platinum"){
          if($v_type=="sales"){ $suffix = "PLSV";  }else{  $suffix = "PLPV"; }
            return  $suffix;
        }else if($department_name=="Bombay BG"){
          if($v_type=="sales"){ $suffix = "BBSV";  }else{  $suffix = "BBPV"; }
            return  $suffix;
        }else{
             if($v_type=="sales"){ $suffix = "SV";  }else{  $suffix = "PV"; }
            return  $suffix;
        }
  }

  public function change_status(){
    $response=get_errorMsg();
/* echo "<pre>";print_r($_POST['category_id']);
 die;*/
 //  $item_category_code=$this->Category_model->find($_POST['category']);
          // print_r($_POST);
          // insert_max_count($_POST['category_id'],$_POST['parent_category_code'],$_POST['sub_code'],$_POST['department_id']);die;
    if (empty($_POST['id']) || !empty($_POST['status'])) {

      $updated_array=array('status'=>$_POST['status']);
     $voucher_array=array();
     $insert_drp_array=array();
      if($_POST['delivery_date'] != $_POST['change_delivery_date']){
            $order_id=$_POST['id'];
            $this->db->set('change_delivery_date',date('Y-m-d', strtotime($_POST["change_delivery_date"])));
            $this->db->where('id',$order_id);
            $this->db->update($this->table_name); 
            //print_r($_POST);
            }
      if ($_POST['status']==3) {
            //print_r($department_ready_product_id);die;
            $category=$this->Category_model->find($_POST['category_id']);
            $department_ready_product_id=$this->insert_department_ready_product($_POST,$category);
            $insert_array['received_qty'] = $_POST['qnt'];
            $insert_array['drp_id'] = $department_ready_product_id;
            $insert_array['order_id'] = $_POST['id'];
            $insert_array['net_weight'] = $_POST['net_weight'];
            $insert_array['gross_weight'] = $_POST['gross_weight'];
            $insert_array['product_code'] = @$_POST['product_code'];            
            $insert_array['updated_at'] = date('Y-m-d H:i:s');
            $insert_array['created_at'] = date('Y-m-d H:i:s');
            $insert_array['karigar_rcpt_voucher_no'] = $_POST['karigar_rcpt_voucher_no'];
            $insert_array['received_from_karigar_mail'] = 0;
            $insert_array['status'] = $this->net_weight_status();
            //$this->db->insert('customer_order_received',$insert_array);
    
            if($this->db->insert('customer_order_received',$insert_array)){   
                $this->order_voucher_process($_POST,$category);
            }
            $result = $this->find($_POST['id']);
            $updated_array['remaining_qty']=$result['remaining_qty'] - $_POST['qnt'];
            //print_r($updated_array['remaining_qty']);die;
            if($updated_array['remaining_qty'] == 0){
              $updated_array['status'] = '3';
            }else{
              unset($updated_array['status']);
            }

            $updated_array['net_weight']=$_POST['net_weight'];
            $updated_array['voucher_no']='V-'.$_POST['id'];
            $updated_array['category_id']=$_POST['category_id'];
            $updated_array['receive_date']=date('Y-m-d H:i:s');
        
            }



            if ($_POST['status']==4) {
                $updated_array['actual_delivery_date']=date('Y-m-d H:i:s');
            }
             if ($_POST['status']==6) {
                $updated_array['updated_at']=date('Y-m-d H:i:s');
            }
            if($updated_array['status'] == 2){
              $updated_array['create_order_mail'] = 0;
              $updated_array['sent_to_karigar_date'] = date('Y-m-d', strtotime("+3 days"));
              $updated_array['inproduction_date'] = date('Y-m-d');

              $type ='send';
              $order_id=$_POST['id'];
              $data['order_details'][] = $this->find($order_id);
              $data['images'][] = $this->get_images($order_id);
              $path=$this->create_pdf($data,$order_id,$type);
              $path_clm['Karigar_pdf_path']=$path;
              $table='customer_orders';
              $column='id';
              $this->update_pdf_path($order_id,$path_clm,$table,$column);
              
            }
      $this->db->where('id',$_POST['id']);
      if($this->db->update($this->table_name,$updated_array)){
        $response = get_successMsg();
        $response['order_id'] =$_POST['id'];
        $_SESSION['order_id'][$_POST['id']] =$_POST['id'];
      }
      //$_SESSION['order_id'][$_POST['id']] =$_POST['id'];
    }
   return $response;
  }


public function backend_entery_data()
{

    $this->db->select('co.*,cor.received_qty,km.name km_name,d.name as department_name,c.name category_name,cor.id cor_id,cor.karigar_rcpt_voucher_no,cor.net_weight net_wt,cor.order_id,cor.gross_weight,co.department_id');
    $this->db->from('customer_order_received cor');
    $this->db->join('customer_orders co','co.id = cor.order_id');
    $this->db->join('category_master c','co.category_id=c.id');
    $this->db->join('karigar_master km','co.karigar_id=km.id');
    $this->db->join('departments d','co.department_id=d.id'); 
    $this->db->where('cor.status','3');
    $this->db->or_where('cor.status','1');
     $this->db->or_where('cor.status','2');
   
   $result_array = $this->db->get()->result_array();
   // get_max_code($value['department_id'],$value['parent_category_id'])
    //print_r($result_array);die;
   foreach ($result_array as $key => $value) {
     //print_r($value);
     $category=$this->Category_model->find($value['category_id']);
     $sub_code=$this->find_sub_code($value['parent_category_id']); 
     $max_code=$this->find_max_code($value['department_id'],$sub_code->code_name,$category['code']);
//echo $this->db->last_query(); 
     if(!empty($max_code->max_count)){
      $p_code=$max_code->max_count;
     }else{
      $p_code=1;
     }

     echo"<pre>";
     //print_r($p_code);
     print_r($sub_code->code_name.$category['code'].'-'.$max_code->max_count);
     echo"</pre>";
     // $category=$this->Category_model->find($value['category_id']);
     // $department_ready_product_id=$this->insert_department_ready_product($value,$category);
     // $this->order_voucher_process($value,$category,$department_ready_product_id);
   }die;
}
private function find_sub_code($name){
    $this->db->select('pcc.code_name');
    $this->db->from('parent_category pc');
    $this->db->where('pc.name',$name);
    $this->db->join('parent_category_codes pcc','pcc.parent_category_id = pc.id');
    return $this->db->get()->row();
}
private function find_max_code($department_id,$code_name,$category){
    $this->db->select("ifnull(max_count+1,1) as max_count");
    $this->db->from('product_code_max_count');
    $this->db->where('department_id',$department_id);
    $this->db->where('sub_code',$code_name);
    $this->db->where("product_code LIKE concat('%','".$code_name."' ,'".$category."','%')");   

    return $this->db->get()->row();
}

  /* accounting purches voucher function start*/
    private function order_voucher_process($postdata,$category){
        // $category=$this->Category_model->find($_POST['category_id']);
        // $department_ready_product_id=$this->insert_department_ready_product($postdata,$category);         
        $sales_purchase_voucher_id=$this->create_purches_voucher($postdata);                 

        if($postdata['department_id']=="1" || $postdata['department_id']=="11" || $postdata['department_id']=="8"   || $postdata['department_id']=="7" && !empty($postdata['product_code']))
        {
           $this->sales_purchase_item_details($postdata,$sales_purchase_voucher_id,$category,$this->_voucher_type_p);                          
        }                           
        $this->sales_purchase_details($postdata,$sales_purchase_voucher_id,$category,$this->_voucher_type_p);            

    }

    private function create_purches_voucher($postdata){

                        $get_suffix=$this->get_suffix($postdata['department_name'],'purchase');
                        $voucher_array['date'] = date('Y-m-d');                   
                        $voucher_number=create_voucher_number($voucher_array, $get_suffix, $this->_voucher_type_p);             
                        $voucher_array['company_id'] = 1;
                   
                        $voucher_array['voucher_number'] = $voucher_number;
                        $voucher_array['department_name'] = $postdata['department_name'];
                        $get_department_id=$this->get_department_id($postdata['department_name']);
                        $voucher_array['department_name_id'] = $get_department_id->department_id;
                        $voucher_array['transaction_type'] = 'Value';
                        $voucher_array['voucher_type'] = $this->_voucher_type_p;
                        $voucher_array['account_name'] = $postdata['customer_name'];
                        $this->customer_name_update(trim($postdata['customer_name']));
                        $voucher_array['gross_wt'] = $postdata['gross_weight'];
                       
                        $voucher_array['custom_order'] = 1;
                        
                        $voucher_array['net_wt'] = $postdata['net_weight'];
                        $voucher_array['fine_wt'] = ($postdata['net_weight']*$this->melting/100);                     
                        $voucher_array['updated_at'] = date('Y-m-d H:i:s');
                        $voucher_array['created_at'] = date('Y-m-d H:i:s');
                        $this->dbs->insert('ac_sales_purchase_voucher',$voucher_array);
                        $sales_purchase_voucher_id=$this->dbs->insert_id();
                        return $sales_purchase_voucher_id;


    }
   private function insert_department_ready_product($postdata,$category){
          $insert_drp_array = array(
              'chitti_no' =>"MNFREC".mt_rand(),   
              'quantity' =>$postdata['quantity'],    
              'gr_wt' =>$postdata['gross_weight'],                               
              'net_wt' =>$postdata['net_weight'],
              'product_code' =>@$postdata['product_code'],
              'product' =>$postdata['product'],
              'product_from'=>'10',
              'department_id'=>$postdata['department_id'],                       
              'karigar_id' => $postdata['karigar_id'],                  
              'item_category_code' =>$category['code'],              
              'status'=>2,
              'custom_order'=>1,
              'approve_date' =>date('Y-m-d'),
              'created_at' =>date('Y-m-d H:i:s'),
              );
            $this->db->insert('department_ready_product',$insert_drp_array);
             $department_ready_product_id=$this->db->insert_id();
             $this->insert_sales_stock($department_ready_product_id,$postdata);
               if($postdata['department_id']=="1" || $postdata['department_id']=="11" || $postdata['department_id']=="8"   || $postdata['department_id']=="7" && !empty($postdata['product_code']))
                {
                     $this->insert_tag_data($category,$postdata);
                     insert_max_count($postdata['category_id'],$postdata['parent_category_code'],$postdata['sub_code'],$postdata['department_id'],$category['name']);
                 }
            return $department_ready_product_id;
    }



    private function insert_sales_stock($department_ready_product_id,$postdata){
                   $insert_sales_array = array(
                                      'drp_id' =>$department_ready_product_id,   
                                      'product_code' =>@$postdata['product_code'],
                                      'approve_date' =>date('Y-m-d H:i:s'),                     
                                      'status'=>0,
                                      'created_at' =>date('Y-m-d H:i:s'),
                                      );
                          $this->db->insert('sale_stock',$insert_sales_array);

    }


    private function sales_purchase_item_details($postdata,$sales_purchase_voucher_id,$category,$voucher_type){
            $sales_purchase_item_details=array(
                            'company_id'=>1,
                            'sales_purchase_voucher_id'=>$sales_purchase_voucher_id,
                            'product'=>$postdata['product'],
                            'karigar_name'=> $postdata['karigar_name'],
                            'product'=>@$postdata['product_code'],
                            'gross_wt'=>$postdata['gross_weight'],
                            'net_wt'=>$postdata['net_weight'],
                            'transaction_type'=>'Value',
                            'category'=>$category['name'],
                            'category_id'=>$postdata['category_id'],
                            'voucher_type'=> $voucher_type,
                            'fine_wt'=>($postdata['net_weight']*$this->melting/100),
                            'melting'=>$this->melting,
                            'created_at'=>date('Y-m-d H:i:s'),
                            

                );                

              $this->dbs->insert('ac_sales_purchase_item_details',$sales_purchase_item_details); 

    }

private function sales_purchase_details($postdata,$sales_purchase_voucher_id,$category,$voucher_type){
                
         $sales_purchase_details=array(
                                    'company_id'=>1,
                                    'sales_purchase_voucher_id'=>$sales_purchase_voucher_id,
                                    'product'=>$postdata['product'],
                                    'karigar_name'=> $postdata['karigar_name'],
                                    'gross_wt'=>$postdata['gross_weight'],
                                    'net_wt'=>$postdata['net_weight'],
                                    'fine_wt'=>($postdata['net_weight']*$this->melting/100),
                                    'melting'=>$this->melting,
                                    'transaction_type'=>'Value',
                                    'voucher_type'=> $voucher_type,
                                    'category'=>$category['name'],
                                    'category_id'=>$postdata['category_id'],
                                    'created_at'=>date('Y-m-d H:i:s'),
                                    

                                    );  


                        $this->dbs->insert('ac_sales_purchase_details',$sales_purchase_details); 
                    
}

private function insert_tag_data($category,$postdata){
                $tag_array =array(
                        'item_category_code' =>$category['code'], 
                        'karigar_id' => $postdata['karigar_id'],  
                        'sub_code' => $postdata['sub_code'],  
                        'product_code' =>@$postdata['product_code'],
                        'status'=>6,
                        'department_id'=>$postdata['department_id'],
                        'quantity'=>1,
                        'sub_category_id'=>$postdata['sub_category_id'],


                  );                   
                      $this->db->insert('tag_products',$tag_array);

}
/* accounting purches voucher function end*/
  public function last_product_code($sub_code, $item_category_code, $qc_id = '') {
        $this->db->select('product_code');
        $this->db->from('tag_products');
        if ($qc_id != ''):
            $this->db->where('qc_id', $qc_id);
        endif;
        $this->db->where('sub_code', $sub_code);
        $this->db->where('item_category_code', $item_category_code);
        $this->db->order_by('product_code', 'desc');
        $this->db->limit(1, 0);
        $result = $this->db->get()->row_array();
        //echo $this->db->last_query();
        return $result;
    }
  private function get_department_id($name){
    $this->dbs->select('id as department_id');
    $this->dbs->from('ac_department');
    $this->dbs->where('name',$name);
    return $this->dbs->get()->row();

  }
    private function customer_name_update($name)
  {
    $this->dbs->select('name');
    $this->dbs->from('ac_account');
    $this->dbs->where('name',$name);
    $result=$this->dbs->get()->row_array();
    if(empty($result['name'])){      
      $ac_account['company_id'] = '1';
      $ac_account['name'] = $name;
      $ac_account['group_code'] = "Vyapari";
      $ac_account['created_at']=date('Y-m-d H:i:s');     
      $this->dbs->insert('ac_account',$ac_account);
      
    }
   } 
  public function get($params='',$search='',$limit=''){
    $this->db->select('co.*,co.party_name as customer_name,km.name as karigar_name,d.name as department_name,pcc.code_name as code ,pc.id as sub_category_id');
    $this->db->from($this->table_name.' co');
    $this->db->join('karigar_master km','co.karigar_id=km.id','left');
    $this->db->join('parent_category pc','pc.name=co.parent_category_id');
    $this->db->join('parent_category_codes pcc','pcc.parent_category_id=pc.id');
   // $this->db->join('customer c','co.customer_id=c.id');
    $this->db->join('departments d','d.id=co.department_id');
    
    $get_status=@$_GET['status'];
    $table_col_name='customer_order';
    if (empty($get_status)) {
      $this->db->where('status',1);
    }elseif ($get_status=="not_send") {
      $table_col_name='customer_order_not_send';
      $this->db->where('status',1);
    }elseif ($get_status=="pending") {
        $table_col_name='customer_order_pending';
      $this->db->where('status',2);

    }elseif ($get_status=="received") {
      $this->db->where('status',3);

    }elseif ($get_status=="sent") {
      $this->db->where('status',4);
      
    }elseif($get_status =='by_app'){
      $this->db->where('status',0);
      $this->db->where('added_by','app');

    }elseif($get_status =='cancelled'){
      $this->db->where('status',6);
    }elseif($get_status =='modified'){
      $this->db->where('status',5);
    }
    if(isset($params['columns']) && !empty($params['columns'])){
      $filter_input=$params['columns'];      
    $this->get_filter_value($filter_input,$table_col_name);
    }


    if($limit == true){
      if($get_status == "cancelled"){
        $this->db->order_by('updated_at','desc');
      }else{
        $this->db->order_by('co.id','desc');
      }
      
      $this->db->limit($params['length'],$params['start']);
      $result = $this->db->get()->result_array();
     // echo $this->db->last_query();print_r($result);exit;
    }else{
      $row_array = $this->db->get()->result_array();
      $result = count($row_array);
     /// echo $this->db->last_query();print_r($result);exit;
    }
    return $result;
  }
  private function get_filter_value($filter_input,$table_col_name){
    $column_name=array();  
    $filter_column_name=filter_column_name($table_col_name);
    $sql='';
    $i=0;
     
     //print_r($filter_input);die;
    foreach ($filter_input as $key => $search_value){
       $column_name=$filter_column_name;
       //print_r($search_value['search']['value']);
        if(!empty($search_value['search']['value'])){
          if($i != 0){
            $sql.=' AND  ';
          }
            $sql.=''.$column_name[$key].' like "%'.$search_value['search']['value'].'%" ';
            $i++;
        

         }   
    }

    if(!empty($sql)){  
      $this->db->where($sql);  
    } 
   
       
  }

  public function get_max_id(){
    $this->db->select_max('id');
    $query = $this->db->get($this->table_name)->row_array();
    return $query['id']+1;
  }

  public function find($order_id){
    //return $this->db->get_where($this->table_name,array('id'=>$order_id))->row_array();
   $this->db->select('co.*,d.name as department_name,a.username as prepared_by,co.weight_range_id as approx_weight,km.name as karigar_name,c.name category_name,cor.karigar_rcpt_voucher_no,km.email as k_email');
    $this->db->from($this->table_name.' co');
    $this->db->join('category_master c','co.category_id=c.id','left');
    $this->db->join('customer_order_received cor','cor.order_id=co.id','left');
    $this->db->join('departments d','co.department_id=d.id','left');
    $this->db->join('karigar_master km','co.karigar_id=km.id','left');
    //$this->db->join('weights w','co.weight_range_id=w.id');
    $this->db->join('admin a','co.prepared_by=a.id','left');
    $this->db->where('co.id',$order_id);
  
    return $this->db->get()->row_array();

  }

  public function find_old_order_data($order_id){

   $this->db->select('co.*,d.name as department_name,a.username as prepared_by,co.weight_range_id as approx_weight,km.name as karigar_name,c.name category_name,cor.karigar_rcpt_voucher_no,km.email as k_email,DATE_FORMAT(co.order_date,"%d-%m-%Y")  order_date , DATE_FORMAT(ifnull(co.change_delivery_date,co.delievery_date),"%d-%m-%Y") delievery_date, DATE_FORMAT(co.expected_date,"%d-%m-%Y")  expected_date ');
    $this->db->from($this->table_name.' co');
    $this->db->join('category_master c','co.category_id=c.id','left');
    $this->db->join('customer_order_received cor','cor.order_id=co.id','left');
    $this->db->join('departments d','co.department_id=d.id','left');
    $this->db->join('karigar_master km','co.karigar_id=km.id','left');
    //$this->db->join('weights w','co.weight_range_id=w.id');
    $this->db->join('admin a','co.prepared_by=a.id','left');
    $this->db->where('co.id',$order_id);
  
    return $this->db->get()->row_array();

  }

  public function find_assign_tbl($assign_id){
    //return $this->db->get_where($this->table_name,array('id'=>$order_id))->row_array();
    $this->db->select('coa.*,co.order_name,co.order_date,co.delievery_date,co.expected_date,co.purity,co.weight_tolerance,co.remark,co.img_path, coa.approx_weight,d.name as department_name,a.username as prepared_by,km.name as karigar_name,co.party_name,co.parent_category_id,co.weight_range_id');
    $this->db->from($this->karigar_assign_table.' coa');
    $this->db->join('customer_orders co','coa.order_id=co.id');
    $this->db->join('karigar_master km','coa.karigar_id=km.id');
    // $this->db->join('customer c','co.customer_id=c.id');
    // $this->db->join('parent_category pc','co.parent_category_id=pc.id');
    $this->db->join('departments d','co.department_id=d.id');
    // $this->db->join('weights w','co.weight_range_id=w.id');
    $this->db->join('admin a','co.prepared_by=a.id');
    $this->db->where('coa.id',$assign_id);
    return $this->db->get()->row_array();

  }

  public function get_customer(){
   return $this->db->get_where('customer',array('customer_type_id'=>2))->result_array();
  }
  
  public function store($post_array=''){

    if(!empty($post_array)){
      $postdata = $post_array;
      $karigar = $this->Karigar_model->get_karigar_by_code($postdata['k_code']);
      if(!empty($karigar['id'])){
        $postdata['karigar_id'] = $karigar['id'];
      }
    }else{
      $postdata = $this->input->post('customer_order');
    }
    $customer = array();
    $karigar = array();
    if(!empty($postdata['party_name'])){
      $customer_value=$this->customer_update($postdata);

    }else{
      $postdata['customer_id'] = 0;
    }
    
    $karigar_value=$this->karigar_update($postdata);
    $insert_data=$this->form_data($postdata);     
    $insert_data['status']=$karigar_value['status'];
    $insert_data['karigar_id']=$karigar_value['karigar_id'];
    $insert_data['customer_id']=$customer_value['customer_id'];
    $insert_data['create_order_mail']=0;
    if ($this->db->insert($this->table_name,$insert_data)) {                
      $response=get_successMsg();
      $response['order_status']=$karigar_value['status'];
      $response['order_id']=$this->db->insert_id(); 
      $this->upload_image($postdata);        
      return $response;
    }else{
      return get_errorMsg();
    }
  }

  public function update(){
    $status_id = $this->input->post('status');
    $postdata= $this->input->post('customer_order');   
    $postdata_id = $this->find_old_order_data($postdata['id']);  
    $order_result=array_diff($postdata,$postdata_id);
    $order_result['added_by']='web';
    $order_result['order_id']=$postdata['id'];
    $order_history=array();
    $order_history_value=order_history_update($order_result);
// print_r($order_result);die;
    $customer = array();
    $karigar = array();
   // $order_history_value=$this->order_history_update($postdata_id);
    if(!empty($postdata['party_name'])){
      $this->customer_update($postdata);
    }else{
      $postdata['customer_id'] = 0;
    }
    $karigar_value=$this->karigar_update($postdata);
    $insert_data=$this->form_data($postdata);
    $insert_data['karigar_id']=$karigar_value['karigar_id'];
    $insert_data['status']=$karigar_value['status']; 
    if($postdata['status']== 0 and $postdata['added_by'] == "app"){
       $insert_data['status'] =1;
       $insert_data['pending_date']=date('Y-m-d');
    }
    if($status_id == 2){
       $insert_data['status'] = 5;
     }
    $insert_data['order_history'] ='1';
     //print_r($insert_data);exit;
    $this->db->where('id',$insert_data['id']);
      if($this->db->update($this->table_name,$insert_data)){
        if($status_id == 2){
          $response=get_sendtoModifiedMsg();
         }else{
           $response=get_successMsg();
         }

      $response['order_status']=$karigar_value['status'];
      $response['order_id']=$insert_data['id'];
      return $response;
    }else{
      return get_errorMsg($postdata);
    }
  }

  

  private function customer_update($postdata)
  {    
    if(empty($postdata['customer_id'])){
      $customer['encrypted_id']=$this->data_encryption->get_encrypted_id('customer');
      $customer['name'] = $postdata['party_name'];
      $customer['mobile_no'] = $postdata['mobile_no'];
      $customer['email'] = $postdata['email_id'];
      $customer['company_name'] = $postdata['company_name'];
      $customer['created_at']=date('Y-m-d H:i:s');
      $customer['customer_type_id'] = '2';
      $this->db->insert('customer',$customer);
      $postdata['customer_id'] = $this->db->insert_id();
    }else{
      $customer['name'] = $postdata['party_name'];
      $customer['mobile_no'] = $postdata['mobile_no'];
      $customer['email'] = $postdata['email_id'];
      $customer['company_name'] = $postdata['company_name'];
      $customer['customer_type_id'] = '2';
      $customer['updated_at']=date('Y-m-d H:i:s');
      $this->db->where('id',$postdata['customer_id']);
      $this->db->update('customer',$customer);
    }
    return $postdata;
  }

  private function karigar_update($postdata)
  {
   
    if(empty($postdata['karigar_id'])){
      $karigar['encrypted_id']=$this->data_encryption->get_encrypted_id('karigar_master');
      $karigar['name'] = $postdata['k_name'];
      $karigar['mobile_no'] = $postdata['k_mob_no'];
      $karigar['email'] = $postdata['k_email'];
      $karigar['code'] = $postdata['k_code'];
      $karigar['created_at']=date('Y-m-d H:i:s');
      $karigar['customer_type_id'] = '1';
      $this->db->insert('karigar_master',$karigar);
      $postdata['karigar_id'] = $this->db->insert_id();

    }else{
      $karigar['name'] = $postdata['k_name'];
      $karigar['mobile_no'] = $postdata['k_mob_no'];
      $karigar['email'] = $postdata['k_email'];
      $karigar['customer_type_id'] = '1';
      $karigar['code'] = $postdata['k_code'];
      $karigar['updated_at']=date('Y-m-d H:i:s');
      $this->db->where('id',$postdata['karigar_id']);
      $this->db->update('karigar_master',$karigar);
      $postdata['karigar_id'] = $postdata['karigar_id'];
    }
    if (!empty($postdata['karigar_id'])) {
      $postdata['status']=1;
    }else{
      $postdata['status']=0;
    }
  
 //die;
    return $postdata;
  }

  private function upload_image($postdata)
  {

      if($_FILES['image']['name']){        
        foreach ($_FILES['image']['name'] as $key => $val )
        {
            if(!empty($val)){
            $val1 = true;
            $file_array['name'] = $val;
            $file_array['type'] = $_FILES['image']['type'][$key];
            $file_array['tmp_name'] = $_FILES['image']['tmp_name'][$key];
            $file_array['error'] = $_FILES['image']['error'][$key];
            $file_array['size'] = $_FILES['image']['size'][$key];
            $upload_img[$key] =uploadImage('order_module',$file_array);
            $uploadData[$key]['img_file_path'] ='order_module/'.$upload_img[$key]['file_name'];
            $uploadData[$key]['created_at'] = date("Y-m-d H:i:s");
            $uploadData[$key]['updated_at'] = date("Y-m-d H:i:s");         
            $uploadData[$key]['customer_id'] = $this->db->insert_id();
          }
      }
    }

    if(isset($val1) && $val1 == true){
      $this->db->insert_batch('customer_orders_img',$uploadData);
    }

    
    return $postdata;  
  }
  

  private function form_data($postdata)
  {
    $postdata['created_at']=date('Y-m-d H:i:s');
    $postdata['updated_at']=date('Y-m-d H:i:s');
    $postdata['delievery_date']=date('Y-m-d',strtotime($postdata['delievery_date']));
    $postdata['expected_date']=date('Y-m-d',strtotime($postdata['expected_date']));
    $postdata['order_date']=date('Y-m-d',strtotime($postdata['order_date']));
    $postdata['remaining_qty']=$postdata['quantity'];
    $postdata['prepared_by']=$this->session->userdata('user_id');
    unset($postdata['k_name']);
    unset($postdata['k_email']);
    unset($postdata['k_code']);
    unset($postdata['k_mob_no']);
    unset($postdata['delete_img']);
    return $postdata;
  } 
  public function assign_karigar(){
    $postdata=$this->input->post();
    $postdata['created_at']=$postdata['updated_at']=date('Y-m-d H:i:s');
    $postdata['remaining_qty']=$postdata['quantity'];

    if ($this->db->insert($this->karigar_assign_table,$postdata)){
      $last_id =$this->db->insert_id();
      $this->deduct_remaning_qty($postdata['order_id'],$postdata['quantity']);
      $response = get_successMsg();
      $response['assign_id']=y_encrypt($last_id);
      return $response;
    }else{
      return get_errorMsg();
    }
  }

  public function deduct_remaning_qty($order_id,$qty){
   $all_data = $this->find($order_id);
   $new_qty =$all_data['remaining_qty']-$qty;
   $updated_array=array('remaining_qty'=>$new_qty);

   if ($new_qty==0) {
    $updated_array['status']=1;
   }
   $this->db->where('id',$order_id);
   $this->db->update($this->table_name,$updated_array);
  }

  
  public function get_received_order($params='',$search='',$limit=''){
    $this->db->select('co.*,cor.received_qty,km.name km_name,d.name as department_name,c.name category_name,cor.id cor_id,cor.karigar_rcpt_voucher_no,cor.net_weight net_wt,cor.order_id,cor.gross_weight');
    $this->db->from('customer_order_received cor');
    $this->db->join('customer_orders co','co.id = cor.order_id');
    $this->db->join('category_master c','co.category_id=c.id');
    $this->db->join('karigar_master km','co.karigar_id=km.id');
    $this->db->join('departments d','co.department_id=d.id');
    if(isset($_GET['status']) && $_GET['status'] == 'sent'){
        $this->db->where('cor.status','3');
    }elseif(isset($_GET['status']) && $_GET['status'] == 'accepted'){
        $this->db->where('cor.status','1');
    }else{
      $this->db->where('cor.status','2');
    }
     if(isset($params['columns']) && !empty($params['columns'])){
      $filter_input=$params['columns'];      
      $table_col_name="customer_received_order";
    $this->get_filter_value($filter_input,$table_col_name);
    }
    /*if(!empty($search)){
      $this->db->where("(co.party_name LIKE '%$search%' OR km.name LIKE '%$search%' OR co.id LIKE '%$search%'OR co.order_name LIKE '%$search%' OR co.parent_category_id LIKE '%$search%')");
    }*/  
    if($limit == true){
      $this->db->order_by('co.id','desc');
      $this->db->limit($params['length'],$params['start']);
      $result = $this->db->get()->result_array();
      //echo $this->db->last_query();print_r($result);exit;
    }else{
      $row_array = $this->db->get()->result_array();
      $result = count($row_array);
    }
    return $result;
  }

  public function change_received_status(){
    $today = date('Y-m-d H:i:s');
    $customer_voucher_no = 'VC-'.strtotime(date('Y-m-d H:i:sa'));
    foreach ($_POST['order_id'] as $key => $value) {
      $result = $this->find_receive_order($value);
    //print_r( $result);die;
      if($result['status'] == 3){
        $this->db->set('status',4);
        $this->db->set('actual_delivery_date',$today);
        $this->db->where('id',$result['id']);
        $this->db->update('customer_orders');
      }
      $this->db->set('status','3');
      $this->db->set('deliver_to_customer_mail',0);
      $this->db->set('send_at',$today);
      $this->db->set('customer_voucher_no',$customer_voucher_no);
      $this->db->where('id',$value);
      if($this->db->update('customer_order_received')){   
             $this->order_sales_voucher_process($result);

                       
      }

      $response = get_successMsg();
      $response['order_id'] =$value;
      $_SESSION['send_order_id'][$value] = $value;
    }//die;
    return $response;
  }
  /*account sales voucher start */
    private function order_sales_voucher_process($result){
        $category=$this->Category_model->find($result['category_id']);
        $sales_purchase_voucher_id=$this->create_sales_voucher($result);
        $this->update_department_ready($result);
       $this->update_sale_stock($result);
        if($result['department_id']=="1" || $result['department_id']=="11" || $result['department_id']=="8"  || $result['department_id']=="7" && !empty($result['product_code']))
            {  
                $this->sales_purchase_item_details($result,$sales_purchase_voucher_id,$category,$this->_voucher_type);
            }                          
                $this->sales_purchase_details($result,$sales_purchase_voucher_id,$category,$this->_voucher_type); 

    }

    private function create_sales_voucher($result){

              $voucher_array['date'] = date('Y-m-d');

              $get_suffix=$this->get_suffix($result['department_name'],'sales');
              $voucher_number=create_voucher_number($voucher_array, $get_suffix, $this->_voucher_type);
              $voucher_array['company_id'] = 1;
              $voucher_array['voucher_number'] = $voucher_number;
              $voucher_array['department_name'] = $result['department_name'];
              $get_department_id=$this->get_department_id($result['department_name']);
              $voucher_array['department_name_id'] = $get_department_id->department_id;
              $voucher_array['transaction_type'] = 'Value';
              $voucher_array['voucher_type'] = $this->_voucher_type;
              $voucher_array['account_name'] = $result['party_name'];
              $this->customer_name_update(trim($result['party_name']));
              $voucher_array['gross_wt'] = $result['gross_weight'];
              $voucher_array['custom_order'] = 1;
              
              $voucher_array['net_wt'] = $result['net_weight'];
              $voucher_array['updated_at'] = date('Y-m-d H:i:s');
              $voucher_array['created_at'] = date('Y-m-d H:i:s');
              $this->dbs->insert('ac_sales_purchase_voucher',$voucher_array);
                         $sales_purchase_voucher_id=$this->dbs->insert_id();
              return $sales_purchase_voucher_id;

    }

    private function  update_department_ready($result){
        $this->db->set('status','7');  
        $this->db->set('sales_date',date('Y-m-d'));       
        $this->db->where('id',$result['drp_id']);
        $this->db->update('department_ready_product');

    }

    private function update_sale_stock($result){
        $this->db->set('status','2'); 
        $this->db->set('updated_at',date('Y-m-d H:i:s'));           
        $this->db->where('id',$result['drp_id']);
        $this->db->update('sale_stock');  
    }

  public function find_receive_order($id){
    $this->db->select('co.*,cor.received_qty,km.name karigar_name,d.name as department_name,c.name category_name,cor.id cor_id,cor.customer_voucher_no,cor.product_code,cor.gross_weight,co.category_id,cor.drp_id');
    $this->db->from('customer_order_received cor');
    $this->db->join('customer_orders co','co.id = cor.order_id');
    $this->db->join('category_master c','co.category_id=c.id');
    $this->db->join('karigar_master km','co.karigar_id=km.id');
    $this->db->join('departments d','co.department_id=d.id');
    $this->db->where('cor.id',$id);
    return $this->db->get()->row_array();
  }
  public function net_weight_status(){
    $this->db->select('weight_range_id,quantity,remaining_qty');
    $this->db->from('customer_orders');
    $this->db->where('id',$_POST['id']);
    $result = $this->db->get()->row_array();
    $single_qty_wt = $result['weight_range_id'] / $result['quantity'];
    $total_net_wt = $_POST['qnt'] * $single_qty_wt;
    $weight_range_id = $total_net_wt;
    $calc = $weight_range_id*15/100;
    $max_limit_weight =$weight_range_id+$calc;
    $low_limit_weight =$weight_range_id-$calc;
    if ($_POST['net_wt'] > $max_limit_weight || $_POST['net_wt'] < $low_limit_weight  ) {
        return '1';
    }else{
        return '2';
    }
  }
  public function get_images($order_id){
    $this->db->select('id,img_file_path');
    $this->db->from('customer_orders_img');
    $this->db->where('customer_id',$order_id);
    $result = $this->db->get()->result_array();
    return $result;
  }
  public function delete($id){
    $this->db->where('id',$id);
    if($this->db->delete($this->table_name)){
      return get_successMsg();
    }else{
      return get_errorMsg();
    }
  } 
    
  public function create_pdf($data,$order_id,$type){
    $html=$this->load->view('order_module/customer_order/'.$type, $data,true);
    makeDirectory('Customer_order_pdf/'.$order_id);
    $pdfFilePath = dirname(dirname(dirname(dirname(__FILE__))))."/uploads/Customer_order_pdf/".$order_id."/".$type.".pdf";
    $mpdf=new mPDF();
    $mpdf->WriteHTML($html);
    $mpdf->Output($pdfFilePath, "F"); 
    $pdfPath="Customer_order_pdf/".$order_id."/".$type.".pdf";
    return $pdfPath;
  } 
  public function update_customer_pdf_path($order_id,$path)
  {  
    $postdata=$this->input->post('customer_orders');
    $postdata['customer_pdf_path']=$path;
    $this->db->where('id',$order_id);
    $this->db->update('customer_orders',$postdata);
  }
    public function update_Karigar_pdf_path($order_id,$path)
  {  
    $postdata=$this->input->post('customer_orders');
    $postdata['Karigar_pdf_path']=$path;
    $this->db->where('id',$order_id);
    $this->db->update('customer_orders',$postdata);
  }
   public function update_recivd_pdf_path($order_id,$path)
  {  
    $postdata=$this->input->post();
    $postdata['Karigar_pdf_path']=$path;
    $this->db->where('karigar_rcpt_voucher_no',$order_id);
    $this->db->update('customer_order_received',$postdata);
  }

  public function update_delivery_pdf_path($order_id,$path)
  {  
    $postdata=$this->input->post();
    $postdata['delivery_pdf_path']=$path;
    $this->db->where('id',$order_id);
    $this->db->update('customer_orders',$postdata);
  }
   public function update_pdf_path($order_id,$path_clm,$table,$column)
  { 
    $this->db->where($column,$order_id);
    $this->db->update($table,$path_clm);
  }

  public function deleteimage($id,$order_id){
        /*unlink start*/
    $this->db->select('img_file_path');
    $this->db->from('customer_orders_img');
    $this->db->where('id',$id);
    $this->db->where('customer_id',$order_id);
    $result = $this->db->get()->result_array();
    $filename=$result[0]['img_file_path']; 
    makeDirectory('order_module');
    $imagePath=dirname(dirname(dirname(dirname(__FILE__))))."/uploads/".$filename; 
    //print_r($filename);die;        
    /*unlick end*/ 
    $this->db->where('id',$id);
    $this->db->where('customer_id',$order_id);
    if($this->db->delete('customer_orders_img')){
       if(file_exists($imagePath)){   
                  unlink($imagePath);
                }
      return get_successMsg();
    }else{
      return get_errorMsg();
    }
  } 

  public function update_image()
  {
    $image_id = $this->input->post('image_id');
    $customer_id = $_POST['customer_order']['id'];
    $file_array['name']=$_FILES['image']['name'][$image_id];
    $file_array['type']=$_FILES['image']['type'][$image_id];
    $file_array['tmp_name']=$_FILES['image']['tmp_name'][$image_id];
    $file_array['error']=$_FILES['image']['error'][$image_id];
    $file_array['size']=$_FILES['image']['size'][$image_id];
    $upload_img =uploadImage('order_module',$file_array);      
    $uploadData['img_file_path'] ='order_module/'.$upload_img['file_name'];        
    $uploadData['customer_id'] =$customer_id;     
   // print_r($_FILES);
    /*unlink start*/
    $this->db->select('img_file_path');
    $this->db->from('customer_orders_img');
    $this->db->where('id',$image_id);
    $result = $this->db->get()->result_array();
    // print_r($result);die;
    $filename=$result[0]['img_file_path']; 
    makeDirectory('order_module');
    $imagePath=dirname(dirname(dirname(dirname(__FILE__))))."/uploads/".$filename; 
    // print_r($uploadData);die;        
    /*unlick end*/ 
    if (in_array($image_id, array('new1','new2','new3'))){
      $uploadData['created_at'] = date("Y-m-d H:i:s");
       /*insert*/
            if($this->db->insert('customer_orders_img',$uploadData)){
            /*   if(file_exists($imagePath)){   
                  unlink($imagePath);
                }*/
                $response=get_successMsg();
                $response['up_image_id']=$this->db->insert_id();
                return $response;     
              }else{
              return get_errorMsg();
              }
      }else{ 
        $uploadData['updated_at'] = date("Y-m-d H:i:s"); 
           $this->db->where('id',$image_id);
        if($this->db->update('customer_orders_img',$uploadData)){
          //echo $this->db->last_query();die;
       if(file_exists($imagePath)){   
                  unlink($imagePath);
                }
              $response=get_successMsg();
              $response['up_image_id']=$image_id;
              return   $response;   
           }else{
        return get_errorMsg();
        }
    }

  }

 public function create_order_mail(){
    $this->db->select('co.*,km.email km_email ,pc.name as category_name');
    $this->db->from('customer_orders co');
    $this->db->join('karigar_master km','co.karigar_id=km.id');
    $this->db->join('parent_category pc','co.parent_category_id=pc.name');
    $this->db->where('status',2);
    $this->db->where('create_order_mail',0);
    $this->db->limit(10);
    $result = $this->db->get()->result_array();
    $from_name ='sejal jewellers PVT LTD';
    if(!empty($result)){
      foreach ($result as $key => $value) {
        $data['order_details'] = $value;
       
        if(!empty($data['order_details']['email_id'])){
        //  print_r('user_email');exit;
          $view ='order_module/customer_order/email_templates/customer_greeting';
          $subject ='GREETINGS FROM SHILPI TEAM ORDER No C-'.$data['order_details']['id'].' RECEIVED';
          $this->email_lib->send($data,$data['order_details']['email_id'],$subject,$view,'order@shilpijewels.com',$from_name,dirname(dirname(dirname(dirname(__FILE__)))) . "/uploads/" .$data['order_details']['Karigar_pdf_path'],'karigar_issue.pdf');
        }
        if(!empty($data['order_details']['km_email'])){
          $view ='order_module/customer_order/email_templates/karigar_issue';
          $subject ='GREETINGS FROM SHILPI TEAM ORDER No K-'.$data['order_details']['id'].' RECEIVED';
          $this->email_lib->send($data,$data['order_details']['km_email'],$subject,$view,'order@shilpijewels.com',$from_name,dirname(dirname(dirname(dirname(__FILE__)))) . "/uploads/" .$data['order_details']['Karigar_pdf_path'],'karigar_issue.pdf');
        }
        $this->db->where('id',$value['id']);
        $this->db->set('create_order_mail',1);
        $this->db->update('customer_orders');
      }  
    }
  }
  public function karigar_received_mail(){
    $from_name ='sejal jewellers PVT LTD';
    $this->db->select('cor.*,km.email  km_email ,cm.name as category_name');
    $this->db->from('customer_order_received cor');
    $this->db->join('customer_orders co','co.id = cor.order_id');
    $this->db->join('karigar_master km','co.karigar_id=km.id');
    $this->db->join('parent_category cm','co.parent_category_id=cm.name');
    $this->db->where('cor.received_from_karigar_mail' ,0);
    $this->db->where('km.email !=' ," ");
     //$this->db->limit(10);
    // $this->db->group_by('karigar_rcpt_voucher_no');
    $result = $this->db->get()->result_array();
   //echo "<pre>";print_r($result);exit;
    if(!empty($result)){
      foreach ($result as $key => $value) {
        $data['order_details'] = $value;
        if(!empty($data['order_details']['km_email'])){
          $view ='order_module/customer_order/email_templates/Karigar_received';
          /*$subject ='GREETINGS FROM SHILPI TEAM VOUCHER NO-'.$data['order_details']['karigar_rcpt_voucher_no'].' RECEIVED';*/
          $subject ='GREETINGS FROM SHILPI TEAM ORDER NO K-'.$data['order_details']['order_id'].' RECEIVED';
          $this->email_lib->send($data,$data['order_details']['km_email'],$subject,$view,'order@shilpijewels.com',$from_name);
          $this->db->where('id',$value['id']);
          $this->db->set('received_from_karigar_mail',1);
          $this->db->update('customer_order_received');
        }
      }
    }
  }
  public function customer_delivery_mail(){
    $from_name ='sejal jewellers PVT LTD';
    $this->db->select('cor.*,co.email_id,cm.name as category_name');
    $this->db->from('customer_order_received cor');
    $this->db->join('customer_orders co','co.id = cor.order_id');
    $this->db->join('category_master cm','co.category_id=cm.id');
    $this->db->where('cor.deliver_to_customer_mail' ,0);
    $this->db->where('co.email_id !=' ," ");
    $this->db->limit(10);

    // $this->db->group_by('karigar_rcpt_voucher_no');
    $result = $this->db->get()->result_array();
    //print_r($result);
    if(!empty($result)){
      foreach ($result as $key => $value) {
        $data['order_details'] = $value;
        if(!empty($data['order_details']['email_id'])){
          $view ='order_module/customer_order/email_templates/ready_order';
     /*     $subject ='GREETINGS FROM SHILPI TEAM VOUCHER NO-'.$data['order_details']['karigar_rcpt_voucher_no'].' RECEIVED';*/
           $subject ='GREETINGS FROM SHILPI TEAM ORDER NO C-'.$data['order_details']['order_id'].' SENT';
          $this->email_lib->send($data,$data['order_details']['email_id'],$subject,$view,'order@shilpijewels.com',$from_name);
          $this->db->where('id',$value['id']);
          $this->db->set('deliver_to_customer_mail','1');
          $this->db->update('customer_order_received');
        }
      }
    }
  }
  
  public function validateOrderExcelpostdata($insert_array = '',$from_excel=false,$key){ 
    //die;

    if($from_excel == true){
      $_POST['customer_order'] = $insert_array;
    }
    $this->form_validation->set_rules($this->config->item('excel_order', 'admin_validationrules'));
  /*  if(!empty($_FILES['excel_order'])){
      $allowed_ext=array('jpg','jpeg','png');
      $image_validation=product_validate_images($_FILES['excel_order'],$allowed_ext);
    }else{
      $image_validation['status'] = true;
    }*/

    $data['status']= 'success';
    if($this->form_validation->run()===FALSE){
      $data['status']= 'failure';
      $data['data']= '';
      $data['error'][$key] = $this->getErrorMsg($key+1);
     
  
    }
    return $data;
  }
  private function getErrorMsg($key=''){
    return array(
        'party_name'=>create_excel_error_msg(strip_tags(form_error('customer_order[party_name]')),$key),
        'company_name'=>create_excel_error_msg(strip_tags(form_error('customer_order[company_name]')),$key),
        'mobile_no'=>create_excel_error_msg(strip_tags(form_error('customer_order[mobile_no]')),$key),
        'email_id'=>create_excel_error_msg(strip_tags(form_error('customer_order[email_id]')),$key),
        'k_name'=>create_excel_error_msg(strip_tags(form_error('customer_order[k_name]')),$key),
        'k_code'=>create_excel_error_msg(strip_tags(form_error('customer_order[k_code]')),$key),
        'k_mob_no'=>create_excel_error_msg(strip_tags(form_error('customer_order[k_mob_no]')),$key),
        'k_email'=>create_excel_error_msg(strip_tags(form_error('customer_order[k_email]')),$key),
        'department_id'=>create_excel_error_msg(strip_tags(form_error('customer_order[department_id]')),$key),
        'order_name'=>create_excel_error_msg(strip_tags(form_error('customer_order[order_name]')),$key),
        'order_date'=>create_excel_error_msg(strip_tags(form_error('customer_order[order_date]')),$key),
        'parent_category_id'=>create_excel_error_msg(strip_tags(form_error('customer_order[parent_category_id]')),$key),
        'weight_range_id'=>create_excel_error_msg(strip_tags(form_error('customer_order[weight_range_id]')),$key),
        'quantity'=>create_excel_error_msg(strip_tags(form_error('customer_order[quantity]')),$key),
        'size'=>create_excel_error_msg(strip_tags(form_error('customer_order[size]')),$key),
        'length'=>create_excel_error_msg(strip_tags(form_error('customer_order[length]')),$key),
        'delievery_date' => create_excel_error_msg(strip_tags(form_error('customer_order[delievery_date]')),$key),
        'expected_date' => create_excel_error_msg(strip_tags(form_error('customer_order[expected_date]')),$key),
        'purity' => create_excel_error_msg(strip_tags(form_error('customer_order[purity]')),$key),
        'weight_tolerance' =>create_excel_error_msg(strip_tags(form_error('customer_order[weight_tolerance]')),$key),
        'remark' => create_excel_error_msg(strip_tags(form_error('customer_order[remark]')),$key),
        'image' => '',//$image_validation['msg'],

      );
  }


  public function ready_product_images()
  {  
  //  print_r('aks');exit;
   $allowed_ext=array('jpg','jpeg','png');
     $postdata = $this->input->post();
   if(empty($_FILES['image']['name']) && empty($_FILES['video']['name']) ){
       $val1['status'] = false;
       $val1['msg'] = "Please atleast one image or video";
   }else if(!empty($_FILES['image']['name']) && ($_FILES['image']['type'] != "image/png" && $_FILES['image']['type'] != "image/jpeg" && $_FILES['image']['type'] != "image/jpg")){
       $val1['status'] = false;
       $val1['msg']="Invalid file format, Please Upload ".implode('/',$allowed_ext)." files.";
   }else if(!empty($_FILES['video']['name']) && $_FILES['video']['type'] != "video/mp4"){
       $val1['status'] = false;
       $val1['msg'] = "Please select video in mp4 format";
   }else{
      if($_FILES['image']['name'] && !empty($_FILES['image']['name'])){              
        // foreach ($_FILES['image']['name'] as $key => $val )
        // {
          $val1['status'] = true;
            $file_array['name'] = $_FILES['image']['name'];
            $file_array['type'] = $_FILES['image']['type'];//[$key];
            $file_array['tmp_name'] = $_FILES['image']['tmp_name'];//[$key];
            $file_array['error'] = $_FILES['image']['error'];//[$key];
            $file_array['size'] = $_FILES['image']['size'];//[$key];
            $upload_img/*[$key]*/ =uploadImage('ready_product',$file_array);
            $uploadData/*[$key]*/['order_image'] ='ready_product/'.$upload_img/*[$key]*/['file_name'];
            $uploadData/*[$key]*/['updated_at'] = date("Y-m-d H:i:s");         
           // $uploadData/*[$key]*/['customer_id'] = $this->db->insert_id();
          
      }
    if (isset($_FILES['video']['name']) && $_FILES['video']['name'] != '') {
           $val1['status'] = true;
             $file_array['name'] = $_FILES['video']['name'];
             $file_array['type'] = $_FILES['video']['type'];//[$key];
             $file_array['tmp_name'] = $_FILES['video']['tmp_name'];//[$key];
             $file_array['error'] = $_FILES['video']['error'];//[$key];
             $file_array['size'] = $_FILES['video']['size'];//[$key];
             $file_array['allowed_types'] = 'mp4';
             $upload_img/*[$key]*/ =uploadVideo('ready_product',$file_array);
             $uploadData/*[$key]*/['orders_video'] ='ready_product/'.$upload_img/*[$key]*/['file_name'];
             $uploadData/*[$key]*/['updated_at'] = date("Y-m-d H:i:s");         
           // $uploadData/*[$key]*/['customer_id'] = $this->db->insert_id();

        }  

    if(isset($val1) && $val1['status'] == true){
      $this->db->where('id',$postdata['customer_id']);
      $this->db->update('customer_orders',$uploadData);
     // echo $this->db->last_query();exit;
    }
   } 
    return $val1;  
  }


  function copy_order($id){
    $this->db->where('id',$id);
    $result = $this->db->get('customer_orders')->row_array();
    unset($result['id']);
    $result['created_at'] = date('Y-m-d H:i:s');
    $result['updated_at'] = date('Y-m-d H:i:s');
    //$result['order_date'] = date('Y-m-d');
    $this->db->insert('customer_orders',$result);
     $insert_id = $this->db->insert_id();

    $this->db->select('*');
    $this->db->where('customer_id',$id);
    $image_result = $this->db->get('customer_orders_img')->result_array();
    if(count($image_result) != 0){
      foreach ($image_result as $key => $value) {
         if($value['img_file_path'] != ""){
          unset($value['id']);
            $value['customer_id'] = $insert_id;
            $value['created_at'] = date('Y-m-d H:i:s');
            $value['updated_at'] = date('Y-m-d H:i:s');
            $this->db->insert('customer_orders_img',$value);
          } 

      }  
       
    }
    return true;
  }

  /*orde product code*/
  public function get_max_code($department_id,$code_name) {

        $this->db->select("cm.*,(select ifnull(max_count+1,1) from product_code_max_count where product_code like concat('%','".$code_name."' ,cm.code,'%') and department_id='".$department_id."' and  sub_code='".$code_name."') as max_count");
        $this->db->from('category_master cm'); 
        if(!empty($department_id)){
            $this->db->where('cm.department_id',$department_id);
          }
           $result = $this->db->get()->result_array();
         /* echo $this->db->last_query();
           print_r($result);
          die;*/
            return $result;        
     
    }
  public function update_delivery_date(){
     
    $result['change_delivery_date'] = date('Y-m-d', strtotime($_POST['change_delivery_date']));
    $result['expected_date']= date('Y-m-d', strtotime('-3 day', strtotime($_POST['change_delivery_date'])));
    $result['order_history'] = '1';
    $result['updated_at'] = date('Y-m-d H:i:s');
  //print_r($result);die;
      $postdata_id = $this->find_old_order_data($_POST['order_id']);  
      $order_result=array_diff($_POST,$postdata_id);
      $order_result['added_by']='web';
      $order_result['order_id']=$_POST['order_id'];
      $order_history_value=order_history_update($order_result);
      $this->db->where('id',$_POST['order_id']);
    if($this->db->update('customer_orders',$result)){
      $insert_array['filed_name']='delievery date';
      $insert_array['filed_value']=$_POST['change_delivery_date'];
      $insert_array['order_id']=$_POST['order_id'];
      $insert_array['added_by']='web';
      $insert_array['order_change_date']=date('Y-m-d H:i:s');
      $insert_array['created_at']=date('Y-m-d H:i:s');
      $insert_array['updated_at']=date('Y-m-d H:i:s');
      $this->db->insert('customer_orders_history',$insert_array);  
      return true;
    }else{
      return false;
    }
  }

  public function  find_order_history($order_id){
        $this->db->select('REPLACE(filed_name, "_", " ") as filed_name,filed_value, DATE_FORMAT( order_change_date,  "%d-%M-%Y  %T" ) as order_change_date');
        $this->db->from('customer_orders_history'); 
        $this->db->where('order_id',$order_id);
        //$this->db->group_by('order_change_date');
       $result = $this->db->get()->result_array();
       return filter_reponse_array($result,'order_change_date');
   // print_r($order_id);die;


  }

  
}//class
