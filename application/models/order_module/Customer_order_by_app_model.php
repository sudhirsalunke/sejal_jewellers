<?php
class Customer_order_by_app_model extends CI_Model {    
    
    function __construct() {
      $this->table_name = "customer_orders";
      $this->karigar_assign_table='customer_order_assign';
      $this->dbs=$this->load->database('account',true);
      parent::__construct();
    }

   public function get($params='',$search='',$limit=''){
      $this->db->select('co.*,co.party_name as customer_name,pcc.code_name as code ,pc.id as sub_category_id');
      $this->db->from($this->table_name.' co');
      $this->db->join('parent_category pc','pc.name=co.parent_category_id');
      $this->db->join('parent_category_codes pcc','pcc.parent_category_id=pc.id');
     // $this->db->join('customer c','co.customer_id=c.id');

      $get_status=@$_GET['status'];
      $table_col_name='customer_app_order';
      if($get_status =='by_app'){
        $this->db->where('status',0);
        $this->db->where('added_by','app');
      }else if($get_status=="cancelled"){
        $this->db->where('status',6);
      }
      
      if(isset($params['columns']) && !empty($params['columns'])){
        $filter_input=$params['columns'];      
      $this->get_filter_value($filter_input,$table_col_name);
      }


      if($limit == true){
        if($get_status == "cancelled"){
          $this->db->order_by('updated_at','desc');
        }else{
          $this->db->order_by('co.id','desc');
        }
        
        $this->db->limit($params['length'],$params['start']);
        $result = $this->db->get()->result_array();
       // echo $this->db->last_query();print_r($result);exit;
      }else{
        $row_array = $this->db->get()->result_array();
        $result = count($row_array);
       /// echo $this->db->last_query();print_r($result);exit;
      }
      return $result;
    }

    private function get_filter_value($filter_input,$table_col_name){
    $column_name=array();  
    $filter_column_name=filter_column_name($table_col_name);
    $sql='';
    $i=0;
     
     //print_r($filter_input);die;
    foreach ($filter_input as $key => $search_value){
       $column_name=$filter_column_name;
       //print_r($search_value['search']['value']);
        if(!empty($search_value['search']['value'])){
          if($i != 0){
            $sql.=' AND  ';
          }
            $sql.=''.$column_name[$key].' like "%'.$search_value['search']['value'].'%" ';
            $i++;
        

         }   
    }

    if(!empty($sql)){  
      $this->db->where($sql);  
    } 
   
       
  }
}
  ?>