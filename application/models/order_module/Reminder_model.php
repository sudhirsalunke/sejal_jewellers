<?php

class Reminder_model extends CI_Model {

  public function get($params='',$search='',$limit='',$is_count,$type,$interval)
  	{ 
      if($interval=='week'){
        $interval=7;
      }else if ($interval=='tommrow') {
        $interval=1;
      }else if ($interval=='today') {
        $interval=0;
      }else {
        $interval= -1;
      }
      //echo $is_count;
      if($is_count ==true)
      {
        $this->db->select('count(order_date) AS order_count');
      }else
      {
        $this->db->select('co.id as id, order_name, order_date, quantity, delievery_date, expected_date, party_name,co.company_name as company_name,km.name as karigar_name,co.karigar_id');        

      } 			
  		
      $this->db->from('customer_orders co');
      $this->db->join('karigar_master km','co.karigar_id=km.id');
      $this->db->where('co.status','2');
      if($type !='2')
    	{
        $table_col_name='Karigar_remainder_tbl';
        $this->get_karigar($interval,$search);
    	}else
      { $table_col_name='customer_remainder_tbl';
        $this->get_customer($interval,$search);
      }
      
      if($limit == true){
        $this->db->limit($params['length'],$params['start']);
      }
    /*  if(!empty($search))
      {
        $this->db->where("(co.id LIKE '%$search%' OR co.order_name LIKE '%$search%' OR km.name LIKE '%$search%'OR party_name LIKE '%$search%' )");
      }*/
      if(isset($params['columns']) && !empty($params['columns'])){
        $filter_input=$params['columns'];
      $this->get_filter_value($filter_input,$table_col_name);
      }
      if($is_count ==true){
      $result = $this->db->get()->row_array();
      $result=$result['order_count'];
      }else
      {
        $result = $this->db->get()->result_array();
      }
   //	echo $this->db->last_query();echo "<pre>";$result;exit;

   		return $result;
  	}

  private function get_karigar($interval,$search)
  {
    if($interval =='0')
      { 

        $this->db->where('DATE(co.expected_date) = CURDATE()');

      }else if($interval=='-1')
      {            
        $this->db->where('co.expected_date  < CURDATE()');
      }else if($interval=='1')
      {            
        $this->db->where('DATE(expected_date) = (CURDATE() + INTERVAL 1 DAY)');
      }
      else 
      {

        $this->db->where('co.expected_date BETWEEN NOW() AND DATE_ADD( NOW( ) , INTERVAL '.$interval.' DAY )');

        $this->db->where('co.expected_date >= ( CURDATE() + INTERVAL 2 DAY ) AND co.expected_date <= ( CURDATE() + INTERVAL 7 DAY )');

        //$this->db->where('co.expected_date BETWEEN NOW() AND DATE_ADD( NOW( ) , INTERVAL '.$interval.' DAY )');
        

      }
  /*    if(!empty($search))
      {
        $this->db->where("(co.id LIKE '%$search%' OR co.order_name LIKE '%$search%' OR km.name LIKE '%$search%'OR party_name LIKE '%$search%' )");
      }*/
  }

  private function get_customer($interval,$search)
  {
      if($interval=='0')

      {        
        $this->db->where('DATE(co.delievery_date) = CURDATE()');
      }else if($interval=='-1')
      { 
        $this->db->where('co.delievery_date < DATE_SUB(CURDATE( ),INTERVAL '.$interval.' DAY )');
      }
      else
      {
        $this->db->where('co.delievery_date BETWEEN NOW() AND DATE_ADD( NOW( ) , INTERVAL '.$interval.' DAY )');    
      /* if($interval=='0')
      {        
        $this->db->where('DATE(delievery_date) = CURDATE()');
      }else*/ if($interval=='-1')
      {  
        
        $this->db->where('co.delievery_date < CURDATE()');
      }else if($interval=='1')
      {  
        
        $this->db->where('DATE(delievery_date) = (CURDATE() + INTERVAL 1 DAY)');
      }
      else
      {
        $this->db->where('delievery_date >= ( CURDATE() + INTERVAL 2 DAY ) AND co.delievery_date <= ( CURDATE() + INTERVAL 7 DAY )');
      }
       /*if(!empty($search))
      {
        $this->db->where("(co.id LIKE '%$search%' OR co.order_name LIKE '%$search%' OR km.name LIKE '%$search%' OR party_name LIKE '%$search%')");
      }*/
    }

  }
  private function get_filter_value($filter_input,$table_col_name){
    $column_name=array();  
    $filter_column_name=filter_column_name($table_col_name);
    $sql='';
    $i=0;
     
     //print_r($filter_input);die;
    foreach ($filter_input as $key => $search_value){
       $column_name=$filter_column_name;
       //print_r($search_value['search']['value']);
        if(!empty($search_value['search']['value'])){
          if($i != 0){
            $sql.=' AND  ';
          }
            $sql.=''.$column_name[$key].' like "%'.$search_value['search']['value'].'%" ';
            $i++;
        

         }   
    }

    if(!empty($sql)){  
      $this->db->where($sql);  
    } 
   
       
  }
}