<?php

class Karigar_engaged_model extends CI_Model {

  function __construct() {
  	$this->table_name = "customer_orders";
    parent::__construct();
  }
  public function get($params='',$search='',$limit='',$karigar_id=''){
  	if(!empty($karigar_id)){
  		$this->db->select('co.*,co.party_name as customer_name,km.name as karigar_name,d.name dept_name,c.name category_name');
  	}else{
  		$this->db->select('co.party_name as customer_name,km.id as karigar_id,km.name as karigar_name,count(co.id) order_count,sum(weight_range_id) total_weight,sum(quantity) total_qty');
  	}
    $this->db->from($this->table_name.' co');
    $this->db->join('karigar_master km','co.karigar_id=km.id','left');
    $this->db->join('departments d','co.department_id=d.id');
    $this->db->join('category_master c','co.category_id=c.id','left');
   // $this->db->where('co.status !=','1');
    $this->db->where('co.status','2');


    $ci = &get_instance();
    $get_url = $ci->uri->segment(3);
 
    if(@$get_url =='view'){
       $table_col_name='karigar_engaged_orders';
    }else{
       $table_col_name='karigar_engaged';
    }
  if(isset($params['columns']) && !empty($params['columns'])){
      $filter_input=$params['columns'];
      $this->get_filter_value($filter_input,$table_col_name);
    }
   /* if(!empty($search)){
      $this->db->where("(co.party_name LIKE '%$search%' OR km.name LIKE '%$search%' OR co.id LIKE '%$search%'OR co.order_name LIKE '%$search%' OR co.parent_category_id LIKE '%$search%')");
    }*/
    if(!empty($karigar_id)){
    	$this->db->where('co.karigar_id',$karigar_id);
    }else{
    	$this->db->group_by('co.karigar_id');
    }
    

    if($limit == true){      
       if(!empty($params)){
      $this->db->limit($params['length'],$params['start']);
      }
      $result = $this->db->get()->result_array();
    }else{
      $row_array = $this->db->get()->result_array();
      $result = count($row_array);
      //echo $this->db->last_query();print_r($result);exit;
    }
/*    echo $this->db->last_query();
    echo '<pre>';
    print_r($result);
    echo '</pre>';die;*/
    return $result;
  }
  private function get_filter_value($filter_input,$table_col_name){
    $column_name=array();  
    $filter_column_name=filter_column_name($table_col_name);
    $sql='';
    $i=0;
     
     //print_r($filter_input);die;
    foreach ($filter_input as $key => $search_value){
       $column_name=$filter_column_name;
       //print_r($search_value['search']['value']);
        if(!empty($search_value['search']['value'])){
          if($i != 0){
            $sql.=' AND  ';
          }
            $sql.=''.$column_name[$key].' like "%'.$search_value['search']['value'].'%" ';
            $i++;
        

         }   
    }

    if(!empty($sql)){  
      $this->db->where($sql);  
    } 
   
       
  }
  public function get_karigar_wise_net_wt($karigar_id){
    $this->db->select('sum(cor.net_weight) as rec_net_wt');
    $this->db->from('customer_order_received cor');
    $this->db->join('customer_orders co','co.id = cor.order_id');
    $this->db->where('co.karigar_id',$karigar_id);
    $result = $this->db->get()->row_array();
    // echo '<pre>';
    // print_r($result);
    // echo '</pre>';
    if($result['rec_net_wt'])
      return $result['rec_net_wt'];
    else
      return 0;
  }
}
