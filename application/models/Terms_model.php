<?php

class Terms_model extends CI_Model {

  function __construct() {
      $this->table_name = "terms_master";
      parent::__construct();
  }

  public function validatepostdata(){
  
  	$this->form_validation->set_rules($this->config->item('terms_master', 'admin_validationrules'));
     if ($this->form_validation->run() == FALSE) {       	
          return FALSE;
      } else {
          return TRUE;
      }
  }
  public function store(){
  	$postdata = $this->input->post('terms');
  	$encrption = $this->data_encryption->get_encrypted_id();
    $insert_array=$postdata;
    $insert_array['encrypted_id']=$encrption;
    $insert_array['created_at']=$insert_array['updated_at']=date('Y-m-d H:i:s');
  	if($this->db->insert($this->table_name,$insert_array)){
  		return get_successMsg();
  	}else{
  		return get_errorMsg();
  	}
  }
  public function get($filter_status='',$status='',$params='',$search='',$limit=''){
    $this->db->select('tm.*,pc.name as parent_category');
    $this->db->from($this->table_name.' tm');
    $this->db->join('parent_category pc','tm.parent_category_id=pc.id');
    if(!empty($filter_status))
       $this->db->order_by($status[$filter_status['column']],$filter_status['dir']);
  /*  if(!empty($search)){
        $this->db->like('pc.name',$search);
    }*/
    if(isset($params['columns']) && !empty($params['columns'])){
      $filter_input=$params['columns'];
      $table_col_name="terms_table";
      $this->get_filter_value($filter_input,$table_col_name);
    }


    if($limit == true){
      $this->db->order_by('id','DESC');
      if(!empty($params)){
      $this->db->limit($params['length'],$params['start']);
      }
      $result = $this->db->get()->result_array();
    }else{
      $row_array = $this->db->get()->result_array();
      $result = count($row_array);
      //echo $this->db->last_query();print_r($result);exit;
    }
    return $result;
  }
  private function get_filter_value($filter_input,$table_col_name){
    $column_name=array();  
    $filter_column_name=filter_column_name($table_col_name);
    $sql='';
    $i=0;
     
     //print_r($filter_input);die;
    foreach ($filter_input as $key => $search_value){
       $column_name=$filter_column_name;
       //print_r($search_value['search']['value']);
        if(!empty($search_value['search']['value'])){
          if($i != 0){
            $sql.=' AND  ';
          }
            $sql.=''.$column_name[$key].' like "%'.$search_value['search']['value'].'%" ';
            $i++;
        

         }   
    }

    if(!empty($sql)){  
      $this->db->where($sql);  
    } 
       
  }

  public function find($id){
  	$this->db->where('id',$id);
  	$result = $this->db->get($this->table_name)->row_array();
  	return $result;
  }
   public function find_by_encrypted_id($id){
    $this->db->where('encrypted_id',$id);
    $result = $this->db->get($this->table_name)->row_array();
    return $result;
  }
  
  
  public function update(){
  	$postdata = $this->input->post('terms');
  	$update_array=$postdata;
    $update_array['updated_at']=date('Y-m-d H:i:s');
  	$this->db->where('encrypted_id',$_POST['encrypted_id']);
  	if($this->db->update($this->table_name,$update_array)){
  		return get_successMsg();
  	}else{
  		return get_errorMsg();
  	}
  }
  public function delete($id){
  	$this->db->where('encrypted_id',$id);
  	if($this->db->delete($this->table_name)){
  		return get_successMsg();
  	}else{
  		return get_errorMsg();
  	}
  }
}    