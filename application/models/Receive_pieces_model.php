<?php

class Receive_pieces_model extends CI_Model {

  function __construct() {
    parent::__construct();
    $this->table_name = "shortlisted_products";
    $this->load->model('product_model');
  }
  public function get($filter_status='',$status='',$params='',$search='',$limit='',$order_id=''){
    $this->db->select('p.sub_Sr_no,p.UID_No,"" IntendedWH,p.Order_Reff,p.Vendor_Code,p.Vendor_Name,p.product_code Vendor_Design_Code,ca.name karat,a.name article_code,a.description,p.Sub_Product,b.name buying_complexity,mt.name manufacturing_type,p.size,p.Size_UOM,pc.name Pcs,p.grs_wt,"" total_stn_qty,p.Total_Stn_Cts,p.Net_Wt,p.Net_Order_Wt,CONCAT(w.from_weight, "-", w.to_weight) as wt_band,p.Stone_Name,p.Stone_Qty,p.Stn_Cts,p.Stone_Rate as stone_rate/Cts,p.Labour_Chg as Labour Chg / Gram,p.Wastage as Wastage %, p.remarks,sp.id shortlisted_id,sp.order_id,k.name karigar_name,k.code karigar_code,,m.name metal_name,c.name category_name,c.code category_code,sc.name sub_category_name,sc.code sub_category_code,ca.name carat_name,a.name article_name,mt.name manufacturing_type_name,p.image image1,ca.name as Gold_Carat,"" as Gold_colour,c.name as Product_Type,sc.name as Category,"" as Quantity,"" as Gross_Wt,"" as Less_Wt,"" as Pure_Wt,"" as Price,"" as Rate,"" as Extra_Charge,"" as Net_Price,"" as AVG_WT,"" as Minimun_Wt,"" as Maximum_Wt,"17 WORKING DAYS" as Shipping_Days,p.product_code as product_code,CONCAT_WS("/thumb/",p.product_code,p.image) as image,"" as Wastage,p.weight_band_id,p.actual_wt,w.from_weight,w.to_weight');
    $this->db->from("shortlisted_products sp");
    $this->all_joins();
    if(!empty($order_id)){
      $this->db->join('orders o', 'o.id = sp.order_id');
      $this->db->where('o.id',$order_id);
    }
    $this->db->where('sp.status','2');
    if(!empty($search)){
        $this->all_like_queries($search);
    }
    if($limit == true)
      $this->db->limit($params['length'],$params['start']);
    $result = $this->db->get()->result_array();
    return $result;
  }
  private function all_joins(){
    $this->db->join('product p', 'p.id = sp.product_id');
    $this->db->join('karigar_master k', 'k.id = p.karigar_id', 'left');
    $this->db->join('category_master c', 'c.id = p.category_id', 'left');
    $this->db->join('sub_category_master sc', 'sc.id = p.sub_category_id', 'left');
    $this->db->join('manufacturing_type_master mt', 'mt.id = p.manufacturing_type_id', 'left');
    $this->db->join('article_master a', 'a.id = p.article_id', 'left');
    $this->db->join('carat_master ca', 'ca.id = p.carat_id', 'left');
    $this->db->join('metal_master m', 'm.id = p.carat_id', 'left');
    $this->db->join('weights w', 'w.id = p.weight_band_id', 'left');
    $this->db->join('buying_complexity_master b', 'b.id = p.buying_complexity_id', 'left');
    $this->db->join('pcs_master pc', 'pc.id = p.pcs_id', 'left');
  }
  private function all_like_queries($search){
    $this->db->where("(`p`.`product_code` LIKE '%$search%' OR  `k`.`name` LIKE '%$search%' OR  `k`.`code` LIKE '%$search%' OR  `c`.`name` LIKE '%$search%' OR  `c`.`code` LIKE '%$search%' OR  `p`.`size` LIKE '%$search%' OR  `mt`.`name` LIKE '%$search%' OR  `a`.`name` LIKE '%$search%'
OR  `ca`.`name` LIKE '%$search%' OR  `m`.`name` LIKE '%$search%'
OR  `w`.`from_weight` LIKE '%$search%' OR  `w`.`to_weight` LIKE '%$search%' OR p.actual_wt LIKE '%$search%')");

  }
  public function approve($id=''){
    $this->db->select('*');
    $this->db->from('shortlisted_products');
    if(!empty($id)){
      $_POST['id'] = $id;
    }
    $this->db->where('id',$_POST['id']);
    $result = $this->db->get()->row_array();
    if(!empty($result)){
      $insert_array = array(
          'product_id'=>$result['product_id'],
          'order_id'=>$result['order_id'],
          'shortlisted_id'=>$_POST['id'],
          'user_id'=>$this->session->userdata('user_id'),
          'created_at'=>date('Y-m-d H:i:s'),
          'updated_at'=>date('Y-m-d H:i:s'),
        );
      if($this->db->insert('Approve_products',$insert_array)){
        $this->db->set('status','3');
        $this->db->where('id',$_POST['id']);
        $this->db->update('shortlisted_products');
        return true;
      }
      return false;
    }
  }

  public function Reject($id=''){
    $this->db->select('*');
    $this->db->from('shortlisted_products');
    if(!empty($id)){
      $_POST['id'] = $id;
    }
    $this->db->where('id',$_POST['id']);
    $this->db->where('id',$_POST['id']);
    $result = $this->db->get()->row_array();
    if(!empty($result)){
      $insert_array = array(
          'product_id'=>$result['product_id'],
          'order_id'=>$result['order_id'],
          'shortlisted_id'=>$_POST['id'],
          'user_id'=>$this->session->userdata('user_id'),
          'created_at'=>date('Y-m-d H:i:s'),
          'updated_at'=>date('Y-m-d H:i:s'),
        );
      if($this->db->insert('rejected_products',$insert_array)){
        $this->db->set('status','4');
        $this->db->where('id',$_POST['id']);
        $this->db->update('shortlisted_products');
        return true;
      }
      return false;
    }
  }

  public function get_orders($filter_status='',$status='',$params='',$search='',$limit='',$vendor=''){
    $this->db->select('o.*,(select count(ssp.product_id) from shortlisted_products ssp Where ssp.order_id = sp.order_id) as no_of_products,c.name as c_name');
    $this->db->from('orders o');
    $this->db->join('shortlisted_products sp','sp.order_id = o.id');
    $this->db->join('corporate c','c.id = o.corporate');
    $this->db->where('sp.status','2');
    if(!empty($vendor))
      $this->db->where('c.id',$vendor);
    if(!empty($search)){
        $this->db->where("(`o`.`id` LIKE '%$search%')");
    }
    $this->db->group_by('o.id');
    $this->db->order_by('o.id DESC');
    $result = $this->db->get()->result_array();
    return $result;
  }

  public function validatepostdata($insert_array = '',$from_excel=false){ 
    $this->form_validation->set_rules($this->config->item('receive_pieces', 'admin_validationrules'));
    $file_error = $this->fileValidation();
    $data['status']= 'success';
    if($this->form_validation->run()===FALSE || !empty($file_error)){
      $data['status']= 'failure';
      $data['data']= '';
      $data['error'] = $this->getErrorMsg($file_error);
    }
    
    return $data;
  }

  private function fileValidation(){
    if(empty($_FILES)){
      return "Please upload excel file.";
    }
    else{
      $content_type = mime_content_type($_FILES['file_0']['tmp_name']);
      $mime_content_type = array(
        "application/octet-stream",
        "application/vnd.ms-excel",
        "application/vnd.ms-excel.addin.macroEnabled.12",
        "application/vnd.ms-excel.sheet.binary.macroEnabled.12",
        "application/vnd.ms-excel.sheet.macroEnabled.12",
        "application/vnd.ms-excel.template.macroEnabled.12",
        "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet"
      );
      if(!in_array($content_type, $mime_content_type)){
        return "Please upload excel file.";
      }
    }
    return "";
  }

  private function getErrorMsg($file_error){
    return array(
      'corporate'=>strip_tags(form_error('corporate')),
      'order_id'=>strip_tags(form_error('order_id')),
      'status'=>strip_tags(form_error('status')),
      'file'=>$file_error,
    );
  }

  function validationExcelData($data,$corporate='',$order_id){
    $insert_array = array();
    $error = array();
    $no=3;
    foreach ($data as $value) {
      if($corporate == '2')
        $value['vendor_design_code'] = $value['product_code'];
      if(trim($value['vendor_design_code'])=="")
        $error[] = "Vendor design code Should not be empty on row no $no";
      else{
        $product = $this->product_model->find(trim($value['vendor_design_code']),true);
        if(empty($product))
          $error[] = "Vendor design code doesn't exist in system on row no $no";
        else{
          $orderData = $this->getDataByOrder($product['id'],$order_id);
          if(empty($orderData))
            $error[] = $value['vendor_design_code']." not shortlisted on row no $no";
          else{
            if($orderData['status']=="4")
              $error[] = $value['vendor_design_code']." already rejected on row no $no";
            if($orderData['status']=="3")
              $error[] = $value['vendor_design_code']." already approved on row no $no";
            else{
              $insert_array[] = array(
                "product_id" => $product['id'],
                "user_id" => $this->session->userdata('user_id'),
                "created_at" => date("Y-m-d H:i:s"),
                "updated_at" => date("Y-m-d H:i:s"),
                "order_id" => $orderData['order_id'],
                "shortlisted_id" => $orderData['id'],
              );
            }
          }
        }
      }
      
      $no++;
    }
    if(empty($error))
      $result['status']= 'success';
    else
      $result['status']= 'failure';
    $result['insert_array'] = $insert_array;
    $result['error'] = $error;
    return $result;
  }

  private function getDataByOrder($product_id,$order_id){
    //$sql = "SELECT s.order_id,s.id,s.status from orders o inner join shortlisted_products s on o.id=s.order_id WHERE corporate='".$corporate."' AND product_id='".$product_id."'";
    $sql = "SELECT s.order_id,s.id,s.status from shortlisted_products s left join orders o on o.id=s.order_id WHERE order_id='".$order_id."' AND product_id='".$product_id."'";
    return $this->db->query($sql)->row_array();
  }

  public function find($order_id){
    $this->db->where('id',$order_id);
    $result = $this->db->get('orders')->row_array();
    return $result['corporate'];
  }
}