<?php
class Hallmarking_center_model extends CI_Model {

  function __construct() {
      $this->table_name = "hallmarking_center";
      parent::__construct();
  }

  public function validatepostdata(){
   
  	$this->form_validation->set_rules($this->config->item('hallmarking_center', 'admin_validationrules'));
     if ($this->form_validation->run() == FALSE) {       	
          return FALSE;
      } else {
          return TRUE;
      }
  }
  public function store(){
    $insert_array=array();
  	$postdata = $this->input->post('hallmarking_center');
  	$insert_array = $postdata;
    $insert_array['encrypted_id']=$this->data_encryption->get_encrypted_id();
    $insert_array['updated_at']=date('Y-m-d H:i:s');
    $insert_array['created_at']=date('Y-m-d H:i:s');
  		
  	if($this->db->insert($this->table_name,$insert_array)){
  		return get_successMsg();
  	}else{
  		return get_errorMsg();
  	}
  }
  public function get($filter_status='',$status='',$params='',$search='',$limit=''){
    $this->db->select('*');
    $this->db->from($this->table_name);
    // if(!empty($filter_status))
    //    $this->db->order_by($status[$filter_status['column']],$filter_status['dir']);

  /*  if(!empty($search)){
        $this->db->like('name',$search);
    }*/


    if($limit == true){
      $this->db->order_by('id','DESC');
      if(!empty($params)){
      $this->db->limit($params['length'],$params['start']);
      }
      $result = $this->db->get()->result_array();
    }else{
      $row_array = $this->db->get()->result_array();
      $result = count($row_array);
      //echo $this->db->last_query();print_r($result);exit;
    }
    return $result;
  }


  public function find_by_encrypted_id($encrypted_id){
   $this->db->where('encrypted_id',$encrypted_id);
   $result = $this->db->get($this->table_name)->row_array();
   return $result;
  }
  public function find($id){
  	$this->db->where('id',$id);
  	$result = $this->db->get($this->table_name)->row_array();
  	return $result;
  }

  // public function get_primary_key($encrypted_key){
  // 	$this->db->where('encrypted_id',$encrypted_key);
  // 	$result = $this->db->get($this->table_name)->row_array();
  // 	return $result['id'];
  // }
  public function update(){
    $postdata =$this->input->post('hallmarking_center');
  	$update_array = array(
  		'name' => $postdata['name'],
  		'code' => $postdata['code'],
  		'updated_at' => date('Y-m-d H:i:s')
  		);
  	$this->db->where('encrypted_id',$postdata['encrypted_id']);
  	if($this->db->update($this->table_name,$update_array)){
  		return get_successMsg();
  	}else{
  		return get_errorMsg();
  	}
  }
  public function delete($id){
  	$this->db->where('encrypted_id',$id);
  	if($this->db->delete($this->table_name)){
  		return get_successMsg();
  	}else{
  		return get_errorMsg();
  	}
  }

  public function check_hc_used($id){
      $this->db->where('hc_id',$id);
      $this->db->from('quality_control');
     return $this->db->count_all_results();
 }
}    