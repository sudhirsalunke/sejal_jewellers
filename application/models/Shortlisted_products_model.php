<?php

class Shortlisted_products_model extends CI_Model {

  function __construct() {
      $this->table_name = "shortlisted_products";
      parent::__construct();
  }
  public function get($id='',$type='',$order_id='',$status='',$type=''){
    
    if($id == true){
      $this->db->select('pst.id');
    }else{
      if($type=="export")
        $this->db->select('p.sub_Sr_no,p.sub_Sr_no as Sr_no,p.UID_No,"" IntendedWH,p.Order_Reff,p.Vendor_Code,p.Vendor_Name,p.product_code Vendor_Design_Code,ca.name karat,a.name article_code,a.description,p.Sub_Product,b.name buying_complexity,mt.name manufacturing_type,p.size,p.Size_UOM,pc.name Pcs,p.grs_wt,"" total_stn_qty,p.Total_Stn_Cts,p.Net_Wt,p.Net_Order_Wt,CONCAT(w.from_weight, "-", w.to_weight) as wt_band,p.Stone_Name,p.Stone_Qty,p.Stn_Cts,p.Stone_Rate as stone_rate/Cts,p.Labour_Chg as Labour Chg / Gram,p.Wastage as Wastage %, p.remarks,ca.name as Gold_Carat,"" as Gold_colour,c.name as Product_Type,sc.name as Category,"" as Quantity,"" as Gross_Wt,"" as Less_Wt,"" as Pure_Wt,"" as Price,"" as Rate,"" as Extra_Charge,"" as Net_Price,"" as AVG_WT,"" as Minimun_Wt,"" as Maximum_Wt,"17 WORKING DAYS" as Shipping_Days,p.product_code as product_code,CONCAT_WS("/thumb/",p.product_code,p.image) as image,"" as Wastage,p.weight_band_id,p.actual_wt,w.from_weight,w.to_weight');
      else
        $this->db->select('p.*,k.name karigar_name,k.code karigar_code,c.name category_name,c.code category_code,sc.name sub_category_name,sc.code sub_category_code,mt.name manufacturing_type_name,a.name article_name,ca.name carat_name,m.name metal_name,w.from_weight,w.to_weight,CONCAT(w.from_weight, "-", w.to_weight) as weight');
    }
    $this->db->from("product p");
    $this->db->join('karigar_master k', 'k.id = p.karigar_id', 'left');
    $this->db->join('category_master c', 'c.id = p.category_id', 'left');
    $this->db->join('sub_category_master sc', 'sc.id = p.sub_category_id', 'left');
    $this->db->join('article_master a', 'a.id = p.article_id', 'left');
    $this->db->join('carat_master ca', 'ca.id = p.carat_id', 'left');
    $this->db->join('metal_master m', 'm.id = p.metal_id', 'left');
    $this->db->join('pcs_master pc', 'pc.id = p.pcs_id', 'left');
    $this->db->join('buying_complexity_master b', 'b.id = p.buying_complexity_id', 'left');
    $this->db->join('manufacturing_type_master mt', 'mt.id = p.manufacturing_type_id', 'left');
    $this->db->join('weights w', 'w.id = p.weight_band_id', 'left');
    $this->db->join('shortlisted_products pst','pst.product_id=p.id',"left");

    if($this->input->post('corporate')!=''){
      $this->db->like("corporate",$this->input->post('corporate'));
    }
    if($this->input->post('category')!=''){
      $category = $this->input->post('category');
      $this->db->where("(c.name LIKE '%$category%' OR c.code LIKE '%$category%')");
    }
    if($this->input->post('karigar')!=''){
      $karigar = $this->input->post('karigar');
      $this->db->where("(k.name LIKE '%$karigar%' OR k.code LIKE '%$karigar%')");
    }
    if($this->input->post('product_code')!=''){
      $product_code = $this->input->post('product_code');
      $this->db->where("p.product_code LIKE '%$product_code%'");
    }
    if($this->input->post('weight_range')!=''){
      $weight_range = $this->input->post('weight_range');
      $weight = explode('-', $weight_range);
      $this->db->where("w.from_weight LIKE '%$weight[0]%'");
      $this->db->where("w.to_weight LIKE '%$weight[1]%'");
    }
    
    if(!empty($order_id))
    $this->db->where('pst.order_id ',$order_id);
    $this->db->where('pst.status',$status);
    $result = $this->db->get()->result_array();

    //$result['shortlist_count']=count($result);
    //echo $cor=$this->input->post('corporate'); die;
 // echo $this->db->last_query();die;
    return $result;
  }

  function getProductCorporate(){
    $this->db->select('p.corporate');
    $this->db->where('pst.status','1');
    $this->db->from('product p');
    $this->db->join('shortlisted_products pst','pst.product_id=p.id',"left");
    return $this->db->get()->row_array();
  }

  private function validation(){
    $corporate = $this->getProductCorporate();

    if(!empty($corporate)){
      $product_id = $this->input->post('product_id');
      $this->db->select('p.corporate');
      $this->db->from('product p');
      $this->db->join('shortlisted_products pst','pst.product_id=p.id',"left");
      $this->db->where('p.id',$product_id);
      $prod_corporate = $this->db->get()->row_array();

      if($corporate!=$prod_corporate)
        return false;
    }
    return true;
  }

  public function store()
  {
/*    $validtion = $this->validation();
     if($validtion){
      $product_id = $this->input->post('product_id');
      $insert_array=array(
          'product_id'=> $product_id,
          'order_id'=> '',
          'status'=> '1',
          'user_id'=> $this->session->userdata('user_id')
        );
      $this->db->from($this->table_name);
      $this->db->where($insert_array);
      $result = $this->db->get()->result_array(); 
         if(count($result) <= 0 ){
          return $this->db->insert($this->table_name,$insert_array);
          }
       }
      return false;*/
          $product_id = $this->input->post('product_id');
      $insert_array=array(
          'product_id'=> $product_id,
          'order_id'=> '',
          'status'=> '1',
          'user_id'=> $this->session->userdata('user_id')
        );
      $this->db->from($this->table_name);
      $this->db->where($insert_array);
      $result = $this->db->get()->result_array(); 
         if(count($result) <= 0 ){
          return $this->db->insert($this->table_name,$insert_array);
          }
  }
  public function delete()
  {  
    $product_id = $this->input->post('product_id');
    if(!empty($product_id)){
      $this->db->where('product_id',$product_id);
      $this->db->where('status','1');
      return $this->db->delete($this->table_name);
    }
    else{
      $_POST = $_GET;
      $data = $this->get('','','','1');
      $ids = array_column($data, 'id');
      $this->db->where_in('product_id',$ids);
      $this->db->where('status','1');
      return $this->db->delete($this->table_name);
    }
  }
  public function get_corporate(){
    return $this->db->get('corporate')->result_array();
  }
  public function get_last_order_id(){
    $this->db->select('id');
    $this->db->from('orders');
    $this->db->order_by('id','desc');
    $this->db->where('status','0');
    $result = $this->db->get()->row_array();
    if(empty($result)){
      $this->db->set('status','0');
      $this->db->set('created_at',date('Y-m-d H:i:s'));
      $this->db->insert('orders');
      return $this->db->insert_id();
    }else{
      return $result['id'];
    }
  }
  public function update(){
    $this->db->set('id',$_POST['order_id']);
    $this->db->set('corporate',@$_POST['corporate']);
    $this->db->set('comment',@$_POST['comment']);
    $this->db->set('user_id',$this->session->userdata('user_id'));
    $this->db->set('status',$_POST['status']);
    $this->db->set('updated_at',date('Y-m-d H:i:s'));
    $this->db->where('id',$_POST['order_id']);
    if($this->db->update('orders')){;
      $product_ids=$this->get($id=true,'','','1');
      $this->update_shortlisted_products();
      foreach ($product_ids as $key => $value) {
        $this->db->set('order_id',$_POST['order_id']);
        $this->db->set('status','2');
        $this->db->where('id',$value['id']);
        $this->db->update($this->table_name);
      }
      
    }else{
      return false;
    }
    return $_POST['order_id'];
  }
  public function find_order($order_id){
    $this->db->where('id',$order_id);
    $result = $this->db->get('orders')->row_array();
    return $result;
  }
  private function update_shortlisted_products(){
    $product_ids = $this->get('','','','1');
    foreach ($product_ids as $key => $value) {
      $this->db->set('status','1');
      $this->db->where('id',$value['id']);
      $this->db->update('product');
    }
  }
  public function shortlisted_all_products(){
    $product_id = array();
    $this->db->select('sp.product_id');
    $this->db->from('shortlisted_products sp');
    $this->db->where('sp.status','1');
    $cp_id = $this->db->get()->result_array();
    //print_r($$cp_id);die;
    if(!empty($cp_id)){
      foreach ($cp_id as $key => $value) {
        $product_id[] = $value['product_id'];
      }
    }
    $this->db->select('p.id,p.corporate');
    $this->db->from('product p');
    $this->db->join('karigar_master k', 'k.id = p.karigar_id', 'left');
    $this->db->join('category_master c', 'c.id = p.category_id', 'left');
    $this->db->join('weights w', 'w.id = p.weight_band_id', 'left');
    $this->db->join('shortlisted_products sp', 'sp.product_id = p.id', 'left');
    if(!empty($_POST['filter'])){
      $filter[] = $_POST['filter'];
      foreach ($filter as $key => $value) {
        if(!empty($value['wt'])){
          $weight=explode('-', $value['wt']);
          $this->db->where('w.to_weight',$weight[0]);
          if(!empty($weight[1])){
            $this->db->or_where('w.to_weight',$weight[1]);
            $this->db->or_where('p.actual_wt BETWEEN "'.$weight[0].'" AND "'.$weight[1].'"');
          }
        }
        if(!empty($value['product_code'])){
          $this->db->like('p.product_code',$value['product_code']);
        }
        if(!empty($value['karigar_code'])){
          $this->db->like('k.code',$value['karigar_code']);
        }
        if(!empty($value['category'])){
          $this->db->like('c.name',$value['category']);
        }
        if(!empty($value['corporate'])){
          $this->db->like('p.corporate',$value['corporate']);
        }
        
      }
    }
    //$this->db->where('image != ""');
    $this->db->where('p.image is not null');
    $this->db->where('p.status','0');
    if(!empty($product_id)){
      $this->db->where('p.id not in ('.implode(',',$product_id).')');
    }
    $result = $this->db->get()->result_array();
    $corporate = $this->getProductCorporate();
    //echo $this->db->last_query();
    //print_r($corporate);die;
    if(empty($corporate)){
      $corporate = '';
    }
    else{
      $corporate = $corporate['corporate'];
    }

    if(!empty($result)){

      foreach ($result as $key => $value) {
        if(empty($corporate)){
          $corporate = $value['corporate'];
        }
        //print_r($corporate);
      /*  if($corporate != $value['corporate']){
          return get_errorMsg("Please select products of any one corporate.");
        }*/
        $insert_array[]=array(
          'product_id'=> $value['id'],
          'order_id'=> '',
          'status'=> '1',
          'user_id'=> $this->session->userdata('user_id')
        );
      }
    }else{
      return get_errorMsg();
    }


    if(!empty($insert_array)){
      $this->db->insert_batch($this->table_name,$insert_array);
      return get_successMsg();
    }
    else{
      return get_errorMsg();
    }
  }

  
  public function get_design_list($filter_status='',$status='',$params='',$search='',$limit='',$limit1=''){
    $this->db->select('k.name karigar_name,k.code karigar,m.name metal,c.name category,c.code category_code,sc.name sub_category,sc.code sub_category_code,ca.name carat,pm.name as pcs,CONCAT(w.from_weight, "-", w.to_weight) as weight_band, ,w.from_weight,w.to_weight,a.name article,pt.name manufacturing_type,bc.name as buying_complexity,group_concat(sp.status separator ",") as shortlisted_products,group_concat(cp.status separator ",") as corporate_products,p.*,p.status,p.corporate');
     
        $this->db->from("product p");
        $this->all_joins();
        $this->all_where_conditions($search);
        $this->db->group_by('p.id');
        $this->db->order_by('p.id','DESC');
        $this->db->limit($params['length'],$params['start']);
        
      $result = $this->db->get()->result_array();
     // echo$this->db->last_query();
        return $result;
  }


  private function all_joins(){
    $this->db->join('karigar_master k', 'k.id = p.karigar_id', 'left');
    $this->db->join('category_master c', 'c.id = p.category_id', 'left');
    $this->db->join('sub_category_master sc', 'sc.id = p.sub_category_id', 'left');
    $this->db->join('manufacturing_type_master pt', 'pt.id = p.manufacturing_type_id', 'left');
    $this->db->join('buying_complexity_master bc', 'bc.id = p.buying_complexity_id', 'left');
    $this->db->join('article_master a', 'a.id = p.article_id', 'left');
    $this->db->join('carat_master ca', 'ca.id = p.carat_id', 'left');
    $this->db->join('metal_master m', 'm.id = p.metal_id', 'left');
    $this->db->join('weights w', 'w.id = p.weight_band_id', 'left');
    $this->db->join('shortlisted_products sp', 'sp.product_id = p.id', 'left');
    $this->db->join('pcs_master pm', 'pm.id = p.pcs_id', 'left');
    $this->db->join('corporate_products cp', 'cp.sort_Item_number = p.product_code', 'left');
  }
   private function all_where_conditions($search){
      $this->db->where('p.status','0');
      if($this->input->post('page_title')=="DESIGNS WITHOUT IMAGES" || $this->input->post('page_title')=="ADD PRODUCTS IMAGES"){
        $this->db->where('p.image',null);
      }
      else if($this->input->post('page_title')=='DESIGNS WITH IMAGES' || $this->input->post('page_title')=='SEND DESIGNS FOR APPROVAL'){
        $this->db->where('p.image is not null');
       
      }


          if(!empty($search)){
              if(!empty($_REQUEST['columns'])){
                  $c_search = array();
                    foreach ($_REQUEST['columns'] as $key => $value) {
                        if(!empty($value['search']['value'])){
                            if($key == 0){
                                $this->db->like('c.name',$value['search']['value']);
                            }
                            if($key == 1){
                              $weight=explode('-', $value['search']['value']);
                              $this->db->where('w.from_weight',$weight[0]);
                              if(!empty($weight[1]))
                                $this->db->where('w.to_weight',$weight[1]);
                              //$this->db->or_where('p.actual_wt BETWEEN "'.$weight[0].'" AND "'.$weight[1].'"');
                            }
                            if($key == 2){
                                $this->db->like('p.product_code',$value['search']['value']);
                            }
                            if($key == 3){
                                $this->db->like('k.name',$value['search']['value']);
                            }
                            if($key == 4){
                                $this->db->like('p.corporate',$value['search']['value']);
                            }
                        }
                    }
                }
             }   

          }


}