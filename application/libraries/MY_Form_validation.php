<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class MY_Form_validation extends CI_Form_validation {

    function verifyCredentials() {

        $ci = & get_instance();
        $ci->db->select('*');
        $ci->db->where('email', $ci->input->post('username'));
        $ci->db->where('password', MD5($ci->input->post('password')));
        $query = $ci->db->get('admin')->row_array();
        if (count($query) > 0) {
            return TRUE;
        } else {
            return FALSE;
        }
    }

    function verifyEmail() {
        $ci = & get_instance();
        $ci->db->select('email');
        $ci->db->where('email', $ci->input->post('email'));
        $query = $ci->db->get('login')->row_array();
        if (count($query) > 0) {
            return false;
        } else {
            return true;
        }
    }

    function verifyIdentifier() {
        $ci = & get_instance();
        $ci->db->select('unique_identifier');
        $ci->db->where('unique_identifier', $ci->input->post('unique_identifier'));
        $query = $ci->db->get('login')->row_array();
        if (count($query) > 0) {
            return false;
        } else {
            return true;
        }
    }

    public function password_check($str) {
        if (preg_match('#[0-9]#', $str) && preg_match('#[a-zA-Z]#', $str)) {
            return TRUE;
        }
        return FALSE;
    }

    function is_json_pos() {
        $ci = & get_instance();
        $string=$ci->input->post('recommendation[pos_filter]');
        $isarr= json_decode($string,true);
        if(is_array($isarr)){
            return true;
        }else{
            return false;
        }
    }

    function is_json_neg() {
        $ci = & get_instance();
        $string=$ci->input->post('recommendation[neg_filter]');

        $isarr= json_decode($string,true);
        if(is_array($isarr)){
            return true;
        }else{
            return false;
        }
    }

    function is_json_boost() {
        $ci = & get_instance();
        $string=$ci->input->post('recommendation[boost_fields]');
        $isarr= json_decode($string,true);
        if(is_array($isarr)){
            return true;
        }else{
            return false;
        }
    }

    function check_max_paging(){
        $ci = & get_instance();
        $string=$ci->input->post('recommendation[paging]');
        if($string<=100){
            return true;
        }else{
            return false;
        }
    }
    function check_email($email) {
        $ci = & get_instance();
        $ci->db->select('*');
        $ci->db->where('email', $email);
        $query = $ci->db->get('user_master')->row_array();
        if (count($query) > 0) {
            return false;
        } else {
            return true;
        }
    }


    function check_model(){
        $ci = & get_instance();
        $model_name=$ci->input->post('model[name]');
        $brand_id=$ci->input->post('model[brand_id]');

        if (!empty($model_name) && !empty($brand_id)) {
            $ci->db->select('*');
            $ci->db->where('name', $model_name);
            $ci->db->where('brand_id', $brand_id);
            $query = $ci->db->get('car_models')->row_array();
            if (count($query) > 0) {
                return false;
            } else {
                return true;
            }
        }
    }
    function check_category_code(){
        $ci = & get_instance();
        $code = $ci->input->post('Category[code]');
        $encrypted_id = trim($ci->input->post('Category[encrypted_id]'));
        if(!empty($encrypted_id)){
            $ci->db->select('*');
            $ci->db->where('code', $code);
            $ci->db->where('encrypted_id', $encrypted_id);
            $query = $ci->db->get('category_master')->row_array();
            if(!empty($query)){
                return true;
            }
        }
        if (!empty($code)) {
            $ci->db->select('*');
            $ci->db->where('code', $code);
            $query = $ci->db->get('category_master')->row_array();
            if (count($query) > 0) {
                return false;
            } else {
                return true;
            }
        }else{
            return false;
        }  
    }
    function check_category_name(){
        $ci = & get_instance();
        $name = $ci->input->post('Category[name]');
        $encrypted_id = trim($ci->input->post('Category[encrypted_id]'));
        if(!empty($encrypted_id)){
            $ci->db->select('*');
            $ci->db->where('name', $name);
            $ci->db->where('encrypted_id', $encrypted_id);
            $query = $ci->db->get('category_master')->row_array();
            if(!empty($query)){
                return true;
            }
        }
        if (!empty($name)) {
            $ci->db->select('*');
            $ci->db->where('name', $name);
            $query = $ci->db->get('category_master')->row_array();
            if (count($query) > 0) {
                return false;
            } else {
                return true;
            }
        }else{
            return false;
        }  
        
    }


    function check_buying_complexity_name(){
        $ci = & get_instance();
        $name = $ci->input->post('buying_complexity[name]');
        $encrypted_id = trim($ci->input->post('buying_complexity[encrypted_id]'));
        $ci->db->select('*');
        $ci->db->where('name', $name);
        if(!empty($encrypted_id)){
            $ci->db->where('encrypted_id !=', $encrypted_id); 
        }
        $query = $ci->db->get('buying_complexity_master')->row_array();
        if(!empty($query)){
            return false;
        } 
        return true;
    }

    function check_manufacturing_type_name(){
        $ci = & get_instance();
        $name = $ci->input->post('manufacturing_type[name]');
        $encrypted_id = trim($ci->input->post('manufacturing_type[encrypted_id]'));
        $ci->db->select('*');
        $ci->db->where('name', $name);
        if(!empty($encrypted_id)){
            $ci->db->where('encrypted_id !=', $encrypted_id); 
        }
        $query = $ci->db->get('manufacturing_type_master')->row_array();
        if(!empty($query)){
            return false;
        } 
        return true;
    }
    function check_pcs_name(){
        $ci = & get_instance();
        $name = $ci->input->post('pcs[name]');
        $encrypted_id = trim($ci->input->post('pcs[encrypted_id]'));
        $ci->db->select('*');
        $ci->db->where('name', $name);
        if(!empty($encrypted_id)){
            $ci->db->where('encrypted_id !=', $encrypted_id); 
        }
        $query = $ci->db->get('pcs_master')->row_array();
        if(!empty($query)){
            return false;
        } 
        return true;
    }

    function check_product_code(){
        $ci = & get_instance();
        $code = $ci->input->post('product[product_code]');
        $encrypted_id = trim($ci->input->post('product[encrypted_id]'));
        if(!empty($encrypted_id)){
            $ci->db->select('*');
            $ci->db->where('product_code', $code);
            $ci->db->where('encrypted_id', $encrypted_id);
            $query = $ci->db->get('product')->row_array();
            if(!empty($query)){
                return true;
            }
        }
        if (!empty($code)) {
            $ci->db->select('*');
            $ci->db->where('product_code', $code);
            $query = $ci->db->get('product')->row_array();
            if (count($query) > 0) {
                return false;
            } else {
                return true;
            }
        }else{
            return false;
        }  
        
    }

    function check_sub_category_code(){
        $ci = & get_instance();
        $code = $ci->input->post('Sub_category[code]');
        $encrypted_id = trim($ci->input->post('Sub_category[encrypted_id]'));
        if(!empty($encrypted_id)){
            $ci->db->select('*');
            $ci->db->where('code', $code);
            $ci->db->where('encrypted_id', $encrypted_id);
            $query = $ci->db->get('sub_category_master')->row_array();
            if(!empty($query)){
                return true;
            }
        }
        if (!empty($code)) {
            $ci->db->select('*');
            $ci->db->where('code', $code);
            $query = $ci->db->get('sub_category_master')->row_array();
            if (count($query) > 0) {
                return false;
            } else {
                return true;
            }
        }else{
            return false;
        }  
        
    }
 
    function check_sub_category_name(){
        $ci = & get_instance();
        $name = $ci->input->post('Sub_category[name]');
        $encrypted_id = trim($ci->input->post('Sub_category[encrypted_id]'));
        if(!empty($encrypted_id)){
            $ci->db->select('*');
            $ci->db->where('name', $name);
            $ci->db->where('encrypted_id', $encrypted_id);
            $query = $ci->db->get('sub_category_master')->row_array();
            if(!empty($query)){
                return true;
            }
        }
        if (!empty($name)) {
            $ci->db->select('*');
            $ci->db->where('name', $name);
            $query = $ci->db->get('sub_category_master')->row_array();
            if (count($query) > 0) {
                return false;
            } else {
                return true;
            }
        }else{
            return false;
        }  
        
    }
    function check_karigar_code(){
        $ci = & get_instance();
        $code = $ci->input->post('Karigar[code]');
        $encrypted_id = trim($ci->input->post('Karigar[encrypted_id]'));
        if(!empty($encrypted_id)){
            $ci->db->select('*');
            $ci->db->where('code', $code);
            $ci->db->where('encrypted_id', $encrypted_id);
            $query = $ci->db->get('karigar_master')->row_array();
            if(!empty($query)){
                return true;
            }
        }
        if (!empty($code)) {
            $ci->db->select('*');
            $ci->db->where('code', $code);
            $query = $ci->db->get('karigar_master')->row_array();
            if (count($query) > 0) {
                return false;
            } else {
                return true;
            }
        }else{
            return false;
        }  
        
    }
    function check_article_exist(){
        $ci = & get_instance();
        $name = $ci->input->post('Article[name]');
        $encrypted_id = trim($ci->input->post('Article[encrypted_id]'));
        if(!empty($encrypted_id)){
            $ci->db->select('*');
            $ci->db->where('name', $name);
            $ci->db->where('encrypted_id', $encrypted_id);
            $query = $ci->db->get('article_master')->row_array();
            if(!empty($query)){
                return true;
            }
        }
        if (!empty($name)) {
            $ci->db->select('*');
            $ci->db->where('name', $name);
            $query = $ci->db->get('article_master')->row_array();
            if (count($query) > 0) {
                return false;
            } else {
                return true;
            }
        }else{
            return false;
        }  
        
    }
    function check_department_exist(){
        $ci = & get_instance();
        $name = $ci->input->post('Department[name]');
        $encrypted_id = trim($ci->input->post('Department[encrypted_id]'));
        if(!empty($encrypted_id)){
            $ci->db->select('*');
            $ci->db->where('name', $name);
            $ci->db->where('encrypted_id', $encrypted_id);
            $query = $ci->db->get('departments')->row_array();
            if(!empty($query)){
                return true;
            }
        }
        if (!empty($name)) {
            $ci->db->select('*');
            $ci->db->where('name', $name);
            $query = $ci->db->get('departments')->row_array();
            if (count($query) > 0) {
                return false;
            } else {
                return true;
            }
        }else{
            return false;
        }  
        
    }
    function check_Sale_under_exist(){
        $ci = & get_instance();
        $ci->form_validation->set_message('check_Sale_under_exist', 'Sales under name already exists');
        $ci = & get_instance();
        $name = $ci->input->post('Sales_under[name]');
        $encrypted_id = trim($ci->input->post('Sales_under[encrypted_id]'));
        if(!empty($encrypted_id)){
            $ci->db->select('*');
            $ci->db->where('name', $name);
            $ci->db->where('encrypted_id', $encrypted_id);
            $query = $ci->db->get('sale_under')->row_array();
            if(!empty($query)){
                return true;
            }
        }
        if (!empty($name)) {
            $ci->db->select('*');
            $ci->db->where('name', $name);
            $query = $ci->db->get('sale_under')->row_array();
            if (count($query) > 0) {
                return false;
            } else {
                return true;
            }
        }else{
            return false;
        }  
        
    }
    function check_product_type_exist(){
        $ci = & get_instance();
        $name = $ci->input->post('Product_type[name]');
        $encrypted_id = trim($ci->input->post('Product_type[encrypted_id]'));
        if(!empty($encrypted_id)){
            $ci->db->select('*');
            $ci->db->where('name', $name);
            $ci->db->where('encrypted_id', $encrypted_id);
            $query = $ci->db->get('product_type')->row_array();
            if(!empty($query)){
                return true;
            }
        }
        if (!empty($name)) {
            $ci->db->select('*');
            $ci->db->where('name', $name);
            $query = $ci->db->get('product_type')->row_array();
            if (count($query) > 0) {
                return false;
            } else {
                return true;
            }
        }else{
            return false;
        }  
        
    }
    function check_metal_name(){
        $ci = & get_instance();
        $name = $ci->input->post('Metal[name]');
        $encrypted_id = trim($ci->input->post('Metal[encrypted_id]'));
        if(!empty($encrypted_id)){
            $ci->db->select('*');
            $ci->db->where('name', $name);
            $ci->db->where('encrypted_id', $encrypted_id);
            $query = $ci->db->get('metal_master')->row_array();
            if(!empty($query)){
                return true;
            }
        }
        if (!empty($name)) {
            $ci->db->select('*');
            $ci->db->where('name', $name);
            $query = $ci->db->get('metal_master')->row_array();
            if (count($query) > 0) {
                return false;
            } else {
                return true;
            }
        }else{
            return false;
        }  
        
    }
    function check_carat_name(){
        $ci = & get_instance();
        $name = $ci->input->post('Carat[name]');
        $encrypted_id = trim($ci->input->post('Metal[encrypted_id]'));
        if(!empty($encrypted_id)){
            $ci->db->select('*');
            $ci->db->where('name', $name);
            $ci->db->where('encrypted_id', $encrypted_id);
            $query = $ci->db->get('carat_master')->row_array();
            if(!empty($query)){
                return true;
            }
        }
        if (!empty($name)) {
            $ci->db->select('*');
            $ci->db->where('name', $name);
            $query = $ci->db->get('carat_master')->row_array();
            if (count($query) > 0) {
                return false;
            } else {
                return true;
            }
        }else{
            return false;
        }  
        
    }

    function check_from(){
        $ci = & get_instance();
        $from = $ci->input->post('Weight_range[from]');
        $to = $ci->input->post('Weight_range[to]');
        if(!empty($to) && $to <= $from){
            $ci->form_validation->set_message('check_from', 'To weight must be greater than from weight');
            return false;
        }
        $encrypted_id = trim($ci->input->post('Weight_range[encrypted_id]'));
        if(!empty($encrypted_id)){
           $sql = "SELECT (CASE WHEN (from_weight =".$from." AND to_weight=".$to." ) then 'true' else 'false' end ) as st FROM `weights` where encrypted_id != '".$encrypted_id."'";
            $query = $ci->db->query($sql)->result_array();
            foreach ($query as $key => $value) {
                $status[] = $value['st'];
            }
            if (in_array('true',$status)) {
                return false;
            } else {
                return true;
            }
        }
        if ($from !== '') {
             $sql = "SELECT (CASE WHEN (from_weight =".$from." AND to_weight=".$to." ) then 'true' else 'false' end ) as st FROM `weights`";
            $query = $ci->db->query($sql)->result_array();
            foreach ($query as $key => $value) {
                $status[] = $value['st'];
            }
            if (in_array('true',$status)) {
                return false;
            } else {
                return true;
            }
        }else{
            return false;
        }  
        
    }
    function check_to(){
        $ci = & get_instance();
        $from = $ci->input->post('Weight_range[from]');
        $to = $ci->input->post('Weight_range[to]');
        
       
            if($to <= $from){
                $ci->form_validation->set_message('check_to', 'To weight must be greater than from weight');
                return false;
            }
            $encrypted_id = trim($ci->input->post('Weight_range[encrypted_id]'));
            if(!empty($encrypted_id)){
               $sql = "SELECT (CASE WHEN (from_weight =".$from." AND to_weight ='".$to."') then 'true' else 'false' end ) as st FROM `weights` where encrypted_id != '".$encrypted_id."'";
                $query = $ci->db->query($sql)->result_array();

                foreach ($query as $key => $value) {
                    $status[] = $value['st'];
                }
                if (in_array('true',$status)) {
                    return false;
                } else {
                    return true;
                }
            }
            if (!empty($to)) {
                $sql = "SELECT (CASE WHEN (from_weight =".$from." AND to_weight =".$to." ) then 'true' else 'false' end ) as st FROM `weights`";
                $query = $ci->db->query($sql)->result_array();
               // echo "SELECT (CASE WHEN (from_weight ='".$from."' AND to_weight ='".$to."' ) then 'true' else 'false' end ) as st FROM `weights`"; die();
                $status[]='false';
                foreach ($query as $key => $value) {
                    $status[] = $value['st'];
                }
                if (in_array('true',$status)) {
                    return false;
                } else {
                    return true;
                }
            }else{
                return false;
            } 
                
        
    }
    function check_karigar($is_excel=false){
        $ci = & get_instance();
        $karigar_id = $ci->input->post('product[karigar_id]');
        $ci->form_validation->set_message('check_karigar', 'Karigar not exists');
        if (!empty($karigar_id)) {
            $ci->db->select('*');
            $ci->db->from('karigar_master');
            $ci->db->where('id',$karigar_id);
            //$ci->db->or_where('name',$karigar_id);
            $ci->db->or_where('code',$karigar_id);
            $query = $ci->db->get()->row_array();
            if(!empty($query)){
                if($is_excel == true)
                    return $query['id'];
                return true;
            }else{
                return false;
            }
        }
    }
    function check_sub_category($is_excel=false){
        $ci = & get_instance();
        $category_id = $ci->input->post('product[category_id]');
        $ci->db->select('*');
        $ci->db->from('category_master');
        $ci->db->where('id',$category_id);
        $ci->db->or_where('name',$category_id);
        $result = $ci->db->get()->row_array(); 
        //print_r($result['id']);       
        $sub_category_id = $ci->input->post('product[sub_category_id]');
        $ci->form_validation->set_message('check_sub_category', 'Sub Category not exists');
        if (!empty($sub_category_id) && !empty($result)) {
            $ci->db->select('*');
            $ci->db->from('sub_category_master');
            $ci->db->where('id',$sub_category_id);
            $ci->db->or_where('name',$sub_category_id);
            //$ci->db->or_where('category_id' ,$result['id']);
            $query = $ci->db->get()->row_array();
           // print_r($sub_category_id .'=s='.$query['category_id'] .'=='.$result['id']);die;
            if(!empty($query) && $query['category_id'] == $result['id'] ){
                if($is_excel == true)
                    return $query['id'];
                return true;
            }else{
                return false;
            }
        }

        if (!empty($sub_category_id)) {
            $ci->db->select('*');
            $ci->db->from('sub_category_master');
            $ci->db->where('id',$sub_category_id);
            $ci->db->or_where('name',$sub_category_id);
            $query = $ci->db->get()->row_array();
            if(!empty($query)){
                if($is_excel == true)
                    return $query['id'];
                return true;
            }else{
                return false;
            }
        }
    }
    function check_product_type($is_excel=false){
        $ci = & get_instance();
        $product_type_id = $ci->input->post('product[product_type_id]');
        $ci->form_validation->set_message('check_product_type', 'Product type not exists');
        if (!empty($product_type_id)) {
            $ci->db->select('*');
            $ci->db->from('product_type');
            $ci->db->where('id',$product_type_id);
            $ci->db->or_where('name',$product_type_id);
            $query = $ci->db->get()->row_array();
            if(!empty($query)){
                if($is_excel == true)
                    return $query['id'];
                return true;
            }else{
                return false;
            }
        }
    }
    function check_manufacturing_type($is_excel=false){
        $ci = & get_instance();
        $product_type_id = $ci->input->post('product[manufacturing_type_id]');
        $ci->form_validation->set_message('check_manufacturing_type', 'Manufacturing type not exists');
        if (!empty($product_type_id)) {
            $ci->db->select('*');
            $ci->db->from('manufacturing_type_master');
            $ci->db->where('id',$product_type_id);
            $ci->db->or_where('name',$product_type_id);
            $query = $ci->db->get()->row_array();
            if(!empty($query)){
                if($is_excel == true)
                    return $query['id'];
                return true;
            }else{
                return false;
            }
        }
    }
    function check_pcs($is_excel=false){
        $ci = & get_instance();
        $product_type_id = $ci->input->post('product[pcs_id]');
        $ci->form_validation->set_message('check_pcs', 'Pcs not exists');
        if (!empty($product_type_id)) {
            $ci->db->select('*');
            $ci->db->from('pcs_master');
            $ci->db->where('id',$product_type_id);
            $ci->db->or_where('name',$product_type_id);
            $query = $ci->db->get()->row_array();
            if(!empty($query)){
                if($is_excel == true){
                    return $query['id'];
                }
                return true;
            }else{
                return false;
            }
        }
    }
    function check_buying_complexity($is_excel=false){
        $ci = & get_instance();
        $buying_complexity_id = $ci->input->post('product[buying_complexity_id]');
        $ci->form_validation->set_message('check_buying_complexity', 'Buying Complexity not exists');
        if (!empty($buying_complexity_id)) {
            $ci->db->select('*');
            $ci->db->from('buying_complexity_master');
            $ci->db->where('id',$buying_complexity_id);
            $ci->db->or_where('name',$buying_complexity_id);
            $query = $ci->db->get()->row_array();
            if(!empty($query)){
                if($is_excel == true)
                    return $query['id'];
                return true;
            }else{
                return false;
            }
        }
    }
    function check_article($is_excel=false){
        $ci = & get_instance();
        $article_id = $ci->input->post('product[article_id]');
        $ci->form_validation->set_message('check_article', 'Article not exists');
        if (!empty($article_id)) {
            $ci->db->select('*');
            $ci->db->from('article_master');
            $ci->db->where('id',$article_id);
            $ci->db->or_where('name',$article_id);
            $query = $ci->db->get()->row_array();
            if(!empty($query)){
                if($is_excel == true)
                    return $query['id'];
                return true;
            }else{
                return false;
            }
        }
    }
    function check_carat($is_excel=false){
        $ci = & get_instance();
        $carat_id = $ci->input->post('product[carat_id]');
        $ci->form_validation->set_message('check_carat', 'Carat not exists');
        if (!empty($carat_id)) {
            $ci->db->select('*');
            $ci->db->from('carat_master');
            $ci->db->where('id',$carat_id);
            $ci->db->or_where('name',$carat_id);
            $query = $ci->db->get()->row_array();
            if(!empty($query)){
                if($is_excel == true)
                    return $query['id'];
                return true;
            }else{
                return false;
            }
        }
    }
    function check_metal($is_excel=false){
        $ci = & get_instance();
        $metal_id = $ci->input->post('product[metal_id]');
        $ci->form_validation->set_message('check_metal', 'Metal not exists');
        if (!empty($metal_id)) {
            $ci->db->select('*');
            $ci->db->from('metal_master');
            $ci->db->where('id',$metal_id);
            $ci->db->or_where('name',$metal_id);
            $query = $ci->db->get()->row_array();
            if(!empty($query)){
                if($is_excel == true)
                    return $query['id'];
                return true;
            }else{
                return false;
            }
        }
    }
    function check_weight_range($is_excel=false){

        $ci = & get_instance();
        $weight_range_id = $ci->input->post('product[weight_band_id]');
        $ci->form_validation->set_message('check_weight_range', 'Weight range not exists');
        $weight_range_id = explode('-',$weight_range_id);
        if (!empty($weight_range_id)) {
            $ci->db->select('*');
            $ci->db->from('weights');
            $ci->db->where('id',$weight_range_id[0]);
            $whr = '';
            if(!empty($weight_range_id[0]) && !empty($weight_range_id[1]))
            {
                $whr = '(from_weight = '.(float)trim($weight_range_id[0]).' AND to_weight = '.(float)trim($weight_range_id[1]).')';
                $ci->db->or_where($whr);
            }
         
            $query = $ci->db->get()->row_array();
            if(!empty($query)){
                if($is_excel == true){
                    return $query['id'];
                }
                return true;
            }else{
                return false;
            }
        }
    }
    function check_category($is_excel=false){
        $ci = & get_instance();
        $category_id = $ci->input->post('product[category_id]');
        $ci->form_validation->set_message('check_category', 'Category not exists');
        if (!empty($category_id)) {
            $ci->db->select('*');
            $ci->db->from('category_master');
            $ci->db->where('id',$category_id);
            $ci->db->or_where('name',$category_id);
            $query = $ci->db->get()->row_array();
            if(!empty($query)){
                if($is_excel == true)
                    return $query['id'];
                return true;
            }else{
                return false;
            }
        }
    }

    function check_manufacturing_quantity(){
        $ci = & get_instance();
        $quantity = $ci->input->post('quantity');
        $ci->form_validation->set_message('check_manufacturing_quantity', 'Quantity exceeded');
        if(!empty($quantity)){
            $ci->db->select('mop.quantity,sum(kmm.quantity) as allocate_qnty');
            $ci->db->from('manufacturing_order_mapping mop');
            $ci->db->join('Karigar_manufacturing_mapping kmm','kmm.mop_id=mop.id','left');
            $ci->db->where('mop.id',$ci->input->post('mop_id'));
            $ci->db->group_by('mop.id');
            $result = $ci->db->get()->row_array();
            if(($quantity + $result['allocate_qnty']) > $result['quantity']){
                return false;
            }else{
                return true;
            }
        }else{
            return false;
        }
    }
    function check_kmm_quantity(){
        $ci = & get_instance();
        $quantity = $ci->input->post('kmm_quantity');
        if($quantity=="0" || $quantity<0 || strpos($quantity, ".") !== false){
            $ci->form_validation->set_message('check_kmm_quantity', 'Please enter valid Quantity.');
            return false;
        }
        else{
            $ci->form_validation->set_message('check_kmm_quantity', 'Quantity exceeded.');
            if(!empty($quantity)){
                $ci->db->select('kmm.quantity,sum(rp.quantity) as allocate_qnty');
                $ci->db->from('Karigar_manufacturing_mapping kmm');
                $ci->db->join('Receive_products rp','kmm.id=rp.kmm_id','left');
                $ci->db->where('kmm.id',$ci->input->post('kmm_id'));
                $ci->db->group_by('kmm.id');
                $result = $ci->db->get()->row_array();
                if(($quantity + $result['allocate_qnty']) > $result['quantity']){
                    return false;
                }else{
                    return true;
                }
            }else{
                return false;
            }
        }
    }

    function quantity_validation(){
        $ci = & get_instance();
        $quantity = $ci->input->post('quantity');
        if($quantity=="0" || $quantity<0 || strpos($quantity, ".") !== false){
            return false;
        }
        return true;
    }
    function check_empty_value(){
        $ci = & get_instance();
        $ci->form_validation->set_message('check_empty_value', 'Weight/Weight range value must not be zero');
        $postdata = $ci->input->post('product');
        if($postdata['weight_band_id'] <= 0.00000){
            return false;
        }else{
            return true;
        }
    }
    public function check_order_exist(){
        $ci = & get_instance();
        $ci->form_validation->set_message('check_order_exist', 'Order Id not exists');
        $postdata = $ci->input->post();
        $ci->db->select('*');
        $ci->db->from('orders');
        $ci->db->where('id',$postdata['order_id']);
        $result = $ci->db->get()->row_array();
        if(count($result) > 0){
            return true;
        }else{
            return false;
        }

    }

    public function check_hallmarkcenter(){
         $ci = & get_instance();
        $all_data = $ci->input->post('hallmarking_center');

        if(!empty($all_data['name'])){
            $ci->db->where('name', $all_data['name']);
            if (isset($all_data['encrypted_id']) && !empty($all_data['encrypted_id'])) {
              $ci->db->where('encrypted_id !=', $all_data['encrypted_id']);
            }
            $query = $ci->db->get('hallmarking_center')->result_array();
            if(count($query) != 0){
                return false;
            }
        }
    }
    
    public function check_parent_category(){
         $ci = & get_instance();
        $all_data = $ci->input->post('parent_category');

        if(!empty($all_data['name'])){
            $ci->db->where('name', $all_data['name']);
            if (isset($all_data['encrypted_id']) && !empty($all_data['encrypted_id'])) {
              $ci->db->where('encrypted_id !=', $all_data['encrypted_id']);
            }
            $query = $ci->db->get('parent_category')->result_array();
            if(count($query) != 0){
                return false;
            }
        }
    }

    public function check_parent_category_code(){
         $ci = & get_instance();
         $all_data = $ci->input->post();

            $query=0;
                if (!empty($_POST['parent_category_id'])) {
                    $ci->db->where('code_name', $all_data['product_code']);
                    $ci->db->where('parent_category_id', $all_data['parent_category_id']);
                    $ci->db->where_not_in('id', $all_data['parent_category']['ids']);
                    $query = $ci->db->get('parent_category_codes')->result_array();
                    $query=count($query);        
                  }
                
          

                $cnt =-1;
                foreach ($all_data['parent_category']['code'] as $key => $value) {
                    if ($all_data['product_code']==$value) {
                        $cnt +=1;
                    }
                }
            
            if($cnt != 0 || $query >=1){
                return false;
            }else{
                return true;
            }
        }

    public function check_product_category(){
         $ci = & get_instance();
        $all_data = $ci->input->post('product_category');

        if(!empty($all_data['name'])){
            $ci->db->where('name', $all_data['name']);
            // $ci->db->where('parent_category', $all_data['parent_category']);
            $ci->db->where('department_id', $all_data['department_id']);
            if (isset($all_data['encrypted_id']) && !empty($all_data['encrypted_id'])) {
              $ci->db->where('encrypted_id !=', $all_data['encrypted_id']);
            }
            $query = $ci->db->get('product_category')->result_array();
            if(count($query) != 0){
                return false;
            }
        }
    }
       public function check_product_category_rate_name(){
         $ci = & get_instance();
        $all_data = $ci->input->post('product_category');

        if(!empty($all_data['name'])){
            $ci->db->where('name', $all_data['name']);         
            $query = $ci->db->get('product_category_rate')->result_array();
            if(count($query) != 0){
                return false;
            }
        }
    }
    function check_product_available(){
        $ci = & get_instance();
        $ci->form_validation->set_message('check_product_available', 'This product is already selected');
        $ci = & get_instance();
        $qc_id = $ci->input->post('sub_category_id');
        $ci->db->select('*');
        $ci->db->from('quality_control');
        $ci->db->where('id',$qc_id);
        if ($_GET['status']=="hallmarking") {
            $ci->db->where('status','9');
        }else{
             $ci->db->where('status','8');
        }
        $result = $ci->db->get()->row_array();
        if(count($result) > 0){
            return true;
        }else{
            return false;
        }
    }

    function check_color_stone(){
         $ci = & get_instance();
         $all_data = $ci->input->post('color_stone');

        if(!empty($all_data['name'])){
            $ci->db->where('name', $all_data['name']);
            if (isset($_POST['encrypted_id']) && !empty($_POST['encrypted_id'])) {
              $ci->db->where('encrypted_id !=', $_POST['encrypted_id']);
            }
            $query = $ci->db->get('color_stone')->result_array();
            if(count($query) != 0){
                return false;
            }else{
                return true;
            }
        }
    }

    function check_kundan_category(){
        $ci = & get_instance();
        $all_data = $ci->input->post('kundan_category');

        if(!empty($all_data['name'])){
            $ci->db->where('name', $all_data['name']);
            if (isset($_POST['encrypted_id']) && !empty($_POST['encrypted_id'])) {
              $ci->db->where('encrypted_id !=', $_POST['encrypted_id']);
            }
            $query = $ci->db->get('kundan_category')->result_array();
            if(count($query) != 0){
                return false;
            }else{
                return true;
            }
        }
    }

    function check_type_master(){
        $ci = & get_instance();
        $all_data = $ci->input->post('type_master');

        if(!empty($all_data['name'])){
            $ci->db->where('name', $all_data['name']);
            if (isset($_POST['encrypted_id']) && !empty($_POST['encrypted_id'])) {
              $ci->db->where('encrypted_id !=', $_POST['encrypted_id']);
            }
            $query = $ci->db->get('type_master')->result_array();
            if(count($query) != 0){
                return false;
            }else{
                return true;
            }
        }
    }

    
    function check_hallmarking_dispatch_qty(){
        $ci = & get_instance();
        $all_data = $ci->input->post('hallmarking');

            $ci->db->select('drp.quantity');
            $ci->db->where('sid.id',$all_data['sid_id']);
            $ci->db->from('sales_invoice_details sid');
            $ci->db->join('sale_stock ss','sid.stock_id=ss.id','left');
            $ci->db->join('department_ready_product drp','ss.drp_id=drp.id','left');
            $query = $ci->db->get()->row_array();

            $ci->db->select('SUM(qch.quantity) as total_qty');
            $ci->db->where('qch.sid_id',$all_data['sid_id']);
            $ci->db->from('quality_control_hallmarking qch');
            $result = $ci->db->get()->row_array();
           
            $max_limit=$query['quantity']-@$result['total_qty'];
            
        if($all_data['quantity'] > $max_limit){
            return false;
        }else{
            return true;
        }
     }

    public function check_hallmarking_received_qty(){
        $ci = & get_instance();
        $all_data = $ci->input->post('hallmarking');

        $ci->db->select('qch.quantity');
        $ci->db->where('qch.id',$all_data['qch_id']);
        $ci->db->from('quality_control_hallmarking qch');
        $result = $ci->db->get()->row_array();

            $max_limit=@$result['quantity'];

        if($all_data['quantity'] > $max_limit){
            return false;
        }else{
            return true;
        }
    }

    public function check_pc_wr(){
    $ci = & get_instance();
    $all_data = $ci->input->post('product');
      if (empty($_POST['check_array'])) {
           $_POST['check_array']=array();
      }
      if (in_array(array($all_data['parent_category'],$all_data['weight']), $_POST['check_array'])) {
          return FALSE;
      }else{
        $_POST['check_array'][]=array($all_data['parent_category'],$all_data['weight']);
        return true;
      }

    }

 function check_exits_customer_type(){
     $ci = & get_instance();
        $all_data = $ci->input->post('customer_type');

        if(!empty($all_data['name'])){
            $ci->db->where('name', $all_data['name']);
            if (isset($_POST['encrypted_id']) && !empty($_POST['encrypted_id'])) {
              $ci->db->where('encrypted_id !=', $_POST['encrypted_id']);
            }
            $query = $ci->db->get('customer_type')->result_array();
            if(count($query) != 0){
                return false;
            }else{
                return true;
            }
        }
    }

    function check_mob_no_saleman(){
        $ci = & get_instance();
        $all_data = $ci->input->post('salesmans');

        if(!empty($all_data['mobile_no'])){
            $ci->db->where('mobile_no', $all_data['mobile_no']);
            if (isset($_POST['encrypted_id']) && !empty($_POST['encrypted_id'])) {
              $ci->db->where('encrypted_id !=', $_POST['encrypted_id']);
            }
            $query = $ci->db->get('salesmans_master')->result_array();
            if(count($query) != 0){
                return false;
            }else{
                return true;
            }
        }
    }
    function check_qc_qnt(){
        $ci = & get_instance();
        $ci->db->select('rp.quantity,sum(qc.quantity) qc_quantity,sum(qch.quantity) qch_quantity,sum(qc.hm_quantity) qc_hm_quantity');
        $ci->db->from('Receive_products rp');
        $ci->db->join('quality_control qc','qc.receive_product_id = rp.id','left');
        $ci->db->join('quality_control_hallmarking qch','qc.id = qch.qc_id','left');
        $ci->db->where('rp.id',$_POST['receive_product_id']);
        $result = $ci->db->get()->row_array();
        if(($result['qc_quantity'] + $_POST['quantity']) > $result['quantity']){
            return false;
        }else{
            return true;
        }
    }
    function check_qc_wt(){
        $ci = & get_instance();
        $ci->db->select('rp.weight,sum(qc.weight) qc_weight');
        $ci->db->from('Receive_products rp');
        $ci->db->join('quality_control qc','qc.receive_product_id = rp.id','left');
        $ci->db->where('rp.id',$_POST['receive_product_id']);
        $result = $ci->db->get()->row_array();
        $a =($result['qc_weight'] + $_POST['bom_weight']) ;
        if(($result['qc_weight'] + $_POST['bom_weight']) > $result['weight']){
            return false;
        }else{
            return true;
        }
    }

    function check_short_code(){
        $ci = & get_instance();
        $all_data = $ci->input->post('material');

        if(!empty($all_data['short_code'])){
            $ci->db->where('short_code', $all_data['short_code']);
            if (isset($_POST['encrypted_id']) && !empty($_POST['encrypted_id'])) {
              $ci->db->where('encrypted_id !=', $_POST['encrypted_id']);
            }
            $query = $ci->db->get('material_master')->result_array();
            if(count($query) != 0){
                return false;
            }else{
                return true;
            }
        }
    }

    function check_Variations_category(){
        $ci = & get_instance();
        $all_data = $ci->input->post('Variations_category');

        if(!empty($all_data['name'])){
            $ci->db->where('name', $all_data['name']);
            if (isset($_POST['encrypted_id']) && !empty($_POST['encrypted_id'])) {
              $ci->db->where('encrypted_id !=', $_POST['encrypted_id']);
            }
            $query = $ci->db->get('variation_category')->result_array();
            if(count($query) != 0){
                return false;
            }else{
                return true;
            }
        }
    }
    function check_qc_quantity(){
        $ci = & get_instance();
        $ci->form_validation->set_message('check_qc_quantity', 'Quantity Exceeded');
        if($_POST['quantity'] > $_POST['original_qty']){
            return false;
        }else{
            return true;
        }
    }

    function check_qc_weight(){
        $ci = & get_instance();
        $ci->form_validation->set_message('check_qc_weight', 'Weight Exceeded');
        if($_POST['weight'] > $_POST['original_wt']){
            return false;
        }else{
            return true;
        }
    }

    

    function check_term_exists(){
       $ci = & get_instance();
       $all_data = $ci->input->post('terms');

        if(!empty($all_data['parent_category_id'])){
            $ci->db->where('parent_category_id', $all_data['parent_category_id']);
            if (isset($_POST['encrypted_id']) && !empty($_POST['encrypted_id'])) {
              $ci->db->where('encrypted_id !=', $_POST['encrypted_id']);
            }
            $query = $ci->db->get('terms_master')->result_array();
            if(count($query) != 0){
                return false;
            }else{
                return true;
            }
        }
    }

    function check_customer_assign_qty(){
        $ci = & get_instance();
        $all_data = $ci->input->post();
        $result = $ci->db->get_where('customer_orders',array('id'=>$all_data['order_id']))->row_array();
        if ($all_data['quantity'] > $result['remaining_qty']) {
           return false;
        }else{
            return true;
        }
    }

    public function check_received_qty(){
        $ci = & get_instance();
        $all_data = $ci->input->post('test_data');
        $result = $ci->db->get_where('customer_order_assign',array('id'=>$all_data['id']))->row_array();
        if ($all_data['quantity'] > $result['remaining_qty']) {
           return false;
        }else{
            return true;
        }
    }
    public function check_customer_mobile_exists(){
        $ci = & get_instance();
        $ci->form_validation->set_message('check_customer_mobile_exists', 'Mobile No already exists');
        $all_data = $ci->input->post('customer_order');
        $ci->db->select('*');
        $ci->db->from('customer');
        $ci->db->where('mobile_no',$all_data['mobile_no']);
        $ci->db->where('customer_type_id','2');
        if(!empty($all_data['customer_id']))
            $ci->db->where('id !=',$all_data['customer_id']);
        $result = $ci->db->get()->row_array();
        //echo $ci->db->last_query();
        if(!empty($result)){
            return false;
        }else{
            return true;
        }
    }
    public function check_customer_email_exists(){
        $ci = & get_instance();
        $ci->form_validation->set_message('check_customer_email_exists', 'Email Id already exists');
        $all_data = $ci->input->post('customer_order');
        $ci->db->select('*');
        $ci->db->from('customer');
        $ci->db->where('email',$all_data['email_id']);
        $ci->db->where('customer_type_id','2');
        if(!empty($all_data['customer_id']))
            $ci->db->where('id !=',$all_data['customer_id']);
        $result = $ci->db->get()->row_array();
        if(!empty($result)){
            return false;
        }else{
            return true;
        }
    }
    public function order_qnt_vald(){
        $ci = & get_instance();
        $ci->form_validation->set_message('order_qnt_vald', 'Quantity Exceeded');
        $ci->db->select('remaining_qty');
        $ci->db->from('customer_orders');
        $ci->db->where('id',$_POST['id']);
        $result = $ci->db->get()->row_array();
        if($_POST['qnt'] > $result['remaining_qty']){
            return false;
        }else{
            return true;
        }
    }
    public function order_net_valdn(){
        $ci = & get_instance();
        $ci->db->select('weight_range_id');
        $ci->db->from('customer_orders');
        $ci->db->where('id',$_POST['id']);
        $result = $ci->db->get()->row_array();
        $weight_range_id = $result['weight_range_id'];
        $calc = $weight_range_id*15/100;
        $max_limit_weight =$weight_range_id+$calc;
        $low_limit_weight =$weight_range_id-$calc;
        $ci->form_validation->set_message('order_net_valdn','Weight Not Matched Should be between '.$low_limit_weight.' - '.$max_limit_weight);


        if ($_POST['net_wt'] > $max_limit_weight || $_POST['net_wt'] < $low_limit_weight  ) {
            return false;
        }else{
            return true;
        }
    }
    function received_products_qnt_val(){
        $ci = & get_instance();
        $ci->form_validation->set_message('received_products_qnt_val', 'Quantity Exceeded');
        $ci->db->select('sum(rp.quantity) quantity');
        $ci->db->from('Receive_products rp');
        $ci->db->where('rp.corporate_product_id', $_POST['corporate_product_id']);
        $ci->db->where('rp.order_id', $_POST['order_id']);
        $ci->db->where('rp.status!=','0');
        $query = $ci->db->get()->row_array();
        $ci->db->select('sum(ppl.quantity) quantity');
        $ci->db->from('Prepare_product_list ppl');
        $ci->db->join('corporate_products cp','cp.id = ppl.corporate_products_id');
        $ci->db->where('cp.id', $_POST['corporate_product_id']);
        $ci->db->where('cp.order_id', $_POST['order_id']);
        $quantity = $ci->db->get()->row_array();
        if (($quantity['quantity'] - @$query['quantity']) < $_POST['quantity'] ) {
          return false;
        }
        else{
            return true;
        }
    }

    function check_department_code(){
        $ci = & get_instance();
        $department_id = $ci->input->post('customer_order[department_id]'); 
        if (!empty($department_id)) {
            $ci->db->select('*');
            $ci->db->where('id', $department_id);
            $query = $ci->db->get('departments')->row_array();
            //echo $ci->db->last_query(); echo count($query);die;
            if (count($query) == 0) {
                return false;
            } else {
       
                return true;
            }
        }else{
            return false;
        }  
        
    }

    function check_parent_category_id_code(){
        $ci = & get_instance();
        $name = $ci->input->post('customer_order[parent_category_id]'); 
        if (!empty($name)) {
            $ci->db->select('*');
            $ci->db->where('name', $name);
            $query = $ci->db->get('parent_category')->row_array();
            //echo $ci->db->last_query();die;
            if (count($query) == 0) {
                return false;
            } else {
                return true;
            }
        }else{
            return false;
        }  
        
    }
    function check_delivery_date(){
        $ci = & get_instance();
        $ci->form_validation->set_message('check_delivery_date', 'Order date must be leass than delivery date');
        $postdata = $ci->input->post('customer_order');
        
        if(strtotime($postdata['order_date']) >= strtotime($postdata['delievery_date'])){
            return false;
        }else{
            return true;
        }
    }

     function check_already_exist(){
        $ci = & get_instance();
        $postData=$ci->input->post();
        $ci->db->select('*');
        $ci->db->where('email', $postData['email']);
        if($postData['id'] != ""){
           $ci->db->where('id !=', $postData['id']);    
        }
        $query = $ci->db->get('customer')->row_array();
        if (count($query) > 0) {
            return FALSE;
        } else {
            return TRUE;
        } 
    }  
    

    function checkFORGOTEMAIL(){
        $ci = & get_instance();
        $postData=$ci->input->post();
        $ci->db->select('*');
        $ci->db->where('email',$postData['forgot']['forgot_email']);
        $query = $ci->db->get('customer')->row_array();
        if (count($query) > 0) {
            return TRUE;
        } else {
            return FALSE;
        } 
    }
    /* few weight box*/
    public function check_product_few_wt_exists(){
    $ci = & get_instance();
    $ci->form_validation->set_message('check_product_few_wt_exists', 'Product already exists');
    $all_data = $ci->input->post('few_box');
    $ci->db->select('*');
    $ci->db->from('few_weight');
    $ci->db->where('product_id',$all_data['product']);
   
    $result = $ci->db->get()->row_array();
   /* echo $ci->db->last_query();
    print_r($result);die;*/
    if(!empty($result)){
        return false;
    }else{
        return true;
    }
    }

}//class