<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Listing {
  public function __construct(){
    $this->ci = & get_instance();
		$this->default_column='';
		$this->group_by='';
		$this->order_by='';
		$this->tables='';
		$this->actionFunction='';
		$this->search_url='';
		$this->headingFunction='';
		$this->theadColumn='';
		$this->limit='';
		$this->orderData='';
		$this->page_no='';
		$this->sql='';
		$this->html='';
		$this->where='';
		$this->extra_select_column='';
		$this->page_url='';
		$this->order_url='';
		$this->url='';
		$this->select_url= '';
		$this->pagination = '';
		$this->getData = '';
		$this->join_type = '';
		$this->join_columns = array();
		$this->record = array();
		$this->where_ids = array();
		$this->ci->load->helper(array('action'),array('populate_table_param'));
  }

  	function initalized($param){
	  	$this->extra_select_column = @$param['extra_select_column'];
	  	$this->default_column = $param['default_column'];
	  	$this->tables = $param['table'];
	  	$this->actionFunction=$param['actionFunction'];
	  	$this->headingFunction=$param['headingFunction'];
	  	$this->search_url=$param['search_url'];
	  	$this->where=$param['where'];
	  
	  	isset($param['limit']) ? $this->limit=$param['limit'] : $this->limit="10";
	  	isset($param['join_type']) ? $this->join_type=$param['join_type'] : $this->join_type="";
	  	isset($param['group_by']) ? $this->group_by=$param['group_by'] : $this->group_by="";
	  	isset($param['order_by']) ? $this->group_by=$param['order_by'] : $this->order_by="";
	  	isset($param['join_columns']) ? $this->join_columns=$param['join_columns'] : $this->join_columns=array();
  		if(!empty($param['new_headers'])){
			$this->theadColumn = $param['new_headers'];
		}
		else{
			$headingFunction = $this->headingFunction;
			$this->theadColumn = $headingFunction();
		}
		$getData = $this->getData();
		$this->url = $getData['url'];
		$this->select_url= $getData['select_url'];
		$this->orderData = $getData['orderData'];
		$this->where_ids=$param['where_ids'];
		$this->page_no = $getData['page_no'];
		$this->page_url = $getData['page_url'];
		$this->order_url = $getData['order_url'];
		$this->url = $getData['url'];
		$this->select_url = $getData['select_url'];
		$this->getData = $getData['getData'];
 	}

 	public function getRecordCounts($param){
   		$this->initalized($param);
		return $this->recordCount();

   	}
	
	public function getRecords($param,$extension,$export=false){
		if($extension!=''){
			$export = true;
		}
		$this->initalized($param);
		if($export && $extension == '.xls'){
			$this->getExelData();
			exit;
		}
		else{
			$this->crateThead();
			$this->createTbody();
			$pagination_html = $this->createPagination();
			$filter_html = $this->filterPopupHtml();
			$searched_html = $this->filterHtml();
			return array('html' => $this->html,'pagination_html' => $pagination_html,'filter_html' => $filter_html, 'searched_html' => $searched_html, 'excel_url' => $this->order_url."&excel=export",'thead'=>$this->thead,'url'=> $this->url,'select_url'=> $this->select_url,'table_name'=>$this->tables,'pagination'=>$this->pagination);
		}
	}

	function getExelData(){
		$actual_excel_data=array();
		$res = $this->recordData($this->orderData,'','',true);
		$excel_data = $this->create_excel_array($res);
		foreach ($excel_data as $key => $value) {
			if(isset($value['contractor_count_report']) && $value['contractor_count_report']==null){
				$value['contractor_count_report'] = 0;
			}
			if(isset($value['wr_count_report']) && $value['wr_count_report']==null){
				$value['wr_count_report'] = 0;
			}
			$actual_excel_data[$key]=$value;
		}
		$header= array();
		foreach ($this->theadColumn as $key => $value) {
			print_r($this->tables[0]);die;
			if($value['1']!='checkbox' && $value['0']!='Action' && $value['0']!='Company Logo' && $value['0']!='School Logo' && $value['0']!='Profile Photo' && $value['0']!='Personal Files' && $value['0']!='Manufacturer Logo'  )
			$header[]=  $value['0'] ;
		}
		$this->ci->load->library('excel_lib');
		$this->ci->excel_lib->export($actual_excel_data,$header,$this->tables.'.xlsx');	
	}

	function create_excel_array($data){
		$ignore_array = array('Company Logo','School Logo','Profile Photo','Personal Files','Manufacturer Logo','checkbox');
		foreach ($data as $key => $value) {
			foreach ($this->theadColumn as $h_key => $h_value) {
				if(array_key_exists($h_value[1], $value) && !in_array($h_value[0], $ignore_array)){
					$result[$key][$h_value[1]] = $value[$h_value[1]];
				}
			}
		}
		return $result;
	}

	function getData(){
		$getData = $this->ci->input->get();

		if(!isset($getData['page_no']) || empty($getData['page_no']))
       		$page_no = "1";
	    else
	    	$page_no = $getData['page_no'];
	    if(isset($getData['order_column']) && !empty($getData['order_column']))
	       $orderData[$getData['order_column']] = $getData['order_by'];
	    else{
	      	$getData['order_column'] = $this->default_column;
	      	$getData['order_by'] = "desc";
	      	$orderData[$this->default_column] = 'desc';
	    }
	    $page_url = ADMIN_PATH.$this->search_url."?1=1";
	    $order_url = ADMIN_PATH.$this->search_url."?order_column=".$getData['order_column']."&order_by=".$getData['order_by'];
	    $url = ADMIN_PATH.$this->search_url."?order_column=".$getData['order_column']."&order_by=".$getData['order_by']."&page_no=".$page_no;
	    $select_url = ADMIN_PATH.$this->search_url."?order_column=".$getData['order_column']."&order_by=".$getData['order_by']."&page_no=".$page_no;
	    unset($getData['page_no']);
	    unset($getData['order_column']);
	    unset($getData['order_by']);
	    foreach($getData as $k => $v){

	    	//print_r($k);
	      $page_url.="&".$k."=".$v;
	      $order_url.="&".$k."=".$v;
	      $url.="&".$k."=".$v;
	      if($k!="remove_id"){
	      	$select_url.="&".$k."=".$v;
	      }
	    }
	    //die;

	    return array("getData" => $getData, "orderData" => $orderData, "page_url" => $page_url, "order_url" => $order_url, "page_no" => $page_no, "url" => $url, "select_url" => $select_url);
	}
	
	function crateThead(){
		$this->thead = '';
		$theadColumn2 =$this->theadColumn;
		foreach($this->theadColumn as $k => $heading){			
			$order_by = $this->getOrderHtml($heading);
			$filter_by = $this->getFilterHtml($heading,$k);
			$this->thead[$k][0]=$order_by;
			$this->thead[$k][1]=$filter_by;
			$this->thead[$k][2]=$heading[0];
		}
	}
	
	function getFilterHtml($heading,$k){
		$filter_by = "";
		if($heading[4])
			$filter_by='<a href="javascript:void(0);" onclick="searchpopup(\''.ADMIN_PATH.'\',\''.$k.'\',\''.$this->headingFunction.'\',\''.$this->search_url.'\');"><i class="fa fa-filter" aria-hidden="true"></i></a>';
		return $filter_by;
	}
	
	function getOrderHtml($heading){
		$order_by = "";
		//print_r($this->page_url);
		if($heading[2]){
			if(isset($this->orderData[$heading[3]]) && $this->orderData[$heading[3]]=='desc')
				$order_by='<a href="'.$this->page_url.'&order_column='.$heading[3].'&order_by=asc"><i class="fa fa-sort-amount-desc" aria-hidden="true"></i></a>';
			else if(isset($this->orderData[$heading[3]]))
				$order_by='<a href="'.$this->page_url.'&order_column='.$heading[3].'&order_by=desc"><i class="fa fa-sort-amount-asc" aria-hidden="true"></i></a>';
			else
				$order_by='<a href="'.$this->page_url.'&order_column='.$heading[3].'&order_by=asc"><i class="fa fa-sort" aria-hidden="true"></i></a>';
		}
		return $order_by;
	}
	
	function createTbody(){
		$this->record = $this->recordData();
		$this->html=$this->record; 
	}
	
	function filterQuery(){
		if(!empty($this->where)){
			$this->ci->db->where($this->where);
		}
		if(!empty($this->where_ids)){
			$this->ci->db->where_in($this->where_ids[0],$this->where_ids[1]);
		}
		$getData = $this->ci->input->get();
		foreach($this->theadColumn as $heading){
			if(isset($getData[$heading[1]]) && trim($getData[$heading[1]])!=""){
				if($heading[1] == 'id'){
					$this->ci->db->where($heading[1],$getData[$heading[1]]);
				}
				else{
					if(isset($heading[6]))
						$like_column = explode(" ", $heading[6]);
					else
						$like_column[0] = $heading[1];
					$this->ci->db->like($like_column[0],$getData[$heading[1]]);
				}
			}
		}
	}
	
	function orderQuery(){
		foreach($this->orderData as $column => $order){
			$this->ci->db->order_by($column,$order);
		}
	}

	function getSelectcolumn(){
		$select = '';
		foreach ($this->theadColumn as $column) {
			
			if($column[0]!='Action' && $column[0]!='<input value="0" type="checkbox" id="check_all" class="pull-left">'){
				if(isset($column[6])){
					$select.=$column[6].',';
					}
				else{
					$select.=$column[1].',';
					
				}
			}

		
		}
		if(!empty($this->extra_select_column))
			$select.=$this->extra_select_column.',';
		return (empty(trim($select,','))) ? '*' : trim($select,',');
	}
	
	function selectQuery(){
		$select = $this->getSelectcolumn();
		//print_r($select);
		$this->ci->db->select($select);
		if(is_array($this->tables)){
			$this->ci->db->from($this->tables[0]);
			foreach ($this->tables as $key => $table) {
				if($key!="0"){
					$this->ci->db->join($table,$this->join_columns[$key-1],$this->join_type);
				}
			}
		}
		else
			$this->ci->db->from($this->tables);
	}
	
	function limitQuery(){
		$start = ($this->page_no-1)*$this->limit;
		$this->ci->db->limit($this->limit,$start);
	}
	
	function recordData($export=false){
		$this->selectQuery();
		$this->filterQuery();
		$this->orderQuery();
		if(!empty($this->group_by)){
			$this->ci->db->group_by($this->group_by);
		}if(!empty($this->order_by)){
			$this->ci->db->order_by($this->order_by,'asc');
		}
		if(!$export){
			$this->limitQuery();
		}
		//echo $this->ci->db->last_query(); exit;
		return $this->ci->db->get()->result_array();
	}
	
	function recordCount(){
		$this->selectQuery($this->tables);
		$this->filterQuery($this->theadColumn);
		if(!empty($this->group_by)){
			$this->ci->db->group_by($this->group_by);
		}
		return sizeOf($this->ci->db->get()->result_array());
	}
	
	function createPagination(){
		$pagination_parameters['order_url'] = $this->order_url;
		$pagination_parameters['page_no'] = $this->page_no;
		$pagination_parameters['showing'] = (($this->page_no-1)*$this->limit)+1;
        $pagination_parameters['prev_page_id'] =   $this->page_no-1;
        $pagination_parameters['next_page_id'] =  $this->page_no+1;
        $pagination_parameters['end'] = (($this->page_no-1)*$this->limit)+sizeof($this->record);
        $pagination_parameters['count'] = $this->recordCount($this->theadColumn);
        $pagination_parameters['pages'] = ceil($pagination_parameters['count']/$this->limit);
        $this->pagination = $pagination_parameters;
	}
	
	function truncateTableColumn($desc,$max_lenght,$min_lenght){
   	$lenght = strlen($desc); 
	    if($lenght > $max_lenght){
	      return '<span class="lessContent">'.substr($desc, '0',$min_lenght).'...'.'</span><span style="display:none;" class="moreContent">'.$desc.'</span>&nbsp;<span><a class="lessMoreContent" href="javascript:void(0);">More</a></span>';
	    }
	    else{
	      return $desc;
	    }
	}

	function filterPopupHtml(){
		$html = '';
		if(!empty($this->getData)){
			$result['param'] = $this->getData;
			$result['heading'] = $this->theadColumn;
			$html = $this->ci->load->view('master/search_popup/index',$result,true);
		}
		return $html;
	}

	function filterHtml(){
		$html = '';
		if(!empty($this->getData)){
			$result['param'] = $this->getData;
			$result['heading'] = $this->theadColumn;
			$html = $this->ci->load->view('master/search_popup/view',$result,true);
		}
		return $html;
	}

}
