<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class View 
{
  public $current_site = "";
  public function __construct() {
    $this->ci = & get_instance();

    //$this->current_site = $_SERVER['REQUEST_URI'];

  }

  public function render($view, $data = array()){
    // print_r($view);die;
    $this->loadHeader($data);
    $this->loadNavigation($data);
    $this->loadContentView($view);
    $this->loadFooter();
  }

  private function loadHeader($data){
    //if(!empty($_SESSION))
      //$data['role_id'] = $_SESSION['user_id'];
      $this->ci->load->view('common/header',$data);        
  }
  private function loadNavigation($data){
    $this->ci->db->select('acm.controller_id,c.display_name,c.name,c.sequence,mm.display_name master_display_name,c.status,mm.icon,c.master_id');
    $this->ci->db->from('admin_controllers_mapping acm');
    $this->ci->db->join('controllers c','acm.controller_id=c.id');
    $this->ci->db->join('menu_master mm','mm.id=c.master_id');
    $this->ci->db->where('acm.admin_id',$_SESSION['user_id']);
    //$this->ci->db->where('c.status','1');
    $this->ci->db->order_by('mm.sequence,c.sequence');
    $result = $this->ci->db->get()->result_array();
    foreach ($result as $key => $value) {
      if($value['status'] == '1')
        $data['navigation'][$value['master_display_name']][] = $value;
        $data['list_menus'][$value['master_display_name']][] = strtolower($value['name']);
    }
    $this->ci->load->view('common/navigation',$data);        
  }
  public function loadActiveNavigation($name){
    $this->ci->db->select('c.master_id');
    $this->ci->db->from('controllers c');
    $this->ci->db->where('c.name',$name);
    $result = $this->ci->db->get()->row_array();
    return @$result['master_id'];
       
  }

  private function loadContentView($view){
    $this->ci->load->view($view);
  }
  
  private function loadFooter(){
    $this->ci->load->view('common/footer');    
  }
}
