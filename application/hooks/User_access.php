<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class User_access{
	protected $ci;
	public function __construct(){
        global $CI;
        $this->ci = $CI;

  }

	public function index(){
		$result = $this->get_user_access();
		if(!empty($result)){
			foreach ($result as $key => $value) {
				$controllers[] = $value['name'];
			}
			
			$controllers = $this->filter_data($controllers);
			//print_r($controllers);die;
			if(!in_array(strtolower($this->ci->uri->segment(1)),$controllers) && !empty($this->ci->uri->segment(1))){
				echo "You don't have a permission to access this page";die;
			}else{
				//$this->create_bedcrumbs();
			}
		}
	}
	private function get_user_access(){

		$this->ci->db->select('acm.*,c.name,c.display_name,mm.default_controller,mm.bedcrum_name');
		$this->ci->db->from('admin_controllers_mapping acm');
		$this->ci->db->join('controllers c','c.id = acm.controller_id','left');
		$this->ci->db->join('menu_master mm','c.master_id = mm.id','left');
		$this->ci->db->where('acm.admin_id',$this->ci->session->userdata('user_id'));
		$result = $this->ci->db->get()->result_array();
		/*echo $this->ci->db->last_query();
		die;*/
		return $result;
	}
	private function filter_data($array){
		array_push($array, 'auth');
		array_push($array, 'Auth');
		array_push($array, 'sejal_antique_accounting');
		array_push($array, 'v1');
	    $array = array_filter($array, 'strlen');  //removes null values but leaves "0"
	    $array = array_filter($array);
	    $strtolower = array_map('strtolower',$array);
	    $strtolower = array_map('trim',$strtolower);
	      return $strtolower;
  	}
  	private function create_bedcrumbs(){
  		$uri_path = parse_url($_SERVER['REQUEST_URI'], PHP_URL_PATH);
		$uri_segments = explode('/', $uri_path);
		unset($uri_segments[0]);
		unset($uri_segments[1]);
		unset($uri_segments[4]);
		$controllers = $this->get_user_access();
		$uri_segments = array_values(array_filter($uri_segments));
		//$uri_segments = $this->check_integer($uri_segments);
		$parent_bedcrum='';
		for($i=0;$i<count($uri_segments);$i++){
			foreach ($controllers as $key => $value) {
				if($uri_segments[$i] == $value['name']){
					if(!empty($value['bedcrum_name']) && $uri_segments[$i] !== $value['default_controller']){
						$parent_bedcrum = $value['default_controller'].'/';
						$this->ci->breadcrumbs->push( $value['bedcrum_name'],  $value['default_controller']);
					}
					$display_name = $value['display_name'];
				}else{
					$display_name = str_replace('_', ' ', $uri_segments[$i]);
				}
			}
			if($i==0){
				$this->ci->breadcrumbs->push($display_name, $parent_bedcrum.$uri_segments[$i]);
			}
			elseif($i>0)
				$this->ci->breadcrumbs->push($display_name, $uri_segments[$i-1].'/'.$uri_segments[$i]);
		}
  	}
}
?>