
if (page_title == "Notification") {
    $('#notificationTable').DataTable({
        "search":true,
        "info":false,
       "bLengthChange": false,
        "bFilter": false,
        "bSort": false,
        "bProcessing": false,
        "serverSide": true,
        "ajax": {
            data: {list: 'list'},
            url: url + "notification/getNotification",
            type: "post",
            error: function () {
            }
        },
        "oLanguage": {
            "sEmptyTable": "<i>No Data Found.</i>"
        },
        "fnDrawCallback": function() {

            //$('.dataTables_filter input').attr('placeholder','Search Order here...')

            //$('#Receive_pieces_filter input').attr('placeholder','Search Order here...')

        }
    });
}

if (page_title == "Due Date Reminder") {
    var dataTable=  $('#corporate_due_date_reminder').DataTable({
        "bLengthChange": false,
        "bFilter" : true,
        "bSort": false,
        "Filter": true,
        "Processing": true,
        "serverSide": true,
          "pageLength": 50,
       
        "ajax": {
            data: {list: 'list'},
            url: url + "Corporate_Dashboard/reminder/"+order_reminder+"?filter="+order_reminder_filter,
            type: "post",
            error: function () {
              console.log('its error')
            }
        },
       "language": {
            "sEmptyTable": "<i>No Data Found.</i>",
            "search": '<i class="fa fa-search"></i>',
            "searchPlaceholder": "Order Name/ Customer Name"
        },
        "aoColumnDefs": [
                    { 'bSortable': false, 'aTargets': [] }
                ]
    });

  $('.dataTables_filter').remove();
  filterFunction(dataTable);

}

if (page_title == "View Imported ARF") {
    $('#importedarfTable').DataTable({
        "search":true,
        "info":false,
       "bLengthChange": false,
        bFilter: false,
        "bSort": false,
        "bProcessing": false,
        "serverSide": true,
        "ajax": {
            data: {list: 'list'},
            url: url + "arf_list/getImportARF",
            type: "post",
            error: function () {
            }
        },
        "oLanguage": {
            "sEmptyTable": "<i>No Data Found.</i>"
        },
        "fnDrawCallback": function() {
             $('.dataTables_paginate').hide();
             $('[data-toggle="tooltip"]').tooltip();
            var page_length=0;
            $('.paginate_button').each(function(i,e){
                page_length=i;
            });
            if(page_length>=3){
                $('.dataTables_paginate').show();
            }
        }
    });
}
$.fn.DataTable.ext.pager.numbers_length = 4;

if (page_title == "Plan Activity") {
    var dataTable=$('#planTable').DataTable({
        "info":false,
       "bLengthChange": false,
        bFilter: true,
        "Filter":true,
        "bSort": false,
        "bProcessing": false,
        "serverSide": true,
        "pageLength": 50,
        
        "ajax": {
            data: {list: 'list','status_f':''},
            url: url + "activity",
            type: "post",
            error: function () {
            }
        },
        "oLanguage": {
            "sEmptyTable": "<i>No Data Found.</i>"
        },
        "fnDrawCallback": function() {
             $('.dataTables_paginate').hide();
             $('[data-toggle="tooltip"]').tooltip();
            var page_length=0;
            $('.paginate_button').each(function(i,e){
                page_length=i;
            });
            if(page_length>=3){
                $('.dataTables_paginate').show();
            }
        }
    });
    filterFunction(dataTable);
}

function filterFunction(dataTable)
{
    $('.search-input-text').on( 'keyup click', function () {   // for text boxes
        var i =$(this).attr('filtercol');  // getting column index
        var v =$(this).val();  // getting search input value
        /*if(v!="")*/
            dataTable.columns(i).search(v).draw();
    } );
    $('.search-input-select').on('change', function () {   // for select box
        var i =$(this).attr('filtercol');
        var v =$(this).val();
         dataTable.columns(i).search(v).draw();
    } );
    $('.search-input-option').on('change', function () {   // for select box
        var i =$(this).attr('filtercol');
        var v =$(this).val();
        /*if(v!="")*/
            dataTable.columns(i).search(v).draw();
    } );
    $('.search-input-date').on( 'keyup click change', function () {
    var i =$(this).attr('filtercol');  // getting column index
    var v =$(this).val();  // getting search input value
    dataTable.columns(i).search(v).draw();
    } );
    $('.corporate_id-input-select').on('change', function () {   // for select box
     var corporate_id=$("#corporate_id").val();
    if(corporate_id =='2'){
        $("#amended_btn").hide();
    }else{
        $("#amended_btn").show();
    }
 });
}

// function filterCheckboxFunction(table_name){
//     var asd ='';
//     $('#'+table_name+' input[type="checkbox"]:checked').each(function() {
//           asd = $(this).val()+',';
//     });   
//     console.log(asd);
//        $('#selected_checkbox').val(asd);

//     var selected;
//     // selected = chkArray.join(',') ;
//     alert(asd);


// }


$('.toggle-vis').on( 'click', function (e) {
   id=$(this).attr('data-column');
   var column = dataTable.column(".col"+id);
    // Toggle the visibility
    column.visible( ! column.visible() );    
} );
if (page_title == "All Catagories") {
  var dataTable= $('#Catagory').DataTable({
        "bLengthChange": false,
        "bFilter" : true,
        "bSort": false,
        "Filter": true,
        "Processing": true,
        "serverSide": true,
        "ajax": {
            data: {list: 'list'},
            url: url + "Category/index",
            type: "post",
            error: function () {
              console.log('its error')
            }
        },
        "language": {
            "sEmptyTable": "<i>No Data Found.</i>",
            "search": '<i class="fa fa-search"></i>',
            "searchPlaceholder": "Search Category"
        },
        "aoColumnDefs": [
                    { 'bSortable': false, 'aTargets': [] }
                ]
    });
  $('.dataTables_filter').remove();
  filterFunction(dataTable);
}

if (page_title == "All Buying Complexity") {
  var dataTable= $('#buying_complexity').DataTable({
        "bLengthChange": false,
        "bFilter" : true,
        "bSort": false,
        "Filter": true,
        "Processing": true,
        "serverSide": true,
        "pageLength": 50,
        "ajax": {
            data: {list: 'list'},
            url: url + "Buying_complexity/index",
            type: "post",
            error: function () {
              console.log('its error')
            }
        },
        "language": {
            "sEmptyTable": "<i>No Data Found.</i>",
            "search": '<i class="fa fa-search"></i>',
            "searchPlaceholder": "Search Buying Complexity"
        },
        "aoColumnDefs": [
                    { 'bSortable': false, 'aTargets': [] }
                ]
    });
   $('.dataTables_filter').remove();
  filterFunction(dataTable);
}

if (page_title == "All Manufacturing Type") {
  var dataTable= $('#manufacturing_type').DataTable({
        "bLengthChange": false,
        "bFilter" : true,
        "bSort": false,
        "Filter": true,
        "Processing": true,
        "serverSide": true,
        "pageLength": 50,
       
        "ajax": {
            data: {list: 'list'},
            url: url + "Manufacturing_type/index",
            type: "post",
            error: function () {
              console.log('its error')
            }
        },
        "language": {
            "sEmptyTable": "<i>No Data Found.</i>",
            "search": '<i class="fa fa-search"></i>',
            "searchPlaceholder": "Search Manufacturing Type"
        },
        "aoColumnDefs": [
                    { 'bSortable': false, 'aTargets': [] }
                ]
    });
    $('.dataTables_filter').remove();
  filterFunction(dataTable);
}
if (page_title == "All PCS") {
  var dataTable= $('#pcs').DataTable({
        "bLengthChange": false,
        "bFilter" : true,
        "bSort": false,
        "Filter": true,
        "Processing": true,
        "serverSide": true,
        "pageLength": 50,
       
        "ajax": {
            data: {list: 'list'},
            url: url + "Pcs/index",
            type: "post",
            error: function () {
              console.log('its error')
            }
        },
        "language": {
            "sEmptyTable": "<i>No Data Found.</i>",
            "search": '<i class="fa fa-search"></i>',
            "searchPlaceholder": "Search PCS"
        },
        "aoColumnDefs": [
                    { 'bSortable': false, 'aTargets': [] }
                ]
    });
   $('.dataTables_filter').remove();
  filterFunction(dataTable);
}
if (page_title == "All Users") {
  var dataTable= $('#user_access').DataTable({
        "bLengthChange": false,
        "bFilter" : true,
        "bSort": false,
        "Filter": true,
        "Processing": true,
        "serverSide": true,
        "pageLength": 50,
       
        "ajax": {
            data: {list: 'list'},
            url: url + "User/index",
            type: "post",
            error: function () {
              console.log('its error')
            }
        },
        "language": {
            "sEmptyTable": "<i>No Data Found.</i>",
            "search": '<i class="fa fa-search"></i>',
            "searchPlaceholder": "Search Users"
        },
        "aoColumnDefs": [
                    { 'bSortable': false, 'aTargets': [] }
                ]
    });
   $('.dataTables_filter').remove();
  filterFunction(dataTable);
}

if(page_title == "DESIGNS WITH IMAGES" || page_title == "ALL PRODUCTS" || page_title == "ADD PRODUCTS IMAGES" || page_title == "DESIGNS WITHOUT IMAGES")  { 
    if(page_title == "ADD PRODUCTS IMAGES")
        controller = 'add_product_images';
    else if(page_title == "DESIGNS WITH IMAGES")
         controller = 'Design/Design_with_images';
    else if(page_title == "DESIGNS WITHOUT IMAGES")
        controller = 'Design/Design_without_images';
    else
         controller = 'Design';
     // alert();
  var dataTable= $('#viewProduct').DataTable({
        "bLengthChange": false,
        "bFilter" : true,
        "bSort": false,
        "Filter": true,
        "Processing": true,
        "serverSide": true,
        "scrollx":true,
        "pageLength": 50,
        "ajax": {
            data: {list: 'list',page_title:page_title},
            url: url + controller,
            type: "post",
            error: function () {
              console.log('its error')
            }
        },
       "language": {
            "sEmptyTable": "<i>No Data Found.</i>",
            "search": '<i class="fa fa-search"></i>',
            "searchPlaceholder": "Product Code/Date/Karigar "
        },
        "aoColumnDefs": [
                    { 'bSortable': false, 'aTargets': [] }
                ]
    });
    var dataTable= $('#viewProductwithoutimg').DataTable({
        "bLengthChange": false,
        "bFilter" : true,
        "bSort": false,
        "Filter": true,
        "Processing": true,
        "serverSide": true,
        "scrollx":true,
        "ajax": {
            data: {list: 'list',page_title:page_title},
            url: url + controller,
            type: "post",
            error: function () {
              console.log('its error')
            }
        },
       "language": {
            "sEmptyTable": "<i>No Data Found.</i>",
            "search": '<i class="fa fa-search"></i>',
            "searchPlaceholder": "Product Code/Date/Karigar "
        },
        "aoColumnDefs": [
                    { 'bSortable': false, 'aTargets': [] }
                ]
    });
 

   
   $('.dataTables_filter').remove();
  filterFunction(dataTable);
 
}

if (page_title == "All Sub Catagories") {
  var dataTable= $('#sub_catagory').DataTable({
        "bLengthChange": false,
        "bFilter" : true,
        "bSort": false,
        "Filter": true,
        "Processing": true,
        "serverSide": true,
        "ajax": {
            data: {list: 'list'},
            url: url + "Sub_category/index",
            type: "post",
            error: function () {
              console.log('its error')
            }
        },
        "language": {
            "sEmptyTable": "<i>No Data Found.</i>",
            "search": '<i class="fa fa-search"></i>',
            "searchPlaceholder": "Search Sub Category Name"
        },
        "aoColumnDefs": [
                    { 'bSortable': false, 'aTargets': [] }
                ]
    });
  $('.dataTables_filter').remove();
  filterFunction(dataTable);
}
if (page_title == "All Karigars") {
  var dataTable= $('#Karigar').DataTable({
        "bLengthChange": false,
        "bFilter" : true,
        "bSort": false,
        "Filter": true,
        "Processing": true,
        "serverSide": true,
        "ajax": {
            data: {list: 'list'},
            url: url + "Karigar/index",
            type: "post",
            error: function () {
              console.log('its error')
            }
        },
        "language": {
            "sEmptyTable": "<i>No Data Found.</i>",
            "search": '<i class="fa fa-search"></i>',
            "searchPlaceholder": "Search Karigar Name"
        },
        "aoColumnDefs": [
                    { 'bSortable': false, 'aTargets': [] }
                ]
    });
  filterFunction(dataTable);
}

if (page_title == "All Karigars Merge Replace") {
  var dataTable= $('#karigar_replace').DataTable({
        "bLengthChange": false,
        "bFilter" : true,
        "bSort": false,
        "Filter": true,
        "Processing": true,
        "serverSide": true,
        "ajax": {
            data: {list: 'list'},
            url: url + "Karigar/merge_replace_karigar_list/"+karigar_id,
            type: "post",
            error: function () {
              console.log('its error')
            }
        },
        "language": {
            "sEmptyTable": "<i>No Data Found.</i>",
            "search": '<i class="fa fa-search"></i>',
            "searchPlaceholder": "Search Karigar Name"
        },
        "aoColumnDefs": [
                    { 'bSortable': false, 'aTargets': [] }
                ]
    });
  filterFunction(dataTable);
}

if (page_title == "All Weight Ranges") {
  var dataTable= $('#weights').DataTable({
        "bLengthChange": false,
        "bFilter" : true,
        "bSort": false,
        "Filter": true,
        "Processing": true,
        "serverSide": true,
        "ajax": {
            data: {list: 'list'},
            url: url + "Weight_range/index",
            type: "post",
            error: function () {
              console.log('its error')
            }
        },
        "language": {
            "sEmptyTable": "<i>No Data Found.</i>",
            "search": '<i class="fa fa-search"></i>',
            "searchPlaceholder": "Search Weight Range"
        },
        "aoColumnDefs": [
                    { 'bSortable': false, 'aTargets': [] }
                ]
    });
    $('.dataTables_filter').remove();
  filterFunction(dataTable);
}
if (page_title == "All Articles") {
  var dataTable= $('#Article').DataTable({
        "bLengthChange": false,
        "bFilter" : true,
        "bSort": false,
        "Filter": true,
        "Processing": true,
        "serverSide": true,
        "ajax": {
            data: {list: 'list'},
            url: url + "Article/index",
            type: "post",
            error: function () {
              console.log('its error')
            }
        },
       "language": {
            "sEmptyTable": "<i>No Data Found.</i>",
            "search": '<i class="fa fa-search"></i>',
            "searchPlaceholder": "Search Article Name"
        },
        "aoColumnDefs": [
                    { 'bSortable': false, 'aTargets': [] }
                ]
    });
   $('.dataTables_filter').remove();
  filterFunction(dataTable);
}

if (page_title == "All Product Type") {
  var dataTable= $('#Product_type').DataTable({
        "bLengthChange": false,
        "bFilter" : true,
        "bSort": false,
        "Filter": true,
        "Processing": true,
        "serverSide": true,
        "ajax": {
            data: {list: 'list'},
            url: url + "Product_type/index",
            type: "post",
            error: function () {
              console.log('its error')
            }
        },
        "language": {
            "sEmptyTable": "<i>No Data Found.</i>",
            "search": '<i class="fa fa-search"></i>',
            "searchPlaceholder": "Search Product Type"
        },
        "aoColumnDefs": [
                    { 'bSortable': false, 'aTargets': [] }
                ]
    });
  filterFunction(dataTable);
}
if (page_title == "All Metals") {
  var dataTable= $('#Metal').DataTable({
        "bLengthChange": false,
        "bFilter" : true,
        "bSort": false,
        "Filter": true,
        "Processing": true,
        "serverSide": true,
        "ajax": {
            data: {list: 'list'},
            url: url + "Metal/index",
            type: "post",
            error: function () {
              console.log('its error')
            }
        },
        "language": {
            "sEmptyTable": "<i>No Data Found.</i>",
            "search": '<i class="fa fa-search"></i>',
            "searchPlaceholder": "Search Metal Name"
        },
        "aoColumnDefs": [
                    { 'bSortable': false, 'aTargets': [] }
                ]
    });
  filterFunction(dataTable);
}
if (page_title == "All Carats") {
  var dataTable= $('#Carat').DataTable({
        "bLengthChange": false,
        "bFilter" : true,
        "bSort": false,
        "Filter": true,
        "Processing": true,
        "serverSide": true,
        "ajax": {
            data: {list: 'list'},
            url: url + "Carat/index",
            type: "post",
            error: function () {
              console.log('its error')
            }
        },
        "language": {
            "sEmptyTable": "<i>No Data Found.</i>",
            "search": '<i class="fa fa-search"></i>',
            "searchPlaceholder": "Search Carat Name"
        },
        "aoColumnDefs": [
                    { 'bSortable': false, 'aTargets': [] }
                ]
    });
  $('.dataTables_filter').remove();

  filterFunction(dataTable);
}

if (page_title == "Receive Designs") {

  var dataTable= $('#show_pieces').DataTable({
        "bLengthChange": false,
        "bFilter" : true,
        "bSort": false,
        "Filter": true,
        "Processing": true,
        "serverSide": true,
        "ajax": {
            data: {list: 'list'},
            url: url + "Receive_pieces/show/"+order_id,
            type: "post",
            error: function () {
              console.log('its error')
            }
        },
        "language": {
            "sEmptyTable": "<i>No Data Found.</i>",
            "search": '<i class="fa fa-search"></i>',
            "searchPlaceholder": "Search Karigar Name"
        },
        "aoColumnDefs": [
                    { 'bSortable': false, 'aTargets': [] }
                ]
    });
  filterFunction(dataTable);
}

if (page_title == "Rejected Designs") {
  var dataTable= $('#Rejected_products').DataTable({
        "bLengthChange": false,
        "bFilter" : true,
        "bSort": false,
        "Filter": true,
        "Processing": true,
        "serverSide": true,
        "ajax": {
            data: {list: 'list'},
            //url: url + "Rejected_Design/show/"+order_id,
            url: url + "Rejected_Design/show/"+vendor,            
            type: "post",
            error: function () {
              console.log('its error')
            }
        },
        "language": {
            "sEmptyTable": "<i>No Data Found.</i>",
            "search": '<i class="fa fa-search"></i>',
            "searchPlaceholder": "Search Corporate"
        },
        "aoColumnDefs": [
                    { 'bSortable': false, 'aTargets': [] }
                ]
    });
  filterFunction(dataTable);
}

if (page_title == "View Designs To Receive Pieces") {
  var dataTable= $('#Receive_pieces').DataTable({
        "bLengthChange": false,
        "bFilter" : true,
        "bSort": false,
        "Filter": true,
        "Processing": true,
        "serverSide": true,
        "ajax": {
            data: {list: 'list'},
            url: url + "Receive_pieces/index/"+vendor,
            type: "post",
            error: function () {
              console.log('its error')
            }
        },
        "language": {
            "sEmptyTable": "<i>No Data Found.</i>",
            "search": '<i class="fa fa-search"></i>',
            "searchPlaceholder": "Search Order Id"
        },
        "aoColumnDefs": [
                    { 'bSortable': false, 'aTargets': [] }
                ]
    });
  filterFunction(dataTable);
}
if (page_title == "Approved Designs") {
  var dataTable= $('#show_approved_pieces').DataTable({
        "bLengthChange": false,
        "bFilter" : true,
        "bSort": false,
        "Filter": true,
        "Processing": true,
        "serverSide": true,
        "ajax": {
            data: {list: 'list'},
            url: url + "Approved_Design/show/"+order_id,
            type: "post",
            error: function () {
              console.log('its error')
            }
        },
        "language": {
            "sEmptyTable": "<i>No Data Found.</i>",
            "search": '<i class="fa fa-search"></i>',
            "searchPlaceholder": "Search Design Code"
        },
        "aoColumnDefs": [
                    { 'bSortable': false, 'aTargets': [] }
                ]
    });
  filterFunction(dataTable);
}
if (page_title == "All Approved Designs") {
  var dataTable= $('#Approved_products').DataTable({
        "bLengthChange": false,
        "bFilter" : true,
        "bSort": false,
        "Filter": true,
        "Processing": true,
        "serverSide": true,
        "ajax": {
            data: {list: 'list'},
            url: url + "Approved_Design/index/"+vendor,
            type: "post",
            error: function () {
              console.log('its error')
            }
        },
       "language": {
            "sEmptyTable": "<i>No Data Found.</i>",
            "search": '<i class="fa fa-search"></i>',
            "searchPlaceholder": "Search order ID/ Date"
        },
        "aoColumnDefs": [
                    { 'bSortable': false, 'aTargets': [] }
                ]
    });
  filterFunction(dataTable);
}
if (page_title == "All Rejected Designs") {
  var dataTable= $('#rejected_products').DataTable({
        "bLengthChange": false,
        "bFilter" : true,
        "bSort": false,
        "Filter": true,
        "Processing": true,
        "serverSide": true,
        "ajax": {
            data: {list: 'list'},
            url: url + "Rejected_Design/index/"+vendor,
            type: "post",
            error: function () {
              console.log('its error')
            }
        },
        "language": {
            "sEmptyTable": "<i>No Data Found.</i>",
            "search": '<i class="fa fa-search"></i>',
            "searchPlaceholder": "Search Order Id"
        },
        "aoColumnDefs": [
                    { 'bSortable': false, 'aTargets': [] }
                ]
    });
  filterFunction(dataTable);
}
if (page_title == "SEND DESIGNS FOR APPROVAL") {
  var dataTable= $('#search_products').DataTable({
        "bLengthChange": false,
        "bFilter" : true,
        "bSort": false,
        "Filter": true,
        "Processing": true,
        "serverSide": true,
        "scrollx" : true,
        "ajax": {
            data: {list: 'list'},
            url: url + "Send_for_approval/index",
            type: "post",
            error: function () {
              console.log('its error')
            }
        },
        "language": {
            "sEmptyTable": "<i>No Data Found.</i>",
            "search": '<i class="fa fa-search"></i>',
            "searchPlaceholder": "Search Karigar Name"
        },
        "aoColumnDefs": [
                    { 'bSortable': false, 'aTargets': [] }
                ]
    });
  filterFunction(dataTable);
}
if (page_title == "PREPARE PRODUCT LIST") {
  var dataTable= $('#Karigar_product_list').DataTable({
        "bLengthChange": false,
        "bFilter" : true,
        "bSort": false,
        "Filter": true,
        "Processing": true,
        "serverSide": true,
        "ajax": {
            data: {list: 'list'},
            url: url + "Karigar_product_list/index",
            type: "post",
            error: function () {
              console.log('its error')
            }
        },
       "language": {
            "sEmptyTable": "<i>No Data Found.</i>",
            "search": '<i class="fa fa-search"></i>',
            "searchPlaceholder": "Search Karigar Name"
        },
        "aoColumnDefs": [
                    { 'bSortable': false, 'aTargets': [1,2,3] }
                ]
    });
  $('.dataTables_filter').remove();
  filterFunction(dataTable);
}
if (page_title == "ENGAGED KARIGAR LIST") {
 if(get =='due_date'){var colum=9;  var sort=[3,4,5,6,7,8,9];}else{var colum=7; var sort=[2,3,4,5,6,7];}

  var dataTable= $('#Engaged_karigar_list').DataTable({
        "bLengthChange": false,
        "bFilter" : true,
        "bSort": false,
        "pageLength": 50,
       
        "Filter": true,
        "Processing": true,
        "serverSide": true,

        "ajax": {
            data: {list: 'list'},
            url: url + "Engaged_karigar_list/index?status="+get,
            type: "post",
            error: function () {
              console.log('its error')
            }
        },
       "language": {
            "sEmptyTable": "<i>No Data Found.</i>",
            "search": '<i class="fa fa-search"></i>',
            "searchPlaceholder": "search by karigar Name"
        },
       
        "aoColumnDefs": [
                    { 'bSortable': false, 'aTargets': sort }
                ]  

    });

   var dataTable= $('#Due_date_tracking_list').DataTable({
        "bLengthChange": false,
        "bFilter" : true,
        "bSort": false,
        "pageLength": 50,
       
        "Filter": true,
        "Processing": true,
        "serverSide": true,

        "ajax": {
            data: {list: 'list'},
            url: url + "Engaged_karigar_list/index?status="+get,
            type: "post",
            error: function () {
              console.log('its error')
            }
        },
       "language": {
            "sEmptyTable": "<i>No Data Found.</i>",
            "search": '<i class="fa fa-search"></i>',
            "searchPlaceholder": "search by karigar Name"
        },
       
        "aoColumnDefs": [
                    { 'bSortable': false, 'aTargets': sort }
                ]  

    });

   $('.dataTables_filter').remove();
  filterFunction(dataTable);


}

if (page_title == "Due Date Tracking List") {


   var dataTable= $('#due_date_tracking').DataTable({
        "bLengthChange": false,
        "bFilter" : true,
        "bSort": false,
        "pageLength": 50,
       
        "Filter": true,
        "Processing": true,
        "serverSide": true,

        "ajax": {
            data: {list: 'list'},
            url: url + "due_date_reminder?status="+get,
            type: "post",
            error: function () {
              console.log('its error')
            }
        },
       "language": {
            "sEmptyTable": "<i>No Data Found.</i>",
            "search": '<i class="fa fa-search"></i>',
            "searchPlaceholder": "search by karigar Name"
        },
       
        "aoColumnDefs": [
                    { 'bSortable': false, 'aTargets': sort }
                ]  

    });
$('.dataTables_filter').remove();
  filterFunction(dataTable);
     
}


if( page_title == "Engaged Karigar Details")
{
    var dataTable= $('#Engaged_karigar_Details_list').DataTable({
        "bLengthChange": false,
        "bFilter" : true,
        "bSort": false,
        "Filter": true,
        "Processing": true,
        "serverSide": true,
        "ajax": {
            data: {list: 'list'},
            url: url + "Engaged_karigar_list/show/"+karigar_id,
            type: "post",
            error: function () {
              console.log('its error')
            }
        },
       "language": {
            "sEmptyTable": "<i>No Data Found.</i>",
            "search": '<i class="fa fa-search"></i>',
            "searchPlaceholder": "Search Karigar Name"
        },
        "aoColumnDefs": [
                    { 'bSortable': false, 'aTargets': [] }
                ]

    });
    $('.dataTables_filter').remove();
    filterFunction(dataTable);
    
}
if( page_title == "MANUFACTURING ENGAGED KARIGAR LIST")
{
    var dataTable= $('#Manufacturing_Engaged_karigar_list').DataTable({
        "bLengthChange": false,
        "bFilter" : true,
        "bSort": false,
        "Filter": true,
        "Processing": true,
        "serverSide": true,
         "pageLength": 50,
       
        "ajax": {
            data: {list: 'list'},
            url: url + "Manufacturing_engaged_karigar",
            type: "post",
            error: function () {
              console.log('its error')
            }
        },
       "language": {
            "sEmptyTable": "<i>No Data Found.</i>",
            "search": '<i class="fa fa-search"></i>',
            "searchPlaceholder": "Search Karigar Name"
        },
        "aoColumnDefs": [
                    { 'bSortable': false, 'aTargets': [] }
                ]

    });

    var dataTable= $('#Mfg_Eng_karigar_bom_list').DataTable({
        "bLengthChange": false,
        "bFilter" : true,
        "bSort": false,
        "Filter": true,
        "Processing": true,
        "serverSide": true,
         "pageLength": 50,
       
        "ajax": {
            data: {list: 'list'},
            url: url + "Manufacturing_engaged_karigar",
            type: "post",
            error: function () {
              console.log('its error')
            }
        },
       "language": {
            "sEmptyTable": "<i>No Data Found.</i>",
            "search": '<i class="fa fa-search"></i>',
            "searchPlaceholder": "Search Karigar Name"
        },
        "aoColumnDefs": [
                    { 'bSortable': false, 'aTargets': [] }
                ]

    });

   
        $('.dataTables_filter').remove();
  filterFunction(dataTable);
  
    
}
if( page_title == "MANUFACTURING ENGAGED KARIGAR DETAILS")
{  

    var dataTable= $('#manufacturing_details_of_engaged_karigar_list').DataTable({
        "bLengthChange": false,
        "bFilter" : true,
        "bSort": false,
        "Filter": true,
        "Processing": true,
        "serverSide": true,
         "pageLength": 50,
       
        "ajax": {
            data: {list: 'list'},
            url: url + "Manufacturing_engaged_karigar/reprint_order/"+karigar_id,
            type: "post",
            error: function () {
              console.log('its error')
            }
        },
       "language": {
            "sEmptyTable": "<i>No Data Found.</i>",
            "search": '<i class="fa fa-search"></i>',
            "searchPlaceholder": "Search Karigar Name"
        },
        "aoColumnDefs": [
                    { 'bSortable': false, 'aTargets': [] }
                ]

    });

    var dataTable= $('#mfg_details_eng_karigar_bom_list').DataTable({
        "bLengthChange": false,
        "bFilter" : true,
        "bSort": false,
        "Filter": true,
        "Processing": true,
        "serverSide": true,
         "pageLength": 50,
       
        "ajax": {
            data: {list: 'list'},
            url: url + "Manufacturing_engaged_karigar/reprint_order/"+karigar_id,
            type: "post",
            error: function () {
              console.log('its error')
            }
        },
       "language": {
            "sEmptyTable": "<i>No Data Found.</i>",
            "search": '<i class="fa fa-search"></i>',
            "searchPlaceholder": "Search Karigar Name"
        },
        "aoColumnDefs": [
                    { 'bSortable': false, 'aTargets': [] }
                ]

    });
        $('.dataTables_filter').remove();
  filterFunction(dataTable);
  
    
}

if (page_title == "All SENT PRODUCTS") {
  var dataTable= $('#Products_sent').DataTable({
        "bLengthChange": false,
        "bFilter" : true,
        "bSort": false,
        "Filter": true,
        "pageLength": 50,
       
        "Processing": true,
        "serverSide": true,
        "ajax": {
            data: function ( d ) {
                d.corporate = $('#corporate_id').val();
                d.list='list';
                    } ,
            url: url + "Products_sent/index",
            type: "post",
            error: function () {
              console.log('its error')
            }
        },
       "language": {
            "sEmptyTable": "<i>No Data Found.</i>",
            "search": '<i class="fa fa-search"></i>',
            "searchPlaceholder": "Search Order Id/Order Date"
        },

        "aoColumnDefs": [
                    { 'bSortable': false, 'aTargets': [] }
                    
                ]
   
    });
    $('.dataTables_filter').remove();
  filterFunction(dataTable);
}
if (page_title == "SENT PRODUCTS") {
  var dataTable= $('#Products_sent_by_order').DataTable({
        "bLengthChange": false,
        "bFilter" : true,
        "bSort": false,
        "Filter": true,
        "Processing": true,
        "serverSide": true,
        "ajax": {
            data: {list: 'list'},
            url: url + "Products_sent/show/"+order_id,
            type: "post",
            error: function () {
              console.log('its error')
            }
        },
       "language": {
            "sEmptyTable": "<i>No Data Found.</i>",
            "search": '<i class="fa fa-search"></i>',
            "searchPlaceholder": "search by order ID /Date"
        },
        "aoColumnDefs": [
                    { 'bSortable': false, 'aTargets': [3,4] }
                ]
    });
   $('.dataTables_filter').remove();
  filterFunction(dataTable);
}
if (page_title == "All Receive Products") {

  var dataTable= $('#Receive_products').DataTable({
        "bLengthChange": false,
        "bFilter" : true,
        "bSort": false,
        "Filter": true,
        "pageLength": 50,
       
        "Processing": true,
        "serverSide": true,
      
        "ajax": {
             /*data: function ( d ) {
                d.seleted_checkbox = $('#selected_checkbox').val();
                d.all_checkbox=$('#check_all').val();
                d.list='list';
                    } ,*/
            data: {list: 'list'},
            url: url + "Receive_products/index?status="+get,
            type: "post",
            error: function () {
              console.log('its error')
            }
        },
        "language": {
            "sEmptyTable": "<i>No Data Found.</i>",
            "search": '<i class="fa fa-search"></i>',
            "searchPlaceholder": "Product code/order ID/Date"
        },
         
        "aoColumnDefs": [
                    { 'bSortable': false, 'aTargets': [0,8,10] }
                ]



    });
  
  $('.dataTables_filter').remove();
  filterFunction(dataTable);
 
}


if (page_title == "Sent Products For Hallmarking") {

  var dataTable= $('#Hallmarking').DataTable({
          "bLengthChange": false,
        "bFilter" : true,
        "pageLength": 50,
       
        "bSort": false,
        "Filter": true,
        "Processing": true,
        "serverSide": true,

        "ajax": {
            data: function ( d ) {
                d.selected_hallmark = $('#add_selected_hallmark').val();
                d.list='list';
                    } ,
            url: url + "Hallmarking/index?status="+get,
            type: "post",
            error: function () {
              console.log('its error')
            }
        },
        "language": {
            "sEmptyTable": "<i>No Data Found.</i>",
            "search": '<i class="fa fa-search"></i>',
            "searchPlaceholder": "Product Code/Order ID/Date "
        },
        "aoColumnDefs": [
                    { 'bSortable': false, 'aTargets': [7,8,9,10,11] }
                ]
    });
   $('.dataTables_filter').remove();
  filterFunction(dataTable);
}
if (page_title == "Sent Orders For Hallmarking") {
  var dataTable= $('#manufacturing_hallmarking').DataTable({
        "bLengthChange": false,
        "bFilter" : true,
        "pageLength": 50,
       
        "bSort": false,
        "Filter": true,
        "Processing": true,
        "serverSide": true,
        "ajax": {
             data: function ( d ) {
                d.selected_hallmark = $('#add_selected_hallmark').val();
                d.list='list';
                    } ,
            url: url + "Manufacturing_hallmarking/index?status="+get,
            type: "post",
            error: function () {
              console.log('its error')
            }
        },
        "language": {
            "sEmptyTable": "<i>No Data Found.</i>",
            "search": '<i class="fa fa-search"></i>',
            "searchPlaceholder": "Search By Order Name/Karigar Name"
        },
        "aoColumnDefs": [
                    { 'bSortable': false, 'aTargets': [1,2,4,5,6,7,8,9] }
                ]
    });
    $('.dataTables_filter').remove();
  filterFunction(dataTable);
}
if (page_title == "QC CHECKED PRODUCTS") {

  var dataTable= $('#Quality_control').DataTable({
        "bLengthChange": false,
        "bFilter" : true,
        "bSort": false,
        "Filter": true,
        "pageLength": 50,
       
        "Processing": true,
        "serverSide": true,
        "ajax": {
            data: {list: 'list'},
            url: url + "Quality_control/index?status="+get,
            type: "post",
            error: function () {
              console.log('its error')
            }
        },
        "language": {
            "sEmptyTable": "<i>No Data Found.</i>",
            "search": '<i class="fa fa-search"></i>',
            "searchPlaceholder": "Product Code,Order ID,Date,Category"
        },
        "aoColumnDefs": [
                    { 'bSortable': false, 'aTargets': [0,6,7,8,9,10] }
                ]
    });
  $('.dataTables_filter').remove();
  filterFunction(dataTable);
}
if (page_title == "QC CHECKED ORDERS") {
  var dataTable= $('#manufacturing_quality_control_rejected_orders').DataTable({
        "bLengthChange": false,
        "bFilter" : true,
        "bSort": false,
        "Filter": true,
        "Processing": true,
        "serverSide": true,
        "pageLength": 50,
       
        "ajax": {
            data: {list: 'list'},
            url: url + "Manufacturing_quality_control/index?status="+get,
            type: "post",
            error: function () {
              console.log('its error')
            }
        },
        "language": {
            "sEmptyTable": "<i>No Data Found.</i>",
            "search": '<i class="fa fa-search"></i>',
            "searchPlaceholder": "Search By Karigar Name"
        },
        "aoColumnDefs": [
                    { 'bSortable': false, 'aTargets': [] }
                ]
    });
    var dataTable= $('#manufacturing_quality_control').DataTable({
        "bLengthChange": false,
        "bFilter" : true,
        "bSort": false,
        "Filter": true,
        "Processing": true,
        "serverSide": true,
        "pageLength": 50,
       
        "ajax": {
            data: {list: 'list'},
            url: url + "Manufacturing_quality_control/index?status="+get,
            type: "post",
            error: function () {
              console.log('its error')
            }
        },
        "language": {
            "sEmptyTable": "<i>No Data Found.</i>",
            "search": '<i class="fa fa-search"></i>',
            "searchPlaceholder": "Search By Karigar Name"
        },
        "aoColumnDefs": [
                    { 'bSortable': false, 'aTargets': [] }
                ]
    });

      var dataTable= $('#mfg_qc_ctl_reje_bom_orders').DataTable({
        "bLengthChange": false,
        "bFilter" : true,
        "bSort": false,
        "Filter": true,
        "Processing": true,
        "serverSide": true,
        "pageLength": 50,
       
        "ajax": {
            data: {list: 'list'},
            url: url + "Manufacturing_quality_control/index?status="+get,
            type: "post",
            error: function () {
              console.log('its error')
            }
        },
        "language": {
            "sEmptyTable": "<i>No Data Found.</i>",
            "search": '<i class="fa fa-search"></i>',
            "searchPlaceholder": "Search By Karigar Name"
        },
        "aoColumnDefs": [
                    { 'bSortable': false, 'aTargets': [] }
                ]
    });
    $('.dataTables_filter').remove();
  filterFunction(dataTable);
}
if (page_title == "Recevied Orders") {
  var dataTable= $('#received_orders').DataTable({
        "bLengthChange": false,
        "bFilter" : true,
        "bSort": false,
        "Filter": true,
        "Processing": true,
        "serverSide": true,
        "pageLength": 50,
       
        "ajax": {
            data: {list: 'list'},
            url: url + "Received_orders/view/"+order_id,
            type: "post",
            error: function () {
              console.log('its error')
            }
        },
        "language": {
            "sEmptyTable": "<i>No Data Found.</i>",
            "search": '<i class="fa fa-search"></i>',
            "searchPlaceholder": "Search By Order ID"
        }
    });
      var dataTable= $('#received_orders_bombay').DataTable({
        "bLengthChange": false,
        "bFilter" : true,
        "bSort": false,
        "Filter": true,
        "Processing": true,
        "serverSide": true,
        "pageLength": 50,
       
        "ajax": {
            data: {list: 'list'},
            url: url + "Received_orders/view/"+order_id,
            type: "post",
            error: function () {
              console.log('its error')
            }
        },
        "language": {
            "sEmptyTable": "<i>No Data Found.</i>",
            "search": '<i class="fa fa-search"></i>',
            "searchPlaceholder": "Search By Order ID"
        }
    });
   $('.dataTables_filter').remove();
  filterFunction(dataTable);
}
if (page_title == "Manufacturing Accounting Received Orders") {
  var dataTable= $('#mng_received_orders').DataTable({
        "bLengthChange": false,
        "bFilter" : true,
        // "bSort": true,
        "bSort": false,
        "Filter": true,
        "Processing": true,
        "serverSide": true,
        "pageLength": 50,
       
        "ajax": {
            data: {list: 'list'},
            url: url + "Mnfg_accounting_received_orders/view/"+order_id,
            type: "post",
            error: function () {
              console.log('its error')
            }
        },
        "language": {
            "sEmptyTable": "<i>No Data Found.</i>",
            "search": '<i class="fa fa-search"></i>',
            "searchPlaceholder": "Search By Parent Category"
        },
        "aoColumnDefs": [
                    /*{ 'bSortable': false, 'aTargets': [1,2,3,4,5,6,7] }*/
                    { 'bSortable': false, 'aTargets': [] }
                ]
    });
  filterFunction(dataTable);
}
if (page_title == "Manufacturing Accounting Rejected Orders") {
  var dataTable= $('#mng_rejected_orders').DataTable({
        "bLengthChange": false,
        "bFilter" : true,
        // "bSort": true,
        "bSort": false,
        "Filter": true,
        "Processing": true,
        "serverSide": true,
        "ajax": {
            data: {list: 'list'},
            url: url + "Mnfg_accounting_rejected_orders/view/"+order_id,
            type: "post",
            error: function () {
              console.log('its error')
            }
        },
        "language": {
            "sEmptyTable": "<i>No Data Found.</i>",
            "search": '<i class="fa fa-search"></i>',
            "searchPlaceholder": "Search By Order"
        },
        "aoColumnDefs": [
                    /*{ 'bSortable': false, 'aTargets': [1,2,3,4,5,6,7] }*/
                    { 'bSortable': false, 'aTargets': [] }
                ]
    });
  filterFunction(dataTable);
}



if (page_title == "Recevied Receipt") {
  var dataTable= $('#received_receipt').DataTable({
        "bLengthChange": false,
        "bFilter" : true,
        // "bSort": true,
        "bSort": false,
        "Filter": true,
        "Processing": true,
        "serverSide": true,
        "pageLength": 50,
       
        "ajax": {
            data: {list: 'list'},
            url: url + "Received_receipt/index",
            type: "post",
            error: function () {
              console.log('its error')
            }
        },
        "language": {
            "sEmptyTable": "<i>No Data Found.</i>",
            "search": '<i class="fa fa-search"></i>',
            "searchPlaceholder": "Search By Receipt Code"
        },
        "aoColumnDefs": [
                    /*{ 'bSortable': false, 'aTargets': [1,2,3,4,5,6,7] }*/
                    { 'bSortable': false, 'aTargets': [] }
                ]
    });
    var dataTable= $('#received_receipt_bom_dept').DataTable({
        "bLengthChange": false,
        "bFilter" : true,
        // "bSort": true,
        "bSort": false,
        "Filter": true,
        "Processing": true,
        "serverSide": true,
        "pageLength": 50,
       
        "ajax": {
            data: {list: 'list'},
            url: url + "Received_receipt/index",
            type: "post",
            error: function () {
              console.log('its error')
            }
        },
        "language": {
            "sEmptyTable": "<i>No Data Found.</i>",
            "search": '<i class="fa fa-search"></i>',
            "searchPlaceholder": "Search By Receipt Code"
        },
        "aoColumnDefs": [
                    /*{ 'bSortable': false, 'aTargets': [1,2,3,4,5,6,7] }*/
                    { 'bSortable': false, 'aTargets': [] }
                ]
    });
   $('.dataTables_filter').remove();
  filterFunction(dataTable);
}

if (page_title == "Sending To Qc") {
  var dataTable= $('#sending_to_qc').DataTable({
        "bLengthChange": false,
        "bFilter" : true,
        // "bSort": true,
        "bSort": false,
        "Filter": true,
        "Processing": true,
        "serverSide": true,
        "pageLength": 50,
       
        "ajax": {
            data: {list: 'list'},
            url: url + "Sending_to_qc_mfg",
            type: "post",
            error: function () {
              console.log('its error')
            }
        },
        "language": {
            "sEmptyTable": "<i>No Data Found.</i>",
            "search": '<i class="fa fa-search"></i>',
            "searchPlaceholder": "Search By Receipt Code"
        },
        "aoColumnDefs": [
                    /*{ 'bSortable': false, 'aTargets': [1,2,3,4,5,6,7] }*/
                    { 'bSortable': false, 'aTargets': [] }
                ]
    });
    var dataTable= $('#sending_to_qc_bom_dept').DataTable({
        "bLengthChange": false,
        "bFilter" : true,
        // "bSort": true,
        "bSort": false,
        "Filter": true,
        "Processing": true,
        "serverSide": true,
        "pageLength": 50,
       
        "ajax": {
            data: {list: 'list'},
            url: url + "Sending_to_qc_mfg",
            type: "post",
            error: function () {
              console.log('its error')
            }
        },
        "language": {
            "sEmptyTable": "<i>No Data Found.</i>",
            "search": '<i class="fa fa-search"></i>',
            "searchPlaceholder": "Search By Receipt Code"
        },
        "aoColumnDefs": [
                    /*{ 'bSortable': false, 'aTargets': [1,2,3,4,5,6,7] }*/
                    { 'bSortable': false, 'aTargets': [] }
                ]
    });
   $('.dataTables_filter').remove();
  filterFunction(dataTable);
}


if (page_title == "Generate Packing List") {
  var dataTable= $('#Generate_packing_list_create').DataTable({
        "bLengthChange": false,
        "bFilter" : true,
        "bSort": false,
        "Filter": true,
        "pageLength": 50,
       
        "Processing": true,
        "serverSide": true,
        "ajax": {
            data: {list: 'list'},
            url: url + "Generate_packing_list/create",
            type: "post",
            error: function () {
              console.log('its error')
            }
        },
        "language": {
            "sEmptyTable": "<i>No Data Found.</i>",
            "search": '<i class="fa fa-search"></i>',
            "searchPlaceholder": "Search  Order ID / Date"
        },
        "aoColumnDefs": [
                    { 'bSortable': false, 'aTargets': [] }
                ]
    });
   $('.dataTables_filter').remove();
  filterFunction(dataTable);
}
if (page_title == "Packing Order View") {
  var dataTable= $('#packing_order_view').DataTable({
        "bLengthChange": false,
        "bFilter" : true,
        "bSort": false,
        "Filter": true,
        "Processing": true,
        "serverSide": true,
        "ajax": {
            data: {list: 'list'},
            url: url + "Generate_packing_list/show/"+order_id+"?status="+get,
            type: "post",
            error: function () {
              console.log('its error')
            }
        },
        "language": {
            "sEmptyTable": "<i>No Data Found.</i>",
            "search": '<i class="fa fa-search"></i>',
            "searchPlaceholder": "Search Product Code"
        },
       
    });
  $('.dataTables_filter').remove();
  filterFunction(dataTable);
}
if (page_title == "Packing List") {
  var dataTable= $('#Packing_list').DataTable({
        "bLengthChange": false,
        "bFilter" : true,
        "bSort": false,
        "Filter": true,
        "pageLength": 50,
       
        "Processing": true,
        "serverSide": true,

        "ajax": {
            data: {list: 'list'},
            url: url + "Generate_packing_list/index?status="+get,
            type: "post",
            error: function () {
              console.log('its error')
            }
        },
        "language": {
            "sEmptyTable": "<i>No Data Found.</i>",
            "search": '<i class="fa fa-search"></i>',
            "searchPlaceholder": "search order ID / Date"
        },
        "aoColumnDefs": [
                    { 'bSortable': false, 'aTargets': [2,3,4] }
                ]
    });
  $('.dataTables_filter').remove();
  filterFunction(dataTable);
}
if (page_title == "All Amended Products") {
  var dataTable= $('#AmendTbl').DataTable({
        "bLengthChange": false,
        "bFilter" : true,
        "bSort": false,
        "Filter": true,
        "pageLength": 50,
       
        "Processing": true,
        "serverSide": true,
        "ajax": {
            data: {list: 'list'},
            url: url + "Amend_products",
            type: "post",
            error: function () {
              console.log('its error')
            }
        },
        "language": {
            "sEmptyTable": "<i>No Data Found.</i>",
            "search": '<i class="fa fa-search"></i>',
            "searchPlaceholder": "Product code/Order ID/Date"
        },
        "aoColumnDefs": [
                    { 'bSortable': false, 'aTargets': [] }
                ]
    });
    $('.dataTables_filter').remove();
  filterFunction(dataTable);

  var dataTable= $('#AmendView').DataTable({
        "bLengthChange": false,
        "bFilter" : true,
        "bSort": false,
        "Filter": true,
        "pageLength": 50,
       
        "Processing": true,
        "serverSide": true,
        "ajax": {
            data: {list: 'list'},
            url: url + "Amend_products/show/"+karigar_id,
            type: "post",
            error: function () {
              console.log('its error')
            }
        },
        "language": {
            "sEmptyTable": "<i>No Data Found.</i>",
            "search": '<i class="fa fa-search"></i>',
            "searchPlaceholder": "Product code/Order ID/Date"
        },
        "aoColumnDefs": [
                    { 'bSortable': false, 'aTargets': [] }
                ]
    });
    $('.dataTables_filter').remove();
  filterFunction(dataTable);
}

if (page_title == "All Sent Products To Relience") {
  var dataTable= $('#Stock').DataTable({
        "bLengthChange": false,
        "bFilter" : true,
        "bSort": false,
        "Filter": true,
        "pageLength": 50,
       
        "Processing": true,
        "serverSide": true,
        "ajax": {
            data: {list: 'list'},
            url: url + "Stock/"+vendor+"/?status="+get,
            type: "post",
            error: function () {
              console.log('its error')
            }
        },
        "language": {
            "sEmptyTable": "<i>No Data Found.</i>",
            "search": '<i class="fa fa-search"></i>',
            "searchPlaceholder": "search by product code /date"
        },
        "aoColumnDefs": [
                    { 'bSortable': false, 'aTargets': [] }
                ]
    });
    var dataTable= $('#All_stock_products').DataTable({
        "bLengthChange": false,
        "bFilter" : true,
        "bSort": false,
        "Filter": true,
        "pageLength": 50,
       
        "Processing": true,
        "serverSide": true,
        "ajax": {
            data: {list: 'list'},
            url: url + "Stock/"+vendor+"/?status="+get,
            type: "post",
            error: function () {
              console.log('its error')
            }
        },
        "language": {
            "sEmptyTable": "<i>No Data Found.</i>",
            "search": '<i class="fa fa-search"></i>',
            "searchPlaceholder": "search by product code /date"
        },
        "aoColumnDefs": [
                    { 'bSortable': false, 'aTargets': [] }
                ]
    });
   $('.dataTables_filter').remove();
  filterFunction(dataTable);
}


if (page_title == "Department Order List") {
  var dataTable= $('#department_order').DataTable({
        "bLengthChange": false,
        "bFilter" : true,
        "bSort": false,
        "Filter": true,
        "Processing": true,
        "serverSide": true,
        "pageLength": 50,
       
        "ajax": {
            data: {list: 'list'},
            url: url + "Manufacturing_department_order",
            type: "post",
            error: function () {
              console.log('its error')
            }
        },
        "language": {
            "sEmptyTable": "<i>No Data Found.</i>",
            "search": '<i class="fa fa-search"></i>',
            "searchPlaceholder": "Search Order Id"
        },
        "aoColumnDefs": [
                    { 'bSortable': false, 'aTargets': [] }
                ]
    });
    $('.dataTables_filter').remove();
  filterFunction(dataTable);
}
if (page_title == "All Department") {
  var dataTable= $('#department').DataTable({
        "bLengthChange": false,
        "bFilter" : true,
        "bSort": false,
        "Filter": true,
        "Processing": true,
        "serverSide": true,
        "pageLength": 50,
       
        "ajax": {
            data: {list: 'list'},
            url: url + "Department",
            type: "post",
            error: function () {
              console.log('its error')
            }
        },
        "language": {
            "sEmptyTable": "<i>No Data Found.</i>",
            "search": '<i class="fa fa-search"></i>',
            "searchPlaceholder": "Search Order Id"
        },
        "aoColumnDefs": [
                    { 'bSortable': false, 'aTargets': [] }
                ]
    });

   $('.dataTables_filter').remove();
  filterFunction(dataTable);
}
if (page_title == "View Orders") {
  var dataTable= $('#department_all_view').DataTable({
        "bLengthChange": false,
        "bFilter" : true,
        "bSort": false,
        "Filter": true,
        "Processing": true,
        "serverSide": true,
        "pageLength": 50,
       
        "ajax": {
            data: {list: 'list'},
            url: url + "Manufacturing_department_order/view/"+order_id,
            type: "post",
            error: function () {
              console.log('its error')
            }
        },
        "language": {
            "sEmptyTable": "<i>No Data Found.</i>",
            "search": '<i class="fa fa-search"></i>',
            "searchPlaceholder": "Search Parent Category"
        },
        "aoColumnDefs": [
                    { 'bSortable': false, 'aTargets': [] }
                ]
    });
    var dataTable= $('#department_mangalsutra_view').DataTable({
        "bLengthChange": false,
        "bFilter" : true,
        "bSort": false,
        "Filter": true,
        "Processing": true,
        "serverSide": true,
        "pageLength": 50,
       
        "ajax": {
            data: {list: 'list'},
            url: url + "Manufacturing_department_order/view/"+order_id,
            type: "post",
            error: function () {
              console.log('its error')
            }
        },
        "language": {
            "sEmptyTable": "<i>No Data Found.</i>",
            "search": '<i class="fa fa-search"></i>',
            "searchPlaceholder": "Search Parent Category"
        },
        "aoColumnDefs": [
                    { 'bSortable': false, 'aTargets': [] }
                ]
    });
   var dataTable= $('#department_wise_view').DataTable({
        "bLengthChange": false,
        "bFilter" : true,
        "bSort": false,
        "Filter": true,
        "Processing": true,
        "serverSide": true,
        "pageLength": 50,
       
        "ajax": {
            data: {list: 'list'},
            url: url + "Manufacturing_department_order/view/"+order_id,
            type: "post",
            error: function () {
              console.log('its error')
            }
        },
        "language": {
            "sEmptyTable": "<i>No Data Found.</i>",
            "search": '<i class="fa fa-search"></i>',
            "searchPlaceholder": "Search Parent Category"
        },
        "aoColumnDefs": [
                    { 'bSortable': false, 'aTargets': [] }
                ]
    });
    $('.dataTables_filter').remove();
    filterFunction(dataTable);
}
if (page_title == "Karigar Order List") {
  var dataTable= $('#Karigar_order').DataTable({
        "bLengthChange": false,
        "bFilter" : true,
        "bSort": false,
        "Filter": true,
        "Processing": true,
        "serverSide": true,
        "pageLength": 50,
       
        "ajax": {
            data: {list: 'list'},
            url: url + "Karigar_order_list/index/",
            type: "post",
            error: function () {
              console.log('its error')
            }
        },
        "language": {
            "sEmptyTable": "<i>No Data Found.</i>",
            "search": '<i class="fa fa-search"></i>',
            "searchPlaceholder": "Search Order Id"
        },
        "aoColumnDefs": [
                    { 'bSortable': false, 'aTargets': [] }
                ]
    });
      var dataTable= $('#Karigar_order_bom_dep').DataTable({
        "bLengthChange": false,
        "bFilter" : true,
        "bSort": false,
        "Filter": true,
        "Processing": true,
        "serverSide": true,
        "pageLength": 50,
       
        "ajax": {
            data: {list: 'list'},
            url: url + "Karigar_order_list/index/",
            type: "post",
            error: function () {
              console.log('its error')
            }
        },
        "language": {
            "sEmptyTable": "<i>No Data Found.</i>",
            "search": '<i class="fa fa-search"></i>',
            "searchPlaceholder": "Search Order Id"
        },
        "aoColumnDefs": [
                    { 'bSortable': false, 'aTargets': [] }
                ]
    });
      $('.dataTables_filter').remove();
  filterFunction(dataTable);
}
if (page_title == "Order Sent") {
  var dataTable= $('#order_sent').DataTable({
        "bLengthChange": false,
        "bFilter" : true,
        "bSort": false,
        "Filter": true,
        "Processing": true,
        "serverSide": true,
        "pageLength": 50,
       
        "ajax": {
            data: {list: 'list'},
            url: url + "Order_sent/index",
            type: "post",
            error: function () {
              console.log('its error')
            }
        },
        "language": {
            "sEmptyTable": "<i>No Data Found.</i>",
            "search": '<i class="fa fa-search"></i>',
            "searchPlaceholder": "Search Order Id"
        },
        "aoColumnDefs": [
                    { 'bSortable': false, 'aTargets': [] }
                ]
    });

      var dataTable= $('#order_sent_oth_dep').DataTable({
        "bLengthChange": false,
        "bFilter" : true,
        "bSort": false,
        "Filter": true,
        "Processing": true,
        "serverSide": true,
        "pageLength": 50,
       
        "ajax": {
            data: {list: 'list'},
            url: url + "Order_sent/index",
            type: "post",
            error: function () {
              console.log('its error')
            }
        },
        "language": {
            "sEmptyTable": "<i>No Data Found.</i>",
            "search": '<i class="fa fa-search"></i>',
            "searchPlaceholder": "Search Order Id"
        },
        "aoColumnDefs": [
                    { 'bSortable': false, 'aTargets': [] }
                ]
    });

   $('.dataTables_filter').remove();
  filterFunction(dataTable);
}
if (page_title == "View Make Set") {
  var dataTable= $('#manufacturing_make_set').DataTable({
        "bLengthChange": false,
        "bFilter" : true,
        "bSort": false,
        "Filter": true,
        "Processing": true,
        "serverSide": true,
        "pageLength": 50,
       
        "ajax": {
            data: {list: 'list'},
            url: url + "Make_set/index",
            type: "post",
            error: function () {
              console.log('its error')
            }
        },
        "language": {
            "sEmptyTable": "<i>No Data Found.</i>",
            "search": '<i class="fa fa-search"></i>',
            "searchPlaceholder": "Search Set Id"
        },
        "aoColumnDefs": [
                    { 'bSortable': false, 'aTargets': [] }
                ]
    });
  filterFunction(dataTable);
}
if (page_title == "Make Set Products") {
    var id = $('#manufacturing_make_set').data('id');
  var dataTable= $('#manufacturing_make_set').DataTable({
        "bLengthChange": false,
        "bFilter" : true,
        "bSort": false,
        "Filter": true,
        "Processing": true,
        "serverSide": true,
        "pageLength": 50,
       
        "ajax": {
            data: {list: 'list'},
            url: url + "Make_set/show/"+ id,
            type: "post",
            error: function () {
              console.log('its error')
            }
        },
        "language": {
            "sEmptyTable": "<i>No Data Found.</i>",
            "search": '<i class="fa fa-search"></i>',
            "searchPlaceholder": "Search Set Id"
        },
        "aoColumnDefs": [
                    { 'bSortable': false, 'aTargets': [] }
                ]
    });
  filterFunction(dataTable);
}
if (page_title == "View Ready Product") {
/*  var dataTable= $('#manufacturing_ready_product').DataTable({
        "bLengthChange": false,
        "bFilter" : true,
        "bSort": false,
        "Filter": true,
        "Processing": true,
        "serverSide": true,
        "pageLength": 50,
       
        "ajax": {
            data: {list: 'list'},
            //url: url + "Ready_product/index/"+department_id,
            url: url + "ready_product?status="+get,
            type: "post",
            error: function () {
              console.log('its error')
            }
        },
        "language": {
            "sEmptyTable": "<i>No Data Found.</i>",
            "search": '<i class="fa fa-search"></i>',
            "searchPlaceholder": "Search By Chitti/Product/Parent Category"
        },
        "aoColumnDefs": [
                    { 'bSortable': false, 'aTargets': [] }
                ]
    });*/
    
    var dataTable= $('#mfg_ready_pro_aprv_by_sales_bom').DataTable({
        "bLengthChange": false,
        "bFilter" : true,
        "bSort": false,
        "Filter": true,
        "Processing": true,
        "serverSide": true,
        "pageLength": 50,
       
        "ajax": {
            data: {list: 'list'},
            //url: url + "Ready_product/index/"+department_id,
            url: url + "ready_product?status="+get,
            type: "post",
            error: function () {
              console.log('its error')
            }
        },
        "language": {
            "sEmptyTable": "<i>No Data Found.</i>",
            "search": '<i class="fa fa-search"></i>',
            "searchPlaceholder": "Search By Chitti/Product/Parent Category"
        },
        "aoColumnDefs": [
                    { 'bSortable': false, 'aTargets': [] }
                ]
    });

    var dataTable= $('#manufacturing_ready_product_approved_by_sales').DataTable({
        "bLengthChange": false,
        "bFilter" : true,
        "bSort": false,
        "Filter": true,
        "Processing": true,
        "serverSide": true,
        "pageLength": 50,
       
        "ajax": {
            data: {list: 'list'},
            //url: url + "Ready_product/index/"+department_id,
            url: url + "ready_product?status="+get,
            type: "post",
            error: function () {
              console.log('its error')
            }
        },
        "language": {
            "sEmptyTable": "<i>No Data Found.</i>",
            "search": '<i class="fa fa-search"></i>',
            "searchPlaceholder": "Search By Chitti/Product/Parent Category"
        },
        "aoColumnDefs": [
                    { 'bSortable': false, 'aTargets': [] }
                ]
    });

    var dataTable= $('#mfg_ready_product_dept_wise').DataTable({
        "bLengthChange": false,
        "bFilter" : true,
        "bSort": false,
        "Filter": true,
        "Processing": true,
        "serverSide": true,
        "pageLength": 50,
       
        "ajax": {
            data: {list: 'list'},
            //url: url + "Ready_product/index/"+department_id,
            url: url + "ready_product?status="+get,
            type: "post",
            error: function () {
              console.log('its error')
            }
        },
        "language": {
            "sEmptyTable": "<i>No Data Found.</i>",
            "search": '<i class="fa fa-search"></i>',
            "searchPlaceholder": "Search By Chitti/Product/Parent Category"
        },
        "aoColumnDefs": [
                    { 'bSortable': false, 'aTargets': [] }
                ]
    });
    
     var dataTable= $('#mfg_ready_product_details').DataTable({
        "bLengthChange": false,
        "bFilter" : true,
        "bSort": false,
        "Filter": true,
        "Processing": true,
        "serverSide": true,
        "pageLength": 50,
       
        "ajax": {
            data: {list: 'list'},
            url: url + "Ready_product/view/"+product_code+"?status="+get,        
            type: "post",
            error: function () {
              console.log('its error')
            }
        },
        "language": {
            "sEmptyTable": "<i>No Data Found.</i>",
            "search": '<i class="fa fa-search"></i>',
            "searchPlaceholder": "Search By Chitti/Product/Parent Category"
        },
        "aoColumnDefs": [
                    { 'bSortable': false, 'aTargets': [] }
                ]
    });

      var dataTable= $('#mfg_sales_ready_product_details').DataTable({
        "bLengthChange": false,
        "bFilter" : true,
        "bSort": false,
        "Filter": true,
        "Processing": true,
        "serverSide": true,
        "pageLength": 50,
       
        "ajax": {
            data: {list: 'list'},
            url: url + "Sales_recieved_product/view/"+product_code+"?status="+get,        
            type: "post",
            error: function () {
              console.log('its error')
            }
        },
        "language": {
            "sEmptyTable": "<i>No Data Found.</i>",
            "search": '<i class="fa fa-search"></i>',
            "searchPlaceholder": "Search By Chitti/Product/Parent Category"
        },
        "aoColumnDefs": [
                    { 'bSortable': false, 'aTargets': [] }
                ]
    });

        
     var dataTable= $('#mfg_ready_product_details_mangalsutra').DataTable({
        "bLengthChange": false,
        "bFilter" : true,
        "bSort": false,
        "Filter": true,
        "Processing": true,
        "serverSide": true,
        "pageLength": 50,
       
        "ajax": {
            data: {list: 'list'},
            url: url + "Ready_product/view/"+product_code+"?status="+get,        
            type: "post",
            error: function () {
              console.log('its error')
            }
        },
        "language": {
            "sEmptyTable": "<i>No Data Found.</i>",
            "search": '<i class="fa fa-search"></i>',
            "searchPlaceholder": "Search By Chitti/Product/Parent Category"
        },
        "aoColumnDefs": [
                    { 'bSortable': false, 'aTargets': [] }
                ]
    }); 

    var dataTable= $('#mfg_ready_product_details_bangl').DataTable({
        "bLengthChange": false,
        "bFilter" : true,
        "bSort": false,
        "Filter": true,
        "Processing": true,
        "serverSide": true,
        "pageLength": 50,
       
        "ajax": {
            data: {list: 'list'},
            url: url + "Ready_product/view/"+product_code+"?status="+get,        
            type: "post",
            error: function () {
              console.log('its error')
            }
        },
        "language": {
            "sEmptyTable": "<i>No Data Found.</i>",
            "search": '<i class="fa fa-search"></i>',
            "searchPlaceholder": "Search By Chitti/Product/Parent Category"
        },
        "aoColumnDefs": [
                    { 'bSortable': false, 'aTargets': [] }
                ]
    }); 

     

    $('.dataTables_filter').remove();
  filterFunction(dataTable);
}
if (page_title == "View Ready Product") {
  var dataTable= $('#manufacturing_ready_product').DataTable({
        "bLengthChange": false,
        "bFilter" : true,
        "bSort": false,
        "Filter": true,
        "Processing": true,
        "serverSide": true,
        "pageLength": 50,
       
        "ajax": {
            data: {list: 'list'},
            //url: url + "Ready_product/index/"+department_id,
            url: url + "ready_product?status="+get,
            type: "post",
            error: function () {
              console.log('its error')
            }
        },
        "language": {
            "sEmptyTable": "<i>No Data Found.</i>",
            "search": '<i class="fa fa-search"></i>',
            "searchPlaceholder": "Search By Chitti/Product/Parent Category"
        },
        "aoColumnDefs": [
                    { 'bSortable': false, 'aTargets': [] }
                ]
    });
    $('.dataTables_filter').remove();
      filterFunction(dataTable);
 
}
if (page_title == "View  Sales return Product") {
  var dataTable= $('#sales_voucher_return_product').DataTable({
        "bLengthChange": false,
        "bFilter" : true,
        "bSort": false,
        "Filter": true,
        "Processing": true,
        "serverSide": true,
        "pageLength": 50,
       
        "ajax": {
            data: {list: 'list'},
            url: url + "sales_return_product",
            type: "post",
            error: function () {
              console.log('its error')
            }
        },
        "language": {
            "sEmptyTable": "<i>No Data Found.</i>",
            "search": '<i class="fa fa-search"></i>',
            "searchPlaceholder": "Search By Chitti/Product/Parent Category"
        },
        "aoColumnDefs": [
                    { 'bSortable': false, 'aTargets': [] }
                ]
    });
    $('.dataTables_filter').remove();
      filterFunction(dataTable);
 
}
if (page_title == "View Few weightt") {
    var ctr='Few_box';
    var dataTable= $('#few_box').DataTable({
        "bLengthChange": false,
        "bFilter" : true,
        "bSort": false,
        "Filter": true,
        "Processing": true,
        "serverSide": true,
        "pageLength": 50,
       
        "ajax": {
            data: {list: 'list'},
            url: url + "Few_box",
            type: "post",
            error: function () {
              console.log('its error')
            }
        },
        "language": {
            "sEmptyTable": "<i>No Data Found.</i>",
            "search": '<i class="fa fa-search"></i>',
            "searchPlaceholder": "Search By Chitti/Product/Parent Category"
        },
        "aoColumnDefs": [
                    { 'bSortable': false, 'aTargets': [] }
                ]
    });
    $('.dataTables_filter').remove();
    //filterFunction(dataTable);
     get_child_row('few_box',dataTable,ctr);  

   
}

if (page_title == "View Repair Product") {
    var ctr ='repair_product';
    var dataTable= $('#mfg_repair_product_bom').DataTable({
        "bLengthChange": false,
        "bFilter" : true,
        "bSort": false,
        "Filter": true,
        "Processing": true,
        "serverSide": true,
        "pageLength": 50,
       
        "ajax": {
            data: {list: 'list'},
            url: url + "repair_product",
            type: "post",
            error: function () {
              console.log('its error')
            }
        },
        "language": {
            "sEmptyTable": "<i>No Data Found.</i>",
            "search": '<i class="fa fa-search"></i>',
            "searchPlaceholder": "Search By Chitti/Product/Parent Category"
        },
        "aoColumnDefs": [
                    { 'bSortable': false, 'aTargets': [] }
                ]
    });
    $('.dataTables_filter').remove();
    //filterFunction(dataTable);
     get_child_row('mfg_repair_product_bom',dataTable,ctr);  
  var dataTable= $('#manufacturing_repair_product').DataTable({
        "bLengthChange": false,
        "bFilter" : true,
        "bSort": false,
        "Filter": true,
        "Processing": true,
        "serverSide": true,
        "pageLength": 50,
       
        "ajax": {
            data: {list: 'list'},
            url: url + "repair_product",
            type: "post",
            error: function () {
              console.log('its error')
            }
        },
        "language": {
            "sEmptyTable": "<i>No Data Found.</i>",
            "search": '<i class="fa fa-search"></i>',
            "searchPlaceholder": "Search By Chitti/Product/Parent Category"
        },
        "aoColumnDefs": [
                    { 'bSortable': false, 'aTargets': [] }
                ]
    });
  $('.dataTables_filter').remove();
     get_child_row('manufacturing_repair_product',dataTable,ctr);
     var dataTable= $('#manufacturing_repair_mangalsutra').DataTable({
        "bLengthChange": false,
        "bFilter" : true,
        "bSort": false,
        "Filter": true,
        "Processing": true,
        "serverSide": true,
        "pageLength": 50,
       
        "ajax": {
            data: {list: 'list'},
            url: url + "repair_product",
            type: "post",
            error: function () {
              console.log('its error')
            }
        },
        "language": {
            "sEmptyTable": "<i>No Data Found.</i>",
            "search": '<i class="fa fa-search"></i>',
            "searchPlaceholder": "Search By Chitti/Product/Parent Category"
        },
        "aoColumnDefs": [
                    { 'bSortable': false, 'aTargets': [] }
                ]
    });
  $('.dataTables_filter').remove();
     get_child_row('manufacturing_repair_mangalsutra',dataTable,ctr);

       var dataTable= $('#manufacturing_repair_bgtrios').DataTable({
        "bLengthChange": false,
        "bFilter" : true,
        "bSort": false,
        "Filter": true,
        "Processing": true,
        "serverSide": true,
        "pageLength": 50,
       
        "ajax": {
            data: {list: 'list'},
            url: url + "repair_product",
            type: "post",
            error: function () {
              console.log('its error')
            }
        },
        "language": {
            "sEmptyTable": "<i>No Data Found.</i>",
            "search": '<i class="fa fa-search"></i>',
            "searchPlaceholder": "Search By Chitti/Product/Parent Category"
        },
        "aoColumnDefs": [
                    { 'bSortable': false, 'aTargets': [] }
                ]
    });
  $('.dataTables_filter').remove();
     get_child_row('manufacturing_repair_bgtrios',dataTable,ctr);



  var dataTable= $('#manufacturing_repair_product_list').DataTable({
        "bLengthChange": false,
        "bFilter" : true,
        "bSort": false,
        "Filter": true,
        "Processing": true,
        "serverSide": true,
        "pageLength": 50,
       
        "ajax": {
            data: {list: 'list'},
            url: url + "repair_product_list",
            type: "post",
            error: function () {
              console.log('its error')
            }
        },
        "language": {
            "sEmptyTable": "<i>No Data Found.</i>",
            "search": '<i class="fa fa-search"></i>',
            "searchPlaceholder": "Search By Chitti/Product/Parent Category"
        },
        "aoColumnDefs": [
                    { 'bSortable': false, 'aTargets': [] }
                ]
    });
    $('.dataTables_filter').remove();
    filterFunction(dataTable);
    
    var dataTable= $('#manufacturing_repair_product_bgtrios_list').DataTable({
        "bLengthChange": false,
        "bFilter" : true,
        "bSort": false,
        "Filter": true,
        "Processing": true,
        "serverSide": true,
        "pageLength": 50,
       
        "ajax": {
            data: {list: 'list'},
            url: url + "repair_product_list",
            type: "post",
            error: function () {
              console.log('its error')
            }
        },
        "language": {
            "sEmptyTable": "<i>No Data Found.</i>",
            "search": '<i class="fa fa-search"></i>',
            "searchPlaceholder": "Search By Chitti/Product/Parent Category"
        },
        "aoColumnDefs": [
                    { 'bSortable': false, 'aTargets': [] }
                ]
    });
    $('.dataTables_filter').remove();
    filterFunction(dataTable);
  
   
}
var detailRows = [];

function get_child_row(table_name,dt,ctr)
{
        
    $('#'+table_name+' tbody').on( 'click', 'tr', function () {
        var tr = $(this).closest('tr');
        var span_id = $(tr).find('span').attr('class');
        var row = dt.row( tr );
        var idx = $.inArray(span_id, detailRows );
        
        if (row.child.isShown() ) {
            tr.removeClass( 'details');
            row.child.hide();
            // Remove from the 'open' array
            detailRows.splice( idx, 1 );
        }
        else {
            tr.addClass( 'details' );
            format(span_id,row,table_name,ctr);
           
            // Add to the 'open' array
            if ( idx === -1 ) {
                detailRows.push( tr.attr('id') );
            }
        }
    } );
}

function format (id,row,table_name,ctr) {
    var html="";
    //alert(ctr);
    var page_url=url+ctr+"/get_repair_details";
    $.ajax({
        url : page_url,
        type: "POST",
        dataType: "json",
        data:{id:id},
        success: function(data, textStatus, jqXHR){
            html=data.html;
            row.child(html).show();
        },
    });
}


/*if (page_title == "All Hallmarking Center") {
  var dataTable= $('#hallmarking_center_table').DataTable({
        "bLengthChange": false,
        "bFilter" : true,
        "bSort": false,
        "Filter": true,
        "Processing": true,
        "serverSide": true,
        "ajax": {
            data: {list: 'list'},
            url: url + "hallmarking_center/index",
            type: "post",
            error: function () {
              console.log('its error')
            }
        },
       "language": {
            "sEmptyTable": "<i>No Data Found.</i>",
            "search": '<i class="fa fa-search"></i>',
            "searchPlaceholder": "Search hallmarking center"
        },
        "aoColumnDefs": [
                    { 'bSortable': false, 'aTargets': [] }
                ]
    });
  filterFunction(dataTable);
}*/


if (page_title == "All Parent Category") {
  var dataTable= $('#parent_category_table').DataTable({
        "bLengthChange": false,
        "bFilter" : true,
        "bSort": false,
        "Filter": true,
        "Processing": true,
        "serverSide": true,
        "ajax": {
            data: {list: 'list'},
            url: url + "parent_category/index",
            type: "post",
            error: function () {
              console.log('its error')
            }
        },
       "language": {
            "sEmptyTable": "<i>No Data Found.</i>",
            "search": '<i class="fa fa-search"></i>',
            "searchPlaceholder": "Search Category"
        },
        "aoColumnDefs": [
                    { 'bSortable': false, 'aTargets': [] }
                ]
    });
   $('.dataTables_filter').remove();
  filterFunction(dataTable);
}

if (page_title == "All Product Category") {
  var dataTable= $('#product_category_table').DataTable({
        "bLengthChange": false,
        "bFilter" : true,
        "bSort": false,
        "Filter": true,
        "Processing": true,
        "serverSide": true,
        "pageLength": 50,
       
        "ajax": {
            data: {list: 'list'},
            url: url + "product_category/index",
            type: "post",
            error: function () {
              console.log('its error')
            }
        },
       "language": {
            "sEmptyTable": "<i>No Data Found.</i>",
            "search": '<i class="fa fa-search"></i>',
            "searchPlaceholder": "Search Category"
        },
        "aoColumnDefs": [
                    { 'bSortable': false, 'aTargets': [] }
                ]
    });
  $('.dataTables_filter').remove();
  filterFunction(dataTable);
}

if (page_title == "All Hallmarking Center") {
  var dataTable= $('#hallmarking_list_table').DataTable({
        "bLengthChange": false,
        "bFilter" : true,
        "bSort": false,
        "Filter": true,
        "Processing": true,
        "serverSide": true,
        "pageLength": 50,
        
        "ajax": {
            data: {list: 'list'},
            url: url + "hallmarking_list/index",
            type: "post",
            error: function () {
              console.log('its error')
            }
        },
       "language": {
            "sEmptyTable": "<i>No Data Found.</i>",
            "search": '<i class="fa fa-search"></i>',
            "searchPlaceholder": "Search Name"
        },
        "aoColumnDefs": [
                    { 'bSortable': false, 'aTargets': [] }
                ]
    });

   $('.dataTables_filter').remove();
  filterFunction(dataTable);
}

if (page_title == "All Prepared Orders") {
  var dataTable= $('#sales_prepared_order_table').DataTable({
        "bLengthChange": false,
        "bFilter" : true,
        "bSort": false,
        "Filter": true,
        "Processing": true,
        "serverSide": true,
        "pageLength": 50,
       
        "ajax": {
            data: {list: 'list'},
            url: url + "prepared_order/index?status="+get,
            type: "post",
            error: function () {
              console.log('its error')
            }
        },
       "language": {
            "sEmptyTable": "<i>No Data Found.</i>",
            "search": '<i class="fa fa-search"></i>',
            "searchPlaceholder": "Search Order Id"
        },
        "aoColumnDefs": [
                    { 'bSortable': false, 'aTargets': [] }
                ]
    });
  $('.dataTables_filter').remove();
  filterFunction(dataTable);
}
if (page_title == "All Sales Under") {
  var dataTable= $('#Sales_under').DataTable({
        "bLengthChange": false,
        "bFilter" : true,
        "bSort": false,
        "Filter": true,
        "Processing": true,
        "serverSide": true,
        "ajax": {
            data: {list: 'list'},
            url: url + "Sales_under/index",
            type: "post",
            error: function () {
              console.log('its error')
            }
        },
       "language": {
            "sEmptyTable": "<i>No Data Found.</i>",
            "search": '<i class="fa fa-search"></i>',
            "searchPlaceholder": "Search Sales Under"
        },
        "aoColumnDefs": [
                    { 'bSortable': false, 'aTargets': [] }
                ]
    });
  filterFunction(dataTable);
}

if (page_title == "Not Sent To Karigar") {
  var dataTable= $('#kundan_karigar').DataTable({
        "bLengthChange": false,
        "bFilter" : true,
        "bSort": false,
        "Filter": true,
        "Processing": true,
        "serverSide": true,
         "pageLength": 50,
       
        "ajax": {
            data: {list: 'list'},
            url: url + "kundan_karigar/index?status="+get,
            type: "post",
            error: function () {
              console.log('its error')
            }
        },
       "language": {
            "sEmptyTable": "<i>No Data Found.</i>",
            "search": '<i class="fa fa-search"></i>',
            "searchPlaceholder": "Search By Karigar Name"
        },
        "aoColumnDefs": [
                    { 'bSortable': false, 'aTargets': [] }
                ]
    });
   var dataTable= $('#kundan_karigar_pending').DataTable({
        "bLengthChange": false,
        "bFilter" : true,
        "bSort": false,
        "Filter": true,
        "Processing": true,
        "serverSide": true,
         "pageLength": 50,
       
        "ajax": {
            data: {list: 'list'},
            url: url + "kundan_karigar/index?status="+get,
            type: "post",
            error: function () {
              console.log('its error')
            }
        },
       "language": {
            "sEmptyTable": "<i>No Data Found.</i>",
            "search": '<i class="fa fa-search"></i>',
            "searchPlaceholder": "Search By Karigar Name"
        },
        "aoColumnDefs": [
                    { 'bSortable': false, 'aTargets': [] }
                ]
    });
    $('.dataTables_filter').remove();
  filterFunction(dataTable);
  
}
if (page_title == "Quality Control") {
  var dataTable= $('#kundan_QC').DataTable({
        "bLengthChange": false,
        "bFilter" : true,
        "bSort": false,
        "Filter": true,
        "Processing": true,
        "serverSide": true,
        "pageLength": 50,
       
        "ajax": {
            data: {list: 'list'},
            url: url + "kundan_QC/index?status="+get,
            type: "post",
            error: function () {
              console.log('its error')
            }
        },
       "language": {
            "sEmptyTable": "<i>No Data Found.</i>",
            "search": '<i class="fa fa-search"></i>',
            "searchPlaceholder": "karigar/parent category"
        },
        "aoColumnDefs": [
                    { 'bSortable': false, 'aTargets': [] }
                ]
    });
    var dataTable= $('#kundan_qc_tbl').DataTable({
        "bLengthChange": false,
        "bFilter" : true,
        "bSort": false,
        "Filter": true,
        "Processing": true,
        "serverSide": true,
        "pageLength": 50,
       
        "ajax": {
            data: {list: 'list'},
            url: url + "kundan_QC/index?status="+get,
            type: "post",
            error: function () {
              console.log('its error')
            }
        },
       "language": {
            "sEmptyTable": "<i>No Data Found.</i>",
            "search": '<i class="fa fa-search"></i>',
            "searchPlaceholder": "karigar/parent category"
        },
        "aoColumnDefs": [
                    { 'bSortable': false, 'aTargets': [] }
                ]
    });
  $('.dataTables_filter').remove();
  filterFunction(dataTable);
}
if (page_title == "Create Kundan Receipt") {
  var dataTable= $('#received_products').DataTable({
        "bLengthChange": false,
        "bFilter" : true,
        "bSort": false,
        "Filter": true,
        "Processing": true,
        "serverSide": true,
        "pageLength": 50,
       
        "ajax": {
            data: {list: 'list'},
            url: url + "karigar_receive_order/index",
            type: "post",
            error: function () {
              console.log('its error')
            }
        },
       "language": {
            "sEmptyTable": "<i>No Data Found.</i>",
            "search": '<i class="fa fa-search"></i>',
            "searchPlaceholder": "Karigar/Product Name/Parent category"
        },
        "aoColumnDefs": [
                    { 'bSortable': false, 'aTargets': [] }
                ]
    });
   $('.dataTables_filter').remove();
  filterFunction(dataTable);
}

if (page_title == "All Recieved Products") {
  var dataTable= $('#sales_recieved_product_table').DataTable({
        "bLengthChange": false,
        "bFilter" : true,
        "bSort": false,
        "Filter": true,
        "Processing": true,
        "serverSide": true,
        "pageLength": 50,
       
        "ajax": {
            data: {list: 'list'},
            url: url + "sales_recieved_product/index?status="+get+"&department="+department,
            type: "post",
            error: function () {
              console.log('its error')
            }
        },
       "language": {
            "sEmptyTable": "<i>No Data Found.</i>",
            "search": '<i class="fa fa-search"></i>',
            "searchPlaceholder": "Search By Chitti No"
        },
        "aoColumnDefs": [
                    { 'bSortable': false, 'aTargets': [] }
                ]
    });
   var dataTable= $('#sales_recieved_product_table_dep').DataTable({
        "bLengthChange": false,
        "bFilter" : true,
        "bSort": false,
        "Filter": true,
        "Processing": true,
        "serverSide": true,
        "pageLength": 50,
       
        "ajax": {
            data: {list: 'list'},
            url: url + "sales_recieved_product/index?status="+get,
            type: "post",
            error: function () {
              console.log('its error')
            }
        },
       "language": {
            "sEmptyTable": "<i>No Data Found.</i>",
            "search": '<i class="fa fa-search"></i>',
            "searchPlaceholder": "Search By Chitti No"
        },
        "aoColumnDefs": [
                    { 'bSortable': false, 'aTargets': [] }
                ]
    });
    var dataTable= $('#sales_returned_product_table').DataTable({
        "bLengthChange": false,
        "bFilter" : true,
        "bSort": false,
        "Filter": true,
        "Processing": true,
        "serverSide": true,
        "pageLength": 50,
       
        "ajax": {
            data: {list: 'list'},
            url: url + "sales_recieved_product/index?status="+get,
            type: "post",
            error: function () {
              console.log('its error')
            }
        },
       "language": {
            "sEmptyTable": "<i>No Data Found.</i>",
            "search": '<i class="fa fa-search"></i>',
            "searchPlaceholder": "Search By Chitti No"
        },
        "aoColumnDefs": [
                    { 'bSortable': false, 'aTargets': [] }
                ]
    });
    var dataTable= $('#sales_returned_product_table_dep').DataTable({
        "bLengthChange": false,
        "bFilter" : true,
        "bSort": false,
        "Filter": true,
        "Processing": true,
        "serverSide": true,
        "pageLength": 50,
       
        "ajax": {
            data: {list: 'list'},
            url: url + "sales_recieved_product/index?status="+get,
            type: "post",
            error: function () {
              console.log('its error')
            }
        },
       "language": {
            "sEmptyTable": "<i>No Data Found.</i>",
            "search": '<i class="fa fa-search"></i>',
            "searchPlaceholder": "Search By Chitti No"
        },
        "aoColumnDefs": [
                    { 'bSortable': false, 'aTargets': [] }
                ]
    });
   $('.dataTables_filter').remove(); 
  filterFunction(dataTable);
}


if (page_title == "Accounting List") {
  var dataTable= $('#accounting').DataTable({
        "bLengthChange": false,
        "bFilter" : true,
        "bSort": false,
        "Filter": true,
        "Processing": true,
        "serverSide": true,
        "pageLength": 50,
       
        "ajax": {
            data: {list: 'list'},
            url: url + "accounting/issue/index",
            type: "post",
            error: function () {
              console.log('its error')
            }
        },
       "language": {
            "sEmptyTable": "<i>No Data Found.</i>",
            "search": '<i class="fa fa-search"></i>',
            "searchPlaceholder": "Search By Name"
        },
        "aoColumnDefs": [
                    { 'bSortable': false, 'aTargets': [] }
                ]
    });
       $('.dataTables_filter').remove();
  filterFunction(dataTable);


  
    var dataTable= $('#accounting_receipt').DataTable({
        "bLengthChange": false,
        "bFilter" : true,
        "bSort": false,
        "Filter": true,
        "Processing": true,
        "serverSide": true,
        "pageLength": 50,
       
        "ajax": {
            data: {list: 'list'},
            url: url + "accounting/issue/index",
            type: "post",
            error: function () {
              console.log('its error')
            }
        },
       "language": {
            "sEmptyTable": "<i>No Data Found.</i>",
            "search": '<i class="fa fa-search"></i>',
            "searchPlaceholder": "Search By Name"
        },
        "aoColumnDefs": [
                    { 'bSortable': false, 'aTargets': [] }
                ]
    });
 $('.dataTables_filter').remove();
  filterFunction(dataTable);
}

if (page_title == "All Stock Products") {
  var dataTable= $('#sales_stock_table').DataTable({
        "bLengthChange": false,
        "bFilter" : true,
        "bSort": false,
        "Filter": true,
        "Processing": true,
        "serverSide": true,
        "pageLength": 50,
       
        "ajax": {
            data: {list: 'list'},
            url: url + "sales_stock/index?department="+department,
            type: "post",
            error: function () {
              console.log('its error')
            }
        },
       "language": {
            "sEmptyTable": "<i>No Data Found.</i>",
            "search": '<i class="fa fa-search"></i>',
            "searchPlaceholder": "Search By Chitti No"
        },
        "aoColumnDefs": [
                    { 'bSortable': false, 'aTargets': [] }
                ]
    });

    var dataTable= $('#sales_stock_approve_table').DataTable({
        "bLengthChange": false,
        "bFilter" : true,
        "bSort": false,
        "Filter": true,
        "Processing": true,
        "serverSide": true,
        "pageLength": 50,
       
        "ajax": {
            data: {list: 'list'},
            url: url + "ready_product/approved_by_sale",
            type: "post",
            error: function () {
              console.log('its error')
            }
        },
       "language": {
            "sEmptyTable": "<i>No Data Found.</i>",
            "search": '<i class="fa fa-search"></i>',
            "searchPlaceholder": "Search By Chitti No"
        },
        "aoColumnDefs": [
                    { 'bSortable': false, 'aTargets': [] }
                ]
    });
    var dataTable= $('#sales_stock_table_dep').DataTable({
        "bLengthChange": false,
        "bFilter" : true,
        "bSort": false,
        "Filter": true,
        "Processing": true,
        "serverSide": true,
        "pageLength": 50,
       
        "ajax": {
            data: {list: 'list'},
            url: url + "sales_stock/index",
            type: "post",
            error: function () {
              console.log('its error')
            }
        },
       "language": {
            "sEmptyTable": "<i>No Data Found.</i>",
            "search": '<i class="fa fa-search"></i>',
            "searchPlaceholder": "Search By Chitti No"
        },
        "aoColumnDefs": [
                    { 'bSortable': false, 'aTargets': [] }
                ]
    });
    $('.dataTables_filter').remove();
  filterFunction(dataTable);
}

if (page_title == "All Color Stone") {
  var dataTable= $('#color_stone_table').DataTable({
        "bLengthChange": false,
        "bFilter" : true,
        "bSort": false,
        "Filter": true,
        "Processing": true,
        "serverSide": true,
        "ajax": {
            data: {list: 'list'},
            url: url + "color_stone/index",
            type: "post",
            error: function () {
              console.log('its error')
            }
        },
       "language": {
            "sEmptyTable": "<i>No Data Found.</i>",
            "search": '<i class="fa fa-search"></i>',
            "searchPlaceholder": "Search By Color Stone"
        },
        "aoColumnDefs": [
                    { 'bSortable': false, 'aTargets': [] }
                ]
    });
  filterFunction(dataTable);
}

if (page_title == "All Kundan Category") {
  var dataTable= $('#kundan_category_table').DataTable({
        "bLengthChange": false,
        "bFilter" : true,
        "bSort": false,
        "Filter": true,
        "Processing": true,
        "serverSide": true,
        "ajax": {
            data: {list: 'list'},
            url: url + "kundan_category/index",
            type: "post",
            error: function () {
              console.log('its error')
            }
        },
       "language": {
            "sEmptyTable": "<i>No Data Found.</i>",
            "search": '<i class="fa fa-search"></i>',
            "searchPlaceholder": "Search By category"
        },
        "aoColumnDefs": [
                    { 'bSortable': false, 'aTargets': [] }
                ]
    });
  filterFunction(dataTable);
}

if (page_title == "All Type") {
  var dataTable= $('#type_master_table').DataTable({
        "bLengthChange": false,
        "bFilter" : true,
        "bSort": false,
        "Filter": true,
        "Processing": true,
        "serverSide": true,
        "pageLength": 50,
       
        "ajax": {
            data: {list: 'list'},
            url: url + "type_master/index",
            type: "post",
            error: function () {
              console.log('its error')
            }
        },
       "language": {
            "sEmptyTable": "<i>No Data Found.</i>",
            "search": '<i class="fa fa-search"></i>',
            "searchPlaceholder": "Search By type"
        },
        "aoColumnDefs": [
                    { 'bSortable': false, 'aTargets': [] }
                ]
    });
   $('.dataTables_filter').remove();
  filterFunction(dataTable);
}

if (page_title == "All Customers") {
  var dataTable= $('#customer_table').DataTable({
        "bLengthChange": false,
        "bFilter" : true,
        "bSort": false,
        "Filter": true,
        "Processing": true,
        "serverSide": true,
        "ajax": {
            data: {list: 'list'},
            url: url + "customer/index",
            type: "post",
            error: function () {
              console.log('its error')
            }
        },
       "language": {
            "sEmptyTable": "<i>No Data Found.</i>",
            "search": '<i class="fa fa-search"></i>',
            "searchPlaceholder": "Search By Customer"
        },
        "aoColumnDefs": [
                    { 'bSortable': false, 'aTargets': [] }
                ]
    });
  filterFunction(dataTable);
}
if (page_title == "Sales Invoice") {
  var dataTable= $('#sales_invoice_table').DataTable({
        "bLengthChange": false,
        "bFilter" : true,
        "bSort": false,
        "Filter": true,
        "Processing": true,
        "serverSide": true,
        "ajax": {
            data: {list: 'list'},
            url: url + "sales_invoice/index?status="+get,
            type: "post",
            error: function () {
              console.log('its error')
            }
        },
       "language": {
            "sEmptyTable": "<i>No Data Found.</i>",
            "search": '<i class="fa fa-search"></i>',
            "searchPlaceholder": "Search By Chitti No"
        },
        "aoColumnDefs": [
                    { 'bSortable': false, 'aTargets': [] }
                ]
    });
  filterFunction(dataTable);
}

if (page_title == "Dispatch Invoice") {
  var dataTable= $('#dispatch_invoice_table').DataTable({
        "bLengthChange": false,
        "bFilter" : true,
        "bSort": false,
        "Filter": true,
        "Processing": true,
        "serverSide": true,
        "ajax": {
            data: {list: 'list'},
            url: url + "Dispatch_invoices/index?status="+get,
            type: "post",
            error: function () {
              console.log('its error')
            }
        },
       "language": {
            "sEmptyTable": "<i>No Data Found.</i>",
            "search": '<i class="fa fa-search"></i>',
            "searchPlaceholder": "Search By Chitti No"
        },
        "aoColumnDefs": [
                    { 'bSortable': false, 'aTargets': [] }
                ]
    });
   $('.dataTables_filter').remove();
  filterFunction(dataTable);
}


if (page_title == "Dispatch Hallmarking") {
  var dataTable= $('#dispatch_hallmarking_table').DataTable({
        "bLengthChange": false,
        "bFilter" : true,
        "bSort": false,
        "Filter": true,
        "Processing": true,
        "serverSide": true,
        "ajax": {
            data: {list: 'list'},
            url: url + "Dispatch/hallmarking/index?status="+get,
            type: "post",
            error: function () {
              console.log('its error')
            }
        },
       "language": {
            "sEmptyTable": "<i>No Data Found.</i>",
            "search": '<i class="fa fa-search"></i>',
            "searchPlaceholder": "Search By Item No"
        },
        "aoColumnDefs": [
                    { 'bSortable': false, 'aTargets': [] }
                ]
    });
  $('.dataTables_filter').remove();
  filterFunction(dataTable);
}

if (page_title == "View Dispatch Invoice") {
  var si=$('#invoice_id').val();
  var dataTable= $('#show_dispatch_invoice').DataTable({
        "bLengthChange": false,
        "bFilter" : true,
        "bSort": false,
        "Filter": true,
        "Processing": true,
        "serverSide": true,
        "ajax": {
            data: {list: 'list'},
            url: url + "Dispatch_invoices/show/"+si,
            type: "post",
            error: function () {
              console.log('its error')
            }
        },
       "language": {
            "sEmptyTable": "<i>No Data Found.</i>",
            "search": '<i class="fa fa-search"></i>',
            "searchPlaceholder": "Search By Item No"
        },
        "aoColumnDefs": [
                    { 'bSortable': false, 'aTargets': [] }
                ]
    });
  filterFunction(dataTable);
}

if (page_title == "Dispatch Hallmarking Recieved") {
  var dataTable= $('#dispatch_hallmarking_rec_table').DataTable({
        "bLengthChange": false,
        "bFilter" : true,
        "bSort": false,
        "Filter": true,
        "Processing": true,
        "serverSide": true,
        "ajax": {
            data: {list: 'list'},
            url: url + "Dispatch/hallmarking_recieved/index?status="+get,
            type: "post",
            error: function () {
              console.log('its error')
            }
        },
       "language": {
            "sEmptyTable": "<i>No Data Found.</i>",
            "search": '<i class="fa fa-search"></i>',
            "searchPlaceholder": "Search By Item No"
        },
        "aoColumnDefs": [
                    { 'bSortable': false, 'aTargets': [] }
                ]
    });
   $('.dataTables_filter').remove();
  filterFunction(dataTable);
}

if (page_title == "All Dispatch Modes") {
  var dataTable= $('#dispatch_mode_table').DataTable({
        "bLengthChange": false,
        "bFilter" : true,
        "bSort": false,
        "Filter": true,
        "Processing": true,
        "serverSide": true,
        "pageLength": 50,
       
        "ajax": {
            data: {list: 'list'},
            url: url + "dispatch_mode/index?status="+get,
            type: "post",
            error: function () {
              console.log('its error')
            }
        },
       "language": {
            "sEmptyTable": "<i>No Data Found.</i>",
            "search": '<i class="fa fa-search"></i>',
            "searchPlaceholder": "Search"
        },
        "aoColumnDefs": [
                    { 'bSortable': false, 'aTargets': [] }
                ]
    });
       $('.dataTables_filter').remove();   
  filterFunction(dataTable);
}


if (page_title == "All Dispatch Products") {
  var dataTable= $('#dispatch_products_table').DataTable({
        "bLengthChange": false,
        "bFilter" : true,
        "bSort": false,
        "Filter": true,
        "Processing": true,
        "serverSide": true,
        "ajax": {
            data: {list: 'list'},
            url: url + "Dispatch/dispatch_products/index?status="+get,
            type: "post",
            error: function () {
              console.log('its error')
            }
        },
       "language": {
            "sEmptyTable": "<i>No Data Found.</i>",
            "search": '<i class="fa fa-search"></i>',
            "searchPlaceholder": "Search By Product Code"
        },
        "aoColumnDefs": [{ 'bSortable': false, 'aTargets': [] }]
    });
    $('.dataTables_filter').remove();
  filterFunction(dataTable);
}

if (page_title == "view challan") {
var challan_no = $('#challan_no').val();
  var dataTable= $('#view_challantbl').DataTable({
        "bLengthChange": false,
        "bFilter" : true,
        "bSort": false,
        "Filter": true,
        "Processing": true,
        "serverSide": true,
        "ajax": {
            data: {list: 'list'},
            url: url + "generate_chitti/show/"+challan_no+"?status="+get,
            type: "post",
            error: function () {
              console.log('its error')
            }
        },
       "language": {
            "sEmptyTable": "<i>No Data Found.</i>",
            "search": '<i class="fa fa-search"></i>',
            "searchPlaceholder": "Search By Product Code"
        },
        "aoColumnDefs": [{ 'bSortable': false, 'aTargets': [] }]
    });
  filterFunction(dataTable);
}

if (page_title == "Jewerly Report") {
  var dataTable= $('#jewerly_report_table').DataTable({
        "bLengthChange": false,
        "bFilter" : true,
        "bSort": false,
        "Filter": true,
        "Processing": true,
        "serverSide": true,
        "pageLength": 50,
       
        "ajax": {
            data: {list: 'list'},
            url: url + "corporate_report/jewerly_report/index?status="+get,
            type: "post",
            error: function () {
              console.log('its error')
            }
        },
       "language": {
            "sEmptyTable": "<i>No Data Found.</i>",
            "search": '<i class="fa fa-search"></i>',
            "searchPlaceholder": "Search By Corporate"
        },
        "aoColumnDefs": [{ 'bSortable': false, 'aTargets': [] }]
    });
    $('.dataTables_filter').remove(); 
  filterFunction(dataTable);
}

if (page_title == "All Recieved Products Show") {
    var chitti = $('#chitti').val();
  var dataTable= $('#sales_recieved_product_show_table').DataTable({
        "bLengthChange": false,
        "bFilter" : true,
        "bSort": false,
        "Filter": true,
        "Processing": true,
        "serverSide": true,
         "pageLength": 50,
       
        "ajax": {
            data: {list: 'list'},
            url: url + "sales_recieved_product/show/"+chitti+"?status="+get,
            type: "post",
            error: function () {
              console.log('its error')
            }
        },
       "language": {
            "sEmptyTable": "<i>No Data Found.</i>",
            "search": '<i class="fa fa-search"></i>',
            "searchPlaceholder": "Search By Parent category"
        },
        "aoColumnDefs": [
                    { 'bSortable': false, 'aTargets': [] }
                ]
    });
      var dataTable= $('#sales_recieved_product_show_table_dep').DataTable({
        "bLengthChange": false,
        "bFilter" : true,
        "bSort": false,
        "Filter": true,
        "Processing": true,
        "serverSide": true,
         "pageLength": 50,
       
        "ajax": {
            data: {list: 'list'},
            url: url + "sales_recieved_product/show/"+chitti+"?status="+get,
            type: "post",
            error: function () {
              console.log('its error')
            }
        },
       "language": {
            "sEmptyTable": "<i>No Data Found.</i>",
            "search": '<i class="fa fa-search"></i>',
            "searchPlaceholder": "Search By Parent category"
        },
        "aoColumnDefs": [
                    { 'bSortable': false, 'aTargets': [] }
                ]
    });
  
  
    var dataTable= $('#sales_recieved_product_show_stock_table').DataTable({
        "bLengthChange": false,
        "bFilter" : true,
        "bSort": false,
        "Filter": true,
        "Processing": true,
        "serverSide": true,
         "pageLength": 50,
       
        "ajax": {
            data: {list: 'list'},
            url: url + "sales_recieved_product/show/"+chitti+"?status="+get,
            type: "post",
            error: function () {
              console.log('its error')
            }
        },
       "language": {
            "sEmptyTable": "<i>No Data Found.</i>",
            "search": '<i class="fa fa-search"></i>',
            "searchPlaceholder": "Search By Parent category"
        },
        "aoColumnDefs": [
                    { 'bSortable': false, 'aTargets': [] }
                ]
    });
   $('.dataTables_filter').remove();
   var dataTable= $('#sales_recieved_product_show_stock_table_dep').DataTable({
        "bLengthChange": false,
        "bFilter" : true,
        "bSort": false,
        "Filter": true,
        "Processing": true,
        "serverSide": true,
         "pageLength": 50,
       
        "ajax": {
            data: {list: 'list'},
            url: url + "sales_recieved_product/show/"+chitti+"?status="+get,
            type: "post",
            error: function () {
              console.log('its error')
            }
        },
       "language": {
            "sEmptyTable": "<i>No Data Found.</i>",
            "search": '<i class="fa fa-search"></i>',
            "searchPlaceholder": "Search By Parent category"
        },
        "aoColumnDefs": [
                    { 'bSortable': false, 'aTargets': [] }
                ]
    });
   $('.dataTables_filter').remove();
  filterFunction(dataTable);
}

if (page_title == "All customer Types") {
  var dataTable= $('#customer_type_table').DataTable({
        "bLengthChange": false,
        "bFilter" : true,
        "bSort": false,
        "Filter": true,
        "Processing": true,
        "serverSide": true,
         "pageLength": 50,
       
        "ajax": {
            data: {list: 'list'},
            url: url + "customer_type/index",
            type: "post",
            error: function () {
              console.log('its error')
            }
        },
       "language": {
            "sEmptyTable": "<i>No Data Found.</i>",
            "search": '<i class="fa fa-search"></i>',
            "searchPlaceholder": "Search By Name"
        },
        "aoColumnDefs": [
                    { 'bSortable': false, 'aTargets': [] }
                ]
    });
  $('.dataTables_filter').remove();

  filterFunction(dataTable);
}

if (page_title == "All Salesmans") {
  var dataTable= $('#salesman_master_table').DataTable({
        "bLengthChange": false,
        "bFilter" : true,
        "bSort": false,
        "Filter": true,
        "Processing": true,
        "serverSide": true,
         "pageLength": 50,
       
        "ajax": {
            data: {list: 'list'},
            url: url + "salesman_master/index",
            type: "post",
            error: function () {
              console.log('its error')
            }
        },
       "language": {
            "sEmptyTable": "<i>No Data Found.</i>",
            "search": '<i class="fa fa-search"></i>',
            "searchPlaceholder": "Search By Salesmans"
        },
        "aoColumnDefs": [
                    { 'bSortable': false, 'aTargets': [] }
                ]
    });
   $('.dataTables_filter').remove();
  filterFunction(dataTable);
}

if (page_title == "Karigar Report") {
  var dataTable= $('#karigar_report_table').DataTable({
        "bLengthChange": false,
        "bFilter" : true,
        "bSort": false,
        "Filter": true,
        "Processing": true,
        "serverSide": true,
         "pageLength": 50,
       
        "ajax": {
            data: {list: 'list'},
            url: url + "Report/Karigar_report_manufacture",
            type: "post",
            error: function () {
              console.log('its error')
            }
        },
       "language": {
            "sEmptyTable": "<i>No Data Found.</i>",
            "search": '<i class="fa fa-search"></i>',
            "searchPlaceholder": "Search By Karigar"
        },
        "aoColumnDefs": [
                    { 'bSortable': false, 'aTargets': [] }
                ]
    });
    $('.dataTables_filter').remove();
  filterFunction(dataTable);
}

if (page_title == "Order Status Report") {
  var dataTable= $('#order_status_report_table').DataTable({
        "bLengthChange": false,
        "bFilter" : true,
        "bSort": false,
        "Filter": true,
        "Processing": true,
        "serverSide": true,
         "pageLength": 50,
       
        "ajax": {
            data: {list: 'list'},
            url: url + "Manufacture_report/order_status",
            type: "post",
            error: function () {
              console.log('its error')
            }
        },
       "language": {
            "sEmptyTable": "<i>No Data Found.</i>",
            "search": '<i class="fa fa-search"></i>',
            "searchPlaceholder": "Search By Order Name"
        },
        "aoColumnDefs": [
                    { 'bSortable': false, 'aTargets': [] }
                ]
    });
    $('.dataTables_filter').remove();
  filterFunction(dataTable);
}

if (page_title == "Daily stock report") {
    // alert(from);
  var dataTable= $('#daily_stock_report_table').DataTable({
        "bLengthChange": false,
        "bFilter" : true,
        "bSort": false,
        "Filter": true,
        "Processing": true,
        "serverSide": true,
         "pageLength": 50,
       
        "ajax": {
            data: {list: 'list'},
            url: url + "Daily_stock_summary_report?department_id="+department_id+"&from="+from_date+"&to="+to_date,
             //url: url + "Manufacture_report/Daily_stock_summary_report",
            type: "post",
            error: function () {
              console.log('its error')
            }
        },
       "language": {
            "sEmptyTable": "<i>No Data Found.</i>",
            "search": '<i class="fa fa-search"></i>',
            "searchPlaceholder": "Search By Order Name"
        },
        "aoColumnDefs": [
                    { 'bSortable': false, 'aTargets': [] }
                ]
    });
    $('.dataTables_filter').remove();
  filterFunction(dataTable);
}





if (page_title == "Work order report") {
   //alert(email_order_id); 
 filterFunction ($('#work_order_detail_table').DataTable({
        "bLengthChange": false,
        "bFilter" : true,
        "bSort": false,
        "Filter": true,
        "Processing": true,
        "serverSide": true,
         "pageLength": 50,
       
        "ajax": {
            data: {list: 'list'},
            url: url + "Corporate_order_list/work_order_detail?order_id="+order_id+"&from="+from_date+"&to="+to_date,
             //url: url + "Manufacture_report/Daily_stock_summary_report",
            type: "post",
            error: function () {
              console.log('its error')
            }
        },
       "language": {
            "sEmptyTable": "<i>No Data Found.</i>",
            "search": '<i class="fa fa-search"></i>',
            "searchPlaceholder": "Search By Order Name"
        },
        "aoColumnDefs": [
                    { 'bSortable': false, 'aTargets': [] }
                ]
    }));
    $('.dataTables_filter').remove();
  //filterFunction(dataTable);
}

if (page_title == "Issue Voucher") {
  var dataTable= $('#issue_voucher').DataTable({
        "bLengthChange": false,
        "bFilter" : true,
        "bSort": false,
        "Filter": true,
        "Processing": true,
        "serverSide": true,
         "pageLength": 50,
       
        "ajax": {
            data: {list: 'list'},
            url: url + "Manufacture_accounting/Issue_voucher/index",
            type: "post",
            error: function () {
              console.log('its error')
            }
        },
       "language": {
            "sEmptyTable": "<i>No Data Found.</i>",
            "search": '<i class="fa fa-search"></i>',
            "searchPlaceholder": "Search By Voucher No"
        },
        "aoColumnDefs": [
                    { 'bSortable': false, 'aTargets': [] }
                ]
    });
   $('.dataTables_filter').remove();
  filterFunction(dataTable);
}

if (page_title == "Cash Bank Issue") {
  var dataTable= $('#cash_bank_voucher').DataTable({
        "bLengthChange": false,
        "bFilter" : true,
        "bSort": false,
        "Filter": true,
        "Processing": true,
        "serverSide": true,
         "pageLength": 50,
       
        "ajax": {
            data: {list: 'list'},
            url: url + "Manufacture_accounting/Cash_bank_issue/index",
            type: "post",
            error: function () {
              console.log('its error')
            }
        },
       "language": {
            "sEmptyTable": "<i>No Data Found.</i>",
            "search": '<i class="fa fa-search"></i>',
            "searchPlaceholder": "Search By Party Name"
        },
        "aoColumnDefs": [
                    { 'bSortable': false, 'aTargets': [] }
                ]
    });
     $('.dataTables_filter').remove();
  filterFunction(dataTable);
}

if (page_title == "Metal Issue Voucher") {
  var dataTable= $('#metal_issue_voucher').DataTable({
        "bLengthChange": false,
        "bFilter" : true,
        "bSort": false,
        "Filter": true,
        "Processing": true,
        "serverSide": true,
         "pageLength": 50,
       
        "ajax": {
            data: {list: 'list'},
            url: url + "Manufacture_accounting/Metal_issue/index",
            type: "post",
            error: function () {
              console.log('its error')
            }
        },
       "language": {
            "sEmptyTable": "<i>No Data Found.</i>",
            "search": '<i class="fa fa-search"></i>',
            "searchPlaceholder": "Search By Party Name"
        },
        "aoColumnDefs": [
                    { 'bSortable': false, 'aTargets': [] }
                ]
    });
   $('.dataTables_filter').remove();
  filterFunction(dataTable);
}

if (page_title == "All Material") {
  var dataTable= $('#material_master_table').DataTable({
        "bLengthChange": false,
        "bFilter" : true,
        "bSort": false,
        "Filter": true,
        "Processing": true,
        "serverSide": true,
         "pageLength": 50,
       
        "ajax": {
            data: {list: 'list'},
            url: url + "Material/index",
            type: "post",
            error: function () {
              console.log('its error')
            }
        },
       "language": {
            "sEmptyTable": "<i>No Data Found.</i>",
            "search": '<i class="fa fa-search"></i>',
            "searchPlaceholder": "Search By Short Code"
        },
        "aoColumnDefs": [
                    { 'bSortable': false, 'aTargets': [] }
                ]
    });
    $('.dataTables_filter').remove();
  filterFunction(dataTable);
}

if (page_title == "All Variations Categories") {
  var dataTable= $('#variation_category_table').DataTable({
        "bLengthChange": false,
        "bFilter" : true,
        "bSort": false,
        "Filter": true,
        "Processing": true,
        "serverSide": true,
         "pageLength": 50,
       
        "ajax": {
            data: {list: 'list'},
            url: url + "Variations_category/index",
            type: "post",
            error: function () {
              console.log('its error')
            }
        },
       "language": {
            "sEmptyTable": "<i>No Data Found.</i>",
            "search": '<i class="fa fa-search"></i>',
            "searchPlaceholder": "Search By Variation Category"
        },
        "aoColumnDefs": [
                    { 'bSortable': false, 'aTargets': [] }
                ]
    });
  $('.dataTables_filter').remove();
  filterFunction(dataTable);
}

if (page_title == "Accounting Recieved List") {
  var dataTable= $('#accounting').DataTable({
        "bLengthChange": false,
        "bFilter" : true,
        "bSort": false,
        "Filter": true,
        "Processing": true,
        "serverSide": true,
         "pageLength": 50,
       
        "ajax": {
            data: {list: 'list'},
            url: url + "accounting/receipt/index",
            type: "post",
            error: function () {
              console.log('its error')
            }
        },
       "language": {
            "sEmptyTable": "<i>No Data Found.</i>",
            "search": '<i class="fa fa-search"></i>',
            "searchPlaceholder": "Search By Name"
        },
        "aoColumnDefs": [
                    { 'bSortable': false, 'aTargets': [] }
                ]
    });
  $('.dataTables_filter').remove();
  filterFunction(dataTable);
}

if (page_title == "Corporate Accounting Report") {
  var dataTable= $('#accounting_report_table').DataTable({
        "bLengthChange": false,
        "bFilter" : true,
        "bSort": false,
        "Filter": true,
        "Processing": true,
        "serverSide": true,
         "pageLength": 50,
       
        "ajax": {
            data: {list: 'list'},
            url: url + "corporate_report/Accounting_report/index",
            type: "post",
            error: function () {
              console.log('its error')
            }
        },
       "language": {
            "sEmptyTable": "<i>No Data Found.</i>",
            "search": '<i class="fa fa-search"></i>',
            "searchPlaceholder": "Search By Name"
        },
        "aoColumnDefs": [
                    { 'bSortable': false, 'aTargets': [] }
                ]
    });
    $('.dataTables_filter').remove();
  filterFunction(dataTable);
}
if (page_title == "Corporate Order Tracking") {
  var dataTable= $('#order_tracking_table').DataTable({
        "bLengthChange": false,
        "bFilter" : true,
        "bSort": false,
        "Filter": true,
        "Processing": true,
        "serverSide": true,
        "bPaginate": true,
        "bInfo" : true,  

        "ajax": {
            data: {list: 'list'},
            url: url + "Order_Tracking/"+order_reminder,
            type: "post",
            error: function () {
              console.log('its error')
            }
        },
       "language": {
            "sEmptyTable": "<i>No Data Found.</i>",
            "search": '<i class="fa fa-search"></i>',
            "searchPlaceholder": "Search By Name"
        },
        "aoColumnDefs": [
                    { 'bSortable': false, 'aTargets': [] }
                ]
    });
    $('.dataTables_filter').remove();
  filterFunction(dataTable);
}

if (page_title == "Corporate Designs Tracking") {
  var dataTable= $('#design_tracking_table').DataTable({
        "bLengthChange": false,
        "bFilter" : true,
        "bSort": false,
        "Filter": true,
        "Processing": true,
        "serverSide": true,
        "bPaginate": true,
        "bInfo" : true,  

        "ajax": {
            data: {list: 'list'},
            url: url + "Design_Tracking/"+order_reminder,
            type: "post",
            error: function () {
              console.log('its error')
            }
        },
       "language": {
            "sEmptyTable": "<i>No Data Found.</i>",
            "search": '<i class="fa fa-search"></i>',
            "searchPlaceholder": "Search By Name"
        },
        "aoColumnDefs": [
                    { 'bSortable': false, 'aTargets': [] }
                ]
    });
    $('.dataTables_filter').remove();
  filterFunction(dataTable);
}


if (page_title == "View Order Status Report") {
  var dataTable= $('#order_status_view').DataTable({
        "bLengthChange": false,
        "bFilter" : true,
        "bSort": false,
        "Filter": true,
        "Processing": true,
        "serverSide": true,
         "pageLength": 50,
       
        "ajax": {
            data: {list: 'list'},
            url: url + "Manufacture_report/order_status/view/"+order_id,
            type: "post",
            error: function () {
              console.log('its error')
            }
        },
       "language": {
            "sEmptyTable": "<i>No Data Found.</i>",
            "search": '<i class="fa fa-search"></i>',
            "searchPlaceholder": "Search By Name"
        },
        "aoColumnDefs": [
                    { 'bSortable': false, 'aTargets': [] }
                ]
    });
  filterFunction(dataTable);
}

if (page_title == "All Terms") {
  var dataTable= $('#terms_table').DataTable({
        "bLengthChange": false,
        "bFilter" : true,
        "bSort": false,
        "Filter": true,
        "Processing": true,
        "serverSide": true,
        "pageLength": 50,
       
        "ajax": {
            data: {list: 'list'},
            url: url + "Term/index",
            type: "post",
            error: function () {
              console.log('its error')
            }
        },
       "language": {
            "sEmptyTable": "<i>No Data Found.</i>",
            "search": '<i class="fa fa-search"></i>',
            "searchPlaceholder": "Search By Parent Category"
        },
        "aoColumnDefs": [
                    { 'bSortable': false, 'aTargets': [] }
                ]
    });
    $('.dataTables_filter').remove();
  filterFunction(dataTable);
}

if (page_title == "Corporate Overall Report View") {
    var search_by="Product Code";
    if ($('#report_name_overall').val()=="total_placed_order") {
        search_by="Order Name";
    }
  var dataTable= $('#overall_report_view_table').DataTable({
        "bLengthChange": false,
        "bFilter" : true,
        "bSort": false,
        "Filter": true,
        "Processing": true,
        "serverSide": true,
        "ajax": {
            data: {list: 'list'},
            url: url + "corporate_report/Overall_report/show/"+$('#report_name_overall').val(),
            type: "post",
            error: function () {
              console.log('its error')
            }
        },
       "language": {
            "sEmptyTable": "<i>No Data Found.</i>",
            "search": '<i class="fa fa-search"></i>',
            "searchPlaceholder": ""+search_by+"/ Corporate",
        },
        "aoColumnDefs": [
                    { 'bSortable': false, 'aTargets': [] }
                ]
    });
  filterFunction(dataTable);
}

if (page_title == "All Parties") {
  var dataTable= $('#party_master_table').DataTable({
        "bLengthChange": false,
        "bFilter" : true,
        "bSort": false,
        "Filter": true,
        "Processing": true,
        "serverSide": true,
        "pageLength": 50,
        "stateSave": true,
        "ajax": {
            data: {list: 'list'},
            url: url + "Party_master/index",
            type: "post",
            error: function () {
              console.log('its error')
            }
        },
       "language": {
            "sEmptyTable": "<i>No Data Found.</i>",
            "search": '<i class="fa fa-search"></i>',
            "searchPlaceholder": "Search By Name"
        },
        "aoColumnDefs": [
                    { 'bSortable': false, 'aTargets': [] }
                ]
    });
  $('.dataTables_filter').remove();
  filterFunction(dataTable);
}

if (page_title == "Jewerly Report Indetail") {
  var seacrh_by="Product Code";
  var func_name=$('#func_name').val();
  var corporate_id=$('#corporate_id').val();

  if (func_name =="recived_weight" || func_name=="order_receipts") {
    seacrh_by="Order Name";
  }
  var dataTable= $('#jewerly_report_table_detail').DataTable({
        "bLengthChange": true,
        "bFilter" : true,
        "bSort": false,
        "Filter": true,
        "Processing": true,
        "serverSide": true,
        "pageLength": 50,
        "stateSave": true,
        "ajax": {
            data: {list: 'list'},
            url: url+"corporate_report/Jewerly_report/"+func_name+"/"+corporate_id,
            type: "post",
            error: function () {
              console.log('its error')
            }
        },
       "language": {
            "sEmptyTable": "<i>No Data Found.</i>",
            "search": '<i class="fa fa-search"></i>',
            "searchPlaceholder": "Search By "+seacrh_by,
        },
        "aoColumnDefs": [
                    { 'bSortable': false, 'aTargets': [] }
                ]
    });
    var dataTable= $('#jewerly_report_table_detail_recived_weight').DataTable({
        "bLengthChange": true,
        "bFilter" : true,
        "bSort": false,
        "Filter": true,
        "Processing": true,
        "serverSide": true,
        "pageLength": 50,
       
        "ajax": {
            data: {list: 'list'},
            url: url+"corporate_report/Jewerly_report/"+func_name+"/"+corporate_id,
            type: "post",
            error: function () {
              console.log('its error')
            }
        },
       "language": {
            "sEmptyTable": "<i>No Data Found.</i>",
            "search": '<i class="fa fa-search"></i>',
            "searchPlaceholder": "Search By "+seacrh_by,
        },
        "aoColumnDefs": [
                    { 'bSortable': false, 'aTargets': [] }
                ]
    });
      var dataTable= $('#jewerly_report_table_detail_order_receipts').DataTable({
        "bLengthChange": true,
        "bFilter" : true,
        "bSort": false,
        "Filter": true,
        "Processing": true,
        "serverSide": true,
        "pageLength": 50,
       
        "ajax": {
            data: {list: 'list'},
            url: url+"corporate_report/Jewerly_report/"+func_name+"/"+corporate_id,
            type: "post",
            error: function () {
              console.log('its error')
            }
        },
       "language": {
            "sEmptyTable": "<i>No Data Found.</i>",
            "search": '<i class="fa fa-search"></i>',
            "searchPlaceholder": "Search By "+seacrh_by,
        },
        "aoColumnDefs": [
                    { 'bSortable': false, 'aTargets': [] }
                ]
    });
      var dataTable= $('#jewerly_report_table_detail_rorder_pending').DataTable({
        "bLengthChange": true,
        "bFilter" : true,
        "bSort": false,
        "Filter": true,
        "Processing": true,
        "serverSide": true,
        "pageLength": 50,
       
        "ajax": {
            data: {list: 'list'},
            url: url+"corporate_report/Jewerly_report/"+func_name+"/"+corporate_id,
            type: "post",
            error: function () {
              console.log('its error')
            }
        },
       "language": {
            "sEmptyTable": "<i>No Data Found.</i>",
            "search": '<i class="fa fa-search"></i>',
            "searchPlaceholder": "Search By "+seacrh_by,
        },
        "aoColumnDefs": [
                    { 'bSortable': false, 'aTargets': [] }
                ]
    });
      $('.dataTables_filter').remove(); 
  filterFunction(dataTable);
}
/*order module*/
if (page_title == "Customer Order List") {
   
  filterFunction($('#customer_order').DataTable({
        "bLengthChange": false,
        "bFilter" : true,
        "bSort": false,
        "Filter": true,
        "Processing": true,
        "serverSide": true,
        "pageLength": 50,
       
        
        "ajax": {
            data: {list: 'list'},
            url: url+"Customer_order/index?status="+get,
            type: "post",
            error: function () {
              console.log('its error')
            }
        },
       "language": {
            "sEmptyTable": "<i>No Data Found.</i>",
            "search": '<i class="fa fa-search"></i>',
            "searchPlaceholder": "Order Name/ Party/ karigar",
        },
        "aoColumnDefs": [
                    { 'bSortable': false, 'aTargets': [] }
                ]
    }));  
  filterFunction($('#customer_cancel_order').DataTable({
        "bLengthChange": false,
        "bFilter" : true,
        "bSort": false,
        "Filter": true,
        "Processing": true,
        "serverSide": true,
        "pageLength": 50,
       
        
        "ajax": {
            data: {list: 'list'},
            url: url+"Cancelled_order/index?status="+get,
            type: "post",
            error: function () {
              console.log('its error')
            }
        },
       "language": {
            "sEmptyTable": "<i>No Data Found.</i>",
            "search": '<i class="fa fa-search"></i>',
            "searchPlaceholder": "Order Name/ Party/ karigar",
        },
        "aoColumnDefs": [
                    { 'bSortable': false, 'aTargets': [] }
                ]
    }));

    filterFunction($('#customer_app_order').DataTable({
        "bLengthChange": false,
        "bFilter" : true,
        "bSort": false,
        "Filter": true,
        "Processing": true,
        "serverSide": true,
        "pageLength": 50,
       
        
        "ajax": {
            data: {list: 'list'},
            url: url+"Customer_order_by_app/index?status="+get,
            type: "post",
            error: function () {
              console.log('its error')
            }
        },
       "language": {
            "sEmptyTable": "<i>No Data Found.</i>",
            "search": '<i class="fa fa-search"></i>',
            "searchPlaceholder": "Order Name/ Party/ karigar",
        },
        "aoColumnDefs": [
                    { 'bSortable': false, 'aTargets': [] }
                ]
    }));

  filterFunction ($('#customer_order_not_send').DataTable({

        "bLengthChange": false,
        "bFilter" : true,
        "bSort": false,
        "Filter": true,
        "Processing": true,
        "serverSide": true,
        "ajax": {
            data: {list: 'list'},
            url: url+"Customer_order/index?status="+get,
            type: "post",
            error: function () {
              console.log('its error')
            }
        },
       "language": {
            "sEmptyTable": "<i>No Data Found.</i>",
            "search": '<i class="fa fa-search"></i>',
            "searchPlaceholder": "Order Name/ Party/ karigar",
        },
        "aoColumnDefs": [
                    { 'bSortable': false, 'aTargets': [] }
                ]
    }));
  //filterFunction(dataTable);

 $('.dataTables_filter').remove();
}

if (page_title == "Karigar Pending To Be Received List") {
       // var ctr ="Customer_order/index?status="+get;
        var ctr ="Customer_order";
filterFunction($('#customer_order_pending').DataTable({
        "pageLength":50,
        "bLengthChange": false,
        "bFilter" : true,
        "bSort": false,
        "Filter": true,
        "Processing": true,
        "serverSide": true,
        "ajax": {
            data: {list: 'list'},
            url: url+"Customer_order/index?status="+get,
            type: "post",
            error: function () {
              console.log('its error')
            }
        },
       "language": {
            "sEmptyTable": "<i>No Data Found.</i>",
            "search": '<i class="fa fa-search"></i>',
            "searchPlaceholder": "Order Number / Party / karigar",
        },
        "fnDrawCallback": function() {
           jQuery('.datepicker_deilverydate').datepicker({
                format: 'dd-mm-yyyy',
                autoclose: true,
                todayHighlight: true,
                orientation: "top auto",
                });
            },
        "aoColumnDefs": [
                    { 'bSortable': false, 'aTargets': [] }
                ]
    }));
   //  filterFunction(dataTable);
   // get_child_row('customer_order_pending',dataTable,ctr); 
    $('.dataTables_filter').remove();
    $('table tbody tr').addClass('abc');
}
if (page_title == "Pending_to_be_recieved") {
  var dataTable= $('#receive_kariagar_tbl').DataTable({
        "bLengthChange": false,
        "bFilter" : true,
        "bSort": false,
        "Filter": true,
        "Processing": true,
        "serverSide": true,
        "ajax": {
            data: {list: 'list'},
            url: url+"Pending_to_be_recieved/index",
            type: "post",
            error: function () {
              console.log('its error')
            }
        },
       "language": {
            "sEmptyTable": "<i>No Data Found.</i>",
            "search": '<i class="fa fa-search"></i>',
            "searchPlaceholder": "Search By Karigar",
        },
        "aoColumnDefs": [
                    { 'bSortable': false, 'aTargets': [] }
                ]
    });
  filterFunction(dataTable);
}

if (page_title == "Recieved_products Order") {
  var dataTable= $('#receive_products_tbl').DataTable({
        "bLengthChange": false,
        "bFilter" : true,
        "bSort": false,
        "Filter": true,
        "Processing": true,
        "serverSide": true,
        "pageLength": 50,
       
        "ajax": {
            data: {list: 'list'},
            url: url+"Recieved_customer_products/index?status="+get,
            type: "post",
            error: function () {
              console.log('its error')
            }
        },
       "language": {
            "sEmptyTable": "<i>No Data Found.</i>",
            "search": '<i class="fa fa-search"></i>',
            "searchPlaceholder": "Search By Karigar",
        },
        "aoColumnDefs": [
                    { 'bSortable': false, 'aTargets': [] }
                ]
    });
  filterFunction(dataTable);
}
if (page_title == "Recevied Orders") {
  var dataTable= $('#received_orders_by_order_id').DataTable({
        "bLengthChange": false,
        "bFilter" : true,
        "bSort": false,
        "Filter": true,
        "Processing": true,
        "serverSide": true,
        "pageLength": 50,
       
        "ajax": {
            data: {list: 'list'},
            url: url+"Received_orders/index?status="+get,
            type: "post",
            error: function () {
              console.log('its error')
            }
        },
       "language": {
            "sEmptyTable": "<i>No Data Found.</i>",
            "search": '<i class="fa fa-search"></i>',
            "searchPlaceholder": "Search By Order ID",
        },
        "aoColumnDefs": [
                    { 'bSortable': false, 'aTargets': [] }
                ]
    });
   $('.dataTables_filter').remove();
  filterFunction(dataTable);
}
if (page_title == "Manufacturing Accounting Received Orders") {
  var dataTable= $('#mnfg_accounting_by_order_id').DataTable({
        "bLengthChange": false,
        "bFilter" : true,
        "bSort": false,
        "Filter": true,
        "Processing": true,
        "serverSide": true,
        "pageLength": 50,
       
        "ajax": {
            data: {list: 'list'},
            url: url+"Mnfg_accounting/index",
            type: "post",
            error: function () {
              console.log('its error')
            }
        },
       "language": {
            "sEmptyTable": "<i>No Data Found.</i>",
            "search": '<i class="fa fa-search"></i>',
            "searchPlaceholder": "Search By Order ID",
        },
        "aoColumnDefs": [
                    { 'bSortable': false, 'aTargets': [] }
                ]
    });
  filterFunction(dataTable);
}
if (page_title == "Manufacturing Accounting Rejected Orders") {
  var dataTable= $('#manufacturing_accounting_rejected_product').DataTable({
        "bLengthChange": false,
        "bFilter" : true,
        "bSort": false,
        "Filter": true,
        "Processing": true,
        "serverSide": true,
        "pageLength": 50,
       
        "ajax": {
            data: {list: 'list'},
            url: url+"Mnfg_accounting_rejected_orders/index",
            type: "post",
            error: function () {
              console.log('its error')
            }
        },
       "language": {
            "sEmptyTable": "<i>No Data Found.</i>",
            "search": '<i class="fa fa-search"></i>',
            "searchPlaceholder": "search by order name /order ID",
        },
        "aoColumnDefs": [
                    { 'bSortable': false, 'aTargets': [] }
                ]
    });
  filterFunction(dataTable);
}

if (page_title == "App Customer List") {
    //alert("sss");
  var dataTable= $('#user_app_tbl').DataTable({
        "bLengthChange": false,
        "bFilter" : true,
        "bSort": false,
        "Filter": true,
        "Processing": true,
        "serverSide": true,
        "pageLength": 50,
       
        "ajax": {
            data: {list: 'list'},
            url: url+"User_app",
            type: "post",
            error: function () {
              console.log('its error')
            }
        },
       "language": {
            "sEmptyTable": "<i>No Data Found.</i>",
            "search": '<i class="fa fa-search"></i>',
            "searchPlaceholder": "search by order name /order ID",
        },
        "aoColumnDefs": [
                    { 'bSortable': false, 'aTargets': [] }
                ]
    });
     $('.dataTables_filter').remove();
  filterFunction(dataTable);
}

if (page_title == "Manufacturing Accounting Rejected Orders") {
  var dataTable= $('#manufacturing_rejected_product').DataTable({
        "bLengthChange": false,
        "bFilter" : true,
        "bSort": false,
        "Filter": true,
        "Processing": true,
        "serverSide": true,
        "pageLength": 50,
       
        "ajax": {
            data: {list: 'list'},
            url: url+"Mnfg_accounting_rejected_orders/view/"+order_id,
            type: "post",
            error: function () {
              console.log('its error')
            }
        },
       "language": {
            "sEmptyTable": "<i>No Data Found.</i>",
            "search": '<i class="fa fa-search"></i>',
            "searchPlaceholder": "Search By Order ID",
        },
        "aoColumnDefs": [
                    { 'bSortable': false, 'aTargets': [] }
                ]
    });
  filterFunction(dataTable);
}
if (page_title == "Tagged Products") {
  var dataTable= $('#tag_products').DataTable({
        "bLengthChange": false,
        "bFilter" : true,
        "bSort": false,
        "Filter": true,
        "Processing": true,
        "serverSide": true,
        "pageLength": 50,
       
        "ajax": {
            data: {list: 'list'},
            url: url+"tag_products/create",
            type: "post",
            error: function () {
              console.log('its error')
            }
        },
       "language": {
            "sEmptyTable": "<i>No Data Found.</i>",
            "search": '<i class="fa fa-search"></i>',
            "searchPlaceholder": "Search By Product Code",
        },
        "aoColumnDefs": [
                    { 'bSortable': false, 'aTargets': [] }
                ]
    });
   $('.dataTables_filter').remove();
  filterFunction(dataTable);
}
if (page_title == "Customer Received Order") {
  var dataTable= $('#customer_received_order').DataTable({
        "bLengthChange": false,
        "bFilter" : true,
        "bSort": false,
        "Filter": true,
        "Processing": true,
        "serverSide": true,
        "pageLength": 50,
       
        "ajax": {
            data: {list: 'list'},
            url: url+"Customer_order/received_order/index?status="+get,
            type: "post",
            error: function () {
              console.log('its error')
            }
        },
       "language": {
            "sEmptyTable": "<i>No Data Found.</i>",
            "search": '<i class="fa fa-search"></i>',
            "searchPlaceholder": "Order Number / Party / karigar",
        },
        "aoColumnDefs": [
                    { 'bSortable': false, 'aTargets': [] }
                ]
    });
    $('.dataTables_filter').remove();
  filterFunction(dataTable);


}

if (page_title == "All Sub Category") {
  var dataTable= $('#product_category_rate').DataTable({
        "bLengthChange": false,
        "bFilter" : true,
        "bSort": false,
        "Filter": true,
        "Processing": true,
        "serverSide": true,
        "ajax": {
            data: {list: 'list'},
            url: url + "product_category_rate/index",
            type: "post",
            error: function () {
              console.log('its error')
            }
        },
       "language": {
            "sEmptyTable": "<i>No Data Found.</i>",
            "search": '<i class="fa fa-search"></i>',
            "searchPlaceholder": "Search Category"
        },
        "aoColumnDefs": [
                    { 'bSortable': false, 'aTargets': [] }
                ]
    });
      $('.dataTables_filter').remove();
  filterFunction(dataTable);
}
if (page_title == "Products Pending in Accounting") {
  var dataTable= $('#Acconting_receive_orders').DataTable({
        "bLengthChange": false,
        "bFilter" : true,
        "bSort": false,
        "Filter": true,
        "Processing": true,
        "serverSide": true,
        "ajax": {
            data: {list: 'list'},
            url: url + "accounting_department/index",
            type: "post",
            error: function () {
              console.log('its error')
            }
        },
       "language": {
            "sEmptyTable": "<i>No Data Found.</i>",
            "search": '<i class="fa fa-search"></i>',
            "searchPlaceholder": "Search Category"
        },
        "aoColumnDefs": [
                    { 'bSortable': false, 'aTargets': [] }
                ]
    });
  filterFunction(dataTable);
}
if (page_title == "Products Pending in Order") {
  var dataTable= $('#Acconting_receive_orders_view').DataTable({
        "bLengthChange": false,
        "bFilter" : true,
        "bSort": false,
        "Filter": true,
        "Processing": true,
        "serverSide": true,
        "ajax": {
            data: {list: 'list'},
            url: url + "accounting_department/view/"+order_id,
            type: "post",
            error: function () {
              console.log('its error')
            }
        },
       "language": {
            "sEmptyTable": "<i>No Data Found.</i>",
            "search": '<i class="fa fa-search"></i>',
            "searchPlaceholder": "Search Category"
        },
        "aoColumnDefs": [
                    { 'bSortable': false, 'aTargets': [] }
                ]
    });
  filterFunction(dataTable);
}
if (page_title == "Karigars Engaged") {
  var dataTable= $('#karigar_engaged').DataTable({
        "bLengthChange": false,
        "bFilter" : true,
        "bSort": false,
        "Filter": true,
        "Processing": true,
        "serverSide": true,
        "ajax": {
            data: {list: 'list'},
            url: url + "Customer_order/Karigar_engaged/index",
            type: "post",
            error: function () {
              console.log('its error')
            }
        },
       "language": {
            "sEmptyTable": "<i>No Data Found.</i>",
            "search": '<i class="fa fa-search"></i>',
            "searchPlaceholder": "Karigar Name"
        },
        "aoColumnDefs": [
                    { 'bSortable': false, 'aTargets': [] }
                ]
    });
     $('.dataTables_filter').remove();
  filterFunction(dataTable);
}
if (page_title == "Karigar Orders") {
  var dataTable= $('#karigar_engaged_orders').DataTable({
        "bLengthChange": false,
        "bFilter" : true,
        "bSort": false,
        "Filter": true,
        "Processing": true,
        "serverSide": true,
        "ajax": {
            data: {list: 'list'},
            url: url + "Customer_order/Karigar_engaged/view/"+karigar_id,
            type: "post",
            error: function () {
              console.log('its error')
            }
        },
       "language": {
            "sEmptyTable": "<i>No Data Found.</i>",
            "search": '<i class="fa fa-search"></i>',
            "searchPlaceholder": "Order Name/ Customer Name"
        },
        "aoColumnDefs": [
                    { 'bSortable': false, 'aTargets': [] }
                ]
    });
  $('.dataTables_filter').remove();
  filterFunction(dataTable);
}

if (page_title == "Orders Report") {

  var customer_id = $('#individual_customer_order_tracking_tbl').attr('value'); 
  var karagir_id = $('#individual_karigar_order_tracking_tbl').attr('value'); 
 // $("#Karigar_order_tracking_tbl").append('<tfoot><td id="tfoot" colspan="2" style="text-align:right">Total:</td><td></td><td></td></tfoot>');
  var dataTable= $('#Karigar_order_tracking_tbl').DataTable({
        "bLengthChange": false,
        "bFilter" : true,
        "bSort": false,
        "Filter": true,
        "Processing": true,
        "serverSide": true,
        "pageLength": 50,
       
        
        "ajax": {
            data: {list: 'list'},
            url: url + "Order_report/order_tracking_report/"+order_reminder,
            type: "post",
            error: function () {
              console.log('its error')
            }
        },
       "language": {
            "sEmptyTable": "<i>No Data Found.</i>",
            "search": '<i class="fa fa-search"></i>',
            "searchPlaceholder": "Order Name/ Customer Name"
        },
        "aoColumnDefs": [
                    { 'bSortable': false, 'aTargets': [] }
                ],
    // "footerCallback": function ( row, data, start, end, display ) {
    //         var api = this.api(), data;
             

    //          var intVal = function ( i ) {
    //             return typeof i === 'string' ?
    //                 i.replace(/<\/?SPAN[^>]*>/gi, ""):
    //                 typeof i === 'number' ?
    //                     i : 0;
    //         };
            
 
    //         // Total over all pages
    //         total = api
    //             .column( 2 )
    //             .data()
    //             .reduce( function (a, b) {
                   
    //                 return Number(intVal(a)) + Number(intVal(b));
    //             }, 0 );
            
    //         // Total over this page
    //         pageTotal = api
    //             .column( 2, { page: 'current'} )
    //             .data()
    //             .reduce( function (a, b) {

    //                 return Number(intVal(a)) + Number(intVal(b));
    //             }, 0 );
 
    //      //   Update footer
    //         $( api.column( 2 ).footer() ).html(total);
        
                    

                    
    //                                     //$(this.footer()).html(sum);
                  
    //             },


    });

    var dataTable= $('#customer_order_tracking_tbl').DataTable({

        "bLengthChange": false,
        "bFilter" : true,
        "bSort": false,
        "Filter": true,
        "Processing": true,
        "serverSide": true,
          "pageLength": 50,
       
        "ajax": {
            data: {list: 'list'},
            url: url + "Order_report/order_tracking_report/"+order_reminder,
            type: "post",
            error: function () {
              console.log('its error')
            }
        },
       "language": {
            "sEmptyTable": "<i>No Data Found.</i>",
            "search": '<i class="fa fa-search"></i>',
            "searchPlaceholder": "Order Name/ Customer Name"
        },
        "aoColumnDefs": [
                    { 'bSortable': false, 'aTargets': [] }
                ]
    });


    var dataTable= $('#delivery_performance_karigar_tbl').DataTable({

        "bLengthChange": false,
        "bFilter" : true,
        "bSort": false,
        "Filter": true,
        "Processing": true,
        "serverSide": true,
          "pageLength": 50,
       
        "ajax": {
            data: {list: 'list'},
            url: url + "Order_report/delivery_performance/"+order_reminder,
            type: "post",
            error: function () {
              console.log('its error')
            }
        },
       "language": {
            "sEmptyTable": "<i>No Data Found.</i>",
            "search": '<i class="fa fa-search"></i>',
            "searchPlaceholder": "Order Name/ Customer Name"
        },
        "aoColumnDefs": [
                    { 'bSortable': false, 'aTargets': [] }
                ]

     });  
    var dataTable= $('#delivery_performance_party_tbl').DataTable({

        "bLengthChange": false,
        "bFilter" : true,
        "bSort": false,
        "Filter": true,
        "Processing": true,
        "serverSide": true,
          "pageLength": 50,
       
        "ajax": {
            data: {list: 'list'},
            url: url + "Order_report/delivery_performance/"+order_reminder,
            type: "post",
            error: function () {
              console.log('its error')
            }
        },
       "language": {
            "sEmptyTable": "<i>No Data Found.</i>",
            "search": '<i class="fa fa-search"></i>',
            "searchPlaceholder": "Order Name/ Customer Name"
        },
        "aoColumnDefs": [
                    { 'bSortable': false, 'aTargets': [] }
                ]
    });
    
    var dataTable= $('#in_stock_report_tbl').DataTable({

        "bLengthChange": false,
        "bFilter" : true,
        "bSort": false,
        "Filter": true,
        "Processing": true,
        "serverSide": true,
          "pageLength": 50,
       
        "ajax": {
            data: {list: 'list'},
            url: url + "Order_report/order_tracking_report/"+order_reminder,
            type: "post",
            error: function () {
              console.log('its error')
            }
        },
       "language": {
            "sEmptyTable": "<i>No Data Found.</i>",
            "search": '<i class="fa fa-search"></i>',
            "searchPlaceholder": "Order Name/ Customer Name"
        },
        "aoColumnDefs": [
                    { 'bSortable': false, 'aTargets': [] }
                ]
    });

    var dataTable= $('#individual_customer_order_tracking_tbl').DataTable({
        "bLengthChange": false,
        "bFilter" : true,
        "bSort": false,
        "Filter": true,
        "Processing": true,
        "serverSide": true,
          "pageLength": 50,
       
        "ajax": {
            data: {list: 'list'},
            url: url + "Order_report/customer_order_report/"+customer_id+"/"+order_reminder,
            type: "post",
            error: function () {
              console.log('its error')
            }
        },
       "language": {
            "sEmptyTable": "<i>No Data Found.</i>",
            "search": '<i class="fa fa-search"></i>',
            "searchPlaceholder": "Order Name/ Karigar Name"
        },
        "aoColumnDefs": [
                    { 'bSortable': false, 'aTargets': [] }
                ]
    });


    var dataTable= $('#individual_karigar_order_tracking_tbl').DataTable({
        "bLengthChange": false,
        "bFilter" : true,
        "bSort": false,
        "Filter": true,
        "Processing": true,
        "serverSide": true,
          "pageLength": 50,
       
        "ajax": {
            data: {list: 'list'},
            url: url + "Order_report/karigar_order_report/"+karagir_id+"/"+order_reminder,
            type: "post",
            error: function () {
              console.log('its error')
            }
        },
       "language": {
            "sEmptyTable": "<i>No Data Found.</i>",
            "search": '<i class="fa fa-search"></i>',
            "searchPlaceholder": "Order Name/ Customer Name"
        },
        "aoColumnDefs": [
                    { 'bSortable': false, 'aTargets': [] }
                ]
    });


    var dataTable= $('#daily_order_received_tbl').DataTable({
        "bLengthChange": false,
        "bFilter" : true,
        "bSort": false,
        "Filter": true,
        "Processing": true,
        "serverSide": true,
        "pageLength": 50,
       
        "ajax": {
            data: {list: 'list'},
            url: url + "Order_report/daily_order_received?today_date="+get_date,
            type:"post",
            error: function () {
              console.log('its error')
            }
        },
       "language": {
            "sEmptyTable": "<i>No Data Found.</i>",
            "search": '<i class="fa fa-search"></i>',
            "searchPlaceholder": "Order Name/ Customer Name"
        },
        "aoColumnDefs": [
                    { 'bSortable': false, 'aTargets': [] }
                ]
    }); 

   var dataTable= $('#daily_order_received_from_karigar_tbl').DataTable({
        "bLengthChange": false,
        "bFilter" : true,
        "bSort": false,
        "Filter": true,
        "Processing": true,
        "serverSide": true,
          "pageLength": 50,
       
        "ajax": {
            data: {list: 'list'},
            url: url + "Order_report/daily_order_received_from_karigar?today_date="+get_date,
            type: "post",
            error: function () {
              console.log('its error')
            }
        },
       "language": {
            "sEmptyTable": "<i>No Data Found.</i>",
            "search": '<i class="fa fa-search"></i>',
            "searchPlaceholder": "Order Name/ Customer Name"
        },
        "aoColumnDefs": [
                    { 'bSortable': false, 'aTargets': [] }
                ]
    });  

   var dataTable= $('#delivery_report_in_qty_tbl').DataTable({
        "bLengthChange": false,
        "bFilter" : true,
        "bSort": false,
        "Filter": true,
        "Processing": true,
        "serverSide": true,
          "pageLength": 50,
       
        "ajax": {
            data: {list: 'list'},
            url: url + "Order_report/delivery_report_in_quantity?from="+from_date+"&to="+to_date+"&type="+type_wise,
            type: "post",
            error: function () {
              console.log('its error')
            }
        },
       "language": {
            "sEmptyTable": "<i>No Data Found.</i>",
            "search": '<i class="fa fa-search"></i>',
            "searchPlaceholder": "Order Name/ Customer Name"
        },
        "aoColumnDefs": [
                    { 'bSortable': false, 'aTargets': [] }
                ]
    });

   var dataTable= $('#client_order_ledger').DataTable({
        "bLengthChange": false,
        "bFilter" : true,
        "bSort": false,
        "Filter": true,
        "Processing": true,
        "serverSide": true,
          "pageLength": 50,
       
        "ajax": {
            data: {list: 'list'},
            url: url + "Order_report/order_ledger/"+order_reminder+"?from="+from_date+"&to="+to_date+"",
            type: "post",
            error: function () {
              console.log('its error')
            }
        },
       "language": {
            "sEmptyTable": "<i>No Data Found.</i>",
            "search": '<i class="fa fa-search"></i>',
            "searchPlaceholder": "Order Name/ Customer Name"
        },
        "aoColumnDefs": [
                    { 'bSortable': false, 'aTargets': [] }
                ]
    });

var dataTable= $('#karigar_order_ledger').DataTable({
        "bLengthChange": false,
        "bFilter" : true,
        "bSort": false,
        "Filter": true,
        "Processing": true,
        "serverSide": true,
          "pageLength": 50,
       
        "ajax": {
            data: {list: 'list'},
            url: url + "Order_report/order_ledger/"+order_reminder+"?from="+from_date+"&to="+to_date+"",
            type: "post",
            error: function () {
              console.log('its error')
            }
        },
       "language": {
            "sEmptyTable": "<i>No Data Found.</i>",
            "search": '<i class="fa fa-search"></i>',
            "searchPlaceholder": "Order Name/ Customer Name"
        },
        "aoColumnDefs": [
                    { 'bSortable': false, 'aTargets': [] }
                ]
    });
   
   var dataTable= $('#order_ledger_by_customer').DataTable({
        "bLengthChange": false,
        "bFilter" : true,
        "bSort": false,
        "Filter": true,
        "Processing": true,
        "serverSide": true,
          "pageLength": 50,
       
        "ajax": {
            data: {list: 'list'},
            url: url + "Order_report/order_ledger_by_customer/"+order_reminder+"?from="+from_date+"&to="+to_date+"",
            type: "post",
            error: function () {
              console.log('its error')
            }
        },
       "language": {
            "sEmptyTable": "<i>No Data Found.</i>",
            "search": '<i class="fa fa-search"></i>',
            "searchPlaceholder": "Order Name/ Customer Name"
        },
        "aoColumnDefs": [
                    { 'bSortable': false, 'aTargets': [] }
                ]
    });
   
   var dataTable= $('#order_ledger_by_karigar').DataTable({
        "bLengthChange": false,
        "bFilter" : true,
        "bSort": false,
        "Filter": true,
        "Processing": true,
        "serverSide": true,
          "pageLength": 50,
       
        "ajax": {
            data: {list: 'list'},
            url: url + "Order_report/order_ledger_by_karigar/"+order_reminder+"?from="+from_date+"&to="+to_date+"",
            type: "post",
            error: function () {
              console.log('its error')
            }
        },
       "language": {
            "sEmptyTable": "<i>No Data Found.</i>",
            "search": '<i class="fa fa-search"></i>',
            "searchPlaceholder": "Order Name/ Customer Name"
        },
        "aoColumnDefs": [
                    { 'bSortable': false, 'aTargets': [] }
                ]
    });

   
   var dataTable= $('#report_summary_departmentWise_tbl').DataTable({
        "bLengthChange": false,
        "bFilter" : true,
        "bSort": false,
        "Filter": true,
        "Processing": true,
        "serverSide": true,
          "pageLength": 50,
       
        "ajax": {
            data: {list: 'list'},
            url: url + "Order_report/daily_report_summary?today_date="+get_date,
            type: "post",
            error: function () {
              console.log('its error')
            }
        },
       "language": {
            "sEmptyTable": "<i>No Data Found.</i>",
            "search": '<i class="fa fa-search"></i>',
            "searchPlaceholder": "Order Name/ Customer Name"
        },
        "aoColumnDefs": [
                    { 'bSortable': false, 'aTargets': [] }
                ]
    });

     var dataTable= $('#sales_purchase_report').DataTable({
        "bLengthChange": false,
        "bFilter" : true,
        "bSort": false,
        "Filter": true,
        "Processing": true,
        "serverSide": true,
         "pageLength": 50,
       
        "ajax": {
            data: {list: 'list'},
            url: url + "Order_report/sales_purchase_report?from="+from_date+"&to="+to_date,
             //url: url + "Manufacture_report/Daily_stock_summary_report",
            type: "post",
            error: function () {
              console.log('its error')
            }
        },
       "language": {
            "sEmptyTable": "<i>No Data Found.</i>",
            "search": '<i class="fa fa-search"></i>',
            "searchPlaceholder": "Search By Order Name"
        },
        "aoColumnDefs": [
                    { 'bSortable': false, 'aTargets': [] }
                ]
    });

  $('.dataTables_filter').remove();
  filterFunction(dataTable);
}

if (page_title == "Reminder Orders") {

     if(order_reminder=='1'){
        var searchPlaceholder='Order Name / Karigar';
      }else{
        var searchPlaceholder='Order Name/  Customer';
      }
  var dataTable= $('#reminder_tbl').DataTable({
        "bLengthChange": false,
        "bFilter" : true,
        "bSort": false,
        "Filter": true,
        "Processing": true,
        "serverSide": true,
        "pageLength": 50,
       
        "ajax": {
            data: {list: 'list'},
            url: url + "Customer_order/Reminder/index/"+order_reminder+'?filter='+order_reminder_filter,
            type: "post",
            error: function () {
              console.log('its error')
            }
        },
       "language": {
            "sEmptyTable": "<i>No Data Found.</i>",
            "search": '<i class="fa fa-search"></i>',
            "searchPlaceholder": searchPlaceholder
        },
        "aoColumnDefs": [
                    { 'bSortable': false, 'aTargets': [] }
                ]
    });
  filterFunction(dataTable);


    var dataTable= $('#Karigar_remainder_tbl').DataTable({
        "bLengthChange": false,
        "bFilter" : true,
        "bSort": false,
        "Filter": true,
        "Processing": true,
        "serverSide": true,
        "ajax": {
            data: {list: 'list'},
            url: url + "Customer_order/Reminder/index/"+order_reminder+'?filter='+order_reminder_filter,
            type: "post",
            error: function () {
              console.log('its error')
            }
        },
       "language": {
            "sEmptyTable": "<i>No Data Found.</i>",
            "search": '<i class="fa fa-search"></i>',
            "searchPlaceholder": searchPlaceholder
        },
        "aoColumnDefs": [
                    { 'bSortable': false, 'aTargets': [] }
                ]
    });
     $('.dataTables_filter').remove();
  filterFunction(dataTable);

      var dataTable= $('#customer_remainder_tbl').DataTable({
        "bLengthChange": false,
        "bFilter" : true,
        "bSort": false,
        "Filter": true,
        "Processing": true,
        "serverSide": true,
        "ajax": {
            data: {list: 'list'},
            url: url + "Customer_order/Reminder/index/"+order_reminder+'?filter='+order_reminder_filter,
            type: "post",
            error: function () {
              console.log('its error')
            }
        },
       "language": {
            "sEmptyTable": "<i>No Data Found.</i>",
            "search": '<i class="fa fa-search"></i>',
            "searchPlaceholder": searchPlaceholder
        },
        "aoColumnDefs": [
                    { 'bSortable': false, 'aTargets': [] }
                ]
    });
     $('.dataTables_filter').remove();
  filterFunction(dataTable);
}

if (page_title == "Email Tracking") {
      if(order_reminder=='1'){

        var searchPlaceholder=' Order Name/ Karigar';
      }else{
        var searchPlaceholder=' Order Name/ Customer';

       }
  var dataTable= $('#tracking_tbl').DataTable({
        "bLengthChange": false,
        "bFilter" : true,
        "bSort": false,
        "Filter": true,
        "Processing": true,
        "serverSide": true,
        "ajax": {
            data: {list: 'list'},
            url: url + "Customer_order/Email_tracking/index/"+order_reminder+'?filter='+order_reminder_filter,
            type: "post",
            error: function () {
              console.log('its error')
            }
        },
       "language": {
            "sEmptyTable": "<i>No Data Found.</i>",
            "search": '<i class="fa fa-search"></i>',
            "searchPlaceholder": searchPlaceholder
        },
        "aoColumnDefs": [
                    { 'bSortable': false, 'aTargets': [] }
                ]
    });
  filterFunction(dataTable);

   var dataTable= $('#karigar_email_tracking_tbl').DataTable({
        "bLengthChange": false,
        "bFilter" : true,
        "bSort": false,
        "Filter": true,
        "Processing": true,
        "serverSide": true,
        "pageLength":50,
        "ajax": {
            data: {list: 'list'},
            url: url + "Customer_order/Email_tracking/index/"+order_reminder+'?filter='+order_reminder_filter,
            type: "post",
            error: function () {
              console.log('its error')
            }
        },
       "language": {
            "sEmptyTable": "<i>No Data Found.</i>",
            "search": '<i class="fa fa-search"></i>',
            "searchPlaceholder": searchPlaceholder
        },
        "aoColumnDefs": [
                    { 'bSortable': false, 'aTargets': [] }
                ]
    });
     $('.dataTables_filter').remove();
  filterFunction(dataTable);

   var dataTable= $('#customer_email_tracking_tbl').DataTable({
        "bLengthChange": false,
        "bFilter" : true,
        "bSort": false,
        "Filter": true,
        "Processing": true,
        "serverSide": true,
        "pageLength":50,
        "ajax": {
            data: {list: 'list'},
            url: url + "Customer_order/Email_tracking/index/"+order_reminder+'?filter='+order_reminder_filter,
            type: "post",
            error: function () {
              console.log('its error')
            }
        },
       "language": {
            "sEmptyTable": "<i>No Data Found.</i>",
            "search": '<i class="fa fa-search"></i>',
            "searchPlaceholder": searchPlaceholder
        },
        "aoColumnDefs": [
                    { 'bSortable': false, 'aTargets': [] }
                ]
    });
   $('.dataTables_filter').remove();
  filterFunction(dataTable);
}
if (page_title == "All Amended Products") {
  var dataTable= $('#karigar_product_amended_list').DataTable({
        "bLengthChange": false,
        "bFilter" : true,
        "bSort": false,
        "Filter": true,
        "Processing": true,
        "serverSide": true,
        "ajax": {
            data: {list: 'list'},
            url: url + "Amend_products",
            type: "post",
            error: function () {
              console.log('its error')
            }
        },
       "language": {
            "sEmptyTable": "<i>No Data Found.</i>",
            "search": '<i class="fa fa-search"></i>',
            "searchPlaceholder": "Search Karigar Name"
        },
        "aoColumnDefs": [
                    { 'bSortable': false, 'aTargets': [] }
                ]
    });
  filterFunction(dataTable);
}

if (page_title == "All Difference Products") {
  var dataTable= $('#difference_product_table').DataTable({
        "bLengthChange": false,
        "bFilter" : true,
        "bSort": false,
        "Filter": true,
        "Processing": true,
        "serverSide": true,
        "ajax": {
            data: {list: 'list'},
            url: url + "difference_product",
            type: "post",
            error: function () {
              console.log('its error')
            }
        },
       "language": {
            "sEmptyTable": "<i>No Data Found.</i>",
            "search": '<i class="fa fa-search"></i>',
            "searchPlaceholder": "Search Karigar Name"
        },
        "aoColumnDefs": [
                    { 'bSortable': false, 'aTargets': [] }
                ]
    });
  $('.dataTables_filter').remove();
  filterFunction(dataTable);
}

if( page_title == "CORPORATE ORDER LIST")
{
    var dataTable= $('#corporate_engaged_order_list').DataTable({
        "bLengthChange": false,
        "bFilter" : true,
        "bSort": false,
        "Filter": true,
        "Processing": true,
        "serverSide": true,
         "pageLength": 50,
       
        "ajax": {
            data: {list: 'list'},
            url: url + "Corporate_order_list",
            type: "post",
            error: function () {
              console.log('its error')
            }
        },
       "language": {
            "sEmptyTable": "<i>No Data Found.</i>",
            "search": '<i class="fa fa-search"></i>',
            "searchPlaceholder": "Search Karigar Name"
        },
        "aoColumnDefs": [
                    { 'bSortable': false, 'aTargets': [] }
                ]

    });
    $('.dataTables_filter').remove();
    filterFunction(dataTable);
}

    if( page_title == "CORPORATE ORDER DETAIL")
{
    //alert(order_id_status);
    var dataTable= $('#corporate_engaged_order_detail_list').DataTable({
        "bLengthChange": false,
        "bFilter" : true,
        "bSort": false,
        "Filter": true,
        "Processing": true,
        "serverSide": true,
        "pageLength": 50,       
        "ajax": {
            data: {list: 'list'},
            url: url + "Corporate_order_list/detail/"+order_id+"?order_id_status="+order_id_status,
            type: "post",
            error: function () {
              console.log('its error')
            }
        },
       "language": {
            "sEmptyTable": "<i>No Data Found.</i>",
            "search": '<i class="fa fa-search"></i>',
            "searchPlaceholder": "Search Karigar Name"
        },
        "aoColumnDefs": [
                    { 'bSortable': false, 'aTargets': [] }
                ]

    });
  $('.dataTables_filter').remove();
  filterFunction(dataTable);
}

if( page_title == "CORPORATE ORDER DETAIL")
{
    
    var dataTable= $('#Corporate_order_Details_list').DataTable({
        "bLengthChange": false,
        "bFilter" : true,
        "bSort": false,
        "Filter": true,
        "Processing": true,
        "serverSide": true,
        "ajax": {
            data: {list: 'list'},
            url: url + "Corporate_order_list/show/"+karigar_id+"?order_id="+order_id,
            type: "post",
            error: function () {
              console.log('its error')
            }
        },
       "language": {
            "sEmptyTable": "<i>No Data Found.</i>",
            "search": '<i class="fa fa-search"></i>',
            "searchPlaceholder": "Search Karigar Name"
        },
        "aoColumnDefs": [
                    { 'bSortable': false, 'aTargets': [] }
                ]

    });
    $('.dataTables_filter').remove();
    filterFunction(dataTable);
    
}



/*by default 50*/
if (dataTable) {

  dataTable.page.len(50).draw();

       //$('.dataTables_filter').remove();
}
