/*
Template Name: Admin Pro Admin
Author: Wrappixel
Email: niravjoshi87@gmail.com
File: js
*/
$(function () {
    "use strict";
    // ============================================================== 
    // Order overview
    // ============================================================== 
    console.log(cart_label);
    if (cart_label) {
         new Chartist.Line('#carts_line_chart', { labels:  JSON.parse(cart_label)
            , series: [
              {meta:"Order", data:  JSON.parse(cart_data)}
          ]
        }, {
            low: 0
            , high:chart_max
            , showArea: true
            , divisor: 10
            , lineSmooth:false
            , fullWidth: true
            , showLine: true
            , chartPadding: 30
            , axisX: {
                showLabel: true
                , showGrid: false
                , offset: 50
            }
            , plugins: [
            	Chartist.plugins.tooltip()
          	], 
          	// As this is axis specific we need to tell Chartist to use whole numbers only on the concerned axis
            axisY: {
            	onlyInteger: true
                , showLabel: true
                , scaleMinSpace: 50 
                , showGrid: true
                , offset: 10,
                labelInterpolationFnc: function(value) {
    		      //return (value / 100) + 'k'
              return value
    		    },

            }
            
        });

    }

 });
