function searchpopup(urls,key,function_name,search_url){
  var urlParams = new URLSearchParams(window.location.search);
  if($("#searchModal").text()==""){
      var postData = {key:key,function_name:function_name,search_url:search_url};
  }
  else{
    if(urlParams.has('ordered_columns') && urlParams.get('ordered_columns') == 1){
      var postData = $('form#searchFrom').serialize()+"&key="+key+"&function_name="+function_name+"&search_url="+search_url+"&ordered_columns=1";
    }
    else if(urlParams.has('selected_column') && urlParams.get('selected_column') == 1){
      var postData = $('form#searchFrom').serialize()+"&key="+key+"&function_name="+function_name+"&search_url="+search_url+"&ordered_columns=0&select_column=1";
    }
    else{
      var postData = $('form#searchFrom').serialize()+"&key="+key+"&function_name="+function_name+"&search_url="+search_url;
    }
    $("#searchModal").remove();
    //$("#searchModal").hide();
  }
     $.ajax({
        url : urls+"search_popup",
        type: "POST",
        data : postData,
        success: function(data, textStatus, jqXHR)
        {
      $('body').append(data);
            $('#searchModal').modal('show');
            //$('#searchModal').show();
        },
        error: function (jqXHR, textStatus, errorThrown)
        {

        }
    });
}

function filterTableData(urls){
  var url = window.location.href;
  var order_column ="";// getParameterByName('order_column',url);
  var order_by = "";//getParameterByName('order_by',url);
  var new_url = $('form#searchFrom').serialize();
  if(order_column!="")
    new_url=new_url+"&order_column="+order_column+"&order_by="+order_by;
  
  
  window.location.href=urls+"?"+new_url;
}

$(document).on('click','.removeInput', function(){
  $(this).parents('.divInput').remove();
});

