var is_type = '';
var page_count = 0;
var object = {};
var load_more = 0;
var page_reload = true;
var total_wt = 0;
var total_wt_d = 0;
var total_qtn = 0;
var sel_karigar = '';

if (page_title == 'PREPARE ORDER') {
    $(window).scroll(function () {
        if ($(window).scrollTop() > $(document).height() - 1200) {
            load_more += 1;
            if (load_more == 1)
                // pagination(page_count, object)
                return false;
        }
    });
}

function showMsgPopup(title, text, success_url, type)
{
    var shortCutFunction = "success";
    var timeColse = 10000;
    if (type == "error")
        var shortCutFunction = "error";
    if (success_url == "")
        timeColse = 2000;

    var msg = "<p>" + text + '</p>';
    var title = title;
    var $showDuration = 300;
    var toastIndex = 0;

    toastr.options = {
        "closeButton": false,
        "debug": false,
        "newestOnTop": false,
        "progressBar": false,
        "positionClass": "toast-top-right",
        "preventDuplicates": false,
        "onclick": null,
        "showDuration": "300",
        "hideDuration": "1000",
        "timeOut": timeColse,
        "extendedTimeOut": 0,
        "showEasing": "swing",
        "hideEasing": "linear",
        "showMethod": "fadeIn",
        "hideMethod": "fadeOut",
        "tapToDismiss": false
    };
    var $toast = toastr[shortCutFunction](msg, title); // Wire up an event handler to a button in the toast, if it exists
    if (success_url != '') {
      //$(".ajaxLoader").fadeIn();
        setTimeout(function () {
            close_toast_popup(success_url);
        }, 1500);
    }
}
function error_msg(data)
{
    $(".text-danger").each(function (index) {
        $(this).html("");
    });
    var i = 0;
    for (var key in data)
    {
        $("#" + key + "_error").html(data[key]);
        if (i == 0)
        {
            $('html, body').animate({
                scrollTop: $("#" + key + "_error").offset().top - ($(".nav").height() + 100)
            }, 1200);
        }
        i++;
    }
}
function excel_error_msg(data)
{
    $(".text-danger").each(function (index) {
        $(this).html("");
    });
    var i = 0;
    for (var key in data)
    {
        if ($.isNumeric(key)) {
            $(".errors").append('<p class="text-danger">' + data[key] + '</p');
            if (i == 0)
            {
                $('html, body').animate({
                    scrollTop: $(".errors").offset().top - ($(".nav").height() + 100)
                }, 1200);
            }
        } else {
            $("#" + key + "_error").html(data[key]);
            if (i == 0)
            {
                $('html, body').animate({
                    scrollTop: $("#" + key + "_error").offset().top - ($(".nav").height() + 100)
                }, 1200);
            }
        }
        i++;
    }
}

function close_toast_popup(success_url)
{
    window.location.href = success_url;
}
function login(urls) {
    $.ajax({
        url: urls + "login/auth",
        type: "POST",
        dataType: "json",
        data: $('form#loginform').serialize(),
        success: function (data, textStatus, jqXHR)
        {
            if (data.status == 'failure')
            {
                error_msg(data.error);
            } else
            {
                if (data.status == 'error')
                    $("#invalid_error").html('<b class="text-danger">' + data.data + '</b>');
                else {
                    window.location.href = urls + 'Collection';
                }
            }
        },
        error: function (jqXHR, textStatus, errorThrown)
        {

        }
    });
}

function save_category() {
    $.ajax({
        url: url + "Category/Store",
        type: "POST",
        dataType: "json",
        data: $('form#category').serialize(),
        success: function (data, textStatus, jqXHR)
        {
            if (data.status == 'failure')
            {
                error_msg(data.error);

            } else if (data.status == "success") {
                showMsgPopup("Success", "Successfully Added", '', 'normal')
                setTimeout(function () {
                    close_toast_popup(url + 'Category')

                }, 2000)

            } else {

                showMsgPopup("Error", "Something Went Wrong Try Again", '', 'error')
                setTimeout(function () {
                    close_toast_popup(url + 'Add_Cars/brand')

                }, 2000)
            }
        },
        error: function (jqXHR, textStatus, errorThrown)
        {

        }
    });
}

function save_product() {
    var formData = new FormData($('#product')[0]);
    formData.append('file', $('#image')[0].files[0]);
    $.ajax({
        url: url + "Design/store",
        type: "POST",
        dataType: "json",
        data: formData,
        processData: false,
        contentType: false,
        success: function (data, textStatus, jqXHR)
        {
            //console.log(data);
            if (data.status == 'failure')
            {
                error_msg(data.error);

            } else if (data.status == "success") {
                showMsgPopup("Success", "Successfully Saved", '', 'normal')
                setTimeout(function () {
                    close_toast_popup(document.referrer)

                }, 2000)

            } else {

                showMsgPopup("Error", "Something Went Wrong Try Again", '', 'error')
                setTimeout(function () {
                    close_toast_popup(document.referrer)

                }, 2000)
            }
        },
        error: function (jqXHR, textStatus, errorThrown)
        {

        }
    });
}
function update_product() {
    var formData = new FormData($('#product')[0]);
    formData.append('file', $('#image')[0].files[0]);
    $.ajax({
        url: url + "Design/update",
        type: "POST",
        dataType: "json",
        data: formData,
        processData: false,
        contentType: false,
        success: function (data, textStatus, jqXHR)
        {
            if (data.status == 'failure')
            {
                error_msg(data.error);

            } else if (data.status == "success") {

                showMsgPopup("Success", "Successfully updated", '', 'normal')
                setTimeout(function () {
                    close_toast_popup(document.referrer)

                }, 2000)

            } else {

                showMsgPopup("Error", "Something Went Wrong Try Again", '', 'error')
                setTimeout(function () {
                    close_toast_popup(document.referrer)

                }, 2000)
            }
        },
        error: function (jqXHR, textStatus, errorThrown)
        {

        }
    });
}

function update_category() {
    $.ajax({
        url: url + "Category/update",
        type: "POST",
        dataType: "json",
        data: $('form#category').serialize(),
        success: function (data, textStatus, jqXHR)
        {
            if (data.status == 'failure')
            {
                error_msg(data.error);

            } else if (data.status == "success") {
                showMsgPopup("Success", "Successfully Updated", '', 'normal')
                setTimeout(function () {
                    close_toast_popup(url + 'Category')

                }, 2000)

            } else {

                showMsgPopup("Error", "Something Went Wrong Try Again", '', 'error')
                setTimeout(function () {
                    close_toast_popup(url + 'Category')

                }, 2000)
            }
        },
        error: function (jqXHR, textStatus, errorThrown)
        {

        }
    });
}
function save_sub_category() {
    $.ajax({
        url: url + "Sub_category/store",
        type: "POST",
        dataType: "json",
        data: $('form#Sub_category').serialize(),
        success: function (data, textStatus, jqXHR)
        {
            if (data.status == 'failure')
            {
                error_msg(data.error);

            } else if (data.status == "success") {
                showMsgPopup("Success", "Successfully Added", '', 'normal')
                setTimeout(function () {
                    close_toast_popup(url + 'Sub_category')

                }, 2000)

            } else {

                showMsgPopup("Error", "Something Went Wrong Try Again", '', 'error')
                setTimeout(function () {
                    close_toast_popup(url + 'Sub_category')

                }, 2000)
            }
        },
        error: function (jqXHR, textStatus, errorThrown)
        {

        }
    });
}
function update_sub_category() {
    $.ajax({
        url: url + "Sub_category/update",
        type: "POST",
        dataType: "json",
        data: $('form#Sub_category').serialize(),
        success: function (data, textStatus, jqXHR)
        {
            if (data.status == 'failure')
            {
                error_msg(data.error);

            } else if (data.status == "success") {
                showMsgPopup("Success", "Successfully Added", '', 'normal')
                setTimeout(function () {
                    close_toast_popup(url + 'Sub_category')

                }, 2000)

            } else {

                showMsgPopup("Error", "Something Went Wrong Try Again", '', 'error')
                setTimeout(function () {
                    close_toast_popup(url + 'Sub_category')

                }, 2000)
            }
        },
        error: function (jqXHR, textStatus, errorThrown)
        {

        }
    });
}
function update_sub_category_images() {
    //validation subcategory images

    if (!$("input[type='checkbox']").is(":checked"))
    {
        $("#sub_cat_img_error").show();
        return false;
    }

    $.ajax({
        url: url + "Sub_category/update_sub_cat_images",
        type: "POST",
        dataType: "json",
        data: $('form#Sub_category').serialize(),

        success: function (data, textStatus, jqXHR)
        {
            if (data.status == 'failure')
            {
                error_msg(data.error);

            } else if (data.status == "success") {
                showMsgPopup("Success", "Successfully Added", '', 'normal')
                setTimeout(function () {
                    close_toast_popup(url + 'Sub_category')

                }, 2000)

            } else {

                showMsgPopup("Error", "Something Went Wrong Try Again", '', 'error')
                setTimeout(function () {
                    close_toast_popup(url + 'Sub_category')

                }, 2000)
            }
        },
        error: function (jqXHR, textStatus, errorThrown)
        {

        }
    });
}

function save_karigar() {
    $.ajax({
        url: url + "Karigar/store",
        type: "POST",
        dataType: "json",
        data: $('form#karigar').serialize(),
        success: function (data, textStatus, jqXHR)
        {
            if (data.status == 'failure')
            {
                error_msg(data.error);

            } else if (data.status == "success") {
                showMsgPopup("Success", "Successfully Added", '', 'normal')
                setTimeout(function () {
                    close_toast_popup(url + 'Karigar')

                }, 2000)

            } else {

                showMsgPopup("Error", "Something Went Wrong Try Again", '', 'error')
                setTimeout(function () {
                    close_toast_popup(url + 'Karigar')

                }, 2000)
            }
        },
        error: function (jqXHR, textStatus, errorThrown)
        {

        }
    });
}
function save_department() {
    $.ajax({
        url: url + "Department/store",
        type: "POST",
        dataType: "json",
        data: $('form#department').serialize(),
        success: function (data, textStatus, jqXHR)
        {
            if (data.status == 'failure')
            {
                error_msg(data.error);

            } else if (data.status == "success") {
                showMsgPopup("Success", "Successfully Added", '', 'normal')
                setTimeout(function () {
                    close_toast_popup(url + 'Department')

                }, 2000)

            } else {

                showMsgPopup("Error", "Something Went Wrong Try Again", '', 'error')
                setTimeout(function () {
                    close_toast_popup(url + 'Department')

                }, 2000)
            }
        },
        error: function (jqXHR, textStatus, errorThrown)
        {

        }
    });
}
function update_karigar() {
    $.ajax({
        url: url + "Karigar/update",
        type: "POST",
        dataType: "json",
        data: $('form#Karigar').serialize(),
        success: function (data, textStatus, jqXHR)
        {
            if (data.status == 'failure')
            {
                error_msg(data.error);

            } else if (data.status == "success") {
                showMsgPopup("Success", "Successfully Added", '', 'normal')
                setTimeout(function () {
                    close_toast_popup(url + 'Karigar')

                }, 2000)

            } else {

                showMsgPopup("Error", "Something Went Wrong Try Again", '', 'error')
                setTimeout(function () {
                    close_toast_popup(url + 'Karigar')

                }, 2000)
            }
        },
        error: function (jqXHR, textStatus, errorThrown)
        {

        }
    });
}
function edit_department() {
    $.ajax({
        url: url + "Department/update",
        type: "POST",
        dataType: "json",
        data: $('form#department').serialize(),
        success: function (data, textStatus, jqXHR)
        {
            if (data.status == 'failure')
            {
                error_msg(data.error);

            } else if (data.status == "success") {
                showMsgPopup("Success", "Successfully Added", '', 'normal')
                setTimeout(function () {
                    close_toast_popup(url + 'Department')

                }, 2000)

            } else {

                showMsgPopup("Error", "Something Went Wrong Try Again", '', 'error')
                setTimeout(function () {
                    close_toast_popup(url + 'Department')

                }, 2000)
            }
        },
        error: function (jqXHR, textStatus, errorThrown)
        {

        }
    });
}
function save_weight_range() {
    $.ajax({
        url: url + "Weight_range/store",
        type: "POST",
        dataType: "json",
        data: $('form#Weight_range').serialize(),
        success: function (data, textStatus, jqXHR)
        {
            if (data.status == 'failure')
            {
                error_msg(data.error);

            } else if (data.status == "success") {
                showMsgPopup("Success", "Successfully Added", '', 'normal')
                setTimeout(function () {
                    close_toast_popup(url + 'Weight_range')

                }, 2000)

            } else {

                showMsgPopup("Error", "Something Went Wrong Try Again", '', 'error')
                setTimeout(function () {
                    close_toast_popup(url + 'Weight_range')

                }, 2000)
            }
        },
        error: function (jqXHR, textStatus, errorThrown)
        {

        }
    });
}
function Update_weight_range() {
    $.ajax({
        url: url + "Weight_range/update",
        type: "POST",
        dataType: "json",
        data: $('form#Weight_range').serialize(),
        success: function (data, textStatus, jqXHR)
        {
            if (data.status == 'failure')
            {
                error_msg(data.error);

            } else if (data.status == "success") {
                showMsgPopup("Success", "Successfully Added", '', 'normal')
                setTimeout(function () {
                    close_toast_popup(url + 'Weight_range')

                }, 2000)

            } else {

                showMsgPopup("Error", "Something Went Wrong Try Again", '', 'error')
                setTimeout(function () {
                    close_toast_popup(url + 'Weight_range')

                }, 2000)
            }
        },
        error: function (jqXHR, textStatus, errorThrown)
        {

        }
    });
}
function save_article() {
    $.ajax({
        url: url + "Article/store",
        type: "POST",
        dataType: "json",
        data: $('form#Article').serialize(),
        success: function (data, textStatus, jqXHR)
        {
            if (data.status == 'failure')
            {
                error_msg(data.error);

            } else if (data.status == "success") {
                showMsgPopup("Success", "Successfully Added", '', 'normal')
                setTimeout(function () {
                    close_toast_popup(url + 'Article')

                }, 2000)

            } else {

                showMsgPopup("Error", "Something Went Wrong Try Again", '', 'error')
                setTimeout(function () {
                    close_toast_popup(url + 'Article')

                }, 2000)
            }
        },
        error: function (jqXHR, textStatus, errorThrown)
        {

        }
    });
}

function update_article() {
    $.ajax({
        url: url + "Article/update",
        type: "POST",
        dataType: "json",
        data: $('form#Article').serialize(),
        success: function (data, textStatus, jqXHR)
        {
            if (data.status == 'failure')
            {
                error_msg(data.error);

            } else if (data.status == "success") {
                showMsgPopup("Success", "Successfully Added", '', 'normal')
                setTimeout(function () {
                    close_toast_popup(url + 'Article')

                }, 2000)

            } else {

                showMsgPopup("Error", "Something Went Wrong Try Again", '', 'error')
                setTimeout(function () {
                    close_toast_popup(url + 'Article')

                }, 2000)
            }
        },
        error: function (jqXHR, textStatus, errorThrown)
        {

        }
    });
}
function save_product_type() {
    $.ajax({
        url: url + "Product_type/store",
        type: "POST",
        dataType: "json",
        data: $('form#Product_type').serialize(),
        success: function (data, textStatus, jqXHR)
        {
            if (data.status == 'failure')
            {
                error_msg(data.error);

            } else if (data.status == "success") {
                showMsgPopup("Success", "Successfully Added", '', 'normal')
                setTimeout(function () {
                    close_toast_popup(url + 'Product_type')

                }, 2000)

            } else {

                showMsgPopup("Error", "Something Went Wrong Try Again", '', 'error')
                setTimeout(function () {
                    close_toast_popup(url + 'Product_type')

                }, 2000)
            }
        },
        error: function (jqXHR, textStatus, errorThrown)
        {

        }
    });
}
function update_product_type() {
    $.ajax({
        url: url + "Product_type/update",
        type: "POST",
        dataType: "json",
        data: $('form#Product_type').serialize(),
        success: function (data, textStatus, jqXHR)
        {
            if (data.status == 'failure')
            {
                error_msg(data.error);

            } else if (data.status == "success") {
                showMsgPopup("Success", "Successfully Added", '', 'normal')
                setTimeout(function () {
                    close_toast_popup(url + 'Product_type')

                }, 2000)

            } else {

                showMsgPopup("Error", "Something Went Wrong Try Again", '', 'error')
                setTimeout(function () {
                    close_toast_popup(url + 'Product_type')

                }, 2000)
            }
        },
        error: function (jqXHR, textStatus, errorThrown)
        {

        }
    });
}
function save_metal() {
    $.ajax({
        url: url + "Metal/store",
        type: "POST",
        dataType: "json",
        data: $('form#Metal').serialize(),
        success: function (data, textStatus, jqXHR)
        {
            if (data.status == 'failure')
            {
                error_msg(data.error);

            } else if (data.status == "success") {
                showMsgPopup("Success", "Successfully Added", '', 'normal')
                setTimeout(function () {
                    close_toast_popup(url + 'Metal')

                }, 2000)

            } else {

                showMsgPopup("Error", "Something Went Wrong Try Again", '', 'error')
                setTimeout(function () {
                    close_toast_popup(url + 'Metal')

                }, 2000)
            }
        },
        error: function (jqXHR, textStatus, errorThrown)
        {

        }
    });
}
function update_metal() {
    $.ajax({
        url: url + "Metal/update",
        type: "POST",
        dataType: "json",
        data: $('form#Metal').serialize(),
        success: function (data, textStatus, jqXHR)
        {
            if (data.status == 'failure')
            {
                error_msg(data.error);

            } else if (data.status == "success") {
                showMsgPopup("Success", "Successfully Added", '', 'normal')
                setTimeout(function () {
                    close_toast_popup(url + 'Metal')

                }, 2000)

            } else {

                showMsgPopup("Error", "Something Went Wrong Try Again", '', 'error')
                setTimeout(function () {
                    close_toast_popup(url + 'Metal')

                }, 2000)
            }
        },
        error: function (jqXHR, textStatus, errorThrown)
        {

        }
    });
}
function save_carat() {
    $.ajax({
        url: url + "Carat/store",
        type: "POST",
        dataType: "json",
        data: $('form#Carat').serialize(),
        success: function (data, textStatus, jqXHR)
        {
            if (data.status == 'failure')
            {
                error_msg(data.error);

            } else if (data.status == "success") {
                showMsgPopup("Success", "Successfully Added", '', 'normal')
                setTimeout(function () {
                    close_toast_popup(url + 'Carat')

                }, 2000)

            } else {

                showMsgPopup("Error", "Something Went Wrong Try Again", '', 'error')
                setTimeout(function () {
                    close_toast_popup(url + 'Metal')

                }, 2000)
            }
        },
        error: function (jqXHR, textStatus, errorThrown)
        {

        }
    });
}
function update_carat() {
    $.ajax({
        url: url + "Carat/update",
        type: "POST",
        dataType: "json",
        data: $('form#Carat').serialize(),
        success: function (data, textStatus, jqXHR)
        {
            if (data.status == 'failure')
            {
                error_msg(data.error);

            } else if (data.status == "success") {
                showMsgPopup("Success", "Successfully Added", '', 'normal')
                setTimeout(function () {
                    close_toast_popup(url + 'Carat')

                }, 2000)

            } else {

                showMsgPopup("Error", "Something Went Wrong Try Again", '', 'error')
                setTimeout(function () {
                    close_toast_popup(url + 'Carat')

                }, 2000)
            }
        },
        error: function (jqXHR, textStatus, errorThrown)
        {

        }
    });
}
function Save_product_excel() {
  $(".ajaxLoader").fadeIn();
    $('.errors').html('');
    var formdata = new FormData();
    var file_data = $('input[type="file"]')[0].files;
    formdata.append("file_0", file_data[0]);
    formdata.append("corporate", $('.corporate').val());
    $.ajax({
        url: url + "Design/import",
        type: "POST",
        dataType: "json",
        contentType: false,
        processData: false,
        data: formdata,
        success: function (data, textStatus, jqXHR)
        {

            if (data.status == 'failure')
            {
              $(".ajaxLoader").fadeOut();
                if ($('.corporate').val() == '2') {
                    $.each(data.error, function (key, val) {
                        $('.errors').append('<p class="text-danger">' + val + '</p>');
                    })
                } else {
                    excel_error_msg(data.error);
                }

            } else if (data.status == "success") {
                showMsgPopup("Success", data.data + " Designs Successfully Added", '', 'normal')
                //location.reload();
                setTimeout(function () {
                    close_toast_popup(url + 'Upload_design_excel')

                }, 2000)

            } else {
              $(".ajaxLoader").fadeOut();
                showMsgPopup("Error", "Something Went Wrong Try Again", '', 'error')
                // setTimeout(function(){
                //     close_toast_popup(url+'Carat')

                // },2000)
            }
        },
        error: function (jqXHR, textStatus, errorThrown)
        {

        }
    });
}
function Delete_record(id, tr, controller) {
    var conf = confirm("Are you sure you want to delete?");
    if (conf)
    {
        $.ajax({
            url: url + controller + "/" + "delete",
            type: "POST",
            dataType: "json",
            data: {id: id},
            success: function (data, textStatus, jqXHR)
            {//console.log(data);
                if (data.status == 'failure')
                {
                    error_msg(data.error);
                } else if (data.status == "success") {
                    if (controller == 'Product')
                        controller = document.referrer;
                    else
                        controller = url + controller;
                    showMsgPopup("Success", "Successfully Deleted", '', 'normal')
                    setTimeout(function () {
                        close_toast_popup(controller)


                    }, 2000)

                } else {

                    showMsgPopup("Error", "Something Went Wrong Try Again", '', 'error')
                    setTimeout(function () {
                        close_toast_popup(url + controller)

                    }, 2000)
                }

            },
            error: function (jqXHR, textStatus, errorThrown)
            {

            }
        });
    }
}

function karigar_replace_record(id, tr, karigar_id, controller) {
    var conf = confirm("Are you sure you want to Merge?");
    if (conf)
    {
        $.ajax({
            url: url + controller + "/" + "merge_replace",
            type: "POST",
            dataType: "json",
            data: {id: id, karigar_id :karigar_id},
            success: function (data, textStatus, jqXHR)
            {
                console.log(data);
                if (data.status == 'failure')
                {
                    error_msg(data.error);
                } else if (data.status == "success") {
               
                        //controller = url + controller+ "/" + "merge_replace_karigar_list/"+karigar_id;
                        controller = url + controller;
                    showMsgPopup("Success", "Successfully Merge", '', 'normal')
                    setTimeout(function () {
                        close_toast_popup(controller)


                    }, 2000)

                } else {

                    showMsgPopup("Error", "Something Went Wrong Try Again", '', 'error')
                    setTimeout(function () {
                        close_toast_popup(url + controller)

                    }, 2000)
                }

            },
            error: function (jqXHR, textStatus, errorThrown)
            {

            }
        });
    }
}
function Delete_party_record(id, tr, controller, type) {
    var conf = confirm("Are you sure you want to delete?");
    if (conf)
    {
        $.ajax({
            url: url + controller + "/" + "delete",
            type: "POST",
            dataType: "json",
            data: {id: id, type: type},
            success: function (data, textStatus, jqXHR)
            {//console.log(data);
                if (data.status == 'failure')
                {
                    error_msg(data.error);
                } else if (data.status == "success") {
                    if (controller == 'Product')
                        controller = document.referrer;
                    else
                        controller = url + controller;
                    showMsgPopup("Success", "Successfully Deleted", '', 'normal')
                    setTimeout(function () {
                        close_toast_popup(controller)


                    }, 2000)

                } else {

                    showMsgPopup("Error", "Something Went Wrong Try Again", '', 'error')
                    setTimeout(function () {
                        close_toast_popup(url + controller)

                    }, 2000)
                }

            },
            error: function (jqXHR, textStatus, errorThrown)
            {

            }
        });
    }
}

function save_product_images(product_code, $file)
{
    var formdata = new FormData();
    //var file_data = $("#add_product_image").prop("files")[0]; 
    formdata.append('file', $file);
    formdata.append('product_code', product_code);
    $.ajax({
        url: url + "add_product_images/upload",
        type: "POST",
        dataType: "json",
        data: formdata,
        processData: false,
        contentType: false,
        success: function (data)
        {
            if (data.status == 'failure')
            {
                error_msg(data.error);

            } else if (data.status == "success") {
                showMsgPopup("Success", "Successfully Deleted", '', 'normal')
                setTimeout(function () {
                    close_toast_popup(url + '');

                }, 2000)

            } else {

                showMsgPopup("Error", "Something Went Wrong Try Again", '', 'error')
                setTimeout(function () {
                    close_toast_popup(url + controller)

                }, 2000)
            }

        },
        error: function (jqXHR, textStatus, errorThrown)
        {

        }
    });//ajax end

}
$(document).on('change', '.change_image', function () {
    // $('#image_product').removeAttr('hidden');
    var attr = $(this).attr('attr');
    var place_id = $(this).attr('place_id');
    $('#' + place_id).removeAttr('hidden');
    if (page_title == 'ADD PRODUCTS IMAGES')
        product_id = $(this).attr('product_code');
    else
        product_id = '';
    readURL(this, place_id, product_id);
})

function readURL(input, place_id, product_id) {
    if (input.files && input.files[0]) {
        var reader = new FileReader();

        reader.onload = function (e) {
            $('#' + place_id).attr('src', e.target.result);
        }

        reader.readAsDataURL(input.files[0]);
    }
    save_product_images(product_id, input.files[0]);
}

function search_product(page_id) {
    $("#product_approval_search").submit();
}

function sortlisted_product_store(page_id = '') {
    //alert(($('#corporate').val()));
    var corporate = $('#corporate').val();
    if (corporate == '') {
        showMsgPopup("Error", "Please selecte corporate", '', 'error');

        return false;
    } else {
        //$("#sortlist_product_form" ).submit();
        location.href = url + "finalize_and_send?" + $("#product_shortlist_search").serialize();
}
}

function fetch_from_dropbox() {
    $('.ajaxLoader').show();
    //var formdata = new FormData();
    $.ajax({
        url: url + "Design/fetch_images_from_dropbox",
        type: "POST",
        dataType: "json",
        data: {status: 1},
        success: function (data, textStatus, jqXHR)
        {
            $('.ajaxLoader').hide();
            if (data.status == 'failure')
            {
                excel_error_msg(data.error);

            } else if (data.status == "success") {
                showMsgPopup("Success", "Downloading images start", '', 'normal')
                // setTimeout(function(){
                //     close_toast_popup(url+'Carat')

                // },2000)

            } else {

                showMsgPopup("Error", "Something Went Wrong Try Again", '', 'error')
                // setTimeout(function(){
                //     close_toast_popup(url+'Carat')

                // },2000)
            }
        },
        error: function (jqXHR, textStatus, errorThrown)
        {

        }
    });
}
function fetch_sub_category_img_from_dropbox(id) {
    $('.ajaxLoader').show();
    //var formdata = new FormData();
    $.ajax({
        url: url + "Sub_category/fetch_images_from_dropbox",
        type: "POST",
        dataType: "json",
        data: {sub_cat_id: id},
        success: function (data, textStatus, jqXHR)
        {
            $('.ajaxLoader').hide();
            if (data.status == 'failure')
            {
                excel_error_msg(data.error);

            } else if (data.status == "success") {
                showMsgPopup("Success", "Downloading images start", '', 'normal')
                setTimeout(function () {
                    close_toast_popup(url + 'Sub_category/view_dropbox_image/' + data.encrypted_id);
                }, 2000)

            } else {

                showMsgPopup("Error", "Something Went Wrong Try Again", '', 'error')
                // setTimeout(function(){
                //     close_toast_popup(url+'Carat')

                // },2000)
            }
        },
        error: function (jqXHR, textStatus, errorThrown)
        {

        }
    });
}

function shortlisted_product(product_id, type, abc)
{
    $.ajax({
        url: url + "send_for_approval/" + type,
        type: "POST",
        dataType: "json",
        data: {'product_id': product_id},
        success: function (data, textStatus, jqXHR)
        {
            if (typeof data.status != 'undefined' && data.status == 'error') {
                showMsgPopup("Error", data.data, '', 'error');
            } else {
                $("#shortlist_count").html(data);
                if (data == 0) {
                    var table = '<tr id="empty_tr"><td>No Products shortlisted</td><td></td><td></td><td></td><td></td><td></td><td></td><td></td><td></td><td></td></tr>';
                    $('#table_html').html(table);
                }
                if (type == "store") {
                    var row = $(abc).closest('tr').html();
                    var current_id = $(abc).closest('tr').attr('id');
                    $('#sortlist_table').append('<tr id="' + product_id + '">' + row + '</tr>').find("button").replaceWith('<button class="btn btn-primary waves-effect waves-light  btn-md" name="commit" type="button" onclick="removeButton(this)">Remove</button>');
                    $(abc).closest('tr').find("button").prop("disabled", true);
                    $(abc).closest('tr').find("button").html("SHORTLISTED");
                    $('#empty_tr').remove()
                    getShortlistDesignProducts();
                }
            }
        },
        error: function (jqXHR, textStatus, errorThrown)
        {

        }
    });
}
function Finalize_and_send() {
    $.ajax({
        url: url + "Finalize_and_send/update",
        type: "POST",
        dataType: "json",
        data: $('form#finalize').serialize(),
        success: function (data, textStatus, jqXHR)
        {
            if (data !== '')
            {
                close_toast_popup(url + 'Finalize_and_send/export_shortlisted_products/' + data);

            } else if (data.status == "success") {
                //showMsgPopup("Success","Successfully Added",'','normal')
            } else {

                showMsgPopup("Error", "Something Went Wrong Try Again", '', 'error')

            }
        },
        error: function (jqXHR, textStatus, errorThrown)
        {

        }
    });
}

function Approve_products(id) {
    $.ajax({
        url: url + "Receive_pieces/approve",
        type: "POST",
        dataType: "json",
        data: {id: id},
        success: function (data, textStatus, jqXHR)
        {
            if (data == true)
            {
                showMsgPopup("Success", "Product Approved", '', 'normal');
                // close_toast_popup(document.referrer);
                // location.reload(); 

                $(".b_" + id).closest('tr').remove();
            } else if (data.status == "success") {
                showMsgPopup("Success", "Successfully Added", '', 'normal')
            } else {

                showMsgPopup("Error", "Something Went Wrong Try Again", '', 'error')

            }
        },
        error: function (jqXHR, textStatus, errorThrown)
        {

        }
    });
}
function reject_product(id) {
    $.ajax({
        url: url + "Receive_pieces/reject",
        type: "POST",
        dataType: "json",
        data: {id: id},
        success: function (data, textStatus, jqXHR)
        {
            if (data == true)
            {
                showMsgPopup("Success", "Product Rejected", '', 'normal');
                //close_toast_popup(document.referrer);
                //location.reload(); 
                $(".b_" + id).closest('tr').remove();
            } else if (data.status == "success") {
                //showMsgPopup("Success","Successfully Added",'','normal')
            } else {

                showMsgPopup("Error", "Something Went Wrong Try Again", '', 'error')

            }
        },
        error: function (jqXHR, textStatus, errorThrown)
        {

        }
    });
}
function search_order() {
    $('#c_class').hide();
    if ($('#serach_order').val() == '') {
        $('#name_error').html('Please enter order number');
        return false;
    }
    $.ajax({
        url: url + "Search_order/search",
        type: "POST",
        dataType: "json",
        data: $('form#serach_text').serialize(),
        success: function (data, textStatus, jqXHR)
        {
            $('#corporate_error').html('');
            $('#name_error').html('');
            if (data.status && data.status == 'failure') {
                $('#corporate_error').html(data.error);
                return false;
            }
            $('.ajaxLoader').hide();
            if (data.data == null)
            {
                $('#corporate_error').html('');
                $('#name_error').html('');
                $('#order_not_found').show();
                $('#order_found').hide();
                $('#already_exist').hide();
                $('.order_id ').html('<input type="hidden" name="Order[order_id]">' + $('#serach_order').val());
                $('.order_id input').val($('#serach_order').val());
            } else if (data.status == '1') {
                $('#already_exist').show();
                $('#order_not_found').hide();
                $('#order_found').hide();
                $('#already_exist').html('Order ID Already in Used');

            } else
            {   //console.log(data);
                var temp_data = data.data;
                $('#order_found').show();
                $('#order_not_found').hide();
                $('#already_exist').hide();
                /* $('.order_id input').val($('#serach_order').val());
                 $('.order_id ').append($('#serach_order').val());*/
                $('.order_id ').html('<input type="hidden" name="Order[order_id]">' + $('#serach_order').val());
                $('.order_id input').val($('#serach_order').val());
                var dt = temp_data.date.split("-");
                var dt1 = dt[2].split(" ");
                var date = dt1[0] + '-' + dt[1] + '-' + dt[0];
                $('.date input').val(date);
                $('.date').append(date);
                if (temp_data.corporate == '1') {
                    var corporate = 'Reliance Jewels';
                } else {
                    var corporate = 'Caratlane';
                }
                $('.corporate input').val(corporate);
                $('.corporate ').append(corporate);
                $('.gross_weight input').val(temp_data.gross_weight);
                $('.gross_weight ').append(temp_data.gross_weight);
                $('.net_weight input').val(temp_data.net_weight);
                $('.net_weight ').append(temp_data.net_weight);
                $('.less_weight input').val(temp_data.less_weight);
                $('.less_weight ').append(temp_data.less_weight);

            }
        },
        error: function (jqXHR, textStatus, errorThrown)
        {

        }
    });
}
function Upload_products(id, elm) {
    $('.errors').html('');
  //$(".ajaxLoader").fadeIn();
  $(elm).attr('disabled', true);
    var formData = new FormData($('form#' + id)[0]);
    formData.append('file', $('#file')[0].files[0]);
    $.ajax({
        url: url + "Search_order/store",
        type: "POST",
        dataType: "json",
        data: formData,
        processData: false,
        contentType: false,
        success: function (data, textStatus, jqXHR)
        {
            $(elm).attr('disabled', false);
            if (data.status == 'failure')
            {
              $(".ajaxLoader").fadeOut();
                excel_error_msg(data.error);
                //error_msg(data.error);


            } else if (data.status == "success") {

                showMsgPopup("Success", "Successfully Uploaded", '', 'normal')
                setTimeout(function () {
                    close_toast_popup(url + 'Prepare_order')

                }, 2000)

            } else {
              $(".ajaxLoader").fadeOut();
                showMsgPopup("Error", "Something Went Wrong Try Again", '', 'error')
                setTimeout(function () {
                    close_toast_popup(url + 'Search_order')

                }, 2000)
            }
        },
        error: function (jqXHR, textStatus, errorThrown)
        {

        }
    });

}
var image_no_popup = 100;
if (page_title == 'PREPARE ORDER') {
//alert('ak')
    function pagination(page_count1, object1, filter) {
        sel_karigar = $('#sel_karigar').val();
       // $('.ajaxLoader').show();
        $.ajax({
            url: url + "Prepare_order/search/" + karigar_id,
            type: "POST",
            dataType: "json",
            data: {offset: page_count1, filter: object1, sel_karigar: sel_karigar},
            success: function (data, textStatus, jqXHR)
            {
                $('.ajaxLoader').hide();

                var html = '';
                if (data !== '') {
                    page_count = data.page_count;
                    load_more = 0;
                    console.log(data.result)
                    $.each(data.result, function (key, val) {

                        var psize = val.size;
                        if (psize == null || psize == '') {
                            psize = '-';
                        }
                        /*    console.log('ppl_quantity'+val.ppl_quantity)
                         console.log('selected'+val.selected)*/
                        //alert(ppl_quantity)karigar_qnty
                        if (ppl_quantity == null || ppl_quantity == '') {
                            ppl_quantity = '';
                        }
                        var ppl_quantity = val.ppl_quantity;

                        //var corp_quantity = val.CW_quantity;
                        var corp_quantity = val.cw_quantity;//changes 16feb18 for products merge same order,size_master,weight_band
                        var display_quantity = '';

                        if (val.selected == '1')
                        {
                            console.log(val.ppl_quantity);
                            display_quantity = ppl_quantity;
                        } else
                        {
                            console.log(val.cw_quantity);
                            display_quantity = corp_quantity - ppl_quantity;
                        }

                        if (val.product_code !== '' || typeof val.product_code !== "undefined") {
                            var merge_qnt = val.merge_id;
                            merge_qnt = merge_qnt.replace(" ", "");
                            var rep_merge_qnt = merge_qnt.replace(",", "_");
                            image_no_popup++;
                            html += '<div style="padding-top: 10px;" class="col-lg-6 col-md-6 col-sm-6 col-xs-12">';
                            html += '<div class="col-lg-5 check-box">'
                            html += '<img onclick="img_popup(' + image_no_popup + ')" id="img_' + image_no_popup + '" class="img-responsive img-rounded" src="' + url + 'uploads/product/' + val.product_code + '/small/' + val.image + '">'

                            // if(val.selected == '1' || (val.product_karigar_id == sel_karigar)){
                            if (val.selected == '1') {
                                html += '<div class="PREPARE_check checkbox checkbox-purpal"><input data-parsley-multiple="group1" data-parsley-id="76" type="checkbox" name="products[]"  value="' + merge_qnt + '" class="from-control checked_products" onclick="check_checked(this)" checked><label for="PREPARE_check" ></label></div>'
                            } else {
                                var che = '';
                                var che_class = 'unchecked_products';

                                if ($('#all_check_qc').val() == 1) {
                                    console.log('checked');
                                    che = 'checked';
                                    che_class = 'checked_products';
                                }//add_seleced_pp
                                html += '<div class="PREPARE_check checkbox checkbox-purpal"><input data-parsley-multiple="group1" data-parsley-id="76" type="checkbox" name="products[]" ' + che + '  value="' + merge_qnt + '" class="from-control ' + che_class + '" onclick="check_checked(this)"><label for="PREPARE_check"></label></div>'
                            }
                            html += '</div>'
                            html += '<div class="col-lg-6">'
                            html += '<p>Order Id : ' + val.order_id + '</p>'
                            html += '<p>Design Code : ' + val.product_code + '</p>'
                            if (val.corporate == 2) {
                                var tolerance = val.tolerance_c_wt;
                                var from_wt_torlen = parseFloat(val.cp_net_weight) - parseFloat(tolerance);
                                var to_wt_torlen = parseFloat(tolerance) + parseFloat(val.cp_net_weight);
                                var tolerance_wt_range = from_wt_torlen.toFixed(2) + ' - ' + to_wt_torlen.toFixed(2);
                                html += '<p>Size/Length : ' + val.cp_size + '</p>'
                                html += '<p>Net Weight : ' + val.cp_net_weight + '</p>'
                                html += '<p>Weight Range : ' + tolerance_wt_range + '</p>'

                            } else {
                                var tolerance = val.tolerance_wt;
                                var from_wt_torlen = parseFloat(val.cp_weight) - parseFloat(tolerance);
                                var to_wt_torlen = parseFloat(tolerance) + parseFloat(val.cp_weight);
                                var tolerance_wt_range = from_wt_torlen.toFixed(2) + ' - ' + to_wt_torlen.toFixed(2);

                                html += '<p>Size/Length : ' + val.size_master + '</p>'
                                html += '<p>Avg Weight  : ' + val.cp_weight + '</p>'
                                html += '<p>Weight Range : ' + tolerance_wt_range + '</p>'
                            }

                            //html += '<p>Pair/Pieces : '+val.CW_quantity+' '+val.CW_unit+'</p>'
                            html += '<p>Quantity : <input type="text" name="quantity[' + val.cp_id + '][]" class="ppl_qnty" id="quantity_' + val.cp_id + '" value="' + display_quantity + '">'

                            html += '<span><input type="hidden" class="corporate1" name="corporate_product_id" value="' + val.cp_id + '"></span><span class="text-danger" id="quantity_error"></span></p>'
                            html += '</div>'
                            html += '<span class="text-danger col-sm-6 error_prepare_order" id="products' + rep_merge_qnt + '_error"></span></div>'
                            $("#prepare_order_submit").css('display', 'block');
                        }
                    })
                    //console.log(data.filters);
                    create_filter(data.filters);
                }
                if (page_count1 == '0') {
                    $('.load_html').html('');
                    if (html == '')
                    {
                        html = '<div style="padding-top: 10px;" class="text-danger col-sm-10 pull-right m-t-20"><p>*Data not found.</p></div>';
                        $("#prepare_order_submit").css('display', 'none');
                    }
                    $('.load_html').html(html);
                } else {
                    $('.load_html').append(html);
                }

                var checked_products_count = $('.load_html [name="products[]"]:checked').length;

                if (checked_products_count == '0') {
                    $("#product_count").hide();

                } else
                {
                    $("#product_count").show();
                    $("#product_count").text('Selected Products :- (' + checked_products_count + ')');
                }



            },
            error: function (jqXHR, textStatus, errorThrown)
            {

            }
        });
        $('.preloader').hide();
    }
}
function create_filter(filters) {
    var order_html = '';
    var category = '';
    var weight_range = '';
    console.log(filters);
    $(".row1").show();
    $("#hide").show();
    $("#show").hide();
    if (filters.orders) {
        $.each(filters.orders, function (o_key, o_val) {
            var checked = '';
            if (o_val.selected && o_val.selected == '1') {
                //checked = 'checked';
            }
            if (checked !== 'checked') {
                var dt = o_val.date.split("-");
                var dt1 = dt[2].split(" ");
                var date = dt1[0] + '-' + dt[1] + '-' + dt[0];
                order_html += '<tr class="row1 corporate_' + o_val.corporate_id + '" style="display: table-row;"><td><div class="checkbox checkbox-purpal"><input type="checkbox" data-parsley-multiple="groups" data-parsley-mincheck="2" data-parsley-id="69" name="cp.order_id" class="filter" onchange="filter()" value="' + o_val.order_id + '" ' + checked + '><label for="order_id"></label></div></td><td><span style="float:right">' + o_val.order_id + '</span></td><td><span style="float:right">' + date + '</span></td><td>' + o_val.corporate + '</td><td> <span style="float:right">' + o_val.total_products + ' </span> </td><td> <span style="float:right">' + o_val.pending_products + '</span></td> </tr>';
            }
        });
        if (order_html != '') {

            $('#orders').show();
            // $('.categorycode').show();
            // $('.weight_range').show();
            // $('.productcode').show();
            $('#orders').html(order_html);
        }
    } else {
        // $('.categorycode').hide();
        // $('.weight_range').hide();
        // $('.productcode').hide();
        $('#orders').hide();

    }
    console.log(filters.categories);
    if (filters.categories) {
        $.each(filters.categories, function (c_key, c_val) {
            var checked = '';
            if (c_val.selected && c_val.selected == '1') {
                checked = 'checked';
            }
            //if(checked == 'checked'){
            category += '<div class="checkbox checkbox-purpal col-md-3"><input type="checkbox" name="cp.product_master" value=' + c_val.product_master + ' class="filter from-control" onchange="filter()" data-parsley-multiple="groups" data-parsley-mincheck="2" data-parsley-id="69" ' + checked + '><label for="inputEmail3"> ' + c_val.product_master + '(' + c_val.product_count + ') </label></div>';
            //}
        });
        if (category != '') {
            $('#category').show();
            //console.log(category)
            //  $('.categorycode').show();
            // $('.weight_range').show();
            //  $('.productcode').show();
            $('#category').html(category);
        }
    } else {
        // $('.categorycode').hide();
        // $('.weight_range').hide();
        // $('.productcode').hide();
        $('#category').hide();
    }
}
/*$(document).ready(function() {
 
 $('body').on('blur', '.ppl_qnty', function() {
 $("#prepare_order_submit").prop( "disabled", false );
 var quantity_textbox = $(this);
 var qnty = quantity_textbox.val();
 var corporate_product_id = quantity_textbox.closest('p').find('.corporate1').val();
 $.ajax({
 url : url+"Prepare_order/check_quantity",
 type: "POST",
 dataType: "json",
 data : {id:corporate_product_id,quantity:qnty},
 success: function(data, textStatus, jqXHR)
 {
 if(data.status=='success')
 {
 
 }
 else
 {
 // showMsgPopup("Error",data.error,'','error')
 showMsgPopup("Error","Something Went Wrong Try Again",'','error')
 $("#prepare_order_submit").prop( "disabled", true );
 }
 },
 });
 })
 });*/
$(".filter").change(function () {
//$( ".filter" ).keyup(function() {
    object = $("form#Prepare_order").serializeArray();
    page_count = 0;
    // console.log(object);
    pagination(page_count, object);

});
function filter() {
//$( ".filter" ).keyup(function() {
    object = $("form#Prepare_order").serializeArray();
    page_count = 0;
    // console.log(object);
    pagination(page_count, object);

}
;
$(".filter").keyup(function () {
    object = $("form#Prepare_order").serializeArray();
    page_count = 0;
    pagination(page_count, object);

});
$(".karigar").on('change', function () {
    var selectedValue = $(".karigar option:selected").val();
    $('#karigar_id').val(selectedValue);
    // //object={'id':$('#selected_id').val()}
    // page_count = 0;
    //  object={'id':''};
    //  if(!selectedValue){
    //     pagination(page_count,object);
    // }
});
function prepare_order(param) {
    $('#prepare_order_submit').prop('disabled', true);
    $('#prepare_order_edit').prop('disabled', true);
    //$('.ajaxLoader').show();

    $.ajax({
        url: url + "Prepare_order/store",
        type: "POST",
        dataType: "json",
        data: $('form#products').serialize(),
        success: function (data, textStatus, jqXHR)
        {
            if (data.status == 'failure')
            {
                showMsgPopup("Error", 'Something Went Wrong Try Again', '', 'error')
                excel_error_msg(data.error);
                $('#prepare_order_submit').prop('disabled', false);
                $('#prepare_order_edit').prop('disabled', false);
                //showMsgPopup("Error",data.error,'','error')


            } else if (data.status == "success") {
                showMsgPopup("Success", "Successfully submitted", '', 'normal')
                setTimeout(function () {
                    if (param == '1') {
                        close_toast_popup(url + 'Print_Order/index/' + data.karigar_id)
                    } else {
                        sel_karigar = $('#sel_karigar').val();
                        close_toast_popup(url + 'Prepare_order/' + sel_karigar);
                    }

                }, 2000)

            } else {

                /* showMsgPopup("Error","Something Went Wrong Try Again",'','error')
                 setTimeout(function(){
                 close_toast_popup(url+'Prepare_order')
                 
                 },2000)*/
            }
        },
        error: function (jqXHR, textStatus, errorThrown)
        {

        }
    });
    $('.ajaxLoader').hide();
}


function MakeEngaged(karigar_id) {
    $.ajax({
        type: "POST",
        dataType: "json",
        data: {id: karigar_id},
        url: url + "Print_Order/make_engaged",
        cache: false,
        success: function (data, textStatus, jqXHR)
        {
            if (data.status == 'failure')
            {
            } else if (data.status == "success") {
                showMsgPopup("Success", "Successfully Updated", '', 'normal')
                setTimeout(function () {
                    close_toast_popup(url + 'Print_Order/index/' + karigar_id)
                }, 2000)

            } else {

                showMsgPopup("Error", "Something Went Wrong Try Again", '', 'error')
                setTimeout(function () {
                    close_toast_popup(url + 'Reprint_Order/index/' + karigar_id)

                }, 2000)
            }
        },
        error: function (jqXHR, textStatus, errorThrown)
        {

        }

    });
}

function save_buying_complexity() {
    $.ajax({
        url: url + "Buying_complexity/store",
        type: "POST",
        dataType: "json",
        data: $('form#buying_complexity').serialize(),
        success: function (data, textStatus, jqXHR)
        {
            if (data.status == 'failure')
            {
                error_msg(data.error);

            } else if (data.status == "success") {
                showMsgPopup("Success", "Successfully Added", '', 'normal')
                setTimeout(function () {
                    close_toast_popup(url + 'Buying_complexity')

                }, 2000)

            } else {

                showMsgPopup("Error", "Something Went Wrong Try Again", '', 'error')
                setTimeout(function () {
                    close_toast_popup(url + 'Buying_complexity')

                }, 2000)
            }
        },
        error: function (jqXHR, textStatus, errorThrown)
        {

        }
    });
}

function update_buying_complexity() {
    $.ajax({
        url: url + "Buying_complexity/update",
        type: "POST",
        dataType: "json",
        data: $('form#buying_complexity').serialize(),
        success: function (data, textStatus, jqXHR)
        {
            if (data.status == 'failure')
            {
                error_msg(data.error);

            } else if (data.status == "success") {
                showMsgPopup("Success", "Successfully Updated", '', 'normal')
                setTimeout(function () {
                    close_toast_popup(url + 'Buying_complexity')

                }, 2000)

            } else {

                showMsgPopup("Error", "Something Went Wrong Try Again", '', 'error')
                setTimeout(function () {
                    close_toast_popup(url + 'Buying_complexity')

                }, 2000)
            }
        },
        error: function (jqXHR, textStatus, errorThrown)
        {

        }
    });
}
function save_manufacturing_type() {
    $.ajax({
        url: url + "Manufacturing_type/store",
        type: "POST",
        dataType: "json",
        data: $('form#manufacturing_type').serialize(),
        success: function (data, textStatus, jqXHR)
        {
            if (data.status == 'failure')
            {
                error_msg(data.error);

            } else if (data.status == "success") {
                showMsgPopup("Success", "Successfully Added", '', 'normal')
                setTimeout(function () {
                    close_toast_popup(url + 'Manufacturing_type')

                }, 2000)

            } else {

                showMsgPopup("Error", "Something Went Wrong Try Again", '', 'error')
                setTimeout(function () {
                    close_toast_popup(url + 'Manufacturing_type')

                }, 2000)
            }
        },
        error: function (jqXHR, textStatus, errorThrown)
        {

        }
    });
}

function update_manufacturing_type() {
    $.ajax({
        url: url + "Manufacturing_type/update",
        type: "POST",
        dataType: "json",
        data: $('form#manufacturing_type').serialize(),
        success: function (data, textStatus, jqXHR)
        {
            if (data.status == 'failure')
            {
                error_msg(data.error);

            } else if (data.status == "success") {
                showMsgPopup("Success", "Successfully Updated", '', 'normal')
                setTimeout(function () {
                    close_toast_popup(url + 'Manufacturing_type')

                }, 2000)

            } else {

                showMsgPopup("Error", "Something Went Wrong Try Again", '', 'error')
                setTimeout(function () {
                    close_toast_popup(url + 'Manufacturing_type')

                }, 2000)
            }
        },
        error: function (jqXHR, textStatus, errorThrown)
        {

        }
    });
}
function save_pcs() {
    $.ajax({
        url: url + "Pcs/store",
        type: "POST",
        dataType: "json",
        data: $('form#pcsForm').serialize(),
        success: function (data, textStatus, jqXHR)
        {
            if (data.status == 'failure')
            {
                error_msg(data.error);

            } else if (data.status == "success") {
                showMsgPopup("Success", "Successfully Added", '', 'normal')
                setTimeout(function () {
                    close_toast_popup(url + 'Pcs')

                }, 2000)

            } else {

                showMsgPopup("Error", "Something Went Wrong Try Again", '', 'error')
                setTimeout(function () {
                    close_toast_popup(url + 'Pcs')

                }, 2000)
            }
        },
        error: function (jqXHR, textStatus, errorThrown)
        {

        }
    });
}

function update_pcs() {
    $.ajax({
        url: url + "Pcs/update",
        type: "POST",
        dataType: "json",
        data: $('form#pcsForm').serialize(),
        success: function (data, textStatus, jqXHR)
        {
            if (data.status == 'failure')
            {
                error_msg(data.error);

            } else if (data.status == "success") {
                showMsgPopup("Success", "Successfully Updated", '', 'normal')
                setTimeout(function () {
                    close_toast_popup(url + 'Pcs')

                }, 2000)

            } else {

                showMsgPopup("Error", "Something Went Wrong Try Again", '', 'error')
                setTimeout(function () {
                    close_toast_popup(url + 'Pcs')

                }, 2000)
            }
        },
        error: function (jqXHR, textStatus, errorThrown)
        {

        }
    });
}
function save_user_access() {
    $.ajax({
        url: url + "User/store",
        type: "POST",
        dataType: "json",
        data: $('form#User_access').serialize(),
        success: function (data, textStatus, jqXHR)
        {
            if (data.status == 'failure')
            {
                error_msg(data.error);

            } else if (data.status == "success") {
                showMsgPopup("Success", "Successfully Updated", '', 'normal')
                setTimeout(function () {
                    close_toast_popup(url + 'User')

                }, 2000)

            } else {

                showMsgPopup("Error", "Something Went Wrong Try Again", '', 'error')
                setTimeout(function () {
                    close_toast_popup(url + 'User')

                }, 2000)
            }
        },
        error: function (jqXHR, textStatus, errorThrown)
        {

        }
    });
}
function Edit_user_access() {
    $.ajax({
        url: url + "User/update",
        type: "POST",
        dataType: "json",
        data: $('form#User_access').serialize(),
        success: function (data, textStatus, jqXHR)
        {
            if (data.status == 'failure')
            {
                error_msg(data.error);

            } else if (data.status == "success") {
                showMsgPopup("Success", "Successfully Updated", '', 'normal')
                setTimeout(function () {
                    close_toast_popup(url + 'User')

                }, 2000)

            } else {

                showMsgPopup("Error", "Something Went Wrong Try Again", '', 'error')
                setTimeout(function () {
                    close_toast_popup(url + 'User')

                }, 2000)
            }
        },
        error: function (jqXHR, textStatus, errorThrown)
        {

        }
    });
}
function Receive_pieces_excel() {
    $('.ajaxLoader').show();
    $('.errors').html('');
    var formdata = new FormData();
    var file_data = $('input[type="file"]')[0].files;
    formdata.append("file_0", file_data[0]);
    var other_data = $('#Upload_receive_pieces').serializeArray();
    $.each(other_data, function (key, input) {
        formdata.append(input.name, input.value);
    });
    $.ajax({
        url: url + "Receive_pieces/import",
        type: "POST",
        dataType: "json",
        contentType: false,
        processData: false,
        data: formdata,
        success: function (data, textStatus, jqXHR)
        {
            $('.ajaxLoader').hide();
            if (data.status == 'failure')
            {
                excel_error_msg(data.error);

            } else if (data.status == "success") {
                showMsgPopup("Success", data.data + " Designs Successfully Updated", '', 'normal')
                setTimeout(function () {
                    close_toast_popup(document.referrer)

                }, 2000)

            } else {

                showMsgPopup("Error", "Something Went Wrong Try Again", '', 'error')
                // setTimeout(function(){
                //     close_toast_popup(url+'Carat')

                // },2000)
            }
        },
        error: function (jqXHR, textStatus, errorThrown)
        {

        }
    });
}

function save_RcvproductFrmKarigar(obj) {
    /*  if ($('.product_check input[type="checkbox"]').not(':checked').length == $('.product_check input[type="checkbox"]').length) {
     showMsgPopup("Error","Please check at least one checkbox",'','error')
     $(obj).attr('disabled',false);
     return false;
     }*/

    $(obj).attr('disabled', true);
    //$('.errors').html('');
    $.ajax({
        url: url + "Receive_products/store",
        type: "POST",
        dataType: "json",
        data: $('form#ReceiveProductsForm').serialize(),
        success: function (data, textStatus, jqXHR)
        {
            if (data.status == 'failure')
            {
                $(obj).attr('disabled', false);
                console.log(data.error);
                $.each(data.error, function (key, val) {
                    $.each(val, function (key1, val1) {
                        $('#' + key1).html(val1);
                    })
                })
                $(obj).attr('disabled', false);

            } else if (data.status == "error") {
                showMsgPopup("Error", "Please Enter at least one product gross weight value", '', 'error')
                setTimeout(function () {
                    // close_toast_popup(url+'Receive_products/create')
                }, 2000)
                $(obj).attr('disabled', false);

            } else if (data.status == "success") {
                showMsgPopup("Success", "Successfully received", '', 'normal')
                setTimeout(function () {
                    close_toast_popup(url + 'Receive_products')

                }, 2000)

            } else {

                showMsgPopup("Error", "Something Went Wrong Try Again", '', 'error')
                setTimeout(function () {
                    close_toast_popup(url + 'Receive_products')

                }, 2000)
                $(obj).attr('disabled', false);
            }
        },
        error: function (jqXHR, textStatus, errorThrown)
        {

        }
    });
}
if (page_title == "Receive Products") {
    $(document).ready(function () {
        $("#product_code_0").focus();
        $('#Receive_Product_btn').prop('disabled', true);
    });

    $("#pdate").datepicker({
        autoclose: true,
        todayHighlight: true,
        format: "dd-mm-yyyy",
        todayHighlight: true,
    }).datepicker("setDate", new Date());

    $("#pdate").datepicker({
        autoclose: true,
        todayHighlight: true,
        format: "dd-mm-yyyy",
        todayHighlight: true,
    }).change(function (selected) {
        selectedDate = $('#pdate').datepicker({dateFormat: 'dd-mm-yyyy'}).val();
        $(".datepickerInput1").datepicker("setDate", selectedDate);
    });

    $(".datepickerInput1").datepicker({
        autoclose: true,
        todayHighlight: true,
        format: "dd-mm-yyyy",
    }).datepicker("setDate", new Date());
}

function Redirect(cntrl) {

    setTimeout(function () {
        window.location.href = url + cntrl;
    }, 2000)
}
function Redirect_url(cntrl) {
    window.location.href = url + cntrl;
}

function accept_qc() {
    var conf = confirm("Are you sure you want to accept?");
    if (!conf)
    {
        return false;
    }
    if ($('#sub_catagory input[type="checkbox"]').not(':checked').length == 0) {
    } else {
        showMsgPopup("Error", "Please check all Parameters", '', 'error')
        setTimeout(function () {
            close_toast_popup(location);
        }, 2000)
        //alert('Please check all Parameters');
        return false;
    }
    $.ajax({
        url: url + "Quality_control/store",
        type: "POST",
        dataType: "json",
        data: $('form#qc_check').serialize(),
        success: function (data, textStatus, jqXHR)
        {
            if (data.status == 'failure')
            {
                error_msg(data.error);

            } else if (data.status == "success") {
                showMsgPopup("Success", "successfully accepted", '', 'normal')
                setTimeout(function () {
                    close_toast_popup(url + 'Receive_products?status=pending')

                }, 2000)

            } else {

                showMsgPopup("Error", "Something Went Wrong Try Again", '', 'error')
                setTimeout(function () {
                    close_toast_popup(url + 'Corporate_Process')

                }, 2000)
            }
        },
        error: function (jqXHR, textStatus, errorThrown)
        {

        }
    });
}
function reject_qc() {
    var conf = confirm("Are you sure you want to reject?");
    if (!conf)
    {
        return false;
    }
    $.ajax({
        url: url + "Quality_control/reject",
        type: "POST",
        dataType: "json",
        data: $('form#qc_check').serialize(),
        success: function (data, textStatus, jqXHR)
        {
            if (data.status == 'failure')
            {
                error_msg(data.error);

            } else if (data.status == "success") {
                showMsgPopup("Success", "successfully rejected", '', 'normal')
                setTimeout(function () {
                    close_toast_popup(url + 'Receive_products?status=pending')

                }, 2000)

            } else {

                showMsgPopup("Error", "Something Went Wrong Try Again", '', 'error')
                setTimeout(function () {
                    close_toast_popup(url + 'Corporate_Process')

                }, 2000)
            }
        },
        error: function (jqXHR, textStatus, errorThrown)
        {

        }
    });
}
function accept_m_qc() {
    var conf = confirm("Are you sure you want to accept?");
    if (!conf)
    {
        return false;
    }
    if ($('#sub_catagory input[type="checkbox"]').not(':checked').length == 0) {
    } else {
        showMsgPopup("Error", "Please check all Parameters", '', 'error')
        // setTimeout(function(){
        //                 close_toast_popup(location);
        //                 },2000)
        //alert('Please check all Parameters');
        return false;
    }
    $.ajax({
        url: url + "Manufacturing_quality_control/store",
        type: "POST",
        dataType: "json",
        data: $('form#qc_check').serialize(),
        success: function (data, textStatus, jqXHR)
        {
            if (data.status == 'failure')
            {
                error_msg(data.error);

            } else if (data.status == "success") {
                showMsgPopup("Success", "Successfully Updated", '', 'normal')
                setTimeout(function () {
                    close_toast_popup(url + 'Manufacturing_Dashboard')

                }, 2000)

            } else {

                showMsgPopup("Error", "Something Went Wrong Try Again", '', 'error')
                setTimeout(function () {
                    close_toast_popup(url + 'Manufacturing_Dashboard')

                }, 2000)
            }
        },
        error: function (jqXHR, textStatus, errorThrown)
        {

        }
    });
}
function reject_m_qc() {
    var conf = confirm("Are you sure you want to reject?");
    if (!conf)
    {
        return false;
    }
    $.ajax({
        url: url + "Manufacturing_quality_control/reject",
        type: "POST",
        dataType: "json",
        data: $('form#qc_check').serialize(),
        success: function (data, textStatus, jqXHR)
        {
            if (data.status == 'failure')
            {
                error_msg(data.error);

            } else if (data.status == "success") {
                showMsgPopup("Success", "Successfully Updated", '', 'normal')
                setTimeout(function () {
                    close_toast_popup(url + 'Manufacturing_Dashboard')

                }, 2000)

            } else {

                showMsgPopup("Error", "Something Went Wrong Try Again", '', 'error')
                setTimeout(function () {
                    close_toast_popup(url + 'Manufacturing_Dashboard')

                }, 2000)
            }
        },
        error: function (jqXHR, textStatus, errorThrown)
        {

        }
    });
}
function amend_qc() {
    var conf = confirm("Are you sure you want to amend?");
    if (!conf)
    {
        return false;
    }
    $.ajax({
        url: url + "Quality_control/amend",
        type: "POST",
        dataType: "json",
        data: $('form#qc_check').serialize(),
        success: function (data, textStatus, jqXHR)
        {
            if (data.status == 'failure')
            {
                error_msg(data.error);

            } else if (data.status == "success") {
                showMsgPopup("Success", "successfully Amended", '', 'normal')
                setTimeout(function () {
                    close_toast_popup(url + 'Receive_products?status=pending')

                }, 2000)

            } else {

                showMsgPopup("Error", "Something Went Wrong Try Again", '', 'error')
                setTimeout(function () {
                    close_toast_popup(url + 'Corporate_Process')

                }, 2000)
            }
        },
        error: function (jqXHR, textStatus, errorThrown)
        {

        }
    });
}
function Reject_design(id) {
    $.ajax({
        url: url + "Corporate_product/reject",
        type: "POST",
        dataType: "json",
        data: {id: id},
        success: function (data, textStatus, jqXHR)
        {
            if (data == true)
            {
                showMsgPopup("Success", "Product Approved", '', 'normal');
                //close_toast_popup(document.referrer);
                close_toast_popup(url + 'Corporate_Process');

            } else if (data.status == "success") {
                //showMsgPopup("Success","Successfully Added",'','normal')
                close_toast_popup(url + 'Corporate_Process');
            } else {

                showMsgPopup("Error", "Something Went Wrong Try Again", '', 'error')

            }
        },
        error: function (jqXHR, textStatus, errorThrown)
        {

        }
    });
}

function Create_Stickers()
{
    $.ajax({
        url: url + "Print_Stickers/store",
        type: "POST",
        dataType: "json",
        data: $('form#CreateStickersForm').serialize(),
        success: function (data, textStatus, jqXHR)
        {
            if (data.status == 'failure')
            {
                error_msg(data.error);

            } else if (data.status == "success") {
                showMsgPopup("Success", "successfully saved", '', 'normal')
                var purity_value = $("#purity_input").val();
                $('.print_area').css('display', 'block');
                $('.input_data').css('display', 'none');
                $("#purity").html(purity_value);

            } else {

                showMsgPopup("Error", "Something Went Wrong Try Again", '', 'error')
                setTimeout(function () {
                    //close_toast_popup(url+'Add_Cars/brand')

                }, 2000)
            }
        },
        error: function (jqXHR, textStatus, errorThrown)
        {

        }
    });
    /*var purity_value = $("#purity_input").val();
     $('.print_area').css('display','block');
     $('.input_data').css('display','none');
     $("#purity").html(purity_value);*/
}
function multiple_send_to_hallmarking() {

    $.ajax({
        url: url + "Quality_control/send_to_hallmarking",
        type: "POST",
        dataType: "json",
        data: $('form#export').serialize(),
        success: function (data, textStatus, jqXHR)
        {
            if (data.status == 'failure')
            {
                showMsgPopup("Error", data.error, '', 'error')


            } else if (data.status == "success") {
                $('.send_to_hm').attr('disabled', true);
                showMsgPopup("Success", "Successfully sent to hallmarking", '', 'normal')
                setTimeout(function () {
                    close_toast_popup(url + 'Quality_control?status=complete')

                }, 600)


            } else {
                showMsgPopup("Error", "Something Went Wrong Try Again", '', 'error')
                setTimeout(function () {
                    close_toast_popup(url + 'Corporate_Process')

                }, 2000)
            }

        },
        error: function (jqXHR, textStatus, errorThrown)
        {

        }
    });
}

function send_to_hallmarking(qc_id) {
    $('.text-danger').html('');
    var hc_name = $('#hc_' + qc_id).val();
    if (hc_name == "" || hc_name == undefined) {
        $('#hc_error_' + qc_id).html('Please Select Hallmarking Center');
    } else {
        $.ajax({
            url: url + "Quality_control/send_to_hallmarking/" + qc_id,
            type: "POST",
            dataType: "json",
            data: {hc_id: hc_name},
            success: function (data, textStatus, jqXHR)
            {
                if (data.status == 'failure')
                {
                    error_msg(data.error);

                } else if (data.status == "success") {
                    showMsgPopup("Success", "Successfully Updated", '', 'normal')
                    setTimeout(function () {
                        close_toast_popup(url + 'Quality_control?status=complete')

                    }, 2000)

                } else {

                    showMsgPopup("Error", "Something Went Wrong Try Again", '', 'error')
                    setTimeout(function () {
                        close_toast_popup(url + 'Corporate_Process')

                    }, 2000)
                }
            },
            error: function (jqXHR, textStatus, errorThrown) {

            }
        });
    }
}
function manufacturing_send_to_hallmarking(qc_id) {
    $.ajax({
        url: url + "Manufacturing_quality_control/send_to_hallmarking/" + qc_id,
        type: "POST",
        dataType: "json",
        success: function (data, textStatus, jqXHR)
        {
            if (data.status == 'failure')
            {
                error_msg(data.error);

            } else if (data.status == "success") {
                showMsgPopup("Success", "Successfully Updated", '', 'normal')
                setTimeout(function () {
                    close_toast_popup(url + 'Manufacturing_Dashboard')

                }, 2000)

            } else {

                showMsgPopup("Error", "Something Went Wrong Try Again", '', 'error')
                setTimeout(function () {
                    close_toast_popup(url + 'Manufacturing_Dashboard')

                }, 2000)
            }
        },
        error: function (jqXHR, textStatus, errorThrown)
        {

        }
    });
}
function export_products_with_image()
{
    $('div.dataTables_filter input').addClass('searchData');
    var searchText = $('.searchData').val();
    window.location = url + "Design/export_products_with_image/" + searchText;
}
function export_products_without_image()
{
    $('div.dataTables_filter input').addClass('searchData');
    var searchText = $('.searchData').val();
    window.location = url + "Design/export_products_without_image/" + searchText;
}
function export_hallmarking_accepted_products()
{
    $('div.dataTables_filter input').addClass('searchData');
    var searchText = $('.searchData').val();
    window.location = url + "Hallmarking/export/" + searchText;
}
function export_qc_accepted_products()
{
    $('div.dataTables_filter input').addClass('searchData');
    var searchText = $('.searchData').val();
    window.location = url + "Quality_control/export/" + searchText;
}
/*function export_all_amended_products()
 {
 $('div.dataTables_filter input').addClass('searchData');
 var searchText = $('.searchData').val();
 //alert(searchText);
 window.location=url+"Amend_products/export/"+searchText; 
 }*/

function export_all_amended_products()
{
    $.ajax({
        url: url + "Amend_products/export",
        type: "POST",
        dataType: "json",
        data: $('form#amend_products').serialize(),
        success: function (data, textStatus, jqXHR)
        {
            //console.log(data)
            close_toast_popup(data);
            //close_toast_popup(url+'Corporate_Dashboard')

        },
        error: function (jqXHR, textStatus, errorThrown)
        {

        }
    });
}
function send_to_prepare_order(corporate_id)
{
    $.ajax({
        url: url + "Quality_control/send_to_prepare_order/" + corporate_id,
        type: "POST",
        dataType: "json",
        success: function (data, textStatus, jqXHR)
        {
            if (data.status == 'failure')
            {
                error_msg(data.error);

            } else if (data.status == "success") {
                showMsgPopup("Success", "Successfully Updated", '', 'normal')
                setTimeout(function () {
                    close_toast_popup(url + 'Corporate_Process')

                }, 2000)

            } else {

                showMsgPopup("Error", "Something Went Wrong Try Again", '', 'error')
                setTimeout(function () {
                    close_toast_popup(url + 'Corporate_Process')

                }, 2000)
            }
        },
        error: function (jqXHR, textStatus, errorThrown)
        {

        }
    });
}

function export_qc_products()
{
    $.ajax({
        url: url + "Quality_control/export",
        type: "POST",
        dataType: "json",
        data: $('form#export').serialize(),
        success: function (data, textStatus, jqXHR)
        {
            //console.log(data)
            close_toast_popup(data);
            //close_toast_popup(url+'Corporate_Dashboard')

        },
        error: function (jqXHR, textStatus, errorThrown)
        {

        }
    });
}
function generate_packing_list()
{
    var formData = new FormData(document.querySelector("form#export"));
    formData.append('search', $('.input-sm').val());
    $.ajax({
        url: url + "Generate_packing_list/store",
        type: "POST",
        dataType: "json",
        data: formData,
        processData: false,
        contentType: false,
        success: function (data, textStatus, jqXHR)
        {
            if (data.status == "success") {
                showMsgPopup("Success", "Successfully Generated Packing List", '', 'normal')
                setTimeout(function () {
                    close_toast_popup(url + 'Corporate_Process')

                }, 2000)

            } else if (data.status == "failure") {
                error_msg(data.error);
            } else {

                showMsgPopup("Error", "Please Select Products", '', 'error')

            }

        },
        error: function (jqXHR, textStatus, errorThrown)
        {

        }
    });
}

function ready_product_images(id)
{
    var formData = new FormData(document.querySelector("form#received_order_images"));
   // formData.append('search', $('.input-sm').val());
    $.ajax({
        url: url + "Customer_order/received_order/upload_images",
        type: "POST",
        dataType: "json",
        data: formData,
        processData: false,
        contentType: false,
        success: function (data, textStatus, jqXHR)
        {
            if (data.status == "success") {
                showMsgPopup("Success", "Successfully Uploded", '', 'normal')
                setTimeout(function () {
                    close_toast_popup(url + 'Customer_order/received_order/add_image/'+id)

                }, 2000)

            } else if (data.status == "failure") {
                error_msg(data.error);
            } else {

                showMsgPopup("Error", data.msg, '', 'error')

            }

        },
        error: function (jqXHR, textStatus, errorThrown)
        {

        }
    });
}
if (page_title == 'Generate Packing List') {
    function pagination1(page_count2, object1, filter) {
        console.log('dasdsa', object1);
        $('.ajaxLoader').show();
        $.ajax({
            url: url + "Generate_packing_list/search",
            type: "POST",
            dataType: "json",
            data: {offset: page_count2, filter: object1},
            success: function (data, textStatus, jqXHR)
            {

                var html = '';
                if (data !== '') {
                    page_count_pl = data.page_count2;
                    load_more_pl = 0;
                    $.each(data.result, function (key, val) {
                        var psize = val.size;
                        if (psize == null || psize == '') {
                            psize = '-';
                        }
                        if (val.product_code !== '' || typeof val.product_code !== "undefined") {
                            html += '<div style="padding-top: 10px;" class="col-lg-6 col-md-6 col-sm-6 col-xs-12">';
                            html += '<div class="col-lg-5 check-box">'
                            html += '<img class="img-responsive img-rounded" src="' + url + 'uploads/product/' + val.product_code + '/small/' + val.image + '">'
                            if (val.selected == '1') {
                                html += '<div class="PREPARE_check checkbox checkbox-purpal"><input data-parsley-multiple="group1" data-parsley-id="76" type="checkbox" name="products[]"  value="' + val.qc_id + '" class="from-control" checked><label for="PREPARE_check"></label></div>'
                            } else {
                                html += '<div class="PREPARE_check checkbox checkbox-purpal"><input data-parsley-multiple="group1" data-parsley-id="76" type="checkbox" name="products[]"  value="' + val.qc_id + '" class="from-control"><label for="PREPARE_check"></label></div>'
                            }
                            html += '</div>'
                            html += '<div class="col-lg-6">'
                            html += '<p>Order Id : ' + val.order_id + '</p>'
                            html += '<p>Product Code : ' + val.product_code + '</p>'
                            html += '<p>Pair/Pieces : ' + val.qc_quantity + '</p>'
                            html += '<p>Size/Length : ' + psize + '</p>'
                            html += '<p>Weight Range : ' + val.from_weight + ' ' + val.to_weight + '</p>';

                            html += '</div>'
                            html += '</div>'
                        }
                    })
                }
                if (page_count2 == '0') {
                    $('.load_html').html('');
                    $('.load_html').html(html);
                } else {
                    $('.load_html').append(html);
                }
            },
            error: function (jqXHR, textStatus, errorThrown)
            {

            }
        });
        $('.ajaxLoader').hide();
    }
}
function Generate_packing_list() {
    $.ajax({
        url: url + "Generate_packing_list/store",
        type: "POST",
        dataType: "json",
        data: $('form#packingListForm').serialize(),
        success: function (data, textStatus, jqXHR)
        {
            if (data.status == 'failure')
            {
                showMsgPopup("Error", 'Something Went Wrong Try Again', '', 'error')
                excel_error_msg(data.error);

                //showMsgPopup("Error",data.error,'','error')

            } else if (data.status == "success") {
                showMsgPopup("Success", "Successfully Added", '', 'normal')
                setTimeout(function () {
                    close_toast_popup(url + 'Generate_packing_list')

                }, 2000)
            } else {
                showMsgPopup("Error", "Something Went Wrong Try Again", '', 'error')
                setTimeout(function () {
                    // close_toast_popup(url+'Prepare_order')

                }, 2000)
            }
        },
        error: function (jqXHR, textStatus, errorThrown)
        {

        }
    });
}
/*function export_hm_export_products(){
 var product_id = $('#add_selected_hallmark').val();
 alert(product_id);
 window.location=url+"Hallmarking/export_accepted/"+product_id;
 }*/

function export_hm_export_products()
{
    $.ajax({
        url: url + "Hallmarking/export_accepted",
        type: "POST",
        dataType: "json",
        data: $('form#export').serialize(),
        success: function (data, textStatus, jqXHR)
        {
            //console.log(data)
            close_toast_popup(data);
            //close_toast_popup(url+'Corporate_Dashboard')

        },
        error: function (jqXHR, textStatus, errorThrown)
        {

        }
    });
}
function hm_barcode_products()
{
    $.ajax({
        url: url + "Hallmarking/barcode_products",
        type: "POST",
        dataType: "json",
        data: $('form#export').serialize(),
        success: function (data, textStatus, jqXHR)
        {
            //console.log(data.hm_detaile)
            //close_toast_popup(data);
            if (data.status == "success") {
                window.open(url+"Hallmarking/barcode_products_print", "_blank")
            
            }else{
                close_toast_popup(url+'Hallmarking?status=export');
            }

        },
        error: function (jqXHR, textStatus, errorThrown)
        {
            close_toast_popup(url+'Hallmarking?status=export');
        }
    });
}


function export_hm_products()
{
    var object = $("form#export").serializeArray();
    $.ajax({
        url: url + "Hallmarking/export",
        type: "POST",
        dataType: "json",
        data: {product_code: object, search: $('.input-sm').val(), selected_products: $('#add_selected_hallmark').val()},
        success: function (data, textStatus, jqXHR)
        {
            //console.log(data)
            if (data.status == "failure") {
                $('.export_hallmark_error').html(data.msg);
            } else {
                //location.reload(true);

                close_toast_popup(data);
                //close_toast_popup(url+'Hallmarking?status=receive');
            }



        },
        error: function (jqXHR, textStatus, errorThrown)
        {

        }
    });
}

function send_to_relience(id)
{
    $.ajax({
        url: url + "Hallmarking/send_to_relience/" + id,
        type: "POST",
        dataType: "json",
        data: {id: id},
        success: function (data, textStatus, jqXHR)
        {
            if (data.status == "success") {
                showMsgPopup("Success", "Product Sent To Relience Jewels", '', 'normal')
                close_toast_popup(document.referrer);
            } else {

                showMsgPopup("Error", "Something Went Wrong Try Again", '', 'error')

            }
        },
        error: function (jqXHR, textStatus, errorThrown)
        {

        }
    });
}
function Approve_products_by_relience(id) {
    $.ajax({
        url: url + "Stock/approve",
        type: "POST",
        dataType: "json",
        data: {id: id},
        success: function (data, textStatus, jqXHR)
        {
            if (data == true)
            {
                showMsgPopup("Success", "Product Approved", '', 'normal');
                close_toast_popup(document.referrer);
            } else if (data.status == "success") {
                //showMsgPopup("Success","Successfully Added",'','normal')
            } else {

                showMsgPopup("Error", "Something Went Wrong Try Again", '', 'error')

            }
        },
        error: function (jqXHR, textStatus, errorThrown)
        {

        }
    });
}
function reject_product_by_relience(id) {
    $.ajax({
        url: url + "Stock/reject",
        type: "POST",
        dataType: "json",
        data: {id: id},
        success: function (data, textStatus, jqXHR)
        {
            if (data == true)
            {
                showMsgPopup("Success", "Product Rejected", '', 'normal');
                close_toast_popup(document.referrer);

            } else if (data.status == "success") {
                //showMsgPopup("Success","Successfully Added",'','normal')
            } else {

                showMsgPopup("Error", "Something Went Wrong Try Again", '', 'error')

            }
        },
        error: function (jqXHR, textStatus, errorThrown)
        {

        }
    });
}
function mark_as_amend(receive_product_id) {
    $.ajax({
        url: url + "Amend_products/mark_as_amend/" + receive_product_id,
        type: "POST",
        dataType: "json",
        success: function (data, textStatus, jqXHR)
        {
            if (data.status == 'failure')
            {
                error_msg(data.error);

            } else if (data.status == "success") {
                showMsgPopup("Success", "Successfully Updated", '', 'normal')
                setTimeout(function () {
                    close_toast_popup(url + 'Corporate_Process')

                }, 2000)

            } else {

                showMsgPopup("Error", "Something Went Wrong Try Again", '', 'error')
                setTimeout(function () {
                    close_toast_popup(url + 'Corporate_Process')

                }, 2000)
            }
        },
        error: function (jqXHR, textStatus, errorThrown)
        {

        }
    });
}
function accept_m_hallmarking_qc() {
    var conf = confirm("Are you sure you want to accept?");
    if (!conf)
    {
        return false;
    }
    if ($('#sub_catagory input[type="checkbox"]').not(':checked').length == 0) {
    } else {
        showMsgPopup("Error", "Please check all Parameters", '', 'error')
        // setTimeout(function(){
        //                 close_toast_popup(location);
        //                 },2000)
        //alert('Please check all Parameters');
        return false;
    }
    $.ajax({
        url: url + "Manufacturing_hallmarking/store",
        type: "POST",
        dataType: "json",
        data: $('form#qc_check').serialize(),
        success: function (data, textStatus, jqXHR)
        {
            if (data.status == 'failure')
            {
                error_msg(data.error);

            } else if (data.status == "success") {
                showMsgPopup("Success", "Successfully Updated", '', 'normal')
                setTimeout(function () {
                    close_toast_popup(url + 'Manufacturing_Dashboard')

                }, 2000)

            } else {

                showMsgPopup("Error", "Something Went Wrong Try Again", '', 'error')
                setTimeout(function () {
                    // close_toast_popup(url+'Corporate_Dashboard')

                }, 2000)
            }
        },
        error: function (jqXHR, textStatus, errorThrown)
        {

        }
    });
}
function accept_hallmarking_qc() {
    var conf = confirm("Are you sure you want to accept?");
    if (!conf)
    {
        return false;
    }
    if ($('#sub_catagory input[type="checkbox"]').not(':checked').length == 0) {
    } else {
        showMsgPopup("Error", "Please check all Parameters", '', 'error')
        // setTimeout(function(){
        //                 close_toast_popup(location);
        //                 },2000)
        //alert('Please check all Parameters');
        return false;
    }
    $.ajax({
        url: url + "Hallmarking/store",
        type: "POST",
        dataType: "json",
        data: $('form#qc_check').serialize(),
        success: function (data, textStatus, jqXHR)
        {
            if (data.status == 'failure')
            {
                error_msg(data.error);

            } else if (data.status == "success") {
                showMsgPopup("Success", "Successfully Updated", '', 'normal')
                setTimeout(function () {
                    close_toast_popup(url + 'Hallmarking?status=send')

                }, 2000)

            } else {

                showMsgPopup("Error", "Something Went Wrong Try Again", '', 'error')
                setTimeout(function () {
                    // close_toast_popup(url+'Corporate_Dashboard')

                }, 2000)
            }
        },
        error: function (jqXHR, textStatus, errorThrown)
        {

        }
    });
}

function export_caratlane(order_id)
{

    $.ajax({
        url: url + "Generate_packing_list/export",
        type: "POST",
        dataType: "json",
        data: {order_id: order_id},
        success: function (data, textStatus, jqXHR)
        {
            //console.log(data)
            close_toast_popup(data);
        },
        error: function (jqXHR, textStatus, errorThrown)
        {

        }
    });
}

function Send_hm_rejected_products(hm_id) {
    $.ajax({
        url: url + "Hallmarking/Send_rejected_products/" + hm_id,
        type: "POST",
        dataType: "json",
        success: function (data, textStatus, jqXHR)
        {
            if (data.status == 'failure')
            {
                showMsgPopup("Error", 'Something Went Wrong Try Again', '', 'error')

            } else if (data.status == "success") {
                showMsgPopup("Success", "Product Successfully Sent to Karigar", '', 'normal')
                setTimeout(function () {
                    close_toast_popup(url + 'Print_Order/index/' + data.karigar_id)

                }, 2000)

            } else {

                showMsgPopup("Error", "Something Went Wrong Try Again", '', 'error')
                setTimeout(function () {
                    close_toast_popup(url + 'Prepare_order')

                }, 2000)
            }

        },
        error: function (jqXHR, textStatus, errorThrown)
        {

        }
    });
}
function Send_rejected_products(id) {
    $('#b_' + id).attr('disabled', true);
    $.ajax({
        url: url + "Quality_control/Send_rejected_products/" + id,
        type: "POST",
        dataType: "json",
        success: function (data, textStatus, jqXHR)
        {
            if (data.status == 'failure')
            {
                showMsgPopup("Error", 'Something Went Wrong Try Again', '', 'error')

            } else if (data.status == "success") {
                showMsgPopup("Success", "successfully resent to karigar", '', 'normal')
                setTimeout(function () {
                    close_toast_popup(url + 'Print_Order/index/' + data.karigar_id)

                }, 2000)

            } else {

                showMsgPopup("Error", "Something Went Wrong Try Again", '', 'error')
                /* setTimeout(function(){
                 close_toast_popup(url+'Prepare_order')
                 
                 },2000)*/
            }

        },
        error: function (jqXHR, textStatus, errorThrown)
        {

        }
    });
}




function Send_rejected_products_to_stock(id) {
    $('#b_' + id).attr('disabled', true);
    $.ajax({
        url: url + "Quality_control/Send_rejected_products/" + id,
        type: "POST",
        dataType: "json",
        success: function (data, textStatus, jqXHR)
        {
            if (data.status == 'failure')
            {
                showMsgPopup("Error", 'Something Went Wrong Try Again', '', 'error')

            } else if (data.status == "success") {
                showMsgPopup("Success", "successfully resent to karigar", '', 'normal')
                setTimeout(function () {
                    close_toast_popup(url + 'Print_Order/index/' + data.karigar_id)

                }, 2000)

            } else {

                showMsgPopup("Error", "Something Went Wrong Try Again", '', 'error')
                /* setTimeout(function(){
                 close_toast_popup(url+'Prepare_order')
                 
                 },2000)*/
            }

        },
        error: function (jqXHR, textStatus, errorThrown)
        {

        }
    });
}


function reject_hallmarking_qc() {
    var conf = confirm("Are you sure you want to reject?");
    if (!conf)
    {
        return false;
    }
    $.ajax({
        url: url + "Hallmarking/reject",
        type: "POST",
        dataType: "json",
        data: $('form#qc_check').serialize(),
        success: function (data, textStatus, jqXHR)
        {
            if (data.status == 'failure')
            {
                error_msg(data.error);

            } else if (data.status == "success") {
                showMsgPopup("Success", "Successfully Updated", '', 'normal')
                setTimeout(function () {
                    close_toast_popup(url + 'Hallmarking?status=send')

                }, 2000)

            } else {

                showMsgPopup("Error", "Something Went Wrong Try Again", '', 'error')
                setTimeout(function () {
                    close_toast_popup(url + 'Corporate_Process')

                }, 2000)
            }
        },
        error: function (jqXHR, textStatus, errorThrown)
        {

        }
    });
}
function reject_m_hallmarking_qc() {
    var conf = confirm("Are you sure you want to reject?");
    if (!conf)
    {
        return false;
    }
    $.ajax({
        url: url + "Manufacturing_hallmarking/reject",
        type: "POST",
        dataType: "json",
        data: $('form#qc_check').serialize(),
        success: function (data, textStatus, jqXHR)
        {
            if (data.status == 'failure')
            {
                error_msg(data.error);

            } else if (data.status == "success") {
                showMsgPopup("Success", "Successfully Updated", '', 'normal')
                setTimeout(function () {
                    close_toast_popup(url + 'Manufacturing_Dashboard')

                }, 2000)

            } else {

                showMsgPopup("Error", "Something Went Wrong Try Again", '', 'error')
                setTimeout(function () {
                    close_toast_popup(url + 'Manufacturing_Dashboard')

                }, 2000)
            }
        },
        error: function (jqXHR, textStatus, errorThrown)
        {

        }
    });
}


function Send_amend_products(am_id) {
    $('#b_' + am_id).attr('disabled', true);
    $.ajax({
        url: url + "Amend_products/send_to_karigar/" + am_id,
        type: "POST",
        dataType: "json",
        success: function (data, textStatus, jqXHR)
        {
            if (data.status == 'failure')
            {
                showMsgPopup("Error", 'Something Went Wrong Try Again', '', 'error')

            } else if (data.status == "success") {
                showMsgPopup("Success", "Product Successfully Sent to Karigar", '', 'normal')
                setTimeout(function () {
                    close_toast_popup(url + 'Print_Order/index/' + data.karigar_id)

                }, 2000)

            } else {

                showMsgPopup("Error", "Something Went Wrong Try Again", '', 'error')
                setTimeout(function () {
                    close_toast_popup(url + 'Amend_products/show/' + data.karigar_id)

                }, 2000)
            }

        },
        error: function (jqXHR, textStatus, errorThrown)
        {

        }
    });
}
function send_caratlane_rejected_products(gp_id) {
    $('#b_' + gp_id).attr('disabled', true);
    $.ajax({
        url: url + "Generate_packing_list/send_to_karigar/" + gp_id,
        type: "POST",
        dataType: "json",
        success: function (data, textStatus, jqXHR)
        {
            if (data.status == 'failure')
            {
                showMsgPopup("Error", 'Something Went Wrong Try Again', '', 'error')

            } else if (data.status == "success") {
                showMsgPopup("Success", "Product Successfully Sent to Karigar", '', 'normal')
                setTimeout(function () {
                    close_toast_popup(url + 'Print_Order/index/' + data.karigar_id)

                }, 2000)

            } else {

                showMsgPopup("Error", "Something Went Wrong Try Again", '', 'error')
                setTimeout(function () {
                    close_toast_popup(url + 'Prepare_order')

                }, 2000)
            }

        },
        error: function (jqXHR, textStatus, errorThrown)
        {

        }
    });
}

/*added to stock*/
function qc_rejected_add_to_stock(qc_id) {
    $('#s_' + qc_id).attr('disabled', true);
    // return false;

    $.ajax({
        url: url + "Quality_control/Send_rejected_products_to_stock/" + qc_id,
        type: "POST",
        dataType: "json",
        success: function (data, textStatus, jqXHR)
        {
            if (data.status == 'failure')
            {
                showMsgPopup("Error", 'Something Went Wrong Try Again', '', 'error')

            } else if (data.status == "success") {

                showMsgPopup("Success", "Product successfully add to stock", '', 'normal')
                setTimeout(function () {
                    close_toast_popup(url + 'Quality_control?status=rejected')

                }, 2000)

            } else {

                showMsgPopup("Error", "Something Went Wrong Try Again", '', 'error')
                setTimeout(function () {
                    close_toast_popup(url + 'Quality_control?status=rejected')

                }, 2000)
            }

        },
        error: function (jqXHR, textStatus, errorThrown)
        {

        }
    });
}
function caratlane_rejected_add_to_stock(qc_id) {
    $('#s_' + qc_id).attr('disabled', true);
    // return false;

    $.ajax({
        url: url + "Generate_packing_list/send_to_stock/" + qc_id,
        type: "POST",
        dataType: "json",
        success: function (data, textStatus, jqXHR)
        {
            if (data.status == 'failure')
            {
                showMsgPopup("Error", 'Something Went Wrong Try Again', '', 'error')

            } else if (data.status == "success") {

                showMsgPopup("Success", "Product successfully add to stock", '', 'normal')
                setTimeout(function () {
                    close_toast_popup(url + '/Generate_packing_list/show/' + data.order_no + '?status=rejected')

                }, 2000)

            } else {

                showMsgPopup("Error", "Something Went Wrong Try Again", '', 'error')
                setTimeout(function () {
                    close_toast_popup(url + '/Generate_packing_list/show/' + data.order_no + '?status=rejected')

                }, 2000)
            }

        },
        error: function (jqXHR, textStatus, errorThrown)
        {

        }
    });
}


function save_minimum_stock() {
    $('.text-danger').html('');
    $.ajax({
        url: url + "Sub_category/save_minimum_stock",
        type: "POST",
        dataType: "json",
        data: $('form#minimum_stock').serialize(),
        success: function (data, textStatus, jqXHR)
        {
            if (data.status == 'failure')
            {
                error_msg(data.error);

            } else if (data.status == "success") {
                showMsgPopup("Success", "Successfully Updated", '', 'normal')
                setTimeout(function () {
                    close_toast_popup(url + 'Sub_category')

                }, 2000)

            } else {

                showMsgPopup("Error", "Something Went Wrong Try Again", '', 'error');
            }

        },
        error: function (jqXHR, textStatus, errorThrown)
        {

        }
    });
}
var valid_data_mfg_order = true;
function add_order() {
    if (valid_data_mfg_order == true) {
        valid_data_mfg_order = false;
        validate_department_order();
    }
}
function save_order() {

    $('#save_order').attr('disabled', true);
    var URL = url + "Manufacturing_department_order/store";
    if ($('#order_id').val() != '') {
        URL = url + "Manufacturing_department_order/store/" + $('#order_id').val()
    }

    $.ajax({
        url: URL,
        type: "POST",
        dataType: "json",
        data: {order_data: order_object, id: $('#order_id').val()},
        success: function (data, textStatus, jqXHR)
        {
            if (data.status == 'failure')
            {
                $('#save_order').attr('disabled', false);
                error_msg(data.error);

            } else if (data.status == "success") {
                showMsgPopup("Success", "Successfully Submitted", '', 'normal')

                setTimeout(function () {
                    close_toast_popup(url + 'Manufacturing_department_order/view/' + data.order_id)

                }, 1000)

            } else {
                $('#save_order').attr('disabled', false);
                showMsgPopup("Error", "Something Went Wrong Try Again", '', 'error');
            }

        },
        error: function (jqXHR, textStatus, errorThrown)
        {

        }
    });
}
function validate_department_order() {
    var is_exist = false;
    $('.text-danger').html('');
    $.each(order_object, function (key, val) {
        if (($('.sub_category').val() == val['sub_category']) && ($('.weight_range').val() == val['weight_range_id'])) {
            // $('#common_error').html('Parent category '+$('.sub_category :selected').text()+' Already exist');
            $('#common_error').html('Product  Already exist');
            is_exist = true;

        }
    })
    if (is_exist == true) {
        valid_data_mfg_order = true;
        return false;
    } else {

        $.ajax({
            url: url + "Manufacturing_department_order/validate_order_sheet",
            type: "POST",
            dataType: "json",
            data: $('form#department_order').serialize(),
            success: function (data, textStatus, jqXHR)
            {
                valid_data_mfg_order = true;
                if (data.status == 'failure')
                {
                    error_msg(data.error);

                } else if (data.status == "success") {
                    create_order_table();

                } else {

                    showMsgPopup("Error", "Something Went Wrong Try Again", '', 'error');
                }

            },
            error: function (jqXHR, textStatus, errorThrown)
            {

            }
        });
    }
}
function create_order_table() {
    var str = $('.weight_range :selected').text();

    if ($('.weight_range').val()) {
        var res = str.split("-");
        var min_weight = (parseFloat(res[0]) + parseFloat(res[1])) / 2;
        var qnt = parseInt($('.quantity').val());
        var wt = parseInt($('.weight').val());
        var dep_id = $('.dep_id').val();
        var display_name = '';
        if (dep_id == '2' || dep_id == '3' || dep_id =='6' || dep_id =='10') {
            //display_name = 'weight';
            total_wt_d = total_wt_d + wt
        }else if (dep_id == '8') {
            //display_name = 'quantity';
            var appx_wt = min_weight * qnt;
            total_wt = total_wt + appx_wt
            total_qtn = total_qtn + qnt
            total_wt_d = total_wt_d + wt
        }else{
            //display_name = 'quantity';
            var appx_wt = min_weight * qnt;
            total_wt = total_wt + appx_wt
            total_qtn = total_qtn + qnt
        }

        var obj = {};
        var variant_select = '';
        var variant = '';
        $(".variant_select > option:selected").each(function () {
            variant_select += $(this).text() + ",";

        });
        var variant = $.trim(variant_select);
        var lastChar = variant.slice(-1);
        if (lastChar == ',') {
            variant = variant.slice(0, -1);
        }
        var table = '<tr id="id_' + tr_count + '">';
        table += '<td>' + $('.sub_category :selected').text() + '</td>';
        table += '<td>' + variant + '</td>';
        table += '<td>' + $('.weight_range :selected').text() + '</td>';
        if (dep_id == '2' || dep_id == '3' || dep_id =='6' || dep_id =='10') {
            table += '<td>' + $('.weight').val() + '</td>';
          
        }else if (dep_id == '8') {
            table += '<td>' + $('.weight').val() + '</td>';
            table += '<td>' + $('.quantity').val() + '</td>';
             table += '<td>' + appx_wt + '</td>';
            
        }else{
            table += '<td>' + $('.quantity').val() + '</td>';
             table += '<td>' + appx_wt + '</td>';
        }
     /*   table += '<td>' + $('.' + display_name).val() + '</td>';*/
      table += '<td><button onclick="remove_order(' + tr_count + ')" type="button" name="commit" class="btn btn-danger small loader-hide  btn-sm">REMOVE</button></td>';
  /*      if (dep_id == '2' || dep_id == '8') {
            table += '<td><button onclick="remove_order(' + tr_count + ')" type="button" name="commit" class="btn btn-danger small loader-hide  btn-sm">REMOVE</button></td>';
        }else{
             table += '<td>' + appx_wt + '</td>';
            table += '<td><button onclick="remove_order(' + tr_count + ')" type="button" name="commit" class="btn btn-danger small loader-hide  btn-sm">REMOVE</button></td>';
        }*/

        //console.log(table)
        obj['sub_category'] = $('.sub_category').val();
        obj['order_name'] = $('.order_name').val();
        obj['department_id'] = $(".sel_department").val();
        obj['weight_range_id'] = $('.weight_range').val();
        obj['quantity'] = $('.quantity').val();
        obj['weight'] = $('.weight').val();
        // obj[display_name] = $('.' + display_name).val();


        //obj['quantity'] = $('.quantity').val();
        obj['order_date'] = $('.order_date').val();
        obj['tr_count'] = tr_count;
        obj['appx_wt'] = appx_wt;
        obj['variant'] = $('.variant_select').val();
        var sub_cat_id_str = "";
        $('input[name="sub_cat_image[]"]:checked').each(function () {
            sub_cat_id_str += $(this).val() + "#";
        });
        obj['sub_cat_img_id'] = sub_cat_id_str;
        order_object.push(obj);
        $('.order_params').remove();
        table += '<tr class="order_params">';
        table += '<td>Total</td>';
        table += '<td></td>';
        table += '<td></td>';
        // table += '<td><span id="total_qtn">Total ' + display_name + ': ' + total_qtn + '</span></td>';

        if (dep_id == '2' || dep_id == '3' || dep_id =='6' || dep_id =='10' ) {
            table += '<td><span id="total_qtn">Total  weight : ' + total_wt_d + '</span></td>';
          
        }else if (dep_id == '8') {
            table += '<td><span id="total_qtn">Total  weight : ' + total_wt_d + '</span></td>';
            table += '<td><span id="total_qtn">Total  quantity : ' + total_qtn + '</span></td>';
             
        }else{
            table += '<td><span id="total_qtn">Total  quantity : ' + total_qtn + '</span></td>';            
            
        }

            table += '<td></td>';

 /*       if (dep_id == '2' || dep_id == '8') {
            table += '<td></td>';
        }else{
                table += '<td class="approx_Wt"><span id="total_weight">Total Weight: ' + total_wt + '</span></td>';
            table += '<td></td>'
        }
*/
        table += '</tr>';
        if ($('#department_order_table tbody tr').length > 1 || tr_count >= 1) {
            tr_count++;
            $('#department_order_table tbody').append(table);
        } else {
            $('#button_save').show();
            $('#order_preview').show();
            $('.order_params').show();
            tr_count = 1;
            $('#department_order_table tbody').html(table);
        }
        showMsgPopup("Success", "Successfully Added", '', 'normal')
        //reset subcategy form feilds
        $(".sub_category").val("");
        $(".weight_range").val("");
        $(".quantity").val("");
        $(".weight").val("");
        $(".variant_select").val("");
        $("#product_variation").hide();
    }

}
function remove_order(trcount) {
    var conf = confirm("Are you sure you want to remove this Parent category?");
    if (conf)
    {
        $.each(order_object, function (key, val) {
            if (trcount == val['tr_count']) {
                total_wt = total_wt - val['appx_wt'];
                total_qtn = total_qtn - val['quantity'];
                order_object.splice(key, 1);
            }
        })
        $('#id_' + trcount).remove();
        $('#total_weight').html('Total Weight: ' + total_wt);
        $('#total_qtn').html('Total Quantity: ' + total_qtn);
        trcount++;
        console.log(JSON.stringify(order_object))
    } else {
        return false;
    }
}
function remove_existing_order(trcount) {
    var conf = confirm("Are you sure you want to remove this sub category?");
    if (conf)
    {
        $.ajax({
            url: url + "Manufacturing_department_order/delete/" + trcount,
            type: "POST",
            dataType: "json",
            success: function (data, textStatus, jqXHR)
            {
                if (data.status == 'failure')
                {
                    error_msg(data.error);

                } else if (data.status == "success") {
                    $.each(order_object, function (key, val) {
                        if (trcount == val['tr_count']) {
                            order_object.splice(key, 1);
                        }
                    })
                    $('#id_' + trcount).remove();

                } else {
                    showMsgPopup("Error", "Something Went Wrong Try Again", '', 'error');
                }

            },
            error: function (jqXHR, textStatus, errorThrown)
            {

            }
        });
    } else {
        return false;
    }
}
function create_karigar(mop_id, qty, ele_id, mid_wieght) {
    $('#create_karigar').modal('show');
    $('.karigar_quantity').val(qty);
    $("#product_code").text($("#product_code" + ele_id).val());
    $('#mop_id').val(mop_id);
    $("#approx_weight").val(Math.round((mid_wieght * qty) * 100) / 100);
    $("#mid_wight_range_hidden").val(mid_wieght);
}
/**
 change approx weight
 **/
function change_approx_weight(qty)
{
    $("#approx_weight").val(Math.round(($("#mid_wight_range_hidden").val() * qty) * 100) / 100);
}


function create_mnfctrng_karigar() {
    $.ajax({
        url: url + "Prepare_karigar/store",
        type: "POST",
        dataType: "json",
        data: $('form#create_karigar').serialize(),

        success: function (data, textStatus, jqXHR)
        {
            if (data.status == 'failure')
            {
                error_msg(data.error);

            } else if (data.status == "success") {
                showMsgPopup("Success", "Successfully Updated", '', 'normal')
                window.location.href = data.data.url
                // $('#create_karigar').modal('hide');
                // location.reload();

            } else {

                showMsgPopup("Error", "Something Went Wrong Try Again", '', 'error');
            }

        },
        error: function (jqXHR, textStatus, errorThrown)
        {

        }
    });
}
function open_mfg_order_modal(id) {
    $('#receive_sub_category').modal('show');
    $('#kmm_id').val(id);
}
function receive_sub_category() {
    $('.text-danger').html('');
    $.ajax({
        url: url + "Order_sent/store",
        type: "POST",
        dataType: "json",
        data: $('form#receive_mfg').serialize(),
        success: function (data, textStatus, jqXHR)
        {
            if (data.status == 'failure')
            {
                error_msg(data.error);

            } else if (data.status == "success") {
                $('#receive_sub_category').modal('hide');
                location.reload();

            } else {

                showMsgPopup("Error", "Something Went Wrong Try Again", '', 'error');
            }

        },
        error: function (jqXHR, textStatus, errorThrown)
        {

        }
    });
}


$(document).ready(function ()
{
    $('.ajaxLoader')
            .hide()
            .ajaxStart(function () {
                $('.ajaxLoader').show();
            })
            .ajaxStop(function () {
                $('.ajaxLoader').hide();
            });

    /**
     Code to view kariger history links
     **/
    $(".change_kariger").change(function () {
        if (this.value != "")
        {
            $(".karigar_history").show();
            $(".karigar_history").attr("href", url + "Karigar_order_list/kariger_details/" + this.value);
        }
    })

});

function import_billing_excel() {
    $('.errors').html('');
  $(".ajaxLoader").fadeIn();
    var formdata = new FormData();
    var file_data = $('input[type="file"]')[0].files;
    formdata.append("file_0", file_data[0]);
    formdata.append("challan_no", $('#challan_no').val());
    $.ajax({
        url: url + "Import_bill_excel/store",
        type: "POST",
        dataType: "json",
        contentType: false,
        processData: false,
        data: formdata,
        success: function (data, textStatus, jqXHR)
        {
            $('.ajaxLoader').fadeOut();
            if (data.status == 'failure')
            {
                excel_error_msg(data.error);

            } else if (data.status == "success") {
                showMsgPopup("Success", data.data + " Designs Successfully Added", '', 'normal')
                //location.reload();
                setTimeout(function () {
                    close_toast_popup(url + 'Import_bill_excel')

                }, 2000)

            } else {

                showMsgPopup("Error", "Something Went Wrong Try Again", '', 'error')
                // setTimeout(function(){
                //     close_toast_popup(url+'Carat')

                // },2000)
            }
        }
    });
}

function save_tag_product(method_name) {
    var atLeastOneIsChecked = false;
    $('input:checkbox').each(function () {
        if ($(this).is(':checked')) {
            atLeastOneIsChecked = true;
            return false;
        }
    });
    if (atLeastOneIsChecked) {
        $('.text-danger').html('');
        $.ajax({
            url: url + "Make_set/" + method_name,
            type: "POST",
            dataType: "json",
            data: $('form#makeSet').serialize(),
            success: function (data, textStatus, jqXHR)
            {
                if (data.status == 'failure')
                {
                    error_msg(data.error);

                } else if (data.status == "success") {
                    showMsgPopup("Success", "Successfully Updated", '', 'normal')
                    setTimeout(function () {
                        close_toast_popup(url + 'Make_set')

                    }, 1000)
                } else {
                    showMsgPopup("Error", "Something Went Wrong Try Again", '', 'error');
                }

            },
            error: function (jqXHR, textStatus, errorThrown)
            {

            }
        });
    } else {
        showMsgPopup("Error", "Please select atleast one order", '', 'error');
    }
}
function orderSentToDept(type) {
    $.ajax({
        url: url + "Ready_product/store",
        type: "POST",
        dataType: "json",
        data: {ms_id: $("#ms_id").val(), product_type: type},
        success: function (data, textStatus, jqXHR)
        {
            if (data.status == 'failure')
            {
                error_msg(data.error);

            } else if (data.status == "success") {
                showMsgPopup("Success", "Successfully Updated", '', 'normal')
                setTimeout(function () {
                    close_toast_popup(url + 'Department')

                }, 1000)
            } else {
                showMsgPopup("Error", "Something Went Wrong Try Again", '', 'error');
            }


        },
        error: function (jqXHR, textStatus, errorThrown)
        {

        }
    });

}

function open_product_ready_modal(id) {
    $('#product_ready').modal('show');
    $('#ms_id').val(id);
}
$(document).ready(function ($) {
    if (typeof isPreviousEventComplete !== 'undefined') {
        $(window).scroll(function () {
            if (isPreviousEventComplete && isDataAvailable) {
                isPreviousEventComplete = false;
                $(".LoaderImage").css("display", "block");
                var filterData = "page_id=" + page_id + "&type=html&id=" + msr_id;
                $.ajax({
                    url: url + "Make_set/index",
                    type: "POST",
                    data: filterData,
                    success: function (result, textStatus, jqXHR)
                    {
                        $(".section1right").css("height", 'auto');
                        isPreviousEventComplete = true;
                        $(".LoaderImage").css("display", "none");

                        if (result.trim() == "") //When data is not available
                            isDataAvailable = false;
                        else {
                            page_id = parseInt(page_id) + parseInt(1);
                            $("#order_div").append(result);
                            $(window).scroll();
                        }
                    },
                    error: function (jqXHR, textStatus, errorThrown)
                    {

                    }
                });
            }
        });
        $(window).scroll();
    }
});
function shortlisted_all_products() {
    var filter = {};
    if ($('#category_search').val() !== '')
        filter.category = $('#category_search').val();
    if ($('.weight-range').val() !== '')
        filter.wt = $('.weight-range').val();
    if ($('#product_code').val() !== '')
        filter.product_code = $('#product_code').val();
    if ($('.karigar_add').val() !== '')
        filter.karigar_code = $('.karigar_add').val();
    if ($('#corporate').val() !== '')
        filter.corporate = $('#corporate').val();

    $.ajax({
        url: url + "send_for_approval/shortlisted_all_products",
        type: "POST",
        dataType: "json",
        data: {filter: filter},
        success: function (data, textStatus, jqXHR)
        {
            //console.log(data);
            if (data.status == 'failure')
            {
                error_msg(data.error);

            } else if (data.status == "success") {
                showMsgPopup("Success", "Successfully Shortlisted", '', 'normal')
                setTimeout(function () {
                    location.reload();

                }, 500)
            } else {
                if (data.data == "Please select products of any one corporate.")
                    showMsgPopup("Error", "Please select products of any one corporate.", '', 'error');
                else
                    showMsgPopup("Error", "No Products for shortlisting", '', 'error');
            }


        },
        error: function (jqXHR, textStatus, errorThrown)
        {

        }
    });

}
function remove_sortlisted_product() {
    $.ajax({
        url: url + "send_for_approval/delete",
        type: "GET",
        dataType: "json",
        data: $("#product_shortlist_search").serialize() + "&list=shortlistdesign",
        success: function (data, textStatus, jqXHR)
        {
            if (data.status == "success") {
                showMsgPopup("Success", "Deleted Successfully", '', 'normal')
                setTimeout(function () {
                    location.reload();

                }, 500)
            }

        },
        error: function (jqXHR, textStatus, errorThrown)
        {

        }
    });
}

/*added to stock*/
function add_to_stock(am_id) {
    //console.log(am_id);
    $('#s_' + am_id).attr('disabled', true);
    // return false;
    $.ajax({
        url: url + "Amend_products/send_to_stock/" + am_id,
        type: "POST",
        dataType: "json",
        success: function (data, textStatus, jqXHR)
        {
            if (data.status == 'failure')
            {
                showMsgPopup("Error", 'Something Went Wrong Try Again', '', 'error')

            } else if (data.status == "success") {

                showMsgPopup("Success", "Product successfully add to stock", '', 'normal')
                setTimeout(function () {
                    close_toast_popup(url + 'Amend_products')

                }, 2000)

            } else {

                showMsgPopup("Error", "Something Went Wrong Try Again", '', 'error')
                setTimeout(function () {
                    close_toast_popup(url + 'Amend_products')

                }, 2000)
            }

        },
        error: function (jqXHR, textStatus, errorThrown)
        {

        }
    });
}

function getShortlistDesignProducts() {
    $.ajax({
        url: url + "Send_for_approval/index/",
        type: "POST",
        data: $("#product_shortlist_search").serialize() + "&list=shortlistdesign",
        success: function (data, textStatus, jqXHR)
        {
            $("#sortlist_product_form").html(data);
        },
        error: function (jqXHR, textStatus, errorThrown)
        {

        }
    });
}

$(document).ready(function () {
    $('#product_shortlist_search input').keyup(function () {
        getShortlistDesignProducts();
    });
    $('#product_shortlist_search select').change(function () {
        getShortlistDesignProducts();
    });

    $('.choose_department').on('change', function () {
        var value = $(this).val();
        window.location.href = url + 'Manufacturing_department_order/create/' + value;
    });

});

/*send to exported Qc*/
function send_to_qcexported() {
    $('.text-danger').html('');
    // var object = $("form#export").serialize();
    var formData = new FormData(document.querySelector("form#export"));
    formData.append('search', $('.input-sm').val());

    // selected_products:$('#add_selected_hallmark').val()
    $.ajax({
        url: url + "Hallmarking/send_to_qcexported",
        type: "POST",
        dataType: "json",
        data: formData,
        processData: false,
        contentType: false,
        success: function (data, textStatus, jqXHR)
        {
            //console.log(data)
            if (data.status == "failure") {
                showMsgPopup("Error", "Please check at least one parameter", '', 'error')

            } else if (data.status == "success") {
                showMsgPopup("Success", "success successfully sent to hallmarking Qc accepted", '', 'normal')
                setTimeout(function () {
                    close_toast_popup(url + 'Hallmarking?status=send')
                }, 2000)

            } else {
                showMsgPopup("Error", "Something Went Wrong Try Again", '', 'error')
                setTimeout(function () {
                    close_toast_popup(url + 'Hallmarking?status=send')
                }, 2000)
            }
        },
        error: function (jqXHR, textStatus, errorThrown)
        {

        }
    });
}


//get subcategory images
function get_sub_cat_images(id)
{
    $.ajax({
        url: url + "parent_category/get_images",
        type: "POST",
        dataType: "json",
        data: {id: id},
        success: function (data, textStatus, jqXHR)
        {
            var html = '';
            for (var i = 0; i < data.images.length; i++)
            {

                /* html += '<div class="col-sm-2"><input class="col-sm-1" type="radio" value="'+data.sub_cat_images[i].id+'" name="sub_cat_image[]" id="sub_cat_image_id"/><label class="col-sm-1 control-label" for="inputEmail3"><img src="'+upload_images+'sub_category/'+data.sub_cat_name+'/thumb/'+data.sub_cat_images[i].sub_cat_image+'"/></label></div>';  */
                html += '<div class="col-md-2"><label class="btn single_image_check_lbl" onclick="check_image(this.id)" id="check_image' + i + '"><img src="' + upload_images + 'parent_category/' + data.images[i].name + '/small/' + data.images[i].path + '" alt="..." class="img-thumbnail img-check"><input type="radio" name="sub_cat_image[]" id="item4" value="' + data.images[i].id + '" autocomplete="off" class="hidden check_image' + i + '"></label></div>'
            }

            if (typeof (data.variants) != 'undefined') {

                var size = Object.keys(data.variants).length;
                if (size > 0) {
                    var variant = ' ';
                    $.each(data.variants, function (key, val) {
                        variant += '<option value="' + val.id + '">' + val.name + '</option>'
                    });
                    variant += '</select></div>'
                    $("select[multiple].variant_select.3col").multiselect("reload");
                    $("#product_variation_sel").html(variant);
                    $('select[multiple].variant_select.3col').multiselect({
                        search: true,
                        searchOptions: {
                            'default': 'Search variant'
                        },
                        selectAll: true
                    });
                    $('select[multiple].variant_select.3col').multiselect('reload');


                    $("#product_variation_sel").html(variant);
                    $("#product_variation").show();
                } else {
                    $("#product_variation").hide();
                }
            }
            $("#sub_image_div").html(html);
            if (typeof (data.variants) == 'undefined') {
                $("#product_variation").hide();
                $("#sub_image_div").html('');
            }


        },
        error: function (jqXHR, textStatus, errorThrown)
        {

        }
    });
}

function save_hallmarking_center(action) {
    $.ajax({
        url: url + "hallmarking_center/" + action,
        type: "POST",
        dataType: "json",
        data: $('form#hallmarking_center').serialize(),
        success: function (data, textStatus, jqXHR)
        {
            if (data.status == 'failure')
            {
                error_msg(data.error);

            } else if (data.status == "success") {
                showMsgPopup("Success", "Successfully Added", '', 'normal')
                setTimeout(function () {
                    close_toast_popup(url + 'hallmarking_center')

                }, 2000)

            } else {

                showMsgPopup("Error", "Something Went Wrong Try Again", '', 'error')
                setTimeout(function () {
                    close_toast_popup(url + 'hallmarking_center')

                }, 2000)
            }
        },
        error: function (jqXHR, textStatus, errorThrown)
        {

        }
    });
}

/*
 Code to remove subcategory from department orders
 */
function remove_sub_category_from_dept_order(id, order_id)
{
    $.ajax({
        url: url + "Manufacturing_department_order/delete/" + id,
        type: "POST",
        dataType: "json",
        data: $('form#hallmarking_center').serialize(),
        success: function (data, textStatus, jqXHR)
        {
            if (data.status == 'failure')
            {
                error_msg(data.error);

            } else if (data.status == "success") {
                showMsgPopup("Success", "Successfully Removed", '', 'normal')
                setTimeout(function () {
                    close_toast_popup(url + 'Manufacturing_department_order/view/' + order_id)

                }, 2000)

            } else {

                showMsgPopup("Error", "Something Went Wrong Try Again", '', 'error')
                setTimeout(function () {
                    close_toast_popup(url + 'hallmarking_center')

                }, 2000)
            }
        },
        error: function (jqXHR, textStatus, errorThrown)
        {

        }
    });
}

function remove_kariger_from_kariger_list(id)
{
    $.ajax({
        url: url + "Karigar_order_list/delete/" + id,
        type: "POST",
        dataType: "json",
        data: $('form#hallmarking_center').serialize(),
        success: function (data, textStatus, jqXHR)
        {
            if (data.status == 'failure')
            {
                error_msg(data.error);

            } else if (data.status == "success") {
                showMsgPopup("Success", "Successfully Removed", '', 'normal')
                setTimeout(function () {
                    close_toast_popup(url + 'Karigar_order_list')

                }, 2000)

            } else {

                showMsgPopup("Error", "Something Went Wrong Try Again", '', 'error')
                setTimeout(function () {
                    close_toast_popup(url + 'hallmarking_center')

                }, 2000)
            }
        },
        error: function (jqXHR, textStatus, errorThrown)
        {

        }
    });
}

/*
 Code to change manufactures kariger
 */
function change_manufactured_kariger(id)
{
    $('#change_karigar').modal('show');
    $("#change_kmm_id").val(id);
}

function change_mnfctrng_karigar() {
    $.ajax({
        url: url + "Prepare_karigar/change",
        type: "POST",
        dataType: "json",
        data: $('form#change_karigar').serialize(),

        success: function (data, textStatus, jqXHR)
        {
            if (data.status == 'failure')
            {
                error_msg(data.error);

            } else if (data.status == "success") {
                $('#create_karigar').modal('hide');
                location.reload();

            } else {

                showMsgPopup("Error", "Something Went Wrong Try Again", '', 'error');
            }

        },
        error: function (jqXHR, textStatus, errorThrown)
        {

        }
    });
}

function remove_mnfctrng_order(order_id)
{
    $.ajax({
        url: url + "Manufacturing_department_order/delete_order/" + order_id,
        type: "POST",
        dataType: "json",
        data: $('form#change_karigar').serialize(),

        success: function (data, textStatus, jqXHR)
        {
            if (data.status == 'failure')
            {
                error_msg(data.error);

            } else if (data.status == "success") {
                $('#create_karigar').modal('hide');
                location.reload();

            } else {

                showMsgPopup("Error", "Something Went Wrong Try Again", '', 'error');
            }

        },
        error: function (jqXHR, textStatus, errorThrown)
        {

        }
    });
}

function check_overall_qc()
{
    if ($("input[name='all_check_qc[]']:checked").val() == undefined) {
        showMsgPopup("Error", "Select product for Qc check", '', 'error');
        return false;
    }

    $(".check_all_qc").submit();

}

function check_all_to_hallmarking(type)
{
    if ($("input[name='send_all_to_hlmar[]']:checked").val() == undefined) {
        showMsgPopup("Error", "Select product for Qc check", '', 'error');
        return false;
    }
    $('#qc_check_modal').modal('show');

    console.log($("input[name='send_all_to_hlmar[]']:checked").val())

}

var page_reload = true;
function check_all_to_hallmarking_after_modal() {
    var center_id = $('#hallmarking_center_name').val();
    var gross_wt = $('#gross_weight').val();
    var hc_logo = $('#hallmarking_logo').val();
    $('.text-danger').html('');
    if (center_id == "" || gross_wt == "" || hc_logo == "") {
        page_reload = false;
        if (center_id == "") {
            $('#hallmarking_center_name_error').html('This Field is required');
        }
        if (gross_wt == "") {
            $('#gross_weight_error').html('This Field is required');
        }
        if (hc_logo == "") {
            $('#hallmarking_logo_error').html('This Field is required');
        }
        page_reload = false;
    } else {
        $('#qc_check_modal').modal('hide');
        $('#hc_id').val(center_id);
        $('#gr_weight').val(gross_wt);
        $('#hc_logo').val(hc_logo);
        $(".send_all_to_hallmarking").submit();

    }

}

function reload_page_hallmarking() {
    if (page_reload == true) {
        location.reload();
    }
}

function accept_all_m_qc()
{
    var conf = confirm("Are you sure you want to accept?");
    if (!conf)
    {
        return false;
    }
    if ($("input[name='qc[receive_product_id][]']:checked").val() == undefined) {
        showMsgPopup("Error", "Please select at least one product for QC", '', 'error')
        $('html, body').animate({
            scrollTop: $("#Receive_pieces").offset().top - ($(".nav").height() + 100)
        }, 1200);
        return false;
    }
    //     showMsgPopup("Error","Please check all Parameters",'','error')
    //     // setTimeout(function(){
    //     //                 close_toast_popup(location);
    //     //                 },2000)
    //     //alert('Please check all Parameters');
    //     return false;
    // }
    $.ajax({
        url: url + "Manufacturing_quality_control/store_all",
        type: "POST",
        dataType: "json",
        data: $('form#qc_check').serialize(),
        success: function (data, textStatus, jqXHR)
        {
            $('.text-danger').html('');
            if (data.status == 'failure')
            {
                error_msg(data.error);

            } else if (data.status == "success") {
                showMsgPopup("Success", "successfully accepted", '', 'normal')
                setTimeout(function () {
                    close_toast_popup(url + 'Received_receipt')

                }, 500)

            } else if (data.status == "hallmarking") {
                var target_url = url + "Manufacturing_quality_control/hallmarking_receipt"
                window.open(target_url, '_blank');
                setTimeout(function () {
                    close_toast_popup(url + 'Received_receipt')

                }, 500)
            } else {

                showMsgPopup("Error", "Something Went Wrong Try Again", '', 'error')
                setTimeout(function () {
                    close_toast_popup(url + 'Received_receipt')

                }, 500)
            }
        },
        error: function (jqXHR, textStatus, errorThrown)
        {

        }
    });
}
function reject_all_m_qc() {
    var conf = confirm("Are you sure you want to reject?");
    if (!conf)
    {
        return false;
    }
    if ($("input[name='qc[receive_product_id][]']:checked").val() == undefined) {
        showMsgPopup("Error", "Please select at least one product for QC", '', 'error')
        $('html, body').animate({
            scrollTop: $("#Receive_pieces").offset().top - ($(".nav").height() + 100)
        }, 1200);
        return false;
    }
    $.ajax({
        url: url + "Manufacturing_quality_control/reject_all",
        type: "POST",
        dataType: "json",
        data: $('form#qc_check').serialize(),
        success: function (data, textStatus, jqXHR)
        {
            if (data.status == 'failure')
            {
                error_msg(data.error);

            } else if (data.status == "success") {
                showMsgPopup("Success", "successfully rejected", '', 'normal')
                setTimeout(function () {
                    close_toast_popup(url + 'Received_receipt')

                }, 2000)

            } else {

                showMsgPopup("Error", "Something Went Wrong Try Again", '', 'error')
                setTimeout(function () {
                    close_toast_popup(url + 'Received_receipt')

                }, 2000)
            }
        },
        error: function (jqXHR, textStatus, errorThrown)
        {

        }
    });
}

function get_receive_product_from_mnfg_accounting(id)
{
    if (id == "")
        return false;
    $.ajax({
        url: url + "Mnfg_accounting/receive_product/" + id,
        type: "POST",
        dataType: "json",
        data: {is_kundan: $('#is_kundan').val()},
        success: function (data, textStatus, jqXHR)
        {
            var product_html = '<>';
            for (var i = 0; i < data.length; i++) {
                product_html += "<tr>";
                product_html += '<th class="col1"><input type="hidden" name=kr[' + data[i]['kmm_id'] + '][parent_category_id] value="' + data[i]['parent_category_id'] + '">' + data[i]['name'] + '</th>';
                product_html += '<th class="col1"><input type="hidden" name=kr[' + data[i]['kmm_id'] + '][weight_range_id] value="' + data[i]['weight_range_id'] + '"><span class="pull-right">' + data[i]['from_weight'] + ' - ' + data[i]['to_weight'] + '</span></th>';
                product_html += '<th class="col1"><input type="hidden" name=kr[' + data[i]['kmm_id'] + '][mop_id] value="' + data[i]['mop_id'] + '"><input type="text" name=kr[' + data[i]['kmm_id'] + '][quantity] value="' + data[i]['quantity'] + '"><p class="text-danger" id="quantity_' + data[i]['kmm_id'] + '_error"></p></th>';

                product_html += '<th class="col1"><input type="text" min="0" id="net_wt" name=kr[' + data[i]['kmm_id'] + '][net_wt] value="" class="for"><p class="text-danger" id="net_wt_' + data[i]['kmm_id'] + '_error"></p></th>';
                product_html += '<th class="col1"><a id="add_more" class="btn btn-danger small loader-hide  btn-sm" href="javascript:void(0)" onclick="remove_tag_code(this)" title="remove">Remove</a></th>';
                product_html += '</tr>';
            }
            ;

            $("#product_details").html(product_html);
        },
        error: function (jqXHR, textStatus, errorThrown)
        {

        }
    });
}

function get_receive_product_from_kairgar(id)
{
    if (id == "")
        return false;
    $.ajax({
        url: url + "Manufacturing_quality_control/receive_product/" + id,
        type: "POST",
        dataType: "json",
        data: {is_kundan: $('#is_kundan').val()},
        success: function (data, textStatus, jqXHR)
        {
            var product_html = '<>';
            ////console.log(data)
            for (var i = 0; i < data.length; i++) {
                /*console.log(data[i]['kmm_id']);
                 console.log(data[i]['weight']);*/
                product_html += "<tr>";
                product_html += '<th class="col1"><input type="hidden" name=kr[' + data[i]['kmm_id'] + '][department_id] value="' + data[i]['department_id'] + '"><input type="hidden" name=kr[' + data[i]['kmm_id'] + '][product_name] value="' + data[i]['name'] + '">' + data[i]['name'] + '</th>';
                product_html += '<th class="col1"><span class="pull-right">' + data[i]['from_weight'] + ' - ' + data[i]['to_weight'] + '</span></th>';
                // product_html += '<th class="col1"><input type="hidden" name=kr['+data[i]['kmm_id']+'][mop_id] value="'+data[i]['mop_id']+'"><input type="text" name=kr['+data[i]['kmm_id']+'][quantity] value="'+data[i]['quantity']+'"><p class="text-danger" id="quantity_error_'+data[i]['kmm_id']+'"></p></th>';
                // product_html += '<th class="col1"><a id="add_more" class="btn btn-danger small loader-hide  btn-sm" href="javascript:void(0)" onclick="remove_tag_code(this)" title="Add More">Remove</a></th>';
                //console.log(data[i]['department_id']);
                var department_id = data[i]['department_id'];
                if (department_id != '2' && department_id !='3' && department_id !='6' && department_id !='10') {
                    //console.log(department_id)
                    product_html += '<th class="col1"><input type="hidden" name=kr[' + data[i]['kmm_id'] + '][few_box_status] value="' + data[i]['few_box'] + '"><input type="hidden" name=kr[' + data[i]['kmm_id'] + '][tagging_status] value="0"><input type="hidden" name=kr[' + data[i]['kmm_id'] + '][mop_id] value="' + data[i]['mop_id'] + '"><input type="text" class="form-control" name=kr[' + data[i]['kmm_id'] + '][quantity] value="' + data[i]['quantity'] + '"><p class="text-danger" id="quantity_error_' + data[i]['kmm_id'] + '"></p></th>';

                    product_html += '<th class="col1"><input type="text"  class="form-control" name="kr[' + data[i]['kmm_id'] + '][gross_wt]" id="gross_wt' + i + '" onkeyup="calculate_pure(' + i + ')"><p class="text-danger" id="gross_wt_error_' + data[i]['kmm_id'] + '" ></p></th>';
                    product_html += '<th class="col1"><input type="text" onchange="calculate_pure(' + i + ')" class="form-control" id="net_wt' + i + '" name="kr[' + data[i]['kmm_id'] + '][net_wt]" ><p class="text-danger" id="net_wt_error_' + data[i]['kmm_id'] + '"></p></th>';
                   
                } else {
                    // console.log(department_id)
                    product_html += '<th class="col1"><input type="hidden" name=kr[' + data[i]['kmm_id'] + '][few_box_status] value="' + data[i]['few_box'] + '"><input type="hidden" name=kr[' + data[i]['kmm_id'] + '][tagging_status] value="' + data[i]['tagging_status'] + '"><input type="hidden" name=kr[' + data[i]['kmm_id'] + '][mop_id] value="' + data[i]['mop_id'] + '"><input type="text" class="form-control" name=kr[' + data[i]['kmm_id'] + '][weight] value="' + data[i]['weight'] + '" onkeyup="copy_mfg_gr_wt(' + i + ')"><p class="text-danger" id="weight_error_' + data[i]['kmm_id'] + '"></p></th>';

                product_html += '<th class="col1"><input type="text" onchange="calculate_pure(' + i + ')" class="form-control" id="net_wt' + i + '" name="kr[' + data[i]['kmm_id'] + '][net_wt]" value="' + data[i]['weight'] + '"><p class="text-danger" id="net_wt_error_' + data[i]['kmm_id'] + '"></p></th>';
                }


                product_html += '<th class="col1"><input id="wastage' + i + '" onchange="calculate_pure(' + i + ')" type="text" class="form-control" name="kr[' + data[i]['kmm_id'] + '][wastage]" value="' + data[i]['wastage'] + '" readonly><p class="text-danger" id="wastage_error_' + data[i]['kmm_id'] + '"></p></th>';

                product_html += '<th class="col1"><input  onchange="calculate_pure(' + i + ')" id="melting' + i + '" type="text" class="form-control" name="kr[' + data[i]['kmm_id'] + '][melting]" value="92"><p class="text-danger" id="melting_error_' + data[i]['kmm_id'] + '"></p></th>';

                product_html += '<th class="col1"><input id="pure' + i + '" type="text" class="form-control" name="kr[' + data[i]['kmm_id'] + '][pure]" readonly></th>';
                if (data[i]['few_box'] == '1') {
                    var disable = '';
                } else {
                    var disable = 'readonly';
                }
                if (department_id != '2' && department_id !='3' && department_id !='6' && department_id !='10') {
               /*     product_html += '<th class="col1"><input type="text" class="form-control" name="kr[' + data[i]['kmm_id'] + '][stone_wt]" ' + disable + '><p class="text-danger" id="stone_wt_error_' + data[i]['kmm_id'] + '"></p></th>';*/
                    product_html += '<th class="col1"><input type="text" class="form-control" name="kr[' + data[i]['kmm_id'] + '][stone_wt]"><p class="text-danger" id="stone_wt_error_' + data[i]['kmm_id'] + '"></p></th>';
                }

                product_html += '<th class="col1"><input type="text" class="form-control" name="kr[' + data[i]['kmm_id'] + '][amount]"><p class="text-danger" id="amount_error_' + data[i]['kmm_id'] + '"></p></th>';

                product_html += '<input type="hidden" value="1" class="form-control shortlist_all" name="kr[' + data[i]['kmm_id'] + '][is_skip_qc]">';
              //  product_html += '<th class="col1"><div class="checkbox checkbox-purpal"><input type="checkbox" value="1" class="form-control shortlist_all" name="kr[' + data[i]['kmm_id'] + '][is_skip_qc]"><label></label></div></th>';

                /* product_html += '<th class="col1"><a id="add_more" class="btn btn-danger small loader-hide  btn-md" href="javascript:void(0)" onclick="remove_tag_code(this)" title="Add More">Remove</a></th>';*/
                product_html += '</tr>';
            }
            
            $("#product_details").html(product_html);
            var rowCount = $('#product_details tr').length;
            $("#totalRecodcnt").text(' Showing  of entries  ' + rowCount);

        },
        error: function (jqXHR, textStatus, errorThrown)
        {

        }
    });
}



function copy_mfg_gr_wt(id) {
   //alert(id)
    var gr_wt = $('#gross_wt' + id).val(); 
     if (!isNaN(gr_wt) ) {
        $('#net_wt' + id).val(gr_wt);
    }
    
}
function calculate_pure(id) {
     var gr_wt = $('#gross_wt' + id).val(); 
    //  if (!isNaN(gr_wt) ) {
    //     $('#net_wt' + id).val(gr_wt);
    // }
    var net_wt = parseFloat($('#net_wt' + id).val());
    var wastage = parseFloat($('#wastage' + id).val());
    var melting = parseFloat($('#melting' + id).val());
    if (!isNaN(wastage) && !isNaN(net_wt) && !isNaN(melting)) {
        var margin_total = ((wastage + melting) / 100) * net_wt;
        var pure = margin_total.toFixed(3);
        $('#pure' + id).val(pure)
    }
}
function copy_mfg_gr_wt_recive_pro(evt) {
   
       var gr_wt = $(evt).closest('tr').find('.gr_wt').val();
       if (!isNaN(gr_wt) ) {
             $(evt).closest('tr').find('.net_wt').val(gr_wt);
     }
}
function calculate_pure_new_recive_pro(evt) {
    evt.value = evt.value.replace(/[^0-9.]/g, '');
    var net_wt = parseFloat($(evt).closest('tr').find('.net_wt').val()) || 0;
    var wastage = parseFloat($(evt).closest('tr').find('.wastage').val()) || 0;
    var melting = parseFloat($(evt).closest('tr').find('.melting').val()) || 0;

    if (!isNaN(wastage) && !isNaN(net_wt) && !isNaN(melting)) {
        var margin_total = ((wastage + melting) / 100) * net_wt;
        var pure = margin_total.toFixed(3);
        $(evt).closest('tr').find('.pure').val(pure);
       
    }
}
function remove_rejected_order(obj, id) {
    var conf = confirm("Are you sure you want to rejected?");
    if (conf)
    {
        $.ajax({
            url: url + "Received_orders/rejected_product/",
            type: "POST",
            dataType: "json",
            data: {id: id},
            success: function (data, textStatus, jqXHR)
            {
                if (data.status == 'failure')
                {
                    error_msg(data.error);
                } else if (data.status == "success") {
                    $(obj).parents('tr').remove();
                    showMsgPopup("Success", "Successfully Rejected", '', 'normal')
                    setTimeout(function () {
                    }, 2000)

                } else {

                    showMsgPopup("Error", "Something Went Wrong Try Again", '', 'error')
                    setTimeout(function () {
                        close_toast_popup()

                    }, 2000)
                }

            },
            error: function (jqXHR, textStatus, errorThrown)
            {

            }
        });
    }
}

function get_rejected_product_from_mnfg(id)
{
    if (id == "")
        return false;
    $.ajax({
        url: url + "Mnfg_accounting_rejected_orders/rejected_products/" + id,
        type: "POST",
        dataType: "json",
        data: {id: id},
        success: function (data, textStatus, jqXHR)
        {
            var product_html = '<>';
            for (var i = 0; i < data.length; i++) {
                // console.log(data[i]);
                product_html += "<tr>";
                product_html += '<th class="col1">' + data[i]['name'] + '</th>';
                product_html += '<th class="col1"><span class="pull-right">' + data[i]['from_weight'] + ' - ' + data[i]['to_weight'] + '</span></th>';
                product_html += '<th class="col1"><input type="hidden" name=kr[' + data[i]['ma_id'] + '][quantity] value="' + data[i]['quantity'] + '">' + data[i]['quantity'] + '</th>';
                product_html += '<th class="col1"><input type="text" min="0" id="net_wt" name=kr[' + data[i]['ma_id'] + '][net_wt] value="' + data[i]['net_wt'] + '"><p class="text-danger" id="net_wt_' + data[i]['ma_id'] + '_error"></p></th>';
                product_html += '<th class="col1"><a id="add_more" class="btn btn-danger small loader-hide  btn-sm" href="javascript:void(0)" onclick="remove_tag_code(this)" title="Add More">Remove</a></th>';
                product_html += '</tr>';
            }
            ;

            $("#product_details").html(product_html);
        },
        error: function (jqXHR, textStatus, errorThrown)
        {

        }
    });
}

function save_sale_under() {
    $.ajax({
        url: url + "Sales_under/store",
        type: "POST",
        dataType: "json",
        data: $('form#Sales_under').serialize(),
        success: function (data, textStatus, jqXHR)
        {
            if (data.status == 'failure')
            {
                error_msg(data.error);

            } else if (data.status == "success") {
                showMsgPopup("Success", "Successfully Added", '', 'normal')
                setTimeout(function () {
                    close_toast_popup(url + 'Sales_under')

                }, 2000)

            } else {

                showMsgPopup("Error", "Something Went Wrong Try Again", '', 'error')
                setTimeout(function () {
                    close_toast_popup(url + 'Sales_under')

                }, 2000)
            }
        },
        error: function (jqXHR, textStatus, errorThrown)
        {

        }
    });
}

function save_parent_category(action) {
    $.ajax({
        url: url + "parent_category/" + action,
        type: "POST",
        dataType: "json",
        data: $('form#parent_category').serialize(),
        success: function (data, textStatus, jqXHR)
        {
            if (data.status == 'failure')
            {
                error_msg(data.error);

            } else if (data.status == "success") {
                showMsgPopup("Success", "Successfully Added", '', 'normal')
                setTimeout(function () {
                    close_toast_popup(url + 'parent_category')

                }, 2000)

            } else {

                showMsgPopup("Error", "Something Went Wrong Try Again", '', 'error')
                setTimeout(function () {
                    close_toast_popup(url + 'parent_category')

                }, 2000)
            }
        },
        error: function (jqXHR, textStatus, errorThrown)
        {

        }
    });
}


function save_product_category(action) {
    $.ajax({
        url: url + "product_category/" + action,
        type: "POST",
        dataType: "json",
        data: $('form#product_category').serialize(),
        success: function (data, textStatus, jqXHR)
        {
            if (data.status == 'failure')
            {
                error_msg(data.error);

            } else if (data.status == "success") {
                showMsgPopup("Success", "Successfully Added", '', 'normal')
                setTimeout(function () {
                    close_toast_popup(url + 'product_category')
                }, 2000)

            } else {

                showMsgPopup("Error", "Something Went Wrong Try Again", '', 'error')
                setTimeout(function () {
                    close_toast_popup(url + 'product_category')

                }, 2000)
            }
        },
        error: function (jqXHR, textStatus, errorThrown)
        {

        }
    });
}



function edit_sale_under() {
    $.ajax({
        url: url + "Sales_under/update",
        type: "POST",
        dataType: "json",
        data: $('form#Sales_under').serialize(),
        success: function (data, textStatus, jqXHR)
        {
            if (data.status == 'failure')
            {
                error_msg(data.error);

            } else if (data.status == "success") {
                showMsgPopup("Success", "Successfully Updated", '', 'normal')
                setTimeout(function () {
                    close_toast_popup(url + 'Sales_under')
                }, 2000)

            } else {

                showMsgPopup("Error", "Something Went Wrong Try Again", '', 'error')
                setTimeout(function () {
                    close_toast_popup(url + 'Sales_under')

                }, 2000)
            }
        },
        error: function (jqXHR, textStatus, errorThrown)
        {

        }
    });
}

function save_hallmarking_list(action) {
    $.ajax({
        url: url + "hallmarking_list/" + action,
        type: "POST",
        dataType: "json",
        data: $('form#hallmarking_list').serialize(),
        success: function (data, textStatus, jqXHR)
        {
            if (data.status == 'failure')
            {
                error_msg(data.error);

            } else if (data.status == "success") {
                if (action == 'update') {
                    var msg = "Successfully Update";
                } else {
                    var msg = "Successfully Added";
                }
                showMsgPopup("Success", msg, '', 'normal')
                setTimeout(function () {
                    close_toast_popup(url + 'hallmarking_list')

                }, 2000)

            } else {

                showMsgPopup("Error", "Something Went Wrong Try Again", '', 'error')
                setTimeout(function () {
                    close_toast_popup(url + 'hallmarking_list')

                }, 2000)
            }
        },
        error: function (jqXHR, textStatus, errorThrown)
        {

        }
    });
}


function print_manufactur_accounting_from()
{       //alert("hii");
    $('text-danger').html('');
    var retvalue = true;
    if ($("#kariger_id").val() == "") {
        $("#kariger_id_error").text("This field is required.")
        retvalue = false;
    }

    if (retvalue == true) {

        $.ajax({
            url: url + "Mnfg_accounting/check_quantity",
            type: "POST",
            dataType: "json",
            data: $('form#print_acc_receipt_frm').serialize(),
            success: function (data, textStatus, jqXHR)
            {
                if (data.status == 'failure')
                {
                    error_msg(data.error);

                } else if (data.status == 'success') {

                    $('#print_acc_receipt_frm').submit();
                    setTimeout(function () {
                        window.location.href = url + 'Mnfg_accounting_received_orders';
                    }, 500)

                }
            },
            error: function (jqXHR, textStatus, errorThrown)
            {

            }
        });
    }
    if (retvalue == false) {
        return false;
    }

}

function print_manufactur_rejected_from()
{
    $('text-danger').html('');
    var retvalue = true;
    if ($("#kariger_id").val() == "") {
        $("#kariger_id_error").text("This field is required.")
        retvalue = false;
    }
    if (retvalue == true) {
        $.ajax({
            url: url + "Mnfg_accounting_rejected_orders/change_net_wt",
            type: "POST",
            dataType: "json",
            data: $('form#print_acc_reject_frm').serialize(),
            success: function (data, textStatus, jqXHR)
            {
                if (data.status == 'failure')
                {
                    error_msg(data.error);

                } else if (data.status == 'success') {

                    $('#print_acc_reject_frm').submit();
                    setTimeout(function () {
                        window.location.href = url + 'Mnfg_accounting_rejected_orders';
                    }, 500)

                }
            },

            error: function (jqXHR, textStatus, errorThrown)
            {

            }
        });
    }
    if (retvalue == false) {
        return false;
    }

}

function validate_print_karigar_from()
{
    $('#save_print').attr('disabled', true);
    $('text-danger').html('');
    var retvalue = true;
    // if($("#net_wt").val() == ""){
    //   $("#net_wt_error").text("This field is required.")
    //   retvalue = false;

    // }
    // if($("#net_wt").val() <= 0){
    //   $("#net_wt_error").text("Net wt should be greater than zero")
    //   retvalue = false;

    // }
    // if(isNaN($("#net_wt").val())){
    //      $("#net_wt_error").text('Net wt should be number');
    //     return false;
    // }
    // if($("#net_wt").val() %1 != 0){
    //      $("#net_wt_error").text('Net wt should not be decimal');
    //     return false;
    // }
    if ($("#kariger_id").val() == "") {
        $("#kariger_id_error").text("This field is required.")
        retvalue = false;
    }
    if (retvalue == true) {
        $.ajax({
            url: url + "Manufacturing_quality_control/check_quantity",
            type: "POST",
            dataType: "json",
            data: $('form#print_receipt_frm').serialize(),
            success: function (data, textStatus, jqXHR)
            {
                ////console.log(data);
                if (data.status == 'failure') {
                    $('#save_print').attr('disabled', false);
                    $.each(data.error, function (key, val) {
                        $.each(val, function (key1, val1) {
                            // $('#'+key1+'_error_'+key).html(val1);
                            $('#' + key1).html(val1);
                        })
                    })
                }
                if (data.status == 'error') {
                    $('#save_print').attr('disabled', false);
                    $.each(data.error.quantity, function (key, val) {
                        $('#quantity_error_' + key).html(val);
                    })
                    //$("#qnty_error").text("Quantity Exceeded");
                    retvalue = false;
                }
                if (data.status == "failure1") {
                    $('#save_print').attr('disabled', false);
                    showMsgPopup("Error", "Please Enter at least one product Net weight value", '', 'error');
                    retvalue = false;
                }
                if (data.status == 'success') {
                    $('#print_receipt_frm').submit();
                    setTimeout(function () {
                        window.location.href = url + 'Received_orders';
                    }, 500)
                }
                // if(data.status == 'error'){
                //     $.each(data.error.quantity,function(key,val){
                //         $('#quantity_error_'+key).html(val);
                //     })
                //     //$("#qnty_error").text("Quantity Exceeded");
                //     retvalue = false;
                // }else{
                //     $('#print_receipt_frm').submit();
                //     setTimeout(function(){
                //         window.location.href = url+'Received_orders';
                //     },500)
                // }
            },
            error: function (jqXHR, textStatus, errorThrown)
            {

            }
        });
    }
    if (retvalue == false) {
        $('#save_print').attr('disabled', false);
        return false;
    }

}

function save_sell_prepare_order(action) {
    $('.text-danger').html('');
    $.ajax({
        url: url + "Prepared_order/" + action,
        type: "POST",
        dataType: "json",
        data: $('form#prepare_order').serialize(),
        success: function (data, textStatus, jqXHR)
        {
            if (data.status == 'failure')
            {
                error_msg(data.error);

            } else if (data.status == "success") {
                showMsgPopup("Success", "successfully submitted", '', 'normal')
                setTimeout(function () {
                    close_toast_popup(url + 'Prepared_order')

                }, 2000)

            } else {

                showMsgPopup("Error", "Something Went Wrong Try Again", '', 'error')
                setTimeout(function () {
                    close_toast_popup(url + 'Prepared_order')

                }, 2000)
            }
        },
        error: function (jqXHR, textStatus, errorThrown)
        {

        }
    });
    // body...
}
function get_hallmarked_pro_by_kariger(id) {
   // alert(id)
    if (id != '') {
        var option_html = ''
        $('.quantity_hidden').val('');
        $('#quantity').val('');
        $.ajax({
            url: url + "tag_products/get_parent_category",
            type: "POST",
            dataType: "json",
            data: {karigar_id: id},
            success: function (data, textStatus, jqXHR)
            {
                console.log(data.karigar_ids)
                if (data.status == 'failure')
                {
                    showMsgPopup("Error", "No Products Founds", '', 'error')

                } else if (data.status == "success") {
                    option_html += '<option value="">Select Product</option>'
                    $.each(data.karigar_ids, function (key, val) {
                        option_html += '<option value="' + val['sub_code'] + "-" + val['weight_range_id'] + '">' + val['name'] + ' (' + val['from_weight'] + ' - ' + val['to_weight'] + ')</option>'
                    })
                    $('.product_name').html(option_html);
                    get_quantity_by_qc('0');
                } else {
                    // showMsgPopup("Error","Something Went Wrong Try Again",'','error')
                    //    setTimeout(function(){
                    //        close_toast_popup(url+'hallmarking_list')

                    //    },2000)
                }
            },
            error: function (jqXHR, textStatus, errorThrown)
            {

            }
        });
    }
}
var tag_products_array = [];
function save_tag_products() {
    $.ajax({
        url: url + "tag_products/store?status=" + get,
        type: "POST",
        dataType: "json",
        data: $('form#department_order').serialize(),
        success: function (data, textStatus, jqXHR)
        {
            if (data.status == 'failure')
            {
                error_msg(data.error);
            } else if (data.status == "success") {
                $('#first_tr').remove();
                //console.log(data.id);
                var table = '<tr id="trid_' + tr_count + '">';
                table += '<td>' + data.karigar_name + '</td>';
                table += '<td>' + data.sub_category + '</td>';
                table += '<td>' + data.product_code + '-' + data.id + '</td>';
                table += '<td>' + data.net_wt + '</td>';
                table += '<td>' + data.quantity + '</td>';
                // table += '<td><button onclick="remove_tag_product('+tr_count+','+data.id+','+data.qc_id+')" type="button" name="commit" class="btn btn-danger small loader-hide  btn-sm">REMOVE</button></td>';
                table += '</tr>';
                tag_products_array.push(data.id);
                $('#tag_products_added').val(tag_products_array);
                if ($('#tag_products tr').length > 1) {
                    $('#tag_products tbody').append(table);
                } else {
                    $('#tag_products tbody').html(table);
                }
            } else {
                // showMsgPopup("Error","Something Went Wrong Try Again",'','error')
                //    setTimeout(function(){
                //        close_toast_popup(url+'hallmarking_list')

                //    },2000)
            }
        },
        error: function (jqXHR, textStatus, errorThrown)
        {

        }
    });
}
var test_array = {};
var max_qty_tag_product = 0;
var max_count = 0;
var karigar_name = '';
var code_name = '';
var tag_data = "";
var parent_category_code = "";
var billing_cat_obj = "";

function get_quantity_by_qc(id) {   
    if (id != '') {
        $.ajax({
            url: url + "tag_products/get_qc_details",
            type: "POST",
            dataType: "json",
            data: {qc_id: id, 'karigar_id': $('#kariger_id').val(), 'parent_category': $('#parent_category_id').val()},
            success: function (data, textStatus, jqXHR)
            {
                ////console.log(data)
                var table_html = '';
                var select_html = '';
                tag_data = data.data;
                max_count = parseInt(data.qc.remaining_qty_tag);
                // var parent_category_code=JSON.parseInt(tag_data.code);
                //parent_category_code= data.code;
                billing_cat_obj = data.billing_cat;
                $('#quantity').val(data.qc.remaining_qty_tag);
                $('#quantity_hidden').val(data.qc.remaining_qty_tag);
                max_qty_tag_product = data.qc.remaining_qty_tag;
                //test_array=parent_category_code;
                var inc = 0;
                // var billing_cat_html = '<option value="">Select Item Category</option>';
                //  $.each(data.billing_cat,function(billing_key,billing_val){
                //     billing_cat_html+='<option value="'+billing_val.id+'">'+billing_val.name+'</option>';
                // });

                //console.log(parent_category_code);
                $.each(data.data, function (key, val) {
                    //console.log(val.sub_code)
                    inc++;
                    karigar_name = val.karigar_name;
                    code_name = val.sub_category;
                    select_html += '<option value="">Select Code</option>';
                    $.each(val.billing_cat, function (key1, val1) {
                        //console.log(val1.max_count)
                        select_html += '<option data-code="' + val1.code + '" data-max_cnt="' + val1.max_count + '" value="' + val1.id + '">' + val1.name + '</option>';
                    });

                    table_html += '<tr class="from-group abc"><input type="hidden" name="qc_id" value="' + val.qc_id + '"><input type="hidden" name="remaining_qty_tag" value="' + data.qc.remaining_qty_tag + '">';
                    table_html += '<td><span style="float:right">' + (key + 1) + '</span></td>';
                    // table_html +='<td>'+val.karigar_name+'</td>';
                    table_html += '<td><input type="hidden" name="tag_products[' + key + '][sub_code]" value="' + val.sub_code + '">' + val.sub_category + '</td>';
                    table_html += '<td> <span style="float:right">' + val.from_weight + '-' + val.to_weight + '</span></td>';
                    table_html += '<td><select onchange="update_tag_product_code(this,' + key + ')" class="form-control item_cat" data-product_code="' + val.sub_code + '" id="code_' + key + '" name="tag_products[' + key + '][billing_category_id]">' + select_html + '</select><span class="text-danger" id="billing_category_id_error_' + key + '"></span></td>';
                    table_html +='<td><input type="text" class="form-control" name="tag_products[' + key + '][gr_wt]" id="gross_wt' + key + '" onkeyup="copy_mfg_gr_wt(' + key + ')"><span class="text-danger" id="gr_wt_error_' + key + '"></span></td>';
       
                    table_html +='<input type="hidden" name="tag_products[' + key + '][item_category_code]" class="item_category_code" value="">';

                    table_html += '<td><input type="hidden" name="tag_products[' + key + '][department_id]" value="' + val.department_id + '"><input type="hidden" name="tag_products[' + key + '][id]" value="' + val.id + '"><input type="text" class="form-control" name="tag_products[' + key + '][net_weight]" id="net_wt' + key + '"><span class="text-danger" id="net_weight_error_' + key + '"></span></td>';

                
                    table_html += '<td><input tag-code="' + val.sub_code + '" class="form-control product_code_tp" id="product_code_' + key + '" name="tag_products[' + key + '][product_code]" readonly="true"></td>';

                    table_html += '<td><a id="add_more" class="btn btn-danger small loader-hide  btn-sm" href="javascript:void(0)" onclick="remove_tag_code(this)" title="Add More">Remove</a></td>'
                    select_html = '';
                    /*set some value*/
                });
                // /**/
                // console.log(table_html)
                $('.added_cat').show();
                $('#tag_products_tbl tbody').html('');
                $('#tag_products_tbl tbody').append(table_html);
                // change_product_code();

            },
            error: function (jqXHR, textStatus, errorThrown)
            {

            }
        });
    }
}
function update_tag_product_code(obj, id) {
    var tag_product_obj = {};
    var count = parseInt(count);
    $(".abc").each(function (key, value) {
        var sel_item_code = $(this).closest('tr').find('.item_cat :selected').attr('data-code');
        var sel_item_cnt = $(this).closest('tr').find('.item_cat :selected').attr('data-max_cnt');
        var sel_product_code = $(this).closest('tr').find('.item_cat').attr('data-product_code');

        if (typeof (sel_item_code) != 'undefined' && typeof (sel_item_code) != null) {
            if (typeof (tag_product_obj[sel_item_code]) == 'undefined' && typeof (tag_product_obj[sel_item_code]) != null) {
                var f_cnt = parseInt(sel_item_cnt);
                tag_product_obj[sel_item_code] = f_cnt;
                //alert($(this).closest('tr').find('.product_code_tp').val(sel_product_code + '' + sel_item_code + '-' + f_cnt));
                $(this).closest('tr').find('.product_code_tp').val(sel_product_code + '' + sel_item_code + '-' + f_cnt);
                //$(this).closest('tr').find('.product_code_tp').val(sel_item_code+sel_product_code+'-'+f_cnt);
            } else {
                var cnt = parseInt(tag_product_obj[sel_item_code]);
                tag_product_obj[sel_item_code] = cnt + 1;
                $(this).closest('tr').find('.product_code_tp').val(sel_product_code + '' + sel_item_code + '-' + tag_product_obj[sel_item_code]);
                //$(this).closest('tr').find('.product_code_tp').val(sel_item_code+sel_product_code+'-'+tag_product_obj[sel_item_code]);              
            }

        } else {
            $(this).closest('tr').find('.product_code_tp').val('');
        }
         $(this).closest('tr').find('.item_category_code').val(sel_item_code);
    })
}
function update_tag_product_ajax(item_code, product_code) {
    $.ajax({
        url: url + "tag_products/max_count_category",
        type: "POST",
        dataType: "json",
        data: {product_code: product_code, item_code: item_code},
        success: function (data, textStatus, jqXHR)
        {
            console.log(data)
            update_tag_code(data, item_code, product_code);
        }
    });
}
function update_tag_code(count, item_code, product_code) {
    var count = parseInt(count);
    $(".abc").each(function (key, value) {
        var sel_item_code = $(this).closest('tr').find('.item_cat :selected').attr('data-code');
        var sel_product_code = $(this).closest('tr').find('.item_cat').attr('data-product_code');
        if (sel_item_code == item_code && sel_product_code == product_code) {
            count++;
            $(this).closest('tr').find('.product_code_tp').val(item_code + '' + product_code + '-' + count);
        }
          $(this).closest('tr').find('.item_category_code').val(item_code);
    })
}


function mauf_hallmarking_qc() {
    //  var a = $('#manuf_table_body input:checkbox:checked').length;
//var selected_products = $('#add_selected_hallmark').val();
    /*    if (a == 0 || a == undefined) {
     //if ($('#hallmarking_qc input[type="checkbox"]').not(':checked').length == 0) {
     showMsgPopup("Error","Select Products",'','error')
     }else{*/
    // $("#hallmarking_qc").submit();
    $('.recieve_fm_hm').prop('disabled', true);
    $.ajax({
        url: url + "Manufacturing_hallmarking/store",
        type: "POST",
        dataType: "json",
        // data :{all_check_qc:selected_products,type:'qc_hallmarking'},
        data: $('form#hallmarking_qc').serialize(),
        success: function (data, textStatus, jqXHR)
        {
            if (data.status == "success") {
                showMsgPopup("Success", "Hallmarking Qc Done", '', 'normal')
                setTimeout(function () {
                    close_toast_popup(url + 'Manufacturing_hallmarking?status=send')

                }, 2000)

            } else if (data.status == "failure") {
                $('.send_to_kun_karigar').prop('disabled', false);
                $('.recieve_fm_hm').prop('disabled', false);
                showMsgPopup("Error", "Please Enter at least one product value", '', 'error')

            } else if (data.status == "failure1") {
                $('.recieve_fm_hm').prop('disabled', false);
                showMsgPopup("Error", "Please Enter at least one product value", '', 'error')
            } else {
                $('.recieve_fm_hm').prop('disabled', false);
                showMsgPopup("Error", "Something Went Wrong Try Again", '', 'error')
                setTimeout(function () {
                    close_toast_popup(url + 'Manufacturing_hallmarking?status=send')

                }, 2000)
            }
        },
        error: function (jqXHR, textStatus, errorThrown)
        {

        }
    });
    /* }*/

}

function send_to_kundan_karigar(id, quantity) {
    $('#kundan_qnty').val(quantity);
    $('#msqr').val(id);
    $('#send_to_kundan_karigar').modal('show');
}


function send_to_karigar() {
    $('.send_to_kun_karigar').prop('disabled', true);
    var karigar_id = $('#kundan_karigar_id').val();
    $.ajax({
        url: url + "Prepare_kundan_karigar_order/store/" + karigar_id,
        type: "POST",
        dataType: "json",
        data: $('form#make_set').serialize(),
        success: function (data, textStatus, jqXHR)
        {
            if (data.status == 'failure')
            {
                page_reload = false;
                //console.log(data.error)
                showMsgPopup("Error", data.error.quantity, '', 'error')
                $('.send_to_kun_karigar').prop('disabled', false);

            } else if (data.status == "success") {
                //$('#barcode_submit').prop('disabled', false);
                showMsgPopup("Success", "Sent product to kundan karigar", '', 'normal')
         /*       setTimeout(function () {
                    location.reload();
                }, 500)*/
                window.location = url + "Prepare_kundan_karigar_order/download_pdf/1";
                //window.open(url+"Prepare_kundan_karigar_order/download_pdf/1", "_blank")
            } else {
                //$('#barcode_submit').prop('disabled', false);
                showMsgPopup("Error", "Something Went Wrong Try Again", '', 'error')
            }
        },
        error: function (jqXHR, textStatus, errorThrown)
        {

        }
    });
}

function remove_tag_product(tr_id, tag_id, qc_id) {
    $.ajax({
        url: url + "tag_products/remove",
        type: "POST",
        dataType: "json",
        data: {id: tag_id, qc_id: qc_id},
        success: function (data, textStatus, jqXHR)
        {
            if (data.status == "success") {
                var index = tag_products_array.indexOf(tag_id);
                tag_products_array.splice(index, 1);
                $('#tag_products_added').val(tag_products_array);
                $('#trid_' + tr_id).remove();

            } else {
                showMsgPopup("Error", "Something Went Wrong Try Again", '', 'error')
                setTimeout(function () {
                }, 2000)
            }
        },
        error: function (jqXHR, textStatus, errorThrown)
        {

        }
    });
}
function assign_kundan_karigar(id) {
    $.ajax({
        url: url + "kundan_karigar/update",
        type: "POST",
        dataType: "json",
        data: {chitti_no: id},
        success: function (data, textStatus, jqXHR)
        {
            if (data.status == 'failure')
            {
                //showMsgPopup("Error","No Products Founds",'','error')

            } else if (data.status == "success") {
                showMsgPopup("Success", "Sent To Karigar", '', 'normal')
                setTimeout(function () {
                    location.reload();

                }, 500)
            } else {
                showMsgPopup("Error", "Something Went Wrong Try Again", '', 'error')
            }
        },
        error: function (jqXHR, textStatus, errorThrown)
        {

        }
    });
}
function receive_kundan_karigar(id) {
    $.ajax({
        url: url + "kundan_karigar/send_to_receive_products/"+id,
        type: "POST",
        dataType: "json",
        //data: $('form#receive_kun_prod').serialize(),
        data : {id:id,quantity:1,kundan_pc:$('#kundan_pc_'+id).val(),k_gold:$('#k_gold_'+id).val(),kundan_rate:$('#kundan_rate_'+id).val(),kundan_amt:$('#kundan_amt_'+id).val(),net_wt:$('#net_wt_'+id).val(),gross_wt:$('#gross_wt_'+id).val()},
        success: function (data, textStatus, jqXHR)
        {
            if (data.status == 'failure')
            {
                //console.log(data.error);
               error_msg(data.error);
                  

            } else if (data.status == 'failure1')
            {
                showMsgPopup("Error", data.error, '', 'error')

            } else if (data.status == "success") {
                showMsgPopup("Success", "Successfully Received", '', 'normal')
                setTimeout(function () {
                    location.reload();

                }, 500)
            } else {
                showMsgPopup("Error", "Something Went Wrong Try Again", '', 'error')
            }
        },
        error: function (jqXHR, textStatus, errorThrown) {
        }
    });
}
function Print_receipt() {
    $.ajax({
        url: url + "karigar_receive_order/validate",
        type: "POST",
        dataType: "json",
        data: $('form#karigar_receive_order').serialize(),
        success: function (data, textStatus, jqXHR)
        {
            if (data.status == 'failure')
            {
                error_msg(data.error);

            } else if (data.status == "success") {
                $("form#karigar_receive_order").submit();
            } else {
                showMsgPopup("Error", "Something Went Wrong Try Again", '', 'error')
            }
        },
        error: function (jqXHR, textStatus, errorThrown)
        {

        }
    });
}
function print_tag_products() {

    var conf = confirm("Are you sure you want to tag products?");
    if (conf)
    {
    $.ajax({
        url: url + "tag_products/store",
        type: "POST",
        dataType: "json",
        data: $('form#tag_products').serialize(),
        success: function (data, textStatus, jqXHR)
        {
            console.log(data.status)
            if (data.status == "success") {
                //window.location.href = url + 'tag_products/make_pdf/' + data.qc_id
                //window.open(url+'tag_products/make_pdf/'+data.qc_id, "_blank")
                
                 showMsgPopup("Success", "Successfully Tagged", '', 'normal')
                       

                setTimeout(function () {
                    close_toast_popup(url + 'tag_products')

                }, 2000)
            } else if (data.status == "failure") {
                $('.text-danger').html('');
                $.each(data.error, function (key, val) {
                    $.each(val, function (key1, val1) {
                        $('#' + key1).html(val1);
                    })
                })
            } else if (data.status == 'failure1') {
                //showMsgPopup("Error","Please Enter at least one product value",'','error')
                showMsgPopup("Error", data.error, '', 'error')
            } else {
                showMsgPopup("Error", "Something Went Wrong Try Again", '', 'error')
            }
        },
        error: function (jqXHR, textStatus, errorThrown)
        {

        }

    });
     }else{
          $('#b_' + id).attr('disabled', false);
          return false;

    }
}


function filter_karigar(controller) {
    //window.location.href=url+'/Print_Order/index/'+$('#karigar_id_filter').val()+'?order_id='+$('#order_filter').val();

    var product_code_html = '';
    $.ajax({
        type: "POST",
        data: {order_id: $("#order_filter").val(), karigar_id: $('#karigar_id_filter').val()},
        dataType: "json",
        url: url + controller + "/get_product_code",
        cache: false,
        success: function (data) {
            if (data.status == 'success') {
                var selected = '';
                var product_code = $('#pdf_product_filter').val();
                var product_filter_code = product_code.split(",");

                $.each(data.result, function (i, obj) {
                    // if ($.inArray(obj.product_code.toString(), ["RNGMSTAXX0628","RNGMSTAXX0642","PSTFCYAEX0021"]) > -1)
                    /*        if ($.inArray(obj.product_code.toString(),code) > -1)
                     {
                     selected='selected';
                     }else{
                     selected=''; 
                     }*/

                    product_code_html += '<option value="' + obj.product_code + '" ' + selected + '>' + obj.product_code + '</option>';
                });
                // $("#product_code_filter55").html(product_code_html);
                $("select[multiple].select_active.3col").multiselect("reload");
                $("#product_code_filter").html(product_code_html);
                $('select[multiple].select_active.3col').multiselect({
                    search: true,
                    searchOptions: {
                        'default': 'Search Design Code'
                    },
                    selectAll: true
                });
                $('select[multiple].select_active.3col').multiselect('reload');

            } else {
                $("#product_code_filter").html(product_code_html);
            }
        }
    });
}
function Prepare_kundan_karigar_order() {
    // if($("input[type='checkbox']").is(":checked") == false){
    //    showMsgPopup("Error","Please select product for Kundan Karigar",'','error');
    //    return false;
    // }
    var checkedValues = $('input:checkbox:checked').map(function () {
        return this.value;
    }).get();

    var error = 0;
    if (checkedValues.length == 0) {
        error = 1;
        showMsgPopup("Error", "Please select product for Kundan Karigar", '', 'error');
    }
    // $.each(checkedValues, function( key, value ) {
    //     var new_qty = $('#newqty_'+value).val();
    //     var old_qty = $('#qty_'+value).val();
    //     var pc_code = $('#pr_name_'+value).text();
    //         if (new_qty > old_qty) {
    //              error=1;
    //          showMsgPopup("Error","Quantity Should be Less Than "+old_qty+" for "+pc_code,'','error')
    //         }
    //      });
    if (error == 0) {
        $('#send_to_kundan_karigar').modal('show');
    }

}
function change_delivery_date(order_id,delievery_date){  

    $('#order_id').val(order_id);
    $('#change_delivery_date').val(delievery_date);  
    $('#change_delivery_date_modal').modal('show');
    }


function sale_tp_manufacture(id, status) {

    $.ajax({
        url: url + "Prepared_order/send_to_manufacture",
        type: "POST",
        dataType: "json",
        data: {id: id, status: status},
        success: function (data, textStatus, jqXHR)
        {
            if (data.status == "success") {
                if (status == 1) {
                    showMsgPopup("Success", "Sent To Manufacturing Department", '', 'normal')
                    var redirect = "";
                } else {
                    showMsgPopup("Success", "Successfully Cancelled", '', 'normal')
                    var redirect = "?status=sent";
                }

                setTimeout(function () {
                    close_toast_popup(url + 'Prepared_order' + redirect)

                }, 2000)
                // if (status==0){
                //     $('#btn_'+id).closest('tr').remove();
                // }else{
                //     $('#btn_'+id).closest('td').html('Already sent');
                // }

            } else {
                showMsgPopup("Error", "Something Went Wrong Try Again", '', 'error')
            }
        },
        error: function (jqXHR, textStatus, errorThrown)
        {

        }
    });
}

function ready_products(id) { /*used in tag products create*/

    $('.ready_products_btn').prop('disabled', true);
    var checkedValues = $('input:checkbox:checked').map(function () {
        return this.value;
    }).get();
    var error = 0;
    if (checkedValues.length == 0) {
        showMsgPopup("Error", "Please Select Product", '', 'error')
         $('.ready_products_btn').prop('disabled', false);
        error = 1;
    }



    // $.each(checkedValues, function( key, value ) {
    //     var new_qty = $('#newqty_'+value).val();
    //     var old_qty = $('#qty_'+value).val();
    //     var pc_code = $('#pr_name_'+value).text();
    //         if (new_qty > old_qty) {
    //              error=1;
    //          showMsgPopup("Error","Quantity Should be Less Than "+old_qty+" for "+pc_code,'','error')
    //         }
    //      });

    if (error == 0) {
        $.ajax({
            url: url + "Ready_product/store",
            type: "POST",
            dataType: "json",
            data: $('form#make_set').serialize(),
            success: function (data, textStatus, jqXHR)
            {
                if (data.status == "success") {
                    $('.ready_products_btn').prop('disabled', true);
                    showMsgPopup("Success", "Addded as Ready Product", '', 'normal')
                    // window.open(data.data.url, "_blank")
                    setTimeout(function () {
                        // window.location=url+"Ready_product/print_tag_products";
                        location.reload();

                    }, 500)


                } else {
                    $('.ready_products_btn').prop('disabled', false);
                    showMsgPopup("Error", "Something Went Wrong Try Again", '', 'error')
                }
            },
            error: function (jqXHR, textStatus, errorThrown)
            {

            }
        });
    }
}

function save_msqr_image(msqr_id) {
    var formData = new FormData(document.querySelector("form"));
    formData.append('msqr_id', msqr_id);
    $.ajax({
        url: url + "Make_set/store_image",
        type: "POST",
        dataType: "json",
        data: formData,
        processData: false,
        contentType: false,
        success: function (data, textStatus, jqXHR)
        {
            if (data.status == "success") {
                $('#img_' + msqr_id).attr("src", data.image);
                showMsgPopup("Success", "Addded as Ready Product", '', 'normal')
            } else {
                showMsgPopup("Error", "Something Went Wrong Try Again", '', 'error')
            }
        },
        error: function (jqXHR, textStatus, errorThrown)
        {

        }
    });
}

function kundan_karigar_QC() {
    if ($("input[name='Qc[]']:checked").val() == undefined) {
        showMsgPopup("Error", "Please select product for QC", '', 'error');
        return false;
    }
    $("#qc_products").submit();
}


function kundan_qc_check() {
    $.ajax({
        url: url + "kundan_QC/store",
        type: "POST",
        dataType: "json",
        data: $('form#qc_check').serialize(),
        success: function (data, textStatus, jqXHR)
        {
            if (data.status == 'failure')
            {
                if (data.weight != "") {
                    showMsgPopup("Error", data.error.weight, '', 'error')
                } else {
                    showMsgPopup("Error", data.error.quantity, '', 'error')
                }


            } else if (data.status == "success") {
                showMsgPopup("Success", "Successfully Accepted", '', 'normal')
                setTimeout(function () {
                    close_toast_popup(url + 'kundan_QC?status=pending')

                }, 500)
            } else {
                showMsgPopup("Error", "Something Went Wrong Try Again", '', 'error')
            }
        },
        error: function (jqXHR, textStatus, errorThrown)
        {

        }
    });
}

function kundan_send_qc_mfg(rp_id) {
    $('#s_' + rp_id).attr('disabled', true);
    $.ajax({
        url: url + "kundan_QC/send_kundan_qc/" + rp_id,
        type: "POST",
        dataType: "json",
        // data : $('form#qc_check').serialize(),
        // data :{id:rp_id},
        success: function (data, textStatus, jqXHR)
        {
            if (data.status == 'failure')
            {
                $('#s_' + rp_id).attr('disabled', false);
                if (data.weight != "") {
                    showMsgPopup("Error", data.error.weight, '', 'error')
                } else {
                    showMsgPopup("Error", data.error.quantity, '', 'error')
                }


            } else if (data.status == "success") {
                showMsgPopup("Success", "Successfully Accepted", '', 'normal')
                setTimeout(function () {
                    close_toast_popup(url + 'kundan_QC?status=send_qc_pending')

                }, 500)
            } else {
                showMsgPopup("Error", "Something Went Wrong Try Again", '', 'error')
                $('#s_' + rp_id).attr('disabled', false);
            }
        },
        error: function (jqXHR, textStatus, errorThrown)
        {

        }
    });
}

function kundan_qc_check_accept(rp_id) {
    $('#s_' + rp_id).attr('disabled', true);
    $.ajax({
        url: url + "kundan_QC/accept/" + rp_id,
        type: "POST",
        dataType: "json",
        // data : $('form#qc_check').serialize(),
        // data :{id:rp_id},
        success: function (data, textStatus, jqXHR)
        {
            if (data.status == 'failure')
            {
                $('#s_' + rp_id).attr('disabled', false);
                if (data.weight != "") {
                    showMsgPopup("Error", data.error.weight, '', 'error')
                } else {
                    showMsgPopup("Error", data.error.quantity, '', 'error')
                }


            } else if (data.status == "success") {
                showMsgPopup("Success", "Successfully Accepted", '', 'normal')
                setTimeout(function () {
                    close_toast_popup(url + 'kundan_QC?status=pending')

                }, 500)
            } else {
                showMsgPopup("Error", "Something Went Wrong Try Again", '', 'error')
                $('#s_' + rp_id).attr('disabled', false);
            }
        },
        error: function (jqXHR, textStatus, errorThrown)
        {

        }
    });
}
function get_receive_product_details(id) {
    if (id == "")
        return false;

    $.ajax({
        url: url + "karigar_receive_order/get_details/" + id,
        type: "POST",
        dataType: "json",
        data: {is_kundan: $('#is_kundan').val()},
        success: function (data, textStatus, jqXHR)
        {
            console.log(data);
            var product_html = '';
            var few_wt = '';
            var gr_w = '';
            $.each(data, function (key, val) {

                if (val['few_wt'] == null) {
                    few_wt = '';
                } else {
                    few_wt = val['few_wt'];
                }
                if(val['gross_wt'] == null){

                 gr_w = parseFloat(val['net_wt'])+parseFloat(val['kundan_pure']);
                }else{
                    gr_w =val['gross_wt'];
                }
                product_html += "<tr>";
                product_html += '<td class="col1">' + val['name'] + '</td><input type="hidden" name="name[]" value="' + val['name'] + '">';
                product_html += '<td class="col1">' + val['product_code'] + '</td><input type="hidden" name="product_code[]" value="' + val['product_code'] + '">';
                product_html += '<td class="col1"> <span style="float:right">' + val['quantity'] + '</span></td><input type="hidden" name="quantity[]" value="' + val['quantity'] + '"><input type="hidden" name="department_id[]" value="' + val['department_id'] + '"><input type="hidden" name="category_id[]" value="' + val['category_id'] + '"><input type="hidden" name="product_id[]" value="' + val['product_id'] + '"><input type="hidden" name="item_category_code[]" value="' + val['item_category_code'] + '">';

                
               product_html += '<td><input type="text" min="0" placeholder="" id="net_wt_' + key + '" onkeyup="calculate_wax_wt(' + key + ',this)" class="form-control" value="' + val['net_wt'] + '" name="net_wt[]"></td><td><input onkeyup="calculate_wax_wt(' + key + ',this)" type="text" min="0" placeholder="" id="few_wt_' + key + '" class="form-control" name="few_wt[]" value=' + few_wt + '></td><td><input onkeyup="calculate_wax_wt(' + key + ',this)" type="text" min="0" placeholder="" id="kundan_pure_' + key + '" class="form-control" onkeyup="calculate_wax_wt(' + key + ',this)" name="kundan_pure[]"  value="' + val['kundan_pure'] + '" readonly></td><td><input type="text" min="0" placeholder="" id="mina_wt_' + key + '" class="form-control" onkeyup="calculate_wax_wt(' + key + ',this)" name="mina_wt[]" value="' + isnan_chk(val['mina_wt']) + '"></td><td><input type="text" min="0" placeholder="" id="wax_wt_' + key + '" class="form-control"  name="wax_wt[]" value="' + isnan_chk(val['wax_wt']) + '" readonly></td><td><input type="text" min="0" placeholder="" id="color_stone_' + key + '" class="form-control" onkeyup="calculate_wax_wt(' + key + ',this)" name="color_stone[]" value="' + isnan_chk(val['color_stone']) + '"></td><td><input type="text" min="0" placeholder="" id="moti_' + key + '" class="form-control" onkeyup="calculate_wax_wt(' + key + ',this)" name="moti[]" value="' + isnan_chk(val['moti']) + '"></td><td><input type="text" min="0" placeholder="" id="checker_wt_' + key + '" class="form-control" onkeyup="calculate_wax_wt(' + key + ',this)" name="checker_wt[]" value="' + isnan_chk(val['checker_wt']) + '" ></td><td><input type="text" min="0" placeholder="" id="black_beads_wt_' + key + '" class="form-control" name="black_beads_wt[]" onkeyup="calculate_wax_wt(' + key + ',this)" value="' + isnan_chk(val['black_beads_wt']) + '" ></td><td><input type="hidden" placeholder="" class="form-control" name="receive_product_id[]" value="' + val['receive_product_id'] + '"><input type="hidden" placeholder="" class="form-control" name="rp_id[]" value="' + val['rp_id'] + '"><input type="text" min="0" placeholder="" id="ttl_gross_wt_' + key + '" class="form-control" name="gross_wt[]" value="'+isnan_chk(gr_w)+'" onkeyup="calculate_wax_wt(' + key + ',this)"></td><td><input type="text" min="0" placeholder="" onkeyup="calculate_checker_amt(' + key + ',this)" id="checker_' + key + '" class="form-control" name="checker[]" value="' + isnan_chk(val['checker']) + '"></td><td><input  type="text" min="0" placeholder="" id="checker_pcs_' + key + '" class="form-control" onkeyup="calculate_checker_amt(' + key + ',this)" name="checker_pcs[]" value="' + isnan_chk(val['checker_pcs']) + '"></td><td><input type="text" min="0" placeholder="" id="checker_amt_' + key + '" class="form-control" name="checker_amt[]"  value="' + isnan_chk(val['checker_amt']) + '" readonly></td><td><input type="text" min="0" placeholder="" onkeyup="calculate_kun_amt(' + key + ',this)" id="kundan_pc_' + key + '" class="form-control" name="kundan_pc[]" value=' + isnan_chk(val['kundan_pc']) + '></td><td><input  type="text" min="0" placeholder="" id="kundan_rate_' + key + '" class="form-control" onkeyup="calculate_kun_amt(' + key + ',this)" name="kundan_rate[]" value=' + isnan_chk(val['kundan_rate']) + '></td><td><input type="text" min="0" placeholder="" id="kundan_amt_' + key + '" class="form-control" name="kundan_amt[]" onkeyup="calculate_total_amt(' + key + ',this)" value=' + isnan_chk(val['kundan_amt']) + '></td><td><input  type="text" min="0" placeholder="" id="stone_wt_' + key + '" class="form-control" name="stone_wt[]" value="' + isnan_chk(val['stone_wt']) + '"></td><td><input type="text" min="0" placeholder="" id="color_stone_rate_' + key + '" class="form-control" name="color_stone_rate[]" onkeyup="calculate_wax_wt(' + key + ',this)" value="' + isnan_chk(val['color_stone_rate']) + '"></td><td><input type="text" min="0" placeholder="" id="stone_amt_' + key + '" class="form-control" name="stone_amt[]" onkeyup="calculate_total_amt(' + key + ',this)" value="' + isnan_chk(val['stone_amt']) + '"></td><td><input type="text" min="0" placeholder="" id="other_wt_' + key + '" class="form-control" name="other_wt[]" onkeyup="calculate_total_amt(' + key + ',this)" value="' + isnan_chk(val['other_wt']) + '" ></td><td><input type="text" min="0" placeholder="" id="ttl_wt_' + key + '" class="form-control" name="total_wt[]" value="' + isnan_chk(val['total_wt']) +'" readonly></td><td><button class="btn btn-danger waves-effect waves-light  btn-sm" onclick="remove_tr(this)" name="commit" type="button">REMOVE</button></td>';
                product_html += '</tr>';
            });
            $("#receive_products > tbody:last").html(product_html);
        },
        error: function (jqXHR, textStatus, errorThrown)
        {

        }
    });
}
function calculate_wax_wt(id, evt) {
    evt.value = evt.value.replace(/[^0-9.]/g, '');
    var ttl_gross_wt = parseFloat($('#ttl_gross_wt_' + id).val()) || 0;
    var net_wt = parseFloat($('#net_wt_' + id).val()) || 0;
    var few_wt = parseFloat($('#few_wt_' + id).val()) || 0;
    var kundan_pure = parseFloat($('#kundan_pure_' + id).val()) || 0;
    var kundan_amt = parseFloat($('#kundan_amt' + id).val()) || 0;
    var mina_wt = parseFloat($('#mina_wt_' + id).val()) || 0;  
    var color_stone = parseFloat($('#color_stone_' + id).val()) || 0;
    var moti = parseFloat($('#moti_' + id).val()) || 0;
    var checker_wt = parseFloat($('#checker_wt_' + id).val()) || 0;
    var color_stone_rate = parseFloat($('#color_stone_rate_' + id).val()) || 0;
    var black_beads_wt = parseFloat($('#black_beads_wt_' + id).val()) || 0;
    var ttl_wt = ttl_gross_wt - net_wt - kundan_pure - few_wt - mina_wt -  color_stone - moti - checker_wt - black_beads_wt - kundan_amt;

    var wax_wt_total =  ttl_wt.toFixed(2);
    $('#wax_wt_' + id).val(wax_wt_total);
    var stone_wt_total=color_stone+moti;
    $('#stone_wt_' + id).val(stone_wt_total);


}
function calculate_grs_wt(id, evt) {
    evt.value = evt.value.replace(/[^0-9.]/g, '');
    var net_wt = parseFloat($('#net_wt_' + id).val()) || 0;
    var few_wt = parseFloat($('#few_wt_' + id).val()) || 0;
    var kundan_pure = parseFloat($('#kundan_pure_' + id).val()) || 0;
    var mina_wt = parseFloat($('#mina_wt_' + id).val()) || 0;
    var wax_wt = parseFloat($('#wax_wt_' + id).val()) || 0;
    var color_stone = parseFloat($('#color_stone_' + id).val()) || 0;
    var moti = parseFloat($('#moti_' + id).val()) || 0;
    var checker_wt = parseFloat($('#checker_wt_' + id).val()) || 0;
    var color_stone_rate = parseFloat($('#color_stone_rate_' + id).val()) || 0;
    var black_beads_wt = parseFloat($('#black_beads_wt_' + id).val()) || 0;
    var ttl_wt = net_wt + few_wt + mina_wt + wax_wt + color_stone + moti + checker_wt + kundan_pure+black_beads_wt;
    var stone_wt = color_stone + moti;
    var stone_amt = color_stone_rate * stone_wt;
    $('#stone_amt_' + id).val(stone_amt);
    var gr_wt_total =  ttl_wt.toFixed(2);
    $('#ttl_gross_wt_' + id).val(gr_wt_total);
    $('#stone_wt_' + id).val(stone_wt);
    $('#ttl_wt_' + id).val(stone_amt);;

}
function calculate_total_amt(id, evt) {
    evt.value = evt.value.replace(/[^0-9.]/g, '');
    // var kundan_wt= parseFloat($('#kundan_wt_'+id).val()) || 0;

    var checker_amt = parseFloat($('#checker_amt_' + id).val()) || 0;
    var kundan_amt = parseFloat($('#kundan_amt_' + id).val()) || 0;
    var stone_amt = parseFloat($('#stone_amt_' + id).val()) || 0;
    var other_wt = parseFloat($('#other_wt_' + id).val()) || 0;
    //var color_stone_rate= parseFloat($('#color_stone_rate_'+id).val()) || 0;
    var ttl_wt = checker_amt + kundan_amt + stone_amt + other_wt;
    $('#ttl_wt_' + id).val(ttl_wt);

}
function calculate_kun_amt(id, evt) {
    evt.value = evt.value.replace(/[^0-9.]/g, '');
    var checker_amt = parseFloat($('#checker_amt_' + id).val()) || 0;
    var kundan_rate = parseFloat($('#kundan_rate_' + id).val()) || 0;
    var kun_pc = parseFloat($('#kundan_pc_' + id).val()) || 0;
    var stone_amt = parseFloat($('#stone_amt_' + id).val()) || 0;

    var kun_amt = kun_pc * kundan_rate;
    $('#kundan_amt_' + id).val(kun_amt);
    $('#ttl_wt_' + id).val(kun_amt+checker_amt+stone_amt);
    

}

function calculate_checker_amt(id, evt) {
    evt.value = evt.value.replace(/[^0-9.]/g, '');
    var checker = parseFloat($('#checker_' + id).val()) || 0;
    var checker_pcs = parseFloat($('#checker_pcs_' + id).val()) || 0;
    var checker_amt = checker_pcs * checker;
    $('#checker_amt_' + id).val(checker_amt);
    $('#ttl_wt_' + id).val(checker_amt);
}
/*Add to stock functionality calculation start*/


function calculate_grs_wt_stock(evt) {
    evt.value = evt.value.replace(/[^0-9.]/g, '');
    var net_wt = parseFloat($(evt).closest('tr').find('.net_wt').val()) || 0;
    var few_wt = parseFloat($(evt).closest('tr').find('.few_wt').val()) || 0;
    var kundan_pure = parseFloat($(evt).closest('tr').find('.kundan_pure').val()) || 0;
    var mina_wt = parseFloat($(evt).closest('tr').find('.mina_wt').val()) || 0;

    var wax_wt = parseFloat($(evt).closest('tr').find('.wax_wt').val()) || 0;
    var moti = parseFloat($(evt).closest('tr').find('.moti').val()) || 0;
    var checker_wt = parseFloat($(evt).closest('tr').find('.checker_wt').val()) || 0;
    var black_beads_wt = parseFloat($(evt).closest('tr').find('.black_beads_wt').val()) || 0;
    var color_stone = parseFloat($(evt).closest('tr').find('.color_stone').val()) || 0;
    var color_stone_rate = parseFloat($(evt).closest('tr').find('.color_stone_rate').val()) || 0;
    var ttl_wt = net_wt + few_wt + mina_wt + wax_wt + color_stone + moti + checker_wt + kundan_pure+ black_beads_wt;
    var stone_wt = color_stone + moti;
    var stone_amt = color_stone_rate * stone_wt;
    //var stone_amt = color_stone_rate * color_stone;
    var checker_amt = parseFloat($(evt).closest('tr').find('.checker_amt').val()) || 0;
    var kundan_amt = parseFloat($(evt).closest('tr').find('.kundan_amt').val()) || 0;

    var other_wt = parseFloat($(evt).closest('tr').find('.other_wt').val()) || 0;
    var gr_wt_total =  ttl_wt.toFixed(2);
    $(evt).closest('tr').find('.ttl_gross_wt').val(gr_wt_total);
    $(evt).closest('tr').find('.stone_amt').val(stone_amt);
    $(evt).closest('tr').find('.stone_wt').val(stone_wt);
    $(evt).closest('tr').find('.ttl_wt').val(checker_amt + kundan_amt + stone_amt + other_wt);

}

function calculate_total_amt_stock(evt) {
    evt.value = evt.value.replace(/[^0-9.]/g, '');
    var checker_amt = parseFloat($(evt).closest('tr').find('.checker_amt').val()) || 0;
    var kundan_amt = parseFloat($(evt).closest('tr').find('.kundan_amt').val()) || 0;
    var stone_amt = parseFloat($(evt).closest('tr').find('.stone_amt').val()) || 0;
    var other_wt = parseFloat($(evt).closest('tr').find('.other_wt').val()) || 0;

    //var color_stone_rate= parseFloat($('#color_stone_rate_'+id).val()) || 0;

    var ttl_wt = checker_amt + kundan_amt + stone_amt + other_wt;
   
    $(evt).closest('tr').find('.ttl_wt').val(ttl_wt);

}
function calculate_kun_amt_stock(evt) {
    evt.value = evt.value.replace(/[^0-9.]/g, '');
    var checker_amt = parseFloat($(evt).closest('tr').find('.checker_amt').val()) || 0;
    var kundan_rate = parseFloat($(evt).closest('tr').find('.kundan_rate').val()) || 0;
    var kun_pc = parseFloat($(evt).closest('tr').find('.kundan_pc').val()) || 0;
    var kun_amt = kun_pc * kundan_rate;
    var stone_amt = parseFloat($(evt).closest('tr').find('.stone_amt').val()) || 0;
    $(evt).closest('tr').find('.kundan_amt').val(kun_amt);
    $(evt).closest('tr').find('.ttl_wt').val(kun_amt+checker_amt+stone_amt);

}

function calculate_checker_amt_stock(evt) {
    evt.value = evt.value.replace(/[^0-9.]/g, '');
    var checker = parseFloat($(evt).closest('tr').find('.checker_pcs').val()) || 0;
    var checker_pcs = parseFloat($(evt).closest('tr').find('.checker_rate').val()) || 0;
    var checker_amt = checker_pcs * checker;
    //alert(checker_amt);
    $(evt).closest('tr').find('.ttl_wt').val(checker_amt);
    $(evt).closest('tr').find('.checker_amt').val(checker_amt);
}




/*Add to stock functionality calculation end  &&  kundan karigar to ready product */
function receive_kundan_karigar_order(obj_btn,type) {
    if(type == 'print'){
    $('.print_kundan_order').prop('disabled', true);
            var type=$(".type").val('store');
    }
   

    var action_type=$("#action_type").val();
   
    if (action_type == 'stock' || action_type == 'edit' || action_type == 'repair_stock') {

        if (!$('#department_id').val()) {
            $('.print_kundan_order').prop('disabled', false);
            showMsgPopup("Error", "Please Select department", '', 'error')
            return false;
        }
        if (!$('#category_id').val()) {
            $('.print_kundan_order').prop('disabled', false);
            showMsgPopup("Error", "Please Select category", '', 'error')
            return false;
        }
        if (!$('#kariger_id').val()) {
            $('.print_kundan_order').prop('disabled', false);
            showMsgPopup("Error", "Please Select Karigar", '', 'error')
            return false;
        }
        var ctrl = 'ready_product';
        var action = 'karigar_receive_order/print_pdf';
      
    }else if(type == 'update'){
       var ctrl = 'karigar_receive_order/karigar_receipt';
       var action = 'karigar_receive_order/print_pdf';
       $('.print_kundan_order_up').prop('disabled', true);
          
    } else{

        var ctrl = 'karigar_receive_order';
        var action = 'karigar_receive_order/print_pdf';
        if (!$('#kariger_id').val()) {
            $('.print_kundan_order').prop('disabled', false);
            $('.print_kundan_order_up').prop('disabled', false);
            showMsgPopup("Error", "Please Select Karigar", '', 'error')
            return false;
        }      
    }
        $.ajax({
            url: url + action,
            type: "POST",
            dataType: "json",
            data: $('form#qc_check').serialize(),
            success: function (data, textStatus, jqXHR)
            {
                //console.log(data)
                if (data.status == 'failure')
                {
                    $('.print_kundan_order').prop('disabled', false);
                    $('.print_kundan_order_up').prop('disabled', false);
                    $('.div_error').html('');
                    var html_error = '';
                    $.each(data.error, function (key, val) {
                        html_error += '<p class="text-danger">' + val + '</p>';
                    })
                    $('.div_error').html(html_error);

                }else if (data.status == "success") {
                    $('.print_kundan_order').prop('disabled', true);
                    if(data.karigar_id ==''){
                        showMsgPopup("Success", "Successfully Updated", '', 'normal')
                        window.open(data.data.url, "_blank")
                        setTimeout(function () {
                            close_toast_popup(url + ctrl)

                        }, 500)
                    }else if(data.url_stock ==''){
                          showMsgPopup("Success", "Successfully Updated", '', 'normal')
                        setTimeout(function () {
                            close_toast_popup(url + 'ready_product' )

                        }, 500)
                    }else{
                        showMsgPopup("Success", "Successfully Updated", '', 'normal')
                        setTimeout(function () {
                            close_toast_popup(url + ctrl +'/'+data.karigar_id)

                        }, 500)
                    }
                } else {
                    $('.print_kundan_order').prop('disabled', false);
                    $('.print_kundan_order_up').prop('disabled', false);
                    showMsgPopup("Error", "Something Went Wrong Try Again", '', 'error')
                }
            },
            error: function (jqXHR, textStatus, errorThrown)
            {

            }
        });
}
var check_dbl_click = 0;
function send_to_salesstock(id) {
    if ($("input[name='drp_id[]']:checked").val() == undefined) {
        showMsgPopup("Error", "Select product to send to Stock", '', 'error');
        return false;
    } else if (check_dbl_click == 0) {
        check_dbl_click = 1;
        $.ajax({
            url: url + "sales_stock/store",
            type: "POST",
            dataType: "json",
            data: $('form#sales_product_form').serialize(),
            //data:{drp_id:id},
            success: function (data, textStatus, jqXHR)
            {
                ////console.log(data)
                if (data.status == 'failure')
                {
                    showMsgPopup("Error", data.error.quantity, '', 'error')

                } else if (data.status == "success") {
                    $('#btn_' + id).closest('tr').remove()
                    showMsgPopup("Success", "Successfully approved", '', 'normal')
                    setTimeout(function () {
                        $(this).closest('tr').remove();
                        close_toast_popup(url + 'Sales_recieved_product')

                    }, 500)
                } else {
                    showMsgPopup("Error", "Something Went Wrong Try Again", '', 'error')
                }
            },
            error: function (jqXHR, textStatus, errorThrown)
            {

            }
        });
    }

}

var check_dbl_click_all = 0;
function send_to_salesstock_all() {
    if ($("input[name='voucher_no[]']:checked").val() == undefined) {
        showMsgPopup("Error", "Select voucher to send to Stock", '', 'error');
        return false;
    } else if (check_dbl_click_all == 0) {
        check_dbl_click = 1;
        $.ajax({
            url: url + "sales_stock/store_all",
            type: "POST",
            dataType: "json",
            data: $('form#sales_product_form').serialize(),
            //data:{drp_id:id},
            success: function (data, textStatus, jqXHR)
            {
                ////console.log(data)
                if (data.status == 'failure')
                {
                    showMsgPopup("Error", data.error.quantity, '', 'error')

                } else if (data.status == "success") {
                    $('#btn_' + id).closest('tr').remove()
                    showMsgPopup("Success", "Successfully approved", '', 'normal')
                    setTimeout(function () {
                        $(this).closest('tr').remove();
                        close_toast_popup(url + 'Sales_recieved_product')

                    }, 500)
                } else {
                    showMsgPopup("Error", "Something Went Wrong Try Again", '', 'error')
                }
            },
            error: function (jqXHR, textStatus, errorThrown)
            {

            }
        });
    }

}



function send_to_manufacture(id) {
    if ($("input[name='drp_id[]']:checked").val() == undefined) {
        showMsgPopup("Error", "Please Select Vouchers", '', 'error');
        return false;
    } else if (check_dbl_click_all == 0) {
        check_dbl_click_all = 1;
        $.ajax({
            url: url + "sales_stock/sales_to_manufacture",
            type: "POST",
            dataType: "json",
            data: $('form#sales_product_form').serialize(),
            // data :{id:id},
            success: function (data, textStatus, jqXHR)
            {
                ////console.log(data)
                if (data.status == 'failure')
                {
                    showMsgPopup("Error", data.error.quantity, '', 'error')

                } else if (data.status == "success") {
                    $('#btn_' + id).closest('tr').remove()
                    showMsgPopup("Success", "Successfully Transfered To Manufacture Department", '', 'normal')
                    setTimeout(function () {
                        close_toast_popup(url + 'Sales_recieved_product')

                    }, 500)
                } else {
                    showMsgPopup("Error", "Something Went Wrong Try Again", '', 'error')
                }
            },
            error: function (jqXHR, textStatus, errorThrown)
            {

            }
        });
    }

}

function send_to_manufacture_all() {
    if ($("input[name='voucher_no[]']:checked").val() == undefined) {
        showMsgPopup("Error", "Please Select Products", '', 'error');
        return false;
    } else if (check_dbl_click == 0) {
        check_dbl_click = 1;
        $.ajax({
            url: url + "sales_stock/sales_to_manufacture_all",
            type: "POST",
            dataType: "json",
            data: $('form#sales_product_form').serialize(),
            // data :{id:id},
            success: function (data, textStatus, jqXHR)
            {
                ////console.log(data)
                if (data.status == 'failure')
                {
                    showMsgPopup("Error", data.error.quantity, '', 'error')

                } else if (data.status == "success") {
                    $('#btn_' + id).closest('tr').remove()
                    showMsgPopup("Success", "Successfully Transfered To Manufacture Department", '', 'normal')
                    setTimeout(function () {
                        close_toast_popup(url + data.redirect_url)

                    }, 500)
                } else {
                    showMsgPopup("Error", "Something Went Wrong Try Again", '', 'error')
                }
            },
            error: function (jqXHR, textStatus, errorThrown)
            {

            }
        });
    }

}
function stock_to_transfer_delhi() {
    if ($("input[name='product_code[]']:checked").val() == undefined) {
        showMsgPopup("Error", "Please Select Products", '', 'error');
        return false;
    } else if (check_dbl_click == 0) {
        check_dbl_click = 1;
        $.ajax({
            url: url + "sales_stock/stock_to_transfer_all",
            type: "POST",
            dataType: "json",
            data: $('form#sales_product_form').serialize(),
            // data :{id:id},
            success: function (data, textStatus, jqXHR)
            {
                ////console.log(data)
                if (data.status == 'failure')
                {
                    showMsgPopup("Error", data.error.location, '', 'error')

                } else if (data.status == "success") {
                    $('#btn_' + id).closest('tr').remove()
                    showMsgPopup("Success", "Successfully Transfered To Delhi Department", '', 'normal')
                    setTimeout(function () {
                        close_toast_popup(url + data.redirect_url)

                    }, 500)
                } else {
                    showMsgPopup("Error", "Something Went Wrong Try Again", '', 'error')
                }
            },
            error: function (jqXHR, textStatus, errorThrown)
            {

            }
        });
    }

}
function stock_to_manufacture_all() {
    if ($("input[name='product_code[]']:checked").val() == undefined) {
        showMsgPopup("Error", "Please Select Products", '', 'error');
        return false;
    } else if (check_dbl_click == 0) {
        check_dbl_click = 1;
        $.ajax({
            url: url + "sales_stock/stock_to_manufacture_all",
            type: "POST",
            dataType: "json",
            data: $('form#sales_product_form').serialize(),
            // data :{id:id},
            success: function (data, textStatus, jqXHR)
            {
                ////console.log(data)
                if (data.status == 'failure')
                {
                    showMsgPopup("Error", data.error.quantity, '', 'error')

                } else if (data.status == "success") {
                    $('#btn_' + id).closest('tr').remove()
                    showMsgPopup("Success", "Successfully Transfered To Manufacture Department", '', 'normal')
                    setTimeout(function () {
                        close_toast_popup(url + data.redirect_url)

                    }, 500)
                } else {
                    showMsgPopup("Error", "Something Went Wrong Try Again", '', 'error')
                }
            },
            error: function (jqXHR, textStatus, errorThrown)
            {

            }
        });
    }

}


function reload_page_hallmarking() {
    setTimeout(function () {
        if (page_reload == true) {
            location.reload();
        }

    }, 1000)
}

function save_accounting(type) {
    $.ajax({
        url: url + "accounting/issue/" + type,
        type: "POST",
        dataType: "json",
        data: $('form#accounting_form').serialize(),
        success: function (data, textStatus, jqXHR)
        {
            ////console.log(data)
            if (data.status == 'failure')
            {
                error_msg(data.error);

            } else if (data.status == "success") {
                showMsgPopup("Success", "Successfully Assigned to Karigar", '', 'normal')
                setTimeout(function () {
                    close_toast_popup(url + 'accounting/issue/show/' + data.voucher_no)

                }, 500)
            } else {
                showMsgPopup("Error", "Something Went Wrong Try Again", '', 'error')
            }
        },
        error: function (jqXHR, textStatus, errorThrown)
        {

        }
    });
}

function rec_gold(btnObj, type) {
    var type_name = $(btnObj).closest('tr').children('td:first-child').text();
    var karigar_name = $(btnObj).closest('tr').children('td:nth-child(2)').text();
    var metal_name = $(btnObj).closest('tr').children('td:nth-child(3)').text();
    var weight = $(btnObj).closest('tr').children('td:nth-child(6)').text();

    var karigar_id = $(btnObj).attr('karigar_id');
    var metal_id = $(btnObj).attr('metal_id');
    $('#k_id').val(karigar_id);
    $('#m_id').val(metal_id);
    $('#type_id').val(type);
    $('#max_limit_weight').val(weight);

    $('#type_name').text(type_name);
    $('#k_name').text(karigar_name);
    $('#m_name').text(metal_name);
    $('#rec_gold').modal('show');
}

function update_accounting() {
    var entered_weight = parseFloat($('#new_weight').val());
    var max_limit = parseFloat($('#max_limit_weight').val());
    var karigar_id = $('#k_id').val();
    var metal_id = $('#m_id').val();
    var type_id = $('#type_id').val();
    var purity = $('#new_purity').val();


    $('.text-danger').html('');
    if (!entered_weight) {
        $('#new_weight_error').html('Please Enter Metal Weight');
    } else if (entered_weight <= 0.00) {
        $('#new_weight_error').html('Please Enter weight greater Than 0.00');
    } else if (is_Nan(entered_weight)) {
        $('#new_weight_error').html('Please Enter valid number');
    } else {
        $('#rec_gold').modal('hide');

        $.ajax({
            url: url + "accounting/recieved_weight",
            type: "POST",
            dataType: "json",
            data: {recieved_weight: entered_weight, karigar_id: karigar_id, metal_id: metal_id, type: type_id, purity: purity},
            success: function (data, textStatus, jqXHR)
            {
                if (data.status == 'failure') {
                    error_msg(data.error);
                } else if (data.status == "success") {
                    showMsgPopup("Success", "Successfully Recieved", '', 'normal')
                    setTimeout(function () {
                        close_toast_popup(url + 'accounting')
                    }, 500)
                } else {
                    showMsgPopup("Error", "Something Went Wrong Try Again", '', 'error')
                }
            },
            error: function (jqXHR, textStatus, errorThrown) {

            }
        });
    }
}


function invoice_data_from_item(btnObj) {
    var entered_value = $(btnObj).val();
    $.ajax({
        url: url + "sales_stock/get_item_details",
        type: "POST",
        dataType: "json",
        data: {product_code: entered_value},
        success: function (data, textStatus, jqXHR)
        {
            var cs_wt_var = parseFloat($('#cs_cat_val option:selected').text()) / 100 || 0;
            var kun_wt_var = parseFloat($('#k_cat_val option:selected').text()) / 100 || 0;

            var kun_wt = kun_wt_var * data.wax_wt;
            var cs_wt = cs_wt_var * (parseFloat(data.moti) + parseFloat(data.color_stone));
            var net_wt = parseFloat(data.gr_wt) - (kun_wt + cs_wt);

            // console.log(data.color_stone);
            if (data.result == "success") {
                showMsgPopup("Success", "Successfully Recieved", '', 'normal')
                $(btnObj).closest('tr').children('td:nth-child(2)').find('input[type="hidden"]').val(data.stock_id);
                $(btnObj).closest('tr').children('td:nth-child(3)').find('label').text(data.gr_wt);
                $(btnObj).closest('tr').children('td:nth-child(3)').find('input[type="hidden"]').val(data.gr_wt);
                $(btnObj).closest('tr').children('td:nth-child(4)').find('input').val(cs_wt);
                $(btnObj).closest('tr').children('td:nth-child(5)').find('input').val(kun_wt);
                $(btnObj).closest('tr').children('td:nth-child(6)').find('label').text(net_wt);
                $(btnObj).closest('tr').children('td:nth-child(8)').find('label').text(00);
                $(btnObj).closest('tr').children('td:nth-child(9)').find('label').text(data.kundan_pcs);
                $(btnObj).closest('tr').children('td:nth-child(9)').find('input[type="hidden"]').val(data.kundan_pcs);
                $(btnObj).closest('tr').children('td:nth-child(11)').find('label').text(data.kundan);

                // var total= parseFloat(data.gr_wt)+parseFloat(data.net_wt)+parseFloat(data.net_wt)+parseFloat(data.kundan_pcs)+parseFloat(data.kundan);
                var total = 0.00;
                //4,5,7,10,12,13,14
                $.each([8, 11, 14], function (index, value) {
                    var cur_value = parseFloat($(btnObj).closest('tr').children('td:nth-child(' + value + ')').find('input').val());
                    if (!isNaN(cur_value)) {
                        total += cur_value;
                    }
                });

                $(btnObj).closest('tr').children('td:nth-child(15)').find('label').text(total);
                total_invoice_table();//last row total
            } else if (data.result == "error") {
                showMsgPopup("Error", "Something Went Wrong Try Again", '', 'error')
                $(btnObj).val('');
            }
        },
        error: function (jqXHR, textStatus, errorThrown) {

        }
    });
}

function autocomplete_product(btnObj) {
    $(btnObj).autocomplete({
        source: url + 'sales_stock/get_stock_product',
        minLength: 2,
        select: function (event, ui) {
            //invoice_data_from_item(btnObj);
        }
    });
}


$(".parent_category").change(function () {
    $("#make_set").submit();
});

function save_color_stone(type) {
    $.ajax({
        url: url + "color_stone/" + type,
        type: "POST",
        dataType: "json",
        data: $('form#color_stone_form').serialize(),
        success: function (data, textStatus, jqXHR)
        {
            ////console.log(data)
            if (data.status == 'failure')
            {
                error_msg(data.error);

            } else if (data.status == "success") {
                showMsgPopup("Success", "Color Stone Successfully Added", '', 'normal')
                setTimeout(function () {
                    close_toast_popup(url + 'color_stone')

                }, 500)
            } else {
                showMsgPopup("Error", "Something Went Wrong Try Again", '', 'error')
            }
        },
        error: function (jqXHR, textStatus, errorThrown)
        {

        }
    });
}

function save_kundan_category(type) {
    $.ajax({
        url: url + "kundan_category/" + type,
        type: "POST",
        dataType: "json",
        data: $('form#kundan_category_form').serialize(),
        success: function (data, textStatus, jqXHR)
        {
            ////console.log(data)
            if (data.status == 'failure')
            {
                error_msg(data.error);

            } else if (data.status == "success") {
                showMsgPopup("Success", "Kundan Category Successfully Added", '', 'normal')
                setTimeout(function () {
                    close_toast_popup(url + 'kundan_category')

                }, 500)
            } else {
                showMsgPopup("Error", "Something Went Wrong Try Again", '', 'error')
            }
        },
        error: function (jqXHR, textStatus, errorThrown)
        {

        }
    });
}

function save_type_master(type) {
    $.ajax({
        url: url + "type_master/" + type,
        type: "POST",
        dataType: "json",
        data: $('form#type_master_form').serialize(),
        success: function (data, textStatus, jqXHR)
        {
            ////console.log(data)
            if (data.status == 'failure')
            {
                error_msg(data.error);

            } else if (data.status == "success") {
                showMsgPopup("Success", "Type Successfully Added", '', 'normal')
                setTimeout(function () {
                    close_toast_popup(url + 'type_master')

                }, 500)
            } else {
                showMsgPopup("Error", "Something Went Wrong Try Again", '', 'error')
            }
        },
        error: function (jqXHR, textStatus, errorThrown)
        {

        }
    });
}

function save_customer(type) {
    $.ajax({
        url: url + "customer/" + type,
        type: "POST",
        dataType: "json",
        data: $('form#customer_form').serialize(),
        success: function (data, textStatus, jqXHR)
        {
            ////console.log(data)
            if (data.status == 'failure')
            {
                error_msg(data.error);

            } else if (data.status == "success") {
                showMsgPopup("Success", "Customer Successfully Added", '', 'normal')
                setTimeout(function () {
                    close_toast_popup(url + 'customer')

                }, 500)
            } else {
                showMsgPopup("Error", "Something Went Wrong Try Again", '', 'error')
            }
        },
        error: function (jqXHR, textStatus, errorThrown)
        {

        }
    });
}
function download_parent_category() {
    $.ajax({
        url: url + "parent_category/fetch_from_dropbox",
        type: "GET",
        dataType: "json",
        success: function (data, textStatus, jqXHR)
        {
            ////console.log(data)
            if (data.status == 'failure')
            {
                error_msg(data.error);

            } else if (data.status == "success") {
                showMsgPopup("Success", "successfully fetched from dropbox", '', 'normal')
            } else {
                showMsgPopup("Error", "Something Went Wrong Try Again", '', 'error')
            }
        },
        error: function (jqXHR, textStatus, errorThrown)
        {

        }
    });
}

function save_invoice(action) {
    $('text-danger').html('');
    $.ajax({
        url: url + "sales_invoice/" + action,
        type: "POST",
        dataType: "json",
        data: $('form#sales_invoice').serialize(),
        success: function (data, textStatus, jqXHR)
        {
            //console.log(data)
            if (data.status == 'failure')
            {
                error_msg(data.error);

            } else if (data.status == "success") {
                showMsgPopup("Success", "Invoice Successfully Added", '', 'normal')
                setTimeout(function () {
                    close_toast_popup(url + 'sales_invoice')

                }, 500)
            } else {
                showMsgPopup("Error", "Something Went Wrong Try Again", '', 'error')
            }
        },
        error: function (jqXHR, textStatus, errorThrown)
        {

        }
    });
}

function send_to_dispatch(btnObj, invoice_id) {
    $('text-danger').html('');
    $.ajax({
        url: url + "sales_invoice/send_to_dispatch",
        type: "POST",
        dataType: "json",
        data: {id: invoice_id},
        success: function (data, textStatus, jqXHR)
        {
            if (data.status == "success") {
                showMsgPopup("Success", "Successfully Transfered To Dispatch", '', 'normal')
                setTimeout(function () {
                    close_toast_popup(url + 'sales_invoice')

                }, 500)
            } else {
                showMsgPopup("Error", "Something Went Wrong Try Again", '', 'error')
            }
        },
        error: function (jqXHR, textStatus, errorThrown)
        {

        }
    });
}
function dispatch_accept_reject(btnObj, invoice_id, new_status) {
    $('text-danger').html('');
    $.ajax({
        url: url + "Dispatch_invoices/approve_reject",
        type: "POST",
        dataType: "json",
        data: {id: invoice_id, status: new_status},
        success: function (data, textStatus, jqXHR)
        {
            if (data.status == "success") {

                if (new_status == 2) {
                    showMsgPopup("Success", "Successfully Approved", '', 'normal')
                } else if (new_status == 3) {
                    showMsgPopup("Success", "Successfully Rejected", '', 'normal')
                }
                setTimeout(function () {
                    close_toast_popup(url + '/Dispatch_invoices')

                }, 500)
            } else {
                showMsgPopup("Error", "Something Went Wrong Try Again", '', 'error')
            }
        },
        error: function (jqXHR, textStatus, errorThrown)
        {

        }
    });
}
function kundan_qc_reject() {
    $.ajax({
        url: url + "kundan_QC/reject",
        type: "POST",
        dataType: "json",
        data: $('form#qc_check').serialize(),
        success: function (data, textStatus, jqXHR)
        {
            if (data.status == 'failure')
            {
                showMsgPopup("Error", data.error.quantity, '', 'error')

            } else if (data.status == "success") {
                showMsgPopup("Success", "Successfully Rejected", '', 'normal')
                setTimeout(function () {
                    close_toast_popup(url + 'kundan_QC?status=pending')

                }, 500)
            } else {
                showMsgPopup("Error", "Something Went Wrong Try Again", '', 'error')
            }
        },
        error: function (jqXHR, textStatus, errorThrown)
        {

        }
    });
}


// function sent_invoice_to_hallmarking()
// {
//     $(".text-danger").text("")
//     var error_flag = true;
//     /*$("#hallmarking_sent_frm input").each(function(){
//         if($(this).val() == "")
//             $(this).parent().find("span").text("");
//     })
//     $("#hallmarking_sent_frm select").each(function(){
//         if($(this).val() == ""){
//                $("#center_hallmarking_error").show();
//         }
//     });*/
//     //return false;
//     $.ajax({
//         url : url+"Dispatch_invoices/sent_to_hallamrking",
//         type: "POST",
//         dataType: "json",
//         data : $('form#hallmarking_sent_frm').serialize(),
//         success: function(data, textStatus, jqXHR)
//         {
//             if(data.status=='failure')
//             {
//                 if(typeof data.keys  != 'undefined'){
//                     for (var i = 0; i <data.keys.length; i++) {
//                         console.log(data.keys[i])
//                         $("#quntity_"+data.keys[i]).text("Quntity exceeded")
//                     };         
//                 }         
//                // showMsgPopup("Error",'error');

//             }else if(data.status == "success"){
//                 showMsgPopup("Success","Successfully Updated",'','normal')
//                 setTimeout(function(){
//                         close_toast_popup(url+'Dispatch_dashboard')

//                     },500)
//             }else{
//                  showMsgPopup("Error","Something Went Wrong Try Again",'','error')
//             }
//         },
//         error: function (jqXHR, textStatus, errorThrown)
//         {

//         }
//     });
// }


function send_to_hallmarking_dispatch(sid_id) {
    $('#sid_id').val(sid_id);
    $('#hallmarking_dispatch_model').modal('show');
}

function dispatch_hallmarking_store() {
    $(".text-danger").text("")
    var error_flag = true;
    $.ajax({
        url: url + "Dispatch/hallmarking/store",
        type: "POST",
        dataType: "json",
        data: $('form#dispatch_hallmarking').serialize(),
        success: function (data, textStatus, jqXHR)
        {
            if (data.status == 'failure') {
                error_msg(data.error);

            } else if (data.status == "success") {
                showMsgPopup("Success", "Successfully Updated", '', 'normal')
                setTimeout(function () {
                    close_toast_popup(url + 'Dispatch_dashboard')

                }, 500)
            } else {
                showMsgPopup("Error", "Something Went Wrong Try Again", '', 'error')
            }
        },
        error: function (jqXHR, textStatus, errorThrown)
        {

        }
    });
}

function recieve_weight_dispatch(qch_id, sid_id) {
    $('#sid_id').val(sid_id);
    $('#qch_id').val(qch_id);
    $('#hallmarking_dispatch_recieved_model').modal('show');
}

function dispatch_hallmarking_recieve() {
    $(".text-danger").text("")
    var error_flag = true;
    $.ajax({
        url: url + "Dispatch/hallmarking/recieve",
        type: "POST",
        dataType: "json",
        data: $('form#dispatch_hallmarking_recieve').serialize(),
        success: function (data, textStatus, jqXHR)
        {
            if (data.status == 'failure') {
                error_msg(data.error);

            } else if (data.status == "success") {
                showMsgPopup("Success", "Successfully Updated", '', 'normal')
                setTimeout(function () {
                    close_toast_popup(url + 'Dispatch/hallmarking')

                }, 500)
            } else {
                showMsgPopup("Error", "Something Went Wrong Try Again", '', 'error')
            }
        },
        error: function (jqXHR, textStatus, errorThrown)
        {

        }
    });
}

function approved_to_dispatch(sid_id, quantity) {
    $('#sid_id_dispatch').val(sid_id);
    $('#quantity_dispatch').val(quantity);

    $('#dispatch_transfered').modal('show');
}

function hallmarking_rec_to_dispatch(sid_id, hr_id, quantity) {
    $('#sid_id_dispatch').val(sid_id);
    $('#hr_id_dispatch').val(hr_id);
    $('#quantity_dispatch').val(quantity);

    $('#dispatch_transfered').modal('show');
}

function dispatch_transfer() {
    $(".text-danger").text("")
    var error_flag = true;
    $.ajax({
        url: url + "Dispatch/dispatch_products/store",
        type: "POST",
        dataType: "json",
        data: $('form#dispatch_recieve').serialize(),
        success: function (data, textStatus, jqXHR)
        {
            if (data.status == 'failure') {
                error_msg(data.error);

            } else if (data.status == "success") {

                showMsgPopup("Success", "Successfully Updated", '', 'normal')
                setTimeout(function () {
                    if ($('#chitti_no').val()) {
                        close_toast_popup(url + 'Dispatch_invoices/show/' + $('#chitti_no').val())
                    } else {
                        close_toast_popup(url + 'Dispatch/hallmarking_recieved')
                    }


                }, 500)
            } else {
                showMsgPopup("Error", "Something Went Wrong Try Again", '', 'error')
            }
        },
        error: function (jqXHR, textStatus, errorThrown)
        {

        }
    });
}

function save_dispatch_mode(action) {
    $(".text-danger").text("")
    var error_flag = true;
    $.ajax({
        url: url + "dispatch_mode/" + action,
        type: "POST",
        dataType: "json",
        data: $('form#dispatch_mode_form').serialize(),
        success: function (data, textStatus, jqXHR)
        {
            if (data.status == 'failure') {
                error_msg(data.error);

            } else if (data.status == "success") {
                showMsgPopup("Success", "Successfully Added", '', 'normal')
                setTimeout(function () {
                    close_toast_popup(url + 'dispatch_mode')

                }, 500)
            } else {
                showMsgPopup("Error", "Something Went Wrong Try Again", '', 'error')
            }
        },
        error: function (jqXHR, textStatus, errorThrown)
        {

        }
    });

}

function save_dispatch_mode_data(action) {
    $(".text-danger").text("")
    var error_flag = true;
    $.ajax({
        url: url + "dispatch_mode/dispatch_mode_data_" + action,
        type: "POST",
        dataType: "json",
        data: $('form#dispatch_mode_view').serialize(),
        success: function (data, textStatus, jqXHR)
        {
            if (data.status == 'failure') {
                error_msg(data.error);

            } else if (data.status == "success") {
                showMsgPopup("Success", "Successfully Added", '', 'normal')
                setTimeout(function () {
                    close_toast_popup(url + 'dispatch_mode/view/' + $('#enc_id').val())

                }, 500)
            } else {
                showMsgPopup("Error", "Something Went Wrong Try Again", '', 'error')
            }
        },
        error: function (jqXHR, textStatus, errorThrown)
        {

        }
    });
}
function assign_to_karigar_manuf() {
    if (!$("input[type='checkbox']").is(":checked")) {
        showMsgPopup("Error", "Please Select Product", '', 'error')
        return false;
    } else {
        $('#manuf_karigar_form').submit();
    }
}

$('#check_all').click(function () {
    if ($(this).prop('checked') == true) {
        $("input[type='checkbox']").prop('checked', true);
    } else {
        $("input[type='checkbox']").prop('checked', false);
    }

})
var add_more_cnt = 0;
var add_variant_cnt = 0;

function add_more_code() {
    add_more_cnt++;
    var html = '<div class="form-group"><label class="col-sm-4 control-label" for="inputEmail3"></label><div class="col-sm-4"><input type="text" placeholder="Enter Code" class="form-control" name="parent_category[code][]"> <span class="text-danger" id="product_code_' + add_more_cnt + '_error"> </span> </div><div class="col-sm-2"><a  class="btn btn-danger small loader-hide  btn-sm" href="javascript:void(0)" onclick="remove_code(this)" title="Add More">Remove</a></div></div>';
    $('.add_more').append(html);
}

function add_more_variant() {
    add_variant_cnt++;
    var html = '<div class="form-group"><label class="col-sm-5 control-label" for="inputEmail3"></label><div class="col-sm-4"><input type="text" placeholder="Enter Variant" class="form-control" name="parent_category[variant][]"> <span class="text-danger" id="product_code_' + add_more_cnt + '_error"> </span> </div><div class="col-sm-2"><a  class="btn btn-danger small loader-hide  btn-sm" href="javascript:void(0)" onclick="remove_code(this)" title="Add More">Remove</a></div></div>';
    $('.add_more_variant').append(html);
}


function remove_code(obj) {
    $(obj).parents('.form-group').remove();
}
function remove_tr(obj) {
    $(obj).parents('tr').remove();
}
/*$("input.tr_clone_add").on('click', function() {
 var $tr    = $(this).closest('.tr_clone');
 var sel_item_code = $(this).closest('tr').find('.item_cat :selected').attr('data-code');
 var $clone = $tr.clone();
 $clone.find(':text').val('');
 $tr.after($clone);
 });*/
// function create_product_code(code,obj){
//     var sub_code = $('.code_'+code).val();
//     $('#code_'+code).val(sub_code+''+$(obj).val());
// }
var dict = {};
var dict_old = {};
function create_product_code(id) {
    var product_code = $('#product_code_val_' + id).val();
    var code = product_code;
    var res = product_code.split("-");

    dict[product_code] = res[1];
    dict_old[product_code] = res[1];

    $(".abc").each(function (key, value) {
        var variable = $('#product_code_val_' + key).val();
        if (typeof (dict[variable]) != "undefined" && dict[variable] !== null) {
            var v_res = variable.split("-");
            $('#code_' + key).val(v_res[0] + '-' + parseInt(dict[variable]));
            dict[variable] = parseInt(dict[variable]) + 1;
        }
    });
    dict = {};
    $.each(dict_old, function (k, v) {
        dict[k] = v;

    })
}
function save_customer_type(type) {
    $.ajax({
        url: url + "customer_type/" + type,
        type: "POST",
        dataType: "json",
        data: $('form#customer_type_form').serialize(),
        success: function (data, textStatus, jqXHR)
        {
            //console.log(data)
            if (data.status == 'failure')
            {
                error_msg(data.error);

            } else if (data.status == "success") {
                showMsgPopup("Success", "Customer Successfully Added", '', 'normal')
                setTimeout(function () {
                    close_toast_popup(url + 'customer_type')

                }, 500)
            } else {
                showMsgPopup("Error", "Something Went Wrong Try Again", '', 'error')
            }
        },
        error: function (jqXHR, textStatus, errorThrown)
        {

        }
    });
}

function save_salesman_master(type) {
    $.ajax({
        url: url + "salesman_master/" + type,
        type: "POST",
        dataType: "json",
        data: $('form#salesman_master_form').serialize(),
        success: function (data, textStatus, jqXHR)
        {
            //console.log(data)
            if (data.status == 'failure')
            {
                error_msg(data.error);

            } else if (data.status == "success") {
                showMsgPopup("Success", "Salesman Successfully Added", '', 'normal')
                setTimeout(function () {
                    close_toast_popup(url + 'salesman_master')
                }, 500)
            } else {
                showMsgPopup("Error", "Something Went Wrong Try Again", '', 'error')
            }
        },
        error: function (jqXHR, textStatus, errorThrown)
        {

        }
    });
}

function resend_to_karigar(id) {
    var conf = confirm("Do you need to order same product?");
    if (conf)
    { //window.location=url+'Manufacturing_quality_control/resend_to_karigar/'+id;
        var weight = $('#weight_' + id).val();
        var quantity = $('#quantity_' + id).val();
        var original_qty = $('#original_qty_' + id).val();
        $.ajax({
            url: url + "Manufacturing_quality_control/resend_to_karigar/" + id,
            type: "POST",
            dataType: "json",
            data: {weight: weight, quantity: quantity, original_qty: original_qty},
            success: function (data, textStatus, jqXHR)
            {
                if (data.status == 'failure') {
                    error_msg(data.error);

                } else if (data.status == "success") {
                    //showMsgPopup("Success","Successfully Rejected",'','normal')
                    setTimeout(function () {
                        close_toast_popup(data.url)
                    }, 500)
                } else {
                    showMsgPopup("Error", "Something Went Wrong Try Again", '', 'error')
                }
            },
            error: function (jqXHR, textStatus, errorThrown)
            {

            }
        });
    }
}
function ammend_mnfc_qc_products(id) {
    var conf = confirm("Are you sure you want to resend this product to karigar?");
    if (conf)
    { //window.location=url+'Manufacturing_quality_control/resend_to_karigar/'+id;
        var weight = $('#weight_' + id).val();
        var quantity = $('#quantity_' + id).val();
        var original_qty = $('#original_qty_' + id).val();
        $.ajax({
            url: url + "Manufacturing_quality_control/qc_ammend/" + id,
            type: "POST",
            dataType: "json",
            data: {weight: weight, quantity: quantity, original_qty: original_qty},
            success: function (data, textStatus, jqXHR)
            {
                if (data.status == 'failure') {
                    error_msg(data.error);

                } else if (data.status == "success") {
                    //showMsgPopup("Success","Successfully Rejected",'','normal')
                    setTimeout(function () {
                        close_toast_popup(data.url)
                    }, 500)
                } else {
                    showMsgPopup("Error", "Something Went Wrong Try Again", '', 'error')
                }
            },
            error: function (jqXHR, textStatus, errorThrown)
            {

            }
        });
    }
}
function remove_tag_code(obj) {
    $(obj).parents('tr').remove();
    var qnt = $('#quantity').val();
    $('#quantity').val(qnt - 1);
    var rowCount = $('#product_details tr').length;
    $("#totalRecodcnt").text(' Showing  of entries  ' + rowCount);
}

function save_tag_image(tag_id) {
    var formData = new FormData(document.querySelector("form"));
    formData.append('tag_id', tag_id);
    $.ajax({
        url: url + "Make_set/store_image",
        type: "POST",
        dataType: "json",
        data: formData,
        processData: false,
        contentType: false,
        success: function (data, textStatus, jqXHR)
        {
            if (data.status == "success") {
                $('#img_' + tag_id).attr("src", data.image);
                showMsgPopup("Success", "Image Successfully Added", '', 'normal')
            } else {
                showMsgPopup("Error", "Something Went Wrong Try Again", '', 'error')
            }
        },
        error: function (jqXHR, textStatus, errorThrown)
        {

        }
    });
}


/*function Transfer_to_sales() {
    var checkedValues = $('input:checkbox:checked').map(function () {
        return this.value;
    }).get();

    var error = 0;
    if (checkedValues.length == 0) {
        error = 1;
        showMsgPopup("Error", "Please select product for Transfer to sales", '', 'error');
    }

    if (error == 0) {
        $('#send_to_transfer_stock').modal('show');
    }

}

function transfer_to_sales_location() {
        $('.send_to_stock_location').prop('disabled', true);
    var location_id=$("#location_id").val();
    //alert(location_id);
    var formData = new FormData(document.querySelector("form#transfer_to_sales"));
    formData.append('location_id', location_id);
    //console.log(formData);
    $.ajax({
        url: url + "Ready_product/transfer_to_sales",
        type: "POST",
        dataType: "json",
        data: formData,
        processData: false,
        contentType: false,
        success: function (data, textStatus, jqXHR)
        {
            if (data.status == "success") {
                    $('.send_to_stock_location').prop('disabled', true);
                showMsgPopup("Success", "Successfully Transfered To Sales", '', 'normal')
                window.open(url + 'Ready_product/generate_voucher', '_blank');
                setTimeout(function () {
                    close_toast_popup(url + 'ready_product')

                }, 250)
            } else {
                    $('.send_to_stock_location').prop('disabled', false);
                showMsgPopup("Error", "Something Went Wrong Try Again", '', 'error')
            }
        },
        error: function (jqXHR, textStatus, errorThrown)
        {

        }
    });
}*/
function Transfer_to_sales() {
    if ($("input[name='rp[]']:checked").val() == undefined) {
        showMsgPopup("Error", "Please select products", '', 'error');
        return false;
    }
    $.ajax({
        url: url + "Ready_product/transfer_to_sales",
        type: "POST",
        dataType: "json",
        data: $('form#transfer_to_sales').serialize(),
        success: function (data, textStatus, jqXHR)
        {
            if (data.status == "success") {
                showMsgPopup("Success", "Successfully Transfered To Sales", '', 'normal')
                window.open(url + 'Ready_product/generate_voucher', '_blank');
                setTimeout(function () {
                    close_toast_popup(url + 'ready_product')

                }, 250)
            } else {
                showMsgPopup("Error", "Something Went Wrong Try Again", '', 'error')
            }
        },
        error: function (jqXHR, textStatus, errorThrown)
        {

        }
    });
}
function Transfer_to_sales_return_voucher() {
    if ($("input[name='voucher_no[]']:checked").val() == undefined) {
        showMsgPopup("Error", "Please select products", '', 'error');
        return false;
    }
    $.ajax({
        url: url + "sales_return_product/transfer_to_sales_return_voucher",
        type: "POST",
        dataType: "json",
        data: $('form#sales_product_form').serialize(),
        success: function (data, textStatus, jqXHR)
        {
            if (data.status == "success") {
                showMsgPopup("Success", "Successfully Transfered To Sales", '', 'normal')
                window.open(url + 'Ready_product/generate_voucher', '_blank');
                setTimeout(function () {
                    close_toast_popup(url + 'sales_return_product')

                }, 250)
            } else {
                showMsgPopup("Error", "Something Went Wrong Try Again", '', 'error')
            }
        },
        error: function (jqXHR, textStatus, errorThrown)
        {

        }
    });
}
function Repair_products() {
    if ($("input[name='rp[]']:checked").val() == undefined) {
        showMsgPopup("Error", "Please select products", '', 'error');
        return false;
    }
    $('.repair_products_btn').prop('disabled', true);
    $.ajax({
        url: url + "Ready_product/repair_products",
        type: "POST",
        dataType: "json",
        data: $('form#transfer_to_sales').serialize(),
        success: function (data, textStatus, jqXHR)
        {
            if (data.status == "success") {
                $('.repair_products_btn').prop('disabled', true);
                window.open(url + 'Ready_product/generate_issue_receipt_repair', '_blank');
                showMsgPopup("Success", "Successfully Repair Product", '', 'normal')
                setTimeout(function () {
                    close_toast_popup(url + 'ready_product')

                }, 250)
            } else {
                $('.repair_products_btn').prop('disabled', false);
                showMsgPopup("Error", "Something Went Wrong Try Again", '', 'error')
            }
        },
        error: function (jqXHR, textStatus, errorThrown)
        {

        }
    });
}
function Repair_products_reject_mfg() {
    if ($("input[name='rp[]']:checked").val() == undefined) {
        showMsgPopup("Error", "Please select products", '', 'error');
        return false;
    }
    $('.repair_products_btn').prop('disabled', true);
    $.ajax({
        url: url + "Ready_product/repair_products",
        type: "POST",
        dataType: "json",
        data: $('form#sales_product_form').serialize(),
        success: function (data, textStatus, jqXHR)
        {
            if (data.status == "success") {
                $('.repair_products_btn').prop('disabled', true);
                window.open(url + 'Ready_product/generate_issue_receipt_repair', '_blank');
                showMsgPopup("Success", "Successfully Repair Product", '', 'normal')
                setTimeout(function () {
                    close_toast_popup(url + 'ready_product/rejected_by_sale')

                }, 250)
            } else {
                $('.repair_products_btn').prop('disabled', false);
                showMsgPopup("Error", "Something Went Wrong Try Again", '', 'error');
                       setTimeout(function () {
                    close_toast_popup(url + 'ready_product/rejected_by_sale')

                }, 250);

            }
        },
        error: function (jqXHR, textStatus, errorThrown)
        {

        }
    });
}

function chitti_accept_reject(id, status) {

    if (!id) {
        showMsgPopup("Error", "Something Went Wrong Try Again", '', 'error')
        return false;
    }
    $.ajax({
        url: url + "generate_chitti/accept_reject/1",
        type: "POST",
        dataType: "json",
        data: {id: id, status: status},
        success: function (data, textStatus, jqXHR)
        {
            if (data.status == "success") {
                if (status == 2) {
                    showMsgPopup("Success", "Successfully Approved", '', 'normal')
                } else {
                    showMsgPopup("Success", "Successfully Rejected", '', 'normal')
                }

                setTimeout(function () {
                    close_toast_popup(url + 'generate_chitti/show/' + $('#challan_no').val())
                }, 500)
            } else {
                showMsgPopup("Error", "Something Went Wrong Try Again", '', 'error')
            }
        },
        error: function (jqXHR, textStatus, errorThrown)
        {

        }
    });
}

function stock_rejected_transfer(type, id) {
    if (!id) {
        showMsgPopup("Error", "Something Went Wrong Try Again", '', 'error')
        return false;
    }
    if (type == 1) {/*send to karigar*/
        var link = url + "stock/rejected/send_to_karigar";
        var msg = "Successfully Sent To Karigar";
    } else {
        var link = url + "stock/rejected/send_to_stock";
        var msg = "Successfully Sent To Stock";
    }
    $.ajax({
        url: link,
        type: "POST",
        dataType: "json",
        data: {id: id},
        success: function (data, textStatus, jqXHR)
        {//console.log(data);
            if (data.status == "success") {
                showMsgPopup("Success", msg, '', 'normal')
                $('#btnstock_' + id).closest("tr").remove();
                setTimeout(function () {
                    //close_toast_popup('')
                    if (type == 1) {
                        close_toast_popup(url + 'Print_Order/index/' + data.karigar_id)
                    } else {
                        close_toast_popup('')
                    }
                }, 500)
            } else {
                showMsgPopup("Error", "Something Went Wrong Try Again", '', 'error')
            }
        },
        error: function (jqXHR, textStatus, errorThrown)
        {

        }
    });
}
$('#engage_karigar').click(function () {
    var status = true;
    $('#engage_karigar').attr("disabled", true);
    $.ajax({
        url: url + "Karigar_order_list/validate_postdata",
        type: "POST",
        dataType: "json",
        data: $('form#assign_kamgar_order').serialize(),
        success: function (data, textStatus, jqXHR)
        {
            if (data.status == "failure") {
                $('#engage_karigar').attr("disabled", false);
                error_msg(data.error);
                return false;
            } else if (data.status == "success") {
                $('#engage_karigar').attr("disabled", true);
                $('#assign_kamgar_order').submit();
                setTimeout(function () {
                    close_toast_popup(url + 'Manufacturing_department_order')
                }, 500)
            } else {
                $('#engage_karigar').attr("disabled", false);
                showMsgPopup("Error", "Something Went Wrong Try Again", '', 'error')
            }
        },
        error: function (jqXHR, textStatus, errorThrown)
        {

        }
    });
    return false;

})


function save_issue_voucher(action) {
    var error_flag = true;
    $.ajax({
        url: url + "Manufacture_accounting/Issue_voucher/" + action,
        type: "POST",
        dataType: "json",
        data: $('form#issue_voucher_form').serialize(),
        success: function (data, textStatus, jqXHR)
        {
            if (data.status == 'failure') {
                error_msg(data.error);

            } else if (data.status == "success") {
                showMsgPopup("Success", "Successfully Added", '', 'normal')
                setTimeout(function () {
                    close_toast_popup(url + 'Manufacture_accounting/Issue_voucher')

                }, 500)
            } else {
                showMsgPopup("Error", "Something Went Wrong Try Again", '', 'error')
            }
        },
        error: function (jqXHR, textStatus, errorThrown)
        {

        }
    });
}

function save_cash_bank_voucher(action) {
    var error_flag = true;
    $.ajax({
        url: url + "Manufacture_accounting/Cash_bank_issue/" + action,
        type: "POST",
        dataType: "json",
        data: $('form#issue_voucher_form').serialize(),
        success: function (data, textStatus, jqXHR)
        {
            if (data.status == 'failure') {
                error_msg(data.error);

            } else if (data.status == "success") {
                showMsgPopup("Success", "Successfully Added", '', 'normal')
                setTimeout(function () {
                    close_toast_popup(url + 'Manufacture_accounting/Cash_bank_issue')

                }, 500)
            } else {
                showMsgPopup("Error", "Something Went Wrong Try Again", '', 'error')
            }
        },
        error: function (jqXHR, textStatus, errorThrown)
        {

        }
    });
}

function save_metal_issue_voucher(action) {
    var error_flag = true;
    $.ajax({
        url: url + "Manufacture_accounting/Metal_issue/" + action,
        type: "POST",
        dataType: "json",
        data: $('form#metal_issue_voucher_form').serialize(),
        success: function (data, textStatus, jqXHR)
        {
            if (data.status == 'failure') {
                error_msg(data.error);

            } else if (data.status == "success") {
                showMsgPopup("Success", "Successfully Added", '', 'normal')
                setTimeout(function () {
                    close_toast_popup(url + 'Manufacture_accounting/Metal_issue')

                }, 500)
            } else {
                showMsgPopup("Error", "Something Went Wrong Try Again", '', 'error')
            }
        },
        error: function (jqXHR, textStatus, errorThrown)
        {

        }
    });
}


function save_material_master(action) {
    var error_flag = true;
    $.ajax({
        url: url + "Material/" + action,
        type: "POST",
        dataType: "json",
        data: $('form#material_master_form').serialize(),
        success: function (data, textStatus, jqXHR)
        {
            if (data.status == 'failure') {
                error_msg(data.error);

            } else if (data.status == "success") {
                showMsgPopup("Success", "Successfully Added", '', 'normal')
                setTimeout(function () {
                    close_toast_popup(url + 'Material')

                }, 500)
            } else {
                showMsgPopup("Error", "Something Went Wrong Try Again", '', 'error')
            }
        },
        error: function (jqXHR, textStatus, errorThrown)
        {

        }
    });
}

function save_Variations_category(action) {
    var error_flag = true;
    $.ajax({
        url: url + "Variations_category/" + action,
        type: "POST",
        dataType: "json",
        data: $('form#Variations_category_form').serialize(),
        success: function (data, textStatus, jqXHR)
        {
            if (action == 'update') {
                $msg = "Updated";
            } else {
                $msg = "Added";
            }
            if (data.status == 'failure') {
                error_msg(data.error);

            } else if (data.status == "success") {
                showMsgPopup("Success", "Successfully " + $msg, '', 'normal')
                setTimeout(function () {
                    close_toast_popup(url + 'Variations_category')

                }, 500)
            } else {
                showMsgPopup("Error", "Something Went Wrong Try Again", '', 'error')
            }
        },
        error: function (jqXHR, textStatus, errorThrown)
        {

        }
    });
}

function save_parent_minimum_stock() {
    var error_flag = true;
    $.ajax({
        url: url + "parent_category/store_minimum_stock",
        type: "POST",
        dataType: "json",
        data: $('form#minimum_stock').serialize(),
        success: function (data, textStatus, jqXHR)
        {
            if (data.status == 'failure') {
                error_msg(data.error);

            } else if (data.status == "success") {
                showMsgPopup("Success", "Successfully Added", '', 'normal')
                setTimeout(function () {
                    close_toast_popup(url + 'parent_category')

                }, 500)
            } else {
                showMsgPopup("Error", "Something Went Wrong Try Again", '', 'error')
            }
        },
        error: function (jqXHR, textStatus, errorThrown)
        {

        }
    });
}

function save_accounting_recieced(type) {
    $.ajax({
        url: url + "accounting/receipt/" + type,
        type: "POST",
        dataType: "json",
        data: $('form#accounting_form').serialize(),
        success: function (data, textStatus, jqXHR)
        {
            //console.log(data)
            if (data.status == 'failure')
            {
                error_msg(data.error);

            } else if (data.status == "success") {
                showMsgPopup("Success", "Successfully Recieved", '', 'normal')
                setTimeout(function () {
                    close_toast_popup(url + 'accounting/receipt/show/' + data.voucher_no)

                }, 500)
            } else {
                showMsgPopup("Error", "Something Went Wrong Try Again", '', 'error')
            }
        },
        error: function (jqXHR, textStatus, errorThrown)
        {

        }
    });
}

function save_party(action) {
    $.ajax({
        url: url + "Party_master/" + action,
        type: "POST",
        dataType: "json",
        data: $('form#party_master_form').serialize(),
        success: function (data, textStatus, jqXHR)
        {
            if (data.status == 'failure')
            {
                error_msg(data.error);

            } else if (data.status == "success") {
                showMsgPopup("Success", "Successfully Added", '', 'normal')
                setTimeout(function () {
                    close_toast_popup(url + 'Party_master')

                }, 2000)

            } else {

                showMsgPopup("Error", "Something Went Wrong Try Again", '', 'error')
                setTimeout(function () {
                    // close_toast_popup(url+'party')

                }, 2000)
            }
        },
        error: function (jqXHR, textStatus, errorThrown)
        {

        }
    });
}

function save_app_user(action) {
    $("#User_app_button").prop( "disabled", true );
    $.ajax({
        url: url + "User_app/" + action,
        type: "POST",
        dataType: "json",
        data: $('form#party_master_form').serialize(),
        success: function (data, textStatus, jqXHR)
        {    $("#User_app_button").prop( "disabled", false );
            if (data.status == 'failure')
            {
                error_msg(data.error);

            } else if (data.status == "success") {
                showMsgPopup("Success", "Successfully Added", '', 'normal')
                setTimeout(function () {
                    close_toast_popup(url + 'User_app')

                }, 2000)

            } else {

                showMsgPopup("Error", "Something Went Wrong Try Again", '', 'error')
                setTimeout(function () {
                    // close_toast_popup(url+'party')

                }, 2000)
            }
        },
        error: function (jqXHR, textStatus, errorThrown)
        {

        }
    });
}

function save_terms(action) {
    $.ajax({
        url: url + "Term/" + action,
        type: "POST",
        dataType: "json",
        data: $('form#term_master_form').serialize(),
        success: function (data, textStatus, jqXHR)
        {
            if (data.status == 'failure')
            {
                error_msg(data.error);
                //console.log(data);

            } else if (data.status == "success") {
                showMsgPopup("Success", "Successfully Added", '', 'normal')
                setTimeout(function () {
                    close_toast_popup(url + 'Term')

                }, 2000)

            } else {

                showMsgPopup("Error", "Something Went Wrong Try Again", '', 'error')
                setTimeout(function () {
                    // close_toast_popup(url+'party')

                }, 2000)
            }
        },
        error: function (jqXHR, textStatus, errorThrown)
        {

        }
    });
}
function receive_kun_prod() {
    $.ajax({
        url: url + "kundan_karigar/send_to_receive_all_products",
        type: "POST",
        dataType: "json",
        data: $('form#receive_kun_prod').serialize(),
        success: function (data, textStatus, jqXHR)
        {
          

            if (data.status == 'failure')
            {
                error_msg(data.error);

            } else if (data.status == 'failure1')
            {
                showMsgPopup("Error", data.error, '', 'error')


            } else if (data.status == "success") {
                showMsgPopup("Success", "Successfully Received", '', 'normal')
                setTimeout(function () {
                    location.reload();

                }, 500)
            } else {
                showMsgPopup("Error", "Something Went Wrong Try Again", '', 'error')
            }
        },
        error: function (jqXHR, textStatus, errorThrown)
        {

        }
    });
}

function gpl_caratlane(btnobj, id, status) {
    $.ajax({
        url: url + "Generate_packing_list/change_status",
        type: "POST",
        dataType: "json",
        data: {id: id, status: status},
        success: function (data, textStatus, jqXHR)
        {
            if (data.status == "success") {
                var txt = "";
                if (status == 1) {
                    txt = "Approved ";
                } else {
                    txt = "Rejected ";
                }
                $(btnobj).closest('td').html(txt);
                showMsgPopup("Success", "Successfully " + txt, '', 'normal')
                setTimeout(function () {
                    //close_toast_popup(url+'Generate_packing_list')
                }, 2000)

            } else {
                showMsgPopup("Error", "Something Went Wrong Try Again", '', 'error')
                setTimeout(function () {
                    // close_toast_popup(url+'Generate_packing_list')
                }, 2000)
            }
        },
        error: function (jqXHR, textStatus, errorThrown)
        {

        }
    });
}

function save_customer_order(action) {
    $(".ajaxLoader").fadeIn();
    var formData = new FormData(document.querySelector("form#customer_order_form"));
    $.ajax({
        url: url + "Customer_order/" + action,
        type: "POST",
        dataType: "json",
        data: formData,
        processData: false,
        contentType: false,
        success: function (data, textStatus, jqXHR)
        {
             if (data.status == 'failure1') {
              $(".ajaxLoader").fadeOut();
               showMsgPopup("Error", "Quantity not greater than 1", '', 'error')
                //console.log(data);

            }else if (data.status == 'failure') {
              $(".ajaxLoader").fadeOut();
                error_msg(data.error);
                //console.log(data);

            } else if (data.status == "success") {
                if (data.order_status == 1) {
                    window.open(url + 'Customer_order/print_assign_order/' + data.order_id, '_blank');
                    if (action == 'store') {
                        msg = "Successfully Saved";
                    } else {
                        msg = "Successfully update";
                    }

                }
                var redirect_url = url + 'Customer_order';
                if (get == 'not_send') {
                    redirect_url = url + "Customer_order/?status=not_send";
                }
                showMsgPopup("Success", msg, '', 'normal')
                setTimeout(function () {
                    close_toast_popup(history.go(-1))
                }, 2000)

            }else if(data.status == "modified"){
                    var redirect_url = url + 'Customer_order?status=pending';
                     showMsgPopup("Success", data.msg, '', 'normal')
                setTimeout(function () {
                    close_toast_popup(redirect_url)
                }, 2000)       
            }else if(data.status == "error" ){
             $(".ajaxLoader").fadeOut();
                showMsgPopup("Error", data.msg, '', 'error')
                setTimeout(function () {
                    // close_toast_popup(url+'party')
                }, 2000)
            }else {
              $(".ajaxLoader").fadeOut();
                showMsgPopup("Error", "Something Went Wrong Try Again", '', 'error')
                setTimeout(function () {
                    // close_toast_popup(url+'party')
                }, 2000)
            }
        },
        error: function (jqXHR, textStatus, errorThrown)
        {

        }
    });
}
function assign_customer_order() {
  $(".ajaxLoader").fadeIn();
    $.ajax({
        url: url + "Customer_order/assign_karigar",
        type: "POST",
        dataType: "json",
        data: $('form#customer_assign_form').serialize(),
        success: function (data, textStatus, jqXHR)
        {
            if (data.status == 'failure') {
              $(".ajaxLoader").fadeOut();
                error_msg(data.error);
                //console.log(data);

            } else if (data.status == "success") {
                // //console.log(data);
                showMsgPopup("Success", "Successfully Added", '', 'normal')
                setTimeout(function () {
                    window.open(url + 'Customer_order/print_assign_order/' + data.assign_id, '_blank');
                    close_toast_popup(url + 'Customer_order')
                }, 2000)

            } else {
              $(".ajaxLoader").fadeOut();
                showMsgPopup("Error", "Something Went Wrong Try Again", '', 'error')
                setTimeout(function () {
                    // close_toast_popup(url+'party')
                }, 2000)
            }
        },
        error: function (jqXHR, textStatus, errorThrown)
        {

        }
    });
}

function order_module_receive_products(dropdownobj) {
    karigar_id = $(dropdownobj).val();
    $.ajax({
        url: url + "Pending_to_be_recieved/get_order_by_karigar",
        type: "POST",
        dataType: "json",
        data: {karigar_id: karigar_id},
        success: function (data, textStatus, jqXHR) {
            if (data.status == "success") {
                var html = "";
                $.each(data.data, function (key, val) {
                    html += "<tr>" +
                            "<input type='hidden' name='assign_order_id[]' value='" + val.id + "'></td>" +
                            "<td>" + val.order_name + "</td>" +
                            "<td>" + val.approx_weight + "</td>" +
                            "<td><input type='text' name='quantity[]' value='" + val.remaining_qty + "' class='form-control'>" +
                            "<span class='text-danger col-sm-12' id='quantity_" + key + "_error'></span></td>" +
                            "<td><input type='text' name='net_weight[]' value='' class='form-control'>" +
                            "<span class='text-danger col-sm-12' id='net_wt_" + key + "_error'></span></td>" +
                            "</tr>";
                });
                $('#tbl_body').html(html);

            } else {
                $('#tbl_body').html('<tr><td colspan="4">No Data Found</td></tr>');
            }
        },
        error: function (jqXHR, textStatus, errorThrown) {
        }
    });
}

function recived_product_order_module() {
    $.ajax({
        url: url + "Pending_to_be_recieved/store",
        type: "POST",
        dataType: "json",
        data: $('form#receive_product_form').serialize(),
        success: function (data, textStatus, jqXHR) {
            if (data.status == 'failure') {
                error_msg(data.error);
                //console.log(data);

            } else if (data.status == "success") {
                showMsgPopup("Success", "Successfully Received", '', 'normal')
                setTimeout(function () {
                    close_toast_popup(url + 'Pending_to_be_recieved')
                }, 2000)

            } else {

                showMsgPopup("Error", "Something Went Wrong Try Again", '', 'error')
                setTimeout(function () {
                    // close_toast_popup(url+'party')
                }, 2000)
            }
        },
        error: function (jqXHR, textStatus, errorThrown) {
        }
    });
}

function sent_to_customer(id) {
    $.ajax({
        url: url + "Recieved_customer_products/sent_to_customer",
        type: "POST",
        dataType: "json",
        data: {id: id},
        success: function (data, textStatus, jqXHR) {
            if (data.status == "success") {
                showMsgPopup("Success", "Successfully Sent To Customer", '', 'normal')
                setTimeout(function () {
                    close_toast_popup(url + 'Recieved_customer_products')
                }, 200)

            } else {

                showMsgPopup("Error", "Something Went Wrong Try Again", '', 'error')
                setTimeout(function () {
                    // close_toast_popup(url+'party')
                }, 200)
            }
        },
        error: function (jqXHR, textStatus, errorThrown) {
        }
    });
}

function order_module_change_status(status, id) {
  $(".ajaxLoader").fadeIn();
    net_weight = "";
    if (status == 3) {
        net_weight = $('#net_' + id).val();
        var weight_range_id = parseFloat($('#net_' + id).attr('weight'));
        var max_limit_weight = weight_range_id + (weight_range_id * 15 / 100);
        var low_limit_weight = weight_range_id - (weight_range_id * 15 / 100);
        var category_id = $('#cat_' + id).val();
        if (!parseFloat(net_weight)) {
          $(".ajaxLoader").fadeOut();
            $('#error_' + id).html('Please Enter Valid Weight');
            return false;
        }
        //console.log(category_id);
        if (!category_id) {
          $(".ajaxLoader").fadeOut();
            $('#select_error_' + id).html('Please select Category');
            return false;
        }
        if (net_weight > max_limit_weight || net_weight < low_limit_weight) {
          $(".ajaxLoader").fadeOut();
            $('#error_' + id).html('Weight Not Matched, Should be between ' + low_limit_weight + ' - ' + max_limit_weight);
            showMsgPopup("Error", "Entered Weight Is Not As Per Order", '', 'error')
            return false;
        }
    }
    if(status == 6){
          var conf = confirm("Are you sure you want to cancel?");
          if(!conf){
        $(".ajaxLoader").fadeOut();
          return false;
         } 
    }
    $.ajax({
        url: url + "Customer_order/change_status",
        type: "POST",
        dataType: "json",
        data: {id: id, status: status, net_weight: net_weight, category_id: category_id, is_single: true},
        success: function (data, textStatus, jqXHR) {
            if (data.status == "success") {
                if (status == 2) {
                    window.open(url + 'Customer_order/send/' + data.order_id, '_blank');
                    msg = "Successfully Send To Karigar";
                } else if (status == 3) {
                    window.open(url + 'Customer_order/receive_print/' + data.order_id, '_blank');
                    msg = "Successfully Send To Karigar";
                } else if(status == 6) {
                    msg = "Successfully Cancelled";
                }else{
                   msg = "Successfully Sent To Customer";
                }
                showMsgPopup("Success", msg, '', 'normal')

                setTimeout(function () {
                    if (status == 4) {
                        window.open(url + "Customer_order/delivery_print/" + data.order_id + "?status=customer", '_blank');
                    }
                    location.reload();
                }, 200)

            } else {
              $(".ajaxLoader").fadeOut();
                showMsgPopup("Error", "Something Went Wrong Try Again", '', 'error')
                setTimeout(function () {
                    // close_toast_popup(url+'party')
                }, 200)
            }
        },
        error: function (jqXHR, textStatus, errorThrown) {
        }
    });
}
function order_submit() {
  $(".ajaxLoader").fadeIn();
    var status = $('#status').val();    
    $.ajax({
        url: url + "Customer_order/change_status",
        type: "POST",
        dataType: "json",
        data: $('form#order').serialize(),
        success: function (data, textStatus, jqXHR) {
            console.log(data);
            //return false;
            if (data.status == "success") {
              $(".ajaxLoader").fadeOut();
                if (status == 2) {
                    window.open(url + 'Customer_order/send/' + data.order_id, '_blank');
                    msg = "Successfully Sent To Pending To Received Card";
                    redirect_url = url + "Customer_order/?status=not_send";
                } else if (status == 3) {
                    window.open(url + 'Customer_order/receive_print/' + data.order_id, '_blank');
                    msg = "Successfully Received";
                    redirect_url = url + "Customer_order/?status=pending";
                } else {
                    msg = "Successfully Sent To Customer";
                }
                showMsgPopup("Success", msg, '', 'normal')


                setTimeout(function () {
                    if (status == 4) {
                        window.open(url + "Customer_order/delivery_print/" + data.order_id + "?status=customer", '_blank');
                    }
                    close_toast_popup(redirect_url)
                }, 2000)

            } else if (data.status == "failure") {
              $(".ajaxLoader").fadeOut();
                // console.log(data.error)
                error_msg(data.error);
            } else if (data.status == "failure1") {
              $(".ajaxLoader").fadeOut();
                showMsgPopup("Error", data.error, '', 'error')
            }
        },
        error: function (jqXHR, textStatus, errorThrown) {
        }
    });
}
function send_to_party(action) {
    var error = 0;
    var checkedValues = $('input:checkbox:checked').map(function () {
        return this.value;
    }).get();

    if (checkedValues.length == 0) {
        error = 1;
        showMsgPopup("Error", "Please check at least one checkbox", '', 'error');
    }
    if (error == 0) {
        $.ajax({
            url: url + "Customer_order/received_order/change_status",
            type: "POST",
            dataType: "json",
            data: $('form#order').serialize(),
            success: function (data, textStatus, jqXHR) {
                if (data.status == "success") {
                    if (action == 'accepted') {
                        redirect_url = "?status=accepted";
                    } else {
                        redirect_url = "";
                    }
                    window.open(url + "Customer_order/received_order/delivery_print", '_blank');
                    showMsgPopup("Success", "Successfully Sent To Customer", '', 'normal')
                    setTimeout(function () {
                        close_toast_popup(url + 'Customer_order/received_order' + redirect_url)
                    }, 2000)


                } else if (data.status == "failure") {
                    showMsgPopup("Error", "Please check at least one parameter", '', 'error')
                }
            },
            error: function (jqXHR, textStatus, errorThrown) {
            }
        });
    }
}

function save_parent_sub_category(action) {
    $.ajax({
        url: url + "parent_sub_category/" + action,
        type: "POST",
        dataType: "json",
        data: $('form#product_category').serialize(),
        success: function (data, textStatus, jqXHR)
        {
            if (data.status == 'failure')
            {
                error_msg(data.error);

            } else if (data.status == "success") {
                showMsgPopup("Success", "Successfully Added", '', 'normal')
                setTimeout(function () {
                    close_toast_popup(url + 'parent_sub_category')
                }, 2000)

            } else {

                showMsgPopup("Error", "Something Went Wrong Try Again", '', 'error')
                setTimeout(function () {
                    close_toast_popup(url + 'parent_sub_category')

                }, 2000)
            }
        },
        error: function (jqXHR, textStatus, errorThrown)
        {

        }
    });
}
function accounting_receive() {
    $.ajax({
        url: url + "accounting_department/store",
        type: "POST",
        dataType: "json",
        data: $('form#accounting_receive').serialize(),
        success: function (data, textStatus, jqXHR)
        {
            if (data.status == 'failure')
            {
                error_msg(data.error);

            } else if (data.status == "success") {
                showMsgPopup("Success", "Successfully Added", '', 'normal')
                setTimeout(function () {
                    close_toast_popup(url + 'parent_sub_category')
                }, 2000)

            } else {

                showMsgPopup("Error", "Something Went Wrong Try Again", '', 'error')
                setTimeout(function () {
                    close_toast_popup(url + 'parent_sub_category')

                }, 2000)
            }
        },
        error: function (jqXHR, textStatus, errorThrown)
        {

        }
    });

}
function Delete_customer_orders(id, tr, controller,status="") {
    var conf = confirm("Are you sure you want to delete?");
    if (conf)
    {
        $.ajax({
            url: url + controller + "/" + "delete",
            type: "POST",
            dataType: "json",
            data: {id: id},
            success: function (data, textStatus, jqXHR)
            {
                if (data.status == 'failure')
                {
                    error_msg(data.error);
                } else if (data.status == "success") {
                    if (controller == 'Product'){
                        controller = document.referrer;
                    }else if(status != ''){
                      controller =   url + controller + status;
                    }else{
                        controller = url + controller ;
                    }
                    showMsgPopup("Success", "Successfully Deleted", '', 'normal')
                    setTimeout(function () {
                        close_toast_popup(controller)


                    }, 2000)

                } else {

                    showMsgPopup("Error", "Something Went Wrong Try Again", '', 'error')
                    setTimeout(function () {
                        close_toast_popup(url + controller)

                    }, 2000)
                }

            },
            error: function (jqXHR, textStatus, errorThrown)
            {

            }
        });
    }
}

function Delete_customer(id) {
    var conf = confirm("Are you sure you want to delete?");
    if (conf)
    {
        $.ajax({
            url: url + "User_app/" + "delete",
            type: "POST",
            dataType: "json",
            data: {id: id},
            success: function (data, textStatus, jqXHR)
            {
               if (data.status == "success") {
                    
                showMsgPopup("Success", "Deleted Successfully", '', 'normal')
                setTimeout(function () {
                    location.reload();
                }, 200)
                } 

            },
            error: function (jqXHR, textStatus, errorThrown)
            {

            }
        });
    }
}
function get_karigar_engaged_products(id) {
    var product_sel = '';
    $('#trdata').html('');
    $.ajax({
        url: url + 'Receive_products/get_karigar_engaged_products',
        type: "POST",
        dataType: "json",
        data: {karigar_id: id},
        success: function (data, textStatus, jqXHR)
        {

            product_sel += '<select class="form-control" onchange="get_engaged_products(this.value)"" id="parent_category_id"><option value="0">Please Select Order</option>'
            $.each(data, function (key, val) {
                product_sel += '<option value="' + val.id + '">' + val.order_id + ' (' + val.count + ')</option>'
            })
            product_sel += '</select>';
            $('#create_orders').html(product_sel);

        },
        error: function (jqXHR, textStatus, errorThrown)
        {

        }
    });
}
function get_engaged_products(id,type="") {
    //alert(upload_images);
    var product_tabl = '';
    var karigar_id = $("#kariger_id").val();
    var order_id = $("#parent_category_id").val();
    var prodcut_code = $("#prodcut_code").val();
    var category_name = $("#category_name").val();
    var article_id = $("#article_id").val();
    var barcode_id = $("#barcode_id").val();
    var old_val= $('#barcode_append_id').val();
     if(type=="append_data"){
       arr = old_val.split(',');
        for(i=0; i < arr.length; i++){
          if(barcode_id == arr[i]){
             $('#barcode_id').val(""); 
             $('#barcode_id').focus(); 
            showMsgPopup("Error", "Product Already Exists", '', 'error');
            return false;
          }
        }   
     }
     
    //alert(prodcut_code);
    $.ajax({
        url: url + 'Receive_products/get_karigar_engaged_products',
        type: "POST",
        dataType: "json",
        data: {order_id: order_id, karigar_id: karigar_id, prodcut_code: prodcut_code, category_name: category_name, article_id: article_id,barcode_id:barcode_id,old_val:old_val,type:type},
        success: function (data, textStatus, jqXHR)
        {
            console.log(data)
            if (data == '') {
                product_tabl += '<tr><td colspan="10">Data Not Found</td><tr>';
            }
            $('.products').show();
            // $.each(data, function (key, val) {
            //     product_tabl += '<tr>';
            //     product_tabl += '<input type="hidden" name="received_products[corporate][' + val.id + ']" value="' + val.corporate + '">';
            //     product_tabl += '<input type="hidden" name="received_products[product_code][' + val.id + ']" value="' + val.sort_Item_number + '">';
            //     product_tabl += '<input type="hidden" name="received_products[category_name][' + val.id + ']" value="' + val.cat_name + '">';
            //     product_tabl += '<input type="hidden" name="received_products[corporate_product_id][' + val.id + ']" value="' + val.corporate_product_id + '">';
            //     product_tabl += '<input type="hidden" name="received_products[karigar_id][' + val.id + ']" value="' + val.karigar_id + '">';
            //     product_tabl += '<input type="hidden" name="received_products[email_order_id][' + val.id + ']" value="' + val.order_id + '">';
            //     product_tabl += '<input type="hidden" name="received_products[barcode_path][' + val.id + ']" value="' + val.barcode_path + '">';
            //     product_tabl += '<input type="hidden" name="received_products[order_id][' + val.id + ']" value="' + val.email_order_id + '">';
            //     if (val.line_number) {
            //         product_tabl += '<input type="hidden" name="received_products[line_number][' + val.id + ']" value="' + val.line_number + '">';
            //     } else {
            //         product_tabl += '<input type="hidden" name="received_products[line_number][' + val.id + ']" value="0">';
            //     }
            //     //product_tabl +='<td><div class="checkbox checkbox-purpal product_check"><input type="checkbox" checked="" class="from-control" name="received_products[id]['+val.id+']" value="'+val.id+'"><label></label></div></td>';
            //     product_tabl += '<input type="hidden" class="from-control" name="received_products[id][' + val.id + ']" value="' + val.id + '">';

            //     product_tabl += '<td><img src="'+upload_images+'barcode/'+val.barcode_path+'"></td>';
            //     product_tabl += '<td>' + val.order_id + '</td>';
            //     product_tabl += '<td>' + val.karigar_id + '</td>';

            //     product_tabl += '<td>' + val.sort_Item_number + '</td>';
            //     if (val.line_number) {
            //         product_tabl += '<td><span style="float:right">' + val.line_number + '</span></td>';
            //     } else {
            //         product_tabl += '<td><span style="float:right">0</span></td>';
            //     }
            //     product_tabl += '<td>' + val.cat_name + '</td>';
            //     product_tabl += '<td>' + val.sub_cat_name + '</td>';

            //     product_tabl += '<td><input type="text" class="form-control" value="' + val.quantity + '" name="received_products[quantity][' + val.id + ']"><p class="text-danger" id="quantity_error_' + val.id + '"></p></td>';
            //     if (val.corporate == '2') {
            //         //var disply_wt_r='display:none';
            //         var tolerance = val.tolerance_c_wt;
            //         var from_wt_torlen = parseFloat(val.net_weight) - parseFloat(tolerance);
            //         var to_wt_torlen = parseFloat(tolerance) + parseFloat(val.net_weight);
            //         var tolerance_wt_range = from_wt_torlen.toFixed(2) + ' - ' + to_wt_torlen.toFixed(2);
            //         if (val.cr_size == null) {
            //             var cr_size = '-';
            //         } else {
            //             var cr_size = val.cr_size;
            //         }
            //         product_tabl += '<td><input type="text" class="form-control" value="' + cr_size + '" name="received_products[size][' + val.id + ']"><p class="text-danger" id="size_error_' + val.id + '"></p></td>';
            //     } else {
            //         //var disply_wt_r='display:block';
            //         var tolerance = val.tolerance_wt;
            //         var from_wt_torlen = parseFloat(val.cp_quantity) - parseFloat(tolerance);
            //         var to_wt_torlen = parseFloat(tolerance) + parseFloat(val.cp_quantity);
            //         var tolerance_wt_range = from_wt_torlen.toFixed(2) + ' - ' + to_wt_torlen.toFixed(2);

            //         if (val.size_master == null) {
            //             var size_master = '-';
            //         } else {
            //             var size_master = val.size_master;
            //         }
            //         product_tabl += '<td><input type="text" class="form-control" value="' + size_master + '" name="received_products[size][' + val.id + ']"><p class="text-danger" id="size_error_' + val.id + '"></p></td>';
            //     }
            //     product_tabl += '<input type="hidden" class="from-control" name="received_products[to_wt_torlen][' + val.id + ']" value="' + to_wt_torlen + '">';
            //     product_tabl += '<input type="hidden" class="from-control" name="received_products[from_wt_torlen][' + val.id + ']" value="' + from_wt_torlen + '">';
            //     product_tabl += '<td><input type="text" class="form-control gross_wt" onchange="calcate_net_wt(' + val.id + ',this)" id="gross_wt_' + val.id + '" value="" name="received_products[gross_wt][' + val.id + ']"><p class="text-danger" id="gross_wt_error_' + val.id + '"></p><span>(' + tolerance_wt_range + ')</span></td>';

            //     product_tabl += '<td><input type="text" class="form-control" onchange="calcate_net_wt(' + val.id + ',this)"  id="less_wt_' + val.id + '"  name="received_products[less_wt][' + val.id + ']" value="0" ><p class="text-danger" id="less_wt_error_' + val.id + '"></p></td>';

            //     product_tabl += '<td><input type="text" id="net_wt_' + val.id + '" class="form-control" value="" name="received_products[net_wt][' + val.id + ']" readonly><p class="text-danger" id="net_wt_error_' + val.id + '"></p></td>';

            //     // product_tabl +='<td><input type="text" id="net_wt_'+val.id+'" class="form-control" value="" name="received_products[stone_wt]['+val.id+']"><p class="text-danger" id="stone_wt_error_'+val.id+'"></p></td>';

            //     product_tabl += '<td><div class="checkbox checkbox-purpal qc_check"><input type="checkbox" checked="" class="from-control" name="received_products[qc][' + val.id + ']" value="' + val.id + '"><label></label></div></td>';
            //     // product_tabl +='<td><a id="add_more" class="btn btn-danger small loader-hide  btn-sm" href="javascript:void(0)" onclick="remove_tag_code(this)" title="Add More">Remove</a></td>';
            //     product_tabl += '</tr>';
            // })

            $('#trdata').html(data.html_details);
            if(barcode_id  && type=="append_data"){    
                if(data.append_details !=""){
                    if(data.append_details == "already_exist"){

                       showMsgPopup("Error", "Product Already Exists", '', 'error')

                    }else{           
                  $('#appenddata').append(data.append_details);
                  showMsgPopup("Success", "Successfully Added", '', 'normal');
                  if(old_val){
                     new_val = old_val+barcode_id+","; 
                  }else{
                    new_val = barcode_id+",";   
                  }
                  $('#barcode_append_id').val(new_val);    
                     
                    }
                }else{
                   showMsgPopup("Error", "No data Found", '', 'error') 
                }
                
             
            } 
            $('.keycontrol').formNavigation();
            $('#barcode_id').val("");
            $('#barcode_id').focus(); 
        },
        error: function (jqXHR, textStatus, errorThrown)
        {

        }
    });
}


function Delete_customer_orders_image(id, order_id, el) {

    $.ajax({
        url: url + "Customer_order/deleteimage",
        type: "POST",
        dataType: "json",
        data: {id: id, order_id: order_id},
        success: function (data, textStatus, jqXHR)
        {
            if (data.status == 'failure')
            {
                error_msg(data.error);

            } else if (data.status == "success") {
                var div_img_id = $(el).closest('.form-group').attr('div_img_id');
                $(el).closest('div').html('');
                var onchange = $('.select_image' + div_img_id).attr('onchange');
                $('.select_image' + div_img_id).attr('onchange', "update_cust_ord_img('new" + div_img_id + "',this)");
                $('.select_image' + div_img_id).attr('name', "image[new" + div_img_id + "]");
                $('.select_image' + div_img_id).val('');

                //$('#div'+id).remove();
                controller = url + 'Customer_order/edit/' + order_id;
                showMsgPopup("Success", "Successfully delete", '', 'normal')
                // setTimeout(function(){
                //             close_toast_popup(controller)

                //         },2000)


            } else {

                showMsgPopup("Error", "Something Went Wrong Try Again", '', 'error')
                setTimeout(function () {
                    close_toast_popup(controller)

                }, 2000)

            }
        },
        error: function (jqXHR, textStatus, errorThrown)
        {

        }
    });

}
function update_cust_ord_img(id, el) {
    var formData = new FormData(document.querySelector("form#customer_order_form"));
    formData.append('image_id', id);
    $.ajax({
        url: url + "Customer_order/update_image",
        type: "POST",
        dataType: "json",
        data: formData,
        processData: false,
        contentType: false,
        success: function (data, textStatus, jqXHR)
        {
            if (data.status == 'failure')
            {
                error_msg(data.error);

            } else if (data.status == "success") {
                console.log(el, data.up_image_id, order_id)
                var image_id = $(el).closest('.form-control').attr('data-id');
                var onchange = $('.select_image' + image_id).attr('onchange');
                $('.select_image' + image_id).attr('onchange', "update_cust_ord_img(" + data.up_image_id + ",this)");
                $('.select_image' + image_id).attr('name', "image[" + data.up_image_id + "]");
                order_image_preview(el, data.up_image_id, order_id);
                /*var img_html=$('.view_image'+image_id).attr(data.up_image_id);
                 $('.view_image'+image_id).html(img_html);*/
                showMsgPopup("Success", "Successfully update", '', 'normal')
                // setTimeout(function(){
                //             location.reload();
                //             //close_toast_popup(controller)

                //         },2000)

            }else if(data.status == "error"){
               showMsgPopup("Error", data.msg, '', 'error')
                
            } else {

                showMsgPopup("Error", "Something Went Wrong Try Again", '', 'error')
                setTimeout(function () {
                    location.reload();

                }, 2000)

            }
        },
        error: function (jqXHR, textStatus, errorThrown)
        {

        }
    });

}
function Export_karigar_images() {
    var search_val = $('#Engaged_karigar_list_filter input').val();
    window.open(url + 'Engaged_karigar_list/export_karigar_images?search=' + search_val, '_blank');
}
function mnfg_accept() {
    $.ajax({
        url: url + "Mnfg_accounting/store",
        type: "POST",
        dataType: "json",
        data: $('form#mnfg').serialize(),
        success: function (data, textStatus, jqXHR)
        {
            if (data.status == 'failure')
            {
                error_msg(data.error);

            } else if (data.status == "success") {
                //  $('#div'+id).remove();
                // controller = url+'Customer_order/edit/'+order_id;
                // showMsgPopup("Success","Successfully delete",'','normal')
                // setTimeout(function(){
                //             close_toast_popup(controller)

                //         },2000)


            } else {

                showMsgPopup("Error", "Something Went Wrong Try Again", '', 'error')
                setTimeout(function () {
                    close_toast_popup(controller)

                }, 2000)

            }
        },
        error: function (jqXHR, textStatus, errorThrown)
        {

        }
    });
}
/*Pending for QC  Reject All and Amend All */

function Pending_product_for_qc_amend_remark(btn_obj) {
    var error = 0;
    var checkedValues = $('input:checkbox:checked').map(function () {
        return this.value;
    }).get();

    if (checkedValues.length == 0) {
        error = 1;
        showMsgPopup("Error", "Please check at least one checkbox", '', 'error');
    }

    $("#type_product_for_qc").val(btn_obj);
    var remark = $("#remark").val();

    if (error == 0 && btn_obj == 'Amend') {

        $.ajax({
            url: url + "Receive_products/check_corporate",
            type: "POST",
            dataType: "json",
            data: $('form#export').serialize(),
            success: function (data, textStatus, jqXHR)
            {
                if (data.status == 'failure')
                {
                    error = 1;
                    showMsgPopup("Error", "Please select  Relience Jewels", '', 'error');

                } else if (data.status == "success") {
                    error = 0;
                    $('#pending_product_for_qc_remark').modal('show');
                } else {
                    showMsgPopup("Error", "Something Went Wrong Try Again", '', 'error')
                    setTimeout(function () {
                        close_toast_popup(url + 'Corporate_Process')

                    }, 2000)
                }

            },
            error: function (jqXHR, textStatus, errorThrown)
            {

            }
        });


    } else {
        if (error == 0) {
            $('#pending_product_for_qc_remark').modal('show');
        }
    }

}

function multiple_qc_send_to_process() {
    var error = 0;
    var type_product_for_qc = $("#type_product_for_qc").val();
    var remark = $("#remark").val();
    if (remark == '') {
        var error = 1;
        $('#qc_remark_error').text('Remark field is required');
    } else if (error == 0) {


        var conf = confirm("Are you sure you want to " + type_product_for_qc + " ?");
        if (!conf)
        {
            return false;
        }
        if (type_product_for_qc == 'Amend') {
            var ctrl = "send_to_amended";
            var msg = "amended";
        }
        if (type_product_for_qc == 'Reject') {
            var ctrl = "send_to_rejected";
            var msg = "rejected";
        }
        $.ajax({
            url: url + "Receive_products/" + ctrl,
            type: "POST",
            dataType: "json",
            data: $('form#export').serialize(),
            success: function (data, textStatus, jqXHR)
            {
                if (data.status == 'failure')
                {
                    showMsgPopup("Error", data.error, '', 'error')


                } else if (data.status == "success") {
                    $('.send_to_hm').attr('disabled', true);
                    showMsgPopup('Success', "Successfully sent to " + msg, '', 'normal')
                    setTimeout(function () {
                        close_toast_popup(url + 'Receive_products?status=pending')

                    }, 600)


                } else {
                    showMsgPopup("Error", "Something Went Wrong Try Again", '', 'error')
                    setTimeout(function () {
                        close_toast_popup(url + 'Corporate_Process')

                    }, 2000)
                }

            },
            error: function (jqXHR, textStatus, errorThrown)
            {

            }
        });
    }
}





/*export hallmarking list function  */
function export_hallmarking_list()
{
    $.ajax({
        url: url + "hallmarking_list/export",
        type: "POST",
        dataType: "json",
        data: $('form#export').serialize(),
        success: function (data, textStatus, jqXHR)
        {
            //console.log(data)
            close_toast_popup(data);
            //close_toast_popup(url+'Corporate_Dashboard')

        },
        error: function (jqXHR, textStatus, errorThrown)
        {

        }
    });
}

/*View Design Sent For Approval  Reject All and approved All */
function multiple_pieces_to_approved() {
    var conf = confirm("Are you sure you want to approved?");
    if (!conf)
    {
        return false;
    }


    $.ajax({
        url: url + "Receive_pieces/pieces_to_approved",
        type: "POST",
        dataType: "json",
        data: $('form#product_form').serialize(),
        success: function (data, textStatus, jqXHR)
        {   //console.log(data);

            if (data.status == 'failure')
            {
                showMsgPopup("Error", data.error, '', 'error')


            } else if (data.status == "success") {

                showMsgPopup("Success", "Successfully sent to approved", '', 'normal')
                setTimeout(function () {
                    close_toast_popup(url + 'Receive_pieces/show/' + data.order_id)

                }, 600)


            } else {
                showMsgPopup("Error", "Something Went Wrong Try Again", '', 'error')
                setTimeout(function () {

                    close_toast_popup(url + 'Dashboard')

                }, 2000)
            }

        },
        error: function (jqXHR, textStatus, errorThrown)
        {

        }
    });
}

function multiple_pieces_to_rejected() {
    var conf = confirm("Are you sure you want to rejected?");
    if (!conf)
    {
        return false;
    }
    $.ajax({
        url: url + "Receive_pieces/pieces_to_rejected",
        type: "POST",
        dataType: "json",
        data: $('form#product_form').serialize(),
        success: function (data, textStatus, jqXHR)
        {
            if (data.status == 'failure')
            {
                showMsgPopup("Error", data.error, '', 'error')


            } else if (data.status == "success") {

                showMsgPopup("Success", "Successfully sent to rejected", '', 'normal')
                setTimeout(function () {
                    close_toast_popup(url + 'Receive_pieces/show/' + data.order_id)

                }, 600)


            } else {
                showMsgPopup("Error", "Something Went Wrong Try Again", '', 'error')
                setTimeout(function () {
                    close_toast_popup(url + 'Dashboard')

                }, 2000)
            }

        },
        error: function (jqXHR, textStatus, errorThrown)
        {

        }
    });
}
/*QC Amended  Reject All and karigar All */
function multiple_amended_pro_to_karigar() {
    var checkedValues = $('input:checkbox:checked').map(function () {
        return this.value;
    }).get();

    var error = 0;
    if (checkedValues.length == 0) {
        error = 1;
        showMsgPopup("Error", "Please check at least one checkbox", '', 'error');
    }
    if (error == 0) {
        var conf = confirm("Are you sure you want to send karigar?");
        if (!conf)
        {
            return false;
        }

        $.ajax({
            url: url + "Amend_products/multiple_send_to_karigar",
            type: "POST",
            dataType: "json",
            data: $('form#amend_products').serialize(),
            success: function (data, textStatus, jqXHR)
            {   //console.log(data);
//alert(url+'Amend_products/view/'+data.karigar_id);
                if (data.status == 'failure')
                {
                    showMsgPopup("Error", data.error, '', 'error')


                } else if (data.status == "success") {

                    showMsgPopup("Success", "Successfully sent to karigar", '', 'normal')
                    setTimeout(function () {
                        close_toast_popup(url + 'Print_Order/index/' + data.karigar_id)


                    }, 600)
                    //window.location.href();

                } else {
                    showMsgPopup("Error", "Something Went Wrong Try Again", '', 'error')
                    setTimeout(function () {

                        close_toast_popup(url + 'Corporate_Process')

                    }, 2000)
                }

            },
            error: function (jqXHR, textStatus, errorThrown)
            {

            }
        });
    }
}

function multiple_amended_pro_to_stock() {
    var checkedValues = $('input:checkbox:checked').map(function () {
        return this.value;
    }).get();
    var error = 0;
    if (checkedValues.length == 0) {
        error = 1;
        showMsgPopup("Error", "Please check at least one checkbox", '', 'error');
    }
    if (error == 0) {
        var conf = confirm("Are you sure you want  send to stock?");
        if (!conf)
        {
            return false;
        }
        $.ajax({
            url: url + "Amend_products/multiple_send_to_stock",
            type: "POST",
            dataType: "json",
            data: $('form#amend_products').serializeArray(),
            success: function (data, textStatus, jqXHR)
            { //console.log(data);
                if (data.status == 'failure')
                {
                    showMsgPopup("Error", data.error, '', 'error')


                } else if (data.status == "success") {
                    //alert(url+'Amend_products/view/'+data.karigar_id);
                    showMsgPopup("Success", "Successfully sent to stock", '', 'normal')
                    setTimeout(function () {
                        close_toast_popup(url + 'Amend_products/show/' + data.karigar_id)

                    }, 600)
                    // window.location.href();


                } else {
                    showMsgPopup("Error", "Something Went Wrong Try Again", '', 'error')
                    setTimeout(function () {
                        close_toast_popup(url + 'Corporate_Process')

                    }, 2000)
                }

            },
            error: function (jqXHR, textStatus, errorThrown)
            {

            }
        });
    }
}
function mnfg_send_to_karigar(id) {
    var weight = $('#weight_' + id).val();
    var quantity = $('#quantity_' + id).val();
    var original_qty = $('#original_qty_' + id).val();
    var department_id = $('#department_id_' + id).val();

    if (weight && quantity) {
        $('#karigar_weight').val(weight);
        $('#karigar_qnt').val(quantity);
        $('#original_qnt').val(original_qty);
        $('#qc_id').val(id);
        $('#department_id').val(department_id);
        $('#mnfg_send_to_karigar').modal('show');
        $('#mnfg_karigar_id').find('option[value="' + $('#karigar_id_' + id).val() + '"]').attr("selected", true);
    } else {
        showMsgPopup("Error", "Please add Weight and Quantity", '', 'error');
        return false;
    }
}
function bom_mnfg_send_to_karigar(id) {
    var weight = $('#weight_' + id).val();
    var original_wt = $('#original_wt_' + id).val();
    var department_id = $('#department_id_' + id).val();

    if (weight) {
        $('#weight').val(weight);
        $('#original_wt').val(original_wt);
        $('#qc_id').val(id);
        $('#department_id').val(department_id);
        $('#mnfg_send_to_karigar').modal('show');
        $('#mnfg_karigar_id').find('option[value="' + $('#karigar_id_' + id).val() + '"]').attr("selected", true);
    } else {
        showMsgPopup("Error", "Please enter Weight", '', 'error');
        return false;
    }
}

function mnfg_send_to_karigar_submit() {
    var conf = confirm("Are you sure you want to resend this product to karigar?");
    if (conf)
    { //window.location=url+'Manufacturing_quality_control/resend_to_karigar/'+id;
        var qc_id = $('#qc_id').val();
        var karigar_id = $('#mnfg_karigar_id').val();
        var department_id = $('#department_id').val();
        if (department_id == '2' || department_id=='3' || department_id =='6' || department_id=='10') {
            var weight = $('#weight').val();
            var original_wt = $('#original_wt').val();
        } else {
            var weight = $('#karigar_weight').val();
            var quantity = $('#karigar_qnt').val();
            var original_qty = $('#original_qnt').val();
        }


       /* console.log({weight:weight,quantity:quantity,original_qty:original_qty,karigar_id:karigar_id,department_id:department_id})
        return false;*/
        $.ajax({
            url: url + "Manufacturing_quality_control/resend_to_karigar/" + qc_id,
            type: "POST",
            dataType: "json",
            data: {weight: weight, quantity: quantity, original_qty: original_qty, karigar_id: karigar_id, department_id: department_id, original_wt: original_wt},
            success: function (data, textStatus, jqXHR)
            {
                if (data.status == 'failure') {
                    error_msg(data.error);

                } else if (data.status == "success") {
                    //showMsgPopup("Success","Successfully Rejected",'','normal')
                    setTimeout(function () {
                        close_toast_popup(data.url)
                    }, 500)
                } else {
                    showMsgPopup("Error", "Something Went Wrong Try Again", '', 'error')
                }
            },
            error: function (jqXHR, textStatus, errorThrown)
            {

            }
        });
    }
}


/*stock to send party*/
function Send_stock_products_party(st_id) {
    $('#s_' + st_id).attr('disabled', true);
    // return false;

    $.ajax({
        url: url + "stock/Send_stock_products_party/" + st_id,
        type: "POST",
        dataType: "json",
        success: function (data, textStatus, jqXHR)
        {
            if (data.status == 'failure')
            {
                showMsgPopup("Error", 'Something Went Wrong Try Again', '', 'error')

            } else if (data.status == "success") {

                showMsgPopup("Success", "Product successfully add to stock", '', 'normal')
                setTimeout(function () {
                    close_toast_popup(url + 'Stock/1/?status=stock')

                }, 2000)

            } else {

                showMsgPopup("Error", "Something Went Wrong Try Again", '', 'error')
                setTimeout(function () {
                    close_toast_popup(url + 'Stock/1/?status=stock')

                }, 2000)
            }

        },
        error: function (jqXHR, textStatus, errorThrown)
        {

        }
    });
}
/*stock to resend karigar*/
function Send_stock_products_karigar(st_id) {
    $('#b_' + st_id).attr('disabled', true);
    $.ajax({
        url: url + "stock/Send_stock_products_karigar/" + st_id,
        type: "POST",
        dataType: "json",
        success: function (data, textStatus, jqXHR)
        {
            if (data.status == 'failure')
            {
                showMsgPopup("Error", 'Something Went Wrong Try Again', '', 'error')

            } else if (data.status == "success") {
                showMsgPopup("Success", "Product Successfully Sent to Karigar", '', 'normal')
                setTimeout(function () {
                    close_toast_popup(url + 'Print_Order/index/' + data.karigar_id)

                }, 2000)

            } else {

                showMsgPopup("Error", "Something Went Wrong Try Again", '', 'error')
                setTimeout(function () {
                    close_toast_popup(url + 'Stock/1/?status=stock')

                }, 2000)
            }

        },
        error: function (jqXHR, textStatus, errorThrown)
        {

        }
    });
}

/*repair to ready product*/

function repair_to_ready_product_all() {
    $('.ready_product_btn').prop('disabled', true);
    $.ajax({
        url: url + "repair_product/store_all",
        type: "POST",
        dataType: "json",
        data: $('form#repair_product').serialize(),
        success: function (data, textStatus, jqXHR)
        {
            if (data.status == "success") {
                $('.ready_product_btn').prop('disabled', true);
                window.open(url + 'Ready_product/generate_issue_receipt_repair', '_blank');
                showMsgPopup("Success", "Ready Product Done", '', 'normal')
                setTimeout(function () {
                    close_toast_popup(url + 'repair_product')

                }, 2000)

            } else if (data.status == "failure") {
                $('.ready_product_btn').prop('disabled', false);
                error_msg(data.error);
            } else if (data.status == "failure1") {
                $('.ready_product_btn').prop('disabled', false);
                showMsgPopup("Error", "Please enter weight and net weight", '', 'error')
            } else {
                $('.ready_product_btn').prop('disabled', false);
                showMsgPopup("Error", "Something Went Wrong Try Again", '', 'error')
                setTimeout(function () {
                    close_toast_popup(url + 'repair_product')

                }, 2000)
            }
        },
        error: function (jqXHR, textStatus, errorThrown)
        {

        }
    });

}

function repair_to_ready_product(id) {
    //$('#b_' + id).attr('disabled', true);
    $.ajax({
        url: url + "repair_product/store/" + id,
        type: "POST",
        dataType: "json",
        //data: $('form#repair_product').serialize(),
        data : {id:id,karigar_id:$('#karigar_id_'+id).val(),quantity_o:$('#quantity_o_'+id).val(),quantity:$('#quantity_'+id).val(),chitti_no:$('#chitti_no_'+id).val(),ready_product_id:$('#ready_product_id_'+id).val(),department_id:$('#department_id_'+id).val(),product_code:$('#product_code_'+id).val(),black_beads_wt_o:$('#black_beads_wt_o_'+id).val(),black_beads_wt:$('#black_beads_wt_'+id).val(),gr_wt:$('#ttl_gross_wt_'+id).val(),gr_wt_o:$('#gr_wt_o_'+id).val(),net_wt:$('#net_wt_'+id).val(),net_wt_o:$('#net_wt_o_'+id).val(),few_wt:$('#few_wt_'+id).val(),few_wt_o:$('#few_wt_o_'+id).val(),kundan_pure:$('#kundan_pure_'+id).val(),kundan_pure_o:$('#kundan_pure_o_'+id).val(),mina_wt:$('#mina_wt_'+id).val(),mina_wt_o:$('#mina_wt_o_'+id).val(),wax_wt:$('#wax_wt_'+id).val(),wax_wt_o:$('#wax_wt_o_'+id).val(),color_stone:$('#color_stone_'+id).val(),color_stone_o:$('#color_stone_o_'+id).val(),moti:$('#moti_'+id).val(),moti_o:$('#moti_o_'+id).val(),checker_wt:$('#checker_wt_'+id).val(),checker_wt_o:$('#checker_wt_o_'+id).val(),checker:$('#checker_'+id).val(),checker_o:$('#checker_o_'+id).val(),checker_pcs_o:$('#checker_pcs_o_'+id).val(),checker_pcs:$('#checker_pcs_'+id).val(),checker_amt:$('#checker_amt_'+id).val(),checker_amt_o:$('#checker_amt_o_'+id).val(),kundan_pc:$('#kundan_pc_'+id).val(),kundan_pc_o:$('#kundan_pc_o_'+id).val(),kundan_rate:$('#kundan_rate_'+id).val(),kundan_rate_o:$('#kundan_rate_o_'+id).val(),kundan_amt:$('#kundan_amt_'+id).val(),kundan_amt_o:$('#kundan_amt_o_'+id).val(),stone_wt:$('#stone_wt_'+id).val(),stone_wt_o:$('#stone_wt_o_'+id).val(),color_stone_rate:$('#color_stone_rate_'+id).val(),color_stone_rate_o:$('#color_stone_rate_o_'+id).val(),stone_amt:$('#stone_amt_'+id).val(),stone_amt_o:$('#stone_amt_o_'+id).val(),other_wt:$('#other_wt_'+id).val(),other_wt_o:$('#other_wt_o_'+id).val(),total_wt:$('#ttl_wt_'+id).val(),total_wt_o:$('#total_wt_o_'+id).val()},
        success: function (data, textStatus, jqXHR)
        {
                console.log(data.error);
            if (data.status == 'failure')
            {
                $('#b_' + id).attr('disabled', false);
                error_msg(data.error);


            } else if (data.status == 'failure1')
            {
                $('#b_' + id).attr('disabled', false);
                showMsgPopup("Error", "Please enter weight and net weight", '', 'error')

            } else if (data.status == "success") {
                $('#b_' + id).attr('disabled', true);
                window.open(url + 'Ready_product/generate_issue_receipt_repair', '_blank');
                showMsgPopup("Success", "Successfully Received", '', 'normal')
                setTimeout(function () {
                    location.reload();

                }, 500)
            } else {
                showMsgPopup("Error", "Something Went Wrong Try Again", '', 'error')
            }
        },
        error: function (jqXHR, textStatus, errorThrown) {
        }
    });
}


function repiar_product_issue(id) {
  //  $('#bid_' + id).attr('disabled', true);
    $.ajax({
        url: url + "repair_product/issue",
        type: "POST",
        dataType: "json",
        //data: $('form#repair_product').serialize(),
        data : {id:id},
        success: function (data, textStatus, jqXHR)
        {
          if (data.status == "success") {
                $('#bid_' + id).attr('disabled', true);
                //window.open(url + 'Ready_product/generate_issue_receipt_repair', '_blank');
                showMsgPopup("Success",data.msg , '', 'normal')
                setTimeout(function () {
                    location.reload();

                }, 500)
            } else {
                showMsgPopup("Error", "Something Went Wrong Try Again", '', 'error')
            }
        },
        error: function (jqXHR, textStatus, errorThrown) {
        }
    });
}

/*few box*/
function save_few_weight(action_type) { 

    $('.few_box_btn').attr('disabled', true);
    $.ajax({
        url: url + "Few_box/"+action_type,
        type: "POST",
        dataType: "json",
        data: $('form#few_box_data').serialize(),
        success: function (data, textStatus, jqXHR)
        {
            if (data.status == 'failure')
            {
                $('.few_box_btn').attr('disabled', false);
                error_msg(data.error);

            } else if (data.status == "success") {
                $('.few_box_btn').attr('disabled', true);
                showMsgPopup("Success", "Successfully Added", '', 'normal')
                setTimeout(function () {
                    close_toast_popup(url + 'Few_box')

                }, 2000)

            } else {
                $('.few_box_btn').attr('disabled', false);
                showMsgPopup("Error", "Something Went Wrong Try Again", '', 'error')
                setTimeout(function () {
                    close_toast_popup(url + 'Few_box')

                }, 2000)
            }
        },
        error: function (jqXHR, textStatus, errorThrown)
        {

        }
    });
}

/*sales module add stock*/

function save_sales_stock(action) {
  $(".ajaxLoader").fadeIn();
    var formData = new FormData(document.querySelector("form#sales_stock_form"));
    $.ajax({
        url: url + "sales_stock/" + action,
        type: "POST",
        dataType: "json",
        data: formData,
        processData: false,
        contentType: false,
        success: function (data, textStatus, jqXHR)
        {
            if (data.status == 'failure') {
              $(".ajaxLoader").fadeOut();
                error_msg(data.error);
                console.log(data);

            } else if (data.status == "success") {
              $(".ajaxLoader").fadeOut();
                showMsgPopup("Success", "Successfully Added", '', 'normal')
                setTimeout(function () {
                    close_toast_popup(url + 'sales_stock')

                }, 2000)

            } else {
              $(".ajaxLoader").fadeOut();
                showMsgPopup("Error", "Something Went Wrong Try Again", '', 'error')
                setTimeout(function () {
                    // close_toast_popup(url+'party')
                }, 2000)
            }
        },
        error: function (jqXHR, textStatus, errorThrown)
        {

        }
    });
}
/*import order excel*/
function Save_order_excel() {
  $(".ajaxLoader").fadeIn();
    $('.errors').html('');
    var formdata = new FormData();
    var file_data = $('input[type="file"]')[0].files;
    formdata.append("file_0", file_data[0]);
    $.ajax({
        url: url + "Customer_order/import",
        type: "POST",
        dataType: "json",
        contentType: false,
        processData: false,
        data: formdata,
        success: function (data, textStatus, jqXHR)
        {


             if (data.status == 'failure1') {
              $(".ajaxLoader").fadeOut();
               showMsgPopup("Error", "Quantity not greater than 1", '', 'error')
                //console.log(data);

            }else if (data.status == 'failure')
            {
              $(".ajaxLoader").fadeOut();

                $.each(data.errors, function (key, val) {
                    $.each(val, function (k, v) {
                        if (v != "") {
                            $('.errors').append('<p class="text-danger">' + v + '</p>');
                        }
                    })

                })


            } else if (data.status == "success") {
                showMsgPopup("Success", data.data + " Order Successfully Added", '', 'normal')
                //location.reload();
                setTimeout(function () {
                    close_toast_popup(url + 'Customer_order/Upload_order_excel')

                }, 2000)

            } else {
              $(".ajaxLoader").fadeOut();
                showMsgPopup("Error", "Something Went Wrong Try Again", '', 'error')

            }
        },
        error: function (jqXHR, textStatus, errorThrown)
        {

        }
    });
}

/*sales module add stock*/

function save_deparment_order(action) {
  $(".ajaxLoader").fadeIn();
    var formData = new FormData(document.querySelector("form#department_order_form"));
    $.ajax({
        url: url + "Mfg_dep_direct_order/"+action,
        type: "POST",
        dataType: "json",
        data: formData,
        processData: false,
        contentType: false,
        success: function (data, textStatus, jqXHR)
        {
            if (data.status == 'failure') {
              $(".ajaxLoader").fadeOut();
                error_msg(data.error);
                console.log(data);

            } else if (data.status == "success") {
              $(".ajaxLoader").fadeOut();
                showMsgPopup("Success", "Successfully Added", '', 'normal')
                setTimeout(function () {
                    close_toast_popup(url + 'sales_stock')

                }, 2000)

            } else {
              $(".ajaxLoader").fadeOut();
                showMsgPopup("Error", "Something Went Wrong Try Again", '', 'error')
                setTimeout(function () {
                    // close_toast_popup(url+'party')
                }, 2000)
            }
        },
        error: function (jqXHR, textStatus, errorThrown)
        {

        }
    });
}
function get_category_dep(obj) {
    var categor_code_html = '';
    $.ajax({
        type: "POST",
        data: {department_id: $("#department_id").val()},
        dataType: "json",
        url: url + "sales_stock/get_category_name",
        cache: false,
        success: function (data) {
            console.log(data)
            if (data != null) {
                categor_code_html = '<option value="">Please Select category</option>';
                $.each(data, function (i, obj) {

                    categor_code_html += '<option value="' + obj.code + '">' + obj.name + '</option>';
                });

                $("#category_id").html(categor_code_html);
            }


        }
    });
}

function get_product_category(otnobj){

    if($(otnobj).val()){

        $.ajax({
            url: url + "tag_products/get_max_code",
            type: "POST",
            dataType: "json",
            data: {item_category_code: $(otnobj).val()},
            success: function (data, textStatus, jqXHR)
            {   
                var options = '<option value="">Select Product</option>';
                console.log(data);
                $.each(data,function(key1,val1){
                    options += '<option data-code="'+val1.code+'" value="'+val1.name+'" data-max_cnt="'+val1.max_count+'">'+val1.name+'</option>';
                });
                $('.item_cat').html(options);
            }
        });
    }
}
function get_karigar_wastage(otnobj){
    if($(otnobj).val()){

        $.ajax({
            url: url + "karigar/get_wastage",
            type: "POST",
            dataType: "json",
            data: {karigar_id: $(otnobj).val()},
            success: function (data, textStatus, jqXHR)
            { 
            //   console.log(data.data.wastage);            
               $('.wastage').val(data.data.wastage);
            }
        });
    }
}

$('body').delegate('.create_code', 'change', function () {
    var tag_product_obj = {};
    var event = $(this);
    sub_code = $(this).val();
    var category_id = $('#category_id').val();
    var count = 0;
   
    $(".abc").each(function (key, value) {
        var sel_item_code = $(this).closest('tr').find('.item_cat :selected').attr('data-code');
        var sel_item_cnt = $(this).closest('tr').find('.item_cat :selected').attr('data-max_cnt');
        var sel_product_code = $(this).closest('tr').find('.item_cat').attr('data-product_code');
        $(this).closest('tr').find('.product_cat_code').val(sel_item_code);
        // var sel_product_code = $(this).closest('tr').find('.pm_id').val(category_id);
        if (typeof (sel_item_code) != 'undefined') {
            if (typeof (tag_product_obj[sel_item_code]) == 'undefined') {
                var f_cnt = parseInt(sel_item_cnt);
                tag_product_obj[sel_item_code] = f_cnt;
                $(this).closest('tr').find('.product_code_tp').val(sel_item_code  + '' + category_id + '-' + f_cnt);
            } else {
                var cnt = parseInt(tag_product_obj[sel_item_code]);
                tag_product_obj[sel_item_code] = cnt + 1;
                $(this).closest('tr').find('.product_code_tp').val(sel_item_code  + '' + category_id + '-' + tag_product_obj[sel_item_code]);        
            }

        } else {
            $(this).closest('tr').find('.product_code_tp').val('');
        }
    });

    // var count = 0;
    // $('#stock_products tbody tr td').each(function () {
    //     // product_code = $(this).find('.product_code').val();
    //     // if (sub_code == product_code) {
    //     //     count++;
    //     // }
    // })

    // $.ajax({
    //     url: url + "Tag_products/create_item_code",
    //     type: "POST",
    //     dataType: "json",
    //     data: {sub_code: sub_code, item_category_code: category_id, count: count},
    //     success: function (data, textStatus, jqXHR)
    //     {
    //         event.parents('tr').find('.item_code').val(data.item_code);

    //     }
    // });
})

/*accept all qc in mfg */
function accept_all_mfg_receipt_qc(btnObj,receipt_code) {
    //console.log('asd')
     $(btnObj).attr('disabled','disabled',true);
      showMsgPopup("Success","Successfully Accepted", '', 'normal')
     window.location.href = url + 'Manufacturing_quality_control/store_all/'+receipt_code;   
}
   
/*send to  qc in mfg */
function send_mfg_qc(btnObj,receipt_code) {
    //console.log('asd')
     $(btnObj).attr('disabled','disabled',true);
      showMsgPopup("Success","Successfully Accepted", '', 'normal')
     window.location.href = url + 'Sending_to_qc_mfg/send_mfg_qc/'+receipt_code;   
}


function app_user_change_status(id,status){

    $.ajax({
            url: url + "User_app/update_status",
            type: "POST",
            dataType: "json",
            data: {id:id,status:status},
            success: function (data, textStatus, jqXHR)
            {   
                 if (data.status == "success") {
                showMsgPopup("Success","Successfully updated", '', 'normal')
                //location.reload();
                setTimeout(function () {
                    close_toast_popup(url + 'User_app')

                }, 2000)

            } else {
              $(".ajaxLoader").fadeOut();
                showMsgPopup("Error", "Something Went Wrong Try Again", '', 'error')

            }
            }
        });
}

/*new product recive from karigar*/
function validate_print_karigar_from_new_prod_recive(action)
{   
    $('#save_print').attr('disabled', true);
    $('text-danger').html('');
    var retvalue = true;
    if ($("#kariger_id").val() == "") {
        $("#kariger_id_error").text("This field is required.")
        retvalue = false;
    }
    if ($("#order_name").val() == "") {
        $("#order_name_error").text("This Order Name is required.")
        retvalue = false;
    }
    if (retvalue == true) {
        $.ajax({
            url: url + "Mfg_dep_direct_order/"+action,
            type: "POST",
            dataType: "json",
            data: $('form#print_receipt_frm').serialize(),
            success: function (data, textStatus, jqXHR)
            {
                console.log(data);
                if (data.status == 'failure') {
                    $('#save_print').attr('disabled', false);
                    $('.div_error').html('');
                    var html_error = '';
                    $.each(data.error, function (key, val) {
                        html_error += '<p class="text-danger">' + val + '</p>';
                    })
                    $('.div_error').html(html_error);
                }else if (data.status == 'success') {

                       $('#save_print').attr('disabled', true);                       
                        //window.location.href = url + 'Mfg_dep_direct_order/print_receipt/'+data.receipt_code;
                        setTimeout(function () {                
                            window.open(url + 'Mfg_dep_direct_order/print_receipt/'+data.receipt_code, '_blank');
                  
                            location.reload();
                        }, 200)
                   // setTimeout(function () {
                   //  }, 500)
                }else {
                   $('#save_print').attr('disabled', false);
                    showMsgPopup("Error", "Something Went Wrong Try Again", '', 'error')

                }
            
            },
            error: function (jqXHR, textStatus, errorThrown)
            {

            }
        });
    }
    if (retvalue == false) {
        $('#save_print').attr('disabled', false);
        return false;
    }

}

function upload_image_video($id){
      $('#customer_id').val($id);
     $('#upload_image').modal('show');
}

/*Kundan QC  Accept All */
function multiple_kundan_qc_to_qc() {
    var checkedValues = $('input:checkbox:checked').map(function () {
        return this.value;
    }).get();

    var error = 0;
    if (checkedValues.length == 0) {
        error = 1;
        showMsgPopup("Error", "Please check at least one checkbox", '', 'error');
    }
    if (error == 0) {
        var conf = confirm("Are you sure you want to QC Accept?");
        if (!conf)
        {
            return false;
        }

        $.ajax({
            url: url + "kundan_QC/accept_all",
            type: "POST",
            dataType: "json",
            data: $('form#qc_products').serialize(),
            success: function (data, textStatus, jqXHR)
            {   
                console.log(data);
                if (data.status == 'failure')
                {
                    showMsgPopup("Error", data.error, '', 'error')


                } else if (data.status == "success") {

                    showMsgPopup("Success", "Successfully QC Accept", '', 'normal')
                    setTimeout(function () {
                        close_toast_popup(url + 'kundan_QC?status=pending')


                    }, 600)
                    //window.location.href();

                } else {
                    showMsgPopup("Error", "Something Went Wrong Try Again", '', 'error')
                    setTimeout(function () {

                        close_toast_popup(url + 'Corporate_Process')

                    }, 2000)
                }

            },
            error: function (jqXHR, textStatus, errorThrown)
            {

            }
        });
    }
}
/*delete ready product */
function Delete_ready_products(id, tr, controller) {
      $('#d_' + id).attr('disabled', true);
    var conf = confirm("Are you sure you want to delete?");
    if (conf)
    {
        $.ajax({
            url: url + controller + "/" + "delete",
            type: "POST",
            dataType: "json",
            data: {product_code: id},
            success: function (data, textStatus, jqXHR)
            {
                if (data.status == 'failure')
                {     $('#b_' + id).attr('disabled', false);
                    error_msg(data.error);
                } else if (data.status == "success") {
                    
                    showMsgPopup("Success", "Successfully Deleted", '', 'normal')
                    setTimeout(function () {
                        close_toast_popup(url + controller)


                    }, 2000)

                } else {
                      $('#b_' + id).attr('disabled', false);
                    showMsgPopup("Error", "Something Went Wrong Try Again", '', 'error')
                    setTimeout(function () {
                        close_toast_popup(url + controller)

                    }, 2000)
                }

            },
            error: function (jqXHR, textStatus, errorThrown)
            {

            }
        });
    }else{
          $('#b_' + id).attr('disabled', false);
          return false;

    }
}


/*update kundan KARIGAR RECEIPT */
function update_kundan_karigar_order() {
    $('.print_kundan_order_up').prop('disabled', true);
    //var file_data =$('#action_type').val('update');

        if (!$('#kariger_id').val()) {
            $('.print_kundan_order_up').prop('disabled', false);
            showMsgPopup("Error", "Please Select Karigar", '', 'error')
            return false;
        }      
        $.ajax({
            url: url + 'karigar_receive_order/print_pdf',
            type: "POST",
            dataType: "json",
            //data: $('form#qc_check').serialize(),
            data:formData,
            success: function (data, textStatus, jqXHR)
            {
                //console.log(data)
                if (data.status == 'failure')
                {
                    $('.print_kundan_order_up').prop('disabled', false);
                    $('.div_error').html('');
                    var html_error = '';
                    $.each(data.error, function (key, val) {
                        html_error += '<p class="text-danger">' + val + '</p>';
                    })
                    $('.div_error').html(html_error);

                } else if (data.status == "success") {
                    $('.print_kundan_order_up').prop('disabled', true);
                    showMsgPopup("Success", "Successfully Updated", '', 'normal')
                    window.open(data.data.url, "_blank")
                    setTimeout(function () {
                        close_toast_popup(url + 'karigar_receive_order')

                    }, 500)
                } else {
                    $('.print_kundan_order_up').prop('disabled', false);
                    showMsgPopup("Error", "Something Went Wrong Try Again", '', 'error')
                }
            },
            error: function (jqXHR, textStatus, errorThrown)
            {

            }
        });
}


function copy_customer_orders(id) {
    var conf = confirm("Are you sure you want to copy?");
    if (conf)
    {
        $.ajax({
            url: url + "Customer_order/copy_order",
            type: "POST",
            dataType: "json",
            data: {id: id},
            success: function (data, textStatus, jqXHR)
            {//console.log(data);
                if (data.status == "success") {

                     controller = url + "Customer_order";
                    showMsgPopup("Success", data.msg, '', 'normal')
                    setTimeout(function () {
                        close_toast_popup(controller)


                    }, 2000)

                } else {

                    showMsgPopup("Error", "Something Went Wrong Try Again", '', 'error')
                    setTimeout(function () {
                        close_toast_popup(url + controller)

                    }, 2000)
                }

            },
            error: function (jqXHR, textStatus, errorThrown)
            {

            }
        });
    }
}


// function reprint_sent_to_karigar(){
//    $.ajax({
//         url: url + "Products_sent/reprint",
//         type: "POST",
//         dataType: "json",
//         data: $('form#product_reprint').serialize(),
//         success: function (data, textStatus, jqXHR)
//         {
//         //     if (data.status == 'failure')
//         //     {
//         //         error_msg(data.error);
//         //     } else
//         //     {
//         //         if (data.status == 'error')
//         //             $("#invalid_error").html('<b class="text-danger">' + data.data + '</b>');
//         //         else {
//         //             window.location.href = urls + 'Collection';
//         //         }
//         //     }
//         // },
//         // error: function (jqXHR, textStatus, errorThrown)
//         // {

//          }
//     });   
// }
function send_to_karigar_corporate_product() {
    $('.text-danger').html('');
    var karigar_id =$('#karigar_id_filter').val();
    // alert(kariger_id);
    // return false;

  $.ajax({
            url: url + "Print_Order/get_karigar_wt_limit" ,
            type: "POST",
            dataType: "json",
            data: $('form#print_order_data').serialize(),
            //data: {karigar_id: karigar_id},
            success: function (data, textStatus, jqXHR)
            {
                if (data.status == 'failure')                {
                   
                showMsgPopup("Error", "karigar weight limit excited", '', 'error')

                } else if (data.status == "success") {
                    showMsgPopup("Success", "Successfully Updated", '', 'normal')
                    setTimeout(function () {
                        close_toast_popup(url + 'Print_Order/createPdf/'+data.karigar_id)

                    }, 2000)

                } else {

                    showMsgPopup("Error", "Something Went Wrong Try Again", '', 'error')
                    setTimeout(function () {
                        close_toast_popup(url + 'Corporate_Process')

                    }, 2000)
                }
            },
            error: function (jqXHR, textStatus, errorThrown) {

            }
        });
}
/*import stock excel*/
function Save_stock_excel() {
  $(".ajaxLoader").fadeIn();
    $('.errors').html('');
    var formdata = new FormData();
    var file_data = $('input[type="file"]')[0].files;
    formdata.append("file_0", file_data[0]);
    $.ajax({
        url: url + "Ready_product/import",
        type: "POST",
        dataType: "json",
        contentType: false,
        processData: false,
        data: formdata,
        success: function (data, textStatus, jqXHR)
        {
            console.log(data.status)
            if (data.status == 'failure')
            {
              $(".ajaxLoader").fadeOut();
            $('.div_error').html('');
                    var html_error = '';
                    $.each(data.error, function (key, val) {
                        html_error += '<p class="text-danger">' + val + '</p>';
                    })
                    $('.div_error').html(html_error);


            } else if (data.status == "success") {
                showMsgPopup("Success", " Stock Successfully Added", '', 'normal')
                //location.reload();
                setTimeout(function () {
                    close_toast_popup(url + 'Upload_stock_excel')

                }, 2000)

            } else {
              $(".ajaxLoader").fadeOut();
                showMsgPopup("Error", "Something Went Wrong Try Again", '', 'error')

            }
        },
        error: function (jqXHR, textStatus, errorThrown)
        {

        }
    });
}



$('body').delegate('.create_code_order', 'change', function () {
   // $(this).table()   
    var tag_product_obj = {}; 
    var count = 0;    
    var data_code = $(this).val();
     $(".sub_code").each(function (key, value) {
        var sel_item_code = $(this).closest('tr').find('.item_cat :selected').attr('data-code');
        var sel_item_cnt = $(this).closest('tr').find('.item_cat :selected').attr('data-max_cnt');
        var sel_product_code = $(this).closest('tr').find('.item_cat ').attr('data-product_code');
        //alert(sel_item_cnt);
           if (typeof (sel_item_code) != 'undefined') {
            if (typeof (tag_product_obj[sel_item_cnt]) == 'undefined') {
                var f_cnt = parseInt(sel_item_cnt);
                
                tag_product_obj[sel_item_cnt] = f_cnt;
            //console.log(sel_product_code + '' + sel_item_code  + '-' + f_cnt)
                $(this).closest('tr').find('.parent_category_code').val(sel_item_code);
                $(this).closest('tr').find('.product_code_tp').val(sel_product_code + '' + sel_item_code  + '-' + f_cnt);
            } else {
                 $(this).closest('tr').find('.parent_category_code').val(sel_item_code);
                var cnt = parseInt(tag_product_obj[sel_item_cnt]);
                tag_product_obj[sel_item_cnt] = cnt + 1;
                $(this).closest('tr').find('.product_code_tp').val(sel_product_code + '' + sel_item_code + '-' + tag_product_obj[sel_item_cnt]);        
            }

        } else {
            $(this).closest('tr').find('.product_code_tp').val('');
        }
        //$(this).closest('tr').find('.parent_category_code').val(sel_product_code);
    });

  

    });


function set_net_weight(enc_id){
  var gross_weight = $('#gross_'+enc_id).val();
  $('#net_'+enc_id).val(gross_weight);
}

function get_repair_product_category(otnobj){
    if($(otnobj).val()){

        $.ajax({
            url: url + "Repair_product/get_repair_max_code",
            type: "POST",
            dataType: "json",
            data: {item_category_code: $(otnobj).val()},
            success: function (data, textStatus, jqXHR)
            {   
                var options = '<option value="">Select Product</option>';
                console.log(data);
                $.each(data,function(key1,val1){
                    options += '<option data-code="'+val1.code+'" value="'+val1.name+'" data-max_cnt="'+val1.max_count+'">'+val1.name+'</option>';
                });
                $('.item_cat').html(options);
            }
        });
    }
}

$('body').delegate('.create_repair_code', 'change', function () {
    var tag_product_obj = {};
    var event = $(this);
    sub_code = $(this).val();
    var category_id = $('#category_id').val();
    var count = 0;
   
    $(".abc").each(function (key, value) {
        var sel_item_code = $(this).closest('tr').find('.item_cat :selected').attr('data-code');
        var sel_item_cnt = $(this).closest('tr').find('.item_cat :selected').attr('data-max_cnt');
        var sel_product_code = $(this).closest('tr').find('.item_cat').attr('data-product_code');
        $(this).closest('tr').find('.repair_product_code').val(sel_item_code);
        // var sel_product_code = $(this).closest('tr').find('.pm_id').val(category_id);
        if (typeof (sel_item_code) != 'undefined') {
            if (typeof (tag_product_obj[sel_item_code]) == 'undefined') {
                var f_cnt = parseInt(sel_item_cnt);
                tag_product_obj[sel_item_code] = f_cnt;
                $(this).closest('tr').find('.product_code_rp').val(sel_item_code  + '' + category_id + '-' + f_cnt);
            } else {
                var cnt = parseInt(tag_product_obj[sel_item_code]);
                tag_product_obj[sel_item_code] = cnt + 1;
                $(this).closest('tr').find('.product_code_rp').val(sel_item_code  + '' + category_id + '-' + tag_product_obj[sel_item_code]);        
            }

        } else {
            $(this).closest('tr').find('.product_code_rp').val('');
        }
    });


})

function ready_products_barcode()
{   
        if ($("input[name='rp[]']:checked").val() == undefined) {
        showMsgPopup("Error", "Please select products", '', 'error');
        return false;
    }
    $.ajax({
        url: url + "Ready_product/barcode_products",
        type: "POST",
        dataType: "json",
        data: $('form#transfer_to_sales').serialize(),
        success: function (data, textStatus, jqXHR)
        {   console.log(data);
            if (data.status == "success") {
                showMsgPopup("Success", "Successfully Barcode", '', 'normal')
                window.open(url + 'Ready_product/print_product_barcode', '_blank');
                setTimeout(function () {
                    close_toast_popup(url + 'ready_product')

                }, 250)
            } else {
                showMsgPopup("Error", "Something Went Wrong Try Again", '', 'error')
            }
        },
        error: function (jqXHR, textStatus, errorThrown)
        {

        }
    });
}

function singal_ready_products_barcode(drp_id)
{   //alert(drp_id);
    $.ajax({
            url: url + "Ready_product/singal_barcode_products/" + drp_id,
            type: "POST",
            dataType: "json",
            data: {rp_id: drp_id},
        success: function (data, textStatus, jqXHR)
        {   //console.log(data);
            if (data.status == "success") {
                showMsgPopup("Success", "Successfully Barcode", '', 'normal')
                window.open(url + 'Ready_product/print_product_barcode', '_blank');
                setTimeout(function () {
                    close_toast_popup(url + 'ready_product')

                }, 250)
            } else {
                showMsgPopup("Error", "Something Went Wrong Try Again", '', 'error')
            }
        },
        error: function (jqXHR, textStatus, errorThrown)
        {

        }
    });
}

/*change_delivery_date_process function  */
function change_delivery_date_process()
{
    $.ajax({
        url: url + "Customer_order/update_delivery_date",
        type: "POST",
        dataType: "json",
        data: $('form#change_delivery_date_form').serialize(),
        success: function (data, textStatus, jqXHR)
        {
            if (data.status == "success") {
                showMsgPopup("Change Order delivery date successfully", '', 'normal')
             
                setTimeout(function () {
                    close_toast_popup(url + 'Customer_order?status=pending')

                }, 250)
            } else {
                showMsgPopup("Error", "Something Went Wrong Try Again", '', 'error')
            }

        },
        error: function (jqXHR, textStatus, errorThrown)
        {

        }
    });
}

function Save_product_location()
{
    $.ajax({
        url: url + "ready_product/change_location",
        type: "POST",
        dataType: "json",
        data: $('form#update_product_location').serialize(),
        success: function (data, textStatus, jqXHR)
        {
            console.log(data.voucher_no);
              if (data.status == "success") {
                showMsgPopup("Change location successfully", '', 'normal')
             
                setTimeout(function () {
                    close_toast_popup(url + 'sales_recieved_product/show/'+data.voucher_no)

                }, 250)
            } else {
                showMsgPopup("Error", "Something Went Wrong Try Again", '', 'error')
            }

        },
        error: function (jqXHR, textStatus, errorThrown)
        {

        }
    });
}

function save_product_category_rate(action) {
    $.ajax({
        url: url + "product_category_rate/" + action,
        type: "POST",
        dataType: "json",
        data: $('form#product_category_rate').serialize(),
        success: function (data, textStatus, jqXHR)
        {
            if (data.status == 'failure')
            {
                error_msg(data.error);

            } else if (data.status == "success") {
                if(action=='update'){
                    var msg="update";
                }else{
                     var msg="Added";
                }
                showMsgPopup("Success", "Successfully "+msg, '', 'normal')
                setTimeout(function () {
                    close_toast_popup(url + 'product_category_rate')

                }, 2000)

            } else {

                showMsgPopup("Error", "Something Went Wrong Try Again", '', 'error')
                setTimeout(function () {
                    close_toast_popup(url + 'product_category_rate')

                }, 2000)
            }
        },
        error: function (jqXHR, textStatus, errorThrown)
        {

        }
    });
}