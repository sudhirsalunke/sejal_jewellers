
/**
* Morris Chart
*/

!function($) {
    "use strict";

    var MorrisCharts = function() {};

    //creates line chart
    MorrisCharts.prototype.createLineChart = function(element, data, xkey, ykeys, labels, opacity, Pfillcolor, Pstockcolor, lineColors) {
        Morris.Line({
          element: element,
          data: data,
          xkey: xkey,
          ykeys: ykeys,
          labels: labels,
          fillOpacity: opacity,
          pointFillColors: Pfillcolor,
          pointStrokeColors: Pstockcolor,
          behaveLikeLine: true,
          gridLineColor: '#2f3e47',
          gridTextColor: '#98a6ad',
          hideHover: 'auto',
          lineWidth: '3px',
          pointSize: 0,
          preUnits: '$ ',
          resize: true, //defaulted to true
          lineColors: lineColors
        });
    },
    //creates area chart
    MorrisCharts.prototype.createAreaChart = function(element, pointSize, lineWidth, data, xkey, ykeys, labels, lineColors) {
        Morris.Area({
            element: element,
            pointSize: 0,
            lineWidth: 0,
            data: data,
            xkey: xkey,
            ykeys: ykeys,
            labels: labels,
            hideHover: 'auto',
            resize: true,
            gridLineColor: '#2f3e47',
            gridTextColor: '#98a6ad',
            lineColors: lineColors
        });
    },
    //creates area chart with dotted
    MorrisCharts.prototype.createAreaChartDotted = function(element, pointSize, lineWidth, data, xkey, ykeys, labels, Pfillcolor, Pstockcolor, lineColors) {
        Morris.Area({
            element: element,
            pointSize: 3,
            lineWidth: 1,
            data: data,
            xkey: xkey,
            ykeys: ykeys,
            labels: labels,
            hideHover: 'auto',
            pointFillColors: Pfillcolor,
            pointStrokeColors: Pstockcolor,
            resize: true,
            gridLineColor: '#2f3e47',
            gridTextColor: '#98a6ad',
            lineColors: lineColors
        });
    },
    //creates Bar chart
    MorrisCharts.prototype.createBarChart  = function(element, data, xkey, ykeys, labels, lineColors) {
      var Ratio = 0.7;
      var size = Object.keys(data).length;
      console.log(size)
      if( size == 1){
          Ratio = 0.08;
      }else if(size == 2){
          Ratio = 0.15;
      }else if(size == 3){
          Ratio = 0.2;
      }else if(size == 3){
          Ratio = 0.25;
      }else if(size == 4){
          Ratio = 0.3;
      }else if(size == 5){
          Ratio = 0.35;
      }else if(size == 6){
          Ratio = 0.4;
      }else if(size == 7){
          Ratio = 0.45;
      }else if(size == 8){
          Ratio = 0.6;
      }
        Morris.Bar({
            element: element,
            data: data,
            xkey: xkey,
            ykeys: ykeys,
            labels: labels,
            hideHover: 'auto',
            resize: true, //defaulted to true
            gridLineColor: '#2f3e47',
            gridTextColor: '#98a6ad',
            barColors: lineColors,
            barSizeRatio:Ratio,
        });
    },
    //creates Stacked chart
    MorrisCharts.prototype.createStackedChart  = function(element, data, xkey, ykeys, labels, lineColors) {
      var Ratio = 0.7;
      var size = Object.keys(data).length;
      console.log(size);
      if( size == 1){
          Ratio = 0.08;
      }else if(size == 2){
          Ratio = 0.15;
      }else if(size == 3){
          Ratio = 0.2;
      }else if(size == 3){
          Ratio = 0.25;
      }else if(size == 4){
          Ratio = 0.3;
      }else if(size == 5){
          Ratio = 0.35;
      }else if(size == 6){
          Ratio = 0.4;
      }else if(size == 7){
          Ratio = 0.45;
      }else if(size == 8){
          Ratio = 0.6;
      }
        Morris.Bar({
            element: element,
            data: data,
            barGap:1,
            xkey: xkey,
            ykeys: ykeys,
            stacked: true,
            labels: labels,
            hideHover: 'auto',
            resize: true, //defaulted to true
            gridLineColor: '#2f3e47',
            gridTextColor: '#98a6ad',
            barColors: lineColors,
            barSizeRatio:Ratio,
        });
    },
    //creates Donut chart
    MorrisCharts.prototype.createDonutChart = function(element, data, colors) {
        Morris.Donut({
            element: element,
            data: data,
            resize: true, //defaulted to true
            colors: colors,
            backgroundColor: '#eee',
            labelColor: '#98a6ad'
        });
    },
    MorrisCharts.prototype.init = function() {
        if ($('#morris-line-example').length) {
          //create line chart
          var $data  = [
               { y: '2008', a: 50, b: 0 },
              { y: '2009', a: 75, b: 50 },
              { y: '2010', a: 30, b: 80 },
              { y: '2011', a: 50, b: 50 },
              { y: '2012', a: 75, b: 10 },
              { y: '2013', a: 50, b: 40 },
              { y: '2014', a: 75, b: 50 },
              { y: '2015', a: 100, b: 70 }
            ];
            this.createLineChart('morris-line-example', $data, 'y', ['a', 'b'], ['Series A', 'Series B'],['0.1'],['#ffffff'],['#999999'], ['#ff8acc', '#5b69bc']);
          }

        if ($('#morris-area-example').length) {
          //creating area chart
          var $areaData = [
              { y: '2009', a: 10, b: 20 },
              { y: '2010', a: 75,  b: 65 },
              { y: '2011', a: 50,  b: 40 },
              { y: '2012', a: 75,  b: 65 },
              { y: '2013', a: 50,  b: 40 },
              { y: '2014', a: 75,  b: 65 },
              { y: '2015', a: 90, b: 60 }
          ];
          this.createAreaChart('morris-area-example', 0, 0, $areaData, 'y', ['a', 'b'], ['Series A', 'Series B'], ['#5b69bc', "#35b8e0"]);
          }

        if ($('#morris-area-with-dotted').length) {
          //creating area chart with dotted
          var $areaDotData = [
              { y: '2009', a: 10, b: 20 },
              { y: '2010', a: 75,  b: 65 },
              { y: '2011', a: 50,  b: 40 },
              { y: '2012', a: 75,  b: 65 },
              { y: '2013', a: 50,  b: 40 },
              { y: '2014', a: 75,  b: 65 },
              { y: '2015', a: 90, b: 60 }
          ];
          this.createAreaChartDotted('morris-area-with-dotted', 0, 0, $areaDotData, 'y', ['a', 'b'], ['Series A', 'Series B'],['#ffffff'],['#999999'], ['#5b69bc', "#35b8e0"]);
        }

        if ($('#morris-bar-example').length) {
          //creating bar chart
          /*var $barData  = [
              { y: '2009', a: 100, b: 90 },
              { y: '2010', a: 75,  b: 65 },
              { y: '2011', a: 50,  b: 40 },
              { y: '2012', a: 75,  b: 65 },
              { y: '2013', a: 50,  b: 40 },
              { y: '2014', a: 75,  b: 65 },
              { y: '2015', a: 100, b: 90 }
          ];*/
          this.createBarChart('morris-bar-example', arf_report, 'y', ['a', 'b'], ['Planned', 'Executed'], ['#E6419F', '#2DBCDE']);
        }

        if ($('#morris-bar-example1').length) {
          console.log(arf_report);
          //creating bar chart
          // var arf_report  = [
          //     { y: '2009', a: 100, b: 90 },
          //     { y: '2010', a: 75,  b: 65 },
          //     { y: '2011', a: 50,  b: 40 },
          //     { y: '2012', a: 75,  b: 65 },
          //     { y: '2013', a: 50,  b: 40 },
          //     { y: '2014', a: 75,  b: 65 },
          //     { y: '2015', a: 100, b: 90 }
          // ];
          this.createBarChart('morris-bar-example1', arf_report, 'y', ['a', 'b'], ['Planned', 'Executed'], ['#36C1E0', '#168244']);
        }

        if ($('#morris-bar-stacked').length) {
        //creating Stacked chart
        var $stckedData  = [
            { y: '2005', a: 45, b: 180, c:10 },
            { y: '2006', a: 75,  b: 65, c:10 },
            { y: '2007', a: 100, b: 90, c:10 },
            { y: '2008', a: 75,  b: 65, c:10 },
            { y: '2009', a: 100, b: 90, c:10 },
            { y: '2010', a: 75,  b: 65, c:10 },
            { y: '2011', a: 50,  b: 40, c:10 },
            { y: '2012', a: 75,  b: 65, c:10 },
            { y: '2013', a: 50,  b: 40, c:10 },
            { y: '2014', a: 75,  b: 65, c:10 },
            { y: '2015', a: 100, b: 90, c:10 }
        ];
        this.createStackedChart('morris-bar-stacked', all_arf_report, 'y', ['a', 'b', 'c'], ['Planned Activities','Open Activities','Closed Activities'], ['#FE89CD',"#5C68BA",'#34B8DF']);
        }

        if ($('#morris-donut-example').length) {
        //creating donut chart
        var $donutData = [
                {label: "Activity Closed", value: activity_status_closed},
                {label: "Activity Approved", value: activity_status_in_progress},
                {label: "Activity Pending", value: activity_status_pending}
            ];
        this.createDonutChart('morris-donut-example', $donutData, ['#EC3C3E', '#EDA82F', "#29A863"]);
      }

      if ($('#morris-donut-example2').length) {
        //creating donut chart
        var $donutData = [
                {label: "Closed Activities", value: activity_across_arf_closed},
                {label: "Open Activities", value: activity_across_arf_open}
                
            ];
        this.createDonutChart('morris-donut-example2', $donutData, ['#EC3C3E','#EDA82F']);
      }

      if ($('#morris-donut-example1').length) {
        //creating donut chart
        var $donutData = [
                {label: "Activity Closed", value: activity_status_closed},
                {label: "Activity Approved", value: activity_status_in_progress},
                {label: "Activity Pending", value: activity_status_pending}
            ];
        this.createDonutChart('morris-donut-example1', $donutData, ['#EC3C3E', '#F5C75A', "#29a863"]);
      }
       if ($('#morris-donut-example3').length) {
        //creating donut chart
        var $donutData = [
                {label: "Closed Activities", value: activity_across_arf_closed},
                {label: "Open Activities", value: activity_across_arf_open}
               
            ];
        this.createDonutChart('morris-donut-example3', $donutData, ["#EC3C3E",'#F5C75A']);
      }

    },
    //init
    $.MorrisCharts = new MorrisCharts, $.MorrisCharts.Constructor = MorrisCharts
}(window.jQuery),

//initializing 
function($) {
    "use strict";
    $.MorrisCharts.init();
}(window.jQuery);